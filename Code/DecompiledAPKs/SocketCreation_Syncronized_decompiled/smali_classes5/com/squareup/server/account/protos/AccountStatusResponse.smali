.class public final Lcom/squareup/server/account/protos/AccountStatusResponse;
.super Lcom/squareup/wire/AndroidMessage;
.source "AccountStatusResponse.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;,
        Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_API_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_BANK_ACCOUNT_STATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_FROZEN:Ljava/lang/Boolean;

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_R12_GETTING_STARTED_VIDEO_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_RETURN_POLICY:Ljava/lang/String; = ""

.field public static final DEFAULT_SERVER_TIME:Ljava/lang/String; = ""

.field public static final DEFAULT_STORE_AND_FORWARD_PAYMENT_EXPIRATION_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final api_authorized_application_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x24
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final api_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1b
    .end annotation
.end field

.field public final api_url_list:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x28
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AppointmentSettings#ADAPTER"
        tag = 0x30
    .end annotation
.end field

.field public final bank_account_status:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final business_banking:Lcom/squareup/server/account/protos/BusinessBanking;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.BusinessBanking#ADAPTER"
        tag = 0x2b
    .end annotation
.end field

.field public final device_credential:Lcom/squareup/server/account/protos/DeviceCredential;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.DeviceCredential#ADAPTER"
        tag = 0x26
    .end annotation
.end field

.field public final disputes:Lcom/squareup/server/account/protos/Disputes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Disputes#ADAPTER"
        tag = 0x2d
    .end annotation
.end field

.field public final employee:Lcom/squareup/server/account/protos/EmployeesEntity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.EmployeesEntity#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final features:Lcom/squareup/server/account/protos/FlagsAndPermissions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FeeTypesProto#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final fees:Lcom/squareup/server/account/protos/ProcessingFees;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFees#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final forced_offline_mode_server_urls:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x29
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final frozen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2f
    .end annotation
.end field

.field public final gift_cards:Lcom/squareup/server/account/protos/GiftCards;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.GiftCards#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstallmentsSettings#ADAPTER"
        tag = 0x2e
    .end annotation
.end field

.field public final instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstantDeposits#ADAPTER"
        tag = 0x18
    .end annotation
.end field

.field public final limits:Lcom/squareup/server/account/protos/Limits;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Limits#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.LoyaltyProgram#ADAPTER"
        tag = 0x2a
    .end annotation
.end field

.field public final merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.MerchantRegisterSettings#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final merchant_units:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.MerchantUnit#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x16
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/MerchantUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final notifications:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Notification#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public final open_tickets:Lcom/squareup/server/account/protos/OpenTickets;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.OpenTickets#ADAPTER"
        tag = 0x25
    .end annotation
.end field

.field public final other_tenders:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.OtherTenderType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public final preferences:Lcom/squareup/server/account/protos/Preferences;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Preferences#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.PrivacyFeatures#ADAPTER"
        tag = 0x32
    .end annotation
.end field

.field public final product_intent:Lcom/squareup/server/account/protos/ProductIntent;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProductIntent#ADAPTER"
        tag = 0x31
    .end annotation
.end field

.field public final r12_getting_started_video_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x27
    .end annotation
.end field

.field public final return_policy:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final server_time:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x20
    .end annotation
.end field

.field public final store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.StoreAndForwardKey#ADAPTER"
        tag = 0x1d
    .end annotation
.end field

.field public final store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.StoreAndForwardKey#ADAPTER"
        tag = 0x1c
    .end annotation
.end field

.field public final store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1e
    .end annotation
.end field

.field public final store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.StoreAndForwardUserCredential#ADAPTER"
        tag = 0x1f
    .end annotation
.end field

.field public final subscriptions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Subscription#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2c
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Subscription;",
            ">;"
        }
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final tax_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.TaxId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x19
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/TaxId;",
            ">;"
        }
    .end annotation
.end field

.field public final third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.OtherTenderType#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final tipping:Lcom/squareup/server/account/protos/Tipping;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Tipping#ADAPTER"
        tag = 0x21
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final tutorial:Lcom/squareup/server/account/protos/Tutorial;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Tutorial#ADAPTER"
        tag = 0x22
    .end annotation
.end field

.field public final user:Lcom/squareup/server/account/protos/User;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.User#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 41
    new-instance v0, Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 43
    sget-object v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 47
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 63
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->DEFAULT_STORE_AND_FORWARD_PAYMENT_EXPIRATION_SECONDS:Ljava/lang/Integer;

    .line 69
    sput-object v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->DEFAULT_FROZEN:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;Lokio/ByteString;)V
    .locals 1

    .line 420
    sget-object v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 421
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->success:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    .line 422
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    .line 423
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->message:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    .line 424
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    .line 425
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_message:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    .line 426
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user:Lcom/squareup/server/account/protos/User;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    .line 427
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->return_policy:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    .line 428
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits:Lcom/squareup/server/account/protos/Limits;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    .line 429
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 430
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders:Ljava/util/List;

    const-string v0, "other_tenders"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    .line 431
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 432
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications:Ljava/util/List;

    const-string v0, "notifications"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    .line 433
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    .line 434
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->bank_account_status:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    .line 435
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    .line 436
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    .line 437
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    .line 438
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units:Ljava/util/List;

    const-string v0, "merchant_units"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    .line 439
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    .line 440
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    .line 441
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids:Ljava/util/List;

    const-string v0, "tax_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    .line 442
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    .line 443
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 444
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 445
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    .line 446
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    .line 447
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->server_time:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    .line 448
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    .line 449
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    .line 450
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    .line 451
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->r12_getting_started_video_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    .line 452
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url_list:Ljava/util/List;

    const-string v0, "api_url_list"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    .line 453
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    .line 454
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    .line 455
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions:Ljava/util/List;

    const-string v0, "subscriptions"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    .line 456
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_authorized_application_ids:Ljava/util/List;

    const-string v0, "api_authorized_application_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    .line 457
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    .line 458
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    .line 459
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->forced_offline_mode_server_urls:Ljava/util/List;

    const-string v0, "forced_offline_mode_server_urls"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    .line 460
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes:Lcom/squareup/server/account/protos/Disputes;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    .line 461
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    .line 462
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->frozen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    .line 463
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    .line 464
    iget-object p2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    iput-object p2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    .line 465
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 859
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 523
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 524
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 525
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    .line 526
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    .line 527
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    .line 528
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    .line 529
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    .line 530
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    .line 531
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    .line 532
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    .line 533
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 534
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    .line 535
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 536
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    .line 537
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    .line 538
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    .line 539
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    .line 540
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    .line 541
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    .line 542
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    .line 543
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    .line 544
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    .line 545
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    .line 546
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    .line 547
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 548
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 549
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    .line 550
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    .line 551
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    .line 552
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    .line 553
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    .line 554
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    .line 555
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    .line 556
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    .line 557
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    .line 558
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    .line 559
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    .line 560
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    .line 561
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    .line 562
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    .line 563
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    .line 564
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    .line 565
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    .line 566
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    .line 567
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    .line 568
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    .line 569
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    .line 570
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 575
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_25

    .line 577
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 578
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 579
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 580
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 581
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 582
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 583
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 584
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 585
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Limits;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 586
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 587
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 588
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/OtherTenderType;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 589
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 590
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Preferences;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 591
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 592
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFees;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 593
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FeeTypesProto;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 594
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 595
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 596
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/GiftCards;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 597
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 598
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 599
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 600
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 601
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 602
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 603
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 604
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 605
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tipping;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 606
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tutorial;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 607
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 608
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 609
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 610
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 611
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/BusinessBanking;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 612
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 613
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 614
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/OpenTickets;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 615
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/DeviceCredential;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 616
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 617
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Disputes;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 618
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstallmentsSettings;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 619
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 620
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AppointmentSettings;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 621
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProductIntent;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 622
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->hashCode()I

    move-result v2

    :cond_24
    add-int/2addr v0, v2

    .line 623
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_25
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 2

    .line 470
    new-instance v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;-><init>()V

    .line 471
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->success:Ljava/lang/Boolean;

    .line 472
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->title:Ljava/lang/String;

    .line 473
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->message:Ljava/lang/String;

    .line 474
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_title:Ljava/lang/String;

    .line 475
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_message:Ljava/lang/String;

    .line 476
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user:Lcom/squareup/server/account/protos/User;

    .line 477
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->return_policy:Ljava/lang/String;

    .line 478
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits:Lcom/squareup/server/account/protos/Limits;

    .line 479
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 480
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders:Ljava/util/List;

    .line 481
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 482
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications:Ljava/util/List;

    .line 483
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences:Lcom/squareup/server/account/protos/Preferences;

    .line 484
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->bank_account_status:Ljava/lang/String;

    .line 485
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    .line 486
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    .line 487
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    .line 488
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units:Ljava/util/List;

    .line 489
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    .line 490
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    .line 491
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids:Ljava/util/List;

    .line 492
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url:Ljava/lang/String;

    .line 493
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 494
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 495
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    .line 496
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    .line 497
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->server_time:Ljava/lang/String;

    .line 498
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping:Lcom/squareup/server/account/protos/Tipping;

    .line 499
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    .line 500
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    .line 501
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->r12_getting_started_video_url:Ljava/lang/String;

    .line 502
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url_list:Ljava/util/List;

    .line 503
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    .line 504
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    .line 505
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions:Ljava/util/List;

    .line 506
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_authorized_application_ids:Ljava/util/List;

    .line 507
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    .line 508
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    .line 509
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->forced_offline_mode_server_urls:Ljava/util/List;

    .line 510
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes:Lcom/squareup/server/account/protos/Disputes;

    .line 511
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    .line 512
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->frozen:Ljava/lang/Boolean;

    .line 513
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    .line 514
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    .line 515
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    .line 516
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 2

    .line 810
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 811
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->title(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 812
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->message(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 813
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 814
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 815
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 816
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->return_policy(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 817
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 818
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 819
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 820
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 821
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 822
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 823
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->bank_account_status(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 824
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 825
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 826
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings(Lcom/squareup/server/account/protos/MerchantRegisterSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 827
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 828
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 829
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 830
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 831
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 832
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 833
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 834
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_payment_expiration_seconds(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 835
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 836
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->server_time(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 837
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping(Lcom/squareup/server/account/protos/Tipping;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 838
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 839
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eqz v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 840
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    if-eqz v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->r12_getting_started_video_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 841
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url_list(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 842
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 843
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    if-eqz v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking(Lcom/squareup/server/account/protos/BusinessBanking;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 844
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 845
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_authorized_application_ids(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 846
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets(Lcom/squareup/server/account/protos/OpenTickets;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 847
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eqz v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential(Lcom/squareup/server/account/protos/DeviceCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 848
    :cond_25
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->forced_offline_mode_server_urls(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 849
    :cond_26
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eqz v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes(Lcom/squareup/server/account/protos/Disputes;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 850
    :cond_27
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eqz v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings(Lcom/squareup/server/account/protos/InstallmentsSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 851
    :cond_28
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->frozen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 852
    :cond_29
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    if-eqz v0, :cond_2a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings(Lcom/squareup/server/account/protos/AppointmentSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 853
    :cond_2a
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eqz v0, :cond_2b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent(Lcom/squareup/server/account/protos/ProductIntent;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 854
    :cond_2b
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eqz v0, :cond_2c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    :cond_2c
    if-nez v1, :cond_2d

    move-object p1, p0

    goto :goto_0

    .line 855
    :cond_2d
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->overlay(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 3

    .line 682
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 683
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v0, :cond_1

    .line 684
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User;->populateDefaults()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    .line 685
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 687
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eqz v0, :cond_2

    .line 688
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Limits;->populateDefaults()Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    .line 689
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eq v0, v2, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 691
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_3

    .line 692
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object v0

    .line 693
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 695
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 696
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 697
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    if-eq v0, v2, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 699
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v0, :cond_5

    .line 700
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/OtherTenderType;->populateDefaults()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v0

    .line 701
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eq v0, v2, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 703
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 704
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 705
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    if-eq v0, v2, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 707
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_7

    .line 708
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Preferences;->populateDefaults()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    .line 709
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eq v0, v2, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 711
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_8

    .line 712
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFees;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object v0

    .line 713
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eq v0, v2, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 715
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eqz v0, :cond_9

    .line 716
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FeeTypesProto;->populateDefaults()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object v0

    .line 717
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eq v0, v2, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 719
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_a

    .line 720
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->populateDefaults()Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    move-result-object v0

    .line 721
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eq v0, v2, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings(Lcom/squareup/server/account/protos/MerchantRegisterSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 723
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 724
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 725
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    if-eq v0, v2, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 727
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    if-eqz v0, :cond_c

    .line 728
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/GiftCards;->populateDefaults()Lcom/squareup/server/account/protos/GiftCards;

    move-result-object v0

    .line 729
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    if-eq v0, v2, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 731
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-eqz v0, :cond_d

    .line 732
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    .line 733
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-eq v0, v2, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 735
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    if-eqz v0, :cond_e

    .line 736
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 737
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    if-eq v0, v2, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 739
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v0, :cond_f

    .line 740
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object v0

    .line 741
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eq v0, v2, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 743
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v0, :cond_10

    .line 744
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object v0

    .line 745
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eq v0, v2, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 747
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    if-eqz v0, :cond_11

    .line 748
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object v0

    .line 749
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    if-eq v0, v2, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 751
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    if-eqz v0, :cond_12

    .line 752
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Tipping;->populateDefaults()Lcom/squareup/server/account/protos/Tipping;

    move-result-object v0

    .line 753
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    if-eq v0, v2, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping(Lcom/squareup/server/account/protos/Tipping;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 755
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eqz v0, :cond_13

    .line 756
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Tutorial;->populateDefaults()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object v0

    .line 757
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eq v0, v2, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 759
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eqz v0, :cond_14

    .line 760
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    .line 761
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eq v0, v2, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 763
    :cond_14
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_15

    .line 764
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->populateDefaults()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object v0

    .line 765
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eq v0, v2, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 767
    :cond_15
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    if-eqz v0, :cond_16

    .line 768
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/BusinessBanking;->populateDefaults()Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object v0

    .line 769
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    if-eq v0, v2, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking(Lcom/squareup/server/account/protos/BusinessBanking;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 771
    :cond_16
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    if-eqz v0, :cond_17

    .line 772
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 773
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    if-eq v0, v2, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 775
    :cond_17
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v0, :cond_18

    .line 776
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/OpenTickets;->populateDefaults()Lcom/squareup/server/account/protos/OpenTickets;

    move-result-object v0

    .line 777
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eq v0, v2, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets(Lcom/squareup/server/account/protos/OpenTickets;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 779
    :cond_18
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eqz v0, :cond_19

    .line 780
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/DeviceCredential;->populateDefaults()Lcom/squareup/server/account/protos/DeviceCredential;

    move-result-object v0

    .line 781
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eq v0, v2, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential(Lcom/squareup/server/account/protos/DeviceCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 783
    :cond_19
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eqz v0, :cond_1a

    .line 784
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Disputes;->populateDefaults()Lcom/squareup/server/account/protos/Disputes;

    move-result-object v0

    .line 785
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eq v0, v2, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes(Lcom/squareup/server/account/protos/Disputes;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 787
    :cond_1a
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eqz v0, :cond_1b

    .line 788
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstallmentsSettings;->populateDefaults()Lcom/squareup/server/account/protos/InstallmentsSettings;

    move-result-object v0

    .line 789
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eq v0, v2, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings(Lcom/squareup/server/account/protos/InstallmentsSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 791
    :cond_1b
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    if-nez v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->DEFAULT_FROZEN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->frozen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 792
    :cond_1c
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    if-eqz v0, :cond_1d

    .line 793
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AppointmentSettings;->populateDefaults()Lcom/squareup/server/account/protos/AppointmentSettings;

    move-result-object v0

    .line 794
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    if-eq v0, v2, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings(Lcom/squareup/server/account/protos/AppointmentSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 796
    :cond_1d
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eqz v0, :cond_1e

    .line 797
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProductIntent;->populateDefaults()Lcom/squareup/server/account/protos/ProductIntent;

    move-result-object v0

    .line 798
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eq v0, v2, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent(Lcom/squareup/server/account/protos/ProductIntent;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 800
    :cond_1e
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eqz v0, :cond_1f

    .line 801
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->populateDefaults()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object v0

    .line 802
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eq v0, v2, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->requireBuilder(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    :cond_1f
    if-nez v1, :cond_20

    move-object v0, p0

    goto :goto_0

    .line 804
    :cond_20
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->populateDefaults()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 630
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 631
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 632
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 636
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v1, :cond_5

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 637
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", return_policy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eqz v1, :cond_7

    const-string v1, ", limits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 639
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v1, :cond_8

    const-string v1, ", features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 640
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", other_tenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 641
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v1, :cond_a

    const-string v1, ", third_party_card_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 642
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 643
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v1, :cond_c

    const-string v1, ", preferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 644
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", bank_account_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v1, :cond_e

    const-string v1, ", fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 646
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eqz v1, :cond_f

    const-string v1, ", fee_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 647
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v1, :cond_10

    const-string v1, ", merchant_register_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 648
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, ", merchant_units="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 649
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    if-eqz v1, :cond_12

    const-string v1, ", gift_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 650
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-eqz v1, :cond_13

    const-string v1, ", instant_deposits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 651
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, ", tax_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 652
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", api_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v1, :cond_16

    const-string v1, ", store_and_forward_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 654
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v1, :cond_17

    const-string v1, ", store_and_forward_bill_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 655
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    const-string v1, ", store_and_forward_payment_expiration_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 656
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    if-eqz v1, :cond_19

    const-string v1, ", store_and_forward_user_credential="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 657
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    if-eqz v1, :cond_1a

    const-string v1, ", server_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    if-eqz v1, :cond_1b

    const-string v1, ", tipping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 659
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eqz v1, :cond_1c

    const-string v1, ", tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 660
    :cond_1c
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eqz v1, :cond_1d

    const-string v1, ", employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 661
    :cond_1d
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    if-eqz v1, :cond_1e

    const-string v1, ", r12_getting_started_video_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 662
    :cond_1e
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1f

    const-string v1, ", api_url_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 663
    :cond_1f
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v1, :cond_20

    const-string v1, ", loyalty_program="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 664
    :cond_20
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    if-eqz v1, :cond_21

    const-string v1, ", business_banking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 665
    :cond_21
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_22

    const-string v1, ", subscriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 666
    :cond_22
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_23

    const-string v1, ", api_authorized_application_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 667
    :cond_23
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v1, :cond_24

    const-string v1, ", open_tickets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 668
    :cond_24
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eqz v1, :cond_25

    const-string v1, ", device_credential="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 669
    :cond_25
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_26

    const-string v1, ", forced_offline_mode_server_urls="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 670
    :cond_26
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eqz v1, :cond_27

    const-string v1, ", disputes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 671
    :cond_27
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eqz v1, :cond_28

    const-string v1, ", installments_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 672
    :cond_28
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    const-string v1, ", frozen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 673
    :cond_29
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    if-eqz v1, :cond_2a

    const-string v1, ", appointment_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 674
    :cond_2a
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eqz v1, :cond_2b

    const-string v1, ", product_intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 675
    :cond_2b
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eqz v1, :cond_2c

    const-string v1, ", privacy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AccountStatusResponse{"

    .line 676
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
