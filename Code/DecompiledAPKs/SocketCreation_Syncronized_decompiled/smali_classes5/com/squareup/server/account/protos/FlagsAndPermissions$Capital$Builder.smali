.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_manage_flex_offer_in_pos_android:Ljava/lang/Boolean;

.field public can_manage_flex_plan_in_pos_android:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19334
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;
    .locals 4

    .line 19352
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;->can_manage_flex_offer_in_pos_android:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;->can_manage_flex_plan_in_pos_android:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 19329
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    move-result-object v0

    return-object v0
.end method

.method public can_manage_flex_offer_in_pos_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;
    .locals 0

    .line 19339
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;->can_manage_flex_offer_in_pos_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_manage_flex_plan_in_pos_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 19346
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital$Builder;->can_manage_flex_plan_in_pos_android:Ljava/lang/Boolean;

    return-object p0
.end method
