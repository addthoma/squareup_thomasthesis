.class public final Lcom/squareup/server/account/protos/LoyaltyProgram;
.super Lcom/squareup/wire/AndroidMessage;
.source "LoyaltyProgram.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;,
        Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;,
        Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;,
        Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;,
        Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram;",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/LoyaltyProgram;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/LoyaltyProgram;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PROGRAM_STATUS:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public static final DEFAULT_PROGRAM_TYPE:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

.field public static final DEFAULT_QUALIFYING_PURCHASE_DESCRIPTION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ExpirationPolicy#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.LoyaltyProgram$PointsTerminology#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.LoyaltyProgram$ProgramStatus#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.LoyaltyProgram$LoyaltyProgramType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final qualifying_purchase_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final reward_tiers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.RewardTier#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 37
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->DEFAULT_PROGRAM_STATUS:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 41
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PROGRAM_TYPE_UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->DEFAULT_PROGRAM_TYPE:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;Ljava/lang/String;Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;Lcom/squareup/server/account/protos/AccrualRules;Ljava/util/List;Lcom/squareup/server/account/protos/ExpirationPolicy;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
            "Lcom/squareup/server/account/protos/AccrualRules;",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;",
            "Lcom/squareup/server/account/protos/ExpirationPolicy;",
            ")V"
        }
    .end annotation

    .line 112
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/LoyaltyProgram;-><init>(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;Ljava/lang/String;Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;Lcom/squareup/server/account/protos/AccrualRules;Ljava/util/List;Lcom/squareup/server/account/protos/ExpirationPolicy;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;Ljava/lang/String;Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;Lcom/squareup/server/account/protos/AccrualRules;Ljava/util/List;Lcom/squareup/server/account/protos/ExpirationPolicy;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
            "Lcom/squareup/server/account/protos/AccrualRules;",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;",
            "Lcom/squareup/server/account/protos/ExpirationPolicy;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 120
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 122
    iput-object p2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    .line 123
    iput-object p3, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 124
    iput-object p4, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    .line 125
    iput-object p5, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    const-string p1, "reward_tiers"

    .line 126
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    .line 127
    iput-object p7, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 227
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 147
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 148
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram;

    .line 149
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    .line 155
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    iget-object p1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    .line 156
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 161
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_6

    .line 163
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 171
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 2

    .line 132
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;-><init>()V

    .line 133
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    .line 134
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->qualifying_purchase_description:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 136
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    .line 137
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    .line 138
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers:Ljava/util/List;

    .line 139
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/LoyaltyProgram;
    .locals 2

    .line 216
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_status(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 217
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->qualifying_purchase_description(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 218
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_type(Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 219
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 220
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 221
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers(Ljava/util/List;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 222
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy(Lcom/squareup/server/account/protos/ExpirationPolicy;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object p1, p0

    goto :goto_0

    .line 223
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->overlay(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/LoyaltyProgram;
    .locals 3

    .line 192
    iget-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram;->DEFAULT_PROGRAM_STATUS:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_status(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram;->DEFAULT_PROGRAM_TYPE:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_type(Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eqz v0, :cond_2

    .line 195
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->populateDefaults()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object v0

    .line 196
    iget-object v2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eq v0, v2, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    if-eqz v0, :cond_3

    .line 199
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object v0

    .line 200
    iget-object v2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 202
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 203
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 204
    iget-object v2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    if-eq v0, v2, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers(Ljava/util/List;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    .line 206
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    if-eqz v0, :cond_5

    .line 207
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->populateDefaults()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object v0

    .line 208
    iget-object v2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    if-eq v0, v2, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy(Lcom/squareup/server/account/protos/ExpirationPolicy;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object v0, p0

    goto :goto_0

    .line 210
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram;->populateDefaults()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    if-eqz v1, :cond_0

    const-string v1, ", program_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", qualifying_purchase_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    if-eqz v1, :cond_2

    const-string v1, ", program_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eqz v1, :cond_3

    const-string v1, ", points_terminology="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    if-eqz v1, :cond_4

    const-string v1, ", accrual_rules="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", reward_tiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    if-eqz v1, :cond_6

    const-string v1, ", expiration_policy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyProgram{"

    .line 186
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
