.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public balance_applet_master_switch:Ljava/lang/Boolean;

.field public can_manage_square_card_master_switch:Ljava/lang/Boolean;

.field public collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

.field public show_active_card_freeze_toggle:Ljava/lang/Boolean;

.field public show_add_to_wallet:Ljava/lang/Boolean;

.field public show_card_activation_billing_address:Ljava/lang/Boolean;

.field public show_notification_preferences:Ljava/lang/Boolean;

.field public show_server_stamps_for_card_customization:Ljava/lang/Boolean;

.field public show_square_card_pan:Ljava/lang/Boolean;

.field public show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

.field public show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

.field public show_unified_activity:Ljava/lang/Boolean;

.field public use_balance_applet:Ljava/lang/Boolean;

.field public view_dashboard_reset_pin:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13659
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public balance_applet_master_switch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13675
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->balance_applet_master_switch:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
    .locals 18

    move-object/from16 v0, p0

    .line 13790
    new-instance v17, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    iget-object v2, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->use_balance_applet:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->balance_applet_master_switch:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    iget-object v5, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_card_activation_billing_address:Ljava/lang/Boolean;

    iget-object v7, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_add_to_wallet:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    iget-object v9, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    iget-object v10, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    iget-object v11, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_unified_activity:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_notification_preferences:Ljava/lang/Boolean;

    iget-object v15, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_pan:Ljava/lang/Boolean;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 13630
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object v0

    return-object v0
.end method

.method public can_manage_square_card_master_switch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13685
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    return-object p0
.end method

.method public collect_mobile_number_after_card_ordering_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13749
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_active_card_freeze_toggle(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13693
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_add_to_wallet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13712
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_add_to_wallet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_card_activation_billing_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13704
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_card_activation_billing_address:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_notification_preferences(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13776
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_notification_preferences:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_server_stamps_for_card_customization(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13722
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_square_card_pan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13784
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_pan:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_square_card_upsell_in_deposits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13731
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_square_card_upsell_in_instant_deposits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13759
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_unified_activity(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13767
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_unified_activity:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_balance_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13667
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->use_balance_applet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public view_dashboard_reset_pin(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    .line 13739
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    return-object p0
.end method
