.class final Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EmployeesEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PasscodeOnlyCredential"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 552
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 573
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;-><init>()V

    .line 574
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 575
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 581
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 579
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    goto :goto_0

    .line 578
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    goto :goto_0

    .line 577
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    goto :goto_0

    .line 585
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 586
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 550
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 565
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 566
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 567
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 568
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 550
    check-cast p2, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)I
    .locals 4

    .line 557
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    const/4 v3, 0x2

    .line 558
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 559
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 560
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 550
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;->encodedSize(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
    .locals 0

    .line 591
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object p1

    .line 592
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 593
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 550
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;->redact(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    return-object p1
.end method
