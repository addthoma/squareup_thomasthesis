.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public pos_bfd_connectivity:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13214
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;
    .locals 3

    .line 13224
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd$Builder;->pos_bfd_connectivity:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 13211
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    move-result-object v0

    return-object v0
.end method

.method public pos_bfd_connectivity(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd$Builder;
    .locals 0

    .line 13218
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd$Builder;->pos_bfd_connectivity:Ljava/lang/Boolean;

    return-object p0
.end method
