.class public final Lcom/squareup/server/account/protos/GiftCards$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GiftCards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/GiftCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/GiftCards;",
        "Lcom/squareup/server/account/protos/GiftCards$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public disable_gift_card_creation:Ljava/lang/Boolean;

.field public refund_to_any_gift_card:Ljava/lang/Boolean;

.field public track1_data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 173
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 174
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->bins:Ljava/util/List;

    .line 175
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->track1_data:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bins(Ljava/util/List;)Lcom/squareup/server/account/protos/GiftCards$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/GiftCards$Builder;"
        }
    .end annotation

    .line 182
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 183
    iput-object p1, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->bins:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/GiftCards;
    .locals 7

    .line 214
    new-instance v6, Lcom/squareup/server/account/protos/GiftCards;

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->bins:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->track1_data:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->refund_to_any_gift_card:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->disable_gift_card_creation:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/GiftCards;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 164
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards$Builder;->build()Lcom/squareup/server/account/protos/GiftCards;

    move-result-object v0

    return-object v0
.end method

.method public disable_gift_card_creation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/GiftCards$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->disable_gift_card_creation:Ljava/lang/Boolean;

    return-object p0
.end method

.method public refund_to_any_gift_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/GiftCards$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->refund_to_any_gift_card:Ljava/lang/Boolean;

    return-object p0
.end method

.method public track1_data(Ljava/util/List;)Lcom/squareup/server/account/protos/GiftCards$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/GiftCards$Builder;"
        }
    .end annotation

    .line 191
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 192
    iput-object p1, p0, Lcom/squareup/server/account/protos/GiftCards$Builder;->track1_data:Ljava/util/List;

    return-object p0
.end method
