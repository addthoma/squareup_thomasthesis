.class public final Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
.super Lcom/squareup/wire/AndroidMessage;
.source "InstantDeposits.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;,
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final DEFAULT_PAN_SUFFIX:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 304
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 306
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 310
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 312
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)V
    .locals 1

    .line 339
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 345
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 346
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 347
    iput-object p2, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 348
    iput-object p3, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 364
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 365
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    .line 366
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    .line 369
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 374
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 376
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 377
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 378
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 379
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 380
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    .locals 2

    .line 353
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;-><init>()V

    .line 354
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 355
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 356
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->pan_suffix:Ljava/lang/String;

    .line 357
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 303
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
    .locals 2

    .line 405
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v1

    .line 406
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v1

    .line 407
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 408
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 303
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->overlay(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
    .locals 2

    .line 397
    iget-object v0, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v1

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 399
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 303
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 388
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_0

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 389
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_1

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardDetails{"

    .line 391
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
