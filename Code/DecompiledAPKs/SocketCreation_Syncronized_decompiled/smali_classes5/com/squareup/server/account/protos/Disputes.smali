.class public final Lcom/squareup/server/account/protos/Disputes;
.super Lcom/squareup/wire/AndroidMessage;
.source "Disputes.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Disputes$ProtoAdapter_Disputes;,
        Lcom/squareup/server/account/protos/Disputes$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Disputes;",
        "Lcom/squareup/server/account/protos/Disputes$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Disputes;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Disputes;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Disputes;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Disputes;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HAS_ACTIVE_DISPUTES:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final has_active_disputes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/Disputes$ProtoAdapter_Disputes;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Disputes$ProtoAdapter_Disputes;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Disputes;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Disputes;->DEFAULT_HAS_ACTIVE_DISPUTES:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/Disputes;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Disputes$Builder;)Lcom/squareup/server/account/protos/Disputes$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Disputes;->newBuilder()Lcom/squareup/server/account/protos/Disputes$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 69
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Disputes;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 70
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Disputes;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Disputes;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Disputes;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    .line 72
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 77
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Disputes;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 81
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Disputes$Builder;
    .locals 2

    .line 60
    new-instance v0, Lcom/squareup/server/account/protos/Disputes$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Disputes$Builder;-><init>()V

    .line 61
    iget-object v1, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Disputes$Builder;->has_active_disputes:Ljava/lang/Boolean;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Disputes;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Disputes$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Disputes;->newBuilder()Lcom/squareup/server/account/protos/Disputes$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Disputes;)Lcom/squareup/server/account/protos/Disputes;
    .locals 2

    .line 103
    iget-object v0, p1, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Disputes;->requireBuilder(Lcom/squareup/server/account/protos/Disputes$Builder;)Lcom/squareup/server/account/protos/Disputes$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Disputes$Builder;->has_active_disputes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Disputes$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Disputes$Builder;->build()Lcom/squareup/server/account/protos/Disputes;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/Disputes;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Disputes;->overlay(Lcom/squareup/server/account/protos/Disputes;)Lcom/squareup/server/account/protos/Disputes;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Disputes;
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Disputes;->requireBuilder(Lcom/squareup/server/account/protos/Disputes$Builder;)Lcom/squareup/server/account/protos/Disputes$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Disputes;->DEFAULT_HAS_ACTIVE_DISPUTES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Disputes$Builder;->has_active_disputes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Disputes$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Disputes$Builder;->build()Lcom/squareup/server/account/protos/Disputes;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Disputes;->populateDefaults()Lcom/squareup/server/account/protos/Disputes;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", has_active_disputes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Disputes{"

    .line 90
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
