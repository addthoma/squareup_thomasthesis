.class public final Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExpirationCalendarPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public period:Ljava/lang/Long;

.field public unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 135
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
    .locals 4

    .line 156
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    iget-object v2, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->period:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;-><init>(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object v0

    return-object v0
.end method

.method public period(Ljava/lang/Long;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->period:Ljava/lang/Long;

    return-object p0
.end method

.method public unit(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    return-object p0
.end method
