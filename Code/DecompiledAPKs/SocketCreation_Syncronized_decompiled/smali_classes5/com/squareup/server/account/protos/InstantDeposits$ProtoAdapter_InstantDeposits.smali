.class final Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InstantDeposits"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/InstantDeposits;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 855
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/InstantDeposits;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 876
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;-><init>()V

    .line 877
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 878
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x6

    if-eq v3, v4, :cond_1

    const/4 v4, 0x7

    if-eq v3, v4, :cond_0

    .line 884
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 882
    :cond_0
    sget-object v3, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    goto :goto_0

    .line 881
    :cond_1
    sget-object v3, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    goto :goto_0

    .line 880
    :cond_2
    sget-object v3, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    goto :goto_0

    .line 888
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 889
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 853
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/InstantDeposits;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 868
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 869
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 870
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 871
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/InstantDeposits;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 853
    check-cast p2, Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/InstantDeposits;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/InstantDeposits;)I
    .locals 4

    .line 860
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    const/4 v3, 0x6

    .line 861
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    const/4 v3, 0x7

    .line 862
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 863
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 853
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;->encodedSize(Lcom/squareup/server/account/protos/InstantDeposits;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/InstantDeposits;
    .locals 2

    .line 894
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object p1

    .line 895
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    iput-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    .line 896
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iput-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 897
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iput-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 898
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 899
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 853
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;->redact(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object p1

    return-object p1
.end method
