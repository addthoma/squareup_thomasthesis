.class final Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;
.super Lcom/squareup/wire/ProtoAdapter;
.source "StoreAndForwardKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/StoreAndForwardKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_StoreAndForwardKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 169
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;-><init>()V

    .line 191
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 192
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 198
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 196
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->raw_certificate(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    goto :goto_0

    .line 195
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->expiration(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    goto :goto_0

    .line 194
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->bletchley_key_id(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    goto :goto_0

    .line 202
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 203
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/StoreAndForwardKey;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 182
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 183
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 184
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 185
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    check-cast p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/StoreAndForwardKey;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/StoreAndForwardKey;)I
    .locals 4

    .line 174
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    const/4 v3, 0x2

    .line 175
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    const/4 v3, 0x3

    .line 176
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 167
    check-cast p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;->encodedSize(Lcom/squareup/server/account/protos/StoreAndForwardKey;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 0

    .line 208
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object p1

    .line 209
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 210
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 167
    check-cast p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;->redact(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object p1

    return-object p1
.end method
