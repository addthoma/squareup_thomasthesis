.class public Lcom/squareup/server/account/CreateResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "CreateResponse.java"


# instance fields
.field public final existing_merchant:Z

.field public final features:Lcom/squareup/server/account/protos/FlagsAndPermissions;


# direct methods
.method public constructor <init>(ZZLjava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/protos/FlagsAndPermissions;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p3, p4}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 20
    iput-object p5, p0, Lcom/squareup/server/account/CreateResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 21
    iput-boolean p2, p0, Lcom/squareup/server/account/CreateResponse;->existing_merchant:Z

    return-void
.end method


# virtual methods
.method public populateDefaultValue()Lcom/squareup/server/account/CreateResponse;
    .locals 7

    .line 25
    iget-object v0, p0, Lcom/squareup/server/account/CreateResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object v0

    :goto_0
    move-object v6, v0

    .line 26
    new-instance v0, Lcom/squareup/server/account/CreateResponse;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/server/account/CreateResponse;->isSuccessful()Z

    move-result v2

    iget-boolean v3, p0, Lcom/squareup/server/account/CreateResponse;->existing_merchant:Z

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/CreateResponse;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/CreateResponse;->getMessage()Ljava/lang/String;

    move-result-object v5

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/server/account/CreateResponse;-><init>(ZZLjava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/protos/FlagsAndPermissions;)V

    return-object v0
.end method
