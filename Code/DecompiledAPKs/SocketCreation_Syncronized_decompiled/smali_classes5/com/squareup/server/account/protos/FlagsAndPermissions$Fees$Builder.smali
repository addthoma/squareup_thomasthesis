.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public tutorial_show_interac_limit:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13956
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;
    .locals 3

    .line 13966
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;->tutorial_show_interac_limit:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 13953
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    move-result-object v0

    return-object v0
.end method

.method public tutorial_show_interac_limit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;
    .locals 0

    .line 13960
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;->tutorial_show_interac_limit:Ljava/lang/Boolean;

    return-object p0
.end method
