.class public final Lcom/squareup/server/account/protos/ProductIntent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProductIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ProductIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/ProductIntent;",
        "Lcom/squareup/server/account/protos/ProductIntent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public best_available:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public best_available(Ljava/lang/String;)Lcom/squareup/server/account/protos/ProductIntent$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProductIntent$Builder;->best_available:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/ProductIntent;
    .locals 3

    .line 117
    new-instance v0, Lcom/squareup/server/account/protos/ProductIntent;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProductIntent$Builder;->best_available:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/ProductIntent;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProductIntent$Builder;->build()Lcom/squareup/server/account/protos/ProductIntent;

    move-result-object v0

    return-object v0
.end method
