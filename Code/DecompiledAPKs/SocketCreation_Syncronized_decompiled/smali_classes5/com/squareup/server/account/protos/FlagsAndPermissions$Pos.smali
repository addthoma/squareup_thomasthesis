.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pos"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$ProtoAdapter_Pos;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_SHOW_NOTIFICATION_CENTER:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SHOW_SALES_REPORT_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SHOW_SALES_REPORT_V2_FEATURE_CAROUSEL:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SHOW_SETTINGS_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_BLE_SCALES:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_CASH_MANAGEMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_FORCED_OFFLINE_MODE:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SCALES:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SPOS_ORDER_HUB:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_SANITIZE_EVENTSTREAM_COORDINATES:Ljava/lang/Boolean;

.field public static final DEFAULT_SETUP_GUIDE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEVICE_LEVEL_PAPER_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_V2_NAME_AND_NOTES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PREDEFINED_TICKETS_V2:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final can_show_notification_center:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final can_show_sales_report_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xab
    .end annotation
.end field

.field public final can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb3
    .end annotation
.end field

.field public final can_show_settings_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc4
    .end annotation
.end field

.field public final can_use_ble_scales:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb6
    .end annotation
.end field

.field public final can_use_cash_management:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbd
    .end annotation
.end field

.field public final can_use_forced_offline_mode:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final can_use_scales:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb5
    .end annotation
.end field

.field public final can_use_spos_order_hub:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa8
    .end annotation
.end field

.field public final enable_felica_certification_environment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb7
    .end annotation
.end field

.field public final sanitize_eventstream_coordinates:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc3
    .end annotation
.end field

.field public final setup_guide:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb4
    .end annotation
.end field

.field public final use_device_level_paper_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xaa
    .end annotation
.end field

.field public final use_open_tickets_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb8
    .end annotation
.end field

.field public final use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbc
    .end annotation
.end field

.field public final use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbb
    .end annotation
.end field

.field public final use_predefined_tickets_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xba
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11039
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$ProtoAdapter_Pos;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$ProtoAdapter_Pos;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11041
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 11045
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_FORCED_OFFLINE_MODE:Ljava/lang/Boolean;

    .line 11047
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_NOTIFICATION_CENTER:Ljava/lang/Boolean;

    .line 11049
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_SPOS_ORDER_HUB:Ljava/lang/Boolean;

    .line 11051
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_DEVICE_LEVEL_PAPER_SIGNATURE:Ljava/lang/Boolean;

    .line 11053
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_SALES_REPORT_V2:Ljava/lang/Boolean;

    .line 11055
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_SALES_REPORT_V2_FEATURE_CAROUSEL:Ljava/lang/Boolean;

    .line 11057
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_SETUP_GUIDE:Ljava/lang/Boolean;

    .line 11059
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_SCALES:Ljava/lang/Boolean;

    .line 11061
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_BLE_SCALES:Ljava/lang/Boolean;

    .line 11063
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Ljava/lang/Boolean;

    .line 11065
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_OPEN_TICKETS_V2:Ljava/lang/Boolean;

    .line 11067
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_PREDEFINED_TICKETS_V2:Ljava/lang/Boolean;

    .line 11069
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Ljava/lang/Boolean;

    .line 11071
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_OPEN_TICKETS_V2_NAME_AND_NOTES:Ljava/lang/Boolean;

    .line 11073
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_CASH_MANAGEMENT:Ljava/lang/Boolean;

    .line 11075
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_SANITIZE_EVENTSTREAM_COORDINATES:Ljava/lang/Boolean;

    .line 11077
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_SETTINGS_V2:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;Lokio/ByteString;)V
    .locals 1

    .line 11214
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 11215
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    .line 11216
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_notification_center:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    .line 11217
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_spos_order_hub:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    .line 11218
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_device_level_paper_signature:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    .line 11219
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    .line 11220
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    .line 11221
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->setup_guide:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    .line 11222
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_scales:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    .line 11223
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_ble_scales:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    .line 11224
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->enable_felica_certification_environment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    .line 11225
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    .line 11226
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    .line 11227
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    .line 11228
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    .line 11229
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_cash_management:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    .line 11230
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    .line 11231
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_settings_v2:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 11380
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 11261
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 11262
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    .line 11263
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    .line 11264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    .line 11265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    .line 11266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    .line 11267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    .line 11268
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    .line 11269
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    .line 11270
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    .line 11271
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    .line 11272
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    .line 11273
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    .line 11274
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    .line 11275
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    .line 11276
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    .line 11277
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    .line 11278
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    .line 11279
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    .line 11280
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 11285
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_11

    .line 11287
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 11288
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11289
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11290
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11291
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11292
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11293
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11294
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11295
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11296
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11297
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11298
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11299
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11300
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11301
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11302
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11303
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11304
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_10
    add-int/2addr v0, v2

    .line 11305
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_11
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 2

    .line 11236
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;-><init>()V

    .line 11237
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    .line 11238
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_notification_center:Ljava/lang/Boolean;

    .line 11239
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_spos_order_hub:Ljava/lang/Boolean;

    .line 11240
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_device_level_paper_signature:Ljava/lang/Boolean;

    .line 11241
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2:Ljava/lang/Boolean;

    .line 11242
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    .line 11243
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->setup_guide:Ljava/lang/Boolean;

    .line 11244
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_scales:Ljava/lang/Boolean;

    .line 11245
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_ble_scales:Ljava/lang/Boolean;

    .line 11246
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->enable_felica_certification_environment:Ljava/lang/Boolean;

    .line 11247
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2:Ljava/lang/Boolean;

    .line 11248
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    .line 11249
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    .line 11250
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    .line 11251
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_cash_management:Ljava/lang/Boolean;

    .line 11252
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    .line 11253
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_settings_v2:Ljava/lang/Boolean;

    .line 11254
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 11038
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
    .locals 2

    .line 11359
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_forced_offline_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11360
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_notification_center(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11361
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_spos_order_hub(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11362
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_device_level_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11363
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11364
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2_feature_carousel(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11365
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->setup_guide(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11366
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_scales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11367
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_ble_scales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11368
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->enable_felica_certification_environment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11369
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11370
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_predefined_tickets_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11371
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_search_sort_filter(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11372
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_name_and_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11373
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_cash_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11374
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->sanitize_eventstream_coordinates(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11375
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_settings_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    :cond_10
    if-nez v1, :cond_11

    move-object p1, p0

    goto :goto_0

    .line 11376
    :cond_11
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 11038
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
    .locals 2

    .line 11336
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_FORCED_OFFLINE_MODE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_forced_offline_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11337
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_NOTIFICATION_CENTER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_notification_center(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11338
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_SPOS_ORDER_HUB:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_spos_order_hub(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11339
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_DEVICE_LEVEL_PAPER_SIGNATURE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_device_level_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11340
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_SALES_REPORT_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11341
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_SALES_REPORT_V2_FEATURE_CAROUSEL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2_feature_carousel(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11342
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_SETUP_GUIDE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->setup_guide(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11343
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_SCALES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_scales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11344
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_BLE_SCALES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_ble_scales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11345
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->enable_felica_certification_environment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11346
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_OPEN_TICKETS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11347
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_PREDEFINED_TICKETS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_predefined_tickets_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11348
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_search_sort_filter(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11349
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_USE_OPEN_TICKETS_V2_NAME_AND_NOTES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_name_and_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11350
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_USE_CASH_MANAGEMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_cash_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11351
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_SANITIZE_EVENTSTREAM_COORDINATES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->sanitize_eventstream_coordinates(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    .line 11352
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->DEFAULT_CAN_SHOW_SETTINGS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_settings_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;

    move-result-object v1

    :cond_10
    if-nez v1, :cond_11

    move-object v0, p0

    goto :goto_0

    .line 11353
    :cond_11
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 11038
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 11312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11313
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", can_use_forced_offline_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11314
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", can_show_notification_center="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11315
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_use_spos_order_hub="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11316
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", use_device_level_paper_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11317
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", can_show_sales_report_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11318
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", can_show_sales_report_v2_feature_carousel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11319
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", setup_guide="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11320
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", can_use_scales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11321
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", can_use_ble_scales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11322
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", enable_felica_certification_environment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11323
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", use_open_tickets_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11324
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", use_predefined_tickets_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11325
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", use_open_tickets_v2_search_sort_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11326
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", use_open_tickets_v2_name_and_notes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11327
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", can_use_cash_management="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11328
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", sanitize_eventstream_coordinates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11329
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", can_show_settings_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Pos{"

    .line 11330
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
