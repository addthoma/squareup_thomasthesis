.class public final Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
.super Lcom/squareup/wire/AndroidMessage;
.source "ExpirationCalendarPeriod.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;,
        Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PERIOD:Ljava/lang/Long;

.field public static final DEFAULT_UNIT:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

.field private static final serialVersionUID:J


# instance fields
.field public final period:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field

.field public final unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.CalendarPeriod$CalendarUnit#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 35
    sget-object v0, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->DO_NOT_USE:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->DEFAULT_UNIT:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    const-wide/16 v0, 0x0

    .line 37
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->DEFAULT_PERIOD:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;Ljava/lang/Long;)V
    .locals 1

    .line 61
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;-><init>(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 67
    iput-object p1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    .line 68
    iput-object p2, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->newBuilder()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 83
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 97
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    .line 75
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->period:Ljava/lang/Long;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->newBuilder()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
    .locals 2

    .line 121
    iget-object v0, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->unit(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v1

    .line 122
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->period(Ljava/lang/Long;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 123
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->overlay(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->DEFAULT_UNIT:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->unit(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v1

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->DEFAULT_PERIOD:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->period(Ljava/lang/Long;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->populateDefaults()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    if-eqz v1, :cond_0

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ExpirationCalendarPeriod{"

    .line 107
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
