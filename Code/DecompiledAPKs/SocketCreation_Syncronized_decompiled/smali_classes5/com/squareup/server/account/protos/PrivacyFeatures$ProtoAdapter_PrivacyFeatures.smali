.class final Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PrivacyFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/PrivacyFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PrivacyFeatures"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/PrivacyFeatures;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 125
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/PrivacyFeatures;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 142
    new-instance v0, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;-><init>()V

    .line 143
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 144
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 148
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 146
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->opted_out_of_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    goto :goto_0

    .line 152
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 153
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->build()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/PrivacyFeatures;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 137
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/PrivacyFeatures;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    check-cast p2, Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/PrivacyFeatures;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/PrivacyFeatures;)I
    .locals 3

    .line 130
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;->encodedSize(Lcom/squareup/server/account/protos/PrivacyFeatures;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/PrivacyFeatures;
    .locals 0

    .line 158
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->newBuilder()Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 160
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->build()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;->redact(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object p1

    return-object p1
.end method
