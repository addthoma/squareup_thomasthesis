.class final Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;
.super Lcom/squareup/wire/ProtoAdapter;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$ReceiptAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReceiptAddress"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 969
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1000
    new-instance v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;-><init>()V

    .line 1001
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1002
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1013
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1011
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1010
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1009
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1008
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->state(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1007
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->city(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1006
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->address_line_3(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1005
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street2(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1004
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street1(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    goto :goto_0

    .line 1017
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1018
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->build()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 967
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$ReceiptAddress;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 987
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 988
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 989
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 990
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 991
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 992
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 993
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 994
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 995
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 967
    check-cast p2, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$ReceiptAddress;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/User$ReceiptAddress;)I
    .locals 4

    .line 974
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    const/4 v3, 0x2

    .line 975
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    const/4 v3, 0x3

    .line 976
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    const/4 v3, 0x4

    .line 977
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    const/4 v3, 0x5

    .line 978
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    const/4 v3, 0x6

    .line 979
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    const/4 v3, 0x7

    .line 980
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    const/16 v3, 0x8

    .line 981
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 982
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 967
    check-cast p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;->encodedSize(Lcom/squareup/server/account/protos/User$ReceiptAddress;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 1

    .line 1023
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->newBuilder()Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 1024
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street1:Ljava/lang/String;

    .line 1025
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street2:Ljava/lang/String;

    .line 1026
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->address_line_3:Ljava/lang/String;

    .line 1027
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->city:Ljava/lang/String;

    .line 1028
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->state:Ljava/lang/String;

    .line 1029
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->postal_code:Ljava/lang/String;

    .line 1030
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code:Ljava/lang/String;

    .line 1031
    iput-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->name:Ljava/lang/String;

    .line 1032
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1033
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->build()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 967
    check-cast p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;->redact(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    return-object p1
.end method
