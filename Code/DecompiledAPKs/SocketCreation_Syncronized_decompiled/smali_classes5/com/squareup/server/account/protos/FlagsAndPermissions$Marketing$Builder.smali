.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enable_sms_pilot:Ljava/lang/Boolean;

.field public enable_sms_pos_v2:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19501
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;
    .locals 4

    .line 19516
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;->enable_sms_pilot:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;->enable_sms_pos_v2:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 19496
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    move-result-object v0

    return-object v0
.end method

.method public enable_sms_pilot(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;
    .locals 0

    .line 19505
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;->enable_sms_pilot:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_sms_pos_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;
    .locals 0

    .line 19510
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing$Builder;->enable_sms_pos_v2:Ljava/lang/Boolean;

    return-object p0
.end method
