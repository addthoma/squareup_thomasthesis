.class public final enum Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;
.super Ljava/lang/Enum;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RoundingType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType$ProtoAdapter_RoundingType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BANKERS:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

.field public static final enum TRUNCATE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 3799
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    const/4 v1, 0x0

    const-string v2, "BANKERS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->BANKERS:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 3801
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    const/4 v2, 0x1

    const-string v3, "TRUNCATE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->TRUNCATE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 3798
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->BANKERS:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->TRUNCATE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->$VALUES:[Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 3803
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType$ProtoAdapter_RoundingType;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType$ProtoAdapter_RoundingType;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3807
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3808
    iput p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 3817
    :cond_0
    sget-object p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->TRUNCATE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    return-object p0

    .line 3816
    :cond_1
    sget-object p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->BANKERS:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;
    .locals 1

    .line 3798
    const-class v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;
    .locals 1

    .line 3798
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->$VALUES:[Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v0}, [Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3824
    iget v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->value:I

    return v0
.end method
