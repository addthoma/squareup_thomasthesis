.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public break_tracking_enabled:Ljava/lang/Boolean;

.field public mandatory_break_completion:Ljava/lang/Boolean;

.field public receipt_summary:Ljava/lang/Boolean;

.field public team_member_notes:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12897
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public break_tracking_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    .locals 0

    .line 12904
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->break_tracking_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
    .locals 7

    .line 12935
    new-instance v6, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->break_tracking_enabled:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->receipt_summary:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->mandatory_break_completion:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->team_member_notes:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 12888
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object v0

    return-object v0
.end method

.method public mandatory_break_completion(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    .locals 0

    .line 12921
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->mandatory_break_completion:Ljava/lang/Boolean;

    return-object p0
.end method

.method public receipt_summary(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    .locals 0

    .line 12912
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->receipt_summary:Ljava/lang/Boolean;

    return-object p0
.end method

.method public team_member_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    .locals 0

    .line 12929
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->team_member_notes:Ljava/lang/Boolean;

    return-object p0
.end method
