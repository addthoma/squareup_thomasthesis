.class public final Lcom/squareup/server/onboard/Address;
.super Lcom/squareup/server/onboard/ComponentBuilder;
.source "PanelComponents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u001c\u0018\u00002\u00020\u0001B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0002R/\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR/\u0010\u000c\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u000f\u0010\u000b\u001a\u0004\u0008\r\u0010\u0007\"\u0004\u0008\u000e\u0010\tR/\u0010\u0010\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0013\u0010\u000b\u001a\u0004\u0008\u0011\u0010\u0007\"\u0004\u0008\u0012\u0010\tR/\u0010\u0014\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0017\u0010\u000b\u001a\u0004\u0008\u0015\u0010\u0007\"\u0004\u0008\u0016\u0010\tR/\u0010\u0018\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001b\u0010\u000b\u001a\u0004\u0008\u0019\u0010\u0007\"\u0004\u0008\u001a\u0010\tR/\u0010\u001c\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001f\u0010\u000b\u001a\u0004\u0008\u001d\u0010\u0007\"\u0004\u0008\u001e\u0010\t\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/server/onboard/Address;",
        "Lcom/squareup/server/onboard/ComponentBuilder;",
        "()V",
        "<set-?>",
        "",
        "defaultCity",
        "getDefaultCity",
        "()Ljava/lang/String;",
        "setDefaultCity",
        "(Ljava/lang/String;)V",
        "defaultCity$delegate",
        "Lcom/squareup/server/onboard/PropertyEntryDelegate;",
        "defaultPostalCode",
        "getDefaultPostalCode",
        "setDefaultPostalCode",
        "defaultPostalCode$delegate",
        "defaultState",
        "getDefaultState",
        "setDefaultState",
        "defaultState$delegate",
        "defaultStreet1",
        "getDefaultStreet1",
        "setDefaultStreet1",
        "defaultStreet1$delegate",
        "defaultStreet2",
        "getDefaultStreet2",
        "setDefaultStreet2",
        "defaultStreet2$delegate",
        "label",
        "getLabel",
        "setLabel",
        "label$delegate",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final defaultCity$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

.field private final defaultPostalCode$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

.field private final defaultState$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

.field private final defaultStreet1$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

.field private final defaultStreet2$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

.field private final label$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/server/onboard/Address;

    const/4 v1, 0x6

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "label"

    const-string v5, "getLabel()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "defaultStreet1"

    const-string v5, "getDefaultStreet1()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "defaultStreet2"

    const-string v5, "getDefaultStreet2()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "defaultCity"

    const-string v5, "getDefaultCity()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "defaultState"

    const-string v5, "getDefaultState()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "defaultPostalCode"

    const-string v4, "getDefaultPostalCode()Ljava/lang/String;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 73
    sget-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-direct {p0, v0}, Lcom/squareup/server/onboard/ComponentBuilder;-><init>(Lcom/squareup/protos/client/onboard/ComponentType;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 74
    invoke-static {v0, v1, v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry$default(Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Address;->label$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    const-string v0, "default_address_line_1"

    .line 75
    invoke-static {v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultStreet1$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    const-string v0, "default_address_line_2"

    .line 76
    invoke-static {v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultStreet2$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    const-string v0, "default_locality"

    .line 77
    invoke-static {v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultCity$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    const-string v0, "default_admin_level_1"

    .line 78
    invoke-static {v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultState$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    const-string v0, "default_postal_code"

    .line 79
    invoke-static {v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultPostalCode$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    return-void
.end method


# virtual methods
.method public final getDefaultCity()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultCity$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultPostalCode()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultPostalCode$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultState()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultState$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultStreet1()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultStreet1$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultStreet2()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultStreet2$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->label$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final setDefaultCity(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultCity$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDefaultPostalCode(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultPostalCode$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDefaultState(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultState$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDefaultStreet1(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultStreet1$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDefaultStreet2(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->defaultStreet2$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setLabel(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Address;->label$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Address;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
