.class final synthetic Lcom/squareup/server/onboard/NavigationBuilder$timeoutMillis$2;
.super Lkotlin/jvm/internal/MutablePropertyReference0;
.source "Panels.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/onboard/Navigation$Builder;)V
    .locals 0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/MutablePropertyReference0;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder$timeoutMillis$2;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;

    .line 198
    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_duration:Ljava/lang/Integer;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "timeout_duration"

    return-object v0
.end method

.method public getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "getTimeout_duration()Ljava/lang/Integer;"

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder$timeoutMillis$2;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;

    .line 198
    check-cast p1, Ljava/lang/Integer;

    iput-object p1, v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_duration:Ljava/lang/Integer;

    return-void
.end method
