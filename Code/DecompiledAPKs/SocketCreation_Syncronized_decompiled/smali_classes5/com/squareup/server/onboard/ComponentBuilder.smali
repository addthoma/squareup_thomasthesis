.class public Lcom/squareup/server/onboard/ComponentBuilder;
.super Ljava/lang/Object;
.source "Panels.kt"


# annotations
.annotation runtime Lcom/squareup/server/onboard/PanelDsl;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0017\u0018\u00002\u00020\u0001B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u0006X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R7\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0011\u0010\u0012\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/server/onboard/ComponentBuilder;",
        "",
        "type",
        "Lcom/squareup/protos/client/onboard/ComponentType;",
        "(Lcom/squareup/protos/client/onboard/ComponentType;)V",
        "builder",
        "Lcom/squareup/protos/client/onboard/Component$Builder;",
        "getBuilder$public_release",
        "()Lcom/squareup/protos/client/onboard/Component$Builder;",
        "<set-?>",
        "",
        "Lcom/squareup/protos/client/onboard/Validator;",
        "validators",
        "getValidators",
        "()Ljava/util/List;",
        "setValidators",
        "(Ljava/util/List;)V",
        "validators$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final builder:Lcom/squareup/protos/client/onboard/Component$Builder;

.field private final validators$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string/jumbo v3, "validators"

    const-string v4, "getValidators()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/onboard/ComponentBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/ComponentType;)V
    .locals 2

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    new-instance v0, Lcom/squareup/protos/client/onboard/Component$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Component$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/server/onboard/ComponentBuilder;->builder:Lcom/squareup/protos/client/onboard/Component$Builder;

    .line 262
    new-instance v0, Lcom/squareup/server/onboard/ComponentBuilder$validators$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/ComponentBuilder;->builder:Lcom/squareup/protos/client/onboard/Component$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/ComponentBuilder$validators$2;-><init>(Lcom/squareup/protos/client/onboard/Component$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/ComponentBuilder;->validators$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 265
    iget-object v0, p0, Lcom/squareup/server/onboard/ComponentBuilder;->builder:Lcom/squareup/protos/client/onboard/Component$Builder;

    iput-object p1, v0, Lcom/squareup/protos/client/onboard/Component$Builder;->type:Lcom/squareup/protos/client/onboard/ComponentType;

    return-void
.end method


# virtual methods
.method public final getBuilder$public_release()Lcom/squareup/protos/client/onboard/Component$Builder;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/server/onboard/ComponentBuilder;->builder:Lcom/squareup/protos/client/onboard/Component$Builder;

    return-object v0
.end method

.method public final getValidators()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Validator;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/server/onboard/ComponentBuilder;->validators$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/ComponentBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final setValidators(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Validator;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/onboard/ComponentBuilder;->validators$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/ComponentBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
