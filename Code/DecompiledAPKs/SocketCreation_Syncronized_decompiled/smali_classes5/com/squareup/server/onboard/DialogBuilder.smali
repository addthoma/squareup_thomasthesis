.class public final Lcom/squareup/server/onboard/DialogBuilder;
.super Ljava/lang/Object;
.source "Panels.kt"


# annotations
.annotation runtime Lcom/squareup/server/onboard/PanelDsl;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0018\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R/\u0010\t\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR/\u0010\u0010\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0013\u0010\u000f\u001a\u0004\u0008\u0011\u0010\u000b\"\u0004\u0008\u0012\u0010\rR/\u0010\u0014\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0017\u0010\u000f\u001a\u0004\u0008\u0015\u0010\u000b\"\u0004\u0008\u0016\u0010\rR/\u0010\u0018\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001b\u0010\u000f\u001a\u0004\u0008\u0019\u0010\u000b\"\u0004\u0008\u001a\u0010\rR/\u0010\u001c\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001f\u0010\u000f\u001a\u0004\u0008\u001d\u0010\u000b\"\u0004\u0008\u001e\u0010\r\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/server/onboard/DialogBuilder;",
        "",
        "()V",
        "builder",
        "Lcom/squareup/protos/client/onboard/Dialog$Builder;",
        "getBuilder$public_release",
        "()Lcom/squareup/protos/client/onboard/Dialog$Builder;",
        "<set-?>",
        "",
        "message",
        "getMessage",
        "()Ljava/lang/String;",
        "setMessage",
        "(Ljava/lang/String;)V",
        "message$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "negativeButtonText",
        "getNegativeButtonText",
        "setNegativeButtonText",
        "negativeButtonText$delegate",
        "neutralButtonText",
        "getNeutralButtonText",
        "setNeutralButtonText",
        "neutralButtonText$delegate",
        "positiveButtonText",
        "getPositiveButtonText",
        "setPositiveButtonText",
        "positiveButtonText$delegate",
        "title",
        "getTitle",
        "setTitle",
        "title$delegate",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

.field private final message$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final negativeButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final neutralButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final positiveButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final title$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/server/onboard/DialogBuilder;

    const/4 v1, 0x5

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string/jumbo v4, "title"

    const-string v5, "getTitle()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "message"

    const-string v5, "getMessage()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "negativeButtonText"

    const-string v5, "getNegativeButtonText()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "positiveButtonText"

    const-string v5, "getPositiveButtonText()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "neutralButtonText"

    const-string v4, "getNeutralButtonText()Ljava/lang/String;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    new-instance v0, Lcom/squareup/protos/client/onboard/Dialog$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Dialog$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    .line 249
    new-instance v0, Lcom/squareup/server/onboard/DialogBuilder$title$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/DialogBuilder$title$2;-><init>(Lcom/squareup/protos/client/onboard/Dialog$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->title$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 250
    new-instance v0, Lcom/squareup/server/onboard/DialogBuilder$message$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/DialogBuilder$message$2;-><init>(Lcom/squareup/protos/client/onboard/Dialog$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->message$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 251
    new-instance v0, Lcom/squareup/server/onboard/DialogBuilder$negativeButtonText$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/DialogBuilder$negativeButtonText$2;-><init>(Lcom/squareup/protos/client/onboard/Dialog$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->negativeButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 252
    new-instance v0, Lcom/squareup/server/onboard/DialogBuilder$positiveButtonText$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/DialogBuilder$positiveButtonText$2;-><init>(Lcom/squareup/protos/client/onboard/Dialog$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->positiveButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 253
    new-instance v0, Lcom/squareup/server/onboard/DialogBuilder$neutralButtonText$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/DialogBuilder$neutralButtonText$2;-><init>(Lcom/squareup/protos/client/onboard/Dialog$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->neutralButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method


# virtual methods
.method public final getBuilder$public_release()Lcom/squareup/protos/client/onboard/Dialog$Builder;
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->builder:Lcom/squareup/protos/client/onboard/Dialog$Builder;

    return-object v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->message$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getNegativeButtonText()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->negativeButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getNeutralButtonText()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->neutralButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getPositiveButtonText()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->positiveButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->title$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final setMessage(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->message$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setNegativeButtonText(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->negativeButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setNeutralButtonText(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->neutralButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setPositiveButtonText(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->positiveButtonText$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/DialogBuilder;->title$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/DialogBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
