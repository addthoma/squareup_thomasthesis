.class public final Lcom/squareup/server/onboard/NavigationBuilder;
.super Ljava/lang/Object;
.source "Panels.kt"


# annotations
.annotation runtime Lcom/squareup/server/onboard/PanelDsl;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Panels.kt\ncom/squareup/server/onboard/NavigationBuilder\n*L\n1#1,379:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u001e\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u001f\u001a\u00020\u0010J$\u0010 \u001a\u00020\u00082\u0006\u0010!\u001a\u00020\"2\n\u0008\u0002\u0010#\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u001f\u001a\u00020\u0010H\u0002J\u001f\u0010$\u001a\u00020%2\u0017\u0010&\u001a\u0013\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020%0\'\u00a2\u0006\u0002\u0008)J\u0016\u0010*\u001a\u00020\u00082\u0006\u0010#\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u0010J\u0016\u0010+\u001a\u00020\u00082\u0006\u0010#\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u0010J\u0010\u0010,\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u001f\u001a\u00020\u0010R\u0014\u0010\u0003\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R/\u0010\t\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR/\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00108F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0016\u0010\u000f\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R/\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00178F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001d\u0010\u000f\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001c\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/server/onboard/NavigationBuilder;",
        "",
        "()V",
        "builder",
        "Lcom/squareup/protos/client/onboard/Navigation$Builder;",
        "getBuilder$public_release",
        "()Lcom/squareup/protos/client/onboard/Navigation$Builder;",
        "<set-?>",
        "",
        "canGoBack",
        "getCanGoBack",
        "()Ljava/lang/Boolean;",
        "setCanGoBack",
        "(Ljava/lang/Boolean;)V",
        "canGoBack$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "",
        "timeoutAction",
        "getTimeoutAction",
        "()Ljava/lang/String;",
        "setTimeoutAction",
        "(Ljava/lang/String;)V",
        "timeoutAction$delegate",
        "",
        "timeoutMillis",
        "getTimeoutMillis",
        "()Ljava/lang/Integer;",
        "setTimeoutMillis",
        "(Ljava/lang/Integer;)V",
        "timeoutMillis$delegate",
        "backButton",
        "action",
        "button",
        "style",
        "Lcom/squareup/protos/client/onboard/NavigationButton$Style;",
        "label",
        "exitDialog",
        "",
        "configure",
        "Lkotlin/Function1;",
        "Lcom/squareup/server/onboard/DialogBuilder;",
        "Lkotlin/ExtensionFunctionType;",
        "primaryButton",
        "secondaryButton",
        "xButton",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

.field private final canGoBack$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final timeoutAction$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final timeoutMillis$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/server/onboard/NavigationBuilder;

    const/4 v1, 0x3

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "canGoBack"

    const-string v5, "getCanGoBack()Ljava/lang/Boolean;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string/jumbo v4, "timeoutMillis"

    const-string v5, "getTimeoutMillis()Ljava/lang/Integer;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string/jumbo v3, "timeoutAction"

    const-string v4, "getTimeoutAction()Ljava/lang/String;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    new-instance v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Navigation$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    .line 192
    new-instance v0, Lcom/squareup/server/onboard/NavigationBuilder$canGoBack$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/NavigationBuilder$canGoBack$2;-><init>(Lcom/squareup/protos/client/onboard/Navigation$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->canGoBack$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 198
    new-instance v0, Lcom/squareup/server/onboard/NavigationBuilder$timeoutMillis$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/NavigationBuilder$timeoutMillis$2;-><init>(Lcom/squareup/protos/client/onboard/Navigation$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->timeoutMillis$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 203
    new-instance v0, Lcom/squareup/server/onboard/NavigationBuilder$timeoutAction$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/NavigationBuilder$timeoutAction$2;-><init>(Lcom/squareup/protos/client/onboard/Navigation$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->timeoutAction$delegate:Lkotlin/properties/ReadWriteProperty;

    const/4 v0, 0x1

    .line 206
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/server/onboard/NavigationBuilder;->setCanGoBack(Ljava/lang/Boolean;)V

    const/4 v0, 0x0

    .line 207
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/server/onboard/NavigationBuilder;->setTimeoutMillis(Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic backButton$default(Lcom/squareup/server/onboard/NavigationBuilder;Ljava/lang/String;ILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, "back"

    .line 236
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/server/onboard/NavigationBuilder;->backButton(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private final button(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->navigation_buttons:Ljava/util/List;

    new-instance v1, Lcom/squareup/protos/client/onboard/NavigationButton;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/protos/client/onboard/NavigationButton;-><init>(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method static synthetic button$default(Lcom/squareup/server/onboard/NavigationBuilder;Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 222
    check-cast p2, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/server/onboard/NavigationBuilder;->button(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic xButton$default(Lcom/squareup/server/onboard/NavigationBuilder;Ljava/lang/String;ILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, "back"

    .line 238
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/server/onboard/NavigationBuilder;->xButton(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final backButton(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->BACK_ARROW:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const-string v1, ""

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/server/onboard/NavigationBuilder;->button(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final exitDialog(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/DialogBuilder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "configure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    new-instance v0, Lcom/squareup/server/onboard/DialogBuilder;

    invoke-direct {v0}, Lcom/squareup/server/onboard/DialogBuilder;-><init>()V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object p1, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-virtual {v0}, Lcom/squareup/server/onboard/DialogBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Dialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/Dialog$Builder;->build()Lcom/squareup/protos/client/onboard/Dialog;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    return-void
.end method

.method public final getBuilder$public_release()Lcom/squareup/protos/client/onboard/Navigation$Builder;
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->builder:Lcom/squareup/protos/client/onboard/Navigation$Builder;

    return-object v0
.end method

.method public final getCanGoBack()Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->canGoBack$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public final getTimeoutAction()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->timeoutAction$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getTimeoutMillis()Ljava/lang/Integer;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->timeoutMillis$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public final primaryButton(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->PRIMARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/NavigationBuilder;->button(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final secondaryButton(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->SECONDARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/NavigationBuilder;->button(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final setCanGoBack(Ljava/lang/Boolean;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->canGoBack$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setTimeoutAction(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->timeoutAction$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setTimeoutMillis(Ljava/lang/Integer;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/NavigationBuilder;->timeoutMillis$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/NavigationBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final xButton(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->X_MARK:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const-string v1, ""

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/server/onboard/NavigationBuilder;->button(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
