.class final Lcom/squareup/server/messages/Message$Image$1;
.super Ljava/lang/Object;
.source "Message.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/messages/Message$Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/server/messages/Message$Image;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/server/messages/Message$Image;
    .locals 3

    .line 149
    new-instance v0, Lcom/squareup/server/messages/Message$Image;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/server/messages/Message$Image;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 147
    invoke-virtual {p0, p1}, Lcom/squareup/server/messages/Message$Image$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/server/messages/Message$Image;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/server/messages/Message$Image;
    .locals 0

    .line 153
    new-array p1, p1, [Lcom/squareup/server/messages/Message$Image;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 147
    invoke-virtual {p0, p1}, Lcom/squareup/server/messages/Message$Image$1;->newArray(I)[Lcom/squareup/server/messages/Message$Image;

    move-result-object p1

    return-object p1
.end method
