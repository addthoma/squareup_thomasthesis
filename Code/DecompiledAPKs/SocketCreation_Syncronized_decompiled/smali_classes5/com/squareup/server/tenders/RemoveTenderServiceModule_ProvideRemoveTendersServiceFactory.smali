.class public final Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;
.super Ljava/lang/Object;
.source "RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenders/RemoveTenderService;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)",
            "Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRemoveTendersService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/tenders/RemoveTenderService;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/server/tenders/RemoveTenderServiceModule;->provideRemoveTendersService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/tenders/RemoveTenderService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenders/RemoveTenderService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/tenders/RemoveTenderService;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;->provideRemoveTendersService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/tenders/RemoveTenderService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/server/tenders/RemoveTenderServiceModule_ProvideRemoveTendersServiceFactory;->get()Lcom/squareup/tenders/RemoveTenderService;

    move-result-object v0

    return-object v0
.end method
