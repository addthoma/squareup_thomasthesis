.class public Lcom/squareup/server/ExperimentsResponse$Bucket;
.super Ljava/lang/Object;
.source "ExperimentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/ExperimentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Bucket"
.end annotation


# instance fields
.field public final experiment_id:I

.field public final id:I

.field public final name:Ljava/lang/String;

.field public final weight:I


# direct methods
.method public constructor <init>(IILjava/lang/String;I)V
    .locals 0

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iput p1, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->experiment_id:I

    .line 205
    iput p2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->id:I

    .line 206
    iput-object p3, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    .line 207
    iput p4, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_7

    .line 212
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 214
    :cond_1
    check-cast p1, Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 216
    iget v2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->experiment_id:I

    iget v3, p1, Lcom/squareup/server/ExperimentsResponse$Bucket;->experiment_id:I

    if-eq v2, v3, :cond_2

    return v1

    .line 217
    :cond_2
    iget v2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->id:I

    iget v3, p1, Lcom/squareup/server/ExperimentsResponse$Bucket;->id:I

    if-eq v2, v3, :cond_3

    return v1

    .line 218
    :cond_3
    iget v2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    iget v3, p1, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    if-eq v2, v3, :cond_4

    return v1

    .line 219
    :cond_4
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    goto :goto_0

    :cond_5
    if-eqz p1, :cond_6

    :goto_0
    return v1

    :cond_6
    return v0

    :cond_7
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 225
    iget v0, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->experiment_id:I

    mul-int/lit8 v0, v0, 0x1f

    .line 226
    iget v1, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->id:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 227
    iget-object v1, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 228
    iget v1, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bucket{experiment_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->experiment_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", weight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
