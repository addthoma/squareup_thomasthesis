.class public abstract Lcom/squareup/server/RestAdapterCommonModule;
.super Ljava/lang/Object;
.source "RestAdapterCommonModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCogsProtoRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 97
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 98
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 99
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 100
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/Wire2Converter;

    invoke-direct {p1}, Lcom/squareup/server/Wire2Converter;-><init>()V

    .line 101
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 102
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method

.method static provideConnectRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/google/gson/Gson;)Lretrofit/RestAdapter;
    .locals 1
    .param p4    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 63
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 64
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 65
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 66
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/GsonConverter;

    invoke-direct {p1, p4}, Lcom/squareup/server/GsonConverter;-><init>(Lcom/google/gson/Gson;)V

    .line 67
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 68
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method

.method static provideConnectServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method

.method static provideGsonRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/google/gson/Gson;)Lretrofit/RestAdapter;
    .locals 1
    .param p4    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 53
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 54
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 55
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/GsonConverter;

    invoke-direct {p1, p4}, Lcom/squareup/server/GsonConverter;-><init>(Lcom/google/gson/Gson;)V

    .line 56
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 57
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method

.method static provideProtoRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 74
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 75
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 76
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 77
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/Wire2Converter;

    invoke-direct {p1}, Lcom/squareup/server/Wire2Converter;-><init>()V

    .line 78
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 79
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method

.method static provideTransactionLedgerRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 108
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 109
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 110
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 111
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/Wire2Converter;

    invoke-direct {p1}, Lcom/squareup/server/Wire2Converter;-><init>()V

    .line 112
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 113
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method

.method static provideTransactionLedgerServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method

.method static provideUnauthenticatedGsonRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/google/gson/Gson;)Lretrofit/RestAdapter;
    .locals 1
    .param p4    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 41
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 42
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 43
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 44
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/GsonConverter;

    invoke-direct {p1, p4}, Lcom/squareup/server/GsonConverter;-><init>(Lcom/google/gson/Gson;)V

    .line 45
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 46
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method

.method static provideUnauthenticatedProtoRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 86
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    .line 87
    invoke-static {p0}, Lcom/squareup/server/Retrofit1ServerEndpointKt;->toEndpoint(Lcom/squareup/http/Server;)Lretrofit/Endpoint;

    move-result-object p0

    invoke-virtual {v0, p0}, Lretrofit/RestAdapter$Builder;->setEndpoint(Lretrofit/Endpoint;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 88
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 89
    invoke-virtual {p0, p2, p3}, Lretrofit/RestAdapter$Builder;->setExecutors(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/server/Wire2Converter;

    invoke-direct {p1}, Lcom/squareup/server/Wire2Converter;-><init>()V

    .line 90
    invoke-virtual {p0, p1}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object p0

    .line 91
    invoke-virtual {p0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object p0

    return-object p0
.end method
