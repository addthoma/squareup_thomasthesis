.class public final Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;
.super Ljava/lang/Object;
.source "RetrofitModule_ProvideWeeblyServiceCreatorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ServiceCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final retrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;->retrofitProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;)",
            "Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideWeeblyServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/server/RetrofitModule;->INSTANCE:Lcom/squareup/server/RetrofitModule;

    invoke-virtual {v0, p0}, Lcom/squareup/server/RetrofitModule;->provideWeeblyServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/ServiceCreator;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/api/ServiceCreator;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;->retrofitProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/Retrofit;

    invoke-static {v0}, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;->provideWeeblyServiceCreator(Lretrofit2/Retrofit;)Lcom/squareup/api/ServiceCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;->get()Lcom/squareup/api/ServiceCreator;

    move-result-object v0

    return-object v0
.end method
