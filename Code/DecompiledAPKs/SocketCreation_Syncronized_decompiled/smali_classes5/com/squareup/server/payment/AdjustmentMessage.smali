.class public Lcom/squareup/server/payment/AdjustmentMessage;
.super Ljava/lang/Object;
.source "AdjustmentMessage.java"


# instance fields
.field private final amount_cents:Ljava/lang/Long;

.field public final amount_money:Lcom/squareup/protos/common/Money;

.field private final applied_amount_cents:J

.field public final applied_money:Lcom/squareup/protos/common/Money;

.field public final calculation_phase:I

.field public final child_adjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizedAdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final coupon_token:Ljava/lang/String;

.field private final currency_code:Lcom/squareup/protos/common/CurrencyCode;

.field public final id:Ljava/lang/String;

.field private final included_in_price:Z

.field private final inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

.field public final name:Ljava/lang/String;

.field public final percentage:Lcom/squareup/util/Percentage;

.field public final type:Ljava/lang/String;

.field public final type_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "I",
            "Lcom/squareup/api/items/Fee$InclusionType;",
            "Ljava/lang/String;",
            "Lcom/squareup/util/Percentage;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizedAdjustmentMessage;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p4, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    .line 81
    iput-object p10, p0, Lcom/squareup/server/payment/AdjustmentMessage;->child_adjustments:Ljava/util/List;

    .line 82
    iget-object p4, p4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_amount_cents:J

    .line 83
    iget-object p4, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object p4, p4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p4, p0, Lcom/squareup/server/payment/AdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 84
    iput p5, p0, Lcom/squareup/server/payment/AdjustmentMessage;->calculation_phase:I

    .line 85
    iput-object p1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->id:Ljava/lang/String;

    .line 86
    iput-object p6, p0, Lcom/squareup/server/payment/AdjustmentMessage;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 87
    sget-object p1, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne p6, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->included_in_price:Z

    .line 88
    iput-object p7, p0, Lcom/squareup/server/payment/AdjustmentMessage;->name:Ljava/lang/String;

    .line 89
    iput-object p8, p0, Lcom/squareup/server/payment/AdjustmentMessage;->percentage:Lcom/squareup/util/Percentage;

    .line 90
    iput-object p9, p0, Lcom/squareup/server/payment/AdjustmentMessage;->amount_money:Lcom/squareup/protos/common/Money;

    if-nez p9, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    .line 91
    :cond_1
    iget-object p1, p9, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    :goto_1
    iput-object p1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->amount_cents:Ljava/lang/Long;

    .line 92
    iput-object p2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    .line 93
    iput-object p3, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type_id:Ljava/lang/String;

    .line 94
    iput-object p11, p0, Lcom/squareup/server/payment/AdjustmentMessage;->coupon_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 113
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    .line 115
    :cond_1
    check-cast p1, Lcom/squareup/server/payment/AdjustmentMessage;

    .line 117
    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_amount_cents:J

    .line 118
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->applied_amount_cents:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 119
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->calculation_phase:I

    .line 120
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->calculation_phase:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->id:Ljava/lang/String;

    .line 121
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 122
    invoke-virtual {p0}, Lcom/squareup/server/payment/AdjustmentMessage;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/AdjustmentMessage;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->name:Ljava/lang/String;

    .line 123
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->percentage:Lcom/squareup/util/Percentage;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->percentage:Lcom/squareup/util/Percentage;

    .line 124
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->amount_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->amount_money:Lcom/squareup/protos/common/Money;

    .line 125
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->amount_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->amount_cents:Ljava/lang/Long;

    .line 126
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    .line 127
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/AdjustmentMessage;->type_id:Ljava/lang/String;

    .line 128
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->coupon_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/payment/AdjustmentMessage;->coupon_token:Ljava/lang/String;

    .line 129
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/server/payment/AdjustmentMessage;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/server/payment/AdjustmentMessage;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v0, :cond_0

    return-object v0

    .line 103
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/server/payment/AdjustmentMessage;->included_in_price:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    :goto_0
    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    .line 134
    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-wide v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_amount_cents:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->calculation_phase:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->id:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->name:Ljava/lang/String;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->percentage:Lcom/squareup/util/Percentage;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type_id:Ljava/lang/String;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->coupon_token:Ljava/lang/String;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Adjustment{applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->calculation_phase:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", inclusion_type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", percentage="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", type=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", type_id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/AdjustmentMessage;->type_id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", applied_amount_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->applied_amount_cents:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", currency_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", included_in_price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->included_in_price:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", coupon_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/AdjustmentMessage;->coupon_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
