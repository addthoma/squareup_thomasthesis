.class public final Lcom/squareup/server/accountstatus/AccountStatusServiceKt;
.super Ljava/lang/Object;
.source "AccountStatusService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u001e\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "getAccountStatusWithDefaults",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "Lcom/squareup/server/accountstatus/AccountStatusService;",
        "authToken",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getAccountStatusWithDefaults(Lcom/squareup/server/accountstatus/AccountStatusService;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$getAccountStatusWithDefaults"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-interface {p0, p1}, Lcom/squareup/server/accountstatus/AccountStatusService;->getAccountStatus(Ljava/lang/String;)Lcom/squareup/server/accountstatus/AccountStatusStandardResponse;

    move-result-object p0

    .line 15
    invoke-virtual {p0}, Lcom/squareup/server/accountstatus/AccountStatusStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p0

    .line 16
    sget-object p1, Lcom/squareup/server/accountstatus/AccountStatusServiceKt$getAccountStatusWithDefaults$1;->INSTANCE:Lcom/squareup/server/accountstatus/AccountStatusServiceKt$getAccountStatusWithDefaults$1;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "getAccountStatus(authTok\u2026onse::populateDefaults) }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
