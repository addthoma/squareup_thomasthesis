.class public interface abstract Lcom/squareup/server/address/AddressService;
.super Ljava/lang/Object;
.source "AddressService.java"


# static fields
.field public static final FORMAT:Ljava/lang/String; = "json"


# virtual methods
.method public abstract lookUp(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/address/PostalCodeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/postal-code/{code}"
    .end annotation
.end method

.method public abstract retail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "lat"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "lng"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "format"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/server/shipping/RetailLocationsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/retail"
    .end annotation
.end method
