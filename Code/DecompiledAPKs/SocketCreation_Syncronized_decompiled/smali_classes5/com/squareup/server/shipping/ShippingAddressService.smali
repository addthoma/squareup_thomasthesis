.class public interface abstract Lcom/squareup/server/shipping/ShippingAddressService;
.super Ljava/lang/Object;
.source "ShippingAddressService.java"


# virtual methods
.method public abstract verify(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/solidshop/verify-address"
    .end annotation
.end method
