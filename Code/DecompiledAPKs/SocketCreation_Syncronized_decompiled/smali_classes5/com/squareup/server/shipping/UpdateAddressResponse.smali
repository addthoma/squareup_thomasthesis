.class public Lcom/squareup/server/shipping/UpdateAddressResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "UpdateAddressResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/shipping/UpdateAddressResponse$Error;,
        Lcom/squareup/server/shipping/UpdateAddressResponse$FieldName;
    }
.end annotation


# instance fields
.field private final errors:[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 18
    iput-object p4, p0, Lcom/squareup/server/shipping/UpdateAddressResponse;->errors:[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;

    return-void
.end method

.method public static success()Lcom/squareup/server/shipping/UpdateAddressResponse;
    .locals 3

    .line 12
    new-instance v0, Lcom/squareup/server/shipping/UpdateAddressResponse;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1, v1, v1}, Lcom/squareup/server/shipping/UpdateAddressResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;)V

    return-object v0
.end method


# virtual methods
.method public getErrors()[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/server/shipping/UpdateAddressResponse;->errors:[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 4

    .line 22
    iget-object v0, p0, Lcom/squareup/server/shipping/UpdateAddressResponse;->errors:[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/squareup/server/SimpleResponse;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 23
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 24
    :goto_0
    iget-object v2, p0, Lcom/squareup/server/shipping/UpdateAddressResponse;->errors:[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;

    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 25
    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/squareup/server/shipping/UpdateAddressResponse$Error;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    iget-object v2, p0, Lcom/squareup/server/shipping/UpdateAddressResponse;->errors:[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_1

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
