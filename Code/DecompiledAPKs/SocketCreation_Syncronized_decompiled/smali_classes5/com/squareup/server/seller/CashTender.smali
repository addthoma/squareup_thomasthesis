.class public Lcom/squareup/server/seller/CashTender;
.super Ljava/lang/Object;
.source "CashTender.java"


# instance fields
.field public final change_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/server/seller/CashTender;->change_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_1

    .line 17
    instance-of v0, p1, Lcom/squareup/server/seller/CashTender;

    if-nez v0, :cond_0

    goto :goto_0

    .line 18
    :cond_0
    check-cast p1, Lcom/squareup/server/seller/CashTender;

    .line 19
    iget-object v0, p0, Lcom/squareup/server/seller/CashTender;->change_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/server/seller/CashTender;->change_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 23
    iget-object v1, p0, Lcom/squareup/server/seller/CashTender;->change_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
