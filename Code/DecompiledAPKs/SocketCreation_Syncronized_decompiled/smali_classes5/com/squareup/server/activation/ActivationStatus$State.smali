.class public final enum Lcom/squareup/server/activation/ActivationStatus$State;
.super Ljava/lang/Enum;
.source "ActivationStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/activation/ActivationStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/activation/ActivationStatus$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum APPROVED:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum CONFIRMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum DECLINED:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum MORE_INFORMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum NO_APPLICATION:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum PENDING:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum QUIZ_TIMED_OUT:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum UNDERWRITING_ERROR:Lcom/squareup/server/activation/ActivationStatus$State;

.field public static final enum UNRECOGNIZED:Lcom/squareup/server/activation/ActivationStatus$State;


# instance fields
.field private final jsonString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 46
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v1, 0x0

    const-string v2, "NO_APPLICATION"

    const-string v3, "no_application"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->NO_APPLICATION:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 48
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v2, 0x1

    const-string v3, "MORE_INFORMATION_REQUESTED"

    const-string v4, "more_information_requested"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->MORE_INFORMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 50
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v3, 0x2

    const-string v4, "CONFIRMATION_REQUESTED"

    const-string v5, "confirmation_requested"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->CONFIRMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 52
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v4, 0x3

    const-string v5, "PENDING"

    const-string v6, "pending"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->PENDING:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 54
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v5, 0x4

    const-string v6, "UNDERWRITING_ERROR"

    const-string/jumbo v7, "underwriting_error"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->UNDERWRITING_ERROR:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 56
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v6, 0x5

    const-string v7, "QUIZ_TIMED_OUT"

    const-string v8, "quiz_timed_out"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->QUIZ_TIMED_OUT:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 58
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v7, 0x6

    const-string v8, "APPROVED"

    const-string v9, "approved"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->APPROVED:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 60
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/4 v8, 0x7

    const-string v9, "DECLINED"

    const-string v10, "declined"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->DECLINED:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 62
    new-instance v0, Lcom/squareup/server/activation/ActivationStatus$State;

    const/16 v9, 0x8

    const-string v10, "UNRECOGNIZED"

    const-string v11, "<unrecognized>"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/server/activation/ActivationStatus$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->UNRECOGNIZED:Lcom/squareup/server/activation/ActivationStatus$State;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/server/activation/ActivationStatus$State;

    .line 44
    sget-object v10, Lcom/squareup/server/activation/ActivationStatus$State;->NO_APPLICATION:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->MORE_INFORMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->CONFIRMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->PENDING:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->UNDERWRITING_ERROR:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->QUIZ_TIMED_OUT:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->APPROVED:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->DECLINED:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/server/activation/ActivationStatus$State;->UNRECOGNIZED:Lcom/squareup/server/activation/ActivationStatus$State;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->$VALUES:[Lcom/squareup/server/activation/ActivationStatus$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput-object p3, p0, Lcom/squareup/server/activation/ActivationStatus$State;->jsonString:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Lcom/squareup/server/activation/ActivationStatus$State;
    .locals 0

    .line 44
    invoke-static {p0}, Lcom/squareup/server/activation/ActivationStatus$State;->fromJsonString(Ljava/lang/String;)Lcom/squareup/server/activation/ActivationStatus$State;

    move-result-object p0

    return-object p0
.end method

.method private static fromJsonString(Ljava/lang/String;)Lcom/squareup/server/activation/ActivationStatus$State;
    .locals 5

    .line 75
    invoke-static {}, Lcom/squareup/server/activation/ActivationStatus$State;->values()[Lcom/squareup/server/activation/ActivationStatus$State;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 76
    iget-object v4, v3, Lcom/squareup/server/activation/ActivationStatus$State;->jsonString:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    :cond_1
    sget-object p0, Lcom/squareup/server/activation/ActivationStatus$State;->UNRECOGNIZED:Lcom/squareup/server/activation/ActivationStatus$State;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/activation/ActivationStatus$State;
    .locals 1

    .line 44
    const-class v0, Lcom/squareup/server/activation/ActivationStatus$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/activation/ActivationStatus$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/activation/ActivationStatus$State;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/server/activation/ActivationStatus$State;->$VALUES:[Lcom/squareup/server/activation/ActivationStatus$State;

    invoke-virtual {v0}, [Lcom/squareup/server/activation/ActivationStatus$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/activation/ActivationStatus$State;

    return-object v0
.end method


# virtual methods
.method public getJsonString()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationStatus$State;->jsonString:Ljava/lang/String;

    return-object v0
.end method
