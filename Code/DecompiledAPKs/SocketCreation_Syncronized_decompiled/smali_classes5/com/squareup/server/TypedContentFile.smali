.class public Lcom/squareup/server/TypedContentFile;
.super Lretrofit/mime/TypedFile;
.source "TypedContentFile.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x1000


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lretrofit/mime/TypedFile;-><init>(Ljava/lang/String;Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 21
    new-instance v1, Lcom/squareup/server/ContentReader;

    invoke-virtual {p0}, Lcom/squareup/server/TypedContentFile;->file()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/server/ContentReader;-><init>(Ljava/io/File;)V

    .line 24
    :goto_0
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/squareup/server/ContentReader;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    .line 26
    invoke-virtual {p1, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 29
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/server/ContentReader;->close()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v1}, Lcom/squareup/server/ContentReader;->close()V

    .line 30
    throw p1
.end method
