.class public Lcom/squareup/server/employees/EmployeesResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "EmployeesResponse.java"


# instance fields
.field private final cursor:Ljava/lang/String;

.field private final employees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/EmployeesEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/EmployeesEntity;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    .line 14
    iput-object p1, p0, Lcom/squareup/server/employees/EmployeesResponse;->employees:Ljava/util/List;

    .line 15
    iput-object p2, p0, Lcom/squareup/server/employees/EmployeesResponse;->cursor:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCursor()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/server/employees/EmployeesResponse;->cursor:Ljava/lang/String;

    return-object v0
.end method

.method public getEmployees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/EmployeesEntity;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/server/employees/EmployeesResponse;->employees:Ljava/util/List;

    return-object v0
.end method
