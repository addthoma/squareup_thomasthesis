.class public Lcom/squareup/server/analytics/Es1BatchUploader;
.super Ljava/lang/Object;
.source "Es1BatchUploader.java"

# interfaces
.implements Lcom/squareup/eventstream/EventBatchUploader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/eventstream/EventBatchUploader<",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private final service:Lcom/squareup/server/analytics/EventStreamService;


# direct methods
.method constructor <init>(Lcom/squareup/server/analytics/EventStreamService;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/server/analytics/Es1BatchUploader;->service:Lcom/squareup/server/analytics/EventStreamService;

    return-void
.end method


# virtual methods
.method public upload(Ljava/util/List;)Lcom/squareup/eventstream/EventBatchUploader$Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            ">;)",
            "Lcom/squareup/eventstream/EventBatchUploader$Result;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;-><init>()V

    .line 27
    invoke-virtual {v0, p1}, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->events(Ljava/util/List;)Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;

    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->build()Lcom/squareup/protos/sawmill/LogEventStreamRequest;

    move-result-object p1

    .line 30
    iget-object v0, p0, Lcom/squareup/server/analytics/Es1BatchUploader;->service:Lcom/squareup/server/analytics/EventStreamService;

    invoke-interface {v0, p1}, Lcom/squareup/server/analytics/EventStreamService;->logEvents(Lcom/squareup/protos/sawmill/LogEventStreamRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->blocking()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 31
    invoke-static {p1}, Lcom/squareup/server/analytics/EventStreamHelper;->getResult(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/eventstream/EventBatchUploader$Result;

    move-result-object p1

    return-object p1
.end method
