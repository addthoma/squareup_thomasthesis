.class public final synthetic Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/tickets/RegisterTicketTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/tickets/RealTickets;

.field private final synthetic f$1:Ljava/lang/String;

.field private final synthetic f$2:Lcom/squareup/payment/Order;

.field private final synthetic f$3:Lcom/squareup/util/Res;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$0:Lcom/squareup/tickets/RealTickets;

    iput-object p2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$1:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$2:Lcom/squareup/payment/Order;

    iput-object p4, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$3:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/tickets/RegisterTicketTask$-CC;->$default$perform(Lcom/squareup/tickets/RegisterTicketTask;Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$0:Lcom/squareup/tickets/RealTickets;

    iget-object v1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$1:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$2:Lcom/squareup/payment/Order;

    iget-object v3, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;->f$3:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/tickets/RealTickets;->lambda$additiveMergeToTicket$17$RealTickets(Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$MergeResults;

    move-result-object p1

    return-object p1
.end method
