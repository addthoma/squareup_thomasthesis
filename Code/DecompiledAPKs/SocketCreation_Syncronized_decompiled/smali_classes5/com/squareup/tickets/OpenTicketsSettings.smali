.class public interface abstract Lcom/squareup/tickets/OpenTicketsSettings;
.super Ljava/lang/Object;
.source "OpenTicketsSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0015\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u000b\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u001dH&J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0003H&J\u0010\u0010!\u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020\u0007H&J\u0010\u0010#\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0003H&J\u0010\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020\u0003H&J\u0010\u0010&\u001a\u00020\u001f2\u0006\u0010\'\u001a\u00020\u0007H&J\u0010\u0010(\u001a\u00020\u001f2\u0006\u0010\'\u001a\u00020\u0007H&J\u0008\u0010)\u001a\u00020\u0007H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0008R\u0012\u0010\t\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0008R\u0012\u0010\n\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u0012\u0010\u000b\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0008R\u0012\u0010\u000c\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0008R\u0012\u0010\r\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u0008R\u0012\u0010\u000e\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0008R\u0012\u0010\u000f\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0008R\u0012\u0010\u0010\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0008R\u0012\u0010\u0011\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0008R\u0012\u0010\u0012\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0008R\u0012\u0010\u0013\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0008R\u0012\u0010\u0014\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0008R\u0012\u0010\u0015\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0008R\u0012\u0010\u0016\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0008R\u0012\u0010\u0017\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0008R\u0012\u0010\u0018\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0005R\u0012\u0010\u001a\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0005\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/tickets/OpenTicketsSettings;",
        "",
        "defaultTicketTemplateCount",
        "",
        "getDefaultTicketTemplateCount",
        "()I",
        "isBulkDeleteAllowed",
        "",
        "()Z",
        "isBulkVoidAllowed",
        "isDipTapToCreateTicketAllowed",
        "isMergeTicketsAllowed",
        "isMoveTicketsAllowed",
        "isOpenTicketsAllowed",
        "isOpenTicketsAsHomeScreenEnabled",
        "isOpenTicketsEmployeeFilteringEnabled",
        "isOpenTicketsEnabled",
        "isPredefinedTicketsAllowed",
        "isPredefinedTicketsEnabled",
        "isSavedCartsMode",
        "isSplitTicketsAllowed",
        "isSwipeToCreateTicketAllowed",
        "isTicketTransferAllowed",
        "isUsingDeviceProfiles",
        "maxTicketGroupCount",
        "getMaxTicketGroupCount",
        "maxTicketTemplatesPerGroup",
        "getMaxTicketTemplatesPerGroup",
        "onPredefinedTicketsEnabled",
        "Lrx/Observable;",
        "setDefaultTicketTemplateCount",
        "",
        "count",
        "setEnabled",
        "openTicketsEnabled",
        "setMaxTicketGroupCount",
        "setMaxTicketTemplatesPerGroup",
        "size",
        "setOpenTicketsAsHomeScreenEnabled",
        "enabled",
        "setPredefinedTicketsEnabled",
        "shouldShowOptInBetaWarning",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDefaultTicketTemplateCount()I
.end method

.method public abstract getMaxTicketGroupCount()I
.end method

.method public abstract getMaxTicketTemplatesPerGroup()I
.end method

.method public abstract isBulkDeleteAllowed()Z
.end method

.method public abstract isBulkVoidAllowed()Z
.end method

.method public abstract isDipTapToCreateTicketAllowed()Z
.end method

.method public abstract isMergeTicketsAllowed()Z
.end method

.method public abstract isMoveTicketsAllowed()Z
.end method

.method public abstract isOpenTicketsAllowed()Z
.end method

.method public abstract isOpenTicketsAsHomeScreenEnabled()Z
.end method

.method public abstract isOpenTicketsEmployeeFilteringEnabled()Z
.end method

.method public abstract isOpenTicketsEnabled()Z
.end method

.method public abstract isPredefinedTicketsAllowed()Z
.end method

.method public abstract isPredefinedTicketsEnabled()Z
.end method

.method public abstract isSavedCartsMode()Z
.end method

.method public abstract isSplitTicketsAllowed()Z
.end method

.method public abstract isSwipeToCreateTicketAllowed()Z
.end method

.method public abstract isTicketTransferAllowed()Z
.end method

.method public abstract isUsingDeviceProfiles()Z
.end method

.method public abstract onPredefinedTicketsEnabled()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setDefaultTicketTemplateCount(I)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setMaxTicketGroupCount(I)V
.end method

.method public abstract setMaxTicketTemplatesPerGroup(I)V
.end method

.method public abstract setOpenTicketsAsHomeScreenEnabled(Z)V
.end method

.method public abstract setPredefinedTicketsEnabled(Z)V
.end method

.method public abstract shouldShowOptInBetaWarning()Z
.end method
