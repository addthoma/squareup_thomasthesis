.class public Lcom/squareup/tickets/RealTickets;
.super Ljava/lang/Object;
.source "RealTickets.java"

# interfaces
.implements Lcom/squareup/tickets/Tickets;
.implements Lcom/squareup/tickets/Tickets$InternalTickets;


# static fields
.field private static final CLOSED_TICKET_EXPIRY_PERIOD_MS:J

.field private static final NO_LOCK:Ljava/lang/String; = null

.field private static final NUM_TICKETS_TO_CLOSE_PER_SWEEP:I = 0x64

.field private static final TICKET_LOCK_ID:Ljava/lang/String; = "TICKET_LOCK_ID"


# instance fields
.field private final broker:Lcom/squareup/tickets/TicketsBroker;

.field private final executor:Lcom/squareup/tickets/TicketsExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private lockedId:Ljava/lang/String;

.field private final mainScheduler:Lrx/Scheduler;

.field private final onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/LocalTicketUpdateEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final remoteUnsyncedFixableTickets:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteUnsyncedUnfixableTickets:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketStore:Lcom/squareup/tickets/TicketStore;

.field private final ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 81
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x48

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/tickets/RealTickets;->CLOSED_TICKET_EXPIRY_PERIOD_MS:J

    return-void
.end method

.method constructor <init>(Lcom/squareup/tickets/TicketStore;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/tickets/TicketsService;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/tickets/TicketsExecutor;Lrx/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 1
    .param p9    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketStore;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/server/tickets/TicketsService;",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;",
            "Lrx/Scheduler;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 109
    sget-object v0, Lcom/squareup/tickets/RealTickets;->NO_LOCK:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    .line 119
    iput-object p1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    .line 120
    iput-object p2, p0, Lcom/squareup/tickets/RealTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 121
    iput-object p3, p0, Lcom/squareup/tickets/RealTickets;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 122
    iput-object p5, p0, Lcom/squareup/tickets/RealTickets;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    .line 123
    iput-object p6, p0, Lcom/squareup/tickets/RealTickets;->remoteUnsyncedFixableTickets:Lcom/squareup/settings/LocalSetting;

    .line 124
    iput-object p7, p0, Lcom/squareup/tickets/RealTickets;->remoteUnsyncedUnfixableTickets:Lcom/squareup/settings/LocalSetting;

    .line 125
    iput-object p8, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    .line 126
    iput-object p9, p0, Lcom/squareup/tickets/RealTickets;->mainScheduler:Lrx/Scheduler;

    .line 127
    iput-object p10, p0, Lcom/squareup/tickets/RealTickets;->features:Lcom/squareup/settings/server/Features;

    .line 128
    iput-object p11, p0, Lcom/squareup/tickets/RealTickets;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 129
    new-instance p1, Lcom/squareup/tickets/TicketsBroker;

    invoke-direct {p1, p5, p4, p0}, Lcom/squareup/tickets/TicketsBroker;-><init>(Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/server/tickets/TicketsService;Lcom/squareup/tickets/Tickets$InternalTickets;)V

    iput-object p1, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/tickets/RealTickets;)Ljava/lang/String;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    return-object p1
.end method

.method private doCloseTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V
    .locals 1

    .line 885
    invoke-virtual {p2, p1}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 886
    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    .line 887
    invoke-virtual {p2, p3}, Lcom/squareup/tickets/OpenTicket;->setTerminalIdPair(Lcom/squareup/protos/client/IdPair;)V

    .line 888
    invoke-virtual {p2, p4, p1}, Lcom/squareup/tickets/OpenTicket;->setCloseReason(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;Ljava/util/Date;)V

    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    .line 890
    invoke-virtual {p3}, Lcom/squareup/protos/client/IdPair;->toString()Ljava/lang/String;

    move-result-object p3

    const/4 v0, 0x0

    aput-object p3, p1, v0

    const/4 p3, 0x1

    aput-object p4, p1, p3

    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object p3

    const/4 p4, 0x2

    aput-object p3, p1, p4

    const-string p3, "[Ticket_Tickets_Close] Using billId %s and reason %s to close ticket with id %s"

    .line 889
    invoke-static {p3, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 891
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {p1, p2}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 892
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {p1, p2}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    return-void
.end method

.method private doDeleteTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;)V
    .locals 2

    .line 896
    invoke-virtual {p2, p1}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 897
    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v0, 0x0

    .line 898
    invoke-virtual {p2, v0}, Lcom/squareup/tickets/OpenTicket;->setTerminalIdPair(Lcom/squareup/protos/client/IdPair;)V

    .line 899
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/tickets/OpenTicket;->setCloseReason(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;Ljava/util/Date;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    .line 901
    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v1, 0x1

    aput-object v0, p1, v1

    const-string v0, "[Ticket_Tickets_Delete] Deleting ticket with id %s for close reason %s"

    .line 900
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 902
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {p1, p2}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 903
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {p1, p2}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    return-void
.end method

.method private doMergeTickets(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/TicketStore;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZ)Lcom/squareup/tickets/Tickets$MergeResults;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Lcom/squareup/tickets/TicketStore;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "ZZ)",
            "Lcom/squareup/tickets/Tickets$MergeResults;"
        }
    .end annotation

    move-object v10, p0

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    .line 910
    iget-object v0, v11, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v0}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v13

    .line 911
    iget-object v0, v10, Lcom/squareup/tickets/RealTickets;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v8

    .line 912
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 915
    new-instance v14, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object v3, v14

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v9, p7

    .line 917
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tickets/RealTickets;->mergeToDestinationTicket(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/util/Set;Lcom/squareup/tickets/TicketStore;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZ)Lcom/squareup/tickets/Tickets$MergeResults;

    move-result-object v0

    .line 919
    iget-boolean v1, v0, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    if-eqz v1, :cond_0

    return-object v0

    .line 930
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 931
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 932
    invoke-virtual {v11, v2}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 933
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz p6, :cond_1

    new-array v4, v4, [Ljava/lang/Object;

    .line 937
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    const-string v3, "[Ticket_Tickets_Merge] Adding new ticket with id %s after merging to it"

    .line 936
    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 938
    invoke-interface {v12, v11}, Lcom/squareup/tickets/TicketStore;->addTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 939
    invoke-static/range {p2 .. p2}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-array v4, v4, [Ljava/lang/Object;

    .line 942
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    const-string v3, "[Ticket_Tickets_Merge] Updating existing ticket with id %s after merging to it"

    .line 941
    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 943
    invoke-interface {v12, v11}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 944
    invoke-static {v13, v11}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 948
    :goto_0
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tickets/OpenTicket;

    .line 949
    invoke-direct {p0, v2, v4}, Lcom/squareup/tickets/RealTickets;->doDeleteTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;)V

    .line 950
    invoke-static {v4}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forDelete(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 954
    :cond_2
    iget-object v2, v10, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {v2, v11}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 955
    iget-object v2, v10, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-object v0
.end method

.method private enforceOpenTicketsEnabled()V
    .locals 2

    .line 850
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 851
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should not be executing Tickets actions without Open Tickets enabled!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private enforceTicketIsLocked(Ljava/lang/String;)V
    .locals 3

    .line 857
    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 858
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot unlock id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " via given id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private hasCustomer(Lcom/squareup/tickets/OpenTicket;)Z
    .locals 0

    .line 1034
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz p1, :cond_0

    .line 1035
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isClosedTicketExpired(Lcom/squareup/tickets/OpenTicket;)Z
    .locals 5

    .line 863
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 867
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientClockVersion()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    .line 872
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClosedAt()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 879
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/squareup/tickets/RealTickets;->CLOSED_TICKET_EXPIRY_PERIOD_MS:J

    sub-long/2addr v1, v3

    .line 880
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    cmp-long p1, v3, v1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 875
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ticket does not have closed_at date: id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 876
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 868
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Closed ticket needs to be synced before deleting: id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 869
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 864
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ticket is not closed: id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private isDiscountApplicationIdEnabled()Z
    .locals 2

    .line 1048
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$getTicketCounts$0(Ljava/lang/String;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketCounter;
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Ticket_Tickets_Counts] Retrieving ticket counts."

    .line 136
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    invoke-interface {p1, p0}, Lcom/squareup/tickets/TicketStore;->getTicketCount(Ljava/lang/String;)I

    move-result v0

    .line 138
    invoke-interface {p1, p0}, Lcom/squareup/tickets/TicketStore;->getCustomTicketCount(Ljava/lang/String;)I

    move-result v1

    .line 139
    invoke-interface {p1, p0}, Lcom/squareup/tickets/TicketStore;->getGroupTicketCounts(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p0

    .line 140
    new-instance p1, Lcom/squareup/tickets/TicketCounter;

    invoke-direct {p1, v0, v1, p0}, Lcom/squareup/tickets/TicketCounter;-><init>(IILjava/util/Map;)V

    return-object p1
.end method

.method static synthetic lambda$getUnsyncedTicketCount$25(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Integer;
    .locals 0

    .line 761
    invoke-interface {p0}, Lcom/squareup/tickets/TicketStore;->getNonZeroClientClockTickets()Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p0

    .line 762
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result p0

    .line 761
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$retrieveNonzeroClientClockTickets$21(Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Ticket_Tickets_Nonzero] Retrieving all non-zero client clock tickets..."

    .line 542
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 543
    invoke-interface {p0}, Lcom/squareup/tickets/TicketStore;->getNonZeroClientClockTickets()Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$retrieveTicketInfo$20(Lcom/squareup/tickets/TicketStore;)Ljava/util/List;
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Ticket_Tickets_Info] Retrieving ticket info..."

    .line 534
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 535
    invoke-interface {p0}, Lcom/squareup/tickets/TicketStore;->getNonClosedTicketsUnordered()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private logTicketListRetrieval(Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 2

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1044
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    invoke-virtual {p3}, Lcom/squareup/tickets/TicketSort;->name()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    const/4 p1, 0x3

    aput-object p4, v0, p1

    const/4 p1, 0x4

    aput-object p5, v0, p1

    invoke-virtual {p6}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, v0, p2

    const-string p1, "[Ticket_Tickets_Cursor] Retrieving ticket cursor with groupId %s, onlyCustomTickets %b, sort %s, filter %s, employeeToken %s, and access %s."

    .line 1042
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private mergeToDestinationTicket(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/util/Set;Lcom/squareup/tickets/TicketStore;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZ)Lcom/squareup/tickets/Tickets$MergeResults;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            ">;",
            "Lcom/squareup/tickets/TicketStore;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "ZZ)",
            "Lcom/squareup/tickets/Tickets$MergeResults;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v8, p2

    .line 966
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getItemsCount()I

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getVoidedItemsCount()I

    move-result v2

    add-int v14, v1, v2

    .line 967
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v15

    .line 969
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v12, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 970
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v13

    .line 973
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v11, 0x0

    move/from16 v1, p9

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 974
    invoke-direct {v0, v3}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v11

    const-string v3, "[Ticket_Tickets_Merge] Skipping merge for locked ticket with id %s"

    .line 975
    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 978
    :cond_0
    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_0

    :cond_1
    move-object/from16 v10, p5

    .line 981
    invoke-interface {v10, v3}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 986
    invoke-virtual {v7}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v16, 0x1

    goto :goto_1

    :cond_2
    move/from16 v16, v1

    .line 990
    :goto_1
    invoke-virtual {v7}, Lcom/squareup/tickets/OpenTicket;->getItemsCount()I

    move-result v1

    invoke-virtual {v7}, Lcom/squareup/tickets/OpenTicket;->getVoidedItemsCount()I

    move-result v3

    add-int/2addr v1, v3

    add-int v17, v2, v1

    .line 991
    invoke-virtual {v7}, Lcom/squareup/tickets/OpenTicket;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 992
    invoke-virtual {v7}, Lcom/squareup/tickets/OpenTicket;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 996
    :cond_3
    invoke-virtual {v7}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v1, :cond_4

    .line 997
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    move-object/from16 v6, p4

    if-eqz v1, :cond_5

    .line 999
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object/from16 v5, p6

    move-object/from16 v4, p7

    move/from16 v3, p8

    .line 1003
    invoke-static {v5, v7, v4, v3}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromOrder(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object v2

    const/16 v18, 0x1

    .line 1005
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tickets/RealTickets;->isDiscountApplicationIdEnabled()Z

    move-result v19

    const/16 v20, 0x0

    move-object/from16 v1, p2

    move-object/from16 v3, p7

    move/from16 v4, v18

    move/from16 v5, p8

    move/from16 v6, v19

    move-object v11, v7

    move-object/from16 v7, v20

    .line 1004
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/tickets/OpenTicket;->additiveMergeCartToTicket(Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZZLcom/squareup/protos/client/Employee;)V

    move-object/from16 v5, p3

    .line 1006
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v1, v16

    move/from16 v2, v17

    const/4 v11, 0x0

    goto/16 :goto_0

    .line 983
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No ticket found with id "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object/from16 v5, p3

    if-eqz v1, :cond_8

    .line 1011
    iget-object v1, v0, Lcom/squareup/tickets/RealTickets;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3, v2, v14, v13}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logMergeTicketsCanceled(IIILjava/lang/String;)V

    .line 1013
    new-instance v1, Lcom/squareup/tickets/Tickets$MergeResults;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tickets/Tickets$MergeResults;-><init>(Ljava/lang/String;IZ)V

    return-object v1

    :cond_8
    const/4 v3, 0x0

    .line 1015
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    .line 1017
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getItemsCount()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getVoidedItemsCount()I

    move-result v6

    add-int v16, v5, v6

    .line 1018
    iget-object v9, v0, Lcom/squareup/tickets/RealTickets;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    .line 1020
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v17

    move v10, v1

    move v11, v2

    .line 1018
    invoke-virtual/range {v9 .. v17}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logMergeTickets(IILjava/util/List;Ljava/lang/String;ILcom/squareup/protos/common/Money;ILcom/squareup/protos/common/Money;)V

    .line 1027
    invoke-interface/range {p4 .. p4}, Ljava/util/Set;->size()I

    move-result v2

    if-ne v2, v4, :cond_9

    invoke-direct {v0, v8}, Lcom/squareup/tickets/RealTickets;->hasCustomer(Lcom/squareup/tickets/OpenTicket;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1028
    invoke-interface/range {p4 .. p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v8, v2, v5}, Lcom/squareup/tickets/OpenTicket;->setBuyerInfo(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/Date;)V

    .line 1030
    :cond_9
    new-instance v2, Lcom/squareup/tickets/Tickets$MergeResults;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v5

    add-int/2addr v1, v4

    invoke-direct {v2, v5, v1, v3}, Lcom/squareup/tickets/Tickets$MergeResults;-><init>(Ljava/lang/String;IZ)V

    return-object v2
.end method

.method private ticketIsLocked(Lcom/squareup/tickets/OpenTicket;)Z
    .locals 0

    .line 839
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private ticketIsLocked(Ljava/lang/String;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 846
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 844
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v0, "ticketId cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public addEmptyTicketAndLock(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;
    .locals 0

    .line 200
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 202
    invoke-static/range {p1 .. p6}, Lcom/squareup/tickets/OpenTicket;->createTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    .line 204
    iget-object p2, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance p3, Lcom/squareup/tickets/-$$Lambda$RealTickets$nC9gufxxUwxFixR2DNP1gIHqCIo;

    invoke-direct {p3, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$nC9gufxxUwxFixR2DNP1gIHqCIo;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {p2, p3}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-object p1
.end method

.method public addTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 1

    .line 236
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 237
    invoke-static {p1, p2}, Lcom/squareup/tickets/OpenTicket;->createTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    .line 239
    iget-object p2, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v0, Lcom/squareup/tickets/-$$Lambda$RealTickets$KKHoYdDxu_D9mst-htLpYrcDyYg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$KKHoYdDxu_D9mst-htLpYrcDyYg;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {p2, v0}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public addTicketAndLock(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;
    .locals 1

    .line 219
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 220
    invoke-static {p1, p2}, Lcom/squareup/tickets/OpenTicket;->createTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    .line 222
    iget-object p2, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v0, Lcom/squareup/tickets/-$$Lambda$RealTickets$nc5aX25xU74IO-wl9-zpk24uPPk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$nc5aX25xU74IO-wl9-zpk24uPPk;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {p2, v0}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-object p1
.end method

.method public additiveMergeToTicket(Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    .line 437
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 438
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/tickets/-$$Lambda$RealTickets$eZNHPF33KXeS9ewD7zYv-L7MOzE;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;)V

    invoke-interface {v0, v1, p4}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public close()V
    .locals 1

    .line 793
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0}, Lcom/squareup/tickets/TicketStore;->close()V

    return-void
.end method

.method public closeTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V
    .locals 2

    .line 289
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 290
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$sx1k-OOorpPdxLomxIh4oFSQBpM;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/tickets/-$$Lambda$RealTickets$sx1k-OOorpPdxLomxIh4oFSQBpM;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public compTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$CompHandler;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/Tickets$CompHandler;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$CompResults;",
            ">;)V"
        }
    .end annotation

    .line 364
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$xaGFHjyU3ERMRGOkiWMGvZI6Wk8;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/tickets/-$$Lambda$RealTickets$xaGFHjyU3ERMRGOkiWMGvZI6Wk8;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;Lcom/squareup/tickets/Tickets$CompHandler;)V

    invoke-interface {v0, v1, p3}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public debugCloseAllTickets()V
    .locals 2

    .line 797
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$wWdtEeD_d2ggYq7T-jl5UYWMHDs;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/-$$Lambda$RealTickets$wWdtEeD_d2ggYq7T-jl5UYWMHDs;-><init>(Lcom/squareup/tickets/RealTickets;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public deleteExpiredClosedTickets()V
    .locals 2

    .line 715
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$hL-V06Uau3kgl1lGKLBRI5ZN9pY;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/-$$Lambda$RealTickets$hL-V06Uau3kgl1lGKLBRI5ZN9pY;-><init>(Lcom/squareup/tickets/RealTickets;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public deleteTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V
    .locals 2

    .line 302
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 303
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$3os09HFl5V4Mgku5sUW6m7EdSek;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$3os09HFl5V4Mgku5sUW6m7EdSek;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public deleteTickets(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 314
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$9rNcGRqU0NM6CZP2TlZcAY1jzAc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$9rNcGRqU0NM6CZP2TlZcAY1jzAc;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getBroker()Lcom/squareup/tickets/TicketsBroker;
    .locals 1

    .line 528
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    return-object v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 1

    .line 767
    new-instance v0, Lcom/squareup/tickets/RealTickets$1;

    invoke-direct {v0, p0}, Lcom/squareup/tickets/RealTickets$1;-><init>(Lcom/squareup/tickets/RealTickets;)V

    return-object v0
.end method

.method public getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 161
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 162
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v7, Lcom/squareup/tickets/-$$Lambda$RealTickets$e0NhNI02AptnXkbL-mlnK_339kQ;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tickets/-$$Lambda$RealTickets$e0NhNI02AptnXkbL-mlnK_339kQ;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    invoke-interface {v0, v7, p5}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 173
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    if-eqz p1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v8, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    invoke-interface {v0, v8, p6}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void

    .line 175
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "GroupId cannot be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getRemoteUnsyncedFixableTicketCount()J
    .locals 2

    .line 699
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->remoteUnsyncedFixableTickets:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getRemoteUnsyncedUnfixableTicketCount()J
    .locals 2

    .line 703
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->remoteUnsyncedUnfixableTickets:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTicketAndLock(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;)V"
        }
    .end annotation

    .line 186
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 187
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$5fDB-wxb-Ae-MVaCPNXnr5p03rA;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public getTicketCounts(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketCounter;",
            ">;)V"
        }
    .end annotation

    .line 134
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 135
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$ldC6UiKRlyd5ujuh167b-9Sg5LY;

    invoke-direct {v1, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$ldC6UiKRlyd5ujuh167b-9Sg5LY;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 149
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 150
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v8, Lcom/squareup/tickets/-$$Lambda$RealTickets$49Ce2Hvg93-WsU-5t-b4f0dC3WU;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/tickets/-$$Lambda$RealTickets$49Ce2Hvg93-WsU-5t-b4f0dC3WU;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Ljava/util/List;)V

    invoke-interface {v0, v8, p6}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public getUnsyncedTicketCount(Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 760
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    sget-object v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$BqEYRyHQ3T77p-engmCzkakBy1s;->INSTANCE:Lcom/squareup/tickets/-$$Lambda$RealTickets$BqEYRyHQ3T77p-engmCzkakBy1s;

    invoke-interface {v0, v1, p1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public synthetic lambda$addEmptyTicketAndLock$5$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 209
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "[Ticket_Tickets_Add_Empty_Lock] Adding and locking ticket with id %s"

    .line 208
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketStore;->addTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 211
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/tickets/RealTickets;->lock(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$addTicket$7$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 3

    .line 243
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, v0}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 244
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 246
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "[Ticket_Tickets_Add] Adding but not locking ticket with id %s"

    .line 245
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketStore;->addTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {v0, p1}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$addTicketAndLock$6$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 3

    .line 223
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, v0}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 224
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 226
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "[Ticket_Tickets_Add_Lock] Adding and locking ticket with id %s"

    .line 225
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketStore;->addTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 228
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/tickets/RealTickets;->lock(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$additiveMergeToTicket$17$RealTickets(Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$MergeResults;
    .locals 10

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Ticket_Tickets_Additive_Merge"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 440
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v0, v3

    const-string v2, "[%s] Retrieving ticket with id %s to merge in order of size %d"

    .line 439
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 441
    invoke-interface {p4, p1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 445
    iget-object p1, v0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {p1}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    .line 448
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 449
    invoke-virtual {v2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v2

    const/4 v6, 0x0

    .line 450
    iget-object v3, p0, Lcom/squareup/tickets/RealTickets;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 454
    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v7

    .line 455
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->isDiscountApplicationIdEnabled()Z

    move-result v8

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    .line 456
    :cond_0
    invoke-virtual {v2}, Lcom/squareup/permissions/Employee;->toEmployeeProto()Lcom/squareup/protos/client/Employee;

    move-result-object v2

    :goto_0
    move-object v9, v2

    move-object v3, v0

    move-object v4, p2

    move-object v5, p3

    .line 450
    invoke-virtual/range {v3 .. v9}, Lcom/squareup/tickets/OpenTicket;->additiveMergeCartToTicket(Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZZLcom/squareup/protos/client/Employee;)V

    .line 458
    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 459
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    .line 460
    invoke-interface {p4, v0}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 461
    iget-object p2, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {p2, v0}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 462
    iget-object p2, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 463
    invoke-static {p1, v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 462
    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 464
    new-instance p1, Lcom/squareup/tickets/Tickets$MergeResults;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1}, Lcom/squareup/tickets/Tickets$MergeResults;-><init>(Ljava/lang/String;I)V

    return-object p1

    .line 443
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "No ticket found with id "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public synthetic lambda$closeTicketAndUnlock$10$RealTickets(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V
    .locals 3

    .line 291
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/tickets/RealTickets;->enforceTicketIsLocked(Ljava/lang/String;)V

    .line 292
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 293
    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    .line 294
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/tickets/RealTickets;->doCloseTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    .line 295
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/tickets/RealTickets;->unlock(Ljava/lang/String;)V

    .line 296
    iget-object p2, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 p3, 0x2

    new-array p3, p3, [Lcom/squareup/tickets/LocalTicketUpdateEvent;

    invoke-static {v1, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, p3, v2

    .line 297
    invoke-static {v1, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    const/4 v0, 0x1

    aput-object p1, p3, v0

    .line 296
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$compTickets$14$RealTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$CompHandler;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$CompResults;
    .locals 6

    .line 366
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 367
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 368
    invoke-direct {p0, v1}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 371
    :cond_1
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v2, v1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    .line 374
    iget-object v2, v1, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v2}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v2

    if-eqz v1, :cond_0

    .line 375
    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 378
    :cond_2
    invoke-interface {p2, v1}, Lcom/squareup/tickets/Tickets$CompHandler;->compTicket(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    if-nez v3, :cond_3

    .line 381
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-direct {p0, v2, v1}, Lcom/squareup/tickets/RealTickets;->doDeleteTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;)V

    .line 382
    invoke-static {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forDelete(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 385
    :cond_3
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    sget-object v5, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-direct {p0, v4, v1, v3, v5}, Lcom/squareup/tickets/RealTickets;->doCloseTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    .line 386
    invoke-static {v2, v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390
    :cond_4
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, p3}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 391
    new-instance p1, Lcom/squareup/tickets/Tickets$CompResults;

    invoke-direct {p1, v0}, Lcom/squareup/tickets/Tickets$CompResults;-><init>(I)V

    return-object p1
.end method

.method public synthetic lambda$debugCloseAllTickets$26$RealTickets()V
    .locals 6

    .line 798
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    sget-object v2, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    sget-object v5, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 799
    invoke-interface/range {v0 .. v5}, Lcom/squareup/tickets/TicketStore;->getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v0

    .line 801
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    .line 802
    invoke-interface {v1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    .line 805
    invoke-direct {p0, v1}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Lcom/squareup/tickets/OpenTicket;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 808
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 809
    invoke-virtual {v1, v2}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 810
    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v3, 0x0

    .line 811
    invoke-virtual {v1, v3}, Lcom/squareup/tickets/OpenTicket;->setTerminalIdPair(Lcom/squareup/protos/client/IdPair;)V

    .line 812
    sget-object v3, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-virtual {v1, v3, v2}, Lcom/squareup/tickets/OpenTicket;->setCloseReason(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;Ljava/util/Date;)V

    .line 813
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v2, v1}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 814
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {v2, v1}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public synthetic lambda$deleteExpiredClosedTickets$24$RealTickets()V
    .locals 7

    const-string v0, "Error deleting expired closed tickets"

    .line 716
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 720
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v2}, Lcom/squareup/tickets/TicketStore;->getAllDeletableTicketsOldestFirst()Lcom/squareup/util/ReadOnlyCursorList;

    move-result-object v2

    const/4 v3, 0x0

    .line 725
    :try_start_0
    invoke-interface {v2}, Lcom/squareup/util/ReadOnlyCursorList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    .line 726
    invoke-interface {v5}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v5

    .line 728
    invoke-direct {p0, v5}, Lcom/squareup/tickets/RealTickets;->isClosedTicketExpired(Lcom/squareup/tickets/OpenTicket;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 729
    invoke-virtual {v5}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 733
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v6, 0x64

    if-lt v5, v6, :cond_0

    .line 743
    :cond_1
    :try_start_1
    invoke-interface {v2}, Lcom/squareup/util/ReadOnlyCursorList;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 753
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const-string v2, "[Ticket_Delete_Expired_Closed] Deleting %d expired closed tickets"

    .line 752
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 755
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketStore;->deleteTickets(Ljava/util/List;)V

    return-void

    :catch_0
    move-exception v1

    .line 747
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v3, v1

    .line 740
    :try_start_2
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 743
    :goto_0
    :try_start_3
    invoke-interface {v2}, Lcom/squareup/util/ReadOnlyCursorList;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v2

    if-nez v3, :cond_2

    .line 747
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 750
    :cond_2
    :goto_1
    throw v1
.end method

.method public synthetic lambda$deleteTicketAndUnlock$11$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 4

    .line 304
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/tickets/RealTickets;->enforceTicketIsLocked(Ljava/lang/String;)V

    .line 305
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 306
    invoke-direct {p0, v0, p1}, Lcom/squareup/tickets/RealTickets;->doDeleteTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;)V

    .line 307
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/tickets/RealTickets;->unlock(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/tickets/LocalTicketUpdateEvent;

    invoke-static {p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forDelete(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x0

    .line 309
    invoke-static {p1, v2}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 308
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$deleteTickets$12$RealTickets(Ljava/util/List;)V
    .locals 4

    .line 315
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 316
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 317
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 318
    invoke-direct {p0, v2}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 321
    :cond_0
    iget-object v3, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v3, v2}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v2

    .line 322
    invoke-direct {p0, v0, v2}, Lcom/squareup/tickets/RealTickets;->doDeleteTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;)V

    .line 323
    invoke-static {v2}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forDelete(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 325
    :cond_1
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$getCustomTicketList$2$RealTickets(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 163
    invoke-direct/range {v0 .. v6}, Lcom/squareup/tickets/RealTickets;->logTicketListRetrieval(Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    .line 164
    invoke-interface {p5, p1, p2, p3, p4}, Lcom/squareup/tickets/TicketStore;->getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getGroupTicketList$3$RealTickets(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 7

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 178
    invoke-direct/range {v0 .. v6}, Lcom/squareup/tickets/RealTickets;->logTicketListRetrieval(Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    move-object v0, p6

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 179
    invoke-interface/range {v0 .. v5}, Lcom/squareup/tickets/TicketStore;->getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getTicketAndLock$4$RealTickets(Ljava/lang/String;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/OpenTicket;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "[Ticket_Tickets_Get_Lock] Retrieving and locking ticket with id %s "

    .line 188
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/RealTickets;->lock(Ljava/lang/String;)V

    .line 190
    invoke-interface {p2, p1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p2

    if-eqz p2, :cond_0

    return-object p2

    .line 192
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No ticket found with id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public synthetic lambda$getTicketList$1$RealTickets(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Ljava/util/List;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 151
    invoke-direct/range {v0 .. v6}, Lcom/squareup/tickets/RealTickets;->logTicketListRetrieval(Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    move-object v0, p6

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 152
    invoke-interface/range {v0 .. v5}, Lcom/squareup/tickets/TicketStore;->getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$mergeTicketsToExisting$15$RealTickets(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$MergeResults;
    .locals 10

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 408
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "[Ticket_Tickets_Merge_To_Existing] Merging %d tickets into ticket with id %s"

    .line 407
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410
    invoke-interface {p5, p2}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v4

    if-eqz v4, :cond_0

    const/4 v8, 0x0

    .line 416
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v9

    move-object v2, p0

    move-object v3, p1

    move-object v5, p5

    move-object v6, p3

    move-object v7, p4

    .line 415
    invoke-direct/range {v2 .. v9}, Lcom/squareup/tickets/RealTickets;->doMergeTickets(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/TicketStore;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZ)Lcom/squareup/tickets/Tickets$MergeResults;

    move-result-object p1

    return-object p1

    .line 412
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "No destination ticket found with id "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$mergeTicketsToNew$16$RealTickets(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$MergeResults;
    .locals 10

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 429
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "[Ticket_Tickets_Merge_To_New] Merging %d tickets into ticket with id %s"

    .line 428
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-object v6, p3

    move-object v7, p4

    .line 430
    invoke-direct/range {v2 .. v9}, Lcom/squareup/tickets/RealTickets;->doMergeTickets(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/TicketStore;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZ)Lcom/squareup/tickets/Tickets$MergeResults;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$processListResponse$22$RealTickets(Ljava/util/List;Lcom/squareup/tickets/TicketStore;)Ljava/lang/Boolean;
    .locals 10

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 551
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[Ticket_Tickets_ListResponse] Attempting to update %d tickets\u2026"

    .line 550
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 555
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tickets/OpenTicket;

    .line 557
    invoke-direct {p0, v4}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Lcom/squareup/tickets/OpenTicket;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-array v5, v0, [Ljava/lang/Object;

    .line 559
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v3

    const-string v4, "[Ticket_Tickets_ListResponse] Ticket %s is locked, skipping update."

    .line 558
    invoke-static {v4, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 564
    :cond_0
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v5

    const/4 v6, 0x2

    if-nez v5, :cond_1

    .line 569
    invoke-interface {p2, v4}, Lcom/squareup/tickets/TicketStore;->addTicket(Lcom/squareup/tickets/OpenTicket;)V

    or-int/lit8 v2, v2, 0x1

    new-array v5, v6, [Ljava/lang/Object;

    const-string v6, "Ticket_Tickets_ListResponse"

    aput-object v6, v5, v3

    .line 573
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const-string v6, "[%s] Ticket %s not found on device, so adding as new."

    .line 572
    invoke-static {v6, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575
    invoke-static {v4}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 579
    :cond_1
    invoke-virtual {v5}, Lcom/squareup/tickets/OpenTicket;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v7

    .line 580
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v8

    .line 579
    invoke-static {v7, v8}, Lcom/squareup/tickets/VectorClocks;->getSyncActionForListRepsonse(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/tickets/VectorClocks$SyncAction;

    move-result-object v7

    .line 581
    sget-object v8, Lcom/squareup/tickets/RealTickets$2;->$SwitchMap$com$squareup$tickets$VectorClocks$SyncAction:[I

    invoke-virtual {v7}, Lcom/squareup/tickets/VectorClocks$SyncAction;->ordinal()I

    move-result v9

    aget v8, v8, v9

    if-eq v8, v0, :cond_4

    if-eq v8, v6, :cond_3

    const/4 v6, 0x3

    if-ne v8, v6, :cond_2

    new-array v6, v0, [Ljava/lang/Object;

    .line 588
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    const-string v4, "[Ticket_Tickets_ListResponse] Result: REJECT_CHANGE_UPDATE_SERVER | About to post update to broker for ticket %s"

    .line 586
    invoke-static {v4, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 589
    invoke-virtual {p0}, Lcom/squareup/tickets/RealTickets;->getBroker()Lcom/squareup/tickets/TicketsBroker;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    goto :goto_0

    .line 613
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot handle unknown SyncAction "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-array v5, v0, [Ljava/lang/Object;

    .line 609
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v3

    const-string v4, "[Ticket_Tickets_ListResponse] Result: NO_OP | No-op for ticket %s"

    .line 608
    invoke-static {v4, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 597
    :cond_4
    invoke-interface {p2, v4}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    new-array v6, v0, [Ljava/lang/Object;

    .line 600
    invoke-virtual {v4}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const-string v7, "[Ticket_Tickets_ListResponse] Result: ACCEPT_CHANGE | Updated ticket %s into store."

    .line 598
    invoke-static {v7, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    or-int/lit8 v2, v2, 0x1

    .line 603
    invoke-static {v5, v4}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 616
    :cond_5
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 617
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$processUpdateResponse$23$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 7

    .line 624
    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Lcom/squareup/tickets/OpenTicket;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Object;

    .line 626
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "[Ticket_Tickets_UpdateResponse] Ticket %s is locked, skipping update."

    .line 625
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    const/4 v3, 0x2

    if-nez v0, :cond_1

    .line 636
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketStore;->addTicket(Lcom/squareup/tickets/OpenTicket;)V

    new-array v0, v3, [Ljava/lang/Object;

    const-string v3, "Ticket_Tickets_UpdateResponse"

    aput-object v3, v0, v1

    .line 639
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "[%s] Clocks are equal; updated ticket %s into store."

    .line 638
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 641
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void

    .line 645
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v4

    .line 646
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v5

    .line 648
    invoke-static {v5, v4}, Lcom/squareup/tickets/VectorClocks;->getSyncActionForUpdateRepsonse(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/tickets/VectorClocks$SyncAction;

    move-result-object v4

    .line 649
    sget-object v5, Lcom/squareup/tickets/RealTickets$2;->$SwitchMap$com$squareup$tickets$VectorClocks$SyncAction:[I

    invoke-virtual {v4}, Lcom/squareup/tickets/VectorClocks$SyncAction;->ordinal()I

    move-result v6

    aget v5, v5, v6

    if-eq v5, v2, :cond_3

    if-eq v5, v3, :cond_4

    const/4 p1, 0x3

    if-eq v5, p1, :cond_2

    .line 685
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot handle unknown SyncAction "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 681
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should never REJECT_CHANGE_UPDATE_SERVER from an update response!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 654
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->resetClientClock()V

    .line 657
    iget-object v3, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v3, p1}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    new-array v2, v2, [Ljava/lang/Object;

    .line 661
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "[Ticket_Tickets_UpdateResponse] Result: ACCEPT_CHANGE | Clocks are equal; updated ticket %s into store."

    .line 659
    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 663
    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 664
    invoke-static {v0, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    .line 663
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public synthetic lambda$transferOwnership$18$RealTickets(Lcom/squareup/permissions/EmployeeInfo;Ljava/util/List;)V
    .locals 9

    .line 471
    new-instance v0, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    .line 472
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    .line 473
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    .line 474
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    .line 475
    invoke-virtual {v0}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    .line 476
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 477
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 478
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 479
    invoke-direct {p0, v3}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v5, :cond_0

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v3, v5, v7

    const-string v3, "[Ticket_Tickets_Transfer] Skipping transfer for locked ticket with id %s"

    .line 480
    invoke-static {v3, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 484
    :cond_0
    iget-object v5, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v5, v3}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v3

    .line 485
    iget-object v5, v3, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v5}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v5

    .line 486
    invoke-virtual {v3, v0, v1}, Lcom/squareup/tickets/OpenTicket;->setEmployee(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V

    .line 487
    invoke-virtual {v3}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    .line 488
    iget-object v8, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v8, v3}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 489
    iget-object v8, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {v8, v3}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 490
    invoke-static {v5, v3}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 491
    invoke-static {v5, v3}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forTransfer(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    .line 492
    iget-object v5, p1, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    aput-object v5, v3, v7

    iget-object v5, p1, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    aput-object v5, v3, v6

    iget-object v5, p1, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    aput-object v5, v3, v4

    const-string v5, "[Ticket_Tickets_Transfer] Ticket transferred to: [%s] %s %s"

    invoke-static {v5, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 495
    :cond_1
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$updateTerminalId$19$RealTickets(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V
    .locals 5

    .line 501
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 510
    iget-object p1, p2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string p1, "Ticket deleted before non-cash offline payment was uploaded."

    invoke-static {v1, p1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void

    .line 514
    :cond_1
    iget-object v3, v0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v3}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v3

    .line 515
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v4}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 516
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    .line 517
    invoke-virtual {v0, p2}, Lcom/squareup/tickets/OpenTicket;->setTerminalIdPair(Lcom/squareup/protos/client/IdPair;)V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 519
    invoke-virtual {p2}, Lcom/squareup/protos/client/IdPair;->toString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v4, v2

    aput-object p1, v4, v1

    const-string p1, "[Ticket_Tickets_Terminal] Setting terminal id %s to ticket with id %s"

    .line 518
    invoke-static {p1, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 520
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {p1, v0}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 521
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {p1, v0}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 522
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 523
    invoke-static {v3, v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p2

    .line 522
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$updateTicketAndMaintainLock$8$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 3

    .line 256
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/tickets/RealTickets;->enforceTicketIsLocked(Ljava/lang/String;)V

    .line 257
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, v0}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 258
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 260
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "[Ticket_Tickets_Update_Locked] Updating locked ticket with id %s"

    .line 259
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    .line 262
    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v1, p1}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 263
    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {v1, p1}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 264
    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 265
    invoke-static {v0, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 264
    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$updateTicketAndUnlock$9$RealTickets(Lcom/squareup/tickets/OpenTicket;)V
    .locals 6

    .line 272
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/tickets/RealTickets;->enforceTicketIsLocked(Ljava/lang/String;)V

    .line 273
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, v0}, Lcom/squareup/tickets/OpenTicket;->setClientUpdatedAt(Ljava/util/Date;)V

    .line 274
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->incrementClientClock()J

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 276
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[Ticket_Tickets_Update_Unlock] Updating and unlocking ticket with id %s"

    .line 275
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    .line 278
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v2, p1}, Lcom/squareup/tickets/TicketStore;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 279
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/tickets/RealTickets;->unlock(Ljava/lang/String;)V

    .line 280
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->broker:Lcom/squareup/tickets/TicketsBroker;

    invoke-virtual {v2, p1}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 281
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/squareup/tickets/LocalTicketUpdateEvent;

    invoke-static {v1, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v5

    aput-object v5, v4, v3

    .line 282
    invoke-static {v1, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object p1

    aput-object p1, v4, v0

    .line 281
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$voidTickets$13$RealTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$VoidHandler;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$VoidResults;
    .locals 6

    .line 333
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 334
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 335
    invoke-direct {p0, v1}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 338
    :cond_1
    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->ticketStore:Lcom/squareup/tickets/TicketStore;

    invoke-interface {v2, v1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    .line 341
    iget-object v2, v1, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v2}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v2

    if-eqz v1, :cond_0

    .line 342
    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 345
    :cond_2
    invoke-interface {p2, v1}, Lcom/squareup/tickets/Tickets$VoidHandler;->voidTicket(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    if-nez v3, :cond_3

    .line 348
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-direct {p0, v2, v1}, Lcom/squareup/tickets/RealTickets;->doDeleteTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;)V

    .line 349
    invoke-static {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forDelete(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 352
    :cond_3
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    sget-object v5, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-direct {p0, v4, v1, v3, v5}, Lcom/squareup/tickets/RealTickets;->doCloseTicket(Ljava/util/Date;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    .line 353
    invoke-static {v2, v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 357
    :cond_4
    iget-object p1, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, p3}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 358
    new-instance p1, Lcom/squareup/tickets/Tickets$VoidResults;

    invoke-direct {p1, v0}, Lcom/squareup/tickets/Tickets$VoidResults;-><init>(I)V

    return-object p1
.end method

.method lock(Ljava/lang/String;)V
    .locals 3

    .line 830
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/tickets/RealTickets;->NO_LOCK:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "[Ticket_Tickets_Lock] Locking ticket with id %s"

    .line 834
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 835
    iput-object p1, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    return-void

    .line 831
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Must release previous lock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " before setting new lock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "!"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public mergeTicketsToExisting(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    .line 398
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 399
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 402
    invoke-direct {p0, p2}, Lcom/squareup/tickets/RealTickets;->ticketIsLocked(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v7, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;)V

    invoke-interface {v0, v7, p5}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void

    .line 403
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot merge into a locked ticket"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 400
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "List of ticket ids for merge set cannot be empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public mergeTicketsToNew(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    .line 422
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 423
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v7, Lcom/squareup/tickets/-$$Lambda$RealTickets$6-zXUQ3sGczxEwlJsRRPVeeDUzU;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tickets/-$$Lambda$RealTickets$6-zXUQ3sGczxEwlJsRRPVeeDUzU;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;)V

    invoke-interface {v0, v7, p5}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void

    .line 424
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "List of ticket ids for move set cannot be empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onLocalTicketsUpdated()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/LocalTicketUpdateEvent;",
            ">;>;"
        }
    .end annotation

    .line 693
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->onLocalTicketUpdated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 694
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->onBackpressureBuffer()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tickets/RealTickets;->mainScheduler:Lrx/Scheduler;

    .line 695
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public processListResponse(Ljava/util/List;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 549
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$dIpHRFjr3E49kOGU766oDIDj1wk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$dIpHRFjr3E49kOGU766oDIDj1wk;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;)V

    invoke-interface {v0, v1, p2}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public processUpdateResponse(Lcom/squareup/tickets/OpenTicket;)V
    .locals 2

    .line 622
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$L9U3AGH7VJ6gqmZE-ymYC5gqZpU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$L9U3AGH7VJ6gqmZE-ymYC5gqZpU;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public retrieveNonzeroClientClockTickets(Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 541
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    sget-object v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;->INSTANCE:Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;

    invoke-interface {v0, v1, p1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public retrieveTicketInfo(Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;>;)V"
        }
    .end annotation

    .line 533
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    sget-object v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$a6OIE4brg0XnmEzR-TaY7EwFWT4;->INSTANCE:Lcom/squareup/tickets/-$$Lambda$RealTickets$a6OIE4brg0XnmEzR-TaY7EwFWT4;

    invoke-interface {v0, v1, p1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public setRemoteUnsyncedFixableTicketCount(J)V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->remoteUnsyncedFixableTickets:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setRemoteUnsyncedUnfixableTicketCount(J)V
    .locals 1

    .line 711
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->remoteUnsyncedUnfixableTickets:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public transferOwnership(Ljava/util/List;Lcom/squareup/permissions/EmployeeInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/permissions/EmployeeInfo;",
            ")V"
        }
    .end annotation

    .line 470
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$lbNZO1teIpOsqYumLJ3RWEvuTts;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$lbNZO1teIpOsqYumLJ3RWEvuTts;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public unlock(Ljava/lang/String;)V
    .locals 2

    .line 820
    invoke-direct {p0, p1}, Lcom/squareup/tickets/RealTickets;->enforceTicketIsLocked(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "[Ticket_Tickets_Unlock] Unlocking ticket with id %s"

    .line 821
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 822
    sget-object p1, Lcom/squareup/tickets/RealTickets;->NO_LOCK:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/tickets/RealTickets;->lockedId:Ljava/lang/String;

    return-void
.end method

.method public updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V
    .locals 2

    .line 500
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$uYGrSPvz-h0M3BtlVVi6zKQuOD4;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/tickets/-$$Lambda$RealTickets$uYGrSPvz-h0M3BtlVVi6zKQuOD4;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public updateTicketAndMaintainLock(Lcom/squareup/tickets/OpenTicket;)V
    .locals 2

    .line 254
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 255
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$680L8cQMBfrmR6Lnv8yoALFkNbk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$680L8cQMBfrmR6Lnv8yoALFkNbk;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public updateTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V
    .locals 2

    .line 270
    invoke-direct {p0}, Lcom/squareup/tickets/RealTickets;->enforceOpenTicketsEnabled()V

    .line 271
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$1Z81caOCn_5xbm9FzP9CuNWUcO8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tickets/-$$Lambda$RealTickets$1Z81caOCn_5xbm9FzP9CuNWUcO8;-><init>(Lcom/squareup/tickets/RealTickets;Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public voidTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$VoidHandler;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/Tickets$VoidHandler;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$VoidResults;",
            ">;)V"
        }
    .end annotation

    .line 331
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets;->executor:Lcom/squareup/tickets/TicketsExecutor;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$RealTickets$RWQ8rH7baCSkRLCpAy6TxqOy-5k;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/tickets/-$$Lambda$RealTickets$RWQ8rH7baCSkRLCpAy6TxqOy-5k;-><init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;Lcom/squareup/tickets/Tickets$VoidHandler;)V

    invoke-interface {v0, v1, p3}, Lcom/squareup/tickets/TicketsExecutor;->execute(Lcom/squareup/tickets/TicketsTask;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method
