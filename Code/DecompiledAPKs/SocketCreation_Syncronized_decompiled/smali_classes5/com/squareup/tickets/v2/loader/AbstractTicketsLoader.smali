.class public abstract Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.super Ljava/lang/Object;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CustomTicketsSyncListener;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$AllTicketsSyncListener;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;,
        Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;
    }
.end annotation


# instance fields
.field private connectivityDisposable:Lio/reactivex/disposables/Disposable;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final readOnlyTicketCursor:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;"
        }
    .end annotation
.end field

.field private ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

.field private ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

.field private ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

.field private final ticketSyncCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

.field private final ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    .line 51
    iput-object p2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    .line 52
    iput-object p3, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    .line 53
    iput-object p4, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 54
    new-instance p1, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSyncCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;

    .line 55
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->readOnlyTicketCursor:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$402(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;)Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    return-object p1
.end method

.method static synthetic access$500(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Z
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasTicketCursor()Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/log/tickets/OpenTicketsLogger;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSyncCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/Tickets;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    return-object p0
.end method

.method private closeTicketCursor()V
    .locals 3

    .line 187
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "[Tickets_Loader] Exception closing ticket cursor."

    .line 191
    invoke-static {v0, v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    .line 193
    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    :cond_0
    return-void
.end method

.method private hasTicketCursor()Z
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private refreshTicketCursorSyncStatus()V
    .locals 6

    .line 198
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    if-nez v0, :cond_0

    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    invoke-interface {v0}, Lcom/squareup/tickets/Tickets;->getRemoteUnsyncedFixableTicketCount()J

    move-result-wide v0

    .line 203
    iget-object v2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    invoke-interface {v2}, Lcom/squareup/tickets/Tickets;->getRemoteUnsyncedUnfixableTicketCount()J

    move-result-wide v2

    .line 205
    iget-object v4, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    iget-object v5, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v5}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Lcom/squareup/tickets/TicketRowCursorList;->setOffline(Z)V

    .line 206
    iget-object v4, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v4, v0, v1}, Lcom/squareup/tickets/TicketRowCursorList;->setUnsyncedFixableTicketCount(J)V

    .line 207
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/tickets/TicketRowCursorList;->setUnsyncedUnfixableTicketCount(J)V

    return-void
.end method

.method private refreshTicketSync(Lcom/squareup/ui/ticket/TicketsListSchedulerListener;)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    invoke-interface {v0}, Lcom/squareup/opentickets/TicketsListScheduler;->stopSyncing()V

    .line 212
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    invoke-interface {v0, p1}, Lcom/squareup/opentickets/TicketsListScheduler;->setCallback(Lcom/squareup/ui/ticket/TicketsListSchedulerListener;)V

    .line 213
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    invoke-interface {p1}, Lcom/squareup/opentickets/TicketsListScheduler;->startPeriodicSync()V

    return-void
.end method


# virtual methods
.method public cancelTicketQueryCallback()V
    .locals 2

    .line 142
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasPendingTicketQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Tickets_Loader] Ticket query callback canceled."

    .line 143
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    invoke-virtual {v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;->cancel()V

    const/4 v0, 0x0

    .line 145
    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    :cond_0
    return-void
.end method

.method public cancelTicketSearchCallback()V
    .locals 2

    .line 154
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasPendingTicketSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Tickets_Loader] Ticket search callback canceled."

    .line 155
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    invoke-virtual {v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->cancel()V

    const/4 v0, 0x0

    .line 157
    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    :cond_0
    return-void
.end method

.method public getSearchFilter()Ljava/lang/String;
    .locals 1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasPendingTicketSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    invoke-virtual {v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->getSearchFilter()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasPendingTicketQuery()Z
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasPendingTicketSearch()Z
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$start$0$AbstractTicketsLoader(Lcom/squareup/connectivity/InternetState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasTicketCursor()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 65
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;Z)V

    :cond_0
    return-void
.end method

.method public loadAllTickets(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")V"
        }
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->cancelTicketQueryCallback()V

    .line 89
    new-instance v0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    .line 90
    iget-object v2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v8, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v2 .. v8}, Lcom/squareup/tickets/Tickets;->getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public loadCustomTickets(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 8

    .line 96
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->cancelTicketQueryCallback()V

    .line 97
    new-instance v0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    .line 98
    iget-object v2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v7, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    const/4 v4, 0x0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    invoke-interface/range {v2 .. v7}, Lcom/squareup/tickets/Tickets;->getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public loadTicketsForGroup(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 9

    .line 104
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->cancelTicketQueryCallback()V

    .line 105
    new-instance v0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    .line 106
    iget-object v2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v8, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketQueryCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v2 .. v8}, Lcom/squareup/tickets/Tickets;->getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public loadTicketsForSearch(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")V"
        }
    .end annotation

    .line 113
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->cancelTicketSearchCallback()V

    .line 114
    new-instance v0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    invoke-direct {v0, p0, p3}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    .line 115
    iget-object v1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v7, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketSearchCallback:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v7}, Lcom/squareup/tickets/Tickets;->getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public readOnlyTicketCursor()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->readOnlyTicketCursor:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;Z)V
    .locals 2

    .line 171
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 173
    :try_start_0
    iget-object p2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {p2}, Lcom/squareup/tickets/TicketRowCursorList;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 175
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_0
    :goto_0
    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 179
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSort()Lcom/squareup/tickets/TicketSort;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v0

    const-string v0, "[Tickets_Loader] Setting ticket cursor with filter \"%s\", sort style %s and size %d"

    .line 178
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    .line 182
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->refreshTicketCursorSyncStatus()V

    .line 183
    iget-object p2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->readOnlyTicketCursor:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method protected start()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 62
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tickets/v2/loader/-$$Lambda$AbstractTicketsLoader$Z-bKcpMxA58sJQFNZDFQpckqOT4;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/v2/loader/-$$Lambda$AbstractTicketsLoader$Z-bKcpMxA58sJQFNZDFQpckqOT4;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->connectivityDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method protected stop()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->connectivityDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 72
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    invoke-interface {v0}, Lcom/squareup/opentickets/TicketsListScheduler;->stopSyncing()V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->cancelTicketQueryCallback()V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->cancelTicketSearchCallback()V

    .line 75
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->closeTicketCursor()V

    return-void
.end method

.method public syncAllTickets(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")V"
        }
    .end annotation

    .line 121
    new-instance v6, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$AllTicketsSyncListener;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$AllTicketsSyncListener;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V

    invoke-direct {p0, v6}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->refreshTicketSync(Lcom/squareup/ui/ticket/TicketsListSchedulerListener;)V

    return-void
.end method

.method public syncCustomTickets(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 7

    .line 127
    new-instance v6, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CustomTicketsSyncListener;

    const/4 v2, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CustomTicketsSyncListener;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V

    invoke-direct {p0, v6}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->refreshTicketSync(Lcom/squareup/ui/ticket/TicketsListSchedulerListener;)V

    return-void
.end method

.method public syncTicketsForGroup(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 8

    .line 133
    new-instance v7, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;

    const/4 v3, 0x0

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V

    invoke-direct {p0, v7}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->refreshTicketSync(Lcom/squareup/ui/ticket/TicketsListSchedulerListener;)V

    return-void
.end method
