.class public abstract Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;
.super Ljava/lang/Object;
.source "TicketFilterType.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;,
        Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;,
        Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;,
        Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$TicketGroup;,
        Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0007\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000c\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;",
        "Landroid/os/Parcelable;",
        "()V",
        "id",
        "",
        "getId",
        "()Ljava/lang/String;",
        "AllTickets",
        "Converter",
        "CustomTickets",
        "SearchByName",
        "TicketGroup",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$TicketGroup;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getId()Ljava/lang/String;
.end method
