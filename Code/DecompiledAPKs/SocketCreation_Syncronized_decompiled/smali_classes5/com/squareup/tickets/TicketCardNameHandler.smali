.class public interface abstract Lcom/squareup/tickets/TicketCardNameHandler;
.super Ljava/lang/Object;
.source "TicketCardNameHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0008\u0010\u0014\u001a\u00020\u0003H&J\u0008\u0010\u0015\u001a\u00020\u0003H&J@\u0010\u0016\u001a\u00020\u000326\u0010\u0017\u001a2\u0012\u0013\u0012\u00110\t\u00a2\u0006\u000c\u0008\u0019\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001a\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0019\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001b\u0012\u0004\u0012\u00020\u00030\u0018H&JH\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u001326\u0010\u0017\u001a2\u0012\u0013\u0012\u00110\t\u00a2\u0006\u000c\u0008\u0019\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001a\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0019\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001b\u0012\u0004\u0012\u00020\u00030\u0018H&\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/tickets/TicketCardNameHandler;",
        "",
        "clearSwipeListeners",
        "",
        "onDipFailure",
        "cardRemovedEarly",
        "",
        "onDipSuccess",
        "name",
        "",
        "onDipTerminated",
        "message",
        "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
        "onReinsertCard",
        "onSwipe",
        "event",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "registerToScope",
        "scope",
        "Lmortar/MortarScope;",
        "registerWithLifeCycle",
        "removeCallback",
        "setCallback",
        "callback",
        "Lkotlin/Function2;",
        "Lkotlin/ParameterName;",
        "formattedName",
        "isSuccessful",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clearSwipeListeners()V
.end method

.method public abstract onDipFailure(Z)V
.end method

.method public abstract onDipSuccess(Ljava/lang/String;)V
.end method

.method public abstract onDipTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
.end method

.method public abstract onReinsertCard()V
.end method

.method public abstract onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract registerToScope(Lmortar/MortarScope;)V
.end method

.method public abstract registerWithLifeCycle()V
.end method

.method public abstract removeCallback()V
.end method

.method public abstract setCallback(Lkotlin/jvm/functions/Function2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setCallback(Lmortar/MortarScope;Lkotlin/jvm/functions/Function2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method
