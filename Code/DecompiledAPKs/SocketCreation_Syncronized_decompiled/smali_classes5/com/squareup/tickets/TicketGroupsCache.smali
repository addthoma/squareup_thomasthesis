.class public Lcom/squareup/tickets/TicketGroupsCache;
.super Ljava/lang/Object;
.source "TicketGroupsCache.java"


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final groupEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final onGroupCountChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->onGroupCountChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 29
    iput-object p1, p0, Lcom/squareup/tickets/TicketGroupsCache;->cogs:Lcom/squareup/cogs/Cogs;

    .line 30
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/TicketGroupsCache;->groupEntries:Ljava/util/List;

    return-void
.end method

.method static synthetic lambda$loadAndPost$0(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 51
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 52
    invoke-interface {p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getGroupCount()I
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->groupEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupEntries()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/tickets/TicketGroupsCache;->groupEntries:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public synthetic lambda$loadAndPost$1$TicketGroupsCache(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 55
    invoke-virtual {p0, p2}, Lcom/squareup/tickets/TicketGroupsCache;->update(Lcom/squareup/shared/catalog/CatalogResult;)V

    if-eqz p1, :cond_0

    .line 57
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method public loadAndPost()V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0, v0}, Lcom/squareup/tickets/TicketGroupsCache;->loadAndPost(Ljava/lang/Runnable;)V

    return-void
.end method

.method public loadAndPost(Ljava/lang/Runnable;)V
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->cogs:Lcom/squareup/cogs/Cogs;

    sget-object v1, Lcom/squareup/tickets/-$$Lambda$TicketGroupsCache$mXX5GqlNyVHZcfzSyai4cL2MboE;->INSTANCE:Lcom/squareup/tickets/-$$Lambda$TicketGroupsCache$mXX5GqlNyVHZcfzSyai4cL2MboE;

    new-instance v2, Lcom/squareup/tickets/-$$Lambda$TicketGroupsCache$gA3shqJAE4mfJNlLaICGSAGLHDU;

    invoke-direct {v2, p0, p1}, Lcom/squareup/tickets/-$$Lambda$TicketGroupsCache$gA3shqJAE4mfJNlLaICGSAGLHDU;-><init>(Lcom/squareup/tickets/TicketGroupsCache;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public onGroupCountChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->onGroupCountChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public update(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ">;)V"
        }
    .end annotation

    .line 63
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-nez p1, :cond_0

    return-void

    .line 68
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->groupEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->groupEntries:Ljava/util/List;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 75
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 77
    iget-object p1, p0, Lcom/squareup/tickets/TicketGroupsCache;->onGroupCountChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache;->groupEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 75
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 76
    throw v0
.end method
