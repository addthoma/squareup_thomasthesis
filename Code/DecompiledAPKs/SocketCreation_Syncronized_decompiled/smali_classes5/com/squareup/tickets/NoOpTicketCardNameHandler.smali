.class public final Lcom/squareup/tickets/NoOpTicketCardNameHandler;
.super Ljava/lang/Object;
.source "NoOpTicketCardNameHandler.kt"

# interfaces
.implements Lcom/squareup/tickets/TicketCardNameHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u0004H\u0016J\u0010\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0004H\u0016J\u0008\u0010\u0016\u001a\u00020\u0004H\u0016J@\u0010\u0017\u001a\u00020\u000426\u0010\u0018\u001a2\u0012\u0013\u0012\u00110\n\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\u001b\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\u001c\u0012\u0004\u0012\u00020\u00040\u0019H\u0016JH\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u001426\u0010\u0018\u001a2\u0012\u0013\u0012\u00110\n\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\u001b\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\u001c\u0012\u0004\u0012\u00020\u00040\u0019H\u0016\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/tickets/NoOpTicketCardNameHandler;",
        "Lcom/squareup/tickets/TicketCardNameHandler;",
        "()V",
        "clearSwipeListeners",
        "",
        "onDipFailure",
        "cardRemovedEarly",
        "",
        "onDipSuccess",
        "name",
        "",
        "onDipTerminated",
        "message",
        "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
        "onReinsertCard",
        "onSwipe",
        "event",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "registerToScope",
        "scope",
        "Lmortar/MortarScope;",
        "registerWithLifeCycle",
        "removeCallback",
        "setCallback",
        "callback",
        "Lkotlin/Function2;",
        "Lkotlin/ParameterName;",
        "formattedName",
        "isSuccessful",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearSwipeListeners()V
    .locals 0

    return-void
.end method

.method public onDipFailure(Z)V
    .locals 0

    return-void
.end method

.method public onDipSuccess(Ljava/lang/String;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onDipTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onReinsertCard()V
    .locals 0

    return-void
.end method

.method public onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public registerToScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public registerWithLifeCycle()V
    .locals 0

    return-void
.end method

.method public removeCallback()V
    .locals 0

    return-void
.end method

.method public setCallback(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setCallback(Lmortar/MortarScope;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "callback"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
