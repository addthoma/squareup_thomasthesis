.class Lcom/squareup/tickets/TicketRowCursor$1;
.super Ljava/lang/Object;
.source "TicketRowCursor.java"

# interfaces
.implements Lcom/squareup/tickets/TicketRowCursorList$TicketRow;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/TicketRowCursor;->getCurrent()Lcom/squareup/tickets/TicketRowCursorList$TicketRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/TicketRowCursor;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/TicketRowCursor;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEmployeeName()Lcom/squareup/resources/TextModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$300(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$300(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketRowCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    new-instance v1, Lcom/squareup/resources/FixedText;

    invoke-direct {v1, v0}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    return-object v1

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursor$1;->getEmployeeToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    new-instance v0, Lcom/squareup/resources/ResourceString;

    invoke-static {}, Lcom/squareup/permissions/Employee;->getUnknownNameRes()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    return-object v0

    .line 81
    :cond_1
    new-instance v0, Lcom/squareup/resources/ResourceString;

    invoke-static {}, Lcom/squareup/permissions/Employee;->getUnassignedNameRes()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    return-object v0
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$200(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$200(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketRowCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$100(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketRowCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$000(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketRowCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOpenTicket()Lcom/squareup/tickets/OpenTicket;
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$600(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketRowCursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/tickets/TicketWrapper;->protoFromByteArray([B)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    return-object v0
.end method

.method public getPriceAmount()J
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v0}, Lcom/squareup/tickets/TicketRowCursor;->access$400(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/TicketRowCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTimeUpdated()Ljava/util/Date;
    .locals 3

    .line 89
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/squareup/tickets/TicketRowCursor$1;->this$0:Lcom/squareup/tickets/TicketRowCursor;

    invoke-static {v1}, Lcom/squareup/tickets/TicketRowCursor;->access$500(Lcom/squareup/tickets/TicketRowCursor;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/tickets/TicketRowCursor;->getLong(I)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method
