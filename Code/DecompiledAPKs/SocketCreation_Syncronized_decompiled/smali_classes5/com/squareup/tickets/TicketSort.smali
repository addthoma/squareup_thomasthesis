.class public final enum Lcom/squareup/tickets/TicketSort;
.super Ljava/lang/Enum;
.source "TicketSort.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tickets/TicketSort;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tickets/TicketSort;

.field public static final enum AMOUNT:Lcom/squareup/tickets/TicketSort;

.field public static final enum EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

.field public static final enum NAME:Lcom/squareup/tickets/TicketSort;

.field public static final enum OLDEST:Lcom/squareup/tickets/TicketSort;

.field public static final enum RECENT:Lcom/squareup/tickets/TicketSort;

.field public static final enum UNORDERED:Lcom/squareup/tickets/TicketSort;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 6
    new-instance v0, Lcom/squareup/tickets/TicketSort;

    const/4 v1, 0x0

    const-string v2, "NAME"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tickets/TicketSort;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    .line 8
    new-instance v0, Lcom/squareup/tickets/TicketSort;

    const/4 v2, 0x1

    const-string v3, "RECENT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tickets/TicketSort;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    .line 10
    new-instance v0, Lcom/squareup/tickets/TicketSort;

    const/4 v3, 0x2

    const-string v4, "OLDEST"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tickets/TicketSort;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketSort;->OLDEST:Lcom/squareup/tickets/TicketSort;

    .line 12
    new-instance v0, Lcom/squareup/tickets/TicketSort;

    const/4 v4, 0x3

    const-string v5, "AMOUNT"

    invoke-direct {v0, v5, v4}, Lcom/squareup/tickets/TicketSort;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketSort;->AMOUNT:Lcom/squareup/tickets/TicketSort;

    .line 14
    new-instance v0, Lcom/squareup/tickets/TicketSort;

    const/4 v5, 0x4

    const-string v6, "EMPLOYEE_NAME"

    invoke-direct {v0, v6, v5}, Lcom/squareup/tickets/TicketSort;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketSort;->EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

    .line 16
    new-instance v0, Lcom/squareup/tickets/TicketSort;

    const/4 v6, 0x5

    const-string v7, "UNORDERED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/tickets/TicketSort;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketSort;->UNORDERED:Lcom/squareup/tickets/TicketSort;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/tickets/TicketSort;

    .line 4
    sget-object v7, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tickets/TicketSort;->OLDEST:Lcom/squareup/tickets/TicketSort;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/tickets/TicketSort;->AMOUNT:Lcom/squareup/tickets/TicketSort;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/tickets/TicketSort;->EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/tickets/TicketSort;->UNORDERED:Lcom/squareup/tickets/TicketSort;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/tickets/TicketSort;->$VALUES:[Lcom/squareup/tickets/TicketSort;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tickets/TicketSort;
    .locals 1

    .line 4
    const-class v0, Lcom/squareup/tickets/TicketSort;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tickets/TicketSort;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tickets/TicketSort;
    .locals 1

    .line 4
    sget-object v0, Lcom/squareup/tickets/TicketSort;->$VALUES:[Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v0}, [Lcom/squareup/tickets/TicketSort;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tickets/TicketSort;

    return-object v0
.end method
