.class public Lcom/squareup/tickets/TicketRowCursor;
.super Lcom/squareup/util/AbstractReadOnlyCursorList;
.source "TicketRowCursor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/util/AbstractReadOnlyCursorList<",
        "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final amountIndex:I

.field private final employeeNameIndex:I

.field private final employeeTokenIndex:I

.field private final idIndex:I

.field private final nameIndex:I

.field private final protoIndex:I

.field private final updatedIndex:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/util/AbstractReadOnlyCursorList;-><init>(Landroid/database/Cursor;)V

    const-string p1, "TICKET_NAME"

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->nameIndex:I

    const-string p1, "TICKET_ID"

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->idIndex:I

    const-string p1, "TICKET_AMOUNT"

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->amountIndex:I

    const-string p1, "TICKET_UPDATED"

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->updatedIndex:I

    const-string p1, "TICKET_EMPLOYEE_TOKEN"

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->employeeTokenIndex:I

    const-string p1, "TICKET_EMPLOYEE_NAME"

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->employeeNameIndex:I

    const-string p1, "DATA"

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/TicketRowCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tickets/TicketRowCursor;->protoIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->nameIndex:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->idIndex:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->employeeTokenIndex:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->employeeNameIndex:I

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->amountIndex:I

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->updatedIndex:I

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/tickets/TicketRowCursor;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/tickets/TicketRowCursor;->protoIndex:I

    return p0
.end method


# virtual methods
.method public getCurrent()Lcom/squareup/tickets/TicketRowCursorList$TicketRow;
    .locals 1

    .line 54
    new-instance v0, Lcom/squareup/tickets/TicketRowCursor$1;

    invoke-direct {v0, p0}, Lcom/squareup/tickets/TicketRowCursor$1;-><init>(Lcom/squareup/tickets/TicketRowCursor;)V

    return-object v0
.end method

.method public bridge synthetic getCurrent()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursor;->getCurrent()Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    move-result-object v0

    return-object v0
.end method
