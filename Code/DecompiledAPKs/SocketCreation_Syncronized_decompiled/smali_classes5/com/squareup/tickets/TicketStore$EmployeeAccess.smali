.class public final enum Lcom/squareup/tickets/TicketStore$EmployeeAccess;
.super Ljava/lang/Enum;
.source "TicketStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/TicketStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EmployeeAccess"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tickets/TicketStore$EmployeeAccess;

.field public static final enum ALL_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

.field public static final enum IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

.field public static final enum ONE_EMPLOYEE:Lcom/squareup/tickets/TicketStore$EmployeeAccess;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    const/4 v1, 0x0

    const-string v2, "IGNORE_EMPLOYEES"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    .line 22
    new-instance v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    const/4 v2, 0x1

    const-string v3, "ONE_EMPLOYEE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ONE_EMPLOYEE:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    .line 23
    new-instance v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    const/4 v3, 0x2

    const-string v4, "ALL_EMPLOYEES"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ALL_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    .line 15
    sget-object v4, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ONE_EMPLOYEE:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ALL_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->$VALUES:[Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tickets/TicketStore$EmployeeAccess;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tickets/TicketStore$EmployeeAccess;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->$VALUES:[Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    invoke-virtual {v0}, [Lcom/squareup/tickets/TicketStore$EmployeeAccess;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    return-object v0
.end method
