.class public Lcom/squareup/tickets/OpenTicket;
.super Lcom/squareup/tickets/TicketWrapper;
.source "OpenTicket.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/OpenTicket$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOpenTicket.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OpenTicket.kt\ncom/squareup/tickets/OpenTicket\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,1009:1\n704#2:1010\n777#2,2:1011\n1099#2,2:1013\n1127#2,4:1015\n704#2:1019\n777#2,2:1020\n1642#2,2:1022\n1642#2,2:1024\n1360#2:1026\n1429#2,3:1027\n747#2:1030\n769#2,2:1031\n1642#2,2:1033\n1360#2:1035\n1429#2,3:1036\n747#2:1039\n769#2,2:1040\n1642#2,2:1042\n1360#2:1044\n1429#2,3:1045\n747#2:1048\n769#2,2:1049\n1642#2,2:1051\n1642#2,2:1053\n1099#2,2:1055\n1127#2,4:1057\n704#2:1061\n777#2,2:1062\n1642#2,2:1064\n704#2:1066\n777#2,2:1067\n1642#2,2:1069\n1099#2,2:1071\n1127#2,4:1073\n1099#2,2:1077\n1127#2,4:1079\n1360#2:1094\n1429#2,3:1095\n1099#2,2:1098\n1127#2,4:1100\n501#3:1083\n486#3,6:1084\n501#3:1104\n486#3,6:1105\n67#4:1090\n92#4,3:1091\n67#4:1111\n92#4,3:1112\n*E\n*S KotlinDebug\n*F\n+ 1 OpenTicket.kt\ncom/squareup/tickets/OpenTicket\n*L\n218#1:1010\n218#1,2:1011\n274#1,2:1013\n274#1,4:1015\n276#1:1019\n276#1,2:1020\n281#1,2:1022\n286#1,2:1024\n340#1:1026\n340#1,3:1027\n343#1:1030\n343#1,2:1031\n344#1,2:1033\n372#1:1035\n372#1,3:1036\n374#1:1039\n374#1,2:1040\n375#1,2:1042\n379#1:1044\n379#1,3:1045\n381#1:1048\n381#1,2:1049\n382#1,2:1051\n411#1,2:1053\n419#1,2:1055\n419#1,4:1057\n425#1:1061\n425#1,2:1062\n426#1,2:1064\n443#1:1066\n443#1,2:1067\n444#1,2:1069\n687#1,2:1071\n687#1,4:1073\n691#1,2:1077\n691#1,4:1079\n715#1:1094\n715#1,3:1095\n718#1,2:1098\n718#1,4:1100\n697#1:1083\n697#1,6:1084\n722#1:1104\n722#1,6:1105\n698#1:1090\n698#1,3:1091\n723#1:1111\n723#1,3:1112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0008\u0005\n\u0002\u0010\u0000\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0016\u0018\u0000 \u0098\u00012\u00020\u0001:\u0002\u0098\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J8\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020A2\u0006\u0010C\u001a\u00020A2\u0008\u0010D\u001a\u0004\u0018\u00010\u000eJ\u0018\u0010E\u001a\n G*\u0004\u0018\u00010F0F2\u0006\u0010H\u001a\u00020IH\u0002J\u0018\u0010J\u001a\n G*\u0004\u0018\u00010K0K2\u0006\u0010H\u001a\u00020IH\u0002J\u0018\u0010L\u001a\n G*\u0004\u0018\u00010M0M2\u0006\u0010N\u001a\u00020MH\u0002J<\u0010O\u001a\u00020M2\u0006\u0010P\u001a\u00020Q2\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020M0S2\u0006\u0010T\u001a\u00020=2\u0006\u0010U\u001a\u00020Q2\u0006\u0010C\u001a\u00020AH\u0002J\u0008\u0010V\u001a\u00020;H\u0002J\u0013\u0010W\u001a\u00020A2\u0008\u0010X\u001a\u0004\u0018\u00010YH\u0096\u0002J\u0010\u0010Z\u001a\u0004\u0018\u00010\u00122\u0006\u0010>\u001a\u00020?J\u0008\u0010[\u001a\u00020\u001aH\u0016J\u0006\u0010\\\u001a\u00020AJ\u0018\u0010]\u001a\u00020Q2\u0006\u0010P\u001a\u00020Q2\u0006\u0010^\u001a\u00020=H\u0002J\u0016\u0010_\u001a\u00020;2\u0006\u0010`\u001a\u00020\u00122\u0006\u0010a\u001a\u00020bJ\u0016\u0010c\u001a\u00020;2\u0006\u0010d\u001a\u00020e2\u0006\u0010f\u001a\u00020gJ\u000e\u0010h\u001a\u00020;2\u0006\u0010i\u001a\u00020jJ\u0016\u0010k\u001a\u00020;2\u0006\u0010l\u001a\u00020m2\u0006\u0010n\u001a\u00020gJ\u0010\u0010o\u001a\u00020;2\u0006\u0010p\u001a\u00020gH\u0002J\u0016\u0010q\u001a\u00020;2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010f\u001a\u00020gJ\u0010\u0010r\u001a\u00020;2\u0006\u0010s\u001a\u00020tH\u0007J\u001c\u0010u\u001a\u00020;2\u0008\u0010v\u001a\u0004\u0018\u00010\u00122\u0008\u0010w\u001a\u0004\u0018\u00010\u0012H\u0007J\u0010\u0010x\u001a\u00020;2\u0008\u0010y\u001a\u0004\u0018\u00010IJ\u001c\u0010z\u001a\u00020;2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0008\u00100\u001a\u0004\u0018\u000101H\u0007J\u000e\u0010{\u001a\u00020;2\u0006\u0010|\u001a\u00020\u0006J\u0016\u0010}\u001a\u00020;2\u0006\u0010T\u001a\u00020=2\u0006\u0010C\u001a\u00020AJ(\u0010~\u001a\u00020M2\u0006\u0010\u007f\u001a\u00020M2\u0006\u0010T\u001a\u00020=2\u0006\u0010P\u001a\u00020Q2\u0006\u0010C\u001a\u00020AH\u0002J>\u0010~\u001a\n G*\u0004\u0018\u00010M0M2\u0006\u0010N\u001a\u00020M2\u0008\u0010\u0080\u0001\u001a\u00030\u0081\u00012\n\u0010\u0082\u0001\u001a\u0005\u0018\u00010\u0083\u00012\u000e\u0010\u0084\u0001\u001a\t\u0012\u0005\u0012\u00030\u0085\u00010,H\u0002J3\u0010\u0086\u0001\u001a\u00020;2\t\u0010\u0087\u0001\u001a\u0004\u0018\u00010\u00122\t\u0010\u0088\u0001\u001a\u0004\u0018\u00010\u00122\t\u0010\u0089\u0001\u001a\u0004\u0018\u00010\u001e2\t\u0010\u008a\u0001\u001a\u0004\u0018\u000101J8\u0010\u008b\u0001\u001a\u0008\u0012\u0004\u0012\u00020F0,2\u0006\u0010P\u001a\u00020Q2\r\u0010\u008c\u0001\u001a\u0008\u0012\u0004\u0012\u00020F0,2\u0008\u0010\u008d\u0001\u001a\u00030\u008e\u00012\u0006\u0010C\u001a\u00020AH\u0002J0\u0010\u008f\u0001\u001a\u0008\u0012\u0004\u0012\u00020K0,2\u0006\u0010P\u001a\u00020Q2\r\u0010\u008c\u0001\u001a\u0008\u0012\u0004\u0012\u00020K0,2\u0008\u0010\u008d\u0001\u001a\u00030\u008e\u0001H\u0002J!\u0010\u0090\u0001\u001a\u000c G*\u0005\u0018\u00010\u0091\u00010\u0091\u0001*\u00030\u0091\u00012\u0007\u0010\u0092\u0001\u001a\u00020=H\u0002J!\u0010\u0093\u0001\u001a\u000c G*\u0005\u0018\u00010\u0094\u00010\u0094\u0001*\u00030\u0094\u00012\u0007\u0010\u0092\u0001\u001a\u00020=H\u0002J\u0015\u0010\u0095\u0001\u001a\u00020;*\u00020=2\u0006\u0010<\u001a\u00020=H\u0002J\u001f\u0010\u0096\u0001\u001a\u00020;*\u00020=2\u0006\u0010<\u001a\u00020=2\u0008\u0010D\u001a\u0004\u0018\u00010\u000eH\u0002J\u0015\u0010\u0097\u0001\u001a\u00020;*\u00020=2\u0006\u0010<\u001a\u00020=H\u0002R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0014R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0014R\u0011\u0010\u0019\u001a\u00020\u001a8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u001e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010 R\u0013\u0010!\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u0014R\u0011\u0010#\u001a\u00020\u001a8F\u00a2\u0006\u0006\u001a\u0004\u0008$\u0010\u001cR(\u0010%\u001a\u0004\u0018\u00010&2\u0008\u0010%\u001a\u0004\u0018\u00010&8F@GX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\'\u0010(\"\u0004\u0008)\u0010*R\u0017\u0010+\u001a\u0008\u0012\u0004\u0012\u00020-0,8F\u00a2\u0006\u0006\u001a\u0004\u0008.\u0010/R\u0013\u00100\u001a\u0004\u0018\u0001018F\u00a2\u0006\u0006\u001a\u0004\u00082\u00103R\u0013\u00104\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\u00085\u0010\u0014R\u0013\u00106\u001a\u0004\u0018\u00010\u00128F\u00a2\u0006\u0006\u001a\u0004\u00087\u0010\u0014R\u0011\u00108\u001a\u00020\u001a8F\u00a2\u0006\u0006\u001a\u0004\u00089\u0010\u001c\u00a8\u0006\u0099\u0001"
    }
    d2 = {
        "Lcom/squareup/tickets/OpenTicket;",
        "Lcom/squareup/tickets/TicketWrapper;",
        "ticketProto",
        "Lcom/squareup/protos/client/tickets/Ticket;",
        "(Lcom/squareup/protos/client/tickets/Ticket;)V",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "getCart",
        "()Lcom/squareup/protos/client/bills/Cart;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "getEmployee",
        "()Lcom/squareup/protos/client/Employee;",
        "employeeFirstName",
        "",
        "getEmployeeFirstName",
        "()Ljava/lang/String;",
        "employeeLastName",
        "getEmployeeLastName",
        "employeeToken",
        "getEmployeeToken",
        "enabledSeatCount",
        "",
        "getEnabledSeatCount",
        "()I",
        "group",
        "Lcom/squareup/api/items/TicketGroup;",
        "getGroup",
        "()Lcom/squareup/api/items/TicketGroup;",
        "groupId",
        "getGroupId",
        "itemsCount",
        "getItemsCount",
        "predefinedTicket",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "getPredefinedTicket",
        "()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "setPredefinedTicket",
        "(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V",
        "productsOpenedOn",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
        "getProductsOpenedOn",
        "()Ljava/util/List;",
        "template",
        "Lcom/squareup/api/items/TicketTemplate;",
        "getTemplate",
        "()Lcom/squareup/api/items/TicketTemplate;",
        "templateId",
        "getTemplateId",
        "templateName",
        "getTemplateName",
        "voidedItemsCount",
        "getVoidedItemsCount",
        "additiveMergeCartToTicket",
        "",
        "incomingOrder",
        "Lcom/squareup/payment/Order;",
        "res",
        "Lcom/squareup/util/Res;",
        "preserveExistingCustomer",
        "",
        "isVoidEnabled",
        "discountApplicationIdEnabled",
        "currentEmployee",
        "buildDeletedDiscountLineItem",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "kotlin.jvm.PlatformType",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "buildDeletedFeeLineItem",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "buildDeletedItemization",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "itemization",
        "buildNewOrUpdatedItemization",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "initialItemizations",
        "",
        "currentState",
        "originalItemForAdjuster",
        "ensureFeatureDetailsExistsAndMigrate",
        "equals",
        "other",
        "",
        "getEmployeeTableName",
        "hashCode",
        "isPredefinedTicket",
        "itemWithDiningOption",
        "currentOrder",
        "saveToBundle",
        "key",
        "bundle",
        "Landroid/os/Bundle;",
        "setBuyerInfo",
        "buyerInfo",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
        "currentTime",
        "Ljava/util/Date;",
        "setClientClockVersion",
        "clientClockSnapshotVersion",
        "",
        "setCloseReason",
        "closeReason",
        "Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;",
        "iso8601Date",
        "setCreatedAt",
        "createdAt",
        "setEmployee",
        "setEmployeeInfo",
        "employeeInfo",
        "Lcom/squareup/permissions/EmployeeInfo;",
        "setNameAndNote",
        "name",
        "note",
        "setTerminalIdPair",
        "terminalIdPair",
        "setTicketGroupAndTemplate",
        "setTotal",
        "amountDue",
        "updateAndSetWriteOnlyDeletes",
        "updateExistingItemization",
        "existingItemization",
        "currentAmounts",
        "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
        "selectedOptions",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
        "events",
        "Lcom/squareup/protos/client/bills/Itemization$Event;",
        "updateFeatureDetails",
        "ticketName",
        "ticketNote",
        "ticketGroup",
        "ticketTemplate",
        "updateItemizationDiscounts",
        "existingLineItems",
        "adjustedItem",
        "Lcom/squareup/calc/AdjustedItem;",
        "updateItemizationFees",
        "copy",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
        "originalOrder",
        "copyAndDisable",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "mergeCourses",
        "mergeSeating",
        "mergeSurcharges",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tickets/OpenTicket$Companion;

.field private static final EMPTY_ITEMIZATIONS_LINE_ITEMS:Lcom/squareup/protos/client/bills/Cart$LineItems;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tickets/OpenTicket$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tickets/OpenTicket$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tickets/OpenTicket;->Companion:Lcom/squareup/tickets/OpenTicket$Companion;

    .line 923
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    .line 924
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object v0

    .line 925
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/OpenTicket;->EMPTY_ITEMIZATIONS_LINE_ITEMS:Lcom/squareup/protos/client/bills/Cart$LineItems;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/tickets/Ticket;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/tickets/TicketWrapper;-><init>(Lcom/squareup/protos/client/tickets/Ticket;)V

    .line 82
    invoke-direct {p0}, Lcom/squareup/tickets/OpenTicket;->ensureFeatureDetailsExistsAndMigrate()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/tickets/Ticket;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/tickets/OpenTicket;-><init>(Lcom/squareup/protos/client/tickets/Ticket;)V

    return-void
.end method

.method public static final synthetic access$getEMPTY_ITEMIZATIONS_LINE_ITEMS$cp()Lcom/squareup/protos/client/bills/Cart$LineItems;
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/tickets/OpenTicket;->EMPTY_ITEMIZATIONS_LINE_ITEMS:Lcom/squareup/protos/client/bills/Cart$LineItems;

    return-object v0
.end method

.method public static final synthetic access$setCreatedAt(Lcom/squareup/tickets/OpenTicket;Ljava/util/Date;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/tickets/OpenTicket;->setCreatedAt(Ljava/util/Date;)V

    return-void
.end method

.method private final buildDeletedDiscountLineItem(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 2

    .line 743
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;-><init>()V

    const/4 v1, 0x1

    .line 744
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 745
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p1

    .line 746
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method

.method private final buildDeletedFeeLineItem(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem;
    .locals 2

    .line 736
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;-><init>()V

    const/4 v1, 0x1

    .line 737
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v0

    .line 738
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object p1

    .line 739
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object p1

    return-object p1
.end method

.method private final buildDeletedItemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Itemization;
    .locals 1

    .line 731
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 732
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    .line 733
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1
.end method

.method private final buildNewOrUpdatedItemization(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Lcom/squareup/payment/Order;Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/protos/client/bills/Itemization;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/checkout/CartItem;",
            "Z)",
            "Lcom/squareup/protos/client/bills/Itemization;"
        }
    .end annotation

    .line 661
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/bills/Itemization;

    if-nez p2, :cond_0

    .line 663
    invoke-virtual {p3, p4}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p2

    .line 664
    invoke-virtual {p1, p2, p5}, Lcom/squareup/checkout/CartItem;->getItemizationProto(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    const-string p2, "item.getItemizationProto\u2026ountApplicationIdEnabled)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 666
    :cond_0
    invoke-direct {p0, p2, p3, p1, p5}, Lcom/squareup/tickets/OpenTicket;->updateExistingItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/payment/Order;Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final copy(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
    .locals 2

    .line 385
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    .line 386
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    .line 387
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object p2

    .line 386
    invoke-direct {p1, v1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->source_ticket_information(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object p1

    .line 390
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    move-result-object p1

    return-object p1
.end method

.method private final copyAndDisable(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;
    .locals 2

    .line 361
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 362
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    .line 363
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_client_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    .line 364
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    .line 365
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    new-instance p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    .line 366
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object p2

    .line 365
    invoke-direct {p1, v1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_ticket_information(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object p1

    .line 369
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object p1

    return-object p1
.end method

.method public static final createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tickets/OpenTicket;->Companion:Lcom/squareup/tickets/OpenTicket$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/tickets/OpenTicket$Companion;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p0

    return-object p0
.end method

.method public static final createTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tickets/OpenTicket;->Companion:Lcom/squareup/tickets/OpenTicket$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/tickets/OpenTicket$Companion;->createTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p0

    return-object p0
.end method

.method public static final createTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tickets/OpenTicket;->Companion:Lcom/squareup/tickets/OpenTicket$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/tickets/OpenTicket$Companion;->createTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p0

    return-object p0
.end method

.method private final ensureFeatureDetailsExistsAndMigrate()V
    .locals 9

    .line 865
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object v0

    .line 866
    :goto_0
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-nez v1, :cond_1

    .line 867
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v0

    .line 868
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v0

    .line 869
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object v0

    .line 873
    :cond_1
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 874
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    .line 875
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    .line 876
    iget-object v4, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v4, v4, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    .line 877
    iget-object v5, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v5, v5, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    .line 881
    check-cast v2, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v2, :cond_3

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v2, 0x1

    :goto_2
    const/4 v8, 0x0

    if-eqz v2, :cond_6

    move-object v2, v4

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v2, 0x1

    :goto_4
    if-nez v2, :cond_6

    .line 882
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    .line 883
    invoke-virtual {v1, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    .line 884
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v1

    .line 885
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 886
    invoke-virtual {v2, v8}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 887
    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    .line 892
    :cond_6
    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_8

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    goto :goto_6

    :cond_8
    :goto_5
    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_b

    move-object v2, v5

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_9

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    const/4 v6, 0x1

    :cond_a
    if-nez v6, :cond_b

    .line 893
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    .line 894
    invoke-virtual {v1, v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    .line 895
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v1

    .line 896
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 897
    invoke-virtual {v2, v8}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 898
    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    .line 902
    :cond_b
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-nez v2, :cond_c

    .line 903
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    .line 904
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v2, v2, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    .line 905
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v1

    .line 908
    :cond_c
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v2}, Lcom/squareup/tickets/VectorClocks;->addClientClockIfMissing(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v2

    .line 909
    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 911
    iget-object v3, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v3, v3, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v3

    .line 913
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v0

    .line 914
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v0

    .line 915
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object v0

    .line 912
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 917
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    .line 910
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 919
    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method private final itemWithDiningOption(Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/Order;)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 633
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->hasDiningOption()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 635
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p2

    sget-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {p2, v0}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 636
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    const-string p2, "item.buildUpon()\n       \u2026IXED))\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method

.method public static final loadFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/squareup/tickets/OpenTicket;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tickets/OpenTicket;->Companion:Lcom/squareup/tickets/OpenTicket$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/tickets/OpenTicket$Companion;->loadFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p0

    return-object p0
.end method

.method private final mergeCourses(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;)V
    .locals 5

    .line 379
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderCoursingHandler;->getCourses()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1044
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1045
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1046
    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 379
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1047
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 380
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderCoursingHandler;->getCourses()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1048
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 1049
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 381
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1050
    :cond_2
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 1051
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 382
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v2

    invoke-direct {p0, v1, p2}, Lcom/squareup/tickets/OpenTicket;->copy(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    move-result-object v1

    const-string v3, "it.copy(incomingOrder)"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/squareup/payment/OrderCoursingHandler;->addCourse(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method private final mergeSeating(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/Employee;)V
    .locals 7

    .line 337
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object p1

    .line 338
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v0

    .line 340
    invoke-virtual {p1}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 1026
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 1027
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1028
    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 340
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1029
    :cond_0
    check-cast v2, Ljava/util/List;

    .line 341
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 342
    invoke-virtual {v0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 1030
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 1031
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 343
    iget-object v6, v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1032
    :cond_2
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 1033
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 345
    invoke-direct {p0, v3, p2}, Lcom/squareup/tickets/OpenTicket;->copyAndDisable(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object v3

    const-string v4, "seat.copyAndDisable(incomingOrder)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Lcom/squareup/payment/OrderSeatingHandler;->addSeat(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)V

    goto :goto_2

    .line 348
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/payment/OrderSeatingHandler;->getLatestCoverCountEvent()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object p2

    .line 349
    invoke-virtual {v0}, Lcom/squareup/payment/OrderSeatingHandler;->getLatestCoverCountEvent()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object v0

    if-nez p2, :cond_4

    if-eqz v0, :cond_7

    :cond_4
    const/4 v2, 0x0

    if-eqz p2, :cond_5

    .line 351
    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_3

    :cond_5
    const/4 p2, 0x0

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_6
    add-int/2addr p2, v2

    .line 354
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->MERGE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 352
    invoke-virtual {p1, p2, v0, p3, v1}, Lcom/squareup/payment/OrderSeatingHandler;->setSeatCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V

    :cond_7
    return-void
.end method

.method private final mergeSurcharges(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;)V
    .locals 4

    .line 372
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v0

    const-string v1, "surcharges"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1035
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1036
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1037
    check-cast v2, Lcom/squareup/checkout/Surcharge;

    .line 372
    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1038
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 372
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 373
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object p2

    const-string v1, "incomingOrder.surcharges"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Iterable;

    .line 1039
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1040
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/Surcharge;

    .line 374
    invoke-virtual {v3}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1041
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1042
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    .line 375
    invoke-virtual {p1, v0}, Lcom/squareup/payment/Order;->addSurcharge(Lcom/squareup/checkout/Surcharge;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method private final setCreatedAt(Ljava/util/Date;)V
    .locals 3

    .line 845
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;-><init>()V

    .line 846
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v0

    .line 847
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object v0

    .line 849
    :goto_0
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v0

    .line 850
    new-instance v1, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object p1

    .line 851
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object p1

    .line 852
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 853
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v1

    .line 854
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v2, v2, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v2

    .line 855
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 856
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    .line 854
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 858
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 853
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 860
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method private final updateExistingItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/payment/Order;Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/protos/client/bills/Itemization;
    .locals 5

    .line 758
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p3, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 759
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    iget-object p3, p3, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Existing itemization event should not be modified"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 766
    :cond_1
    invoke-virtual {p2, p3}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p2

    .line 767
    invoke-virtual {p3, p2, p4}, Lcom/squareup/checkout/CartItem;->getItemizationProto(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v0

    .line 774
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    .line 777
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    :goto_0
    const-string v4, "adjustedItem"

    .line 778
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 775
    invoke-direct {p0, p3, v3, p2}, Lcom/squareup/tickets/OpenTicket;->updateItemizationFees(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/calc/AdjustedItem;)Ljava/util/List;

    move-result-object v3

    .line 782
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 780
    :goto_1
    invoke-direct {p0, p3, v4, p2, p4}, Lcom/squareup/tickets/OpenTicket;->updateItemizationDiscounts(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/calc/AdjustedItem;Z)Ljava/util/List;

    move-result-object p2

    .line 786
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p4

    .line 787
    invoke-virtual {p4, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p4

    .line 788
    invoke-virtual {p4, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p2

    .line 789
    iget-object p4, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz p4, :cond_4

    iget-object p4, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz p4, :cond_4

    iget-object p4, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    goto :goto_2

    :cond_4
    move-object p4, v2

    :goto_2
    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p2

    .line 790
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p2

    if-eqz p2, :cond_5

    move-object v2, p2

    goto :goto_3

    .line 791
    :cond_5
    iget-object p2, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz p2, :cond_6

    iget-object v2, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 797
    :cond_6
    :goto_3
    iget-object p2, v0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    const-string p4, "currentItemization.amounts"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 799
    iget-object p3, p3, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    const-string p4, "item.events"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 795
    invoke-direct {p0, p1, p2, v2, p3}, Lcom/squareup/tickets/OpenTicket;->updateExistingItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/protos/client/bills/Itemization$Amounts;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    const-string/jumbo p2, "updateExistingItemizatio\u2026vents = item.events\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final updateExistingItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/protos/client/bills/Itemization$Amounts;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization;"
        }
    .end annotation

    .line 812
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    .line 813
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p2

    .line 814
    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/bills/Itemization$Builder;->event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p2

    .line 816
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    .line 817
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    .line 818
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p1

    .line 815
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    .line 820
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1
.end method

.method private final updateItemizationDiscounts(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/calc/AdjustedItem;Z)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Lcom/squareup/calc/AdjustedItem;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation

    .line 687
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    const/16 v1, 0xa

    .line 1071
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v2

    .line 1072
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v4, Ljava/util/Map;

    .line 1073
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1074
    move-object v5, v2

    check-cast v5, Lcom/squareup/checkout/Discount;

    .line 687
    iget-object v5, v5, Lcom/squareup/checkout/Discount;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 690
    :cond_0
    check-cast p2, Ljava/lang/Iterable;

    .line 1077
    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    invoke-static {v0, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 1078
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 1079
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1080
    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 691
    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1083
    :cond_1
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p2, Ljava/util/Map;

    .line 1084
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 697
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 1086
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1090
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1091
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 698
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string v2, "it.value.discount_line_item_id_pair"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/tickets/OpenTicket;->buildDeletedDiscountLineItem(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1093
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 701
    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1, p3, p4}, Lcom/squareup/checkout/CartItem;->getDiscountLineItems(Lcom/squareup/calc/AdjustedItem;Z)Ljava/util/List;

    move-result-object p1

    const-string p2, "item.getDiscountLineItem\u2026ountApplicationIdEnabled)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final updateItemizationFees(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/calc/AdjustedItem;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/calc/AdjustedItem;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation

    .line 715
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1094
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1095
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1096
    check-cast v3, Lcom/squareup/checkout/Tax;

    .line 715
    iget-object v3, v3, Lcom/squareup/checkout/Tax;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1097
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 715
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 718
    check-cast p2, Ljava/lang/Iterable;

    .line 1098
    invoke-static {p2, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 1099
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 1100
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1101
    move-object v3, v1

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 718
    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1104
    :cond_1
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p2, Ljava/util/Map;

    .line 1105
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 722
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    .line 1107
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1111
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1112
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 723
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string v2, "it.value.fee_line_item_id_pair"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/tickets/OpenTicket;->buildDeletedFeeLineItem(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1114
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 726
    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1, p3}, Lcom/squareup/checkout/CartItem;->getFeeLineItems(Lcom/squareup/calc/AdjustedItem;)Ljava/util/List;

    move-result-object p1

    const-string p2, "item.getFeeLineItems(adjustedItem)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final additiveMergeCartToTicket(Lcom/squareup/payment/Order;Lcom/squareup/util/Res;ZZZLcom/squareup/protos/client/Employee;)V
    .locals 6

    const-string v0, "incomingOrder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    invoke-static {p1, p0, p2, p4}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromOrder(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object p2

    .line 267
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->popUninterestingItem()Lcom/squareup/checkout/CartItem;

    .line 269
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p4

    .line 270
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 274
    check-cast v0, Ljava/lang/Iterable;

    const/16 v1, 0xa

    .line 1013
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 1014
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 1015
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1016
    move-object v4, v3

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 274
    invoke-virtual {v4}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 276
    :cond_0
    check-cast p4, Ljava/lang/Iterable;

    .line 1019
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1020
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/checkout/Discount;

    .line 276
    invoke-virtual {v5}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1021
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 277
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {p4, v1}, Lkotlin/collections/CollectionsKt;->minus(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p4

    .line 278
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->minus(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 281
    check-cast v0, Ljava/lang/Iterable;

    .line 1022
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 282
    invoke-virtual {p2, v1}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    goto :goto_2

    .line 286
    :cond_3
    check-cast p4, Ljava/lang/Iterable;

    .line 1024
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_3
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Discount;

    .line 287
    invoke-virtual {p1, v0}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    goto :goto_3

    .line 293
    :cond_4
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p4

    .line 294
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    if-nez p4, :cond_5

    if-eqz v0, :cond_5

    .line 296
    invoke-virtual {p2, v0}, Lcom/squareup/payment/Order;->setDiningOption(Lcom/squareup/checkout/DiningOption;)V

    .line 302
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    if-eqz v0, :cond_6

    move-object v3, v0

    goto :goto_5

    :cond_6
    move-object v3, p4

    .line 304
    :goto_5
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->hasDiningOption()Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz v3, :cond_7

    .line 306
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 307
    sget-object v4, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v3, v4}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 308
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 305
    invoke-virtual {p2, v2}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    goto :goto_4

    .line 311
    :cond_7
    invoke-virtual {p2, v2}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    goto :goto_4

    .line 317
    :cond_8
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->hasCustomer()Z

    move-result p4

    if-eqz p4, :cond_9

    if-nez p3, :cond_9

    .line 319
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p3

    .line 320
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p4

    .line 321
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    .line 318
    invoke-virtual {p2, p3, p4, v0}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 325
    :cond_9
    invoke-direct {p0, p2, p1, p6}, Lcom/squareup/tickets/OpenTicket;->mergeSeating(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/Employee;)V

    .line 326
    invoke-direct {p0, p2, p1}, Lcom/squareup/tickets/OpenTicket;->mergeSurcharges(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;)V

    .line 327
    invoke-direct {p0, p2, p1}, Lcom/squareup/tickets/OpenTicket;->mergeCourses(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;)V

    .line 330
    invoke-virtual {p0, p2, p5}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 556
    move-object v0, p0

    check-cast v0, Lcom/squareup/tickets/OpenTicket;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 557
    :cond_0
    instance-of v0, p1, Lcom/squareup/tickets/OpenTicket;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 558
    :cond_1
    check-cast p1, Lcom/squareup/tickets/OpenTicket;

    iget-object p1, p1, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 553
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    const-string/jumbo v1, "ticketProto.cart"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getEmployee()Lcom/squareup/protos/client/Employee;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getEmployeeFirstName()Ljava/lang/String;
    .locals 1

    .line 197
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/Employee;->first_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getEmployeeLastName()Ljava/lang/String;
    .locals 1

    .line 203
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/Employee;->last_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getEmployeeTableName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v1, v0, Lcom/squareup/protos/client/Employee;->first_name:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/Employee;->last_name:Ljava/lang/String;

    .line 209
    invoke-static {p1, v1, v0}, Lcom/squareup/permissions/Employee;->getShortName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getEnabledSeatCount()I
    .locals 5

    .line 218
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->seat:Ljava/util/List;

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Iterable;

    .line 1010
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1011
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 218
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    const-string v4, "it.enabled"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1012
    :cond_1
    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final getGroup()Lcom/squareup/api/items/TicketGroup;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getGroupId()Ljava/lang/String;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getItemsCount()I
    .locals 1

    .line 243
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object v0
.end method

.method public final getProductsOpenedOn()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    const-string/jumbo v1, "ticketProto.cart.feature\u2026ticket.products_opened_on"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getTemplate()Lcom/squareup/api/items/TicketTemplate;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getTemplateId()Ljava/lang/String;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getTemplate()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getTemplateName()Ljava/lang/String;
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getTemplate()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getVoidedItemsCount()I
    .locals 1

    .line 247
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 561
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isPredefinedTicket()Z
    .locals 2

    .line 250
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    :cond_1
    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final saveToBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bundle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 826
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v0}, Lcom/squareup/tickets/TicketWrapper;->byteArrayFromProto(Lcom/squareup/protos/client/tickets/Ticket;)[B

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public final setBuyerInfo(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/Date;)V
    .locals 2

    const-string v0, "buyerInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 228
    new-instance v1, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p2}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p2

    .line 230
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v1

    .line 233
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    .line 231
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 236
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 229
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 238
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setClientClockVersion(J)V
    .locals 1

    .line 525
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {v0}, Lcom/squareup/tickets/VectorClocks;->addClientClockIfMissing(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    .line 526
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tickets/OpenTicket;->setClientClock(J)V

    return-void
.end method

.method public final setCloseReason(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;Ljava/util/Date;)V
    .locals 3

    const-string v0, "closeReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iso8601Date"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 542
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 543
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;-><init>()V

    .line 544
    :goto_0
    new-instance v2, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p2}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v2, p2}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    move-result-object p2

    .line 545
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->close_reason(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    move-result-object p1

    .line 546
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    move-result-object p1

    .line 543
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 548
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setEmployee(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V
    .locals 4

    const-string v0, "employee"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 166
    new-instance v1, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p2}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p2

    .line 168
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v1

    .line 172
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v2, v2, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v2

    .line 174
    new-instance v3, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 175
    invoke-virtual {v3, p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p1

    .line 173
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_owner(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object p1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object p1

    .line 171
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    .line 169
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 167
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 184
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setEmployeeInfo(Lcom/squareup/permissions/EmployeeInfo;)V
    .locals 4

    const-string v0, "employeeInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v0

    .line 597
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v1

    .line 598
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->newBuilder()Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 599
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v2

    .line 600
    iget-object v2, v2, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/protos/client/Employee;->newBuilder()Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    new-instance v2, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    .line 606
    :goto_1
    iget-object v3, p1, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    .line 607
    iget-object v3, p1, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    .line 608
    iget-object p1, p1, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object p1

    .line 609
    invoke-virtual {p1}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object p1

    .line 604
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p1

    .line 611
    invoke-virtual {p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p1

    .line 602
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_owner(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object p1

    .line 613
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object p1

    .line 614
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 616
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v1

    .line 618
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v2, v2, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v2

    .line 619
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 620
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    .line 617
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 622
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 615
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 624
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setNameAndNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 567
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 569
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v1

    .line 571
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v2, v2, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v2

    .line 573
    iget-object v3, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v3, v3, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v3

    .line 574
    invoke-virtual {v3, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object p1

    .line 575
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object p1

    .line 576
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object p1

    .line 572
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 578
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    .line 570
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 580
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 568
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 582
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->predefined_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object p1

    .line 96
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v1

    .line 100
    iget-object v2, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v2, v2, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v2

    .line 101
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    .line 99
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 97
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setTerminalIdPair(Lcom/squareup/protos/client/IdPair;)V
    .locals 2

    .line 530
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;-><init>()V

    .line 532
    :goto_0
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->terminal_event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;

    move-result-object p1

    .line 533
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    move-result-object p1

    .line 531
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 535
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final setTicketGroupAndTemplate(Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 1

    .line 589
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;-><init>()V

    .line 590
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object p1

    .line 591
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object p1

    .line 592
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/tickets/OpenTicket;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    return-void
.end method

.method public final setTotal(Lcom/squareup/protos/common/Money;)V
    .locals 3

    const-string v0, "amountDue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 830
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 831
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 833
    iget-object v1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v1

    .line 835
    new-instance v2, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 836
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 837
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    .line 834
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 839
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 832
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 841
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method public final updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V
    .locals 16

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    const-string v0, "currentState"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 406
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    const/4 v8, 0x0

    if-eqz v0, :cond_0

    .line 405
    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->buildDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v0

    move-object v9, v0

    goto :goto_0

    :cond_0
    move-object v9, v8

    .line 409
    :goto_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    move-object v10, v0

    check-cast v10, Ljava/util/Map;

    .line 410
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    move-object v11, v0

    check-cast v11, Ljava/util/Map;

    .line 411
    iget-object v0, v6, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    const/4 v12, 0x1

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Iterable;

    .line 1053
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Itemization;

    .line 412
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "itemization"

    const-string v4, "itemization.itemization_id_pair.client_id"

    if-eqz v2, :cond_1

    .line 413
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v11, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 415
    :cond_1
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v10, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 421
    :cond_2
    iget-object v0, v6, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/Iterable;

    const/16 v1, 0xa

    .line 1055
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 1056
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 1057
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1058
    move-object v3, v1

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization;

    .line 420
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 421
    :cond_3
    invoke-static {v2}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    :goto_3
    move-object v13, v0

    .line 424
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getDeletedItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "currentState.deletedItems"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1061
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1062
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 425
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v11, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v12

    if-eqz v3, :cond_5

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1063
    :cond_6
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1064
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_5
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/squareup/checkout/CartItem;

    const-string v0, "deletedItem"

    .line 431
    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object v1, v15

    move-object v2, v10

    move-object/from16 v3, p1

    move-object v4, v15

    move/from16 v5, p2

    .line 430
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/OpenTicket;->buildNewOrUpdatedItemization(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Lcom/squareup/payment/Order;Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v0

    .line 437
    iget-object v1, v15, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v2, "deletedItem.idPair.client_id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v6, v0}, Lcom/squareup/tickets/OpenTicket;->buildDeletedItemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v0

    const-string v2, "buildDeletedItemization(itemization)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v11, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 440
    :cond_7
    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v11

    .line 441
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "currentState.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1066
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1067
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 443
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v13, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v12

    if-eqz v3, :cond_8

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1068
    :cond_9
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1069
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_7
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 447
    iget-boolean v0, v4, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    const-string v1, "originalItem"

    if-nez v0, :cond_a

    .line 448
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v6, v4, v7}, Lcom/squareup/tickets/OpenTicket;->itemWithDiningOption(Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/Order;)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    move-object v15, v0

    goto :goto_8

    :cond_a
    move-object v15, v4

    :goto_8
    const-string v0, "item"

    .line 455
    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object v1, v15

    move-object v2, v10

    move-object/from16 v3, p1

    move/from16 v5, p2

    .line 454
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/OpenTicket;->buildNewOrUpdatedItemization(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Lcom/squareup/payment/Order;Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v0

    .line 462
    invoke-virtual {v15}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 463
    iget-object v1, v15, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v13, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 465
    :cond_b
    move-object v1, v11

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 470
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 472
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v12

    if-eqz v1, :cond_d

    .line 474
    new-instance v1, Lcom/squareup/permissions/EmployeeInfo$Builder;

    invoke-direct {v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;-><init>()V

    .line 475
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/permissions/EmployeeInfo$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v1

    .line 476
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getEmployeeFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/permissions/EmployeeInfo$Builder;->firstName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v1

    .line 477
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getEmployeeLastName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/permissions/EmployeeInfo$Builder;->lastName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v1

    .line 478
    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->build()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v1

    const-string v2, "EmployeeInfo.Builder()\n \u2026e)\n              .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 473
    invoke-virtual {v6, v1}, Lcom/squareup/tickets/OpenTicket;->setEmployeeInfo(Lcom/squareup/permissions/EmployeeInfo;)V

    .line 481
    :cond_d
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    .line 482
    invoke-virtual {v1, v9}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->default_dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object v1

    .line 483
    invoke-virtual {v1, v11}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object v1

    .line 484
    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->void_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object v1

    .line 486
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->buildTopLevelSurchargeLineItems()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->surcharge(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    .line 488
    iget-object v2, v6, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 489
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v2

    .line 491
    iget-object v3, v6, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v3, v3, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/Cart;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v3

    .line 492
    iget-object v4, v6, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v4, v4, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v7, v4}, Lcom/squareup/payment/Order;->getFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v3

    .line 493
    invoke-virtual {v7, v0, v8}, Lcom/squareup/payment/Order;->getAmounts(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 494
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items(Lcom/squareup/protos/client/bills/Cart$LineItems;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    .line 490
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 497
    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    iput-object v0, v6, Lcom/squareup/tickets/OpenTicket;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    .line 498
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->ensureFeatureDetailsExistsAndMigrate()V

    .line 499
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lcom/squareup/tickets/OpenTicket;->setNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/squareup/tickets/OpenTicket;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 501
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->assertValidTicket()V

    return-void
.end method

.method public final updateFeatureDetails(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 0

    .line 514
    invoke-direct {p0}, Lcom/squareup/tickets/OpenTicket;->ensureFeatureDetailsExistsAndMigrate()V

    .line 515
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tickets/OpenTicket;->setNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    invoke-virtual {p0, p3, p4}, Lcom/squareup/tickets/OpenTicket;->setTicketGroupAndTemplate(Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    .line 517
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicket;->assertValidTicket()V

    return-void
.end method
