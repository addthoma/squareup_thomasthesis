.class public final Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;
.super Ljava/lang/Object;
.source "RealTicketCardNameHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tickets/RealTicketCardNameHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final toasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->resProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->toasterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;)",
            "Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/tickets/OpenTicketsSettings;)Lcom/squareup/tickets/RealTicketCardNameHandler;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/tickets/RealTicketCardNameHandler;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tickets/RealTicketCardNameHandler;-><init>(Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/tickets/OpenTicketsSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tickets/RealTicketCardNameHandler;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->toasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v2, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v3, p0, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/tickets/OpenTicketsSettings;)Lcom/squareup/tickets/RealTicketCardNameHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/tickets/RealTicketCardNameHandler_Factory;->get()Lcom/squareup/tickets/RealTicketCardNameHandler;

    move-result-object v0

    return-object v0
.end method
