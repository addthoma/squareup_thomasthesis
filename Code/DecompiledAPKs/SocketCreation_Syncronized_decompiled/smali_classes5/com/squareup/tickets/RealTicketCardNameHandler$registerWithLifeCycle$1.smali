.class final Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$1;
.super Ljava/lang/Object;
.source "RealTicketCardNameHandler.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/RealTicketCardNameHandler;->registerWithLifeCycle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/RealTicketCardNameHandler;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/RealTicketCardNameHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$1;->this$0:Lcom/squareup/tickets/RealTicketCardNameHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$1;->this$0:Lcom/squareup/tickets/RealTicketCardNameHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler;->onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    invoke-virtual {p0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler$registerWithLifeCycle$1;->accept(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method
