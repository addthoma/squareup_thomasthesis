.class public abstract Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;
.super Ljava/lang/Object;
.source "NoSeparatedPrintoutsWorkflowModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule$NoOpSeparatedPrintoutsViewFactory;
    }
.end annotation


# static fields
.field private static final NO_VIEW_FACTORY:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

.field private static final NO_WORKFLOW:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 15
    const-class v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    .line 16
    invoke-static {v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    sput-object v0, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->NO_WORKFLOW:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    .line 18
    new-instance v0, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule$NoOpSeparatedPrintoutsViewFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule$NoOpSeparatedPrintoutsViewFactory;-><init>(Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule$1;)V

    sput-object v0, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->NO_VIEW_FACTORY:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideSeparatedPrintoutsLauncher()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 22
    sget-object v0, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->NO_WORKFLOW:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    return-object v0
.end method

.method static provideSeparatedPrintoutsManager()Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 30
    sget-object v0, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;->INSTANCE:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;

    return-object v0
.end method

.method static provideSeparatedPrintoutsViewFactory()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    sget-object v0, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->NO_VIEW_FACTORY:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    return-object v0
.end method


# virtual methods
.method abstract bindSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflow;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
