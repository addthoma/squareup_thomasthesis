.class public Lcom/squareup/ui/crm/rows/ProfileSectionHeader;
.super Landroid/widget/LinearLayout;
.source "ProfileSectionHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;
    }
.end annotation


# instance fields
.field private final actionClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final actionView:Landroid/widget/TextView;

.field private final divider:Landroid/view/View;

.field private final dropDownView:Landroid/view/View;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    sget p2, Lcom/squareup/crm/R$layout;->crm_v2_profile_section_header:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 34
    sget p1, Lcom/squareup/crm/R$id;->crm_profile_section_header_title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->titleView:Landroid/widget/TextView;

    .line 35
    sget p1, Lcom/squareup/crm/R$id;->crm_profile_section_header_action:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionView:Landroid/widget/TextView;

    .line 36
    sget p1, Lcom/squareup/crm/R$id;->crm_profile_section_header_dropdown:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->dropDownView:Landroid/view/View;

    .line 37
    sget p1, Lcom/squareup/crm/R$id;->crm_profile_section_header_divider:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->divider:Landroid/view/View;

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionView:Landroid/widget/TextView;

    .line 40
    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->dropDownView:Landroid/view/View;

    .line 41
    invoke-static {p2}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p2

    .line 39
    invoke-static {p1, p2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionClicked:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public onActionClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V
    .locals 3

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionView:Landroid/widget/TextView;

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    const/4 v1, 0x0

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionView:Landroid/widget/TextView;

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    const/16 v2, 0x8

    if-ne p2, v0, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->dropDownView:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setActionToDropDown()V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->actionView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->dropDownView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setDividerVisible(Z)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->divider:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
