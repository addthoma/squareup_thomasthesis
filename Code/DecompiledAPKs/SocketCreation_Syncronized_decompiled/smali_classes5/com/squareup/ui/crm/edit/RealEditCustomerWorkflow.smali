.class public final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditCustomerWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditCustomerWorkflow.kt\ncom/squareup/ui/crm/edit/RealEditCustomerWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 6 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,432:1\n32#2,12:433\n41#3:445\n56#3,2:446\n41#3:505\n56#3,2:506\n276#4:448\n276#4:508\n149#5,5:449\n149#5,5:478\n149#5,5:495\n149#5,5:500\n149#5,5:513\n149#5,5:518\n1412#6,9:454\n1642#6,2:463\n1421#6:465\n1412#6,9:466\n1642#6,2:475\n1421#6:477\n1412#6,9:483\n1642#6,2:492\n1421#6:494\n1360#6:509\n1429#6,3:510\n704#6:523\n777#6,2:524\n1360#6:526\n1429#6,3:527\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditCustomerWorkflow.kt\ncom/squareup/ui/crm/edit/RealEditCustomerWorkflow\n*L\n104#1,12:433\n115#1:445\n115#1,2:446\n167#1:505\n167#1,2:506\n115#1:448\n167#1:508\n124#1,5:449\n141#1,5:478\n146#1,5:495\n165#1,5:500\n186#1,5:513\n186#1,5:518\n128#1,9:454\n128#1,2:463\n128#1:465\n133#1,9:466\n133#1,2:475\n133#1:477\n144#1,9:483\n144#1,2:492\n144#1:494\n181#1:509\n181#1,3:510\n332#1:523\n332#1,2:524\n333#1:526\n333#1,3:527\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ca\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002BE\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018\u00a2\u0006\u0002\u0010\u001aJ\u0016\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010 \u001a\u00020\u0003H\u0002J\u001a\u0010!\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u00032\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u0018\u0010$\u001a\u00020%2\u0006\u0010 \u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u0004H\u0002J\u001c\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010(\u001a\u00020)H\u0002JR\u0010*\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010 \u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u00042\u0016\u0010+\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050,j\u0002`-H\u0016J\u001c\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010/\u001a\u000200H\u0002J\u001c\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u00102\u001a\u000203H\u0002J*\u00104\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010(\u001a\u00020)2\u000c\u00105\u001a\u0008\u0012\u0004\u0012\u00020706H\u0002J\u0010\u00108\u001a\u00020#2\u0006\u0010&\u001a\u00020\u0004H\u0016J\u001c\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010:\u001a\u00020;H\u0002J$\u0010<\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010/\u001a\u0002002\u0006\u0010=\u001a\u00020>H\u0002J4\u0010?\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010A\u0012\u0006\u0012\u0004\u0018\u00010B0@*\u00020;2\u0016\u0010+\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050,j\u0002`-H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006C"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "editCustomerEnumWorkflow",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
        "chooseGroupsWorkflow",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
        "addressWorkflow",
        "Lcom/squareup/address/workflow/AddressWorkflow;",
        "groupLoader",
        "Lcom/squareup/crm/RolodexGroupLoader;",
        "merchantLoader",
        "Lcom/squareup/crm/RolodexMerchantLoader;",
        "res",
        "Lcom/squareup/util/Res;",
        "countryCodeProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/CountryCode;",
        "(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;Lcom/squareup/address/workflow/AddressWorkflow;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V",
        "closeAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "getTitle",
        "Lcom/squareup/resources/TextModel;",
        "",
        "props",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "isSaveEnabled",
        "",
        "state",
        "loadGroupsAction",
        "groupsAttribute",
        "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/ui/crm/edit/EditCustomerContext;",
        "saveAction",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "showEnumEditorAction",
        "enum",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;",
        "showGroupsEditorAction",
        "groups",
        "",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "snapshotState",
        "updateAttributeAction",
        "attribute",
        "Lcom/squareup/crm/model/ContactAttribute;",
        "updateSchemaAction",
        "schema",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema;",
        "toEditRow",
        "Lkotlin/Pair;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addressWorkflow:Lcom/squareup/address/workflow/AddressWorkflow;

.field private final chooseGroupsWorkflow:Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;

.field private final closeAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final editCustomerEnumWorkflow:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private final merchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;Lcom/squareup/address/workflow/AddressWorkflow;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
            "Lcom/squareup/address/workflow/AddressWorkflow;",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editCustomerEnumWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseGroupsWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addressWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupLoader"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantLoader"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCodeProvider"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->editCustomerEnumWorkflow:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->chooseGroupsWorkflow:Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->addressWorkflow:Lcom/squareup/address/workflow/AddressWorkflow;

    iput-object p4, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    iput-object p5, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->merchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    iput-object p6, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p7, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    .line 428
    sget-object p1, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$closeAction$1;->INSTANCE:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$closeAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x0

    const/4 p3, 0x1

    invoke-static {p0, p2, p1, p3, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->closeAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getCloseAction$p(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    iget-object p0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->closeAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getCountryCodeProvider$p(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;)Ljavax/inject/Provider;
    .locals 0

    .line 85
    iget-object p0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$loadGroupsAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->loadGroupsAction(Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$saveAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->saveAction(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showEnumEditorAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->showEnumEditorAction(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showGroupsEditorAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->showGroupsEditorAction(Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateAttributeAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->updateAttributeAction(Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateSchemaAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->updateSchemaAction(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final getTitle(Lcom/squareup/ui/crm/edit/EditCustomerProps;)Lcom/squareup/resources/TextModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 205
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 207
    new-instance v0, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 206
    :cond_1
    new-instance p1, Lcom/squareup/resources/ResourceString;

    sget v0, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_create_new_customer_title:I

    invoke-direct {p1, v0}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/resources/TextModel;

    :goto_0
    return-object v0
.end method

.method private final isSaveEnabled(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;)Z
    .locals 3

    .line 215
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/EditCustomerState;->getData()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/squareup/crm/util/RolodexAttributeHelper;->withContactAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Collection;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p2

    .line 217
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getContactValidationType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    .line 222
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p1, :cond_6

    :goto_0
    const/4 v0, 0x1

    goto :goto_5

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 221
    :cond_1
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p1, :cond_6

    .line 219
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_3

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x1

    :goto_2
    if-nez p1, :cond_6

    .line 220
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_5

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 p1, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 p1, 0x1

    :goto_4
    if-nez p1, :cond_6

    .line 221
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    goto :goto_0

    :cond_6
    :goto_5
    return v0
.end method

.method private final loadGroupsAction(Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    .line 404
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$loadGroupsAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$loadGroupsAction$1;-><init>(Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final saveAction(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    .line 424
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$saveAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$saveAction$1;-><init>(Lcom/squareup/protos/client/rolodex/Contact;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showEnumEditorAction(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    .line 396
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$showEnumEditorAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$showEnumEditorAction$1;-><init>(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showGroupsEditorAction(Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    .line 415
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$showGroupsEditorAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$showGroupsEditorAction$1;-><init>(Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final toEditRow(Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "-",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;)",
            "Lkotlin/Pair<",
            "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 230
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    new-instance v3, Lcom/squareup/ui/crm/edit/EditCustomerRow$BooleanRow;

    .line 231
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v6

    .line 232
    move-object v7, v1

    check-cast v7, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;

    invoke-virtual {v7}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;->getName()Ljava/lang/String;

    move-result-object v8

    .line 233
    invoke-virtual {v7}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;->getValue()Ljava/lang/Boolean;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 234
    :cond_0
    new-instance v7, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$1;

    invoke-direct {v7, v0, v1, v2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 230
    invoke-direct {v3, v6, v8, v4, v7}, Lcom/squareup/ui/crm/edit/EditCustomerRow$BooleanRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V

    .line 238
    invoke-static {v3, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 239
    :cond_1
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;

    const-string v6, ""

    if-eqz v3, :cond_3

    .line 240
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 241
    iget-object v4, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_email_address:I

    invoke-interface {v4, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 242
    move-object v7, v1

    check-cast v7, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;

    invoke-virtual {v7}, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;->getEmail()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    move-object v6, v7

    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "email:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$2;

    invoke-direct {v8, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$2;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v6, v7, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 239
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerRow$EmailRow;

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/ui/crm/edit/EditCustomerRow$EmailRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 246
    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 247
    :cond_3
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    if-eqz v3, :cond_5

    .line 248
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 249
    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;->getName()Ljava/lang/String;

    move-result-object v7

    .line 250
    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;->getValue()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    goto :goto_0

    :cond_4
    move-object v4, v6

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "custom_email:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;

    invoke-direct {v8, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v4, v6, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 247
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerRow$EmailRow;

    invoke-direct {v2, v3, v7, v1}, Lcom/squareup/ui/crm/edit/EditCustomerRow$EmailRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 254
    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 255
    :cond_5
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;

    const/4 v7, 0x1

    if-eqz v3, :cond_7

    .line 256
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 257
    iget-object v4, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_company:I

    invoke-interface {v4, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 259
    move-object v8, v1

    check-cast v8, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;

    invoke-virtual {v8}, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;->getCompanyName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    move-object v6, v8

    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "company:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$4;

    invoke-direct {v9, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$4;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v9, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v6, v8, v9}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 255
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;

    invoke-direct {v2, v3, v4, v7, v1}, Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 263
    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 264
    :cond_7
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;

    if-eqz v3, :cond_9

    .line 265
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 266
    iget-object v4, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_reference_id:I

    invoke-interface {v4, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 268
    move-object v8, v1

    check-cast v8, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;

    invoke-virtual {v8}, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;->getReferenceId()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_8

    move-object v6, v8

    :cond_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "reference:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$5;

    invoke-direct {v9, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$5;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v9, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v6, v8, v9}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 264
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;

    invoke-direct {v2, v3, v4, v7, v1}, Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 272
    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 273
    :cond_9
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    if-eqz v3, :cond_b

    .line 274
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 275
    move-object v7, v1

    check-cast v7, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    invoke-virtual {v7}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object v8

    .line 277
    invoke-virtual {v7}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    move-object v6, v7

    :cond_a
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "custom_text:"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v9, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$6;

    invoke-direct {v9, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$6;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v9, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v6, v7, v9}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 273
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;

    invoke-direct {v2, v3, v8, v4, v1}, Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 281
    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 282
    :cond_b
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;

    if-eqz v3, :cond_d

    .line 283
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 284
    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;

    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;->getName()Ljava/lang/String;

    move-result-object v7

    .line 285
    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;->getValue()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    goto :goto_1

    :cond_c
    move-object v4, v6

    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "custom_number:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$7;

    invoke-direct {v8, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$7;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v4, v6, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 282
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerRow$NumberRow;

    invoke-direct {v2, v3, v7, v1}, Lcom/squareup/ui/crm/edit/EditCustomerRow$NumberRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 289
    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 290
    :cond_d
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    if-eqz v3, :cond_f

    .line 291
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 292
    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v6

    .line 293
    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_e

    goto :goto_2

    :cond_e
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 294
    :goto_2
    new-instance v7, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$8;

    invoke-direct {v7, v0, v1, v2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$8;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 290
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerRow$EnumRow;

    invoke-direct {v1, v3, v6, v4, v7}, Lcom/squareup/ui/crm/edit/EditCustomerRow$EnumRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    .line 295
    invoke-static {v1, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 296
    :cond_f
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    if-eqz v3, :cond_14

    .line 297
    iget-object v3, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_first_name:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 298
    iget-object v3, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_last_name:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 300
    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;->getFirstName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_10

    goto :goto_3

    :cond_10
    move-object v8, v6

    :goto_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "first_name:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;

    invoke-direct {v10, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v10, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v8, v9, v10}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v13

    .line 305
    invoke-virtual {v4}, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;->getLastName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_11

    goto :goto_4

    :cond_11
    move-object v4, v6

    :goto_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "last_name:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$lastNameEdit$1;

    invoke-direct {v8, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$lastNameEdit$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v4, v6, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    .line 310
    iget-object v4, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/CountryCode;

    if-nez v4, :cond_12

    goto :goto_5

    :cond_12
    sget-object v6, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v4}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v4

    aget v4, v6, v4

    if-eq v4, v7, :cond_13

    .line 318
    :goto_5
    new-instance v4, Lcom/squareup/ui/crm/edit/EditCustomerRow$NameRow;

    .line 319
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v9

    move-object v8, v4

    move-object v10, v12

    move-object v11, v13

    move-object v12, v3

    move-object v13, v2

    .line 318
    invoke-direct/range {v8 .. v13}, Lcom/squareup/ui/crm/edit/EditCustomerRow$NameRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 324
    invoke-static {v4, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 311
    :cond_13
    new-instance v4, Lcom/squareup/ui/crm/edit/EditCustomerRow$NameRow;

    .line 312
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v9

    move-object v8, v4

    move-object v10, v3

    move-object v11, v2

    .line 311
    invoke-direct/range {v8 .. v13}, Lcom/squareup/ui/crm/edit/EditCustomerRow$NameRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 317
    invoke-static {v4, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 327
    :cond_14
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    if-eqz v3, :cond_1a

    .line 328
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 329
    iget-object v6, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_groups:I

    invoke-interface {v6, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 330
    move-object v8, v1

    check-cast v8, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    invoke-virtual {v8}, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;->getGroups()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_15

    goto :goto_6

    .line 331
    :cond_15
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    :goto_6
    check-cast v8, Ljava/lang/Iterable;

    .line 523
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    check-cast v9, Ljava/util/Collection;

    .line 524
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_16
    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_18

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    move-object v11, v10

    check-cast v11, Lcom/squareup/protos/client/rolodex/Group;

    .line 332
    iget-object v11, v11, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v12, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne v11, v12, :cond_17

    const/4 v11, 0x1

    goto :goto_8

    :cond_17
    const/4 v11, 0x0

    :goto_8
    if-eqz v11, :cond_16

    invoke-interface {v9, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 525
    :cond_18
    check-cast v9, Ljava/util/List;

    check-cast v9, Ljava/lang/Iterable;

    .line 526
    new-instance v4, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v9, v7}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 527
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_19

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 528
    check-cast v8, Lcom/squareup/protos/client/rolodex/Group;

    .line 333
    iget-object v8, v8, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 529
    :cond_19
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 334
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->sorted(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    .line 335
    new-instance v7, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$11;

    invoke-direct {v7, v0, v1, v2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$11;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 327
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerRow$GroupsRow;

    invoke-direct {v1, v3, v6, v4, v7}, Lcom/squareup/ui/crm/edit/EditCustomerRow$GroupsRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    .line 336
    invoke-static {v1, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 337
    :cond_1a
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    const-string v4, "countryCodeProvider.get()"

    if-eqz v3, :cond_1c

    .line 339
    iget-object v3, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->addressWorkflow:Lcom/squareup/address/workflow/AddressWorkflow;

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 340
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "address:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 341
    new-instance v12, Lcom/squareup/address/workflow/AddressProps;

    .line 342
    move-object v6, v1

    check-cast v6, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    invoke-virtual {v6}, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->getAddress()Lcom/squareup/address/Address;

    move-result-object v6

    if-eqz v6, :cond_1b

    goto :goto_a

    :cond_1b
    new-instance v6, Lcom/squareup/address/Address;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v13, v6

    invoke-direct/range {v13 .. v19}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    :goto_a
    move-object v7, v6

    .line 343
    iget-object v6, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v6

    check-cast v8, Lcom/squareup/CountryCode;

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v12

    .line 341
    invoke-direct/range {v6 .. v11}, Lcom/squareup/address/workflow/AddressProps;-><init>(Lcom/squareup/address/Address;Lcom/squareup/CountryCode;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 345
    new-instance v4, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$1;

    invoke-direct {v4, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 338
    invoke-interface {v2, v3, v12, v5, v4}, Lcom/squareup/workflow/RenderContext;->renderChild(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/address/workflow/AddressScreen;

    .line 349
    new-instance v3, Lcom/squareup/ui/crm/edit/EditCustomerRow$AddressRow;

    .line 350
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 351
    iget-object v4, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_edit_customer_address:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 352
    invoke-virtual {v2}, Lcom/squareup/address/workflow/AddressScreen;->getBody()Lcom/squareup/address/workflow/AddressBodyScreen;

    move-result-object v5

    .line 349
    invoke-direct {v3, v1, v4, v5}, Lcom/squareup/ui/crm/edit/EditCustomerRow$AddressRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/address/workflow/AddressBodyScreen;)V

    .line 353
    invoke-virtual {v2}, Lcom/squareup/address/workflow/AddressScreen;->getDialog()Lcom/squareup/address/workflow/ChooseStateDialogScreen;

    move-result-object v1

    invoke-static {v3, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto/16 :goto_c

    .line 355
    :cond_1c
    instance-of v3, v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    if-eqz v3, :cond_1e

    .line 357
    iget-object v3, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->addressWorkflow:Lcom/squareup/address/workflow/AddressWorkflow;

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 358
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "custom_address:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 359
    new-instance v12, Lcom/squareup/address/workflow/AddressProps;

    .line 360
    move-object v13, v1

    check-cast v13, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    invoke-virtual {v13}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;->getValue()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v6

    if-eqz v6, :cond_1d

    invoke-static {v6}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object v6

    if-eqz v6, :cond_1d

    goto :goto_b

    :cond_1d
    new-instance v6, Lcom/squareup/address/Address;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v14, v6

    invoke-direct/range {v14 .. v20}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    :goto_b
    move-object v7, v6

    .line 361
    iget-object v6, v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v6

    check-cast v8, Lcom/squareup/CountryCode;

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v12

    .line 359
    invoke-direct/range {v6 .. v11}, Lcom/squareup/address/workflow/AddressProps;-><init>(Lcom/squareup/address/Address;Lcom/squareup/CountryCode;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 363
    new-instance v4, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;

    invoke-direct {v4, v0, v1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 356
    invoke-interface {v2, v3, v12, v5, v4}, Lcom/squareup/workflow/RenderContext;->renderChild(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/address/workflow/AddressScreen;

    .line 369
    new-instance v3, Lcom/squareup/ui/crm/edit/EditCustomerRow$AddressRow;

    .line 370
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 371
    invoke-virtual {v13}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;->getName()Ljava/lang/String;

    move-result-object v4

    .line 372
    invoke-virtual {v2}, Lcom/squareup/address/workflow/AddressScreen;->getBody()Lcom/squareup/address/workflow/AddressBodyScreen;

    move-result-object v5

    .line 369
    invoke-direct {v3, v1, v4, v5}, Lcom/squareup/ui/crm/edit/EditCustomerRow$AddressRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/address/workflow/AddressBodyScreen;)V

    .line 373
    invoke-virtual {v2}, Lcom/squareup/address/workflow/AddressScreen;->getDialog()Lcom/squareup/address/workflow/ChooseStateDialogScreen;

    move-result-object v1

    invoke-static {v3, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    goto :goto_c

    .line 375
    :cond_1e
    invoke-static {v5, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    :goto_c
    return-object v1
.end method

.method private final updateAttributeAction(Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    .line 389
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateAttributeAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateAttributeAction$1;-><init>(Lcom/squareup/crm/model/ContactAttribute;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final updateSchemaAction(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    .line 382
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;-><init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/crm/edit/EditCustomerState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 433
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 438
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 439
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 440
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 441
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 442
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 444
    :cond_3
    check-cast v1, Lcom/squareup/ui/crm/edit/EditCustomerState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 104
    :cond_4
    sget-object p1, Lcom/squareup/ui/crm/edit/EditCustomerState$Loading;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerState$Loading;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/crm/edit/EditCustomerState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->initialState(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/crm/edit/EditCustomerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;

    check-cast p2, Lcom/squareup/ui/crm/edit/EditCustomerState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->render(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "-",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    instance-of v0, p2, Lcom/squareup/ui/crm/edit/EditCustomerState$Loading;

    const-string v1, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    const-string v3, ""

    if-eqz v0, :cond_1

    .line 115
    sget-object p2, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->merchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexMerchantLoader;->merchant()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/receiving/StandardReceiver$Companion;->filterSuccess(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p2

    .line 445
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p2

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lorg/reactivestreams/Publisher;

    if-eqz p2, :cond_0

    .line 447
    invoke-static {p2}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p2

    .line 448
    const-class v0, Lcom/squareup/protos/client/rolodex/Merchant;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v1

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 115
    new-instance p2, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/ui/crm/edit/EditCustomerProps;)V

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 119
    new-instance p2, Lcom/squareup/ui/crm/edit/EditCustomerScreen;

    .line 120
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->getTitle(Lcom/squareup/ui/crm/edit/EditCustomerProps;)Lcom/squareup/resources/TextModel;

    move-result-object p1

    .line 121
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$2;

    invoke-direct {v0, p0, p3}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$2;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 122
    sget-object p3, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Loading;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Loading;

    check-cast p3, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    .line 119
    invoke-direct {p2, p1, v0, p3}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;-><init>(Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 450
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 451
    const-class p3, Lcom/squareup/ui/crm/edit/EditCustomerScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 452
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 450
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 125
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_8

    .line 447
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 127
    :cond_1
    instance-of v0, p2, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing;

    if-eqz v0, :cond_12

    .line 128
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/EditCustomerState;->getKeys()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 454
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 463
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 462
    check-cast v5, Ljava/lang/String;

    .line 128
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/EditCustomerState;->getData()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/crm/model/ContactAttribute;

    if-eqz v5, :cond_3

    invoke-direct {p0, v5, p3}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->toEditRow(Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;

    move-result-object v5

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_2

    .line 462
    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 465
    :cond_4
    check-cast v4, Ljava/util/List;

    .line 130
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->getTitle(Lcom/squareup/ui/crm/edit/EditCustomerProps;)Lcom/squareup/resources/TextModel;

    move-result-object v0

    .line 131
    new-instance v5, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$1;

    invoke-direct {v5, p0, p3}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 133
    check-cast v4, Ljava/lang/Iterable;

    .line 466
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    check-cast v6, Ljava/util/Collection;

    .line 475
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 474
    check-cast v8, Lkotlin/Pair;

    .line 133
    invoke-virtual {v8}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/ui/crm/edit/EditCustomerRow;

    if-eqz v8, :cond_5

    .line 474
    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 477
    :cond_6
    check-cast v6, Ljava/util/List;

    .line 134
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->isSaveEnabled(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;)Z

    move-result v7

    .line 135
    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;

    invoke-direct {v8, p0, p3, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;)V

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 132
    new-instance p1, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;

    invoke-direct {p1, v6, v7, v8}, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;-><init>(Ljava/util/List;ZLkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    .line 129
    new-instance v6, Lcom/squareup/ui/crm/edit/EditCustomerScreen;

    invoke-direct {v6, v0, v5, p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;-><init>(Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;)V

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 479
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 480
    const-class v0, Lcom/squareup/ui/crm/edit/EditCustomerScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 481
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 479
    invoke-direct {p1, v0, v6, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 141
    sget-object v0, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    .line 483
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 492
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 491
    check-cast v5, Lkotlin/Pair;

    .line 144
    invoke-virtual {v5}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    if-eqz v5, :cond_7

    .line 491
    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 494
    :cond_8
    check-cast v0, Ljava/util/List;

    .line 144
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 145
    instance-of v4, v0, Lcom/squareup/address/workflow/ChooseStateDialogScreen;

    if-eqz v4, :cond_9

    .line 496
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 497
    const-class v5, Lcom/squareup/address/workflow/ChooseStateDialogScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 498
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 496
    invoke-direct {v4, v5, v0, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 147
    sget-object v0, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v4, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto :goto_4

    .line 148
    :cond_9
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 151
    :goto_4
    invoke-static {p1, v0}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 154
    instance-of v0, p2, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingEnum;

    if-eqz v0, :cond_b

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->editCustomerEnumWorkflow:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    move-object v5, p1

    check-cast v5, Lcom/squareup/workflow/Workflow;

    .line 157
    move-object p1, p2

    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingEnum;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingEnum;->getSelectedEnum()Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingEnum;->getSelectedEnum()Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getAllValues()Ljava/util/List;

    move-result-object v1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingEnum;->getSelectedEnum()Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_a

    goto :goto_5

    :cond_a
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 160
    :goto_5
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingEnum;->getSelectedEnum()Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getAllowMultiple()Z

    move-result p1

    .line 156
    new-instance v6, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    invoke-direct {v6, v0, v1, v2, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Z)V

    const/4 v7, 0x0

    .line 162
    new-instance p1, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$3;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$3;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/ui/crm/edit/EditCustomerState;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p3

    .line 154
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 501
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 502
    const-class p3, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 503
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 501
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 165
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_8

    .line 167
    :cond_b
    instance-of v0, p2, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$LoadingGroups;

    if-eqz v0, :cond_d

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v0

    .line 505
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_c

    .line 507
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 508
    const-class v1, Ljava/util/List;

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 168
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/crm/edit/EditCustomerState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 174
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto/16 :goto_8

    .line 507
    :cond_c
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 176
    :cond_d
    instance-of v0, p2, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    if-eqz v0, :cond_11

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->chooseGroupsWorkflow:Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;

    move-object v5, v0

    check-cast v5, Lcom/squareup/workflow/Workflow;

    .line 179
    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getAllGroups()Ljava/util/List;

    move-result-object v1

    .line 180
    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getAttribute()Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;->getGroups()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_e

    goto :goto_6

    .line 181
    :cond_e
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_6
    check-cast v0, Ljava/lang/Iterable;

    .line 509
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 510
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 511
    check-cast v4, Lcom/squareup/protos/client/rolodex/Group;

    .line 181
    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 512
    :cond_f
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 181
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 178
    new-instance v6, Lcom/squareup/crm/groups/choose/ChooseGroupsProps;

    invoke-direct {v6, v1, v0}, Lcom/squareup/crm/groups/choose/ChooseGroupsProps;-><init>(Ljava/util/List;Ljava/util/Set;)V

    const/4 v7, 0x0

    .line 183
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/ui/crm/edit/EditCustomerState;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p3

    .line 176
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 186
    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 188
    instance-of p3, p2, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;

    if-eqz p3, :cond_10

    .line 514
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 515
    const-class p3, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 516
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 514
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 190
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_8

    .line 191
    :cond_10
    instance-of p3, p2, Lcom/squareup/crm/groups/edit/EditGroupRendering;

    if-eqz p3, :cond_11

    .line 519
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 520
    const-class p3, Lcom/squareup/crm/groups/edit/EditGroupRendering;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 521
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 519
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 193
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :cond_11
    :goto_8
    return-object p1

    .line 153
    :cond_12
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/crm/edit/EditCustomerState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 85
    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->snapshotState(Lcom/squareup/ui/crm/edit/EditCustomerState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
