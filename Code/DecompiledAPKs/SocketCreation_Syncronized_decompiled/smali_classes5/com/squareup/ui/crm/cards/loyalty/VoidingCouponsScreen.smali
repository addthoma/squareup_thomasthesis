.class public Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "VoidingCouponsScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Factory;,
        Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;,
        Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Component;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
