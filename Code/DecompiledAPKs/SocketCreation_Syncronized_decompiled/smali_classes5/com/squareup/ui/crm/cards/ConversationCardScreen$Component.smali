.class public interface abstract Lcom/squareup/ui/crm/cards/ConversationCardScreen$Component;
.super Ljava/lang/Object;
.source "ConversationCardScreen.java"

# interfaces
.implements Lcom/squareup/ui/crm/ConversationView$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ConversationCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/ConversationCardView;)V
.end method
