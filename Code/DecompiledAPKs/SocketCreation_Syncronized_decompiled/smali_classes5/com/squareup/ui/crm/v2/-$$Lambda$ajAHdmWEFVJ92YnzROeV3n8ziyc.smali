.class public final synthetic Lcom/squareup/ui/crm/v2/-$$Lambda$ajAHdmWEFVJ92YnzROeV3n8ziyc;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func1;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/-$$Lambda$ajAHdmWEFVJ92YnzROeV3n8ziyc;->f$0:Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/-$$Lambda$ajAHdmWEFVJ92YnzROeV3n8ziyc;->f$0:Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->generateViewData(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    move-result-object p1

    return-object p1
.end method
