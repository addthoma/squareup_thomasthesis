.class public Lcom/squareup/ui/crm/cards/ConversationCardView;
.super Landroid/widget/LinearLayout;
.source "ConversationCardView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private conversationView:Lcom/squareup/ui/crm/ConversationView;

.field presenter:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    return-void
.end method


# virtual methods
.method conversationView()Lcom/squareup/ui/crm/ConversationView;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 37
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->presenter:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->presenter:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 33
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_conversation:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/ConversationView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 48
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method setActionBarUpButtonTextBackArrow(Ljava/lang/String;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setConversationToken(Ljava/lang/String;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/ConversationView;->setConversationToken(Ljava/lang/String;)V

    return-void
.end method
