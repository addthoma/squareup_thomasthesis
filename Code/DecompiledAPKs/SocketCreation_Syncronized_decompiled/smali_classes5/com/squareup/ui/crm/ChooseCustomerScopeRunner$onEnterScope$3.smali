.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "kotlin.jvm.PlatformType",
        "useRecent",
        "",
        "call",
        "(Ljava/lang/Boolean;)Lrx/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;->call(Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Boolean;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "useRecent"

    .line 133
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getRecentRolodexContactLoader$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/crm/RolodexContactLoader;

    move-result-object p1

    goto :goto_0

    .line 136
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getRolodexContactLoader$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/crm/RolodexContactLoader;

    move-result-object p1

    .line 132
    :goto_0
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
