.class final Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;
.super Ljava/lang/Object;
.source "PersonalInformationViewDataRenderer.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->toViewData(Lcom/squareup/protos/client/rolodex/Contact;Z)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/Merchant;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contact:Lcom/squareup/protos/client/rolodex/Contact;

.field final synthetic $editEnabled:Z

.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/protos/client/rolodex/Contact;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->this$0:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-boolean p3, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->$editEnabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/Merchant;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->this$0:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    .line 102
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-boolean v2, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->$editEnabled:Z

    .line 104
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Merchant;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Merchant;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 101
    :goto_0
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->viewData(Lcom/squareup/protos/client/rolodex/Contact;ZLcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    move-result-object p1

    return-object p1
.end method
