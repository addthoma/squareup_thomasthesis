.class public final Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;
.super Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;
.source "AddCustomerToSaleRunner.java"

# interfaces
.implements Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;
.implements Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;
.implements Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private contactListScrollPosition:I

.field private final contactLoaderForSearch:Lcom/squareup/crm/RolodexContactLoader;

.field private final eventLoader:Lcom/squareup/crm/RolodexEventLoader;

.field private final recentContactLoader:Lcom/squareup/crm/RolodexRecentContactLoader;

.field private searchTerm:Ljava/lang/String;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/crm/RolodexEventLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V
    .locals 29
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    move-object/from16 v20, p16

    move-object/from16 v11, p17

    move-object/from16 v14, p18

    move-object/from16 v13, p19

    move-object/from16 v18, p20

    move-object/from16 v19, p21

    move-object/from16 v12, p22

    move-object/from16 v15, p23

    move-object/from16 v21, p24

    move-object/from16 v22, p25

    move-object/from16 v23, p26

    move-object/from16 v24, p27

    move-object/from16 v25, p28

    move-object/from16 v26, p29

    move-object/from16 v27, p30

    move-object/from16 v28, p31

    .line 189
    invoke-direct/range {v0 .. v28}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;-><init>(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V

    const/4 v0, 0x0

    move-object/from16 v1, p0

    .line 151
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 152
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->searchTerm:Ljava/lang/String;

    move-object/from16 v0, p1

    .line 218
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->contactLoaderForSearch:Lcom/squareup/crm/RolodexContactLoader;

    move-object/from16 v0, p2

    .line 219
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->recentContactLoader:Lcom/squareup/crm/RolodexRecentContactLoader;

    move-object/from16 v0, p5

    .line 220
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    move-object/from16 v0, p16

    .line 221
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Ljava/lang/String;
    .locals 0

    .line 141
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->searchTerm:Ljava/lang/String;

    return-object p0
.end method

.method private addCardOnResult(Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V
    .locals 4

    .line 607
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 610
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v2, 0x1

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private customerCreated(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 6

    .line 369
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    sget-object v3, Lflow/Direction;->BACKWARD:Lflow/Direction;

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-static {v0, v3, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 373
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->isInvoicePath(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result v0

    const/4 v3, 0x2

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5, v5}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    sget-object v4, Lflow/Direction;->BACKWARD:Lflow/Direction;

    new-array v3, v3, [Lkotlin/jvm/functions/Function1;

    .line 378
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v3, v1

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$M_Z5yui7PYqBWFiakHztfhnwTa8;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$M_Z5yui7PYqBWFiakHztfhnwTa8;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    aput-object p1, v3, v2

    .line 377
    invoke-static {v0, v4, v3}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 390
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->setBaseContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->recentContactLoader:Lcom/squareup/crm/RolodexRecentContactLoader;

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/squareup/crm/RolodexRecentContactLoader;->addContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    sget-object v4, Lflow/Direction;->REPLACE:Lflow/Direction;

    new-array v3, v3, [Lkotlin/jvm/functions/Function1;

    .line 396
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v3, v1

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$i11sTryerXFpBS5AklxR3HWuO44;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$i11sTryerXFpBS5AklxR3HWuO44;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    aput-object p1, v3, v2

    .line 395
    invoke-static {v0, v4, v3}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method

.method private getTenderTokenForSaveCardPostTransaction()Ljava/lang/String;
    .locals 1

    .line 565
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    instance-of v0, v0, Lcom/squareup/payment/BillPayment;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    check-cast v0, Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 567
    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static synthetic lambda$7_ysgZhPOq44pm_AfjK9vRIe9TU(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->customerCreated(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V

    return-void
.end method

.method public static synthetic lambda$MCYoGKX4SPliZlCovnKC-2kg3X4(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->saveCardPostTransactionResult(Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V

    return-void
.end method

.method public static synthetic lambda$hLL5x0j9GH3nwiCn9-JekIfW4C0(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->addCardOnResult(Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 243
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getUpdateCustomerResultKey()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->ADD_TO_SALE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 244
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object p0

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private saveCardPostTransactionResult(Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V
    .locals 6

    .line 579
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 581
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getTenderTokenForSaveCardPostTransaction()Ljava/lang/String;

    move-result-object v0

    .line 582
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_ADD_CARD_POST_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    .line 583
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 584
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v5

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 582
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 587
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    .line 588
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$Z6KEhWi5sCtN4FUa22aS9tN1HY0;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$Z6KEhWi5sCtN4FUa22aS9tN1HY0;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 587
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public addThisCustomerToSale()V
    .locals 3

    .line 473
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 474
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 476
    sget-object v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$2;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 496
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid path type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 481
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method protected applyCouponToCartAndAddCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 0

    .line 614
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {p1, p2}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 615
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->addThisCustomerToSale()V

    return-void
.end method

.method public canShowCreateCustomerScreen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public cancelDeleteSingleCustomer()V
    .locals 1

    .line 544
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public closeChooseCustomerScreen()V
    .locals 2

    .line 303
    sget-object v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$2;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 335
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid path type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    goto :goto_0

    .line 313
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 306
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 331
    :pswitch_3
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->cancelSaveCard()Z

    goto :goto_0

    .line 318
    :pswitch_4
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public closeChooseCustomerScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 343
    sget-object v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$2;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 364
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->setBaseContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    goto :goto_0

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, v1}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 348
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->showFirstScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    :goto_0
    return-void
.end method

.method public closeCustomerActivityScreen()V
    .locals 2

    .line 502
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 503
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/CustomerActivityScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSendMessageScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 507
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->sendMessageScreenMessage:Ljava/lang/String;

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SendMessageScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSendMessageScreen(Lcom/squareup/protos/client/dialogue/Conversation;)V
    .locals 1

    .line 512
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexEventLoader;->refresh()V

    const/4 p1, 0x0

    .line 513
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->sendMessageScreenMessage:Ljava/lang/String;

    .line 514
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/cards/SendMessageScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public deleteSingleCustomer()V
    .locals 1

    .line 540
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getChooseCustomerScreenTitle()Ljava/lang/String;
    .locals 2

    .line 411
    sget-object v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$2;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_add_customer_to_invoice_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 421
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid path type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/crm/R$string;->crm_cardonfile_savecard_header:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContactListScrollPosition()I
    .locals 1

    .line 439
    iget v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->contactListScrollPosition:I

    return v0
.end method

.method public getContactLoaderForSearch()Lcom/squareup/crm/RolodexContactLoader;
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->contactLoaderForSearch:Lcom/squareup/crm/RolodexContactLoader;

    return-object v0
.end method

.method public getSearchTerm()Ljava/lang/String;
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->searchTerm:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public goBackFromViewCustomer()V
    .locals 2

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->goBackInCustomerFlow()Z

    .line 469
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public invoiceResults()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 448
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoicesCustomerLoader;->results()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$customerCreated$2$AddCustomerToSaleRunner(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 2

    .line 380
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_0

    .line 381
    const-class v0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    :cond_0
    return-object p1
.end method

.method public synthetic lambda$customerCreated$3$AddCustomerToSaleRunner(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 2

    .line 398
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    return-object p1
.end method

.method public synthetic lambda$saveCardPostTransactionResult$4$AddCustomerToSaleRunner(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 4

    .line 592
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->isInstance(Ljava/lang/Object;[Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 597
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 598
    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isOnPostTransactionMonitor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->pipTenderScope()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    :cond_1
    return-object p1
.end method

.method public loadMoreInvoices(Ljava/lang/Integer;)V
    .locals 1

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/InvoicesCustomerLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method public loadingInvoices()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .line 453
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoicesCustomerLoader;->progress()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 226
    invoke-super {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onEnterScope(Lmortar/MortarScope;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->contactLoaderForSearch:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->recentContactLoader:Lcom/squareup/crm/RolodexRecentContactLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    .line 232
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onResult()Lrx/Observable;

    move-result-object v0

    .line 234
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v1, v2, :cond_1

    .line 235
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$hLL5x0j9GH3nwiCn9-JekIfW4C0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$hLL5x0j9GH3nwiCn9-JekIfW4C0;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    goto :goto_1

    :cond_1
    :goto_0
    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$MCYoGKX4SPliZlCovnKC-2kg3X4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$MCYoGKX4SPliZlCovnKC-2kg3X4;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    .line 233
    :goto_1
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 231
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    .line 242
    invoke-interface {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$eAnotPrzc763wgFBWgwO_TC0PHs;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$eAnotPrzc763wgFBWgwO_TC0PHs;

    .line 243
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$7TEfz9_LWieKYiY5jBEFyfUcVkg;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$7TEfz9_LWieKYiY5jBEFyfUcVkg;

    .line 244
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$7_ysgZhPOq44pm_AfjK9vRIe9TU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCustomerToSaleRunner$7_ysgZhPOq44pm_AfjK9vRIe9TU;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    .line 245
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 241
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 247
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->isForTransferringLoyalty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 248
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->setBaseContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    :cond_2
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 253
    invoke-super {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onLoad(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "searchTerm"

    .line 257
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->searchTerm:Ljava/lang/String;

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 261
    invoke-super {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onSave(Landroid/os/Bundle;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->searchTerm:Ljava/lang/String;

    const-string v1, "searchTerm"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method scopeSupportsProfileAttachments()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setContactListScrollPosition(I)V
    .locals 0

    .line 443
    iput p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->contactListScrollPosition:I

    return-void
.end method

.method public setSearchTerm(Ljava/lang/String;)V
    .locals 0

    .line 435
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->searchTerm:Ljava/lang/String;

    return-void
.end method

.method public showConflictingContact()V
    .locals 3

    .line 548
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->setBaseContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 549
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showCreateCustomerScreen()V
    .locals 3

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;-><init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public showDeleteSingleCustomerScreen()V
    .locals 1

    .line 536
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showImagePreviewScreen(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 0

    .line 532
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public showInvoiceReadOnlyScreen(Ljava/lang/String;)V
    .locals 3

    .line 523
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 524
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, p1, v2}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;-><init>(Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showMergeContacts()V
    .locals 1

    .line 518
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showOverflowBottomSheet(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 0

    .line 528
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public showTransferLoyaltyAccount(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 623
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, v2, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {v1, v2, p2, p1}, Lcom/squareup/ui/crm/flow/CrmScope;->newTransferLoyaltyCustomerCard(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showUpdateLoyaltyPhoneConflictDialog(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 4

    .line 554
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    .line 555
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 558
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    sget-object p2, Lflow/Direction;->REPLACE:Lflow/Direction;

    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method
