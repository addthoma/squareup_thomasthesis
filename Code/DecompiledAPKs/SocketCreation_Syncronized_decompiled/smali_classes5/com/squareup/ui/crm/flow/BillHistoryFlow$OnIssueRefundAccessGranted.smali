.class Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "BillHistoryFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/BillHistoryFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnIssueRefundAccessGranted"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/BillHistoryFlow;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)V
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;->this$0:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/BillHistoryFlow$1;)V
    .locals 0

    .line 355
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;-><init>(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 5

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;->this$0:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->access$100(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;->this$0:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    invoke-static {v2}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->access$200(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;->this$0:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    invoke-static {v3}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->access$300(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lflow/Flow;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;Z)V

    return-void
.end method
