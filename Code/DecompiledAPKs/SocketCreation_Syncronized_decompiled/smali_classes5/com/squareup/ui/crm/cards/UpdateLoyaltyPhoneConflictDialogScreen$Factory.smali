.class public final Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Factory;
.super Ljava/lang/Object;
.source "UpdateLoyaltyPhoneConflictDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 65
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;->showConflictingContact()V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 67
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;->cancelUpdateLoyaltyPhoneConflictScreen()V

    return-void
.end method

.method static synthetic lambda$create$2(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 69
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;->showLoyaltyMergeProposal()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 56
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 57
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 58
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->updateLoyaltyConflictScreen()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;->runner()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;

    move-result-object v1

    .line 59
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->updateLoyaltyConflictScreen()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;->res()Lcom/squareup/util/Res;

    move-result-object v2

    .line 60
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->updateLoyaltyConflictScreen()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;->phoneHelper()Lcom/squareup/text/PhoneNumberHelper;

    move-result-object v0

    .line 62
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_conflict_title:I

    .line 63
    invoke-virtual {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_conflict_neutral:I

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$Factory$Ug2Ihf7kRfeu7BrV237Ds9O_j7Q;

    invoke-direct {v4, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$Factory$Ug2Ihf7kRfeu7BrV237Ds9O_j7Q;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;)V

    .line 64
    invoke-virtual {p1, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_conflict_negative:I

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$Factory$gIADsQZbNsm7sPGiuZa7jhjEZQE;

    invoke-direct {v4, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$Factory$gIADsQZbNsm7sPGiuZa7jhjEZQE;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;)V

    .line 66
    invoke-virtual {p1, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_conflict_positive:I

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$Factory$tRgZz6R6VMktaaGtO96P0mWpfzk;

    invoke-direct {v4, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$Factory$tRgZz6R6VMktaaGtO96P0mWpfzk;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;)V

    .line 68
    invoke-virtual {p1, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 70
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;->getConflictingPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2, v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;->access$000(Ljava/lang/String;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 62
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
