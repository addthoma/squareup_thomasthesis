.class public final Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;
.super Ljava/lang/Object;
.source "UpdateGroup2Flow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final createFilterFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateFilterFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final updateFilterFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/UpdateFilterFlow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateFilterFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/UpdateFilterFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->createFilterFlowProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->updateFilterFlowProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateFilterFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/UpdateFilterFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateFilterFlow;Lcom/squareup/ui/crm/flow/UpdateFilterFlow;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;-><init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateFilterFlow;Lcom/squareup/ui/crm/flow/UpdateFilterFlow;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->createFilterFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->updateFilterFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->newInstance(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateFilterFlow;Lcom/squareup/ui/crm/flow/UpdateFilterFlow;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow_Factory;->get()Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    move-result-object v0

    return-object v0
.end method
