.class public Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;
.super Ljava/lang/Object;
.source "RefundBillHistoryWrapper.java"


# instance fields
.field private final billHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;->billHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    const-string v0, "refundBill"

    .line 18
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;->billHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public getObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;->billHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method
