.class public Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ViewLoyaltyBalanceScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Component;,
        Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ViewLoyaltyBalanceScreen$KzO9xPBOx27BsoZA0X2Rh5g67XQ;->INSTANCE:Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ViewLoyaltyBalanceScreen$KzO9xPBOx27BsoZA0X2Rh5g67XQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;
    .locals 1

    .line 74
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 75
    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 69
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 38
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Component;->viewLoyaltyBalanceCoordinator()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 34
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_view_loyalty_balance:I

    return v0
.end method
