.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->toEditRow(Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "+",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditCustomerWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditCustomerWorkflow.kt\ncom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1\n*L\n1#1,432:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "newValue",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;->$this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "newValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;->$this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

    check-cast v0, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object p1, v2

    :cond_0
    check-cast p1, Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;->copy$default(Lcom/squareup/crm/model/ContactAttribute$NameAttribute;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    move-result-object p1

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$updateAttributeAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$firstNameEdit$1;->invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
