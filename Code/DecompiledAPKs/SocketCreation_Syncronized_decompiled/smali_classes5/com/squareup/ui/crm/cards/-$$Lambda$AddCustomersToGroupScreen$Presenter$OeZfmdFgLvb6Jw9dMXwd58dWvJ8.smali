.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

.field private final synthetic f$2:Ljava/util/concurrent/atomic/AtomicReference;

.field private final synthetic f$3:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final synthetic f$4:Lcom/squareup/protos/client/rolodex/Group;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$2:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object p5, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$4:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$2:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;->f$4:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->lambda$null$1$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
