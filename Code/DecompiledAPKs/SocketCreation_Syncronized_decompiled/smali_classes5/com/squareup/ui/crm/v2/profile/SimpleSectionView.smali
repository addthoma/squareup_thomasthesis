.class public Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;
.super Landroid/widget/LinearLayout;
.source "SimpleSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    }
.end annotation


# instance fields
.field private final callToAction:Landroid/widget/Button;

.field private final ctaButtonClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rows:Landroid/widget/LinearLayout;

.field private final sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_simple_section:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 56
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->setOrientation(I)V

    .line 58
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_simple_section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 59
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_simple_section_rows:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->rows:Landroid/widget/LinearLayout;

    .line 60
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_simple_section_cta_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->callToAction:Landroid/widget/Button;

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->callToAction:Landroid/widget/Button;

    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->ctaButtonClicked:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public getCtaButtonClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->ctaButtonClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public onEditButtonClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->onActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 66
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setViewData(Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;)V
    .locals 5

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerActionName:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 74
    iget-object v0, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->lineData:Ljava/util/List;

    .line 75
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 p1, 0x8

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->setVisibility(I)V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 79
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->setVisibility(I)V

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v2, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->lineData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    .line 82
    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->lineData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    .line 83
    new-instance v3, Lcom/squareup/ui/crm/rows/ProfileLineRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/ui/crm/rows/ProfileLineRow;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {v3, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->setInfo(Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;)V

    .line 85
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 87
    :cond_1
    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->bottomButtonText:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 88
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->callToAction:Landroid/widget/Button;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;->bottomButtonText:Ljava/lang/String;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;->callToAction:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :cond_2
    :goto_1
    return-void
.end method
