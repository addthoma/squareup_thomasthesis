.class public Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;
.super Ljava/lang/Object;
.source "ChooseFiltersFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$SharedScope;,
        Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;
    }
.end annotation


# static fields
.field private static final NO_FILTERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private bypassingChooseFiltersScreen:Z

.field private final createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

.field private filterIndex:Ljava/lang/Integer;

.field private final flow:Lflow/Flow;

.field private final modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;",
            ">;"
        }
    .end annotation
.end field

.field private final originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->NO_FILTERS:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateFilterFlow;Lcom/squareup/ui/crm/flow/UpdateFilterFlow;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    sget-object v0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->NO_FILTERS:Ljava/util/List;

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 72
    sget-object v0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->NO_FILTERS:Ljava/util/List;

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 73
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 81
    iput-boolean v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->bypassingChooseFiltersScreen:Z

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    .line 115
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    .line 116
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    .line 117
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic lambda$closeSaveFiltersScreen$3(Lflow/History;)Lflow/History;
    .locals 1

    .line 214
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    .line 215
    const-class v0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 216
    const-class v0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 217
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    return-object p0
.end method

.method private reset()V
    .locals 1

    const/4 v0, 0x0

    .line 287
    iput-boolean v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->bypassingChooseFiltersScreen:Z

    const/4 v0, 0x0

    .line 288
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 289
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public clearModifiedFilters()V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->NO_FILTERS:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_REMOVE_ALL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public closeChooseFiltersScreen()V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->NO_FILTERS:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->NO_FILTERS:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSaveFiltersScreen()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SaveFiltersScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSaveFiltersScreen(Lcom/squareup/protos/client/rolodex/GroupV2;)V
    .locals 3

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;

    sget-object v2, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$qre2cj-gM2qBAUF7Uopv89-u614;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$qre2cj-gM2qBAUF7Uopv89-u614;

    invoke-direct {v1, p1, v2}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;-><init>(Lcom/squareup/protos/client/rolodex/GroupV2;Lrx/functions/Func1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 219
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->reset()V

    .line 220
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_SAVE_AS_GROUP:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public commitChooseFiltersScreen()V
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 231
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->reset()V

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_APPLY:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public getFiltersForSaveFiltersScreen()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 121
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ChooseFiltersFlow(Lkotlin/Pair;Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 133
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/functions/Func1;

    invoke-interface {p1, p2}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    .line 138
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    const-class v0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    invoke-virtual {p2, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 139
    invoke-virtual {p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 140
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p2, v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    .line 141
    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 142
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    .line 145
    :cond_0
    sget-object p2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$ChooseFiltersFlow(Lkotlin/Pair;)V
    .locals 3

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 129
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$p0mtRWg2VMcHSefpQsnGdBZJzRo;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$p0mtRWg2VMcHSefpQsnGdBZJzRo;-><init>(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;Lkotlin/Pair;)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$ChooseFiltersFlow(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 2

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-eqz p1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 157
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 159
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 160
    iget-boolean p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->bypassingChooseFiltersScreen:Z

    if-eqz p1, :cond_1

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 163
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->reset()V

    :cond_1
    return-void
.end method

.method public modifiedFilters()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    .line 126
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$RQbNrNixtcAFFYp5n9oESpdB8E4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$RQbNrNixtcAFFYp5n9oESpdB8E4;-><init>(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;)V

    .line 127
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 125
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    .line 150
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$1-pt_YmCrAqRd_m-kQQM1jD_5n0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ChooseFiltersFlow$1-pt_YmCrAqRd_m-kQQM1jD_5n0;-><init>(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;)V

    .line 151
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 149
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 173
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v0, -0x1

    const-string v1, "filterIndex"

    .line 174
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    .line 175
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_1

    const/4 v0, 0x0

    .line 176
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v2, "originalFilters"

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Protos;->loadProtos(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v2, "modifiedFilters"

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Protos;->loadProtos(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;",
            ">;"
        }
    .end annotation

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "filterIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Ljava/util/List;)[B

    move-result-object v0

    const-string v1, "originalFilters"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Ljava/util/List;)[B

    move-result-object v0

    const-string v1, "modifiedFilters"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public originalFilters()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public showCreateFilterScreen()V
    .locals 3

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    new-instance v1, Lcom/squareup/ui/crm/applet/CreateFilterScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/applet/CreateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 238
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 237
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_ADD_FILTER:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)V"
        }
    .end annotation

    .line 253
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 256
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    .line 258
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->createFilterFlow:Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    new-instance v1, Lcom/squareup/ui/crm/applet/CreateFilterScope;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/applet/CreateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1, p2}, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;)V

    goto :goto_0

    .line 262
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public showFirstScreenForFilter(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ")V"
        }
    .end annotation

    .line 272
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 273
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->originalFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 276
    invoke-interface {p2, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    .line 277
    iput-boolean v1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->bypassingChooseFiltersScreen:Z

    .line 279
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    new-instance v0, Lcom/squareup/ui/crm/applet/UpdateFilterScope;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/applet/UpdateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, v0, p3}, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method

.method public showSaveFiltersScreen()V
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveFiltersScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showUpdateFilter(I)V
    .locals 3

    .line 243
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->filterIndex:Ljava/lang/Integer;

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->updateFilterFlow:Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    new-instance v1, Lcom/squareup/ui/crm/applet/UpdateFilterScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/applet/UpdateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->modifiedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 245
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 244
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method
