.class public final enum Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;
.super Ljava/lang/Enum;
.source "ReminderScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ReminderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReminderOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

.field public static final enum ONE_MONTH:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

.field public static final enum ONE_WEEK:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

.field public static final enum TOMORROW:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 42
    new-instance v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    const/4 v1, 0x0

    const-string v2, "TOMORROW"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->TOMORROW:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    new-instance v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    const/4 v2, 0x1

    const-string v3, "ONE_WEEK"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->ONE_WEEK:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    new-instance v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    const/4 v3, 0x2

    const-string v4, "ONE_MONTH"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->ONE_MONTH:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    .line 41
    sget-object v4, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->TOMORROW:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->ONE_WEEK:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->ONE_MONTH:Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->$VALUES:[Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;
    .locals 1

    .line 41
    const-class v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->$VALUES:[Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;

    return-object v0
.end method
