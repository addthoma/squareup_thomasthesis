.class Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ProfileAttachmentsSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Attachment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

.field final synthetic val$attachment:Lcom/squareup/protos/client/rolodex/Attachment;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;->val$attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->access$100(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->access$000(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;->val$attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-interface {p1, v0, v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->showImagePreviewScreen(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V

    return-void
.end method
