.class public Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;
.super Lmortar/ViewPresenter;
.source "ProfileAttachmentsSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_ATTACHMENTS_MOBILE:I = 0x3

.field private static final MAX_ATTACHMENTS_TABLET:I = 0x5


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

.field private final contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final res:Lcom/squareup/util/Res;

.field private final resultsForContact:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 64
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 66
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 67
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    .line 68
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->res:Lcom/squareup/util/Res;

    .line 69
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->dateFormatter:Ljava/text/DateFormat;

    .line 70
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 71
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->locale:Ljava/util/Locale;

    .line 72
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->device:Lcom/squareup/util/Device;

    .line 73
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    .line 76
    invoke-virtual {p1}, Lcom/squareup/crm/RolodexAttachmentLoader;->results()Lio/reactivex/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$dS1CJpF0ShMSoCudlYU_sSa36ds;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$dS1CJpF0ShMSoCudlYU_sSa36ds;

    .line 75
    invoke-static {p1, p2, p3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->resultsForContact:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method private bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 4

    .line 125
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->getFormattedDateTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 127
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 128
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment$FileType;->IMAGE:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 129
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/crm/R$drawable;->crm_file_image:I

    invoke-static {v1, v3, v2}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$1;-><init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;Lcom/squareup/protos/client/rolodex/Attachment;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 137
    :cond_0
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment$FileType;->PDF:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    if-ne v0, v1, :cond_1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 140
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/crm/R$drawable;->crm_file_document:I

    invoke-static {v1, v3, v2}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 139
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 141
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$2;-><init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 147
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisibility(I)V

    .line 148
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 150
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/crm/R$drawable;->crm_action_overflow:I

    invoke-static {v1, v3, v2}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 152
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter$3;-><init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;Lcom/squareup/protos/client/rolodex/Attachment;)V

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private getFormattedDateTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 4

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->timeFormatter:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->locale:Ljava/util/Locale;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatDateTimeForProfileAttachments(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$new$0(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method

.method private onAttachments(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;)V"
        }
    .end annotation

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->clearRows()V

    .line 118
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Attachment;

    .line 119
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->addRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Attachment;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    .line 121
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->setVisible(Z)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$2$ProfileAttachmentsSectionPresenter(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 94
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasItems()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object p2

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->device:Lcom/squareup/util/Device;

    .line 98
    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    .line 99
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, v1, :cond_1

    .line 100
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->onAttachments(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;Ljava/util/List;)V

    goto :goto_1

    .line 102
    :cond_1
    invoke-interface {p2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->onAttachments(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;Ljava/util/List;)V

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->showViewAll()V

    goto :goto_1

    .line 106
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->onAttachments(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$ProfileAttachmentsSectionPresenter(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/RolodexAttachmentLoader;->setContactToken(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$ProfileAttachmentsSectionPresenter(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->resultsForContact:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$19MLIno4dhYJsK_5kFi3J3HNtsg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$19MLIno4dhYJsK_5kFi3J3HNtsg;-><init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;)V

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 80
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$Qchj5awCXHtwel-Nhrp0BL_XLWM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$Qchj5awCXHtwel-Nhrp0BL_XLWM;-><init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 82
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 88
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;

    .line 91
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$bWM5MXldGppGQr5hauEufWN7_-s;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ProfileAttachmentsSectionPresenter$bWM5MXldGppGQr5hauEufWN7_-s;-><init>(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
