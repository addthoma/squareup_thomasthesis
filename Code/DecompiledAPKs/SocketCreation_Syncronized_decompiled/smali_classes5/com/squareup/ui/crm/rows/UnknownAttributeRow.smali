.class public Lcom/squareup/ui/crm/rows/UnknownAttributeRow;
.super Lcom/squareup/marketfont/MarketEditText;
.source "UnknownAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# instance fields
.field private unknownAttribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;->unknownAttribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-object v0
.end method

.method public showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 1

    .line 19
    iput-object p2, p0, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;->unknownAttribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 21
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;->setHint(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 22
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;->setText(Ljava/lang/CharSequence;)V

    .line 23
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const/16 p1, 0x8

    :goto_1
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/UnknownAttributeRow;->setVisibility(I)V

    return-void
.end method
