.class public Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;
.super Ljava/lang/Object;
.source "UpdateGroup2Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Result"
.end annotation


# instance fields
.field public final backOut:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field public final newGroup:Lcom/squareup/protos/client/rolodex/Group;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/rolodex/Group;Lrx/functions/Func1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Group;",
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;)V"
        }
    .end annotation

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->newGroup:Lcom/squareup/protos/client/rolodex/Group;

    .line 84
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->backOut:Lrx/functions/Func1;

    return-void
.end method

.method public static dismissed()Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;-><init>(Lcom/squareup/protos/client/rolodex/Group;Lrx/functions/Func1;)V

    return-object v0
.end method

.method static groupDeleted(Lrx/functions/Func1;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;"
        }
    .end annotation

    .line 74
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;-><init>(Lcom/squareup/protos/client/rolodex/Group;Lrx/functions/Func1;)V

    return-object v0
.end method

.method static groupUpdated(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;
    .locals 2

    .line 70
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;-><init>(Lcom/squareup/protos/client/rolodex/Group;Lrx/functions/Func1;)V

    return-object v0
.end method
