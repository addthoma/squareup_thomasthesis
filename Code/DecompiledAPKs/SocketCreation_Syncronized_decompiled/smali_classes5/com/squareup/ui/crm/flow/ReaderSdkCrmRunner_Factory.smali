.class public final Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;
.super Ljava/lang/Object;
.source "ReaderSdkCrmRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final controllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiAddCardOnFileController;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiAddCardOnFileController;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;->controllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiAddCardOnFileController;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/api/ApiAddCardOnFileController;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;->controllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;->newInstance(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner_Factory;->get()Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;

    move-result-object v0

    return-object v0
.end method
