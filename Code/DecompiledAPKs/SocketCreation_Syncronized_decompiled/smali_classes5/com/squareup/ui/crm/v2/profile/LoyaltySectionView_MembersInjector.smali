.class public final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;
.super Ljava/lang/Object;
.source "LoyaltySectionView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final rewardRecyclerViewHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->rewardRecyclerViewHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    return-void
.end method

.method public static injectRewardRecyclerViewHelper(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->rewardRecyclerViewHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->injectRewardRecyclerViewHelper(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;)V

    return-void
.end method
