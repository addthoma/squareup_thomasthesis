.class public Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;
.super Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.source "ContactListViewV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;,
        Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$SharedScope;
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

.field private inMultiSelectMode:Z

.field private final onContactClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final scrollPosition:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 50
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 p2, 0x0

    .line 51
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 57
    const-class v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;->inject(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V

    .line 59
    iput-boolean p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->inMultiSelectMode:Z

    .line 63
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setAreHeadersSticky(Z)V

    .line 65
    new-instance p2, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-direct {p2, p1, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;-><init>(Landroid/content/Context;Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;)V

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    .line 67
    new-instance p1, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$1;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Z
    .locals 0

    .line 37
    iget-boolean p0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->inMultiSelectMode:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic lambda$onNearEndOfList$1(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$onNearEndOfList$2(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 208
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method


# virtual methods
.method public getMultiSelectedContactSet()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;>;"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getMultiSelectedContactSet()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method hideBottomRow()Z
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->hideBottomRow()Z

    move-result v0

    return v0
.end method

.method public hideTopRow()V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->hideTopRow()V

    return-void
.end method

.method public init(Lcom/squareup/crm/RolodexContactLoader;I)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 117
    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->initForMultiSelect(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;IZ)V

    return-void
.end method

.method public initForMultiSelect(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;IZ)V
    .locals 6

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->init(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;IZ)V

    return-void
.end method

.method public synthetic lambda$scrollToPosition$0$ContactListViewV2()V
    .locals 1

    const/4 v0, 0x0

    .line 198
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setSelection(I)V

    return-void
.end method

.method notifyDataSetChanged()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->notifyDataSetChanged()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 100
    invoke-super {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->onAttachedToWindow()V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->takeView(Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    return-void
.end method

.method public onContactClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->dropView(Ljava/lang/Object;)V

    .line 108
    invoke-super {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 83
    invoke-super {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->onFinishInflate()V

    const/4 v0, 0x0

    .line 86
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$cxTKYfRu5qI29ejzRNVQD8GdH7g;->INSTANCE:Lcom/squareup/ui/crm/cards/contact/-$$Lambda$cxTKYfRu5qI29ejzRNVQD8GdH7g;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 88
    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$2;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method onNearEndOfList()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 206
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListViewV2$hLpBWTQuZUjFL6h7B4DNxqrxCuE;->INSTANCE:Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListViewV2$hLpBWTQuZUjFL6h7B4DNxqrxCuE;

    .line 207
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListViewV2$hyC_FgDje5TKR0l_8Z5ZJ3rspFY;->INSTANCE:Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListViewV2$hyC_FgDje5TKR0l_8Z5ZJ3rspFY;

    .line 208
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public scrollPosition()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method scrollToPosition(I)V
    .locals 5

    if-lez p1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getHeaderId(I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getHeaderId(I)J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    add-int/lit8 p1, p1, 0x1

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    .line 193
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setSelection(I)V

    if-nez p1, :cond_2

    .line 196
    new-instance p1, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListViewV2$OI-YcGpmv70Lirk-0ld7RvhdMnA;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListViewV2$OI-YcGpmv70Lirk-0ld7RvhdMnA;-><init>(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method scrollToTop()V
    .locals 1

    const/4 v0, -0x1

    .line 166
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 167
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public selectedContactCount()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->selectedContactCount()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setMultiSelect(ZLcom/squareup/protos/client/rolodex/ContactSet;)V
    .locals 1

    .line 142
    iput-boolean p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->inMultiSelectMode:Z

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->setMultiSelect(ZLcom/squareup/protos/client/rolodex/ContactSet;)V

    return-void
.end method

.method public setShowGreyHeaderRows(Z)V
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->setShowGreyHeaderRows(Z)V

    return-void
.end method

.method showBottomRow(Lrx/functions/Action1;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    sget v1, Lcom/squareup/crm/R$layout;->crm_contact_list_bottom_row:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->showBottomRow(ILrx/functions/Action1;)Z

    move-result p1

    return p1
.end method

.method public showTop2Row(ILrx/functions/Action1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->showTop2Row(ILrx/functions/Action1;)V

    return-void
.end method

.method public showTopRow(ILrx/functions/Action1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->adapter:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->showTopRow(ILrx/functions/Action1;)V

    return-void
.end method
