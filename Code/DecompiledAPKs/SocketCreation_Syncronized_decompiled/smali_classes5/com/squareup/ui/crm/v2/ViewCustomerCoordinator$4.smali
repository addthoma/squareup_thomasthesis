.class synthetic Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;
.super Ljava/lang/Object;
.source "ViewCustomerCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

.field static final synthetic $SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

.field static final synthetic $SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 314
    invoke-static {}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->values()[Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->UNINITIALIZED:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    sget-object v3, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->LOADING:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    sget-object v4, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {v4}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    sget-object v5, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ERROR:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {v5}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 1012
    :catch_3
    invoke-static {}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->values()[Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    :try_start_4
    sget-object v4, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v5, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v5}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v4, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v5, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewAppointment:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v5}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v5

    aput v1, v4, v5
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v4, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v5, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->AddToSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v5}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v5

    aput v2, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v4, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v5, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromEstimate:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v5}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    const/4 v4, 0x5

    :try_start_8
    sget-object v5, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v6, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromInvoice:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v6}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v6

    aput v4, v5, v6
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    const/4 v5, 0x6

    :try_start_9
    sget-object v6, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v7, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v7}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v7

    aput v5, v6, v7
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    const/4 v6, 0x7

    :try_start_a
    sget-object v7, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$v2$ViewCustomerCoordinator$ExposedAction:[I

    sget-object v8, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->TransferLoyalty:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v8}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->ordinal()I

    move-result v8

    aput v6, v7, v8
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    .line 966
    :catch_a
    invoke-static {}, Lcom/squareup/ui/crm/flow/CrmScopeType;->values()[Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v7

    array-length v7, v7

    new-array v7, v7, [I

    sput-object v7, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    :try_start_b
    sget-object v7, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v8, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v8}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v8

    aput v0, v7, v8
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v7, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v7}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v7

    aput v1, v0, v7
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aput v4, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aput v5, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aput v6, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    :catch_12
    :try_start_13
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_13

    :catch_13
    :try_start_14
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :catch_14
    :try_start_15
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_15

    :catch_15
    :try_start_16
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_16

    :catch_16
    :try_start_17
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_17

    :catch_17
    :try_start_18
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_18

    :catch_18
    :try_start_19
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_19

    :catch_19
    :try_start_1a
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_1a

    :catch_1a
    :try_start_1b
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$4;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_1b

    :catch_1b
    return-void
.end method
