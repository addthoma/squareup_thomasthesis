.class public Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;
.super Landroid/widget/LinearLayout;
.source "ConversationMessageLeftRow.java"


# instance fields
.field private final creatorTimestamp:Lcom/squareup/widgets/MessageView;

.field private final message:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    sget v0, Lcom/squareup/crm/R$layout;->crm_conversation_message_left_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 21
    sget p1, Lcom/squareup/crm/R$id;->crm_message_left:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->message:Landroid/widget/TextView;

    .line 22
    sget p1, Lcom/squareup/crm/R$id;->crm_message_creator_timestamp:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->creatorTimestamp:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public showCreatorTimestamp(Ljava/lang/String;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->creatorTimestamp:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showMessage(Ljava/lang/String;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->message:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
