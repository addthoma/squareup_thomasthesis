.class public interface abstract Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionRunner;
.super Ljava/lang/Object;
.source "CardOnFileSectionRunner.java"


# virtual methods
.method public abstract getContactForUnlinkInstrumentDialog()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getInstrumentForUnlinkInstrumentDialog()Lcom/squareup/protos/client/instruments/InstrumentSummary;
.end method

.method public abstract showConfirmUnlinkInstrumentDialog(Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
.end method

.method public abstract showSaveCardToCustomerScreenV2()V
.end method

.method public abstract unlinkInstrumentConfirmClicked()V
.end method
