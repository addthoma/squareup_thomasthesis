.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapComponent;
.super Ljava/lang/Object;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$BaseComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule$WithBinds;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ApiCardOnFileBootstrapComponent"
.end annotation


# virtual methods
.method public abstract customerSaveCardScreen()Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Component;
.end method

.method public abstract dippedCardSpinnerScreen()Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Component;
.end method

.method public abstract getReaderSdkCrmRunner()Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;
.end method

.method public abstract saveCardCustomerEmailScreen()Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;
.end method

.method public abstract saveCardSpinnerScreen()Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Component;
.end method

.method public abstract saveCardVerifyPostalCodeScreen()Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;
.end method
