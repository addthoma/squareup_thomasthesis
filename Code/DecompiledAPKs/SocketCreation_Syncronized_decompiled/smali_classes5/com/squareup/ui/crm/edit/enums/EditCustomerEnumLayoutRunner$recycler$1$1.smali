.class final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerEnumLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditCustomerEnumLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditCustomerEnumLayoutRunner.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,63:1\n1103#2,7:64\n*E\n*S KotlinDebug\n*F\n+ 1 EditCustomerEnumLayoutRunner.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1\n*L\n42#1,7:64\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "item",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
        "row",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;

    invoke-direct {v0}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;

    check-cast p3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;->invoke(ILcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;Lcom/squareup/noho/NohoCheckableRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 0

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;->getValue()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 40
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;->getChecked()Z

    move-result p1

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 41
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;->getAllowMultiple()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    :goto_0
    check-cast p1, Lcom/squareup/noho/CheckType;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setType(Lcom/squareup/noho/CheckType;)V

    .line 42
    check-cast p3, Landroid/view/View;

    .line 64
    new-instance p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
