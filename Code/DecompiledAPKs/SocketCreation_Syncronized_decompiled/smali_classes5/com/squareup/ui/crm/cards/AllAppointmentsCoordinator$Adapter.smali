.class Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "AllAppointmentsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->access$100(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 108
    check-cast p1, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->onBindViewHolder(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;I)V
    .locals 0

    .line 124
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->bind(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 108
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;
    .locals 0

    .line 119
    invoke-static {p1}, Lcom/squareup/ui/crm/rows/AppointmentRow;->inflateAppointmentRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/crm/rows/AppointmentRow;

    move-result-object p1

    .line 120
    new-instance p2, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;Landroid/view/View;)V

    return-object p2
.end method
