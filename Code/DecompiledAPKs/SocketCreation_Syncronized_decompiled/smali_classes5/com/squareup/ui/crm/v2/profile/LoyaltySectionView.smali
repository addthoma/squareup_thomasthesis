.class public Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;
.super Landroid/widget/LinearLayout;
.source "LoyaltySectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView$Component;
    }
.end annotation


# instance fields
.field private header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private pointsView:Lcom/squareup/marketfont/MarketTextView;

.field presenter:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

.field private rewardTiersAdapterMultiRedemption:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            ">;"
        }
    .end annotation
.end field

.field private rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

.field private rows:Landroid/widget/LinearLayout;

.field private seeAllRewards:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const-class p2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView$Component;->inject(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;)V

    .line 49
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_loyalty_section_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setOrientation(I)V

    .line 51
    sget p2, Lcom/squareup/crmviewcustomer/R$id;->loyalty_section_header:I

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 52
    sget p2, Lcom/squareup/crmviewcustomer/R$id;->loyalty_section_rows:I

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rows:Landroid/widget/LinearLayout;

    .line 53
    sget p2, Lcom/squareup/crmviewcustomer/R$id;->loyalty_section_points:I

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->pointsView:Lcom/squareup/marketfont/MarketTextView;

    .line 54
    sget p2, Lcom/squareup/crmviewcustomer/R$id;->redeem_reward_tiers_list:I

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    .line 55
    sget p2, Lcom/squareup/crmviewcustomer/R$id;->loyalty_section_see_all_rewards:I

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->seeAllRewards:Lcom/squareup/marketfont/MarketTextView;

    .line 57
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->applyMultipleCoupons()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 58
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    .line 59
    invoke-virtual {p2, v0}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->createRecyclerFor(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersAdapterMultiRedemption:Lcom/squareup/cycler/Recycler;

    goto :goto_0

    .line 61
    :cond_0
    new-instance p2, Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-direct {p2}, Lcom/squareup/loyalty/ui/RewardAdapter;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 63
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 65
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 67
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_header_uppercase:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-virtual {p2, v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 69
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->DISABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-void
.end method


# virtual methods
.method addRow()Lcom/squareup/ui/crm/rows/ProfileLineRow;
    .locals 2

    .line 112
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileLineRow;-><init>(Landroid/content/Context;)V

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearRows()V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public disableHeaderAction()V
    .locals 3

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    return-void
.end method

.method public enableHeaderAction()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionToDropDown()V

    return-void
.end method

.method public enableNullStateHeader()V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_null_state:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 75
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->dropView(Ljava/lang/Object;)V

    .line 81
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onHeaderActionClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->onActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onSeeAllRewardsClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->seeAllRewards:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method setPoints(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->pointsView:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->pointsView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 122
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->pointsView:Lcom/squareup/marketfont/MarketTextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method setRewardTiersItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/ui/RewardAdapter;->setItems(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method setRewardTiersMultiRedemption(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            ">;)V"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rewardTiersAdapterMultiRedemption:Lcom/squareup/cycler/Recycler;

    if-eqz v0, :cond_0

    .line 137
    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    :cond_0
    return-void
.end method

.method setSeeAllRewardsVisibility(Z)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->seeAllRewards:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setVisible(Z)V
    .locals 1

    .line 103
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->rows:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method
