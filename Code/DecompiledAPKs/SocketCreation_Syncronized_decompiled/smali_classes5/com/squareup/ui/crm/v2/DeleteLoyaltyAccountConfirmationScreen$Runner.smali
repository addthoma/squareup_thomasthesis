.class public interface abstract Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;
.super Ljava/lang/Object;
.source "DeleteLoyaltyAccountConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelDeleteLoyaltyAccount()V
.end method

.method public abstract deleteLoyaltyAccount()V
.end method

.method public abstract getDeleteLoyaltyAccountConfirmationText()Ljava/lang/String;
.end method
