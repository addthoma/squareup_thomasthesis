.class public Lcom/squareup/ui/crm/cards/CreateGroupScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "CreateGroupScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;,
        Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/CreateGroupScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 209
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$ShabzUNyACrypb8tiOybp-7tah8;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$ShabzUNyACrypb8tiOybp-7tah8;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/CreateGroupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/CreateGroupScreen;
    .locals 1

    .line 210
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 211
    new-instance v0, Lcom/squareup/ui/crm/cards/CreateGroupScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 205
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CREATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 60
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_create_group_view:I

    return v0
.end method
