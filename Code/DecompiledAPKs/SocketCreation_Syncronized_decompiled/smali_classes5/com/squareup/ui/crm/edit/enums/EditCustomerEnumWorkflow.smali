.class public final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditCustomerEnumWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditCustomerEnumWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditCustomerEnumWorkflow.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,68:1\n32#2,12:69\n1360#3:81\n1429#3,3:82\n*E\n*S KotlinDebug\n*F\n+ 1 EditCustomerEnumWorkflow.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow\n*L\n22#1,12:69\n37#1:81\n37#1,3:82\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J$\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00082\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0002H\u0002J\u001a\u0010\u000b\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u00022\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J,\u0010\u000e\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u00032\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u0003H\u0016J,\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00082\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;",
        "()V",
        "closeAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "props",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "updateCheckedAction",
        "value",
        "",
        "checked",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method

.method public static final synthetic access$closeAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->closeAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateCheckedAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Ljava/lang/String;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->updateCheckedAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Ljava/lang/String;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final closeAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;-><init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final updateCheckedAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Ljava/lang/String;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
            ">;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;

    invoke-direct {v0, p1, p3, p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;-><init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;ZLjava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 69
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 74
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 76
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 77
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 78
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 80
    :cond_3
    check-cast v2, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 22
    :cond_4
    new-instance v2, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    .line 23
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;->getSelected()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p2

    .line 24
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;->getAllowMultiple()Z

    move-result p1

    .line 22
    invoke-direct {v2, p2, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;-><init>(Ljava/util/Set;Z)V

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->initialState(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "-",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    .line 37
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;->getOptions()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 81
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 82
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 83
    check-cast v3, Ljava/lang/String;

    .line 38
    new-instance v4, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;

    .line 40
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getSelected()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 41
    invoke-virtual {p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getAllowMultiple()Z

    move-result v6

    .line 42
    new-instance v7, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;

    invoke-direct {v7, v3, p0, p2, p3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;-><init>(Ljava/lang/String;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 38
    invoke-direct {v4, v3, v5, v6, v7}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;-><init>(Ljava/lang/String;ZZLkotlin/jvm/functions/Function1;)V

    .line 45
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :cond_0
    check-cast v2, Ljava/util/List;

    .line 47
    new-instance v1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;-><init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 35
    new-instance p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    invoke-direct {p1, v0, v2, v1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;-><init>(Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    check-cast p2, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->render(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->snapshotState(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
