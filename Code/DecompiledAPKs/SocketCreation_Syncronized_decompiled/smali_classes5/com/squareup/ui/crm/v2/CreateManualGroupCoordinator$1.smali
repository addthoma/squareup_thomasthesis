.class Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "CreateManualGroupCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;

.field final synthetic val$marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$1;->this$0:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$1;->val$marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$1;->val$marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
