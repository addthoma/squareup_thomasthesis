.class public Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;
.super Ljava/lang/Object;
.source "ChooseFiltersFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Result"
.end annotation


# instance fields
.field public final backOut:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field public final filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public final group:Lcom/squareup/protos/client/rolodex/GroupV2;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/rolodex/GroupV2;Lrx/functions/Func1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;)V"
        }
    .end annotation

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    .line 104
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->backOut:Lrx/functions/Func1;

    const/4 p1, 0x0

    .line 105
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->filters:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)V"
        }
    .end annotation

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->filters:Ljava/util/List;

    const/4 p1, 0x0

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->backOut:Lrx/functions/Func1;

    return-void
.end method
