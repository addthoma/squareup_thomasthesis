.class public Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "UpdateLoyaltyPhoneConflictDialogScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Factory;,
        Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;,
        Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 84
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$0nZ7pOvS_P72O5EVas35a4js2mc;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneConflictDialogScreen$0nZ7pOvS_P72O5EVas35a4js2mc;

    .line 85
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;)Ljava/lang/String;
    .locals 0

    .line 23
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;->formatConflictMessage(Ljava/lang/String;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static formatConflictMessage(Ljava/lang/String;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;)Ljava/lang/String;
    .locals 1

    .line 79
    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_conflict_message:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 80
    invoke-interface {p2, p0}, Lcom/squareup/text/PhoneNumberHelper;->formatPartial(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string p2, "number"

    invoke-virtual {p1, p2, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 81
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;
    .locals 1

    .line 86
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 87
    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
