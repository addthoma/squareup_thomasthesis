.class public Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;
.super Landroid/widget/LinearLayout;
.source "MergeCustomersConfirmationView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private contactList:Landroid/widget/LinearLayout;

.field private message:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-class p2, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Component;->inject(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;)V

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method addCustomerRow()Lcom/squareup/ui/crm/rows/ChooseCustomerRow;
    .locals 2

    .line 68
    sget v0, Lcom/squareup/crm/R$layout;->crm_recent_customer_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->contactList:Landroid/widget/LinearLayout;

    .line 69
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;

    .line 70
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->contactList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method hideProgress()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->presenter:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->presenter:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 36
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 37
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->message:Lcom/squareup/widgets/MessageView;

    .line 38
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_contact_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->contactList:Landroid/widget/LinearLayout;

    .line 39
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->progressBar:Landroid/widget/ProgressBar;

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    sget v1, Lcom/squareup/crmviewcustomer/R$id;->non_stable_action_bar:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/ActionBarView;->setId(I)V

    return-void
.end method

.method showMessage(Ljava/lang/String;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
