.class public final Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;
.super Ljava/lang/Object;
.source "UpdateCustomerScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseGroupsFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeContactAddressBookProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexGroupLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final updateDateFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->permissionsPresenterProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->chooseGroupsFlowProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->updateDateFlowProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p10, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p11, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p12, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->maybeContactAddressBookProvider:Ljavax/inject/Provider;

    .line 85
    iput-object p13, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->rolodexGroupLoaderProvider:Ljavax/inject/Provider;

    .line 86
    iput-object p14, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    .line 87
    iput-object p15, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;"
        }
    .end annotation

    .line 108
    new-instance v16, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;Lcom/squareup/util/Res;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;
    .locals 17

    .line 118
    new-instance v16, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;Lcom/squareup/util/Res;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;
    .locals 17

    move-object/from16 v0, p0

    .line 92
    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->permissionsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->chooseGroupsFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->updateDateFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->maybeContactAddressBookProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->rolodexGroupLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;Lcom/squareup/util/Res;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner_Factory;->get()Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    move-result-object v0

    return-object v0
.end method
