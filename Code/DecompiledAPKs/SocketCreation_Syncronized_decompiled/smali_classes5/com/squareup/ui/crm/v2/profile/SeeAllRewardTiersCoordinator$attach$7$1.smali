.class final Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SeeAllRewardTiersCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->call(Ljava/lang/Integer;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/loyalty/RewardTier;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "rewardTier",
        "Lcom/squareup/protos/client/loyalty/RewardTier;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7$1;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/protos/client/loyalty/RewardTier;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7$1;->invoke(Lcom/squareup/protos/client/loyalty/RewardTier;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/loyalty/RewardTier;)V
    .locals 1

    const-string v0, "rewardTier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7$1;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;->rewardTierClicked(Lcom/squareup/protos/client/loyalty/RewardTier;)V

    return-void
.end method
