.class public final Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;
.super Ljava/lang/Object;
.source "RewardRecyclerViewHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRewardRecyclerViewHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RewardRecyclerViewHelper.kt\ncom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,132:1\n1360#2:133\n1429#2,2:134\n1360#2:136\n1429#2,3:137\n1431#2:140\n49#3:141\n50#3,3:147\n53#3:164\n599#4,4:142\n601#4:146\n310#5,3:150\n313#5,3:159\n35#6,6:153\n43#7,2:162\n*E\n*S KotlinDebug\n*F\n+ 1 RewardRecyclerViewHelper.kt\ncom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper\n*L\n43#1:133\n43#1,2:134\n43#1:136\n43#1,3:137\n43#1:140\n61#1:141\n61#1,3:147\n61#1:164\n61#1,4:142\n61#1:146\n61#1,3:150\n61#1,3:159\n61#1,6:153\n61#1,2:162\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rJP\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0017\u0010\u0014\u001a\u0013\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00160\u0015\u00a2\u0006\u0002\u0008\u00172\u0017\u0010\u0018\u001a\u0013\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00160\u0015\u00a2\u0006\u0002\u0008\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/recycler/RecyclerFactory;)V",
        "createRecyclerFor",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "createRowData",
        "",
        "spendablePoints",
        "",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "onRewardRowRemoved",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "onRewardRowRedeemed",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final createRecyclerFor(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            ">;"
        }
    .end annotation

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 141
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 142
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 143
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 147
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 148
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 151
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 63
    sget v2, Lcom/squareup/crmscreens/R$layout;->compact_loyalty_reward_row:I

    .line 153
    new-instance v3, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1;-><init>(I)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 151
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 150
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 162
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0x8

    .line 115
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 162
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 145
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 142
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final createRowData(ILcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/checkout/HoldsCoupons;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "onRewardRowRemoved"

    move-object/from16 v11, p3

    invoke-static {v11, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onRewardRowRedeemed"

    move-object/from16 v12, p4

    invoke-static {v12, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v2, v0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_8

    check-cast v2, Ljava/lang/Iterable;

    .line 133
    new-instance v3, Ljava/util/ArrayList;

    const/16 v14, 0xa

    invoke-static {v2, v14}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object v15, v3

    check-cast v15, Ljava/util/Collection;

    .line 134
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 135
    move-object v4, v3

    check-cast v4, Lcom/squareup/server/account/protos/RewardTier;

    const-string/jumbo v3, "tier"

    .line 45
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v5, v4, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    if-nez v5, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string/jumbo v3, "tier.name!!"

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v3, v0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v6, v4, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const-string/jumbo v7, "tier.points"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/squareup/loyalty/PointsTermsFormatter;->pointsWithSuffix(J)Ljava/lang/String;

    move-result-object v6

    if-eqz v1, :cond_4

    .line 48
    iget-object v3, v4, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v1, v3}, Lcom/squareup/checkout/HoldsCoupons;->getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    check-cast v3, Ljava/lang/Iterable;

    .line 136
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v3, v14}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 137
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 138
    check-cast v8, Lcom/squareup/checkout/Discount;

    .line 49
    invoke-virtual {v8}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 139
    :cond_3
    move-object v3, v7

    check-cast v3, Ljava/util/List;

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_5

    goto :goto_3

    .line 50
    :cond_5
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    :goto_3
    move-object v7, v3

    .line 51
    iget-object v3, v4, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move/from16 v10, p1

    int-to-long v13, v10

    cmp-long v3, v8, v13

    if-gtz v3, :cond_6

    if-eqz v1, :cond_6

    const/4 v3, 0x1

    const/4 v8, 0x1

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 44
    :goto_4
    new-instance v13, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    move-object v3, v13

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;-><init>(Lcom/squareup/server/account/protos/RewardTier;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 54
    invoke-interface {v15, v13}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const/16 v14, 0xa

    goto/16 :goto_0

    .line 140
    :cond_7
    move-object v13, v15

    check-cast v13, Ljava/util/List;

    goto :goto_5

    :cond_8
    const/4 v13, 0x0

    :goto_5
    if-eqz v13, :cond_9

    goto :goto_6

    .line 56
    :cond_9
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v13

    :goto_6
    return-object v13
.end method
