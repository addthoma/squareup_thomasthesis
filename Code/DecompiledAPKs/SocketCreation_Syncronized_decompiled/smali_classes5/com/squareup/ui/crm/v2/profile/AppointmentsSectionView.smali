.class public Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;
.super Landroid/widget/LinearLayout;
.source "AppointmentsSectionView.java"


# instance fields
.field private final appointmentClicked:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private final onViewAllClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rows:Landroid/widget/LinearLayout;

.field private final sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private final viewAllButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_profile_rows_and_button_section:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 37
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->setOrientation(I)V

    .line 39
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->profile_section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 40
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->profile_section_rows:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->rows:Landroid/widget/LinearLayout;

    .line 41
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->view_all_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->viewAllButton:Landroid/widget/Button;

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->viewAllButton:Landroid/widget/Button;

    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->onViewAllClicked:Lio/reactivex/Observable;

    .line 44
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->appointmentClicked:Lio/reactivex/subjects/PublishSubject;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;)Lio/reactivex/subjects/PublishSubject;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->appointmentClicked:Lio/reactivex/subjects/PublishSubject;

    return-object p0
.end method

.method private getActionButtonState(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/ui/crm/flow/CrmScopeType;)Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;
    .locals 1

    .line 109
    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p2, v0, :cond_0

    iget-boolean p1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->actionButtonEnabled:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public getAppointmentClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->appointmentClicked:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public getOnViewAllClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->onViewAllClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getSectionHeaderActionClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->onActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/ui/crm/flow/CrmScopeType;)V
    .locals 5

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_book_appointment:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->getActionButtonState(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/ui/crm/flow/CrmScopeType;)Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    move-result-object p2

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v1, v0, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 68
    iget-boolean p2, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->isEnabled:Z

    const/16 v0, 0x8

    if-eqz p2, :cond_3

    const/4 p2, 0x0

    .line 69
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->setVisibility(I)V

    .line 73
    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->appointments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-void

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    .line 82
    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->appointments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    .line 83
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-static {v3}, Lcom/squareup/ui/crm/rows/AppointmentRow;->inflateAppointmentRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/crm/rows/AppointmentRow;

    move-result-object v3

    .line 84
    invoke-virtual {v3, v2}, Lcom/squareup/ui/crm/rows/AppointmentRow;->setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V

    .line 86
    new-instance v4, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;

    invoke-direct {v4, p0, v2}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;-><init>(Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V

    invoke-virtual {v3, v4}, Lcom/squareup/ui/crm/rows/AppointmentRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->viewAllButton:Landroid/widget/Button;

    iget-boolean p1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->viewAllButtonEnabled:Z

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/16 p2, 0x8

    :goto_1
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 97
    :cond_3
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->setVisibility(I)V

    :goto_2
    return-void
.end method
