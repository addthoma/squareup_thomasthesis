.class Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$4;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "AbstractCrmScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showUpdateLoyaltyPhoneScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V
    .locals 0

    .line 1115
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$4;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 3

    .line 1117
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$4;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$4;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v2, v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
