.class public interface abstract Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;
.super Ljava/lang/Object;
.source "ChooseGroupsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelChooseGroupsScreen()V
.end method

.method public abstract clearJustSavedGroupForChooseGroupsScreen()V
.end method

.method public abstract closeChooseGroupsScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract getContactForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getJustSavedGroupForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Group;
.end method

.method public abstract setContactForChooseGroupsScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract showCreateGroupScreen()V
.end method
