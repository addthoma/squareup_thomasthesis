.class public interface abstract Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;
.super Ljava/lang/Object;
.source "ProfileAttachmentsScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract profileAttachmentsCoordinator()Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;
.end method

.method public abstract profileAttachmentsPreviewCoordinator()Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;
.end method

.method public abstract profileAttachmentsUploadCoordinator()Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;
.end method
