.class public interface abstract Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;
.super Ljava/lang/Object;
.source "RemovingCardOnFileScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeRemovingCardOnFileScreen()V
.end method

.method public abstract getContactTokenForUnlinkInstrumentDialog()Ljava/lang/String;
.end method

.method public abstract getInstrumentTokenForUnlinkInstrumentDialog()Ljava/lang/String;
.end method

.method public abstract successRemoveCardOnFile()V
.end method
