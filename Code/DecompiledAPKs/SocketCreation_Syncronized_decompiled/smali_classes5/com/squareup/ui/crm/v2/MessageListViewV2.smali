.class public Lcom/squareup/ui/crm/v2/MessageListViewV2;
.super Landroid/widget/LinearLayout;
.source "MessageListViewV2.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private conversationList:Landroid/widget/ListView;

.field private emptyListWarning:Landroid/view/View;

.field private final endOfListIndex:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private listViewAdapter:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

.field presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final selectConversationListItem:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->endOfListIndex:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->selectConversationListItem:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 46
    const-class p2, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Component;->inject(Lcom/squareup/ui/crm/v2/MessageListViewV2;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/MessageListViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->selectConversationListItem:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method


# virtual methods
.method endOfListIndex()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->endOfListIndex:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 61
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->takeView(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->conversationList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->dropView(Ljava/lang/Object;)V

    .line 68
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 50
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 52
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 53
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_messages_list_empty_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->emptyListWarning:Landroid/view/View;

    .line 54
    sget v0, Lcom/squareup/crm/applet/R$id;->customers_applet_customer_messages_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->conversationList:Landroid/widget/ListView;

    .line 56
    new-instance v0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;-><init>(Lcom/squareup/ui/crm/v2/MessageListViewV2;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->listViewAdapter:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->conversationList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->listViewAdapter:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->endOfListIndex:Lcom/jakewharton/rxrelay/PublishRelay;

    add-int/2addr p2, p3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method

.method refresh()V
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->listViewAdapter:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->notifyDataSetChanged()V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->presenter:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->getConversations()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->conversationList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->emptyListWarning:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->conversationList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->emptyListWarning:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method selectConversationListItem()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->selectConversationListItem:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
