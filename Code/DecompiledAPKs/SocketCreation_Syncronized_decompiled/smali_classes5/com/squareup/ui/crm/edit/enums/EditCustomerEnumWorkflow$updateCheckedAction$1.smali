.class final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerEnumWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->updateCheckedAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Ljava/lang/String;Z)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
        "-",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $checked:Z

.field final synthetic $state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

.field final synthetic $value:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    iput-boolean p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$checked:Z

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$value:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "-",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    .line 55
    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getAllowMultiple()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$checked:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$value:Ljava/lang/String;

    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    goto :goto_0

    .line 56
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$checked:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getSelected()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$value:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getSelected()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$value:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/collections/SetsKt;->minus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    goto :goto_0

    .line 58
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$updateCheckedAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getSelected()Ljava/util/Set;

    move-result-object v1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 53
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->copy$default(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Ljava/util/Set;ZILjava/lang/Object;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
