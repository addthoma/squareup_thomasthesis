.class public Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ChooseCustomer3ScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Component;,
        Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;,
        Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public isFirstScreen:Z

.field private final parentPath:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 63
    iput-boolean p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;->isFirstScreen:Z

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 83
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;->isFirstScreen:Z

    if-eqz v0, :cond_1

    .line 84
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 75
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_choose_customer3_view:I

    return v0
.end method
