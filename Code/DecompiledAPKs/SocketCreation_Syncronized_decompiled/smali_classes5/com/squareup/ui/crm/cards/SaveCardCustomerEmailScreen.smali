.class public Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "SaveCardCustomerEmailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;,
        Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Module;,
        Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 150
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$pSpHUQX1NU4H0-lF0qPX9wxCIOg;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$pSpHUQX1NU4H0-lF0qPX9wxCIOg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;
    .locals 1

    .line 151
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 152
    new-instance v0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 146
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_CUSTOMER_EMAIL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 48
    sget v0, Lcom/squareup/ui/crm/R$layout;->crm_customer_email_view:I

    return v0
.end method
