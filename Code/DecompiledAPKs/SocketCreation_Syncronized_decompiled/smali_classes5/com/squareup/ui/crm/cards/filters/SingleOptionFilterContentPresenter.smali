.class public Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;
.super Lmortar/ViewPresenter;
.source "SingleOptionFilterContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;",
        ">;"
    }
.end annotation


# instance fields
.field private final filter:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private final helper:Lcom/squareup/crm/filters/SingleOptionFilterHelper;


# direct methods
.method constructor <init>(Lcom/squareup/crm/filters/SingleOptionFilterHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 24
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/SingleOptionFilterHelper;

    return-void
.end method

.method private rebind(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)V
    .locals 6

    .line 54
    invoke-static {p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    .line 58
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 60
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/SingleOptionFilterHelper;

    invoke-virtual {v2, v0}, Lcom/squareup/crm/filters/SingleOptionFilterHelper;->getAvailableOptions(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 61
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->addRow()Lcom/squareup/ui/crm/rows/CheckableRow;

    move-result-object v4

    .line 62
    iget-object v5, v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/squareup/ui/crm/rows/CheckableRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v5, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/SingleOptionFilterHelper;

    invoke-virtual {v5, v0, v3}, Lcom/squareup/crm/filters/SingleOptionFilterHelper;->isSelected(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    .line 65
    invoke-virtual {v4, v5}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 66
    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 69
    :cond_0
    new-instance v5, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleOptionFilterContentPresenter$iWipt58k59Ni1071sEkum1zBHDI;

    invoke-direct {v5, p0, v4, v1, v3}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleOptionFilterContentPresenter$iWipt58k59Ni1071sEkum1zBHDI;-><init>(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)V

    invoke-static {v4, v5}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method isValid()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/SingleOptionFilterHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$H3X-ugsKAP5qkp5qMzmCmmPgjGo;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$H3X-ugsKAP5qkp5qMzmCmmPgjGo;-><init>(Lcom/squareup/crm/filters/SingleOptionFilterHelper;)V

    .line 49
    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$SingleOptionFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;Lkotlin/Unit;)V
    .locals 1

    .line 72
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->isChecked()Z

    move-result p4

    if-nez p4, :cond_1

    .line 73
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p4

    if-eqz p4, :cond_0

    .line 74
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/ui/crm/rows/CheckableRow;

    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    :cond_0
    const/4 p4, 0x1

    .line 76
    invoke-virtual {p1, p4}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 77
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/SingleOptionFilterHelper;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p2, p4, p3}, Lcom/squareup/crm/filters/SingleOptionFilterHelper;->select(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$rebind$1$SingleOptionFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lrx/Subscription;
    .locals 2

    .line 70
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->onClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleOptionFilterContentPresenter$qTFF9-YaXARwUyOAwas4lf-9n5o;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleOptionFilterContentPresenter$qTFF9-YaXARwUyOAwas4lf-9n5o;-><init>(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)V

    .line 71
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 32
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->rebind(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)V

    return-void
.end method

.method setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->rebind(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)V

    :cond_0
    return-void
.end method
