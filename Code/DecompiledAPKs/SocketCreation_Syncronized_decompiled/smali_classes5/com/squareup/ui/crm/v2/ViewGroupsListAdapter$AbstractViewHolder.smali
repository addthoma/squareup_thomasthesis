.class abstract Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ViewGroupsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "AbstractViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;ILandroid/view/ViewGroup;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    .line 37
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method abstract bind(I)V
.end method
