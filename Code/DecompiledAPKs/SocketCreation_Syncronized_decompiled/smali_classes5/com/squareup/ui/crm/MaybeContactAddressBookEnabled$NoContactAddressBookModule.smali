.class public abstract Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$NoContactAddressBookModule;
.super Ljava/lang/Object;
.source "MaybeContactAddressBookEnabled.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NoContactAddressBookModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract contactAddressBookEnabled(Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$NoContactAddressBook;)Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
