.class Lcom/squareup/ui/crm/ConversationPresenter;
.super Lmortar/ViewPresenter;
.source "ConversationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/ConversationView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_UNIQUE_KEY:Ljava/lang/String; = "uniqueKey"


# instance fields
.field private final addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bill:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final billListService:Lcom/squareup/server/bills/BillListServiceHelper;

.field private final busy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/dialogue/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private final conversationToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final dialogue:Lcom/squareup/crm/DialogueServiceHelper;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final mediumDateFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private sendMessageDisposable:Lio/reactivex/disposables/Disposable;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatter:Ljava/text/DateFormat;

.field private uniqueKey:Ljava/util/UUID;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/coupon/AddCouponState;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Ljava/util/Locale;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/Features;Ljava/text/DateFormat;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Ljava/util/Locale;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            "Lcom/squareup/settings/server/Features;",
            "Ljava/text/DateFormat;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 106
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 92
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversationToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x0

    .line 94
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 95
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->bill:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 96
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 97
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 108
    iput-object p2, p0, Lcom/squareup/ui/crm/ConversationPresenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    .line 109
    iput-object p5, p0, Lcom/squareup/ui/crm/ConversationPresenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    .line 110
    iput-object p6, p0, Lcom/squareup/ui/crm/ConversationPresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 111
    iput-object p8, p0, Lcom/squareup/ui/crm/ConversationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 112
    iput-object p9, p0, Lcom/squareup/ui/crm/ConversationPresenter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 113
    iput-object p10, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    .line 114
    iput-object p3, p0, Lcom/squareup/ui/crm/ConversationPresenter;->dateFormatter:Ljava/text/DateFormat;

    .line 115
    iput-object p4, p0, Lcom/squareup/ui/crm/ConversationPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 116
    iput-object p7, p0, Lcom/squareup/ui/crm/ConversationPresenter;->locale:Ljava/util/Locale;

    .line 117
    iput-object p11, p0, Lcom/squareup/ui/crm/ConversationPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 118
    iput-object p12, p0, Lcom/squareup/ui/crm/ConversationPresenter;->billListService:Lcom/squareup/server/bills/BillListServiceHelper;

    .line 119
    iput-object p13, p0, Lcom/squareup/ui/crm/ConversationPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 120
    iput-object p14, p0, Lcom/squareup/ui/crm/ConversationPresenter;->mediumDateFormatter:Ljava/text/DateFormat;

    const/4 p1, 0x1

    .line 122
    invoke-virtual {p6, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method private bindCouponRow(Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 0

    .line 355
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;->showMessage(Ljava/lang/String;)V

    .line 356
    invoke-direct {p0, p3}, Lcom/squareup/ui/crm/ConversationPresenter;->formatCreatorTimestamp(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;->showCreatorTimestamp(Ljava/lang/String;)V

    return-void
.end method

.method private bindFeedbackRowWithNoComment(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 2

    .line 420
    sget-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-ne p2, v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_no_comment_positive_feedback:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/ui/crm/ConversationPresenter;->bindSentimentRow(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_0

    .line 423
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->NEGATIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-ne p2, v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_no_comment_negative_feedback:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 425
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/ui/crm/ConversationPresenter;->bindSentimentRow(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private bindLeftRow(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Comment;)V
    .locals 1

    .line 345
    iget-object v0, p2, Lcom/squareup/protos/client/dialogue/Comment;->content:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->showMessage(Ljava/lang/String;)V

    .line 346
    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/Comment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/ConversationPresenter;->formatCreatorTimestamp(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->showCreatorTimestamp(Ljava/lang/String;)V

    return-void
.end method

.method private bindRightRow(Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 0

    .line 350
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;->showMessage(Ljava/lang/String;)V

    .line 351
    invoke-direct {p0, p3}, Lcom/squareup/ui/crm/ConversationPresenter;->formatCreatorTimestamp(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;->showCreatorTimestamp(Ljava/lang/String;)V

    return-void
.end method

.method private bindSentimentRow(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 0

    .line 361
    invoke-virtual {p1, p3}, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->showMessage(Ljava/lang/String;)V

    .line 362
    invoke-direct {p0, p2, p4}, Lcom/squareup/ui/crm/ConversationPresenter;->formatCreatorTimestampWithSentiment(Lcom/squareup/protos/client/dialogue/Sentiment;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;->showCreatorTimestamp(Ljava/lang/String;)V

    return-void
.end method

.method private convertServerResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 454
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    .line 453
    invoke-static {p1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method private static emptyCreateCouponResponseWithConversationToken(Ljava/lang/String;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "Lcom/squareup/protos/client/dialogue/CreateCouponResponse;",
            ">;"
        }
    .end annotation

    .line 328
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    new-instance v1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/dialogue/Conversation$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/dialogue/Conversation$Builder;-><init>()V

    .line 329
    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Conversation$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->build()Lcom/squareup/protos/client/dialogue/Conversation;

    move-result-object p0

    .line 328
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->conversation(Lcom/squareup/protos/client/dialogue/Conversation;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    move-result-object p0

    .line 329
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private formatCreatorTimestamp(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 3

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->mediumDateFormatter:Ljava/text/DateFormat;

    .line 441
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 442
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 443
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatCreatorTimestampWithSentiment(Lcom/squareup/protos/client/dialogue/Sentiment;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 2

    .line 430
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    .line 431
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget-object v1, Lcom/squareup/protos/client/dialogue/Sentiment;->POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-ne p1, v1, :cond_0

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_comment_positive_feedback:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/crmscreens/R$string;->crm_comment_negative_feedback:I

    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->mediumDateFormatter:Ljava/text/DateFormat;

    .line 433
    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 434
    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    const-string/jumbo v0, "time"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 435
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$10(Lcom/squareup/protos/client/dialogue/Conversation;)Ljava/lang/Boolean;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$null$11(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 191
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$12(Lcom/squareup/ui/crm/ConversationView;Ljava/lang/Boolean;)V
    .locals 0

    .line 194
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 195
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ConversationView;->enableMessageEdit(Z)V

    return-void
.end method

.method static synthetic lambda$null$14(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 203
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$15(Lcom/squareup/ui/crm/ConversationView;Ljava/lang/Boolean;)V
    .locals 0

    .line 206
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 207
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ConversationView;->enableSendMessageButton(Z)V

    return-void
.end method

.method static synthetic lambda$null$17(Lkotlin/Unit;Lcom/squareup/api/items/Discount;)Lcom/squareup/api/items/Discount;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$null$3(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    instance-of p0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    return p0
.end method

.method static synthetic lambda$null$8(Lcom/squareup/ui/crm/ConversationView;Ljava/lang/Boolean;)V
    .locals 0

    .line 181
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 182
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ConversationView;->showProgress(Z)V

    return-void
.end method

.method static synthetic lambda$onBillHistoryClicked$26(Lkotlin/Unit;Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 0

    return-object p1
.end method

.method private onSendPressed(Lcom/squareup/ui/crm/ConversationView;Ljava/lang/String;Lcom/squareup/api/items/Discount;)V
    .locals 2

    .line 282
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversationToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$H0aRsZ6e8EoUQobvZv5FhNyv_1o;

    invoke-direct {v1, p0, p3}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$H0aRsZ6e8EoUQobvZv5FhNyv_1o;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/api/items/Discount;)V

    .line 288
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p3

    new-instance v0, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$Sy9ZSoY6naM7FzgMym4WS4XpCks;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$Sy9ZSoY6naM7FzgMym4WS4XpCks;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Ljava/lang/String;)V

    .line 296
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance p3, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$QI5crchb1xPS-NjCx-C65ysbvEg;

    invoke-direct {p3, p0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$QI5crchb1xPS-NjCx-C65ysbvEg;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;)V

    .line 306
    invoke-virtual {p2, p3}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance p3, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$-AtWw-toFEy15xPBk6vHUygbR10;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$-AtWw-toFEy15xPBk6vHUygbR10;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    .line 307
    invoke-virtual {p2, p3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private showConversation(Lcom/squareup/ui/crm/ConversationView;Lcom/squareup/protos/client/dialogue/Conversation;)V
    .locals 5

    .line 366
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->clearRows()V

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->showAddCouponButton()V

    .line 375
    :cond_0
    iget-object v0, p2, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 377
    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 378
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->addLeftRow()Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;

    move-result-object v1

    iget-object v2, p2, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v1, v0, v2}, Lcom/squareup/ui/crm/ConversationPresenter;->bindFeedbackRowWithNoComment(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Lcom/squareup/protos/common/time/DateTime;)V

    .line 380
    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_8

    .line 381
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->showAddCouponButton()V

    goto/16 :goto_2

    .line 384
    :cond_1
    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    .line 387
    sget-object v2, Lcom/squareup/protos/client/dialogue/Sentiment;->POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/dialogue/Sentiment;->NEGATIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-ne v0, v2, :cond_4

    :cond_2
    const/4 v2, 0x0

    .line 388
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/dialogue/Message;

    .line 389
    iget-object v3, v2, Lcom/squareup/protos/client/dialogue/Message;->type:Lcom/squareup/protos/client/dialogue/Message$Type;

    sget-object v4, Lcom/squareup/protos/client/dialogue/Message$Type;->COMMENT:Lcom/squareup/protos/client/dialogue/Message$Type;

    if-ne v3, v4, :cond_3

    iget-object v3, v2, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v3, v3, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v3, v3, Lcom/squareup/protos/client/dialogue/Comment;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    sget-object v4, Lcom/squareup/protos/client/dialogue/Commenter;->BUYER:Lcom/squareup/protos/client/dialogue/Commenter;

    .line 390
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/dialogue/Commenter;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 391
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->addLeftRow()Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;

    move-result-object v3

    iget-object v4, v2, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v4, v4, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v4, v4, Lcom/squareup/protos/client/dialogue/Comment;->content:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/protos/client/dialogue/Message;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v3, v0, v4, v2}, Lcom/squareup/ui/crm/ConversationPresenter;->bindSentimentRow(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V

    const/4 v0, 0x1

    .line 393
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 395
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->addLeftRow()Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;

    move-result-object v2

    iget-object v3, p2, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v2, v0, v3}, Lcom/squareup/ui/crm/ConversationPresenter;->bindFeedbackRowWithNoComment(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Sentiment;Lcom/squareup/protos/common/time/DateTime;)V

    .line 399
    :goto_0
    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 400
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->showAddCouponButton()V

    .line 404
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_5
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/dialogue/Message;

    .line 405
    iget-object v1, v0, Lcom/squareup/protos/client/dialogue/Message;->type:Lcom/squareup/protos/client/dialogue/Message$Type;

    sget-object v2, Lcom/squareup/protos/client/dialogue/Message$Type;->COMMENT:Lcom/squareup/protos/client/dialogue/Message$Type;

    if-ne v1, v2, :cond_6

    iget-object v1, v0, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v1, v1, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v1, v1, Lcom/squareup/protos/client/dialogue/Comment;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    sget-object v2, Lcom/squareup/protos/client/dialogue/Commenter;->BUYER:Lcom/squareup/protos/client/dialogue/Commenter;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/dialogue/Commenter;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 406
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->addLeftRow()Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/ConversationPresenter;->bindLeftRow(Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;Lcom/squareup/protos/client/dialogue/Comment;)V

    goto :goto_1

    .line 407
    :cond_6
    iget-object v1, v0, Lcom/squareup/protos/client/dialogue/Message;->type:Lcom/squareup/protos/client/dialogue/Message$Type;

    sget-object v2, Lcom/squareup/protos/client/dialogue/Message$Type;->COMMENT:Lcom/squareup/protos/client/dialogue/Message$Type;

    if-ne v1, v2, :cond_7

    iget-object v1, v0, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v1, v1, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v1, v1, Lcom/squareup/protos/client/dialogue/Comment;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    sget-object v2, Lcom/squareup/protos/client/dialogue/Commenter;->MERCHANT:Lcom/squareup/protos/client/dialogue/Commenter;

    .line 408
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/dialogue/Commenter;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 409
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->addRightRow()Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v2, v2, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v2, v2, Lcom/squareup/protos/client/dialogue/Comment;->content:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Comment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/crm/ConversationPresenter;->bindRightRow(Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_1

    .line 411
    :cond_7
    iget-object v1, v0, Lcom/squareup/protos/client/dialogue/Message;->type:Lcom/squareup/protos/client/dialogue/Message$Type;

    sget-object v2, Lcom/squareup/protos/client/dialogue/Message$Type;->COUPON:Lcom/squareup/protos/client/dialogue/Message$Type;

    if-ne v1, v2, :cond_5

    .line 412
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->addCouponRow()Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/protos/client/dialogue/Message;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v2, v2, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object v2, v2, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Message;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/crm/ConversationPresenter;->bindCouponRow(Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_1

    :cond_8
    :goto_2
    return-void
.end method

.method private showLoadConversationError()V
    .locals 3

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_conversation_loading_error:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showNoResponsesError()V
    .locals 3

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_conversation_no_response_error:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showSendMessageError()V
    .locals 3

    .line 333
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_send_message_error:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method busy()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 462
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method conversation()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/dialogue/Conversation;",
            ">;"
        }
    .end annotation

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public synthetic lambda$null$18$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;Lcom/squareup/api/items/Discount;)V
    .locals 1

    .line 218
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 219
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/crm/ConversationPresenter;->onSendPressed(Lcom/squareup/ui/crm/ConversationView;Ljava/lang/String;Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method public synthetic lambda$null$20$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;Lkotlin/Unit;)V
    .locals 1

    .line 226
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 227
    iget-object p2, p0, Lcom/squareup/ui/crm/ConversationPresenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 228
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->hideCouponRow()V

    return-void
.end method

.method public synthetic lambda$null$22$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;Lcom/squareup/api/items/Discount;)V
    .locals 2

    .line 235
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 236
    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    if-eq p2, v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_add_coupon_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 238
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "amount"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 239
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 237
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/ConversationView;->showCouponRow(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 241
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->hideCouponRow()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$24$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/crm/ConversationPresenter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/crm/ConversationPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 250
    invoke-static {p3, v0, v1, v2, v3}, Lcom/squareup/billhistory/Bills;->formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p3

    .line 251
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    .line 249
    invoke-virtual {p1, p3, p2}, Lcom/squareup/ui/crm/ConversationView;->showBillHistoryButton(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$null$4$ConversationPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 153
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/ConversationPresenter;->convertServerResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lcom/squareup/util/Optional;

    move-result-object p1

    goto :goto_0

    .line 156
    :cond_0
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public synthetic lambda$null$6$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;Lcom/squareup/protos/client/dialogue/Conversation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 170
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 171
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/ConversationPresenter;->showConversation(Lcom/squareup/ui/crm/ConversationView;Lcom/squareup/protos/client/dialogue/Conversation;)V

    .line 172
    iget-object p1, p2, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 173
    invoke-direct {p0}, Lcom/squareup/ui/crm/ConversationPresenter;->showNoResponsesError()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$ConversationPresenter(Ljava/lang/String;)Lio/reactivex/ObservableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/DialogueServiceHelper;->getConversation(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$ConversationPresenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$ConversationPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 136
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 139
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/ConversationPresenter;->showLoadConversationError()V

    .line 141
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$5$ConversationPresenter(Lcom/squareup/protos/client/dialogue/Conversation;)Lio/reactivex/MaybeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 148
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->billListService:Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/bills/BillListServiceHelper;->getBill(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$F7cMEuNKMr2ffPhoGXyHWb8Qi5M;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$F7cMEuNKMr2ffPhoGXyHWb8Qi5M;

    .line 152
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$zKVNez4tkpYcxgMDTV1agAy1ohY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$zKVNez4tkpYcxgMDTV1agAy1ohY;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;)V

    .line 153
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$13$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lrx/Subscription;
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 189
    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$D2Ct46F1q9ZHd3U8fDEyZgcJ9sg;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$D2Ct46F1q9ZHd3U8fDEyZgcJ9sg;

    .line 190
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$bS73-wjYXppvwMJB0B3ooanaFQE;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$bS73-wjYXppvwMJB0B3ooanaFQE;

    .line 187
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$FBsgHJFtm_Fw9lViTgS6p63shZ8;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$FBsgHJFtm_Fw9lViTgS6p63shZ8;-><init>(Lcom/squareup/ui/crm/ConversationView;)V

    .line 193
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$16$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lrx/Subscription;
    .locals 3

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 202
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->messageIsBlank()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$h8nR5e4ptZ2OHCqqfBPMkf32fUk;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$h8nR5e4ptZ2OHCqqfBPMkf32fUk;

    .line 200
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 204
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$pN36aqXM9o-2T1HTd-cFd8QqLas;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$pN36aqXM9o-2T1HTd-cFd8QqLas;-><init>(Lcom/squareup/ui/crm/ConversationView;)V

    .line 205
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$19$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lrx/Subscription;
    .locals 3

    .line 212
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->onSendMessageClicked()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    .line 214
    invoke-virtual {v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount()Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    check-cast v2, Lcom/squareup/api/items/Discount;

    .line 215
    invoke-virtual {v1, v2}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$sNC3Yezmr0stz7wMVhppoLqpTKg;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$sNC3Yezmr0stz7wMVhppoLqpTKg;

    .line 213
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$SRtlQ6V11mrAHgxPw2zGrk5U9V8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$SRtlQ6V11mrAHgxPw2zGrk5U9V8;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    .line 217
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$21$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lrx/Subscription;
    .locals 2

    .line 224
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->onRemoveCouponClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$StlNAbiOOJssqsFb_uJTlckzx8w;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$StlNAbiOOJssqsFb_uJTlckzx8w;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    .line 225
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$23$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lrx/Subscription;
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$aqr3Up-LMnAGoQ998d7Ax8Q2_Oc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$aqr3Up-LMnAGoQ998d7Ax8Q2_Oc;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    .line 234
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$25$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 247
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 248
    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->bill:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v2, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$ZPqYhMIP5c86ib9gEdvEoXooXrY;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$ZPqYhMIP5c86ib9gEdvEoXooXrY;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 249
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v1, 0x1

    .line 252
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$ybKCaf95CXTW1skcHeYqYM-5EzM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$ybKCaf95CXTW1skcHeYqYM-5EzM;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    .line 169
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;)Lrx/Subscription;
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$ibAUmBeX4cTL-XwLET3-TwnJsyM;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$ibAUmBeX4cTL-XwLET3-TwnJsyM;-><init>(Lcom/squareup/ui/crm/ConversationView;)V

    .line 180
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSendPressed$27$ConversationPresenter(Lcom/squareup/api/items/Discount;Ljava/lang/String;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 289
    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/Discount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0, p2, p1, v1}, Lcom/squareup/crm/DialogueServiceHelper;->createCoupon(Ljava/lang/String;Lcom/squareup/api/items/Discount;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 291
    :cond_1
    :goto_0
    invoke-static {p2}, Lcom/squareup/ui/crm/ConversationPresenter;->emptyCreateCouponResponseWithConversationToken(Ljava/lang/String;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    move-result-object p1

    .line 290
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSendPressed$28$ConversationPresenter(Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 297
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0, p2, p1, v1}, Lcom/squareup/crm/DialogueServiceHelper;->createComment(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p1

    .line 299
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 301
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 302
    invoke-direct {p0}, Lcom/squareup/ui/crm/ConversationPresenter;->showSendMessageError()V

    .line 303
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSendPressed$29$ConversationPresenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 306
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSendPressed$30$ConversationPresenter(Lcom/squareup/ui/crm/ConversationView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 309
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 310
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 311
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->clearMessage()V

    .line 312
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 313
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 315
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 316
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 317
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/dialogue/CreateCommentResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/CreateCommentResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    .line 316
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 318
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 319
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->uniqueKey:Ljava/util/UUID;

    goto :goto_0

    .line 321
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/ConversationPresenter;->showSendMessageError()V

    :goto_0
    return-void
.end method

.method onBillHistoryClicked(Lcom/squareup/ui/crm/ConversationView;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/ConversationView;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 276
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->onBillHistoryButtonClicked()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->bill:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 277
    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$EVyI9FpT5_gFzQjkHiLixUxya3w;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$EVyI9FpT5_gFzQjkHiLixUxya3w;

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 126
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_SHOW_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversationToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$_kr7SOT5pxwIgxOfgbLnh_SRZe4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$_kr7SOT5pxwIgxOfgbLnh_SRZe4;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;)V

    .line 132
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$rlj8Go1tnysZmgJ-_Vcy8OT8iLc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$rlj8Go1tnysZmgJ-_Vcy8OT8iLc;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;)V

    .line 133
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$EqWJxtjk9vIWU6sg8V_A2WADWAc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$EqWJxtjk9vIWU6sg8V_A2WADWAc;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;)V

    .line 134
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 130
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversation:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$magqnI3YKloRCnaPh6t-MyhPA84;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$magqnI3YKloRCnaPh6t-MyhPA84;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;)V

    .line 147
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 159
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->bill:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 160
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 145
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 272
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 164
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ConversationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/ConversationView;

    .line 167
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$kQaBg2EgfnUSfjX67-eMAsoB_sk;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$kQaBg2EgfnUSfjX67-eMAsoB_sk;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 178
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$-MfLsCBVVlHdo6KzJKAE3asbKUI;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$-MfLsCBVVlHdo6KzJKAE3asbKUI;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 186
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$XKBvDv14-KOeHWW68tqONDZETQw;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$XKBvDv14-KOeHWW68tqONDZETQw;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 199
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$CoftfaRmL535BefTsC8RdxTMSlI;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$CoftfaRmL535BefTsC8RdxTMSlI;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 211
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$RirzjpLhiCr-lSOuMonXo4TKRnQ;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$RirzjpLhiCr-lSOuMonXo4TKRnQ;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 223
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$2miTv_GbmDhn6IG3Q1ArxNGCeJ4;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$2miTv_GbmDhn6IG3Q1ArxNGCeJ4;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 232
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$hJWP1WkClMMItPpVxPSgKvgKrIc;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$hJWP1WkClMMItPpVxPSgKvgKrIc;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 246
    new-instance v1, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$IxXbr6d9Tz2Gs6_ei0Yd-n9vtm8;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/-$$Lambda$ConversationPresenter$IxXbr6d9Tz2Gs6_ei0Yd-n9vtm8;-><init>(Lcom/squareup/ui/crm/ConversationPresenter;Lcom/squareup/ui/crm/ConversationView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_0

    .line 257
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->uniqueKey:Ljava/util/UUID;

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "uniqueKey"

    .line 259
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter;->uniqueKey:Ljava/util/UUID;

    :goto_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method setConversationToken(Ljava/lang/String;)V
    .locals 1

    .line 447
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 448
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter;->conversationToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
