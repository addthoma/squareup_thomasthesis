.class public Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;
.super Ljava/lang/Object;
.source "NotesSectionDataRenderer.java"


# static fields
.field public static final SHOW_VIEW_ALL_NOTES_SIZE:Ljava/lang/Integer;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    .line 18
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;->SHOW_VIEW_ALL_NOTES_SIZE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public generateViewData(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;
    .locals 4

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 32
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;->SHOW_VIEW_ALL_NOTES_SIZE:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 33
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;->SHOW_VIEW_ALL_NOTES_SIZE:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p1, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 36
    :cond_1
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 38
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-direct {v1, v0, v2, p1}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;-><init>(ZZLjava/util/List;)V

    return-object v1

    .line 29
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Notes section not supposed to be accessible"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
