.class public Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;
.super Ljava/lang/Object;
.source "CardOnFileViewDataRenderer.java"


# instance fields
.field private final expiryHelper:Lcom/squareup/card/ExpirationHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/crm/CustomerManagementSettings;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/card/ExpirationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->res:Lcom/squareup/util/Res;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->expiryHelper:Lcom/squareup/card/ExpirationHelper;

    return-void
.end method

.method private buildRowForInstrument(Lcom/squareup/protos/client/instruments/InstrumentSummary;Z)Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;
    .locals 11

    .line 69
    iget-object v0, p1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    const-string v1, "instrument.card"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/instruments/CardSummary;

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->expiryHelper:Lcom/squareup/card/ExpirationHelper;

    iget-object v2, v0, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, v0, Lcom/squareup/protos/client/instruments/CardSummary;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/card/ExpirationHelper;->isExpired(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Z

    move-result v8

    .line 73
    new-instance v1, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    iget-object v2, v0, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 74
    invoke-static {v2}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->getDrawableFromBrand(Lcom/squareup/Card$Brand;)I

    move-result v5

    invoke-static {v0}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->formatNameAndNumber(Lcom/squareup/protos/client/instruments/CardSummary;)Ljava/lang/CharSequence;

    move-result-object v6

    iget-object v2, v0, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 75
    :goto_0
    invoke-direct {p0, v0, v8, v2}, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->formatCardExpiry(Lcom/squareup/protos/client/instruments/CardSummary;ZZ)Ljava/lang/CharSequence;

    move-result-object v7

    move-object v4, v1

    move v9, p2

    move-object v10, p1

    invoke-direct/range {v4 .. v10}, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZLcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-object v1
.end method

.method private formatCardExpiry(Lcom/squareup/protos/client/instruments/CardSummary;ZZ)Ljava/lang/CharSequence;
    .locals 1

    if-eqz p2, :cond_0

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/crm/R$string;->card_on_file_expired:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    if-eqz p3, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 114
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/crmviewcustomer/R$string;->crm_cardonfile_expiry:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object p3, p1, Lcom/squareup/protos/client/instruments/CardSummary;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->month:Ljava/lang/String;

    const-string v0, "month"

    .line 115
    invoke-virtual {p2, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/CardSummary;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->year:Ljava/lang/String;

    const-string/jumbo p3, "year"

    .line 116
    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private getDrawableFromBrand(Lcom/squareup/Card$Brand;)I
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer$1;->$SwitchMap$com$squareup$Card$Brand:[I

    invoke-virtual {p1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 103
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_generic:I

    return p1

    .line 101
    :pswitch_0
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_visa:I

    return p1

    .line 99
    :pswitch_1
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_unionpay:I

    return p1

    .line 97
    :pswitch_2
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_giftcard:I

    return p1

    .line 95
    :pswitch_3
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_mastercard:I

    return p1

    .line 93
    :pswitch_4
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_jcb:I

    return p1

    .line 91
    :pswitch_5
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_interac:I

    return p1

    .line 89
    :pswitch_6
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_dinersclub:I

    return p1

    .line 87
    :pswitch_7
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_discover:I

    return p1

    .line 85
    :pswitch_8
    sget p1, Lcom/squareup/crm/R$drawable;->crm_payment_amex:I

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public viewData(Lcom/squareup/protos/client/rolodex/Contact;Z)Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance p1, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;

    const/4 p2, 0x0

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    .line 49
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;-><init>(ZLcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;)V

    return-object p1

    .line 51
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    .line 54
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 55
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 56
    invoke-direct {p0, v1, p2}, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;->buildRowForInstrument(Lcom/squareup/protos/client/instruments/InstrumentSummary;Z)Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_1
    new-instance p1, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;

    const/4 v1, 0x1

    if-eqz p2, :cond_2

    sget-object p2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    goto :goto_1

    :cond_2
    sget-object p2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    :goto_1
    invoke-direct {p1, v1, p2, v0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;-><init>(ZLcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;)V

    return-object p1
.end method
