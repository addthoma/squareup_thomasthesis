.class public final Lcom/squareup/ui/crm/ChooseCustomerScopeRunnerKt;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "DEFAULT_PAGE_SIZE",
        "",
        "NEAR_END_OF_LIST",
        "RECENT_CONTACTS_MAX_LIMIT",
        "SEARCH_DELAY_MS",
        "",
        "crm-choose-customer_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final DEFAULT_PAGE_SIZE:I = 0x32

.field public static final NEAR_END_OF_LIST:I = 0x32

.field public static final RECENT_CONTACTS_MAX_LIMIT:I = 0xa

.field public static final SEARCH_DELAY_MS:J = 0xc8L
