.class public Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;
.super Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;
.source "AllCustomersMasterCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;
    }
.end annotation


# instance fields
.field private final mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

.field private final runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/BadgePresenter;)V
    .locals 12
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v11, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p7

    move-object/from16 v6, p11

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p5

    move-object/from16 v10, p10

    .line 61
    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/AppletSelection;)V

    move-object v0, p2

    .line 63
    iput-object v0, v11, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    move-object/from16 v0, p6

    .line 64
    iput-object v0, v11, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    .line 65
    iget-object v0, v11, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->defaultActionBarTitle:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget v1, Lcom/squareup/crm/applet/R$string;->customers_applet_title:I

    move-object v2, p1

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$0(Lkotlin/Unit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$setupDropDownActions$4(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 1

    .line 111
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/4 v0, 0x1

    if-le p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupDropDownActions$5(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 112
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupDropDownActions$7(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 126
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 74
    invoke-super {p0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->attach(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$LsawUf0Lp-gzDIZu3-HWY88hxwY;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$LsawUf0Lp-gzDIZu3-HWY88hxwY;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method protected getGroupToken()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSupportsFilters()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$null$1$AllCustomersMasterCoordinator(Ljava/lang/String;)V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:Create Customer"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->showCreateCustomerScreen(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$10$AllCustomersMasterCoordinator()V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:View Groups"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->viewGroupsList()V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$11$AllCustomersMasterCoordinator()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:Add to Group"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$12$AllCustomersMasterCoordinator()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:View Feedback"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->viewFeedbackV2()V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$2$AllCustomersMasterCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 89
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    .line 90
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchTerm()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$QiAboSZSnkTzgOTJ8gvd2FHaiaQ;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$QiAboSZSnkTzgOTJ8gvd2FHaiaQ;

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$AGAC3-JZMMF9m-r2JoaNGbp4Cck;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$AGAC3-JZMMF9m-r2JoaNGbp4Cck;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    .line 91
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setupDropDownActions$3$AllCustomersMasterCoordinator()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:Filter Customers"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->showChooseFiltersScreen()V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$6$AllCustomersMasterCoordinator()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:Merge Customers"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->MERGE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$8$AllCustomersMasterCoordinator()V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:Resolve Duplicates"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->showResolveDuplicatesScreen()V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$9$AllCustomersMasterCoordinator()V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Menu:Bulk Delete"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->BULK_DELETE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method protected setupDropDownActions(Landroid/view/View;Lrx/Observable;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 87
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_create_customer:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$3b-5hfsc0V292ESC2GRVUy5j34s;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$3b-5hfsc0V292ESC2GRVUy5j34s;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;Landroid/view/View;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_filter_list:I

    .line 99
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$sG3YJxXdSMe9rZRzoZGpHB140NA;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$sG3YJxXdSMe9rZRzoZGpHB140NA;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    .line 98
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 107
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_merge_customers:I

    .line 108
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 111
    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->allCustomersCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$Jd59kHbw-1mz-ob4SOwaiJLUrAY;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$Jd59kHbw-1mz-ob4SOwaiJLUrAY;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$j7-pCnDfZyzWHUG5jj26rv9s6UM;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$j7-pCnDfZyzWHUG5jj26rv9s6UM;

    .line 110
    invoke-static {p2, v0, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$HTHYjE7h5Pfr8Qiez4khiB1yLVA;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$HTHYjE7h5Pfr8Qiez4khiB1yLVA;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    move-object v1, p0

    .line 107
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 120
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_resolve_duplicates:I

    .line 121
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MERGE_CONTACTS:Lcom/squareup/settings/server/Features$Feature;

    .line 122
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    .line 123
    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$DNNzu2dCk6pr2_Jb5HztkgCrjws;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$DNNzu2dCk6pr2_Jb5HztkgCrjws;

    .line 124
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$fQy8RIiiAIEWX7EYJ4aBgtal-hE;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$fQy8RIiiAIEWX7EYJ4aBgtal-hE;

    .line 126
    invoke-virtual {v0, p2, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$lFZvvfUUQWSk_aKNBn-TZHEsQRc;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$lFZvvfUUQWSk_aKNBn-TZHEsQRc;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    move-object v1, p0

    .line 120
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 133
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_delete_customers:I

    .line 134
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$kVlAF1s1-WlXTDd7QO9zmqhbR3g;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$kVlAF1s1-WlXTDd7QO9zmqhbR3g;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    const/4 v4, 0x1

    move-object v5, p2

    .line 133
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 142
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_view_groups:I

    .line 143
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    const/4 v0, 0x1

    .line 144
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$Qo_RUdPFYFCaGoIVGqyNRb27w_I;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$Qo_RUdPFYFCaGoIVGqyNRb27w_I;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    .line 142
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 151
    sget v1, Lcom/squareup/crm/applet/R$id;->crm_master_menu_add_to_manual_group:I

    .line 152
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    new-instance v7, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$a7f6KdF4YJ8Xh83PHqgvBAL0lgY;

    invoke-direct {v7, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$a7f6KdF4YJ8Xh83PHqgvBAL0lgY;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    const/4 v5, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    .line 151
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 160
    sget p2, Lcom/squareup/crm/applet/R$id;->crm_master_menu_view_feedback:I

    .line 161
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 162
    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$ljUZ5nswJdiX4_CzDX6ojzD56YU;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AllCustomersMasterCoordinator$ljUZ5nswJdiX4_CzDX6ojzD56YU;-><init>(Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;)V

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p1

    .line 160
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    return-void
.end method

.method protected showCreateCustomerOnNoResults()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 174
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected upButtonAction()Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
