.class Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ManageCouponsAndRewardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ManageCouponsAndRewardsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final CHECKABLE_ROW:I

.field final NON_CHECKABLE_ROW:I

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;",
            ">;)V"
        }
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    const/4 p1, 0x1

    .line 104
    iput p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->CHECKABLE_ROW:I

    const/4 p1, 0x2

    .line 105
    iput p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->NON_CHECKABLE_ROW:I

    .line 110
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    .line 180
    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->access$400(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->access$400(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->items:Ljava/util/List;

    .line 181
    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    .line 180
    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 v1, 0x2

    :cond_1
    return v1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 101
    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;I)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->bindCoupon(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;
    .locals 2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 170
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_v2_manage_coupons_and_rewards_row_checkable:I

    goto :goto_0

    .line 172
    :cond_0
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_v2_manage_coupons_and_rewards_row_uncheckable:I

    .line 175
    :goto_0
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;Lcom/squareup/noho/NohoRow;I)V

    return-object v1
.end method

.method updateItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;",
            ">;)V"
        }
    .end annotation

    .line 160
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->items:Ljava/util/List;

    .line 161
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->notifyDataSetChanged()V

    return-void
.end method
