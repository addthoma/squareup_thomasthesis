.class public interface abstract Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;
.super Ljava/lang/Object;
.source "TransferringLoyaltyAccountScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeTransferringLoyaltyAccountScreen(Z)V
.end method

.method public abstract getSourceLoyaltyAccountToken()Ljava/lang/String;
.end method

.method public abstract transferLoyaltyTargetContactToken()Ljava/lang/String;
.end method
