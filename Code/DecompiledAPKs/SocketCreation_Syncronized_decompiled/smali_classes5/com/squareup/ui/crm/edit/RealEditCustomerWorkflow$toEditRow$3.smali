.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->toEditRow(Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "+",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "newValue",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;->$this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "newValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;->$this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

    move-object v1, v0

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v8}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;->copy$default(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    move-result-object p1

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$updateAttributeAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$3;->invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
