.class public Lcom/squareup/ui/crm/cards/BillHistoryScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "BillHistoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;,
        Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/BillHistoryScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isFirstCard:Z

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 171
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$tAhuoB6adswf3VniTb4WzCozkPY;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$tAhuoB6adswf3VniTb4WzCozkPY;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 45
    iput-boolean p2, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->isFirstCard:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/BillHistoryScreen;)Z
    .locals 0

    .line 37
    iget-boolean p0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->isFirstCard:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/BillHistoryScreen;
    .locals 2

    .line 172
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 173
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 174
    :goto_0
    new-instance v1, Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 166
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 168
    iget-boolean p2, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->isFirstCard:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 58
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_bill_history_card_view:I

    return v0
.end method
