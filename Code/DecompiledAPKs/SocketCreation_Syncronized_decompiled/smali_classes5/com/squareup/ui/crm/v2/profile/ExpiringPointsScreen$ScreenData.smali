.class public final Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;
.super Ljava/lang/Object;
.source "ExpiringPointsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation


# instance fields
.field public final empty:Z

.field public final errorMessage:Ljava/lang/String;

.field public final expirationPolicy:Ljava/lang/String;

.field public final expiringPoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;",
            ">;"
        }
    .end annotation
.end field

.field public final loading:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->expiringPoints:Ljava/util/List;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->expirationPolicy:Ljava/lang/String;

    .line 44
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->empty:Z

    .line 45
    iput-boolean p3, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->loading:Z

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->errorMessage:Ljava/lang/String;

    return-void
.end method

.method public static forError(Ljava/lang/String;)Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;
    .locals 4

    .line 68
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;-><init>(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static forLoading()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;
    .locals 4

    .line 64
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, ""

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;-><init>(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getRowTitle(ILjava/text/DateFormat;)Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->expiringPoints:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;

    .line 51
    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexProtoHelper;->formatDate(Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRowValue(I)J
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->expiringPoints:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;

    .line 56
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;->expiring_points_amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSize()I
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->expiringPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
