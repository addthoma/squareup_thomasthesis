.class public Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "UpdateCustomerCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final onCloseClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSaveClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

.field private final shortAnimTimeMs:I

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->onSaveClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 46
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->onCloseClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->res:Lcom/squareup/util/Res;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 55
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    const/high16 p1, 0x10e0000

    .line 56
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 155
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 156
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->progressBar:Landroid/view/View;

    .line 157
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_contact_edit:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    return-void
.end method

.method public static synthetic lambda$309FCglUNPK5TZLsP3OIJWbBZy4(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->update(Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;)V

    return-void
.end method

.method private showContactEdit(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 186
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 188
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 178
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 180
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method private update(Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;)V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->saveButtonEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->x2DisplayToCustomerEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    .line 165
    iget-boolean v0, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->showProgress(Z)V

    .line 166
    iget-boolean v0, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->showContactEdit(Z)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setEnabled(Z)V

    .line 169
    iget-object v0, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    goto :goto_0

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->bindViews(Landroid/view/View;)V

    .line 64
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 65
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$VyHKr6X7a39zP8i1N_0XVnPO0mE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$VyHKr6X7a39zP8i1N_0XVnPO0mE;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    .line 66
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$GgtRbS4FHrJ8WXc1KBRew7EbKSw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$GgtRbS4FHrJ8WXc1KBRew7EbKSw;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    .line 67
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$l1BTuP5__kArBPEHzN4BeFM6z-c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$l1BTuP5__kArBPEHzN4BeFM6z-c;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 72
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmupdatecustomer/R$string;->crm_display_to_customer:I

    .line 73
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_text_selector_blue_disabled_light_gray:I

    .line 74
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextColors(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setAnalyticsPathType(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;)V

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$H3QmM5gZb-gk1TsT_WV3Nbd4I0k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$H3QmM5gZb-gk1TsT_WV3Nbd4I0k;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 91
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$VWYbjX4q9s-hVqz1eYYJ7qC1bkU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$VWYbjX4q9s-hVqz1eYYJ7qC1bkU;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 97
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$66xRJutPsmu9bHq1olb208zGj6M;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$66xRJutPsmu9bHq1olb208zGj6M;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 101
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$LCUpN73pU5hdrfny9J-4F6zj7zs;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$LCUpN73pU5hdrfny9J-4F6zj7zs;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->shouldShowAddressBookButton()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->showAddFromAddressBookButton()V

    .line 111
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$eq77Dji71pLojZlvUqrAW_cHty8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$eq77Dji71pLojZlvUqrAW_cHty8;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 117
    :cond_1
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$OKQ3GG47J9jIoloZeNmqqrCly4Q;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$OKQ3GG47J9jIoloZeNmqqrCly4Q;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$AOwidQpOJ3SAj-BlSTaqT8j1hbI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$AOwidQpOJ3SAj-BlSTaqT8j1hbI;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 133
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$rneRaVyGkdj4No8f7ae5aTPgU0g;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$rneRaVyGkdj4No8f7ae5aTPgU0g;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 142
    :cond_2
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$TPUFDIHhBveHs7yd9m-eu5CHtRY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$TPUFDIHhBveHs7yd9m-eu5CHtRY;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Zw1MaK6p719BQLgWkglFiJcT5dY;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$Zw1MaK6p719BQLgWkglFiJcT5dY;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    .line 151
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$attach$0$UpdateCustomerCoordinator()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->onSaveClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$attach$1$UpdateCustomerCoordinator()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->onCloseClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$attach$10$UpdateCustomerCoordinator()Lrx/Subscription;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAddFromAddressBookClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$iKeVw20G2VJNmFkwXk5XAlbNhLY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$iKeVw20G2VJNmFkwXk5XAlbNhLY;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    .line 113
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$12$UpdateCustomerCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onGroupsClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$TT8j5nipa8V1rSw4Dy9sNKgJMmw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$TT8j5nipa8V1rSw4Dy9sNKgJMmw;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    .line 119
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$14$UpdateCustomerCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onEnumAttributeClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$njF6d-426428ohx8BYT5HSMy5ZY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$njF6d-426428ohx8BYT5HSMy5ZY;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    .line 128
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$16$UpdateCustomerCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onDateAttributeClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$QWU2h8xnSFHYfS7UIY9RYrxgMyY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$QWU2h8xnSFHYfS7UIY9RYrxgMyY;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    .line 135
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$18$UpdateCustomerCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->onCloseClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$b2LWFF8Q-PuKCF3AL5snbkeThU8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$b2LWFF8Q-PuKCF3AL5snbkeThU8;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$2$UpdateCustomerCoordinator()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    sget-object v1, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->EDITING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->X2displayCustomerDetailsOnBran(Lcom/squareup/x2/customers/CustomerInfoWithState$State;)V

    return-void
.end method

.method public synthetic lambda$attach$3$UpdateCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->getScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$309FCglUNPK5TZLsP3OIJWbBZy4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$309FCglUNPK5TZLsP3OIJWbBZy4;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$5$UpdateCustomerCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactChangedByDialogScreen()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$ZNHaipNLcUs-IMI-X_jfr8jlkGU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$ZNHaipNLcUs-IMI-X_jfr8jlkGU;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$6$UpdateCustomerCoordinator()Lrx/Subscription;
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->contact()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ddhlnq8cy2INSA-sUXuHDTQI0jc;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ddhlnq8cy2INSA-sUXuHDTQI0jc;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$8$UpdateCustomerCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->onSaveClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$O732sU62pgR6ecxYlhwnQHE6aAI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerCoordinator$O732sU62pgR6ecxYlhwnQHE6aAI;-><init>(Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;Landroid/view/View;)V

    .line 103
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$11$UpdateCustomerCoordinator(Landroid/view/View;Lkotlin/Unit;)V
    .locals 0

    .line 120
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->showChooseGroupsScreen()V

    return-void
.end method

.method public synthetic lambda$null$13$UpdateCustomerCoordinator(Landroid/view/View;Lcom/squareup/ui/crm/rows/EnumAttribute;)V
    .locals 0

    .line 129
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->showChooseEnumAttributeScreen(Lcom/squareup/ui/crm/rows/EnumAttribute;)V

    return-void
.end method

.method public synthetic lambda$null$15$UpdateCustomerCoordinator(Landroid/view/View;Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V
    .locals 0

    .line 136
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->showChooseCustomDateAttributeScreen(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V

    return-void
.end method

.method public synthetic lambda$null$17$UpdateCustomerCoordinator(Landroid/view/View;Lkotlin/Unit;)V
    .locals 0

    .line 144
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->closeUpdateCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$null$4$UpdateCustomerCoordinator(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->contactEdit:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$null$7$UpdateCustomerCoordinator(Landroid/view/View;Lkotlin/Unit;)V
    .locals 0

    .line 104
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->saveContact()V

    return-void
.end method

.method public synthetic lambda$null$9$UpdateCustomerCoordinator(Lkotlin/Unit;)V
    .locals 0

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;->runner:Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->addressBookClicked()V

    return-void
.end method
