.class public Lcom/squareup/ui/crm/rows/EditDateAttributeRow;
.super Lcom/squareup/ui/account/view/SmartLineRow;
.source "EditDateAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;
    }
.end annotation


# instance fields
.field private attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field private onClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-object v0
.end method

.method public onClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->onClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 28
    invoke-super {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->onFinishInflate()V

    .line 29
    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->onClicked:Lrx/Observable;

    return-void
.end method

.method public showAttribute(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;Ljava/lang/String;)V
    .locals 3

    .line 33
    iget-object v0, p1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 35
    iget-object v0, p1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->setValueVisible(Z)V

    .line 37
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 38
    iget-object p2, p1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {p0, v2}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->setVisibility(I)V

    .line 39
    iget-object p2, p1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    xor-int/2addr p2, v1

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->setEnabled(Z)V

    .line 40
    iget-object p1, p1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    goto :goto_2

    :cond_2
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 43
    :goto_2
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method
