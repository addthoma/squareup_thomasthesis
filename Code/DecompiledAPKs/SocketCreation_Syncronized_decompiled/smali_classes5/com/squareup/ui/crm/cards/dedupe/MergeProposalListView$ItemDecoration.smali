.class public Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "MergeProposalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemDecoration"
.end annotation


# instance fields
.field private final gapLarge:I

.field private final gutter:I

.field final synthetic this$0:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;Landroid/content/res/Resources;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;->this$0:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 45
    sget p1, Lcom/squareup/marin/R$dimen;->marin_gutter:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;->gutter:I

    .line 46
    sget p1, Lcom/squareup/marin/R$dimen;->marin_gap_large:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;->gapLarge:I

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 0

    .line 51
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V

    .line 54
    iget p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;->gutter:I

    iput p2, p1, Landroid/graphics/Rect;->left:I

    .line 55
    iget p3, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;->gapLarge:I

    iput p3, p1, Landroid/graphics/Rect;->top:I

    .line 56
    iput p2, p1, Landroid/graphics/Rect;->right:I

    const/4 p2, 0x0

    .line 57
    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method
