.class public abstract Lcom/squareup/ui/crm/applet/CreateGroupScope$Module;
.super Ljava/lang/Object;
.source "CreateGroupScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CreateGroupScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideCreateGroupScreenController(Lcom/squareup/ui/crm/flow/CreateGroupFlow;)Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
