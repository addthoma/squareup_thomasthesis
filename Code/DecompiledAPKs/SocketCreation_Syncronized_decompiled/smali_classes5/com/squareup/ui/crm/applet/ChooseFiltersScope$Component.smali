.class public interface abstract Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;
.super Ljava/lang/Object;
.source "ChooseFiltersScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/ChooseFiltersScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract chooseFiltersCardScreen()Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Component;
.end method

.method public abstract chooseFiltersFlow()Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;
.end method

.method public abstract createFilterFlow()Lcom/squareup/ui/crm/flow/CreateFilterFlow;
.end method

.method public abstract createFilterScope()Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;
.end method

.method public abstract saveFiltersCardScreen()Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Component;
.end method

.method public abstract updateFilterFlow()Lcom/squareup/ui/crm/flow/UpdateFilterFlow;
.end method

.method public abstract updateFilterScope()Lcom/squareup/ui/crm/applet/UpdateFilterScope$Component;
.end method
