.class public final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "AdjustPointsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$4$2",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $maxAmountScrubber$inlined:Lcom/squareup/text/QuantityScrubber;

.field final synthetic $view$inlined:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/view/View;Lcom/squareup/text/QuantityScrubber;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;->$view$inlined:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;->$maxAmountScrubber$inlined:Lcom/squareup/text/QuantityScrubber;

    .line 157
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$scrubbedAdjustmentValue(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/text/Editable;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->enterAdjustment(I)V

    return-void
.end method
