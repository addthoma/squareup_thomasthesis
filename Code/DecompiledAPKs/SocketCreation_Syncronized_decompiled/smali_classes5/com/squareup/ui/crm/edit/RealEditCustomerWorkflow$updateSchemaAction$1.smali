.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->updateSchemaAction(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "-",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditCustomerWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditCustomerWorkflow.kt\ncom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,432:1\n1099#2,2:433\n1127#2,4:435\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditCustomerWorkflow.kt\ncom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1\n*L\n384#1,2:433\n384#1,4:435\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contact:Lcom/squareup/protos/client/rolodex/Contact;

.field final synthetic $schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;->$schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "-",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;->$schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    invoke-static {v0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->getOrderedAttributes(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    const/16 v1, 0xa

    .line 433
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 434
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 435
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 436
    move-object v3, v1

    check-cast v3, Lcom/squareup/crm/model/ContactAttribute;

    .line 384
    invoke-virtual {v3}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$updateSchemaAction$1;->$schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->attribute_keys_in_order:Ljava/util/List;

    const-string v1, "schema.attribute_keys_in_order"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 383
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$ShowingList;

    invoke-direct {v1, v2, v0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$ShowingList;-><init>(Ljava/util/Map;Ljava/util/List;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
