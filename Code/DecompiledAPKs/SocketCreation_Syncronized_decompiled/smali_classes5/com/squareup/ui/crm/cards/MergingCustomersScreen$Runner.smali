.class public interface abstract Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;
.super Ljava/lang/Object;
.source "MergingCustomersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/MergingCustomersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeMergingCustomersScreen()V
.end method

.method public abstract getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
.end method

.method public abstract getTargetLoyaltyAccountMapping()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end method

.method public abstract successMergedContact(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract supportsLoyaltyDirectoryIntegration()Z
.end method
