.class synthetic Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;
.super Ljava/lang/Object;
.source "ChooseGroupsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 200
    invoke-static {}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->values()[Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I

    sget-object v1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_SOME_GROUPS:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I

    sget-object v1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_NO_GROUPS_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I

    sget-object v1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_ERROR_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
