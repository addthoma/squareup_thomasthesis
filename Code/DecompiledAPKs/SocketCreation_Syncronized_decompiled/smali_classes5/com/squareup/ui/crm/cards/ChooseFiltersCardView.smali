.class public Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;
.super Landroid/widget/LinearLayout;
.source "ChooseFiltersCardView.java"


# instance fields
.field private addFilterButton:Lcom/squareup/marketfont/MarketButton;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private filterContainer:Landroid/widget/LinearLayout;

.field presenter:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private removeAllButton:Lcom/squareup/marketfont/MarketButton;

.field private saveFiltersButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 62
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method addFilterRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 3

    .line 96
    sget v0, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->filterContainer:Landroid/widget/LinearLayout;

    .line 97
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 98
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->filterContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->filterContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method onAddFilterClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->addFilterButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->presenter:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->presenter:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_filter_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->filterContainer:Landroid/widget/LinearLayout;

    .line 43
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_add_filter:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->addFilterButton:Lcom/squareup/marketfont/MarketButton;

    .line 44
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_remove_all_filters:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->removeAllButton:Lcom/squareup/marketfont/MarketButton;

    .line 45
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_save_filters:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->saveFiltersButton:Lcom/squareup/marketfont/MarketButton;

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->saveFiltersButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    .line 48
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-void
.end method

.method onRemoveAllClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->removeAllButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onSaveFiltersClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->saveFiltersButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method removeAllFilterRows()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->filterContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->filterContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method setAddFilterEnabled(Z)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->addFilterButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method showRemoveAll(Z)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->removeAllButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showSaveFilters(Z)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->saveFiltersButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
