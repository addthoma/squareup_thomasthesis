.class Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;
.super Ljava/lang/Object;
.source "BirthdayPicker.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->onMeasuredOnce()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$500(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinDatePicker;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$400(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$500(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinDatePicker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->getMonth()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$4;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {v2}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$500(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinDatePicker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinDatePicker;->getDayOfMonth()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/marin/widgets/MarinDatePicker;->updateDate(III)V

    return-void
.end method
