.class public final Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;
.super Ljava/lang/Object;
.source "CrmScopeType.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u001a\u0010\u0010\u0004\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u001a\u0010\u0010\u0005\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u001a\u000c\u0010\u0006\u001a\u0004\u0018\u00010\u0003*\u00020\u0007\u001a\u000c\u0010\u0008\u001a\u00020\u0007*\u0004\u0018\u00010\u0003\u00a8\u0006\t"
    }
    d2 = {
        "inAddCustomerToSale",
        "",
        "crmScopeType",
        "Lcom/squareup/ui/crm/flow/CrmScopeType;",
        "inCustomersApplet",
        "isInvoicePath",
        "toCrmScopeType",
        "",
        "toInt",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final inAddCustomerToSale(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z
    .locals 1

    .line 121
    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final inCustomersApplet(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z
    .locals 1

    .line 112
    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isInvoicePath(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final toCrmScopeType(I)Lcom/squareup/ui/crm/flow/CrmScopeType;
    .locals 1

    if-ltz p0, :cond_0

    .line 99
    invoke-static {}, Lcom/squareup/ui/crm/flow/CrmScopeType;->values()[Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    aget-object p0, v0, p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final toInt(Lcom/squareup/ui/crm/flow/CrmScopeType;)I
    .locals 0

    if-eqz p0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, -0x1

    :goto_0
    return p0
.end method
