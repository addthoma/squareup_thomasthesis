.class public Lcom/squareup/ui/crm/flow/CrmScope$ComponentFactory;
.super Ljava/lang/Object;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 568
    check-cast p2, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 569
    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScope$1;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    iget-object p2, p2, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    packed-switch p2, :pswitch_data_0

    .line 635
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Invalid path type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 627
    :pswitch_0
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;

    .line 628
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;

    .line 629
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;->viewCustomerAddedToSale(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewInTransC;

    move-result-object p1

    return-object p1

    .line 617
    :pswitch_1
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentInvoicesAppletScopeComponent;

    .line 618
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentInvoicesAppletScopeComponent;

    .line 619
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentInvoicesAppletScopeComponent;->viewCustomerFromInvoicesAppletScope(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewInInvoiceOnDetailC;

    move-result-object p1

    return-object p1

    .line 611
    :pswitch_2
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;

    .line 612
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;

    .line 613
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;->viewCustomer(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewCustomerFromServicesComponent;

    move-result-object p1

    return-object p1

    .line 603
    :pswitch_3
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentRetailHomeAppletScopeComponent;

    .line 604
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentRetailHomeAppletScopeComponent;

    .line 605
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentRetailHomeAppletScopeComponent;->viewCustomerFromRetailHomeAppletScope(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewInRetailDetailC;

    move-result-object p1

    return-object p1

    .line 597
    :pswitch_4
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentCustomersAppletComponent;

    .line 598
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentCustomersAppletComponent;

    .line 599
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentCustomersAppletComponent;->viewCustomerProfileInApplet(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewInAppletC;

    move-result-object p1

    return-object p1

    .line 589
    :pswitch_5
    const-class p2, Lcom/squareup/ui/main/CrmScopeApiCardOnFileParentComponent;

    .line 590
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/CrmScopeApiCardOnFileParentComponent;

    .line 591
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;-><init>()V

    .line 593
    invoke-interface {p1, p2}, Lcom/squareup/ui/main/CrmScopeApiCardOnFileParentComponent;->apiCrmScope(Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;)Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapComponent;

    move-result-object p1

    return-object p1

    .line 583
    :pswitch_6
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;

    .line 584
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;

    .line 585
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$AddInTransModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$AddInTransModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;->addCustomerToSaleTicket(Lcom/squareup/ui/crm/flow/CrmScope$AddInTransModule;)Lcom/squareup/ui/crm/flow/CrmScope$AddInTransC;

    move-result-object p1

    return-object p1

    .line 573
    :pswitch_7
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;

    .line 574
    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;

    .line 575
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerModule;

    invoke-direct {p2}, Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerModule;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;->addCustomerToInvoice(Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerModule;)Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerC;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
