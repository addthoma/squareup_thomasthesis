.class public Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;
.super Lcom/squareup/ui/inventory/InAdjustInventoryScope;
.source "AdjustInventoryErrorDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$iLqa0lhNSJXkPoMzYGR-0QBKgTY;->INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$iLqa0lhNSJXkPoMzYGR-0QBKgTY;

    .line 46
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/InAdjustInventoryScope;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    .line 48
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    .line 49
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
