.class Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;
.super Ljava/lang/Object;
.source "AdjustInventoryController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AdjustInventoryErrorDialogScreenData"
.end annotation


# instance fields
.field final messageResId:I

.field final request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

.field final titleResId:I


# direct methods
.method constructor <init>(IILcom/squareup/protos/client/AdjustVariationInventoryRequest;)V
    .locals 0

    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    iput p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;->titleResId:I

    .line 522
    iput p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;->messageResId:I

    .line 523
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    return-void
.end method
