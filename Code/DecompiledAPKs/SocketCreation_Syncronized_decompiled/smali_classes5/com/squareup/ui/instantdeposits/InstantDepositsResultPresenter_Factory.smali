.class public final Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;
.super Ljava/lang/Object;
.source "InstantDepositsResultPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;"
        }
    .end annotation

    .line 64
    new-instance v8, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/ui/activity/ActivityAppletScopeRunner;Lcom/squareup/util/BrowserLauncher;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            "Lcom/squareup/ui/activity/ActivityAppletScopeRunner;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;"
        }
    .end annotation

    .line 70
    new-instance v8, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/ui/activity/ActivityAppletScopeRunner;Lcom/squareup/util/BrowserLauncher;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;
    .locals 8

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/ui/activity/ActivityAppletScopeRunner;Lcom/squareup/util/BrowserLauncher;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter_Factory;->get()Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    move-result-object v0

    return-object v0
.end method
