.class Lcom/squareup/ui/ConfirmButton$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ConfirmButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ConfirmButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ConfirmButton;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ConfirmButton;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/ConfirmButton$1;->this$0:Lcom/squareup/ui/ConfirmButton;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton$1;->this$0:Lcom/squareup/ui/ConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ConfirmButton;->access$000(Lcom/squareup/ui/ConfirmButton;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton$1;->this$0:Lcom/squareup/ui/ConfirmButton;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/ConfirmButton;->access$100(Lcom/squareup/ui/ConfirmButton;Z)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton$1;->this$0:Lcom/squareup/ui/ConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ConfirmButton;->access$200(Lcom/squareup/ui/ConfirmButton;)Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton$1;->this$0:Lcom/squareup/ui/ConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ConfirmButton;->access$200(Lcom/squareup/ui/ConfirmButton;)Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;->onConfirm()V

    goto :goto_0

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton$1;->this$0:Lcom/squareup/ui/ConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ConfirmButton;->access$300(Lcom/squareup/ui/ConfirmButton;)V

    :cond_1
    :goto_0
    return-void
.end method
