.class Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$2;
.super Lcom/squareup/flowlegacy/NoResultPopupPresenter;
.source "GiftCardActivationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/BundleKey;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
        "Lcom/squareup/widgets/warning/Warning;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;Ljava/lang/String;)V
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-direct {p0, p2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 126
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$2;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 0

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    return-void
.end method
