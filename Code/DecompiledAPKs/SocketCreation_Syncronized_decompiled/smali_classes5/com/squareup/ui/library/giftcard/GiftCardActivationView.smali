.class public Lcom/squareup/ui/library/giftcard/GiftCardActivationView;
.super Landroid/widget/LinearLayout;
.source "GiftCardActivationView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private amount:Lcom/squareup/widgets/SelectableEditText;

.field private cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field private ignoreEditEvents:Z

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private swipedCard:Lcom/squareup/ui/TokenView;

.field thirdPartyGiftCardPanValidationStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    const-class p2, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen$Component;->inject(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    .line 57
    new-instance p2, Lcom/squareup/caller/FailurePopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    .line 58
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Z
    .locals 0

    .line 36
    iget-boolean p0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->ignoreEditEvents:Z

    return p0
.end method

.method private bindViews()V
    .locals 1

    .line 174
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 175
    sget v0, Lcom/squareup/orderentry/R$id;->gift_card_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    .line 176
    sget v0, Lcom/squareup/orderentry/R$id;->gift_card_number:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/PanEditor;

    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    .line 177
    sget v0, Lcom/squareup/orderentry/R$id;->swiped_gift_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/TokenView;

    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->swipedCard:Lcom/squareup/ui/TokenView;

    return-void
.end method


# virtual methods
.method clearCardNumber()V
    .locals 2

    const/4 v0, 0x1

    .line 164
    iput-boolean v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->ignoreEditEvents:Z

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 166
    iput-boolean v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->ignoreEditEvents:Z

    return-void
.end method

.method disableAmount()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    return-void
.end method

.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method getCard()Lcom/squareup/Card;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PanEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$GiftCardActivationView(Landroid/view/View;Z)V
    .locals 0

    .line 74
    sget p1, Lcom/squareup/orderentry/R$id;->amount_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->setViewBackground(Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$GiftCardActivationView(Landroid/view/View;Z)V
    .locals 0

    .line 92
    sget p1, Lcom/squareup/orderentry/R$id;->card_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->setViewBackground(Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$GiftCardActivationView()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onClearSwipedCardClicked()V

    return-void
.end method

.method public synthetic lambda$requestInitialFocus$3$GiftCardActivationView()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 63
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->bindViews()V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getMaxMoneyScrubber()Lcom/squareup/money/MaxMoneyScrubber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$vGA8ajSRvfl7MCNAx1knGtsQZVw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$vGA8ajSRvfl7MCNAx1knGtsQZVw;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    new-instance v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setOnPanListener(Lcom/squareup/register/widgets/card/OnPanListener;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$IM6LVbyZ8tQxydKYZl2s7g9Spwc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$IM6LVbyZ8tQxydKYZl2s7g9Spwc;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->thirdPartyGiftCardPanValidationStrategyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    new-instance v1, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 101
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->swipedCard:Lcom/squareup/ui/TokenView;

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$RCjWFHrJTDtbRagHiM_5rpjr3nc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$RCjWFHrJTDtbRagHiM_5rpjr3nc;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/TokenView;->setListener(Lcom/squareup/ui/TokenView$Listener;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->takeView(Ljava/lang/Object;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->failurePresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->dropView(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->failurePresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 119
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$bQJLDrzW1lahV48t1aUHRd0K6rc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationView$bQJLDrzW1lahV48t1aUHRd0K6rc;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setAmount(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x1

    .line 133
    iput-boolean v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->ignoreEditEvents:Z

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->amount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 135
    iput-boolean p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->ignoreEditEvents:Z

    return-void
.end method

.method setViewBackground(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 127
    sget p2, Lcom/squareup/marin/R$drawable;->marin_edit_text_focused:I

    goto :goto_0

    :cond_0
    sget p2, Lcom/squareup/marin/R$drawable;->marin_edit_text_normal:I

    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method showGiftCardEditor()V
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->swipedCard:Lcom/squareup/ui/TokenView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/TokenView;->setVisibility(I)V

    return-void
.end method

.method showSwipedCard(Ljava/lang/String;)V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->cardNumber:Lcom/squareup/register/widgets/card/PanEditor;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->swipedCard:Lcom/squareup/ui/TokenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/TokenView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->swipedCard:Lcom/squareup/ui/TokenView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/TokenView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->clearCardNumber()V

    return-void
.end method
