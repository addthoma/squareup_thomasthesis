.class public Lcom/squareup/ui/library/ModifierListRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "ModifierListRow.java"


# instance fields
.field private dragHandle:Lcom/squareup/glyph/SquareGlyphView;

.field protected final gapMedium:I

.field protected final gutterHalf:I

.field private name:Landroid/widget/TextView;

.field private options:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/library/ModifierListRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 26
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/library/ModifierListRow;->gutterHalf:I

    .line 27
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/library/ModifierListRow;->gapMedium:I

    .line 28
    iget p2, p0, Lcom/squareup/ui/library/ModifierListRow;->gutterHalf:I

    invoke-virtual {p0, p2}, Lcom/squareup/ui/library/ModifierListRow;->setHorizontalInsets(I)V

    .line 29
    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    .line 30
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/ModifierListRow;->setBorderWidth(I)V

    const/16 p1, 0x8

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/ModifierListRow;->addBorder(I)V

    return-void
.end method


# virtual methods
.method public getDragHandle()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/library/ModifierListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 35
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    .line 36
    sget v0, Lcom/squareup/widgets/pos/R$id;->drag_handle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/library/ModifierListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    .line 37
    sget v0, Lcom/squareup/widgets/pos/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/library/ModifierListRow;->name:Landroid/widget/TextView;

    .line 38
    sget v0, Lcom/squareup/widgets/pos/R$id;->options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/library/ModifierListRow;->options:Landroid/widget/TextView;

    return-void
.end method

.method public setContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/library/ModifierListRow;->name:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/library/ModifierListRow;->options:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDraggableDisplay(Z)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/library/ModifierListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/library/ModifierListRow;->name:Landroid/widget/TextView;

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    .line 51
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/ModifierListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/library/ModifierListRow;->name:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/ui/library/ModifierListRow;->gapMedium:I

    iget v2, p0, Lcom/squareup/ui/library/ModifierListRow;->gutterHalf:I

    add-int/2addr v1, v2

    invoke-virtual {p1, v1, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_0
    return-void
.end method
