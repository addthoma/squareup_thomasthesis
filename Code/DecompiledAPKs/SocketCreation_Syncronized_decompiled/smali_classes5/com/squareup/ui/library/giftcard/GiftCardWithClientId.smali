.class public Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;
.super Lcom/squareup/Card;
.source "GiftCardWithClientId.java"


# instance fields
.field private final clientId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/Card;Ljava/lang/String;)V
    .locals 15

    .line 15
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v4

    .line 16
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getExpirationYear()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getExpirationMonth()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getServiceCode()Ljava/lang/String;

    move-result-object v8

    .line 17
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getPinVerification()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getDiscretionaryData()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getVerification()Ljava/lang/String;

    move-result-object v11

    .line 18
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/Card;->getInputType()Lcom/squareup/Card$InputType;

    move-result-object v14

    move-object v0, p0

    .line 15
    invoke-direct/range {v0 .. v14}, Lcom/squareup/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card$Brand;Lcom/squareup/Card$InputType;)V

    move-object/from16 v1, p2

    .line 19
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;->clientId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 27
    invoke-super {p0, p1}, Lcom/squareup/Card;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;

    iget-object p1, p1, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;->clientId:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;->clientId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 31
    invoke-super {p0}, Lcom/squareup/Card;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;->clientId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
