.class public Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;
.super Lcom/squareup/ui/library/BaseGlyphButtonEditText;
.source "GlyphButtonAutoCompleteEditText.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/library/BaseGlyphButtonEditText<",
        "Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 18
    sget v0, Lcom/squareup/widgets/pos/R$layout;->glyph_button_auto_complete_edit_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public setAutoCompleteAdapter(Landroid/widget/ArrayAdapter;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->textField:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
