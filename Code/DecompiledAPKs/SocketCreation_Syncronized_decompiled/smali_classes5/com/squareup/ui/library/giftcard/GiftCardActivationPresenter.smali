.class Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;
.super Lmortar/ViewPresenter;
.source "GiftCardActivationPresenter.java"

# interfaces
.implements Lcom/squareup/ui/seller/SwipedCardHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/library/giftcard/GiftCardActivationView;",
        ">;",
        "Lcom/squareup/ui/seller/SwipedCardHandler;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final WARNING_POPUP_STATE_KEY:Ljava/lang/String; = "GIFT_CARD_WARNING_POPUP"


# instance fields
.field private activationInProgress:Z

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field failurePresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private progressSpinnerSubscription:Lrx/Subscription;

.field private final registerCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/BundleKey;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/badbus/BadBus;",
            "Lflow/Flow;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 99
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 84
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->registerCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    move-object v1, p1

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p2

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p5

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p6

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    move-object v1, p7

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p3

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p4

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object v1, p8

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    move-object v1, p9

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->bus:Lcom/squareup/badbus/BadBus;

    move-object v1, p10

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->flow:Lflow/Flow;

    move-object v1, p11

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object v1, p12

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    move-object/from16 v1, p13

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->workingItemBundleKey:Lcom/squareup/BundleKey;

    move-object/from16 v1, p14

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-object/from16 v1, p15

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 116
    new-instance v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->failurePresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 126
    new-instance v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$2;

    const-string v2, "GIFT_CARD_WARNING_POPUP"

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$2;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->registerCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->clearCard()V

    return-void
.end method

.method private addToCartAndExit(Lcom/squareup/protos/client/giftcards/GiftCard;)V
    .locals 3

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->hasGiftCardItemWithServerId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    .line 325
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/orderentry/R$string;->duplicate_gift_card_title:I

    sget v2, Lcom/squareup/orderentry/R$string;->duplicate_gift_card_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {p1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    .line 331
    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 332
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 334
    :cond_1
    new-instance v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    .line 335
    invoke-direct {p0, v2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getActivityType(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->activity_type(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    .line 336
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->gift_card_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    .line 337
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    move-result-object p1

    .line 338
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object p1

    .line 339
    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v2, v2, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 340
    invoke-virtual {v2, v0}, Lcom/squareup/configure/item/WorkingItem;->setOrderItemName(Ljava/lang/String;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setGiftCardDetails(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 339
    invoke-virtual {v1, p1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    const/4 p1, 0x0

    .line 341
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->finish(Z)V

    return-void
.end method

.method private canLoadCard()Z
    .locals 7

    .line 374
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 376
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->extractAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    .line 378
    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMinimum()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 379
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCard()Lcom/squareup/Card;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private clearCard()V
    .locals 2

    .line 441
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    if-eqz v0, :cond_0

    .line 442
    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->clearCardNumber()V

    .line 443
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->clearSwipedCard()V

    .line 444
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardActivation(Lcom/squareup/protos/common/Money;)Z

    return-void
.end method

.method private clearSwipedCard()V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    .line 437
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->updateUi()V

    return-void
.end method

.method private extractAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {v1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getAmount()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method private finish(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 393
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    .line 395
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissGiftCardActivation()Z

    .line 396
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method private getActivityType(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
    .locals 1

    .line 312
    sget-object v0, Lcom/squareup/protos/client/giftcards/GiftCard$State;->PENDING:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-ne p1, v0, :cond_0

    .line 313
    sget-object p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p1

    .line 314
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/giftcards/GiftCard$State;->ACTIVE:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-ne p1, v0, :cond_1

    .line 315
    sget-object p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->RELOAD:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p1

    .line 317
    :cond_1
    sget-object p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p1
.end method

.method private getCard()Lcom/squareup/Card;
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getCard()Lcom/squareup/Card;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getCurrentPrice()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 448
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->isVariablePrice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 449
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    .line 450
    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private isVariablePrice()Z
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$JTuNgeJqqHvIp8u0PGZtl_JbU9I(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->loadCard()V

    return-void
.end method

.method private loadCard()V
    .locals 5

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTotalPurchaseMaximum()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 296
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->extractAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 297
    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 298
    invoke-static {v2, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-lez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/configure/item/R$string;->gift_card_activation_failed:I

    sget v3, Lcom/squareup/configure/item/R$string;->gift_card_purchase_limit_exceeded_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->loadingGiftCard()Z

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->registerCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onX2ChipCardSwiped()V
    .locals 4

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/common/strings/R$string;->swipe_failed_invalid_card_title:I

    sget v3, Lcom/squareup/common/strings/R$string;->swipe_failed_invalid_card_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method private subscribeToRegisterCardNetworkCall()Lrx/Subscription;
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->registerCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$6SVaA6Ea1p0Uj-HeHRtX-wW0beI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$6SVaA6Ea1p0Uj-HeHRtX-wW0beI;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    .line 196
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$Xl-93KDXGdzft288sYWYXd4h3qQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$Xl-93KDXGdzft288sYWYXd4h3qQ;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    .line 197
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$ziNpeUGJpz74b8lf-4omnBt0OGg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$ziNpeUGJpz74b8lf-4omnBt0OGg;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    .line 203
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method private updateUi()V
    .locals 7

    .line 345
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 346
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {v2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getAmount()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 350
    iget-object v2, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    .line 352
    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->gift_card_load_amount:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 353
    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v3, "amount"

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 354
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 355
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 357
    :cond_2
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->gift_card_custom:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 361
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v1, v1, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    if-eqz v1, :cond_3

    .line 362
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget-object v3, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    iget-object v4, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v4, v4, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    .line 364
    invoke-virtual {v4}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v4

    .line 363
    invoke-static {v2, v3, v4}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 362
    invoke-virtual {v1, v2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->showSwipedCard(Ljava/lang/String;)V

    goto :goto_2

    .line 366
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {v1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->showGiftCardEditor()V

    .line 370
    :goto_2
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->canLoadCard()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V
    .locals 1

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->progressSpinnerSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    .line 188
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    const/4 v0, 0x0

    .line 189
    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->progressSpinnerSubscription:Lrx/Subscription;

    .line 191
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->dropView(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    return-void
.end method

.method getMaxMoneyScrubber()Lcom/squareup/money/MaxMoneyScrubber;
    .locals 4

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMaximum()J

    move-result-wide v0

    .line 405
    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 406
    new-instance v1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v1, v2, v3, v0}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    return-object v1
.end method

.method public ignoreSwipeFor(Lcom/squareup/Card;)Z
    .locals 0

    .line 271
    iget-boolean p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->activationInProgress:Z

    return p1
.end method

.method public isThirdPartyGiftCardFeatureEnabled()Z
    .locals 1

    .line 432
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$null$4$GiftCardActivationPresenter(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 208
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-direct {p0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->addToCartAndExit(Lcom/squareup/protos/client/giftcards/GiftCard;)V

    return-void
.end method

.method public synthetic lambda$null$5$GiftCardActivationPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 211
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 212
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->failurePresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->gift_card_activation_failed:I

    .line 213
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->gift_card_activation_error_message:I

    .line 215
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->ok:I

    .line 216
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->retry:I

    .line 217
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 212
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 219
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->failurePresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->gift_card_activation_failed:I

    .line 220
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->gift_card_activation_error_message:I

    .line 222
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->ok:I

    .line 223
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 219
    invoke-static {v0, v1, v2}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 227
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$GiftCardActivationPresenter(Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 144
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onX2ChipCardSwiped()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$GiftCardActivationPresenter()V
    .locals 1

    const/4 v0, 0x1

    .line 176
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->finish(Z)V

    return-void
.end method

.method public synthetic lambda$subscribeToRegisterCardNetworkCall$2$GiftCardActivationPresenter(Lkotlin/Unit;)V
    .locals 0

    const/4 p1, 0x1

    .line 196
    iput-boolean p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->activationInProgress:Z

    return-void
.end method

.method public synthetic lambda$subscribeToRegisterCardNetworkCall$3$GiftCardActivationPresenter(Lkotlin/Unit;)Lrx/Observable;
    .locals 5

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    .line 199
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCard()Lcom/squareup/Card;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v1, v1, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v1}, Lcom/squareup/configure/item/WorkingItem;->ensureIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/giftcard/GiftCardServiceHelper;->registerGiftCard(Lcom/squareup/Card;Lcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;

    move-result-object p1

    .line 200
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 198
    invoke-static {p1, v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget v1, Lcom/squareup/orderentry/R$string;->gift_card_activating:I

    sget-object v2, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x320

    .line 201
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(IJLjava/util/concurrent/TimeUnit;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$subscribeToRegisterCardNetworkCall$6$GiftCardActivationPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    const/4 v0, 0x0

    .line 205
    iput-boolean v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->activationInProgress:Z

    .line 207
    new-instance v0, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$I7RlvQhRPH4HoAXK6yODXoPBcV8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$I7RlvQhRPH4HoAXK6yODXoPBcV8;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$3i4XeS5dI1oKv2PiXhOq66UN4Xo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$3i4XeS5dI1oKv2PiXhOq66UN4Xo;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onAmountChanged()V
    .locals 3

    .line 410
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {v2}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getAmount()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    goto :goto_0

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardActivation(Lcom/squareup/protos/common/Money;)Z

    .line 416
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->updateUi()V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    .line 265
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->finish(Z)V

    return v0
.end method

.method onClearSwipedCardClicked()V
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardActivation(Lcom/squareup/protos/common/Money;)Z

    .line 261
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->clearSwipedCard()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 134
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;

    .line 137
    iget-object v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->workingItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v0, v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-static {v1, v0}, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->loaded(Lcom/squareup/BundleKey;Lcom/squareup/configure/item/WorkingItem;)Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    goto :goto_0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static {v0}, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->empty(Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    .line 142
    :goto_0
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$qf8CDHADBnt2thYpOa-YRb080Lw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$qf8CDHADBnt2thYpOa-YRb080Lw;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    .line 144
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 143
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/x2/X2SwipedGiftCard;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$eY9VUr3q0zeWD00VhcfjJBBBLgo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$eY9VUr3q0zeWD00VhcfjJBBBLgo;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/ui/payment/SwipeHandler;->setSwipeHandler(Lmortar/MortarScope;Lcom/squareup/ui/seller/SwipedCardHandler;)V

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->subscribeToRegisterCardNetworkCall()Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissGiftCardActivation()Z

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 156
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 157
    invoke-virtual {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    .line 159
    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->progressSpinnerSubscription:Lrx/Subscription;

    .line 161
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->isVariablePrice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v2, v2, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v2, v2, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->setAmount(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_1

    .line 165
    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->requestInitialFocus()V

    goto :goto_0

    .line 168
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iget-object v1, v1, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v1, v1, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->setAmount(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->disableAmount()V

    .line 174
    :cond_1
    :goto_0
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->gift_card_custom:I

    .line 175
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$2fBFD5ipnSSclgDB6pWZEXWY0M0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$2fBFD5ipnSSclgDB6pWZEXWY0M0;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    .line 176
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->gift_card_load:I

    .line 177
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$JTuNgeJqqHvIp8u0PGZtl_JbU9I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationPresenter$JTuNgeJqqHvIp8u0PGZtl_JbU9I;-><init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    .line 178
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 179
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 180
    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardActivation(Lcom/squareup/protos/common/Money;)Z

    .line 183
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->updateUi()V

    return-void
.end method

.method public onPanInvalid()V
    .locals 0

    .line 428
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->updateUi()V

    return-void
.end method

.method public onPanNext()V
    .locals 1

    .line 420
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->canLoadCard()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->loadCard()V

    :cond_0
    return-void
.end method

.method public onPanValid()V
    .locals 0

    .line 424
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->updateUi()V

    return-void
.end method

.method onX2SwipedGiftCard(Lcom/squareup/x2/X2SwipedGiftCard;)V
    .locals 3

    .line 249
    invoke-virtual {p1}, Lcom/squareup/x2/X2SwipedGiftCard;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    .line 251
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/common/strings/R$string;->swipe_failed_invalid_card_title:I

    sget v2, Lcom/squareup/common/strings/R$string;->swipe_failed_invalid_card_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {p1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 256
    invoke-virtual {p1}, Lcom/squareup/x2/X2SwipedGiftCard;->getCard()Lcom/squareup/Card;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V

    return-void
.end method

.method public processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V
    .locals 2

    .line 275
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p1, p2}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->clearSwipedCard()V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    .line 278
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance p2, Lcom/squareup/widgets/warning/WarningIds;

    sget v0, Lcom/squareup/orderentry/R$string;->gift_card_unsupported_title:I

    sget v1, Lcom/squareup/orderentry/R$string;->gift_card_unsupported_message:I

    invoke-direct {p2, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {p1, p2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 283
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->state:Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    iput-object p2, p1, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->swipedCard:Lcom/squareup/Card;

    .line 284
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->updateUi()V

    .line 285
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->canLoadCard()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 286
    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->loadCard()V

    goto :goto_0

    .line 288
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-direct {p0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->getCurrentPrice()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringGiftCard(Lcom/squareup/protos/common/Money;)Z

    :goto_0
    return-void
.end method
