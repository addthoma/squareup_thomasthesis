.class public final enum Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;
.super Ljava/lang/Enum;
.source "TendersAwaitingTipLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/TendersAwaitingTipLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SortField"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

.field public static final enum AMOUNT:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

.field public static final enum EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

.field public static final enum TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;


# instance fields
.field public final nameForAnalytics:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 43
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    const/4 v1, 0x0

    const-string v2, "TIME"

    const-string v3, "Time"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    .line 44
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    const/4 v2, 0x1

    const-string v3, "AMOUNT"

    const-string v4, "Amount"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->AMOUNT:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    .line 45
    new-instance v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    const/4 v3, 0x2

    const-string v4, "EMPLOYEE"

    const-string v5, "Employee"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    .line 42
    sget-object v4, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->AMOUNT:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->$VALUES:[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->nameForAnalytics:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->$VALUES:[Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-virtual {v0}, [Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    return-object v0
.end method
