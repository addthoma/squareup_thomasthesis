.class public interface abstract Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.java"

# interfaces
.implements Lcom/squareup/ui/activity/QuickTipEditor$Component;
.implements Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/SettleTipRow;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/SettleTipsSection;)V
.end method
