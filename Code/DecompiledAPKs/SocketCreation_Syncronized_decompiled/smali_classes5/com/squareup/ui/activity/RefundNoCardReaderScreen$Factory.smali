.class public Lcom/squareup/ui/activity/RefundNoCardReaderScreen$Factory;
.super Ljava/lang/Object;
.source "RefundNoCardReaderScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/RefundNoCardReaderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 27
    invoke-static {p0}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object p0

    invoke-virtual {p0}, Lflow/Flow;->goBack()Z

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/common/strings/R$string;->dismiss:I

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$RefundNoCardReaderScreen$Factory$TVaCKTgzUuB0zo_C-kWgC0mWyeY;

    invoke-direct {v2, p1}, Lcom/squareup/ui/activity/-$$Lambda$RefundNoCardReaderScreen$Factory$TVaCKTgzUuB0zo_C-kWgC0mWyeY;-><init>(Landroid/content/Context;)V

    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 28
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->no_reader_connected:I

    .line 29
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->contactless_reader_required:I

    .line 30
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 31
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 25
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
