.class public interface abstract Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;
.super Ljava/lang/Object;
.source "ActivitySearchPaymentStarter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$NoActivitySearchPaymentStarter;,
        Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;
    }
.end annotation


# virtual methods
.method public abstract disableContactlessField()V
.end method

.method public abstract enableContactlessField()V
.end method

.method public abstract register(Lmortar/MortarScope;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V
.end method
