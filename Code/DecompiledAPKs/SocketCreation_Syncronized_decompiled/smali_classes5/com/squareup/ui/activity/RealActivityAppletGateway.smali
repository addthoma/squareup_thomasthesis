.class public final Lcom/squareup/ui/activity/RealActivityAppletGateway;
.super Ljava/lang/Object;
.source "RealActivityAppletGateway.kt"

# interfaces
.implements Lcom/squareup/ui/activity/ActivityAppletGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\u000c\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/activity/RealActivityAppletGateway;",
        "Lcom/squareup/ui/activity/ActivityAppletGateway;",
        "flow",
        "Lflow/Flow;",
        "activityApplet",
        "Lcom/squareup/ui/activity/ActivityApplet;",
        "(Lflow/Flow;Lcom/squareup/ui/activity/ActivityApplet;)V",
        "activateInitialScreen",
        "",
        "goToDisallowInventoryApiDialog",
        "issueRefundScope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "goToRestockOnItemizedRefundScreen",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/ui/activity/ActivityApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityApplet"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway;->activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

    return-void
.end method


# virtual methods
.method public activateInitialScreen()V
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway;->activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityApplet;->activate()V

    return-void
.end method

.method public goToDisallowInventoryApiDialog(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 2

    const-string v0, "issueRefundScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/DisallowInventoryApiDialog;

    check-cast p1, Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/DisallowInventoryApiDialog;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToRestockOnItemizedRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 2

    const-string v0, "issueRefundScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/RestockOnItemizedRefundScreen;

    check-cast p1, Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/RestockOnItemizedRefundScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
