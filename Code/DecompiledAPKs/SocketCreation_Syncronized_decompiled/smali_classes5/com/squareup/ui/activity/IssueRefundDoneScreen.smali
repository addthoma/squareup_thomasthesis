.class public Lcom/squareup/ui/activity/IssueRefundDoneScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "IssueRefundDoneScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/IssueRefundDoneScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundDoneScreen$s0aak_hN6JvErZLpI87I3Pgtc7Y;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundDoneScreen$s0aak_hN6JvErZLpI87I3Pgtc7Y;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/IssueRefundDoneScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/IssueRefundDoneScreen;
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/activity/IssueRefundDoneScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 49
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/IssueRefundScope;

    .line 50
    new-instance v0, Lcom/squareup/ui/activity/IssueRefundDoneScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/IssueRefundDoneScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundDoneScreen;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 29
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 2

    .line 33
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 34
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->refundDoneCoordinator()Lcom/squareup/activity/refund/RefundDoneCoordinator$Factory;

    move-result-object v0

    .line 35
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object p1

    .line 36
    invoke-virtual {p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->screenData()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator$Factory;->create(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;)Lcom/squareup/activity/refund/RefundDoneCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 40
    sget v0, Lcom/squareup/activity/R$layout;->activity_applet_issue_refund_done_view:I

    return v0
.end method
