.class final Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/RealCardPresentRefund;->requestContactlessRefund(Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/ui/activity/ReaderResult;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $refundMoney:Lcom/squareup/protos/common/Money;

.field final synthetic this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/RealCardPresentRefund;Lcom/squareup/protos/common/Money;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    iput-object p2, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->$refundMoney:Lcom/squareup/protos/common/Money;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/ui/activity/ReaderResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    new-instance v0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;

    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {v1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;-><init>(Lcom/squareup/ui/NfcProcessor;Lio/reactivex/SingleEmitter;)V

    .line 118
    :try_start_0
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {p1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result p1

    if-nez p1, :cond_0

    .line 119
    sget-object p1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    .line 122
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {p1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;

    move-result-object p1

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {v1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getScope$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lmortar/MortarScope;

    move-result-object v1

    .line 124
    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-static {v2}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$nfcErrorHandler(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    move-result-object v2

    .line 122
    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/NfcProcessor;->registerErrorListenerForScope(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {p1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;

    move-result-object p1

    .line 128
    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {v1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getScope$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lmortar/MortarScope;

    move-result-object v1

    .line 129
    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-static {v2}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$nfcAuthDelegate(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    move-result-object v2

    .line 127
    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {p1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;

    move-result-object p1

    .line 133
    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {v1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getScope$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lmortar/MortarScope;

    move-result-object v1

    .line 134
    iget-object v2, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {v2}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getCardReaderListeners$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v2

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-static {v2, v3}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$nfcListenerOverrider(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    move-result-object v2

    .line 132
    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/NfcProcessor;->registerNfcListener(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;)V

    .line 137
    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->this$0:Lcom/squareup/ui/activity/RealCardPresentRefund;

    invoke-static {v1}, Lcom/squareup/ui/activity/RealCardPresentRefund;->access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;->$refundMoney:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v3, "refundMoney.amount"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p1, v1, v2, v3}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$startRefundRequestOnReaders(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Z

    move-result p1

    if-nez p1, :cond_1

    .line 138
    sget-object p1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 141
    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->onReaderError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method
