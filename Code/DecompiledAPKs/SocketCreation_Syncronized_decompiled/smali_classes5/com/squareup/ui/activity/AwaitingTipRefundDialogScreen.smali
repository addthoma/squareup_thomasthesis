.class public Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "AwaitingTipRefundDialogScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Factory;,
        Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Component;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
