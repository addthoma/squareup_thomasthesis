.class public final Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;
.super Ljava/lang/Object;
.source "BillHistoryTenderSection_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final billHistoryRowFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->resourcesProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->clockProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->employeesProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->localeProvider2:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->billHistoryRowFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;",
            ">;"
        }
    .end annotation

    .line 76
    new-instance v11, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static injectBillHistoryRowFactory(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Ljava/lang/Object;)V
    .locals 0

    .line 145
    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    return-void
.end method

.method public static injectClock(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/util/Clock;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static injectEmployees(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/permissions/Employees;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->employees:Lcom/squareup/permissions/Employees;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectLocale(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Ljava/util/Locale;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->locale:Ljava/util/Locale;

    return-void
.end method

.method public static injectLocaleProvider(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 139
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 115
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPasscodeEmployeeManagement(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-void
.end method

.method public static injectPercentageFormatter(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 122
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectResources(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/util/Res;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;->resources:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectFeatures(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/settings/server/Features;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectResources(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/util/Res;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectLocale(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Ljava/util/Locale;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectClock(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/util/Clock;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/text/Formatter;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectPercentageFormatter(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/text/Formatter;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->employeesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Employees;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectEmployees(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/permissions/Employees;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectPasscodeEmployeeManagement(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->localeProvider2:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectLocaleProvider(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Ljavax/inject/Provider;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->billHistoryRowFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectBillHistoryRowFactory(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;)V

    return-void
.end method
