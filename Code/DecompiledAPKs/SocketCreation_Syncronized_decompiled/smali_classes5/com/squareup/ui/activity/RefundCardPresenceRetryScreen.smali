.class public Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "RefundCardPresenceRetryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$RefundCardPresenceRetryScreen$fddipPgRaEtZ43g8KW8CfOqrd2E;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$RefundCardPresenceRetryScreen$fddipPgRaEtZ43g8KW8CfOqrd2E;

    .line 54
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;
    .locals 1

    .line 55
    const-class v0, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/IssueRefundScope;

    .line 57
    new-instance v0, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 30
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 2

    .line 39
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 40
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->refundCardPresenceCoordinator()Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$Factory;

    move-result-object v0

    .line 41
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$Factory;->create(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;)Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 46
    sget v0, Lcom/squareup/activity/R$layout;->activity_applet_contactless_cp_refund_view:I

    return v0
.end method
