.class final enum Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;
.super Ljava/lang/Enum;
.source "ActivityBadge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityBadge$CountSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CountType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

.field public static final enum RAW:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

.field public static final enum RIPENED:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 74
    new-instance v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    const/4 v1, 0x0

    const-string v2, "RIPENED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RIPENED:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    .line 75
    new-instance v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    const/4 v2, 0x1

    const-string v3, "RAW"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RAW:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    .line 73
    sget-object v3, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RIPENED:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RAW:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->$VALUES:[Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->$VALUES:[Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    invoke-virtual {v0}, [Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    return-object v0
.end method
