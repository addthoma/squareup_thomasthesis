.class public final Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;
.super Ljava/lang/Object;
.source "ActivityAppletStarter.kt"

# interfaces
.implements Lcom/squareup/ui/activity/ActivityAppletStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityAppletStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotSupported"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nActivityAppletStarter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ActivityAppletStarter.kt\ncom/squareup/ui/activity/ActivityAppletStarter$NotSupported\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,18:1\n151#2,2:19\n*E\n*S KotlinDebug\n*F\n+ 1 ActivityAppletStarter.kt\ncom/squareup/ui/activity/ActivityAppletStarter$NotSupported\n*L\n16#1,2:19\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u0096\u0001\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;",
        "Lcom/squareup/ui/activity/ActivityAppletStarter;",
        "()V",
        "activateActivityApplet",
        "",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/ui/activity/ActivityAppletStarter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;

    invoke-direct {v0}, Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;-><init>()V

    sput-object v0, Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 20
    const-class v2, Lcom/squareup/ui/activity/ActivityAppletStarter;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/ActivityAppletStarter;

    iput-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;->$$delegate_0:Lcom/squareup/ui/activity/ActivityAppletStarter;

    return-void
.end method


# virtual methods
.method public activateActivityApplet()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletStarter$NotSupported;->$$delegate_0:Lcom/squareup/ui/activity/ActivityAppletStarter;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivityAppletStarter;->activateActivityApplet()V

    return-void
.end method
