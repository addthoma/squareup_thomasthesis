.class public Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Factory;
.super Ljava/lang/Object;
.source "TipSettlementFailedDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/activity/BillHistoryRunner;)V
    .locals 1

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/BillHistoryRunner;->onTipSettlementFailedPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/activity/BillHistoryRunner;)V
    .locals 1

    const/4 v0, 0x1

    .line 34
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/BillHistoryRunner;->onTipSettlementFailedPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 29
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;

    .line 30
    const-class v1, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Component;

    .line 31
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Component;

    invoke-interface {v1}, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Component;->billHistoryController()Lcom/squareup/ui/activity/BillHistoryRunner;

    move-result-object v1

    .line 32
    invoke-static {v0}, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;->access$000(Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$TipSettlementFailedDialogScreen$Factory$2lQ7dohC3dWKt0FYOBMxH8fhnVo;

    invoke-direct {v2, v1}, Lcom/squareup/ui/activity/-$$Lambda$TipSettlementFailedDialogScreen$Factory$2lQ7dohC3dWKt0FYOBMxH8fhnVo;-><init>(Lcom/squareup/ui/activity/BillHistoryRunner;)V

    new-instance v3, Lcom/squareup/ui/activity/-$$Lambda$TipSettlementFailedDialogScreen$Factory$jFWBzHPRrSyrT8qAl0vmdewPklY;

    invoke-direct {v3, v1}, Lcom/squareup/ui/activity/-$$Lambda$TipSettlementFailedDialogScreen$Factory$jFWBzHPRrSyrT8qAl0vmdewPklY;-><init>(Lcom/squareup/ui/activity/BillHistoryRunner;)V

    invoke-virtual {v0, p1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
