.class Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;
.super Ljava/lang/Object;
.source "TendersAwaitingTipLoader.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->load()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Ljava/util/List<",
        "Lcom/squareup/billhistory/model/TenderHistory;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)V
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->access$200(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 167
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-static {v0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->access$100(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Ljava/util/List;)V

    return-void
.end method
