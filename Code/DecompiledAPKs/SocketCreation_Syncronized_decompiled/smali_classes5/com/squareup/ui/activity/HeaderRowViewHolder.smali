.class public Lcom/squareup/ui/activity/HeaderRowViewHolder;
.super Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
.source "HeaderRowViewHolder.java"


# instance fields
.field private final header:Landroid/widget/TextView;

.field private final presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

.field private final resources:Landroid/content/res/Resources;

.field public final rowType:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

.field private slimHeight:F


# direct methods
.method public constructor <init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/content/res/Resources;)V
    .locals 0

    .line 33
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;-><init>(Landroid/view/View;)V

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->resources:Landroid/content/res/Resources;

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/activity/HeaderRowViewHolder;->bindViews()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->rowType:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->header:Landroid/widget/TextView;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_slim_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->slimHeight:F

    return-void
.end method

.method public static inflate(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/HeaderRowViewHolder;
    .locals 3

    .line 18
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bulk_settle_header_row:I

    const/4 v2, 0x0

    .line 19
    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 20
    new-instance v1, Lcom/squareup/ui/activity/HeaderRowViewHolder;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/squareup/ui/activity/HeaderRowViewHolder;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/content/res/Resources;)V

    return-object v1
.end method


# virtual methods
.method public adjustPaddingTop()V
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->header:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->slimHeight:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->header:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->header:Landroid/widget/TextView;

    .line 47
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    .line 46
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->header:Landroid/widget/TextView;

    const/16 v1, 0x53

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method public onBind(I)V
    .locals 0

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->bindHeaderRow(Lcom/squareup/ui/activity/HeaderRowViewHolder;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/HeaderRowViewHolder;->header:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
