.class Lcom/squareup/ui/activity/BulkSettlePresenter$Row;
.super Ljava/lang/Object;
.source "BulkSettlePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/BulkSettlePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Row"
.end annotation


# instance fields
.field private final tender:Lcom/squareup/ui/activity/TenderEditState;

.field final type:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;)V
    .locals 1

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/TenderEditState;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/TenderEditState;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->type:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    .line 69
    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->tender:Lcom/squareup/ui/activity/TenderEditState;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/BulkSettlePresenter$Row;)Lcom/squareup/ui/activity/TenderEditState;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->tender:Lcom/squareup/ui/activity/TenderEditState;

    return-object p0
.end method


# virtual methods
.method employeeToken()Ljava/lang/String;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->tenderHistory()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method tenderHistory()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->tender:Lcom/squareup/ui/activity/TenderEditState;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
