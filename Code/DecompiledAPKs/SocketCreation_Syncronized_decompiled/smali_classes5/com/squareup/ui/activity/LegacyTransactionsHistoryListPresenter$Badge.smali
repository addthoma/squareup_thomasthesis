.class public final enum Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;
.super Ljava/lang/Enum;
.source "LegacyTransactionsHistoryListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Badge"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

.field public static final enum ERROR:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

.field public static final enum EXPIRING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

.field public static final enum PENDING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

.field public static final enum REFUND:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;


# instance fields
.field public final resId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 91
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    sget v1, Lcom/squareup/billhistoryui/R$drawable;->selector_payment_badge_expiring:I

    const/4 v2, 0x0

    const-string v3, "EXPIRING"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->EXPIRING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    .line 92
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    sget v1, Lcom/squareup/billhistoryui/R$drawable;->selector_payment_badge_failure:I

    const/4 v3, 0x1

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->ERROR:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    .line 93
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    sget v1, Lcom/squareup/widgets/pos/R$drawable;->selector_payment_badge_refund:I

    const/4 v4, 0x2

    const-string v5, "REFUND"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->REFUND:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    .line 94
    new-instance v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    sget v1, Lcom/squareup/billhistoryui/R$drawable;->selector_payment_badge_pending:I

    const/4 v5, 0x3

    const-string v6, "PENDING"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->PENDING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    .line 90
    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->EXPIRING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->ERROR:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->REFUND:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->PENDING:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->$VALUES:[Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    iput p3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->resId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;
    .locals 1

    .line 90
    const-class v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->$VALUES:[Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    invoke-virtual {v0}, [Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    return-object v0
.end method
