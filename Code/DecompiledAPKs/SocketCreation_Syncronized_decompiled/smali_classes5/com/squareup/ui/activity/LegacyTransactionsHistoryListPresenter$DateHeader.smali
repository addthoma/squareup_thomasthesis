.class Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistoryListPresenter.java"

# interfaces
.implements Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Header;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DateHeader"
.end annotation


# instance fields
.field private final date:Ljava/util/Date;

.field final synthetic this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Ljava/util/Date;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;->date:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public getHeaderText()Ljava/lang/CharSequence;
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$000(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/text/DayAndDateFormatter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;->date:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/squareup/text/DayAndDateFormatter;->date(Ljava/util/Date;)Lcom/squareup/text/DayAndDateFormatter;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$DateHeader;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$100(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/settings/server/Features;

    move-result-object v1

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/squareup/text/DayAndDateFormatter;->format()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/text/DayAndDateFormatter;->toUpperCase()Lcom/squareup/text/DayAndDateFormatter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/text/DayAndDateFormatter;->format()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
