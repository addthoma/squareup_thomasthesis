.class public Lcom/squareup/ui/activity/BillHistoryDetailView;
.super Landroid/widget/LinearLayout;
.source "BillHistoryDetailView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

.field private emptyView:Lcom/squareup/mosaic/core/Root;

.field legacyPresenter:Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private statusContainer:Landroid/view/ViewGroup;

.field private statusMessage:Lcom/squareup/widgets/MessageView;

.field private statusTitle:Landroid/widget/TextView;

.field private viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const-class p2, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;->inject(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    return-void
.end method


# virtual methods
.method billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    return-object v0
.end method

.method clearStatusLink()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusMessage:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method hideStatus()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method linkStatusToSupportUrl(I)V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusMessage:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/activity/BillHistoryDetailView$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/BillHistoryDetailView$2;-><init>(Lcom/squareup/ui/activity/BillHistoryDetailView;I)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 64
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->dropView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 51
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_detail_empty_view_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/Root;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->emptyView:Lcom/squareup/mosaic/core/Root;

    .line 52
    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_receipt_message_panel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 53
    sget v0, Lcom/squareup/billhistoryui/R$id;->status_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusContainer:Landroid/view/ViewGroup;

    .line 54
    sget v0, Lcom/squareup/billhistoryui/R$id;->status_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusTitle:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/squareup/billhistoryui/R$id;->status_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusMessage:Lcom/squareup/widgets/MessageView;

    .line 56
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_details_view_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 57
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->clearGlyph()V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setSupportCenterButton()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget v1, Lcom/squareup/checkout/R$string;->support_center:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonText(I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    new-instance v1, Lcom/squareup/ui/activity/BillHistoryDetailView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/BillHistoryDetailView$1;-><init>(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->showButton()V

    return-void
.end method

.method showContent()V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/ActionBarView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/billhistoryui/R$id;->activity_applet_receipt_content:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showEmptyView(I)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/ActionBarView;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->emptyView:Lcom/squareup/mosaic/core/Root;

    invoke-static {v0, p1}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->updateEmptyView(Lcom/squareup/mosaic/core/Root;I)V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_detail_empty_view_container:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showLoading()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/ActionBarView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bill_details_loading_panel:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideMessage()V

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideButton()V

    goto :goto_0

    .line 84
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->messageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 87
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->viewAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    sget p2, Lcom/squareup/billhistoryui/R$id;->activity_applet_receipt_message_panel:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showStatus(IIZ)V
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showStatus(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method showStatus(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusTitle:Landroid/widget/TextView;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p3, :cond_0

    sget p3, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    :cond_0
    sget p3, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    :goto_0
    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    .line 118
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    iget-object p3, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusTitle:Landroid/widget/TextView;

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->statusMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
