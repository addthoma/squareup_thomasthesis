.class public interface abstract Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;
.super Ljava/lang/Object;
.source "TenderWithCustomerInfoCache.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u00a6\u0002J\'\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0006\u0010\u0008\u001a\u00020\u00062\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00a6\u0002\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
        "",
        "get",
        "",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
        "ids",
        "Lcom/squareup/transactionhistory/TransactionIds;",
        "set",
        "key",
        "value",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract get(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transactionhistory/TransactionIds;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract set(Lcom/squareup/transactionhistory/TransactionIds;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transactionhistory/TransactionIds;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation
.end method
