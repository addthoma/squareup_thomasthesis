.class Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "LegacyTransactionsHistoryListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onInstantDepositButtonClicked(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

.field final synthetic val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 0

    .line 526
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    iput-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 528
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-virtual {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedDebitCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$300(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;

    new-instance v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    sget-object v3, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;->TRANSACTIONS_APPLET:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    invoke-direct {v2, v3}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    iget-object v0, v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne v0, v1, :cond_1

    .line 532
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$300(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;

    sget-object v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 534
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$1;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$400(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    :goto_0
    return-void
.end method
