.class Lcom/squareup/ui/activity/BillHistoryRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "BillHistoryRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/BillHistoryRunner;->continueToRefund()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/BillHistoryRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BillHistoryRunner;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryRunner$1;->this$0:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner$1;->this$0:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BillHistoryRunner;->showFirstIssueRefundScreen(Ljava/lang/String;)V

    return-void
.end method
