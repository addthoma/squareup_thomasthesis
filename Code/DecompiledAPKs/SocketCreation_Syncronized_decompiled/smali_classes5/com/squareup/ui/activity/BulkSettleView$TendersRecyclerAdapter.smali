.class Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "BulkSettleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/BulkSettleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TendersRecyclerAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/BulkSettleView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BulkSettleView;)V
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method canKeyboardMoveToNextRowFrom(I)Z
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->canKeyboardMoveToNextRowFrom(I)Z

    move-result p1

    return p1
.end method

.method public getItemCount()I
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getRowCount()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getRowType(I)Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ordinal()I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 202
    check-cast p1, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;I)V
    .locals 0

    .line 221
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;->onBind(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 202
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
    .locals 2

    .line 205
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->SORT:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 206
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    invoke-static {p1}, Lcom/squareup/ui/activity/BulkSettleView;->access$000(Lcom/squareup/ui/activity/BulkSettleView;)Lcom/squareup/ui/activity/SortRowViewHolder;

    move-result-object p1

    return-object p1

    .line 207
    :cond_0
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 208
    sget-object p2, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-static {p2, v0, p1}, Lcom/squareup/ui/activity/HeaderRowViewHolder;->inflate(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/HeaderRowViewHolder;

    move-result-object p1

    return-object p1

    .line 209
    :cond_1
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ALL_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 210
    sget-object p2, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ALL_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-static {p2, v0, p1}, Lcom/squareup/ui/activity/HeaderRowViewHolder;->inflate(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/HeaderRowViewHolder;

    move-result-object p1

    return-object p1

    .line 211
    :cond_2
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 212
    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {p2}, Lcom/squareup/ui/activity/BulkSettleView;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-static {p2, v0, p1, p0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->inflate(Landroid/content/Context;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;)Lcom/squareup/ui/activity/TenderRowViewHolder;

    move-result-object p1

    return-object p1

    .line 213
    :cond_3
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->VIEW_ALL:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_4

    .line 214
    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object p2, p2, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-static {p2, p1}, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->inflate(Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/ViewAllRowViewHolder;

    move-result-object p1

    return-object p1

    .line 216
    :cond_4
    new-instance p1, Ljava/lang/AssertionError;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, v1

    const-string p2, "Unknown view type %d."

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method onKeyboardActionNextClicked(I)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->onKeyboardActionNextClicked(I)V

    return-void
.end method

.method onTipAmountChanged(Lcom/squareup/ui/activity/TenderRowViewHolder;Ljava/lang/Long;)V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, v0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->isQuickTipOptionUsed()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->onTipAmountChanged(Lcom/squareup/ui/activity/TenderRowViewHolder;Ljava/lang/Long;Z)V

    return-void
.end method

.method public bridge synthetic onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 202
    check-cast p1, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->onViewRecycled(Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;)V

    return-void
.end method

.method public onViewRecycled(Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;)V
    .locals 0

    .line 225
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;->onViewRecycled()V

    return-void
.end method
