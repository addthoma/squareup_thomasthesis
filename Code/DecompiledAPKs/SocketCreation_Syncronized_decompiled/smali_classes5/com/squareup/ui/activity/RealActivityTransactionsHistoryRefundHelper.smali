.class public Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;
.super Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
.source "RealActivityTransactionsHistoryRefundHelper.java"


# instance fields
.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private final res:Lcom/squareup/util/Res;

.field private final tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;


# direct methods
.method constructor <init>(Lcom/squareup/activity/CurrentBill;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public ingestRefundsResponse(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/IssueRefundsResponse;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 3

    .line 48
    iget-object p2, p2, Lcom/squareup/protos/client/bills/IssueRefundsResponse;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->res:Lcom/squareup/util/Res;

    invoke-static {p1, p2, v0}, Lcom/squareup/billhistory/Bills;->toRefundBill(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    .line 51
    iget-object p2, p1, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    .line 52
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->isUnsettledCardTender()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/papersignature/TenderStatusManager;->cacheAsSettledAndScheduleNextUpdateIfNecessary(Ljava/util/Collection;)V

    .line 58
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    .line 59
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    .line 61
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory;->replaceTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    goto :goto_0

    .line 65
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {p2, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->addRefundBill(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {p2, p1}, Lcom/squareup/activity/CurrentBill;->set(Lcom/squareup/billhistory/model/BillHistory;)V

    return-object p1
.end method
