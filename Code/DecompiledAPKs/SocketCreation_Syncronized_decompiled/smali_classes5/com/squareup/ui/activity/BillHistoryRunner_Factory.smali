.class public final Lcom/squareup/ui/activity/BillHistoryRunner_Factory;
.super Ljava/lang/Object;
.source "BillHistoryRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/BillHistoryRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityIssueRefundStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityIssueRefundStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityIssueRefundStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->currentBillProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->activityIssueRefundStarterProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p10, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/BillHistoryRunner_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityIssueRefundStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;)",
            "Lcom/squareup/ui/activity/BillHistoryRunner_Factory;"
        }
    .end annotation

    .line 78
    new-instance v11, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/CurrentBill;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ActivityIssueRefundStarter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)Lcom/squareup/ui/activity/BillHistoryRunner;
    .locals 12

    .line 87
    new-instance v11, Lcom/squareup/ui/activity/BillHistoryRunner;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/activity/BillHistoryRunner;-><init>(Lflow/Flow;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/CurrentBill;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ActivityIssueRefundStarter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/BillHistoryRunner;
    .locals 11

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/activity/CurrentBill;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->activityIssueRefundStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/hudtoaster/HudToaster;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/CurrentBill;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ActivityIssueRefundStarter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)Lcom/squareup/ui/activity/BillHistoryRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner_Factory;->get()Lcom/squareup/ui/activity/BillHistoryRunner;

    move-result-object v0

    return-object v0
.end method
