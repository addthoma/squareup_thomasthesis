.class public Lcom/squareup/ui/activity/ActivityAppletScopeRunner;
.super Ljava/lang/Object;
.source "ActivityAppletScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;
.implements Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;


# instance fields
.field private final allTransactionsHistoryLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

.field private final applet:Lcom/squareup/ui/activity/ActivityApplet;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private final searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

.field private final showFullHistoryPermissionController:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;

.field private final tenderCounterScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/activity/CurrentBill;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->applet:Lcom/squareup/ui/activity/ActivityApplet;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->allTransactionsHistoryLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    .line 53
    iput-object p6, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    .line 54
    iput-object p7, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->cacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    .line 55
    iput-object p8, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->tenderCounterScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    .line 56
    iput-object p9, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 57
    iput-object p10, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 58
    iput-object p11, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->showFullHistoryPermissionController:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    return-void
.end method


# virtual methods
.method public checkIfEligibleForInstantDeposit()V
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v2, 0x0

    .line 103
    invoke-interface {v1, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public instantDepositResultScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    iget-object v1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/activity/CurrentBill;->registerOnBadBus(Lmortar/MortarScope;Lcom/squareup/badbus/BadBus;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->applet:Lcom/squareup/ui/activity/ActivityApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityApplet;->select()V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->cacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-virtual {v0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->scheduleNextUpdateIfNecessary()V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->allTransactionsHistoryLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->tenderCounterScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->load()V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringActivitySearch()Z

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->cacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-virtual {v0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cancel()V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->exitingActivitySearch()Z

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->showFullHistoryPermissionController:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->unsetPermission()V

    return-void
.end method

.method public onLinkDebitCardResult()V
    .locals 0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->checkIfEligibleForInstantDeposit()V

    return-void
.end method

.method public onPriceChangeDismissed()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->setIsConfirmingInstantTransfer()V

    return-void
.end method

.method public sendInstantDeposit()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->sendInstantDeposit()Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
