.class final Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2$1;
.super Ljava/lang/Object;
.source "BillHistoryPresenterHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;->apply(Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
        "it",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $tender:Lcom/squareup/billhistory/model/TenderHistory;


# direct methods
.method constructor <init>(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2$1;->$tender:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/Optional;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2$1;->$tender:Lcom/squareup/billhistory/model/TenderHistory;

    const-string v2, "tender"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;-><init>(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2$1;->apply(Lcom/squareup/util/Optional;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;

    move-result-object p1

    return-object p1
.end method
