.class public Lcom/squareup/ui/activity/TransactionsHistoryView;
.super Landroid/widget/LinearLayout;
.source "TransactionsHistoryView.java"


# static fields
.field private static final BUNDLE_SEARCH_BAR_FOCUS_KEY:Ljava/lang/String; = "com.squareup.ui.activity.search.bar.focus"

.field private static final BUNDLE_SUPER_KEY:Ljava/lang/String; = "com.squareup.ui.activity.bundle.super.key"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field appletSelection:Lcom/squareup/applet/AppletSelection;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field badgePresenter:Lcom/squareup/applet/BadgePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchBar:Lcom/squareup/ui/activity/SearchBar;

.field private swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field private tapStateMessageView:Lcom/squareup/widgets/MessageView;

.field private topMessagePanel:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;->inject(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/TransactionsHistoryView;)Lcom/squareup/ui/activity/SearchBar;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    return-object p0
.end method

.method private bindViews()V
    .locals 2

    .line 322
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 323
    sget v0, Lcom/squareup/billhistoryui/R$id;->transactions_history_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 324
    sget v0, Lcom/squareup/billhistoryui/R$id;->swipe_refresh:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    new-instance v0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$oIK2Pnfeq_LBnx1smQnDb-B6nqU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$oIK2Pnfeq_LBnx1smQnDb-B6nqU;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;-><init>(Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)V

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    goto :goto_0

    .line 333
    :cond_0
    new-instance v0, Lcom/squareup/ui/activity/DeprecatedSearchBar;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/DeprecatedSearchBar;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    .line 335
    :goto_0
    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel_deprecated:I

    .line 336
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    .line 337
    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->topMessagePanel:Landroid/widget/LinearLayout;

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->topMessagePanel:Landroid/widget/LinearLayout;

    sget v1, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel_tap_state_message:I

    .line 339
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    .line 340
    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_center_message_panel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method private setupTapStateMessage(Z)V
    .locals 3

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    if-eqz p1, :cond_0

    sget v1, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_enabled:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_disabled:I

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_1

    sget v2, Lcom/squareup/billhistoryui/R$style;->TextAppearance_TransactionsHistory_TopMessagePanel_TapEnabledMessage:I

    goto :goto_1

    :cond_1
    sget v2, Lcom/squareup/billhistoryui/R$style;->TextAppearance_TransactionsHistory_TopMessagePanel_TapDisabledMessage:I

    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/MessageView;->setTextAppearance(Landroid/content/Context;I)V

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    new-instance p1, Lcom/squareup/ui/activity/TransactionsHistoryView$3;

    invoke-direct {p1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryView$3;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    :goto_2
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method private setupTopMessage(Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    .line 249
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setupTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setupTopMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->topMessagePanel:Landroid/widget/LinearLayout;

    sget v1, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel_title:I

    .line 254
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 256
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/16 v2, 0x8

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 257
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 258
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 260
    :cond_0
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 263
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->topMessagePanel:Landroid/widget/LinearLayout;

    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel_message:I

    .line 264
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    .line 265
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->tapStateMessageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method handleSearchByCard()V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0}, Lcom/squareup/ui/activity/SearchBar;->clearFocus()V

    .line 214
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method hideSoftKeyboard()V
    .locals 0

    .line 318
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$bindViews$6$TransactionsHistoryView()Lkotlin/Unit;
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0}, Lcom/squareup/ui/activity/SearchBar;->requestFocus()Z

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSearchAction(Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0}, Lcom/squareup/ui/activity/SearchBar;->clearFocus()V

    .line 330
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$null$1$TransactionsHistoryView(Lcom/squareup/applet/Applet;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 86
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/billhistoryui/R$string;->transactions_history_activity:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$TransactionsHistoryView()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$FTZqTcrgC2uNya-YnXV77ElRuPI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$FTZqTcrgC2uNya-YnXV77ElRuPI;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    .line 84
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$3$TransactionsHistoryView(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSearchBarTapped()V

    goto :goto_0

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSearchBarLostFocus()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$4$TransactionsHistoryView(Landroid/view/View;)V
    .locals 0

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {p1}, Lcom/squareup/ui/activity/SearchBar;->isFocused()Z

    move-result p1

    if-nez p1, :cond_0

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSearchBarTapped()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$5$TransactionsHistoryView()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSwipeRefresh()V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$TransactionsHistoryView()V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onEnableTapClicked()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 83
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$OkQYhTMbRHo4V3fEGiPFOHYYzwU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$OkQYhTMbRHo4V3fEGiPFOHYYzwU;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->takeView(Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    new-instance v1, Lcom/squareup/ui/activity/TransactionsHistoryView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryView$1;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/SearchBar;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$wMRUhZbiB0XimpfBpJNBsQV7ip4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$wMRUhZbiB0XimpfBpJNBsQV7ip4;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/SearchBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$58kMLyQGe6zcKZ-ykGY3-AuLA0Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$58kMLyQGe6zcKZ-ykGY3-AuLA0Y;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/SearchBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    new-instance v1, Lcom/squareup/ui/activity/TransactionsHistoryView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryView$2;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/SearchBar;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getDeprecatedHintTextResId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarHint(I)V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$LrSW1XppwGfAZNiZXTe8yLepinQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$LrSW1XppwGfAZNiZXTe8yLepinQ;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/SearchBar;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->dropView(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    .line 145
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 68
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->bindViews()V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->clearGlyph()V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$8HN8wY1WRpbux54ylIbRQN8w7u0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryView$8HN8wY1WRpbux54ylIbRQN8w7u0;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->setOnTextViewClickedListener(Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;)V

    :cond_0
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    .line 150
    invoke-static {p0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {p1}, Lcom/squareup/ui/activity/SearchBar;->requestFocus()Z

    const/4 p1, 0x1

    return p1

    .line 154
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 169
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "com.squareup.ui.activity.bundle.super.key"

    .line 170
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    instance-of v0, v0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;

    if-eqz v0, :cond_0

    const-string v0, "com.squareup.ui.activity.search.bar.focus"

    .line 173
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 174
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {p1}, Lcom/squareup/ui/activity/SearchBar;->requestFocus()Z

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 158
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 159
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "com.squareup.ui.activity.bundle.super.key"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    instance-of v2, v1, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;

    if-eqz v2, :cond_0

    .line 163
    invoke-interface {v1}, Lcom/squareup/ui/activity/SearchBar;->isFocused()Z

    move-result v1

    const-string v2, "com.squareup.ui.activity.search.bar.focus"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-object v0
.end method

.method setPromptEnabled(Z)V
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->setTextViewClickable(Z)V

    return-void
.end method

.method setSearchBarEnabled(Z)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/SearchBar;->isEnabled(Z)V

    return-void
.end method

.method setSearchBarHint(I)V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/SearchBar;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method setSearchBarText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0}, Lcom/squareup/ui/activity/SearchBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/SearchBar;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method setSearchBarVisible(Z)V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->searchBar:Lcom/squareup/ui/activity/SearchBar;

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/SearchBar;->setVisibleOrGone(Z)V

    return-void
.end method

.method showDeprecatedSupportCenterButton()V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget v1, Lcom/squareup/checkout/R$string;->support_center:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonText(I)V

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    new-instance v1, Lcom/squareup/ui/activity/TransactionsHistoryView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryView$4;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->showButton()V

    return-void
.end method

.method showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 224
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->hideClickableTextView()V

    goto :goto_0

    .line 227
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->deprecatedTopMessagePanel:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->setClickableText(Ljava/lang/CharSequence;)V

    .line 230
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget p2, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel_deprecated:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showLoading()V
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/billhistoryui/R$id;->loading_panel:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showMessageCenter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 304
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 305
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideMessage()V

    goto :goto_0

    .line 307
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->centerMessagePanel:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 310
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget p2, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_center_message_panel:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showTopMessagePanel(Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    .line 234
    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTopMessagePanel(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method showTopMessagePanel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 238
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setupTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget p2, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showTopMessagePanel(Ljava/lang/String;Z)V
    .locals 0

    .line 243
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setupTopMessage(Ljava/lang/String;)V

    .line 244
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setupTapStateMessage(Z)V

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget p2, Lcom/squareup/billhistoryui/R$id;->activity_applet_list_top_message_panel:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method showTransactionsHistory()V
    .locals 2

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/billhistoryui/R$id;->swipe_refresh:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method
