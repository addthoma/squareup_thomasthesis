.class public final enum Lcom/squareup/ui/cardreader/HudToasts;
.super Ljava/lang/Enum;
.source "HudToasts.java"

# interfaces
.implements Lcom/squareup/hudtoaster/HudToaster$ToastBundle;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/cardreader/HudToasts;",
        ">;",
        "Lcom/squareup/hudtoaster/HudToaster$ToastBundle;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_CARD_ERROR:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_NFC_CARD_TAP_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_NFC_COLLISION:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_NFC_UNLOCK_PAYMENT_DEVICE:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_REQUEST_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum ACTIVITY_SEARCH_REQUEST_TAP:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum DIP_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum INVALID_CARD:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum MUST_REINSERT_CARD_FOR_CHARGE:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum MUST_REMOVE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum SWIPE_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum SWIPE_FAILED_SALES_HISTORY_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum SWIPE_FAILED_SALES_HISTORY_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum SWIPE_FAILED_SQUARE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

.field public static final enum TRY_INSERTING_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;


# instance fields
.field private myGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private myMessageId:I

.field private myTitleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 16
    new-instance v6, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/common/strings/R$string;->payment_failed_card_not_charged:I

    sget v5, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_message:I

    const-string v1, "SWIPE_FAILED_TRY_AGAIN"

    const/4 v2, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v6, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    .line 21
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->payment_failed_card_not_charged:I

    sget v12, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_message_swipe_straight:I

    const-string v8, "SWIPE_FAILED_SWIPE_STRAIGHT"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    .line 26
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_title_transactions_history:I

    sget v6, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_message:I

    const-string v2, "SWIPE_FAILED_SALES_HISTORY_TRY_AGAIN"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SALES_HISTORY_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    .line 31
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_title_transactions_history:I

    sget v12, Lcom/squareup/common/strings/R$string;->swipe_failed_bad_swipe_message_swipe_straight:I

    const-string v8, "SWIPE_FAILED_SALES_HISTORY_SWIPE_STRAIGHT"

    const/4 v9, 0x3

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SALES_HISTORY_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    .line 36
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->crm_cardonfile_read_error:I

    sget v6, Lcom/squareup/common/strings/R$string;->crm_cardonfile_swipe_retry:I

    const-string v2, "SWIPE_FAILED_COF_CRM"

    const/4 v3, 0x4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

    .line 41
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->swipe_failed_square_card_confirmation_title:I

    sget v12, Lcom/squareup/common/strings/R$string;->swipe_failed_square_card_confirmation_message:I

    const-string v8, "SWIPE_FAILED_SQUARE_CARD"

    const/4 v9, 0x5

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SQUARE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    .line 46
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->crm_cardonfile_read_error:I

    sget v6, Lcom/squareup/common/strings/R$string;->crm_cardonfile_dip_retry:I

    const-string v2, "DIP_FAILED_COF_CRM"

    const/4 v3, 0x6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->DIP_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

    .line 51
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->swipe_failed_invalid_card_title:I

    sget v12, Lcom/squareup/common/strings/R$string;->swipe_failed_invalid_card_message:I

    const-string v8, "INVALID_CARD"

    const/4 v9, 0x7

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->INVALID_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    .line 55
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->emv_fallback_title:I

    sget v6, Lcom/squareup/common/strings/R$string;->emv_card_error_message:I

    const-string v2, "ACTIVITY_SEARCH_CARD_ERROR"

    const/16 v3, 0x8

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_CARD_ERROR:Lcom/squareup/ui/cardreader/HudToasts;

    .line 59
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->hud_reinsert_chip_card_to_search:I

    const-string v8, "ACTIVITY_SEARCH_MUST_REINSERT_CARD"

    const/16 v9, 0x9

    const/4 v12, 0x0

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    .line 64
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->contactless_unable_to_process_title:I

    sget v6, Lcom/squareup/common/strings/R$string;->emv_card_error_message:I

    const-string v2, "ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE"

    const/16 v3, 0xa

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    .line 69
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->contactless_unlock_phone_to_search_title:I

    sget v12, Lcom/squareup/cardreader/R$string;->contactless_unlock_phone_and_try_again_message:I

    const-string v8, "ACTIVITY_SEARCH_NFC_UNLOCK_PAYMENT_DEVICE"

    const/16 v9, 0xb

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_UNLOCK_PAYMENT_DEVICE:Lcom/squareup/ui/cardreader/HudToasts;

    .line 74
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/cardreader/R$string;->contactless_tap_again_title:I

    sget v6, Lcom/squareup/cardreader/R$string;->contactless_tap_again_message:I

    const-string v2, "ACTIVITY_SEARCH_NFC_CARD_TAP_AGAIN"

    const/16 v3, 0xc

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_CARD_TAP_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    .line 79
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/cardreader/R$string;->contactless_one_card_title:I

    sget v12, Lcom/squareup/cardreader/R$string;->contactless_one_card_message:I

    const-string v8, "ACTIVITY_SEARCH_NFC_COLLISION"

    const/16 v9, 0xd

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_COLLISION:Lcom/squareup/ui/cardreader/HudToasts;

    .line 84
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->emv_request_tap_activity_search_title:I

    sget v6, Lcom/squareup/common/strings/R$string;->emv_request_tap_activity_search_message:I

    const-string v2, "ACTIVITY_SEARCH_REQUEST_TAP"

    const/16 v3, 0xe

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_REQUEST_TAP:Lcom/squareup/ui/cardreader/HudToasts;

    .line 89
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->activity_search_request_swipe:I

    sget v12, Lcom/squareup/common/strings/R$string;->emv_request_swipe_activity_search_message:I

    const-string v8, "ACTIVITY_SEARCH_REQUEST_SWIPE"

    const/16 v9, 0xf

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_REQUEST_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    .line 94
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->hud_reinsert_chip_card:I

    const-string v2, "MUST_REINSERT_CARD"

    const/16 v3, 0x10

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    .line 99
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->hud_reinsert_chip_card_to_charge:I

    const-string v8, "MUST_REINSERT_CARD_FOR_CHARGE"

    const/16 v9, 0x11

    const/4 v12, 0x0

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REINSERT_CARD_FOR_CHARGE:Lcom/squareup/ui/cardreader/HudToasts;

    .line 104
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/strings/R$string;->hud_remove_chip_card:I

    const-string v2, "MUST_REMOVE_CARD"

    const/16 v3, 0x12

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REMOVE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    .line 109
    new-instance v0, Lcom/squareup/ui/cardreader/HudToasts;

    sget-object v10, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v11, Lcom/squareup/common/strings/R$string;->emv_card_error_title:I

    sget v12, Lcom/squareup/common/strings/R$string;->emv_card_error_message:I

    const-string v8, "TRY_INSERTING_AGAIN"

    const/16 v9, 0x13

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/cardreader/HudToasts;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->TRY_INSERTING_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/ui/cardreader/HudToasts;

    .line 13
    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SALES_HISTORY_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SALES_HISTORY_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SQUARE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->DIP_FAILED_COF_CRM:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->INVALID_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_CARD_ERROR:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_UNLOCK_PAYMENT_DEVICE:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_CARD_TAP_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_COLLISION:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_REQUEST_TAP:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_REQUEST_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REINSERT_CARD_FOR_CHARGE:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REMOVE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->TRY_INSERTING_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/cardreader/HudToasts;->$VALUES:[Lcom/squareup/ui/cardreader/HudToasts;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "II)V"
        }
    .end annotation

    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 118
    iput-object p3, p0, Lcom/squareup/ui/cardreader/HudToasts;->myGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 119
    iput p4, p0, Lcom/squareup/ui/cardreader/HudToasts;->myTitleId:I

    .line 120
    iput p5, p0, Lcom/squareup/ui/cardreader/HudToasts;->myMessageId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/cardreader/HudToasts;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/ui/cardreader/HudToasts;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/cardreader/HudToasts;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/cardreader/HudToasts;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->$VALUES:[Lcom/squareup/ui/cardreader/HudToasts;

    invoke-virtual {v0}, [Lcom/squareup/ui/cardreader/HudToasts;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/cardreader/HudToasts;

    return-object v0
.end method


# virtual methods
.method public glyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/cardreader/HudToasts;->myGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public messageId()I
    .locals 1

    .line 132
    iget v0, p0, Lcom/squareup/ui/cardreader/HudToasts;->myMessageId:I

    return v0
.end method

.method public titleId()I
    .locals 1

    .line 128
    iget v0, p0, Lcom/squareup/ui/cardreader/HudToasts;->myTitleId:I

    return v0
.end method
