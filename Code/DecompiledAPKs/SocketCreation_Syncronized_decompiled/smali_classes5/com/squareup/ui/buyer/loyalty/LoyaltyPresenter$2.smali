.class synthetic Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;
.super Ljava/lang/Object;
.source "LoyaltyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$loyalty$MaybeLoyaltyEvent$LoyaltyEvent$Type:[I

.field static final synthetic $SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 550
    invoke-static {}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->values()[Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    sget-object v2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->NON_QUALIFYING_VIEW_TIERS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    sget-object v3, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ENROLLMENT_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v3}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    sget-object v4, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ALL_DONE_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v4}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    sget-object v4, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->REWARD_STATUS_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v4}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 600
    :catch_3
    invoke-static {}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->values()[Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$loyalty$MaybeLoyaltyEvent$LoyaltyEvent$Type:[I

    :try_start_4
    sget-object v3, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$loyalty$MaybeLoyaltyEvent$LoyaltyEvent$Type:[I

    sget-object v4, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->EARN_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    invoke-virtual {v4}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$loyalty$MaybeLoyaltyEvent$LoyaltyEvent$Type:[I

    sget-object v3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ENROLL_WITHOUT_EARNING_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    invoke-virtual {v3}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$loyalty$MaybeLoyaltyEvent$LoyaltyEvent$Type:[I

    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->SHOW_STATUS_WITHOUT_EARNING_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    invoke-virtual {v1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    return-void
.end method
