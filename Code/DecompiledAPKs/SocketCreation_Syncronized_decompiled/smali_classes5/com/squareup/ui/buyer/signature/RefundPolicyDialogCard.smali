.class public Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;
.super Lcom/squareup/container/ContainerTreeKey;
.source "RefundPolicyDialogCard.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogCardScreen;
    value = Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final refundPolicy:Lcom/squareup/ui/buyer/signature/RefundPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 69
    new-instance v0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$1;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/buyer/signature/RefundPolicy;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;->refundPolicy:Lcom/squareup/ui/buyer/signature/RefundPolicy;

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 30
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;->refundPolicy:Lcom/squareup/ui/buyer/signature/RefundPolicy;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
