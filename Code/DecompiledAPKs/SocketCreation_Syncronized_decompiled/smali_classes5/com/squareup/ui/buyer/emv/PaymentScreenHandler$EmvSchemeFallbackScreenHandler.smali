.class public Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;
.super Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmvSchemeFallbackScreenHandler"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Res;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    sget v4, Lcom/squareup/ui/buyerflow/R$string;->emv_scheme_fallback_msg:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;-><init>(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;ILcom/squareup/permissions/PermissionGatekeeper;)V

    return-void
.end method
