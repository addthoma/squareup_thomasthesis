.class public final Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "StoreAndForwardQuickEnableWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStoreAndForwardQuickEnableWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StoreAndForwardQuickEnableWorkflow.kt\ncom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,122:1\n149#2,5:123\n85#3:128\n240#4:129\n276#5:130\n*E\n*S KotlinDebug\n*F\n+ 1 StoreAndForwardQuickEnableWorkflow.kt\ncom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow\n*L\n64#1,5:123\n42#1:128\n42#1:129\n42#1:130\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002*\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0005j\u0004\u0018\u0001`\u00060\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001f\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00022\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016\u00a2\u0006\u0002\u0010\u0017JA\u0010\u0018\u001a\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u00032\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001bH\u0016\u00a2\u0006\u0002\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0003H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "analytics",
        "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "(Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;)V",
        "permissionWorker",
        "Lcom/squareup/workflow/Worker;",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;",
        "snapshotState",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final permissionWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iput-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 41
    sget-object p1, Lcom/squareup/permissions/Permission;->CHANGE_OFFLINE_MODE_SETTING:Lcom/squareup/permissions/Permission;

    invoke-static {p4, p1}, Lcom/squareup/permissions/PermissionGatekeepersKt;->seekPermission(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;)Lio/reactivex/Single;

    move-result-object p1

    .line 128
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$$special$$inlined$asWorker$1;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$$special$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 129
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 130
    sget-object p2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 128
    iput-object p3, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->permissionWorker:Lcom/squareup/workflow/Worker;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)Lcom/squareup/analytics/StoreAndForwardAnalytics;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getOfflineModeMonitor$p(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)Lcom/squareup/payment/OfflineModeMonitor;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget-object p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$ShowingScreen;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$ShowingScreen;

    check-cast p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
            "-",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$1;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p3, p1, v1, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 59
    sget-object p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$ShowingScreen;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$ShowingScreen;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 60
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 61
    new-instance p2, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;

    .line 62
    new-instance p3, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$2;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$2;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 63
    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$3;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 61
    invoke-direct {p2, p3, v0}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 124
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 125
    const-class p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    const-string p3, ""

    invoke-static {p1, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 126
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 124
    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto :goto_0

    .line 66
    :cond_0
    sget-object p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$RequestingPermission;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState$RequestingPermission;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 67
    iget-object v3, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->permissionWorker:Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    new-instance p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;

    invoke-direct {p1, p0}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :goto_0
    return-object v1

    .line 78
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->render(Lkotlin/Unit;Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->snapshotState(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
