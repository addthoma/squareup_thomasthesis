.class public interface abstract Lcom/squareup/ui/buyer/FormattedTotalProvider;
.super Ljava/lang/Object;
.source "FormattedTotalProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0016\u0010\u0004\u001a\u00020\u00032\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
        "",
        "getBuyerFormattedTotalAmount",
        "",
        "getFormattedTotalAmount",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getBuyerFormattedTotalAmount()Ljava/lang/String;
.end method

.method public abstract getFormattedTotalAmount()Ljava/lang/String;
.end method

.method public abstract getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
