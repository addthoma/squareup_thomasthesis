.class public final Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;
.super Ljava/lang/Object;
.source "BuyerOrderTicketNamePresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerAmountTextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final cardholderNameProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final displayNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final emvPaymentStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFetchInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHubScoperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tipDeterminerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            ">;)V"
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p2, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p4, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p5, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p6, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->cardholderNameProcessorProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p7, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p8, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->emvPaymentStarterProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p9, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p10, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->readerHubScoperProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p11, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    .line 85
    iput-object p12, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 86
    iput-object p13, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    .line 87
    iput-object p14, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    .line 88
    iput-object p15, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->displayNameProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            ">;)",
            "Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;"
        }
    .end annotation

    .line 111
    new-instance v16, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/ui/buyer/DisplayNameProvider;)Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;
    .locals 17

    .line 123
    new-instance v16, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/ui/buyer/DisplayNameProvider;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;
    .locals 17

    move-object/from16 v0, p0

    .line 93
    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->cardholderNameProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->emvPaymentStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->readerHubScoperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/payment/TipDeterminerFactory;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    iget-object v1, v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->displayNameProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/buyer/DisplayNameProvider;

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/ui/buyer/DisplayNameProvider;)Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter_Factory;->get()Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    move-result-object v0

    return-object v0
.end method
