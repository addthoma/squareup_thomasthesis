.class public final Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;
.super Ljava/lang/Object;
.source "RetryNonEmvTenderStrategy.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000c\u001a\u00020\nH\u0016J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u000eH\u0016J\u0008\u0010\u0010\u001a\u00020\u000eH\u0016J\u0008\u0010\u0011\u001a\u00020\u000eH\u0016R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u000bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;",
        "Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;",
        "buyerScopeRunner",
        "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;)V",
        "billPayment",
        "Lcom/squareup/payment/BillPayment;",
        "isOfflineCompatible",
        "",
        "()Z",
        "checkBillPaymentForOfflineModeButton",
        "confirmCancelPayment",
        "",
        "maybeAutomaticallyRetryInOfflineMode",
        "onEnterOfflineMode",
        "retryPayment",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billPayment:Lcom/squareup/payment/BillPayment;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerScopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->transaction:Lcom/squareup/payment/Transaction;

    .line 18
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->billPayment:Lcom/squareup/payment/BillPayment;

    return-void
.end method

.method private final isOfflineCompatible()Z
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->billPayment:Lcom/squareup/payment/BillPayment;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->billPayment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->offlineCompatible()Z

    move-result v0

    return v0

    :cond_1
    return v1
.end method


# virtual methods
.method public checkBillPaymentForOfflineModeButton()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public confirmCancelPayment()V
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    return-void
.end method

.method public maybeAutomaticallyRetryInOfflineMode()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->isOfflineCompatible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasGiftCardItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->gotoAuthSpinnerScreen()V

    :cond_0
    return-void
.end method

.method public onEnterOfflineMode()V
    .locals 0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->retryPayment()V

    return-void
.end method

.method public retryPayment()V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->gotoAuthSpinnerScreen()V

    return-void
.end method
