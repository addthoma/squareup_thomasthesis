.class synthetic Lcom/squareup/ui/buyer/BuyerScopeRunner$4;
.super Ljava/lang/Object;
.source "BuyerScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/BuyerScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 796
    invoke-static {}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->values()[Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/buyer/BuyerScopeRunner$4;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/buyer/BuyerScopeRunner$4;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/buyer/BuyerScopeRunner$4;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/buyer/BuyerScopeRunner$4;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/buyer/BuyerScopeRunner$4;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
