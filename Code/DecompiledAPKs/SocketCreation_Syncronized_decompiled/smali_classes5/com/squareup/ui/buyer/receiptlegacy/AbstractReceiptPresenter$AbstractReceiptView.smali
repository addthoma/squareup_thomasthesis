.class public interface abstract Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;
.super Ljava/lang/Object;
.source "AbstractReceiptPresenter.java"

# interfaces
.implements Lcom/squareup/mortar/HasContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AbstractReceiptView"
.end annotation


# virtual methods
.method public abstract asView()Landroid/view/View;
.end method

.method public abstract enableClickAnywhereToFinish()V
.end method

.method public abstract enableSendEmailButton(Z)V
.end method

.method public abstract enableSendSmsButton(Z)V
.end method

.method public abstract getEmailString()Ljava/lang/String;
.end method

.method public abstract getSmsString()Ljava/lang/String;
.end method

.method public abstract onNewSaleClicked()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resourcesUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method public abstract setAmountSubtitle(Ljava/lang/CharSequence;)V
.end method

.method public abstract setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V
.end method

.method public abstract setDigitalReceiptHint(Ljava/lang/CharSequence;)V
.end method

.method public abstract setEmailInputHint(Ljava/lang/String;)V
.end method

.method public abstract setEmailReceiptDisclaimer(Ljava/lang/CharSequence;)V
.end method

.method public abstract setNewSaleText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setNoReceiptText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setSmsInputHint(Ljava/lang/String;)V
.end method

.method public abstract setSmsReceiptDisclaimer(Ljava/lang/CharSequence;)V
.end method

.method public abstract setTicketName(Ljava/lang/CharSequence;)V
.end method

.method public abstract showBuyerLanguageSelection()V
.end method

.method public abstract showCustomerAddCardButton(Z)V
.end method

.method public abstract showCustomerButton(Z)V
.end method

.method public abstract showPrintFormalReceiptButton()V
.end method

.method public abstract showPrintReceiptButton()V
.end method

.method public abstract showSmsInput(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method public abstract updateEmail(Lcom/squareup/util/Res;)V
.end method

.method public abstract updateSms(Lcom/squareup/util/Res;)V
.end method
