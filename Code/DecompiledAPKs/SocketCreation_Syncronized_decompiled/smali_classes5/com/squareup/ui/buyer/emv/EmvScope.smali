.class public final Lcom/squareup/ui/buyer/emv/EmvScope;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "EmvScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/buyer/emv/EmvScope$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/EmvScope$Component;,
        Lcom/squareup/ui/buyer/emv/EmvScope$ComponentFactory;,
        Lcom/squareup/ui/buyer/emv/EmvScope$Module;,
        Lcom/squareup/ui/buyer/emv/EmvScope$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/EmvScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final isContactless:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 565
    sget-object v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$8DuNex1HdGhy0q7wLHLht9dNyq8;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$8DuNex1HdGhy0q7wLHLht9dNyq8;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/EmvScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V
    .locals 0

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    .line 109
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 110
    iput-boolean p3, p0, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/EmvScope;
    .locals 3

    .line 566
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 567
    new-instance v1, Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/CardReaderId;-><init>(I)V

    .line 568
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v2, 0x1

    if-ne p0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 569
    :goto_0
    new-instance p0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 560
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 561
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget p2, p2, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 562
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 119
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_BEGIN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 114
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    .line 115
    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Component;->scopeRunner()Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
