.class public final Lcom/squareup/ui/buyer/receipt/BillReceiptInput;
.super Ljava/lang/Object;
.source "BillReceiptInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008$\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bg\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u000b\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0014J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u0007H\u00c6\u0003J\u0015\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0003J\t\u0010*\u001a\u00020\tH\u00c6\u0003J\t\u0010+\u001a\u00020\u000bH\u00c6\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u000eH\u00c6\u0003J\t\u0010.\u001a\u00020\u000bH\u00c6\u0003J\u0010\u0010/\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0016J\t\u00100\u001a\u00020\u000bH\u00c6\u0003J\u0084\u0001\u00101\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u000b2\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0007H\u00c6\u0001\u00a2\u0006\u0002\u00102J\u0013\u00103\u001a\u00020\u000b2\u0008\u00104\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00105\u001a\u000206H\u00d6\u0001J\t\u00107\u001a\u00020\u0007H\u00d6\u0001R\u0015\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\n\n\u0002\u0010\u0017\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0013\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0013\u0010\u000c\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0019R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0011\u0010\u0012\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010$R\u0011\u0010\u000f\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010$\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receipt/BillReceiptInput;",
        "",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getRemainingBalanceText",
        "Lkotlin/Function1;",
        "Lcom/squareup/util/Res;",
        "",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "showLanguageSelection",
        "",
        "displayName",
        "mostRecentActiveCardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "supportsSms",
        "autoReceiptCompleteTimeout",
        "",
        "showSmsMarketing",
        "businessName",
        "(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V",
        "getAutoReceiptCompleteTimeout",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getBusinessName",
        "()Ljava/lang/String;",
        "getCountryCode",
        "()Lcom/squareup/CountryCode;",
        "getDisplayName",
        "getGetRemainingBalanceText",
        "()Lkotlin/jvm/functions/Function1;",
        "getMostRecentActiveCardReaderId",
        "()Lcom/squareup/cardreader/CardReaderId;",
        "getReceipt",
        "()Lcom/squareup/payment/PaymentReceipt;",
        "getShowLanguageSelection",
        "()Z",
        "getShowSmsMarketing",
        "getSupportsSms",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)Lcom/squareup/ui/buyer/receipt/BillReceiptInput;",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoReceiptCompleteTimeout:Ljava/lang/Long;

.field private final businessName:Ljava/lang/String;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final displayName:Ljava/lang/String;

.field private final getRemainingBalanceText:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final receipt:Lcom/squareup/payment/PaymentReceipt;

.field private final showLanguageSelection:Z

.field private final showSmsMarketing:Z

.field private final supportsSms:Z


# direct methods
.method public constructor <init>(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/CountryCode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Z",
            "Ljava/lang/Long;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "receipt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getRemainingBalanceText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    iput-boolean p4, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    iput-object p5, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-boolean p7, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    iput-object p8, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    iput-boolean p9, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    iput-object p10, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/buyer/receipt/BillReceiptInput;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-boolean v10, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v1, p10

    :goto_9
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->copy(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-object v0
.end method

.method public final component10()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lcom/squareup/CountryCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    return v0
.end method

.method public final component8()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    return-object v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    return v0
.end method

.method public final copy(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)Lcom/squareup/ui/buyer/receipt/BillReceiptInput;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/CountryCode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Z",
            "Ljava/lang/Long;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptInput;"
        }
    .end annotation

    const-string v0, "receipt"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getRemainingBalanceText"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    move-object v1, v0

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;-><init>(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    iget-object v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    iget-object v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    iget-object v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoReceiptCompleteTimeout()Ljava/lang/Long;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    return-object v0
.end method

.method public final getBusinessName()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGetRemainingBalanceText()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-object v0
.end method

.method public final getShowLanguageSelection()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    return v0
.end method

.method public final getShowSmsMarketing()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    return v0
.end method

.method public final getSupportsSms()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BillReceiptInput(receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getRemainingBalanceText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showLanguageSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showLanguageSelection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mostRecentActiveCardReaderId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", supportsSms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->supportsSms:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", autoReceiptCompleteTimeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSmsMarketing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->showSmsMarketing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", businessName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
