.class final Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "BuyerActionContainer.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/BuyerActionContainer;->createSecondaryButton(Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;ZLandroid/widget/LinearLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V",
        "com/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $info$inlined:Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;

.field final synthetic $isFirstElement$inlined:Z


# direct methods
.method constructor <init>(ZLcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;->$isFirstElement$inlined:Z

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;->$info$inlined:Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 37
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 0

    .line 177
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;->$info$inlined:Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->getCommand()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
