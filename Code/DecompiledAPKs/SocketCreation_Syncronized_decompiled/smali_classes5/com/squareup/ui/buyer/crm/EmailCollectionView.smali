.class public Lcom/squareup/ui/buyer/crm/EmailCollectionView;
.super Landroid/widget/FrameLayout;
.source "EmailCollectionView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private allDoneContainer:Landroid/widget/LinearLayout;

.field private allDoneMessage:Landroid/widget/TextView;

.field private emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

.field private enrollmentContents:Landroid/widget/LinearLayout;

.field private newSaleButton:Lcom/squareup/marketfont/MarketButton;

.field private noThanksButton:Lcom/squareup/marketfont/MarketButton;

.field private onKeyboardSendPressed:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onNewSaleButtonClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onNoThanksButtonClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSubmitButtonClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final shortAnimTimeMs:I

.field private submitButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const-class p2, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Component;->inject(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->res:Lcom/squareup/util/Res;

    const/high16 p2, 0x10e0000

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->shortAnimTimeMs:I

    return-void
.end method


# virtual methods
.method public emailText()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedShortText(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$requestInitialFocus$0$EmailCollectionView()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$showAllDone$1$EmailCollectionView()V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 84
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->presenter:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->presenter:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->presenter:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 90
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 59
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 64
    invoke-static {p0}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x14

    .line 65
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 67
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_enroll_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->enrollmentContents:Landroid/widget/LinearLayout;

    .line 68
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_all_done_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->allDoneContainer:Landroid/widget/LinearLayout;

    .line 69
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_all_done_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->allDoneMessage:Landroid/widget/TextView;

    .line 70
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_email:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    .line 71
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_new_sale:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->newSaleButton:Lcom/squareup/marketfont/MarketButton;

    .line 72
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_submit_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->submitButton:Lcom/squareup/marketfont/MarketButton;

    .line 73
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_collection_no_thanks:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->noThanksButton:Lcom/squareup/marketfont/MarketButton;

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->newSaleButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onNewSaleButtonClicked:Lrx/Observable;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->submitButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onSubmitButtonClicked:Lrx/Observable;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->debouncedOnEditorAction(Landroid/widget/TextView;I)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onKeyboardSendPressed:Lrx/Observable;

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->noThanksButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onNoThanksButtonClicked:Lrx/Observable;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onKeyboardSendPressed()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onKeyboardSendPressed:Lrx/Observable;

    return-object v0
.end method

.method public onNewSaleButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onNewSaleButtonClicked:Lrx/Observable;

    return-object v0
.end method

.method public onNoThanksButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onNoThanksButtonClicked:Lrx/Observable;

    return-object v0
.end method

.method public onSubmitButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onSubmitButtonClicked:Lrx/Observable;

    return-object v0
.end method

.method public requestInitialFocus()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->requestFocus()Z

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionView$_dF-pQjpLL2st1heo-6gyCy3r4o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionView$_dF-pQjpLL2st1heo-6gyCy3r4o;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setSubmitButtonEnabled(Z)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->submitButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method public showAllDone(Ljava/lang/CharSequence;)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->allDoneMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailEdit:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    new-instance v0, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionView$9Z0rhfysbBPnPxPHRwOO-zSEMnc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionView$9Z0rhfysbBPnPxPHRwOO-zSEMnc;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->post(Ljava/lang/Runnable;)Z

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->enrollmentContents:Landroid/widget/LinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->allDoneContainer:Landroid/widget/LinearLayout;

    iget v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void
.end method
