.class public Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;
.super Landroid/widget/LinearLayout;
.source "PartialAuthWarningView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private buyerLanguageSelectButton:Lcom/squareup/marketfont/MarketTextView;

.field private continueButton:Landroid/widget/Button;

.field private partialAuthMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field presenter:Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const-class p2, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Component;->inject(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 101
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 102
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->partial_auth_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->partialAuthMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 103
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->charge_and_continue_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->continueButton:Landroid/widget/Button;

    .line 104
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->switch_language_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerLanguageSelectButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 70
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_TWEEN_Y:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$onFinishInflate$0$PartialAuthWarningView()Lkotlin/Unit;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->presenter:Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->onBackPressed()Z

    .line 48
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->presenter:Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->presenter:Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->dropView(Ljava/lang/Object;)V

    .line 66
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->bindViews()V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningView$KsMOqa3pa-SyXl9k7wdigf6VKP8;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningView$KsMOqa3pa-SyXl9k7wdigf6VKP8;-><init>(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->continueButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView$1;-><init>(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerLanguageSelectButton:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView$2;-><init>(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->presenter:Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setButtonText(Ljava/lang/String;)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->continueButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setButtonVisible(Z)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->continueButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method setTitleAndMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->partialAuthMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->partialAuthMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public showLanguageSelection()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerLanguageSelectButton:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method updateLanguageText(Lcom/squareup/locale/LocaleFormatter;Lcom/squareup/util/Res;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerLanguageSelectButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->buyerLanguageSelectButton:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    .line 97
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    const/4 v0, 0x0

    .line 96
    invoke-virtual {p1, p2, v0, v0, v0}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
