.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;
.super Lcom/squareup/ui/LinkSpan;
.source "SignLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderAgreement(Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$viewRefundPolicy$1$1",
        "Lcom/squareup/ui/LinkSpan;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res$inlined:Lcom/squareup/util/Res;

.field final synthetic $showReturnPolicyHandler:Lkotlin/jvm/functions/Function0;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function0;ILcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;->$showReturnPolicyHandler:Lkotlin/jvm/functions/Function0;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;->$res$inlined:Lcom/squareup/util/Res;

    .line 203
    invoke-direct {p0, p2}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;->$showReturnPolicyHandler:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
