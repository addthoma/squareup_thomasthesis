.class public final Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;
.super Ljava/lang/Object;
.source "RetryNonEmvTenderStrategy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy_Factory;->get()Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderStrategy;

    move-result-object v0

    return-object v0
.end method
