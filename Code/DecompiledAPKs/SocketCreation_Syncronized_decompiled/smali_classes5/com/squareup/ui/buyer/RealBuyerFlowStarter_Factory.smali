.class public final Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;
.super Ljava/lang/Object;
.source "RealBuyerFlowStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/RealBuyerFlowStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final currentSecureTouchModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->homeProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->buyerWorkflowProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->currentSecureTouchModeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;)",
            "Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/main/Home;Ldagger/Lazy;Ljava/lang/Object;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/CurrentSecureTouchMode;)Lcom/squareup/ui/buyer/RealBuyerFlowStarter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/ui/main/Home;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ")",
            "Lcom/squareup/ui/buyer/RealBuyerFlowStarter;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    move-object v4, p3

    check-cast v4, Lcom/squareup/ui/buyer/BuyerWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;-><init>(Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/main/Home;Ldagger/Lazy;Lcom/squareup/ui/buyer/BuyerWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/CurrentSecureTouchMode;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/RealBuyerFlowStarter;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/api/ApiTransactionController;

    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/Home;

    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->buyerWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->currentSecureTouchModeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/securetouch/CurrentSecureTouchMode;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->newInstance(Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/main/Home;Ldagger/Lazy;Ljava/lang/Object;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/CurrentSecureTouchMode;)Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter_Factory;->get()Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    move-result-object v0

    return-object v0
.end method
