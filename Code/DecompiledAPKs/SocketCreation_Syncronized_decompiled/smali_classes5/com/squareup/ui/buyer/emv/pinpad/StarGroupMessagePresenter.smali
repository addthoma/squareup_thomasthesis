.class public Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
.super Lmortar/ViewPresenter;
.source "StarGroupMessagePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public setStarCount(I)V
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->setStarCount(I)V

    return-void
.end method

.method public setStarGroupCorrect(Z)V
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->setStarGroupCorrect(Z)V

    return-void
.end method

.method public showErrorMessage(Z)V
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->showMessage(Z)V

    return-void
.end method

.method public showStarGroup()V
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->showStarGroup()V

    return-void
.end method
