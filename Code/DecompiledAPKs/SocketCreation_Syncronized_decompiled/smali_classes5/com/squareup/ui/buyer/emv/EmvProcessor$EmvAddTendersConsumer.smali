.class Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;
.super Ljava/lang/Object;
.source "EmvProcessor.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EmvAddTendersConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final billPayment:Lcom/squareup/payment/BillPayment;

.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 833
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 834
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->billPayment:Lcom/squareup/payment/BillPayment;

    return-void
.end method


# virtual methods
.method public accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)V"
        }
    .end annotation

    .line 839
    new-instance v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$EmvAddTendersConsumer$ERa9o6Fjo59K7doJcnUpTp-H2P4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$EmvAddTendersConsumer$ERa9o6Fjo59K7doJcnUpTp-H2P4;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;)V

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$EmvAddTendersConsumer$KJLGueYOPDyW0S4XlwRP90xaWM4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$EmvAddTendersConsumer$KJLGueYOPDyW0S4XlwRP90xaWM4;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 829
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method public synthetic lambda$accept$0$EmvProcessor$EmvAddTendersConsumer(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 855
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->billPayment:Lcom/squareup/payment/BillPayment;

    .line 856
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->billPayment:Lcom/squareup/payment/BillPayment;

    .line 857
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 858
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->billPayment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->sendArpcToReader(Lcom/squareup/payment/tender/SmartCardTender;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$accept$1$EmvProcessor$EmvAddTendersConsumer(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 861
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 862
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->retryableError()V

    goto :goto_0

    .line 865
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 867
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 868
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 869
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersResponse;

    .line 870
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    .line 873
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1600(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/protos/client/Status;)V

    .line 875
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->billPayment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    .line 876
    iget-object v1, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/tender/SmartCardTender;->setServerError(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->sendArpcToReader(Lcom/squareup/payment/tender/SmartCardTender;)V

    goto :goto_0

    .line 878
    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz p1, :cond_2

    .line 879
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelPayment()V

    :cond_2
    :goto_0
    return-void
.end method
