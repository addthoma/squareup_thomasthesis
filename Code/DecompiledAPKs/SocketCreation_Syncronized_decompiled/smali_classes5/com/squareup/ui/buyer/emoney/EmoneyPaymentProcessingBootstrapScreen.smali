.class public final Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "EmoneyPaymentProcessingBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0003H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "buyerScope",
        "Lcom/squareup/ui/buyer/BuyerScope;",
        "billPayment",
        "Lcom/squareup/payment/BillPayment;",
        "money",
        "Lcom/squareup/protos/common/Money;",
        "brand",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V",
        "getBillPayment",
        "()Lcom/squareup/payment/BillPayment;",
        "getBrand",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "getMoney",
        "()Lcom/squareup/protos/common/Money;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billPayment:Lcom/squareup/payment/BillPayment;

.field private final brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

.field private final buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

.field private final money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V
    .locals 1

    const-string v0, "buyerScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billPayment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "money"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brand"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

    iput-object p2, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->billPayment:Lcom/squareup/payment/BillPayment;

    iput-object p3, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->money:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->Companion:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    move-result-object p1

    .line 23
    new-instance v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->billPayment:Lcom/squareup/payment/BillPayment;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;-><init>(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeInput;

    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->start(Lcom/squareup/ui/buyer/BuyerScopeInput;)V

    return-void
.end method

.method public final getBillPayment()Lcom/squareup/payment/BillPayment;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->billPayment:Lcom/squareup/payment/BillPayment;

    return-object v0
.end method

.method public final getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-object v0
.end method

.method public final getMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/buyer/BuyerScope;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;->getParentKey()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v0

    return-object v0
.end method
