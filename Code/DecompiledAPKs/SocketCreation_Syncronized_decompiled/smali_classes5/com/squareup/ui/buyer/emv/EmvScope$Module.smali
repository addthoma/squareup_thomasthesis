.class public Lcom/squareup/ui/buyer/emv/EmvScope$Module;
.super Ljava/lang/Object;
.source "EmvScope.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/buyer/emv/EmvSecureTouchModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 0

    .line 503
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Module;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideCardReader(Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/CardReader;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 514
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Module;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/EmvScope;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    return-object p1
.end method

.method provideCardReaderInfo(Lcom/squareup/cardreader/CardReader;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 519
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    return-object p1
.end method

.method provideEmvPath()Lcom/squareup/ui/buyer/emv/EmvScope;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 506
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Module;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope;

    return-object v0
.end method

.method providePinListener(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 510
    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$1000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object p1

    return-object p1
.end method
