.class final enum Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;
.super Ljava/lang/Enum;
.source "PinPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

.field public static final enum AWAITING_INPUT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

.field public static final enum INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

.field public static final enum INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

.field public static final enum SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 51
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v1, 0x0

    const-string v2, "AWAITING_INPUT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->AWAITING_INPUT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 52
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v2, 0x1

    const-string v3, "SUBMITTED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 53
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v3, 0x2

    const-string v4, "INCORRECT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 54
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v4, 0x3

    const-string v5, "INCORRECT_FINAL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    .line 50
    sget-object v5, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->AWAITING_INPUT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->SUBMITTED:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->INCORRECT_FINAL:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->$VALUES:[Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;
    .locals 1

    .line 50
    const-class v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->$VALUES:[Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    invoke-virtual {v0}, [Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$State;

    return-object v0
.end method
