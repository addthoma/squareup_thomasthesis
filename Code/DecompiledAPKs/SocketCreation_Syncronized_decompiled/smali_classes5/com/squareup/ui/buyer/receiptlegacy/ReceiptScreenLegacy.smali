.class public final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "ReceiptScreenLegacy.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/InReceiptScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$Responsive;
    phone = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$PhoneComponent;
    tablet = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$TabletComponent;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$TabletComponent;,
        Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$PhoneComponent;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final allowAutocomplete:Z

.field public final smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptScreenLegacy$4x2834Gn1R0Qw4P1voz5_fLDIZU;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptScreenLegacy$4x2834Gn1R0Qw4P1voz5_fLDIZU;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, v1, v0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;-><init>(Lcom/squareup/ui/buyer/BuyerScope;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    .line 43
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->allowAutocomplete:Z

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->uniqueKey:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;
    .locals 1

    .line 74
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 75
    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 69
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 79
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->receipt_view_legacy:I

    return v0
.end method
