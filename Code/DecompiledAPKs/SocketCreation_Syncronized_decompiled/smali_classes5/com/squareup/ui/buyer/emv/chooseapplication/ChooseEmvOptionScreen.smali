.class public Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "ChooseEmvOptionScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/ui/buyer/RequiresBuyerInteraction;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;
    }
.end annotation


# static fields
.field public static final EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final onOptionSelectedAtIndex:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final stringOptions:[Ljava/lang/String;

.field public final stringResOptions:[I

.field public final valueType:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    .line 35
    sget-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->emv_account_selection_credit:I

    .line 36
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 35
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->emv_account_selection_debit:I

    .line 38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 37
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->emv_account_selection_savings:I

    .line 40
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 39
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->emv_account_selection_default:I

    .line 42
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 41
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;[ILrx/functions/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/EmvScope;",
            "[I",
            "Lrx/functions/Action1<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 63
    sget-object p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->STRING_RESOURCE_ID:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->valueType:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    .line 64
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->stringResOptions:[I

    .line 65
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->onOptionSelectedAtIndex:Lrx/functions/Action1;

    const/4 p1, 0x0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->stringOptions:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;[Ljava/lang/String;Lrx/functions/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/EmvScope;",
            "[",
            "Ljava/lang/String;",
            "Lrx/functions/Action1<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 53
    sget-object p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->STRING:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->valueType:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->stringOptions:[Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->onOptionSelectedAtIndex:Lrx/functions/Action1;

    const/4 p1, 0x0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->stringResOptions:[I

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 79
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    .line 80
    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Component;->chooseEmvOptionCoordinator()Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 75
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->emv_choose_option_view:I

    return v0
.end method
