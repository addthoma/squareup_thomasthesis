.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;
.super Ljava/lang/Object;
.source "SignLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/signature/SignatureView$SignatureStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderSignatureView(Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSignLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SignLayoutRunner.kt\ncom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3\n*L\n1#1,468:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016J\u0008\u0010\u0005\u001a\u00020\u0003H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3",
        "Lcom/squareup/signature/SignatureView$SignatureStateListener;",
        "onClearedSignature",
        "",
        "onSigned",
        "onStartedSigning",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;",
            ")V"
        }
    .end annotation

    .line 167
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->$rendering:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearedSignature()V
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->$rendering:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->getClearSignature()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 182
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getSignHereView$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 178
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Clear handler must be set when the signature view is cleared."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onSigned()V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getSignatureView$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/signature/SignatureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/signature/SignatureView;->getSignatureRenderer()Lcom/squareup/signature/SignatureRenderer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->getSignature()Lcom/squareup/signature/SignatureAsJson;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->$rendering:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->getSetSignature()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-interface {v1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$fadeInButtons(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V

    return-void

    .line 169
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Signature should be non-null during onSigned callback."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onStartedSigning()V
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getSignHereView$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$fadeOutButtons(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V

    return-void
.end method
