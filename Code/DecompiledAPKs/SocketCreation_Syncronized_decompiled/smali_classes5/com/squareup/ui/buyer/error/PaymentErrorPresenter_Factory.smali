.class public final Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;
.super Ljava/lang/Object;
.source "PaymentErrorPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;)",
            "Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v2, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter_Factory;->get()Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    move-result-object v0

    return-object v0
.end method
