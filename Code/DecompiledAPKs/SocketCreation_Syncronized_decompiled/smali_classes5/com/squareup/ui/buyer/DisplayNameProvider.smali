.class public final Lcom/squareup/ui/buyer/DisplayNameProvider;
.super Ljava/lang/Object;
.source "DisplayNameProvider.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0013\u001a\u00020\u0008H\u0007J\u0012\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0015H\u0016R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/DisplayNameProvider;",
        "Lmortar/Scoped;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "printerStations",
        "Lcom/squareup/print/PrinterStations;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/print/PrinterStations;)V",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "setName",
        "(Ljava/lang/String;)V",
        "shouldShowDisplayName",
        "",
        "getShouldShowDisplayName",
        "()Z",
        "setShouldShowDisplayName",
        "(Z)V",
        "getDisplayNameText",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private name:Ljava/lang/String;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private shouldShowDisplayName:Z

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/print/PrinterStations;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->printerStations:Lcom/squareup/print/PrinterStations;

    return-void
.end method


# virtual methods
.method public final getDisplayNameText()Ljava/lang/String;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->name:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDefaultOrderName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "transaction.defaultOrderName"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getShouldShowDisplayName()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->shouldShowDisplayName:Z

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 25
    iget-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->name:Ljava/lang/String;

    .line 29
    iget-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {p1}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 30
    iget-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->canStillNameCart()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->shouldShowDisplayName:Z

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->name:Ljava/lang/String;

    return-void
.end method

.method public final setShouldShowDisplayName(Z)V
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/DisplayNameProvider;->shouldShowDisplayName:Z

    return-void
.end method
