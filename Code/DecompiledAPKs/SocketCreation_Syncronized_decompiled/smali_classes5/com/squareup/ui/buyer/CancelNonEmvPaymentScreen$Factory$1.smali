.class Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CancelNonEmvPaymentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;->lambda$create$0(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;

.field final synthetic val$buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field final synthetic val$screen:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;->this$0:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;

    iput-object p2, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;->val$buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iput-object p3, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;->val$screen:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;->val$buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;->val$screen:Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    invoke-static {v1}, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;->access$000(Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->dropPaymentOrTender(Z)V

    return-void
.end method
