.class Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;
.super Ljava/lang/Object;
.source "EmvScope.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/emv/EmvScope$Runner;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderInfo;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field final synthetic val$cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field final synthetic val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

.field final synthetic val$emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

.field final synthetic val$readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field final synthetic val$secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

.field final synthetic val$tenderInEdit:Lcom/squareup/payment/TenderInEdit;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onListAccounts$1$EmvScope$Runner$1([Lcom/squareup/cardreader/lcr/CrPaymentAccountType;Ljava/lang/Integer;)V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    aget-object p1, p1, p2

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public synthetic lambda$onListApplications$0$EmvScope$Runner$1([Lcom/squareup/cardreader/EmvApplication;Ljava/lang/Integer;)V
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    aget-object p1, p1, p2

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public onAuthorizing(Z)V
    .locals 3

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$500(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 256
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$600(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    .line 257
    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$600(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    if-eqz p1, :cond_0

    goto :goto_0

    .line 264
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onCardError()V
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v1, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$ReinsertCard;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$ReinsertCard;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cancelPayment()V

    goto :goto_0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasFallback()Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v1, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$CardRemovedDuringPayment;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$CardRemovedDuringPayment;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 2

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->launch(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public onListAccounts([Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 5

    .line 227
    array-length v0, p1

    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 228
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 229
    sget-object v2, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    aget-object v3, p1, v1

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v2, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    new-instance v4, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$Runner$1$XIgWJP34JcBOxe8dJ3Gs46jzFuQ;

    invoke-direct {v4, p0, p1}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$Runner$1$XIgWJP34JcBOxe8dJ3Gs46jzFuQ;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;[Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    invoke-direct {v2, v3, v0, v4}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;[ILrx/functions/Action1;)V

    invoke-static {v1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 5

    .line 213
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 214
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 215
    aget-object v2, p1, v1

    invoke-virtual {v2}, Lcom/squareup/cardreader/EmvApplication;->getLabel()Ljava/lang/String;

    move-result-object v2

    const-string v3, "eftpos"

    const-string v4, ""

    .line 218
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 219
    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v2, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    new-instance v4, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$Runner$1$T1K7tl7OfVu5zkPWUjJj8Eik-oo;

    invoke-direct {v4, p0, p1}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$Runner$1$T1K7tl7OfVu5zkPWUjJj8Eik-oo;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;[Lcom/squareup/cardreader/EmvApplication;)V

    invoke-direct {v2, v3, v0, v4}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;[Ljava/lang/String;Lrx/functions/Action1;)V

    invoke-static {v1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public onMagSwipeApproved()V
    .locals 3

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public onPaymentApproved()V
    .locals 3

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v1, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-object v2, v2, Lcom/squareup/ui/buyer/emv/EmvScope;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public onPaymentCanceled(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V
    .locals 2

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$700(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 292
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    .line 293
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    .line 294
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    .line 295
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    .line 296
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 297
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 298
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void

    .line 286
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "EmvProcessor should already have unset the active card reader!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V
    .locals 2

    .line 272
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_DECLINED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 274
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    .line 275
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    .line 276
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    .line 277
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    .line 278
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 279
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 280
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 8

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v7, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1}, Lcom/squareup/cardreader/PinRequestData;->getCanSkip()Z

    move-result v3

    .line 244
    invoke-virtual {p1}, Lcom/squareup/cardreader/PinRequestData;->getCardInfo()Lcom/squareup/cardreader/CardInfo;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/cardreader/PinRequestData;->getFinalPinAttempt()Z

    move-result v5

    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$400(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Z

    move-result v6

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;ZLcom/squareup/cardreader/CardInfo;ZZ)V

    .line 243
    invoke-static {v0, v7}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public onSwipeChipCardForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$202(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 199
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->enableSwipePassthroughOnOtherReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 201
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->TECHNICAL:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-ne p1, v0, :cond_0

    .line 202
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v0, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    goto :goto_0

    .line 204
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FALLBACK_SCHEME:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$100(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V

    :goto_0
    return-void
.end method

.method public onUseChipCardDuringFallback()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DIP_REQUIRED_DURING_FALLBACK_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$100(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V

    return-void
.end method

.method public retryableError()V
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v1, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    return-void
.end method
