.class public Lcom/squareup/ui/buyer/BuyerScopeRunner;
.super Ljava/lang/Object;
.source "BuyerScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/pauses/PausesAndResumes;


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

.field private final autoCaptureNotifier:Lcom/squareup/notifications/AutoCaptureNotifier;

.field private final autoCaptureSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/autocapture/PerformAutoCapture;",
            ">;"
        }
    .end annotation
.end field

.field private autoCaptureSubscription:Lrx/Subscription;

.field private final autoVoid:Lcom/squareup/payment/AutoVoid;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final buyerFacingScreensState:Lcom/squareup/ui/buyer/BuyerFacingScreensState;

.field private final buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

.field private final buyerFlowWorkflowRunner:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

.field private final buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

.field private final changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private currentHistory:Lflow/History;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

.field private displayedOrderTicketNameScreen:Z

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

.field private enterFullscreenModeUntil:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private ignoreReceiptAutomaticExit:Z

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field private final isReaderSdk:Z

.field private final loyaltyEvent:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final maybeLoyaltyEvent:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final missedLoyaltyReason:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final performAutoCapture:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/autocapture/PerformAutoCapture;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final receiptHelper:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
            ">;"
        }
    .end annotation
.end field

.field private receiptSelectionMade:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

.field private receiptSender:Lcom/squareup/receipt/ReceiptSender;

.field private final separatedPrintoutsLauncher:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

.field private final softInput:Lcom/squareup/ui/SoftInputPresenter;

.field private final statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

.field private final subs:Lrx/subscriptions/CompositeSubscription;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field private validEmailAddress:Ljava/lang/String;

.field private validSmsNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/notifications/AutoCaptureNotifier;Lcom/squareup/payment/AutoVoid;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadBus;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/buyer/BuyerWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/buyer/BuyerFacingScreensState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiTransactionState;Lflow/Flow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/loyalty/LoyaltyEventPublisher;ZLcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/ui/buyer/DisplayNameProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;Ldagger/Lazy;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            "Lcom/squareup/notifications/AutoCaptureNotifier;",
            "Lcom/squareup/payment/AutoVoid;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/TouchEventMonitor;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            "Lcom/squareup/ui/buyer/BuyerFacingScreensState;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lflow/Flow;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            "Z",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
            ">;",
            "Lcom/squareup/receipt/ReceiptSender;",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 180
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 181
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubject:Lrx/subjects/BehaviorSubject;

    .line 182
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubject:Lrx/subjects/BehaviorSubject;

    new-instance v2, Lcom/squareup/ui/buyer/BuyerScopeRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner$1;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    invoke-virtual {v1, v2}, Lrx/subjects/BehaviorSubject;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->performAutoCapture:Lrx/Observable;

    .line 213
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->UNSET:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptSelectionMade:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    .line 228
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->enterFullscreenModeUntil:Lio/reactivex/subjects/PublishSubject;

    const/4 v1, 0x0

    .line 234
    iput-boolean v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->ignoreReceiptAutomaticExit:Z

    move-object v1, p1

    .line 262
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    move-object v1, p2

    .line 263
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureNotifier:Lcom/squareup/notifications/AutoCaptureNotifier;

    move-object v1, p3

    .line 264
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoVoid:Lcom/squareup/payment/AutoVoid;

    move-object v1, p4

    .line 265
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p5

    .line 266
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->badBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p6

    .line 267
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    move-object v1, p7

    .line 268
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    move-object v1, p8

    .line 269
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    move-object v1, p9

    .line 270
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object v1, p10

    .line 271
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    move-object v1, p11

    .line 272
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object v1, p12

    .line 273
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object/from16 v1, p13

    .line 274
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    move-object/from16 v1, p14

    .line 275
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFacingScreensState:Lcom/squareup/ui/buyer/BuyerFacingScreensState;

    move-object/from16 v1, p15

    .line 276
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v1, p16

    .line 277
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p17

    .line 278
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object/from16 v1, p18

    .line 279
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->softInput:Lcom/squareup/ui/SoftInputPresenter;

    move-object/from16 v1, p19

    .line 280
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object/from16 v1, p20

    .line 281
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-object/from16 v1, p21

    .line 282
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    move-object/from16 v1, p22

    .line 283
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    move-object/from16 v1, p23

    .line 284
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    move-object/from16 v1, p24

    .line 285
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 286
    invoke-virtual/range {p25 .. p25}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->getLoyaltyEvent()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->loyaltyEvent:Lrx/Observable;

    .line 287
    invoke-virtual/range {p25 .. p25}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->getMaybeLoyaltyEvent()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->maybeLoyaltyEvent:Lrx/Observable;

    .line 289
    invoke-virtual/range {p25 .. p25}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->getMissedLoyaltyReason()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->missedLoyaltyReason:Lrx/Observable;

    move/from16 v1, p26

    .line 290
    iput-boolean v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReaderSdk:Z

    move-object/from16 v1, p27

    .line 291
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    move-object/from16 v1, p28

    .line 292
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p29

    .line 293
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    move-object/from16 v1, p30

    .line 294
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    move-object/from16 v1, p31

    .line 295
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowWorkflowRunner:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    move-object/from16 v1, p32

    .line 296
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    move-object/from16 v1, p33

    .line 297
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    move-object/from16 v1, p34

    .line 298
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-object/from16 v1, p35

    .line 299
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    move-object/from16 v1, p36

    .line 300
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    move-object/from16 v1, p37

    .line 301
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->separatedPrintoutsLauncher:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    move-object/from16 v1, p38

    .line 302
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptHelper:Ldagger/Lazy;

    move-object/from16 v1, p39

    .line 303
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    move-object/from16 v1, p40

    .line 304
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/BuyerScopeRunner;)Lcom/squareup/ui/buyer/BuyerScope;
    .locals 0

    .line 146
    iget-object p0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 146
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private afterPaymentOrTenderDropped(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 3

    .line 951
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayedOrderTicketNameScreen:Z

    if-eqz v0, :cond_1

    .line 952
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 954
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 955
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 958
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 959
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 961
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 962
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 963
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->splitTenderSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 964
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 965
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 966
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {p1}, Lcom/squareup/ui/tender/TenderStarter;->goToSplitTender()V

    goto :goto_0

    .line 967
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 968
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireInvoiceId()Ljava/lang/String;

    move-result-object v0

    .line 969
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 970
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v1, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 971
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {p1, v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->invoicePaymentCanceled(Ljava/lang/String;)V

    goto :goto_0

    .line 972
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    .line 973
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mapReason(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/api/ApiErrorResult;

    move-result-object v2

    .line 972
    invoke-virtual {v0, v1, v2}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 974
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 975
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    :cond_4
    :goto_0
    return-void
.end method

.method private disableAutoCapture()V
    .locals 1

    .line 636
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    .line 637
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    const/4 v0, 0x0

    .line 638
    iput-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubscription:Lrx/Subscription;

    :cond_0
    return-void
.end method

.method private enableAutoCapture()V
    .locals 2

    .line 606
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    return-void

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->performAutoCapture:Lrx/Observable;

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$5oEd4rPfzobvZjN-L2g6o0L89Jw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$5oEd4rPfzobvZjN-L2g6o0L89Jw;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubscription:Lrx/Subscription;

    return-void
.end method

.method private finishStoreAndForwardQuickEnableScreen(Z)V
    .locals 4

    .line 820
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->hasEmvPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->dropPaymentAndUnsetCardReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    goto :goto_0

    .line 845
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    .line 846
    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/container/WorkflowTreeKey;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    move-result-object v0

    .line 848
    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    if-eqz p1, :cond_1

    .line 851
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getAuthSpinnerScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 852
    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    .line 854
    :cond_1
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setHistory(Lflow/History;Lflow/Direction;)V

    :goto_0
    return-void
.end method

.method private getAuthSpinnerScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 1126
    new-instance v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method

.method private goTo(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 1031
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlowForFastCheckout(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private handleEmoneyPaymentProcessingResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V
    .locals 3

    .line 441
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult$EmoneySuccess;

    if-eqz v0, :cond_0

    .line 443
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_0

    .line 444
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult$EmoneyAbort;

    if-eqz v0, :cond_1

    .line 446
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 447
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->afterPaymentOrTenderDropped(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :goto_0
    return-void

    .line 449
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This emoney payment result is not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleReceiptResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V
    .locals 3

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    .line 456
    instance-of v1, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptExited;

    if-eqz v1, :cond_1

    .line 457
    check-cast p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptExited;

    .line 458
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptExited$ExitedManually;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptExited$ExitedAutomatically;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->ignoreReceiptAutomaticExit:Z

    if-nez v0, :cond_4

    .line 460
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptExited;->getUniqueKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptExited;->getMaybeMissedOpportunity()Z

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->onReceiptExited(Ljava/lang/String;Z)V

    goto :goto_0

    .line 462
    :cond_1
    instance-of v1, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptSelectionMade;

    if-eqz v1, :cond_2

    .line 463
    check-cast p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptSelectionMade;

    .line 464
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptSelectionMade;->getReceiptSelection()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    move-result-object v0

    .line 465
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$ReceiptSelectionMade;->getUniqueKey()Ljava/lang/String;

    move-result-object p1

    .line 467
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->onReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Ljava/lang/String;)V

    goto :goto_0

    .line 468
    :cond_2
    instance-of v1, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$UpdateCustomerClicked;

    if-eqz v1, :cond_3

    .line 469
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstCrmScreen(Lcom/squareup/payment/PaymentReceipt;)V

    .line 470
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    goto :goto_0

    .line 471
    :cond_3
    instance-of v1, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult$AddCardClicked;

    if-eqz v1, :cond_5

    .line 472
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstAddCardScreen(Lcom/squareup/payment/PaymentReceipt;)V

    :cond_4
    :goto_0
    return-void

    .line 474
    :cond_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This receipt result is not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleSeparatedPrintoutsFinished()V
    .locals 1

    .line 562
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    return-void
.end method

.method private handleTipResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V
    .locals 3

    .line 430
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowTipResult$TipEntered;

    if-eqz v0, :cond_0

    .line 431
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->standAloneTipSelected()V

    goto :goto_0

    .line 432
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowTipResult$ExitTip;

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    .line 433
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    :goto_0
    return-void

    .line 435
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This tip result is not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleWorkflowResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V
    .locals 3

    .line 410
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$SelectingLanguage;

    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showSelectBuyerLanguageScreen()V

    goto :goto_0

    .line 412
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;

    if-eqz v0, :cond_1

    .line 413
    instance-of p1, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult$Enable;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->finishStoreAndForwardQuickEnableScreen(Z)V

    goto :goto_0

    .line 415
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowTipResult;

    if-eqz v0, :cond_2

    .line 416
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->handleTipResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V

    goto :goto_0

    .line 417
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult;

    if-eqz v0, :cond_3

    .line 418
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->handleEmoneyPaymentProcessingResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V

    goto :goto_0

    .line 419
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$BuyerWorkflowReceiptResult;

    if-eqz v0, :cond_4

    .line 420
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->handleReceiptResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V

    goto :goto_0

    .line 421
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$SeparatedPrintoutsFinished;

    if-eqz v0, :cond_5

    .line 422
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->handleSeparatedPrintoutsFinished()V

    :goto_0
    return-void

    .line 424
    :cond_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This workflow result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is not supported"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private hasEmvPayment()Z
    .locals 1

    .line 859
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 860
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 861
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/payment/tender/SmartCardTender;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$gU0ahaacbR7I-AZFiKyTwdU2iIw(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->handleWorkflowResult(Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$1(Lflow/History;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 328
    invoke-virtual {p0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/ContainerTreeKey;

    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onEnterScope$3(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 365
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onReceiptSelectionMade$10(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lkotlin/Unit;
    .locals 0

    .line 488
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$onReceiptSelectionMade$11(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)Lkotlin/Unit;
    .locals 0

    .line 489
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$onReceiptSelectionMade$12(Lkotlin/Unit;)Lkotlin/Unit;
    .locals 0

    .line 490
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method private mapReason(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/api/ApiErrorResult;
    .locals 1

    .line 796
    sget-object v0, Lcom/squareup/ui/buyer/BuyerScopeRunner$4;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 806
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_UNEXPECTED:Lcom/squareup/api/ApiErrorResult;

    return-object p1

    .line 804
    :cond_0
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_READER_INITIATED:Lcom/squareup/api/ApiErrorResult;

    return-object p1

    .line 802
    :cond_1
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/api/ApiErrorResult;

    return-object p1

    .line 800
    :cond_2
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

    return-object p1

    .line 798
    :cond_3
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_HUMAN_INITIATED:Lcom/squareup/api/ApiErrorResult;

    return-object p1
.end method

.method private maybeGoToLoyaltyOrEmailCollectionScreen(Z)V
    .locals 1

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->setHasEmailAddress(Z)V

    .line 534
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToLoyaltyOrEmailCollection()V

    return-void
.end method

.method private normalBuyerFlowCompleted()V
    .locals 2

    .line 924
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->assertNoCard()V

    .line 926
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->assertNoPayment()V

    .line 929
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->checkFees()V

    .line 930
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->checkDiscounts()V

    goto :goto_0

    .line 931
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    if-nez v0, :cond_3

    .line 935
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isTransactionLockModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 936
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPaymentWithAtLeastOneTender()Z

    move-result v0

    if-nez v0, :cond_1

    .line 938
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->clearCurrentEmployee()V

    .line 942
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionController;->handleBuyerFlowCompleted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 943
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    :cond_2
    return-void

    .line 932
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected completed payment after buyer flow completed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onReceiptExited(Ljava/lang/String;Z)V
    .locals 4

    .line 538
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    .line 539
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptHelper:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;

    .line 541
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getReceiptSelectionMade()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-result-object v2

    .line 542
    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 543
    invoke-interface {v3, v2}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 546
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 547
    invoke-interface {p2, v2, p1}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->getSeparatedPrintoutsStartArgsForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    .line 549
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 553
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->checkAndEnqueueMissedOpportunity()V

    .line 555
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isPaymentComplete()Z

    move-result p2

    invoke-virtual {v1, p2, p1}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->endTransaction(ZLjava/lang/String;)V

    .line 556
    invoke-virtual {v1, v0}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->finish(Lcom/squareup/payment/PaymentReceipt;)V

    .line 557
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    :goto_0
    return-void
.end method

.method private onReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Ljava/lang/String;)V
    .locals 5

    .line 480
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v1}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->init(Lio/reactivex/Observable;Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    .line 488
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onGoToLoyaltyScreen()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$mvWPZG9DaObdwXTg02dRAkC6lsg;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$mvWPZG9DaObdwXTg02dRAkC6lsg;

    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    .line 489
    invoke-virtual {v2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onStartSeparatedPrintoutsWorkflow()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$unXa6Qr4cr-e85S_T_6bixOSChs;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$unXa6Qr4cr-e85S_T_6bixOSChs;

    invoke-virtual {v2, v3}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    .line 490
    invoke-virtual {v3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToEmailCollectionScreen()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$qFRWT9ZHL976v1FVJWDo-lO0fSA;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$qFRWT9ZHL976v1FVJWDo-lO0fSA;

    invoke-virtual {v3, v4}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v3

    .line 488
    invoke-static {v1, v2, v3}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$ClW7wBYN1MosZOYFeCjQnEd2Cm4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$ClW7wBYN1MosZOYFeCjQnEd2Cm4;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 491
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 487
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 497
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;->getReceiptSelectionType()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-result-object v0

    .line 498
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 499
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptHelper:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isPaymentComplete()Z

    move-result v2

    invoke-virtual {v1, v2, p2}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->endTransaction(ZLjava/lang/String;)V

    .line 500
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->receiptOptionSelected()V

    .line 502
    instance-of p2, p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    .line 503
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;->getDigitalDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 504
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setDigitalDestination(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V

    .line 505
    instance-of p1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->maybeGoToLoyaltyOrEmailCollectionScreen(Z)V

    .line 506
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    .line 507
    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 510
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    :cond_1
    return-void
.end method

.method private popLastScreen(Ljava/lang/Class;)Lflow/History$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lflow/History$Builder;"
        }
    .end annotation

    .line 1037
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 1038
    invoke-static {v0, p1}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    move-result-object p1

    return-object p1
.end method

.method private restartQuickTimer()V
    .locals 2

    .line 1042
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1043
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {v1, v0}, Lcom/squareup/autocapture/AutoCaptureControl;->restartQuickTimer(Lcom/squareup/payment/RequiresAuthorization;)V

    :cond_0
    return-void
.end method

.method private setDigitalDestination(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 516
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;->getDestinationValue()Ljava/lang/String;

    move-result-object v0

    .line 518
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->hasValidSmsNumber()Z

    move-result v1

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 520
    invoke-interface {v1, v0}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 521
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidSmsNumber(Ljava/lang/String;)V

    .line 524
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->hasValidEmailAddress()Z

    move-result v1

    if-nez v1, :cond_1

    instance-of p1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    if-eqz p1, :cond_1

    .line 526
    invoke-static {v0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 527
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidEmailAddress(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private setHistory(Lflow/History;Lflow/Direction;)V
    .locals 1

    .line 1047
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 1048
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlowForFastCheckout(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0, p1, p2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    :cond_0
    return-void
.end method

.method private showReceiptScreenForContactlessPayment(Lcom/squareup/payment/PaymentReceipt;)Z
    .locals 0

    .line 1059
    invoke-static {p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1061
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->supportsPaperSigAndTip()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 1062
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->shouldSkipReceipt()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private standAloneTipSelected()V
    .locals 4

    .line 870
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isMagStripeTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 871
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->promoteMagStripeCardTender()Lcom/squareup/payment/tender/MagStripeTender;

    move-result-object v0

    .line 872
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 873
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getAuthSpinnerScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    .line 875
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstPostAuthScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    .line 876
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 880
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 881
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->isContactless()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 882
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 884
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreenToStartEmvAuth(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 890
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isInstrumentTender()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 891
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->promoteInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender;

    move-result-object v0

    .line 892
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 893
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 894
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getAuthSpinnerScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    .line 895
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstPostAuthScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    .line 896
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 900
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "Expected a pre-auth tip screen, where no tender should be in edit."

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 902
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsSignature;->askForSignature()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 904
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    invoke-static {v1, v2}, Lcom/squareup/ui/buyer/signature/SignScreen;->getSignScreen(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/buyer/signature/SignScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_3

    .line 906
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v2, v3, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_3
    return-void
.end method


# virtual methods
.method public claimCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 3

    .line 920
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doClaimCoupon(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public completeBuyerFlow(Lcom/squareup/payment/Payment;)V
    .locals 1

    .line 755
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAddCustomerToSaleEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->enqueueAttachContactTask()V

    .line 760
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 761
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 763
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 764
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {p1}, Lcom/squareup/ui/tender/TenderStarter;->goToSplitTender()V

    goto :goto_0

    .line 765
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->hasInvoice()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 766
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCompleted()V

    .line 767
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->invoicePaymentSucceeded()V

    goto :goto_0

    .line 769
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCompleted()V

    .line 770
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->normalBuyerFlowCompleted()V

    :goto_0
    return-void
.end method

.method completeBuyerFlowForFastCheckout(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 4

    .line 988
    instance-of v0, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;

    if-eqz v0, :cond_1

    :cond_0
    instance-of p1, p1, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    if-eqz p1, :cond_2

    :cond_1
    return v1

    .line 996
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFacingScreensState:Lcom/squareup/ui/buyer/BuyerFacingScreensState;

    .line 997
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerFacingScreensState;->isAnyBuyerFacingScreenShown()Z

    move-result v0

    .line 996
    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->shouldSkipReceiptScreen(Z)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 998
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    .line 1002
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->maybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 1004
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showReceiptScreenForContactlessPayment(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    .line 1008
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    .line 1009
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultEmail()Z

    move-result v2

    invoke-interface {v0, v2}, Lcom/squareup/log/CheckoutInformationEventLogger;->setEmailOnFile(Z)V

    .line 1010
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 1011
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v2

    .line 1012
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 1013
    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v3, v2, v0, v1}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 1018
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalanceTextForHud()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1020
    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {v2, v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->prepareRemainingBalanceToast(Ljava/lang/String;)V

    .line 1023
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    :cond_6
    return v1
.end method

.method public confirmCancelPayment(Z)V
    .locals 2

    .line 775
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 776
    new-instance v0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Z)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_0

    .line 778
    :cond_0
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->afterPaymentOrTenderDropped(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :goto_0
    return-void
.end method

.method public contactlessPaymentStartedAfterPreAuthTip()V
    .locals 4

    .line 911
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    .line 912
    invoke-virtual {v2, v3}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreenToStartEmvAuth(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v2

    .line 911
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 783
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 784
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->afterPaymentOrTenderDropped(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method public dropPaymentAndUnsetCardReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 788
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 789
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 791
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    :cond_0
    return-void
.end method

.method public dropPaymentOrTender(Z)V
    .locals 2

    .line 811
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 812
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->afterPaymentOrTenderDropped(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method public exit()V
    .locals 1

    const/4 v0, 0x0

    .line 980
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    return-void
.end method

.method public finishCouponScreen()V
    .locals 2

    .line 916
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doFinishCouponScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public finishPartialAuthWarningScreen()V
    .locals 4

    .line 865
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    .line 866
    invoke-virtual {v2, v3}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getPostPartialAuthWarningScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v2

    .line 865
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public getDisplayNameText()Ljava/lang/String;
    .locals 1

    .line 650
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/DisplayNameProvider;->getDisplayNameText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReceiptSelectionMade()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;
    .locals 1

    .line 654
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptSelectionMade:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-static {v0}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    return-object v0
.end method

.method public getValidEmailAddress()Ljava/lang/String;
    .locals 1

    .line 1105
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->validEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getValidSmsNumber()Ljava/lang/String;
    .locals 1

    .line 1093
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->validSmsNumber:Ljava/lang/String;

    return-object v0
.end method

.method public goToEmailCollectionScreen()V
    .locals 3

    .line 1109
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToFirstAddCardScreen(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 3

    .line 713
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CARD_ON_FILE:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/buyer/BuyerScopeRunner$3;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner$3;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/PaymentReceipt;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public goToFirstCrmScreen(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 3

    .line 702
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/buyer/BuyerScopeRunner$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner$2;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/PaymentReceipt;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public goToFirstScreenAfterAuthSpinner()Z
    .locals 4

    .line 724
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreenAfterAuthApproved(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return v1

    .line 728
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 729
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstPostAuthMagStripeScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    .line 730
    instance-of v3, v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    if-eqz v3, :cond_1

    return v2

    .line 736
    :cond_1
    const-class v2, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    invoke-direct {p0, v2}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->popLastScreen(Ljava/lang/Class;)Lflow/History$Builder;

    move-result-object v2

    .line 737
    invoke-virtual {v2, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 738
    invoke-virtual {v2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setHistory(Lflow/History;Lflow/Direction;)V

    return v1

    .line 741
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    .line 743
    invoke-virtual {v3}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Unexpected screen. Found %s but needed EmvApprovedScreen or AuthSpinnerScreen"

    .line 742
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public goToFirstScreenAfterTicketName()V
    .locals 2

    .line 698
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreenAfterTicketName(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public goToLoyaltyScreen(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 2

    .line 1079
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->matchesOtherTenderIdPair(Lcom/squareup/protos/client/IdPair;)Z

    move-result p1

    const-string v0, "Receipt and LoyaltyEvent Tender do not match."

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1081
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v0, v1, p2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToStoreAndForward()V
    .locals 2

    .line 816
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowWorkflowRunner:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    sget-object v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningStoreAndForwardQuickEnable;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeInput$RunningStoreAndForwardQuickEnable;

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->start(Lcom/squareup/ui/buyer/BuyerScopeInput;)V

    return-void
.end method

.method public gotoAuthSpinnerScreen()V
    .locals 2

    .line 1113
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getAuthSpinnerScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public hasValidEmailAddress()Z
    .locals 1

    .line 1097
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->validEmailAddress:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasValidSmsNumber()Z
    .locals 1

    .line 1085
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->validSmsNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isPaymentComplete()Z
    .locals 1

    .line 666
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isReceiptSelectionMade()Z
    .locals 2

    .line 658
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getReceiptSelectionMade()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-result-object v0

    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->UNSET:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$enableAutoCapture$14$BuyerScopeRunner(Lcom/squareup/autocapture/PerformAutoCapture;)V
    .locals 3

    .line 609
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->dismiss(ZZ)V

    .line 611
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object p1

    .line 612
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->canAutoCapture()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 614
    iget-boolean v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReaderSdk:Z

    if-nez v1, :cond_0

    .line 615
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureNotifier:Lcom/squareup/notifications/AutoCaptureNotifier;

    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/notifications/AutoCaptureNotifier;->show(Lcom/squareup/protos/common/Money;)V

    .line 617
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 621
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    if-eqz v1, :cond_1

    .line 622
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 624
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    .line 625
    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 626
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setHistory(Lflow/History;Lflow/Direction;)V

    goto :goto_0

    .line 629
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoVoid:Lcom/squareup/payment/AutoVoid;

    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/AutoVoid;->reportVoidOnTimeout(Lcom/squareup/protos/common/Money;)V

    .line 630
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$BuyerScopeRunner(Lkotlin/Unit;)V
    .locals 0

    .line 322
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->restartQuickTimer()V

    .line 323
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->tap()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$BuyerScopeRunner(Lflow/History;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 332
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->currentHistory:Lflow/History;

    .line 334
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    .line 338
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disableAutoCapture()V

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->showScreen()V

    .line 344
    invoke-static {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTipScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    .line 345
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/squareup/ui/buyer/RequiresBuyerInteraction;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFacingScreensState:Lcom/squareup/ui/buyer/BuyerFacingScreensState;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerFacingScreensState;->buyerFacingScreenShown()V

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowWorkflowRunner:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    .line 352
    invoke-interface {v0}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->isWorkflowRunning()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 354
    :goto_0
    const-class v3, Lcom/squareup/ui/buyer/PaymentExempt;

    invoke-virtual {p1, v3}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 355
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 356
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    const-string p1, "Screen %s requires a payment (does not implement PaymentExempt)."

    .line 357
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360
    :cond_4
    :goto_1
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayedOrderTicketNameScreen:Z

    instance-of v1, p1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayedOrderTicketNameScreen:Z

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->softInput:Lcom/squareup/ui/SoftInputPresenter;

    sget-object v1, Lcom/squareup/workflow/SoftInputMode;->RESIZE:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/SoftInputPresenter;->prepKeyboardForScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/SoftInputMode;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$4$BuyerScopeRunner(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 366
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->enableAutoCapture()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$5$BuyerScopeRunner(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    .line 383
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->setBuyerSelectedLocale(Ljava/util/Locale;)V

    goto :goto_0

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 385
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 387
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->setBuyerSelectedLanguage(Ljava/util/Locale;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$6$BuyerScopeRunner(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToLoyaltyScreen(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$7$BuyerScopeRunner(Lkotlin/Unit;)V
    .locals 0

    .line 398
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToEmailCollectionScreen()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$8$BuyerScopeRunner(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V
    .locals 0

    .line 402
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$9$BuyerScopeRunner(Ljava/lang/Boolean;)V
    .locals 0

    .line 406
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->ignoreReceiptAutomaticExit:Z

    return-void
.end method

.method public synthetic lambda$onReceiptSelectionMade$13$BuyerScopeRunner(Lkotlin/Unit;)V
    .locals 0

    const/4 p1, 0x1

    .line 492
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->ignoreReceiptAutomaticExit:Z

    .line 493
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->terminate()V

    return-void
.end method

.method public loyaltyEvent()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 1066
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->loyaltyEvent:Lrx/Observable;

    return-object v0
.end method

.method public maybeLoyaltyEvent()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 1070
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->maybeLoyaltyEvent:Lrx/Observable;

    return-object v0
.end method

.method public missedLoyaltyReason()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
            ">;"
        }
    .end annotation

    .line 1074
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->missedLoyaltyReason:Lrx/Observable;

    return-object v0
.end method

.method onAutoCapture(Lcom/squareup/autocapture/PerformAutoCapture;)V
    .locals 1

    .line 602
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->autoCaptureSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 308
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScope;

    iput-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {p1}, Lcom/squareup/mortar/MortarScopes;->completeOnExit(Lmortar/MortarScope;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimer(Lio/reactivex/Completable;)V

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->enterFullscreenModeUntil:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v1}, Lio/reactivex/subjects/PublishSubject;->ignoreElements()Lio/reactivex/Completable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/statusbar/event/StatusBarEventManager;->enterFullscreenMode(Lio/reactivex/Completable;)V

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/ChangeHudToaster;->prepare(Lcom/squareup/payment/CompletedPayment;)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->startCheckoutIfHasNot()V

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    .line 320
    invoke-interface {v1}, Lcom/squareup/ui/TouchEventMonitor;->observeTouchEvents()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$YlBFmkYWKTgq4qL_sCRJ5PnErKQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$YlBFmkYWKTgq4qL_sCRJ5PnErKQ;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 321
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 319
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$E5Ipu_g_cglTHssu-PnhSgoIg6I;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$E5Ipu_g_cglTHssu-PnhSgoIg6I;

    .line 328
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$hu5yhiP9C9uEqkiWYM851JLn1c8;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$hu5yhiP9C9uEqkiWYM851JLn1c8;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 329
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 326
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v1}, Lcom/squareup/ui/main/PosContainer;->topOfTraversalCompleting()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$8Oog_NY1wwZBfQg9pKxiebjbMFQ;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$8Oog_NY1wwZBfQg9pKxiebjbMFQ;

    .line 365
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$lY0T7KePVCMIhhMsg6xitQ9uD_E;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$lY0T7KePVCMIhhMsg6xitQ9uD_E;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 366
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 364
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/autocapture/PerformAutoCapture;

    .line 369
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$w1JjrGhdQZY1xnbXNUVyRtUgZes;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$w1JjrGhdQZY1xnbXNUVyRtUgZes;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 368
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0, p1, p0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    .line 373
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->continueMonitoring()V

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowWorkflowRunner:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$gU0ahaacbR7I-AZFiKyTwdU2iIw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$gU0ahaacbR7I-AZFiKyTwdU2iIw;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 379
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$6adSANZ223WrFF383jLvUX6nGi8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$6adSANZ223WrFF383jLvUX6nGi8;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 380
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 379
    invoke-virtual {p1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 392
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onGoToLoyaltyScreen()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$V0p7A1HWu7S0JkfHE3WWsExZovQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$V0p7A1HWu7S0JkfHE3WWsExZovQ;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 393
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 392
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 397
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onGoToEmailCollectionScreen()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$CCEWwROZ17gFkvqyLkHbbwMzSO8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$CCEWwROZ17gFkvqyLkHbbwMzSO8;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 398
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 397
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 400
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onStartSeparatedPrintoutsWorkflow()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$Iq7irtGi9ciVQyxBelGInajCfBw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$Iq7irtGi9ciVQyxBelGInajCfBw;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 401
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 400
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 405
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onIsFinishing()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$Ko7x7-UOGvuD35uoVstMVBMnjCg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScopeRunner$Ko7x7-UOGvuD35uoVstMVBMnjCg;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;)V

    .line 406
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 405
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 578
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->stopMonitoringSoon()V

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 590
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayedOrderTicketNameScreen:Z

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 593
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->disableAutoCapture()V

    .line 595
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->enterFullscreenModeUntil:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    .line 597
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->emailAndLoyaltyScopeHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->terminate()V

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    .line 573
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->restartQuickTimer()V

    return-void
.end method

.method public setReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V
    .locals 0

    .line 662
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->receiptSelectionMade:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    return-void
.end method

.method public setValidEmailAddress(Ljava/lang/String;)V
    .locals 0

    .line 1101
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->validEmailAddress:Ljava/lang/String;

    return-void
.end method

.method public setValidSmsNumber(Ljava/lang/String;)V
    .locals 0

    .line 1089
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->validSmsNumber:Ljava/lang/String;

    return-void
.end method

.method public shouldShowDisplayNamePerScreen()Z
    .locals 1

    .line 643
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/DisplayNameProvider;->getShouldShowDisplayName()Z

    move-result v0

    return v0
.end method

.method public showLoyaltyStatusDialog()V
    .locals 2

    .line 1122
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyStatusSentDialog;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyStatusSentDialog;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public showReceiptScreen()V
    .locals 3

    .line 747
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public showSelectBuyerLanguageScreen()V
    .locals 2

    .line 751
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;->INSTANCE:Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startSeparatedPrintoutsOrCompleteBuyerFlow()V
    .locals 5

    .line 674
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 675
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getOrderDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 676
    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getUniqueKey()Ljava/lang/String;

    move-result-object v2

    .line 677
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getReceiptSelectionMade()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-result-object v3

    .line 678
    sget-object v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->FORMAL_PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-ne v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    .line 680
    iget-object v4, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-interface {v4}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithPaperReceipt()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 681
    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-ne v3, v0, :cond_2

    sget-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->NORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->FORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    .line 682
    :goto_2
    invoke-static {v1, v2, v0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object v0

    .line 681
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    goto :goto_3

    .line 687
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-interface {v3}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 688
    invoke-static {v1, v2}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->noPaperReceipt(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    goto :goto_3

    .line 690
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isPaymentComplete()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 691
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {v1, v2}, Lcom/squareup/ui/main/TransactionMetrics;->endTransaction(Ljava/lang/String;)V

    .line 693
    :cond_5
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    :goto_3
    return-void
.end method

.method public startSeparatedPrintoutsWorkflow(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V
    .locals 2

    .line 1117
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->enterFullscreenModeUntil:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    .line 1118
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->separatedPrintoutsLauncher:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-interface {v0, v1, p1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;->launchViaBootstrapScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    return-void
.end method
