.class Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;
.super Ljava/lang/Object;
.source "EmvProcessor.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OfflineApprovedEmvAddTendersConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V
    .locals 0

    .line 886
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)V"
        }
    .end annotation

    .line 890
    new-instance v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$OfflineApprovedEmvAddTendersConsumer$hgBbrCoNZEO4hiKvk3gPqLxJSRc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$OfflineApprovedEmvAddTendersConsumer$hgBbrCoNZEO4hiKvk3gPqLxJSRc;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;)V

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$OfflineApprovedEmvAddTendersConsumer$-R4CMJQxmREXMJpYm_5q6oKY9AE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$OfflineApprovedEmvAddTendersConsumer$-R4CMJQxmREXMJpYm_5q6oKY9AE;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 886
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method public synthetic lambda$accept$0$EmvProcessor$OfflineApprovedEmvAddTendersConsumer(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 893
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentApproved()V

    .line 894
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1300(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$accept$1$EmvProcessor$OfflineApprovedEmvAddTendersConsumer(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 896
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 897
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->retryableError()V

    goto :goto_0

    .line 900
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 902
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 903
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 904
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersResponse;

    .line 905
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    .line 908
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1600(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/protos/client/Status;)V

    .line 910
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    new-instance v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1700(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)Z

    goto :goto_0

    .line 912
    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz p1, :cond_2

    .line 913
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelPayment()V

    :cond_2
    :goto_0
    return-void
.end method
