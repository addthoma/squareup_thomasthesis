.class Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;
.super Ljava/lang/Object;
.source "LoyaltyPresenter.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->lambda$onLoad$28(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/util/concurrent/atomic/AtomicBoolean;)Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;",
        ">;"
    }
.end annotation


# instance fields
.field private previous:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

.field final synthetic this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

.field final synthetic val$isLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .line 545
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    iput-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iput-object p3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$isLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 546
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->previous:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;)V
    .locals 4

    .line 549
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->previous:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    .line 550
    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->previous:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v3}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ordinal()I

    move-result v3

    aget v0, v0, v3

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 556
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->hideEnrollment()V

    goto :goto_0

    .line 560
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected loyalty view contents."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 552
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->hideViewTiers()V

    .line 564
    :cond_2
    :goto_0
    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$2;->$SwitchMap$com$squareup$ui$buyer$loyalty$LoyaltyPresenter$ViewContents:[I

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ordinal()I

    move-result v3

    aget v0, v0, v3

    if-eq v0, v2, :cond_6

    if-eq v0, v1, :cond_5

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    goto :goto_1

    .line 578
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$isLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    xor-int/2addr v1, v2

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->shouldShowCashAppBanner()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showRewardStatus(ZZ)V

    goto :goto_1

    .line 566
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$isLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showAllDone(Z)V

    goto :goto_1

    .line 574
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$isLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    xor-int/2addr v1, v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-static {v3}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->access$1200(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v3

    xor-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showEnrollment(ZZ)V

    goto :goto_1

    .line 570
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$view:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->val$isLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    xor-int/2addr v1, v2

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getLoyaltyProgramName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showRewardTiers(ZLjava/lang/String;)V

    .line 581
    :goto_1
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->previous:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 545
    check-cast p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;->call(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;)V

    return-void
.end method
