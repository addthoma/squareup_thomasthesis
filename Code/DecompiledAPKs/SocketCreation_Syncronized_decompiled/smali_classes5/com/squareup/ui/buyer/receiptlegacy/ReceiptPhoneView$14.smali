.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ReceiptPhoneView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->resourcesUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

.field final synthetic val$buyerLocaleOverride:Lcom/squareup/locale/LocaleOverrideFactory;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 583
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;->val$buyerLocaleOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 585
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$14;->val$buyerLocaleOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->sendEmailReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
