.class public interface abstract Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
.super Ljava/lang/Object;
.source "PinPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PinListener"
.end annotation


# virtual methods
.method public abstract onCancel()V
.end method

.method public abstract onClear()V
.end method

.method public abstract onFocusLost()V
.end method

.method public abstract onPinEntered(I)V
.end method

.method public abstract onSkip()V
.end method

.method public abstract onSubmit()V
.end method
