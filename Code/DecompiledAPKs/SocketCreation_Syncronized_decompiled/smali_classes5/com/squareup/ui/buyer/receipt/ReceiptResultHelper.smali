.class public final Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;
.super Ljava/lang/Object;
.source "ReceiptResultHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0010\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u000e\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u001fR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
        "",
        "offlineMode",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "emailAndLoyaltyRunner",
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "transactionMetrics",
        "Lcom/squareup/ui/main/TransactionMetrics;",
        "checkoutInformationEventLogger",
        "Lcom/squareup/log/CheckoutInformationEventLogger;",
        "customerManagementSettings",
        "Lcom/squareup/crm/CustomerManagementSettings;",
        "(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;)V",
        "checkAndEnqueueMissedOpportunity",
        "",
        "endTransaction",
        "isPaymentComplete",
        "",
        "uniqueKey",
        "",
        "enqueueMissedLoyaltyOpportunity",
        "reason",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
        "finish",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final emailAndLoyaltyRunner:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

.field private final res:Lcom/squareup/util/Res;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "offlineMode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emailAndLoyaltyRunner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionMetrics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutInformationEventLogger"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerManagementSettings"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p4, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->emailAndLoyaltyRunner:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    iput-object p5, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    iput-object p7, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    iput-object p8, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    return-void
.end method

.method private final enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->emailAndLoyaltyRunner:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    .line 42
    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v3, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v2, v3}, Lcom/squareup/activity/refund/CreatorDetailsHelper;->forEmployeeToken(Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;)Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    .line 41
    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->logMissedLoyalty(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/CreatorDetails;)V

    return-void
.end method


# virtual methods
.method public final checkAndEnqueueMissedOpportunity()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 33
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    goto :goto_0

    .line 35
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_RECEIPT_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    :goto_0
    return-void
.end method

.method public final endTransaction(ZLjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "uniqueKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {p1, p2}, Lcom/squareup/ui/main/TransactionMetrics;->endTransaction(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final finish(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 4

    const-string v0, "receipt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->emailAndLoyaltyRunner:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->setIsFinishing()V

    .line 59
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string v1, "receipt.payment"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string v1, "payment"

    .line 61
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v1

    .line 62
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 64
    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    const-string/jumbo v3, "tenderType"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v2, v1, v0, v3}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAddCustomerToSaleEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->enqueueAttachContactTask()V

    :cond_1
    return-void
.end method
