.class public interface abstract Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;
.super Ljava/lang/Object;
.source "CardholderNameProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onFailure()V
.end method

.method public abstract onNameReceived(Ljava/lang/String;)V
.end method

.method public abstract onTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
.end method
