.class public final Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;
.super Lcom/squareup/ui/main/errors/PaymentInputHandler;
.source "EmvPaymentInputHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u007f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u00a2\u0006\u0002\u0010 J\u0008\u0010!\u001a\u00020\"H\u0016J\u0010\u0010#\u001a\u00020\"2\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\"2\u0006\u0010\'\u001a\u00020(H\u0014J\u0010\u0010)\u001a\u00020\"2\u0006\u0010*\u001a\u00020+H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "badBus",
        "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
        "emvDipScreenHandler",
        "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
        "smartPaymentFlowStarter",
        "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
        "emvRunner",
        "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
        "readerHudManager",
        "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
        "dippedCardTracker",
        "Lcom/squareup/cardreader/DippedCardTracker;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "readerIssueSink",
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
        "readerIssueNavigator",
        "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
        "swipeValidator",
        "Lcom/squareup/swipe/SwipeValidator;",
        "giftCards",
        "Lcom/squareup/giftcard/GiftCards;",
        "(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)V",
        "initializeWithoutNfc",
        "",
        "navigateForEmvPayment",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "onSwipeFailed",
        "failedSwipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
        "onSwipeSuccess",
        "successfulSwipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)V
    .locals 17
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p6

    const-string v1, "nfcProcessor"

    move-object/from16 v3, p1

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mainThread"

    move-object/from16 v4, p2

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "badBus"

    move-object/from16 v5, p3

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "emvDipScreenHandler"

    move-object/from16 v6, p4

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "smartPaymentFlowStarter"

    move-object/from16 v7, p5

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "emvRunner"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "readerHudManager"

    move-object/from16 v8, p7

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dippedCardTracker"

    move-object/from16 v9, p8

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cardReaderHubUtils"

    move-object/from16 v10, p9

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "transaction"

    move-object/from16 v11, p10

    invoke-static {v11, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "tenderStarter"

    move-object/from16 v12, p11

    invoke-static {v12, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "readerIssueSink"

    move-object/from16 v13, p12

    invoke-static {v13, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "readerIssueNavigator"

    move-object/from16 v14, p13

    invoke-static {v14, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "swipeValidator"

    move-object/from16 v15, p14

    invoke-static {v15, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "giftCards"

    move-object/from16 v2, p15

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v2, p0

    move-object/from16 v16, p15

    .line 48
    invoke-direct/range {v2 .. v16}, Lcom/squareup/ui/main/errors/PaymentInputHandler;-><init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    return-void
.end method


# virtual methods
.method public initializeWithoutNfc()V
    .locals 1

    .line 55
    invoke-super {p0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithoutNfc()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->restoreEmvProcessorAsPaymentCompletionListener()V

    return-void
.end method

.method public navigateForEmvPayment(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goToPreparingPaymentScreen()V

    return-void
.end method

.method protected onSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 2

    const-string v0, "failedSwipe"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;->getPublishEvent()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/main/errors/CardFailed;

    sget-object v1, Lcom/squareup/ui/main/errors/SwipeStraight;->INSTANCE:Lcom/squareup/ui/main/errors/SwipeStraight;

    check-cast v1, Lcom/squareup/ui/main/errors/PaymentError;

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/errors/CardFailed;-><init>(Lcom/squareup/ui/main/errors/PaymentError;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    const-string v0, "successfulSwipe"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
