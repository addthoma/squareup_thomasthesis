.class public Lcom/squareup/ui/buyer/RealBuyerFlowStarter;
.super Ljava/lang/Object;
.source "RealBuyerFlowStarter.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/buyer/BuyerFlowStarter;


# instance fields
.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

.field private final currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final home:Lcom/squareup/ui/main/Home;

.field private isInBuyerFlow:Z

.field private isInTransitionToBuyerFlow:Z

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/main/Home;Ldagger/Lazy;Lcom/squareup/ui/buyer/BuyerWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/CurrentSecureTouchMode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/ui/main/Home;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 53
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInTransitionToBuyerFlow:Z

    .line 54
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInBuyerFlow:Z

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->home:Lcom/squareup/ui/main/Home;

    .line 61
    iput-object p3, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    .line 62
    iput-object p4, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    .line 63
    iput-object p5, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 64
    iput-object p6, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    return-void
.end method

.method private replaceTo(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {v0, p1, v1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method private startBuyerFlow(ZLcom/squareup/ui/buyer/BuyerScope;Z)V
    .locals 3

    const/4 v0, 0x1

    .line 145
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInTransitionToBuyerFlow:Z

    .line 147
    iget-object v1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionController;->buyerFlowStarted()V

    if-eqz p1, :cond_0

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    new-instance v0, Lcom/squareup/container/CalculatedKey;

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;

    invoke-direct {v1, p0, p2, p3}, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;-><init>(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;Lcom/squareup/ui/buyer/BuyerScope;Z)V

    const-string p2, "startBuyerFlow-recreateSellerFlow"

    invoke-direct {v0, p2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 158
    :cond_0
    iget-object p1, p2, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean p1, p1, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    if-eqz p1, :cond_1

    .line 159
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object p1

    .line 161
    iget-object p3, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getPinPadDialog(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p2

    .line 162
    instance-of p3, p2, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;

    new-array v0, v0, [Ljava/lang/Object;

    .line 163
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "buyerScreen is a %s"

    .line 162
    invoke-static {p3, v1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 168
    invoke-virtual {p2}, Lcom/squareup/container/ContainerTreeKey;->getParentKey()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 169
    new-instance v0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    invoke-direct {v0, p3, v2}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 171
    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 173
    iget-object p2, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    invoke-interface {p2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lflow/Flow;

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p3, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {p2, p1, p3}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    goto :goto_0

    .line 175
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    .line 176
    iget-object p2, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    invoke-interface {p2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lflow/Flow;

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    sget-object p3, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {p2, p1, p3}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public goToEmvAuthorizingScreen(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 3

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionController;->buyerFlowStarted()V

    .line 193
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-static {}, Lcom/squareup/ui/buyer/BuyerScope;->create()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    .line 195
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    invoke-direct {p1, v0, v2}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->replaceTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public goToPreparingPaymentScreen(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionController;->buyerFlowStarted()V

    .line 184
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-static {}, Lcom/squareup/ui/buyer/BuyerScope;->create()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    .line 186
    new-instance p1, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;

    invoke-direct {p1, v0}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->replaceTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public isInBuyerScope(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 94
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public isInTransitionToBuyerFlow()Z
    .locals 1

    .line 90
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInTransitionToBuyerFlow:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInBuyerFlow:Z

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$RealBuyerFlowStarter(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 69
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInBuyerFlow:Z

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$RealBuyerFlowStarter(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x0

    .line 73
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInTransitionToBuyerFlow:Z

    return-void
.end method

.method public synthetic lambda$startBuyerFlow$3$RealBuyerFlowStarter(Lcom/squareup/ui/buyer/BuyerScope;ZLflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->home:Lcom/squareup/ui/main/Home;

    invoke-static {v0, p3}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p3

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->buyerWorkflow:Lcom/squareup/ui/buyer/BuyerWorkflow;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    .line 154
    invoke-virtual {p3, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 156
    invoke-virtual {p3}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$startEmoneyBuyerFlow$2$RealBuyerFlowStarter(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->home:Lcom/squareup/ui/main/Home;

    invoke-static {v0, p4}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p4

    .line 134
    new-instance v0, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;

    .line 135
    invoke-static {}, Lcom/squareup/ui/buyer/BuyerScope;->create()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/squareup/ui/buyer/emoney/EmoneyPaymentProcessingBootstrapScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    .line 137
    invoke-virtual {p4, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 139
    invoke-virtual {p4}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$pEkvaW8MYMj-Eu5BrxbzaLagi2s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$pEkvaW8MYMj-Eu5BrxbzaLagi2s;-><init>(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->topOfTraversalCompleting()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$8Dx6otWO7Wb-S9Ln0is9UnZuPLM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$8Dx6otWO7Wb-S9Ln0is9UnZuPLM;-><init>(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public startBuyerFlow()V
    .locals 2

    .line 98
    invoke-static {}, Lcom/squareup/ui/buyer/BuyerScope;->create()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->startBuyerFlow(ZLcom/squareup/ui/buyer/BuyerScope;Z)V

    return-void
.end method

.method public startBuyerFlow(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V
    .locals 1

    .line 106
    invoke-static {p1}, Lcom/squareup/ui/buyer/BuyerScope;->fromContactlessPin(Lcom/squareup/ui/buyer/ContactlessPinRequest;)Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object p1

    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, v0, p1, v0}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->startBuyerFlow(ZLcom/squareup/ui/buyer/BuyerScope;Z)V

    return-void
.end method

.method public startBuyerFlowForHardwarePinRequest(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 4

    .line 112
    invoke-static {}, Lcom/squareup/ui/buyer/BuyerScope;->create()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v0

    const/4 v1, 0x1

    .line 114
    iput-boolean v1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInTransitionToBuyerFlow:Z

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionController;->buyerFlowStarted()V

    .line 116
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v1

    .line 118
    new-instance v2, Lcom/squareup/ui/buyer/emv/EmvScope;

    const/4 v3, 0x0

    invoke-direct {v2, v0, p1, v3}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    .line 119
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    invoke-direct {p1, v2, v3}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    invoke-virtual {v1, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 120
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;

    new-instance v0, Lcom/squareup/securetouch/SecureTouchInput;

    .line 121
    invoke-virtual {p2}, Lcom/squareup/securetouch/SecureTouchPinRequestData;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p2

    iget-object v3, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    .line 122
    invoke-virtual {v3}, Lcom/squareup/securetouch/CurrentSecureTouchMode;->getSecureTouchMode()Lcom/squareup/securetouch/SecureTouchMode;

    move-result-object v3

    invoke-direct {v0, p2, v3}, Lcom/squareup/securetouch/SecureTouchInput;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;Lcom/squareup/securetouch/SecureTouchMode;)V

    invoke-direct {p1, v2, v0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchInput;)V

    .line 120
    invoke-virtual {v1, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    invoke-virtual {v1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p2

    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {p1, p2, v0}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method public startBuyerFlowRecreatingSellerFlow(Z)V
    .locals 2

    .line 102
    invoke-static {}, Lcom/squareup/ui/buyer/BuyerScope;->create()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0, p1}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->startBuyerFlow(ZLcom/squareup/ui/buyer/BuyerScope;Z)V

    return-void
.end method

.method public startEmoneyBuyerFlow(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V
    .locals 3

    const/4 v0, 0x1

    .line 128
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->isInTransitionToBuyerFlow:Z

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionController;->buyerFlowStarted()V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;-><init>(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    const-string p1, "startEmoneyBuyerFlow"

    invoke-direct {v1, p1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
