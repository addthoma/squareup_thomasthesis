.class Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;
.super Ljava/lang/Object;
.source "PinView.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$OnKeyPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/ui/buyer/emv/pinpad/PinView$1;)V
    .locals 0

    .line 141
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V

    return-void
.end method


# virtual methods
.method public onBackspaceClicked()V
    .locals 0

    return-void
.end method

.method public onCancelClicked()V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->pinCancelClicked()V

    return-void
.end method

.method public onClearClicked()V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->clearPinSelected()V

    return-void
.end method

.method public onClearLongpressed()V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->clearPinSelected()V

    return-void
.end method

.method public onDecimalClicked()V
    .locals 0

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 0

    return-void
.end method

.method public onPinDigitEntered(FF)V
    .locals 5

    .line 144
    invoke-static {}, Lcom/squareup/padlock/Padlock$Key;->values()[Lcom/squareup/padlock/Padlock$Key;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 145
    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    invoke-static {v4}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->access$100(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)Lcom/squareup/padlock/Padlock;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/squareup/padlock/Padlock;->getButtonInfo(Lcom/squareup/padlock/Padlock$Key;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v4

    .line 146
    invoke-virtual {v4, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v3}, Lcom/squareup/padlock/Padlock$Key;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->pinDigitEntered(I)V

    return-void

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onSkipClicked()V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->pinSkipClicked()V

    return-void
.end method

.method public onSubmitClicked()V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->onSubmit()V

    return-void
.end method
