.class public Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;
.super Ljava/lang/Object;
.source "CancelEmvPaymentScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$null$0(Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    .line 45
    invoke-virtual {p0}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 48
    :cond_0
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 34
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    .line 35
    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Component;->scopeRunner()Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-result-object v1

    .line 36
    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Component;->permissionPasscodeGatekeeper()Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object v0

    .line 38
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_confirm:I

    new-instance v4, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$VzLmqyz_0NU7wcdEKtKPBbYywTQ;-><init>(Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;Landroid/content/Context;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V

    .line 39
    invoke-virtual {v2, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_cancel:I

    .line 57
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->discard_payment_prompt_message:I

    .line 58
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_title:I

    .line 59
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 61
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 63
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$create$1$CancelEmvPaymentScreen$Factory(Landroid/content/Context;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 41
    invoke-static {p1}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object p1

    .line 43
    new-instance p4, Lcom/squareup/container/CalculatedKey;

    sget-object p5, Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$JYU-N8Axp8zkWvfxXNa43wBE1HQ;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$CancelEmvPaymentScreen$Factory$JYU-N8Axp8zkWvfxXNa43wBE1HQ;

    invoke-direct {p4, p5}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, p4}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 51
    sget-object p1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance p4, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory$1;

    invoke-direct {p4, p0, p3}, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory$1;-><init>(Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V

    invoke-virtual {p2, p1, p4}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
