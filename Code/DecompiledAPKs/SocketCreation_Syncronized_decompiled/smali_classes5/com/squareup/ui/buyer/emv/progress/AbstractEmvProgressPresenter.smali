.class public abstract Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;
.super Lmortar/ViewPresenter;
.source "AbstractEmvProgressPresenter.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;",
        ">;",
        "Lcom/squareup/workflow/ui/HandlesBack;"
    }
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    return-void
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getBuyerFormattedTotalAmount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->setTotal(Ljava/lang/CharSequence;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getBuyerFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method
