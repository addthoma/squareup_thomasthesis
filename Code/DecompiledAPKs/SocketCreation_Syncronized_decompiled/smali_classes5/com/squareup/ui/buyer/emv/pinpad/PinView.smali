.class public Lcom/squareup/ui/buyer/emv/pinpad/PinView;
.super Landroid/widget/LinearLayout;
.source "PinView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;
    }
.end annotation


# instance fields
.field private cardInfo:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private message:Lcom/squareup/widgets/ScalingTextView;

.field private padlock:Lcom/squareup/padlock/Padlock;

.field private final panelHeight:I

.field private final panelWidth:I

.field presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private title:Lcom/squareup/widgets/ScalingTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const-class p2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Component;->inject(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V

    const-string/jumbo p2, "window"

    .line 54
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/WindowManager;

    .line 55
    new-instance p2, Landroid/graphics/Point;

    invoke-direct {p2}, Landroid/graphics/Point;-><init>()V

    .line 56
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 57
    iget p1, p2, Landroid/graphics/Point;->x:I

    iput p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->panelWidth:I

    .line 58
    iget p1, p2, Landroid/graphics/Point;->y:I

    iput p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->panelHeight:I

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)Lcom/squareup/padlock/Padlock;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    return-object p0
.end method


# virtual methods
.method public getPadlock()Lcom/squareup/padlock/Padlock;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 138
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public hideCardInfo()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->cardInfo:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->dropView(Ljava/lang/Object;)V

    .line 82
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 63
    sget v0, Lcom/squareup/pinpad/R$id;->keypad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setSubmitEnabled(Z)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    new-instance v2, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/squareup/ui/buyer/emv/pinpad/PinView$InternalListener;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/ui/buyer/emv/pinpad/PinView$1;)V

    invoke-virtual {v0, v2}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v2, v3, v1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    .line 67
    sget v0, Lcom/squareup/pinpad/R$id;->fake_up_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    .line 68
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 69
    new-instance v1, Lcom/squareup/ui/buyer/emv/pinpad/PinView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView$1;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    sget v0, Lcom/squareup/pinpad/R$id;->pin_card_info:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->cardInfo:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 75
    sget v0, Lcom/squareup/pinpad/R$id;->pin_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->title:Lcom/squareup/widgets/ScalingTextView;

    .line 76
    sget v0, Lcom/squareup/pinpad/R$id;->pin_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->message:Lcom/squareup/widgets/ScalingTextView;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 188
    iget p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->panelWidth:I

    iget v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->panelHeight:I

    const/4 v1, 0x0

    if-le p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 189
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/pinpad/R$dimen;->pin_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/pinpad/R$dimen;->pin_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 191
    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->device:Lcom/squareup/util/Device;

    invoke-interface {v3}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    if-eqz v3, :cond_1

    .line 192
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    .line 195
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 196
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    invoke-static {p2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 195
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1

    .line 198
    :cond_2
    iget p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->panelWidth:I

    invoke-static {p1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    const/4 v0, -0x2

    .line 199
    invoke-static {p2, v1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->getChildMeasureSpec(III)I

    move-result p2

    .line 198
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    :goto_1
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .line 86
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    if-nez p1, :cond_0

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;->windowFocusLost()V

    :cond_0
    return-void
.end method

.method public setAllButtonsEnabled(Z)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setAllButtonsEnabled(Z)V

    return-void
.end method

.method public setCardInfo(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->cardInfo:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->cardInfo:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    return-void
.end method

.method public setDigitsEnabled(Z)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setDigitsEnabled(Z)V

    return-void
.end method

.method public setMessage(ILcom/squareup/util/Res;)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->message:Lcom/squareup/widgets/ScalingTextView;

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPinPadLeftButtonState(Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setPinPadLeftButtonState(Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;)V

    return-void
.end method

.method public setPinPadRightButtonState(Lcom/squareup/padlock/Padlock$PinPadRightButtonState;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setPinPadRightButtonState(Lcom/squareup/padlock/Padlock$PinPadRightButtonState;)V

    return-void
.end method

.method public setTitle(ILcom/squareup/util/Res;)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showCardInfo()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->cardInfo:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method public updatePadlock(Lcom/squareup/util/Res;)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->updateRes(Lcom/squareup/util/Res;)V

    return-void
.end method
