.class final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->addSubscriptions(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 1

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->access$getMainThreadEnforcer$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->access$isFinishing$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->access$getGoToEmailCollectionScreen$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
