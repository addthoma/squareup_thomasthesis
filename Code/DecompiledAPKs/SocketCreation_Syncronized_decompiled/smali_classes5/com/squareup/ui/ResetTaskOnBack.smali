.class public Lcom/squareup/ui/ResetTaskOnBack;
.super Ljava/lang/Object;
.source "ResetTaskOnBack.java"

# interfaces
.implements Lcom/squareup/ui/MainActivityBackHandler;
.implements Lcom/squareup/ui/LocationActivityBackHandler;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationActivityBackPressed(Lcom/squareup/ui/LocationActivity;)V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-static {p1, v0}, Lcom/squareup/ui/PaymentActivity;->exitWithResult(Landroid/app/Activity;I)V

    return-void
.end method

.method public onMainActivityBackPressed(Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x0

    .line 19
    invoke-static {p1, v0}, Lcom/squareup/ui/PaymentActivity;->exitWithResult(Landroid/app/Activity;I)V

    return-void
.end method
