.class Lcom/squareup/ui/DropDownContainer$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DropDownContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/DropDownContainer;->closeDropDown()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/DropDownContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/DropDownContainer;)V
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 357
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/DropDownContainer;->setDropDownVisible(Z)V

    .line 358
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    invoke-static {p1, v0}, Lcom/squareup/ui/DropDownContainer;->access$200(Lcom/squareup/ui/DropDownContainer;Z)V

    .line 359
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object v0, v0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {p1, v0}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownHidden(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 353
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer$2;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object v0, v0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {p1, v0}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownHiding(Landroid/view/View;)V

    :cond_0
    return-void
.end method
