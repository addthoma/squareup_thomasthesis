.class public final synthetic Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

.field private final synthetic f$1:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

.field private final synthetic f$2:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;->f$0:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    iput-object p2, p0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;->f$1:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iput-object p3, p0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;->f$2:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;->f$0:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    iget-object v1, p0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;->f$1:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iget-object v2, p0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;->f$2:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->lambda$fromSettlementReportWrapper$2$BalanceAppletSectionsListPresenter(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Landroid/view/View;)V

    return-void
.end method
