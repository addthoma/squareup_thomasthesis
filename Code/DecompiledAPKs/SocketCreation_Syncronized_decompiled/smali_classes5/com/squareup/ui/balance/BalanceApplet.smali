.class public final Lcom/squareup/ui/balance/BalanceApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "BalanceApplet.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B]\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0016J\u0018\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0014J\u0008\u0010\'\u001a\u00020\u0018H\u0016J\u000e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020*0)H\u0016J\u0006\u0010+\u001a\u00020\u000bJ\u0010\u0010,\u001a\u00020\u00182\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0 H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u0018X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u00020\u001c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001eR\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceApplet;",
        "Lcom/squareup/applet/HistoryFactoryApplet;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "balanceAppletEntryPoint",
        "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "sections",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/ui/balance/BalanceAppletSectionsList;",
        "bizBankingAnalytics",
        "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
        "accountFreeze",
        "Lcom/squareup/accountfreeze/AccountFreeze;",
        "squareCardUpseller",
        "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
        "resources",
        "Landroid/content/res/Resources;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Ldagger/Lazy;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Landroid/content/res/Resources;Lio/reactivex/Scheduler;)V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "initialDetailScreen",
        "Lflow/path/Path;",
        "getInitialDetailScreen",
        "()Lflow/path/Path;",
        "badge",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/applet/Applet$Badge;",
        "getHomeScreens",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "currentHistory",
        "Lflow/History;",
        "getIntentScreenExtra",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "getSections",
        "getText",
        "visibility",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

.field private final analyticsName:Ljava/lang/String;

.field private final balanceAppletEntryPoint:Lcom/squareup/ui/balance/BalanceAppletEntryPoint;

.field private final bizBankingAnalytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final resources:Landroid/content/res/Resources;

.field private final sections:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsList;",
            ">;"
        }
    .end annotation
.end field

.field private final squareCardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Landroid/content/res/Resources;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p9    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsList;",
            ">;",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            "Landroid/content/res/Resources;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAppletEntryPoint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sections"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bizBankingAnalytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountFreeze"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardUpseller"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    iput-object p2, p0, Lcom/squareup/ui/balance/BalanceApplet;->balanceAppletEntryPoint:Lcom/squareup/ui/balance/BalanceAppletEntryPoint;

    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceApplet;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceApplet;->sections:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/ui/balance/BalanceApplet;->bizBankingAnalytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    iput-object p6, p0, Lcom/squareup/ui/balance/BalanceApplet;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    iput-object p7, p0, Lcom/squareup/ui/balance/BalanceApplet;->squareCardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    iput-object p8, p0, Lcom/squareup/ui/balance/BalanceApplet;->resources:Landroid/content/res/Resources;

    iput-object p9, p0, Lcom/squareup/ui/balance/BalanceApplet;->mainScheduler:Lio/reactivex/Scheduler;

    const-string p1, "balance"

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceApplet;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getResources$p(Lcom/squareup/ui/balance/BalanceApplet;)Landroid/content/res/Resources;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/balance/BalanceApplet;->resources:Landroid/content/res/Resources;

    return-object p0
.end method


# virtual methods
.method public badge()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceApplet;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    .line 66
    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreeze;->canShowBadge()Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceApplet;->squareCardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    .line 68
    invoke-interface {v1}, Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;->showUpsell()Lio/reactivex/Observable;

    move-result-object v1

    const/4 v2, 0x0

    .line 70
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    .line 74
    sget-object v2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    const-string v3, "showBalanceUpsell"

    .line 75
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 76
    new-instance v1, Lcom/squareup/ui/balance/BalanceApplet$badge$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/BalanceApplet$badge$1;-><init>(Lcom/squareup/ui/balance/BalanceApplet;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceApplet;->bizBankingAnalytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logShowBalanceClick()V

    .line 56
    sget-object p1, Lcom/squareup/ui/balance/BalanceMasterScreen;->INSTANCE:Lcom/squareup/ui/balance/BalanceMasterScreen;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceApplet;->balanceAppletEntryPoint:Lcom/squareup/ui/balance/BalanceAppletEntryPoint;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/BalanceAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const-string v1, "balanceAppletEntryPoint.initialScreen"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lflow/path/Path;

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "BALANCE"

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 92
    sget-object v0, Lcom/squareup/permissions/Permission;->DEPOSITS_APPLET:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getSections()Lcom/squareup/ui/balance/BalanceAppletSectionsList;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceApplet;->sections:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "sections.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletSectionsList;

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget v0, Lcom/squareup/balance/applet/impl/R$string;->titlecase_balance_applet_name:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026case_balance_applet_name)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_BALANCE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceApplet;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
