.class public final Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;
.super Ljava/lang/Object;
.source "BalanceAppletSectionsList_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/BalanceAppletSectionsList;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p9, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg8Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;)",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;"
        }
    .end annotation

    .line 68
    new-instance v10, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)Lcom/squareup/ui/balance/BalanceAppletSectionsList;
    .locals 11

    .line 75
    new-instance v10, Lcom/squareup/ui/balance/BalanceAppletSectionsList;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/balance/BalanceAppletSectionsList;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/BalanceAppletSectionsList;
    .locals 10

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/balance/BalanceAppletEntryPoint;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)Lcom/squareup/ui/balance/BalanceAppletSectionsList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsList_Factory;->get()Lcom/squareup/ui/balance/BalanceAppletSectionsList;

    move-result-object v0

    return-object v0
.end method
