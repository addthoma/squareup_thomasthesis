.class final Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "AddMoneyWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;->render(Lcom/squareup/ui/balance/addmoney/AddMoneyProps;Lcom/squareup/ui/balance/addmoney/AddMoneyState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
        "+",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        "result",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;

    invoke-direct {v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult$ReturnFromSubmitAmount;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult$ReturnFromSubmitAmount;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;

    sget-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyResult$ReturnFromAddMoney;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyResult$ReturnFromAddMoney;

    check-cast v0, Lcom/squareup/ui/balance/addmoney/AddMoneyResult;

    invoke-direct {p1, v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 120
    :cond_0
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult$FailedAddingFunds;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult$FailedAddingFunds;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;

    sget-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyResult$MoneyCouldNotBeAdded;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyResult$MoneyCouldNotBeAdded;

    check-cast v0, Lcom/squareup/ui/balance/addmoney/AddMoneyResult;

    invoke-direct {p1, v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 121
    :cond_1
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult$SucceededAddingFunds;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult$SucceededAddingFunds;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;

    sget-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyResult$MoneyAddedSuccessfully;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyResult$MoneyAddedSuccessfully;

    check-cast v0, Lcom/squareup/ui/balance/addmoney/AddMoneyResult;

    invoke-direct {p1, v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$render$3;->invoke(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
