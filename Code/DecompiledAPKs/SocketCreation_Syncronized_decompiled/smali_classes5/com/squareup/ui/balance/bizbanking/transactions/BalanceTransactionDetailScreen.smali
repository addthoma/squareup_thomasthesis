.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "BalanceTransactionDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;,
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;

    .line 38
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;

    .line 39
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 26
    const-class v0, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 34
    const-class v0, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    .line 35
    invoke-interface {p1}, Lcom/squareup/ui/balance/BalanceAppletScope$Component;->balanceTransactionDetailCoordinator()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 30
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->balance_transaction_detail_view:I

    return v0
.end method
