.class public final enum Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;
.super Ljava/lang/Enum;
.source "TransferToBankRequester.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DepositStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;",
        "",
        "(Ljava/lang/String;I)V",
        "hasTransferResult",
        "",
        "BUILDING",
        "IN_FLIGHT",
        "SUCCESS",
        "FAILURE",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

.field public static final enum BUILDING:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

.field public static final enum FAILURE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

.field public static final enum IN_FLIGHT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

.field public static final enum SUCCESS:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v2, 0x0

    const-string v3, "BUILDING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->BUILDING:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v2, 0x1

    const-string v3, "IN_FLIGHT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->IN_FLIGHT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v2, 0x2

    const-string v3, "SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->SUCCESS:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v2, 0x3

    const-string v3, "FAILURE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->FAILURE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->$VALUES:[Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 247
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;
    .locals 1

    const-class v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;
    .locals 1

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->$VALUES:[Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    invoke-virtual {v0}, [Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    return-object v0
.end method


# virtual methods
.method public final hasTransferResult()Z
    .locals 2

    .line 268
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->SUCCESS:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->FAILURE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
