.class final Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;
.super Ljava/lang/Object;
.source "SquareCardActivityRequester.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u0008\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u000e\u0010\u0008\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;",
        "",
        "()V",
        "ALLOWED_ACTIVITY_TYPES",
        "",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
        "getALLOWED_ACTIVITY_TYPES",
        "()Ljava/util/List;",
        "DEFAULT_BATCH_SIZE",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getALLOWED_ACTIVITY_TYPES()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation

    .line 225
    invoke-static {}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->access$getALLOWED_ACTIVITY_TYPES$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
