.class public final Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;
.super Lcom/squareup/applet/AppletSection;
.source "UnifiedActivitySection.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
        "Lcom/squareup/applet/AppletSection;",
        "access",
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;",
        "(Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;)V",
        "getInitialScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "showUnifiedActivitySection",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final access:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "access"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    move-object v0, p1

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;->access:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;

    return-void
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;->INSTANCE:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public final showUnifiedActivitySection()Z
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;->access:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;->determineVisibility()Z

    move-result v0

    return v0
.end method
