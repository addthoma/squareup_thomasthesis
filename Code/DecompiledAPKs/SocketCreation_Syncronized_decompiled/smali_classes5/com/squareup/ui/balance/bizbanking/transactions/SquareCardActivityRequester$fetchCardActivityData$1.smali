.class final Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;
.super Ljava/lang/Object;
.source "SquareCardActivityRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->fetchCardActivityData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
        "kotlin.jvm.PlatformType",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;"
        }
    .end annotation

    const-string v0, "received"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    invoke-static {v0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->access$onLoadCardActivitySuccess(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p1

    goto :goto_0

    .line 152
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->access$onLoadCardActivityFailure(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p1

    return-object p1
.end method
