.class public Lcom/squareup/ui/balance/BalanceAppletSectionsListView;
.super Lcom/squareup/applet/AppletSectionsListView;
.source "BalanceAppletSectionsListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;,
        Lcom/squareup/ui/balance/BalanceAppletSectionsListView$Component;
    }
.end annotation


# instance fields
.field private addMoneyButton:Lcom/squareup/noho/NohoButton;

.field private availableBalance:Landroid/widget/TextView;

.field private availableBalanceTitle:Landroid/widget/TextView;

.field private balanceCard:Landroid/view/View;

.field private emptyRecentActivityContainer:Landroid/view/ViewGroup;

.field private errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private instantDepositHint:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recentActivityContainer:Landroid/view/ViewGroup;

.field private recentActivityTitle:Landroid/widget/TextView;

.field private spinner:Landroid/view/View;

.field private transferToBankButton:Lcom/squareup/noho/NohoButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/squareup/applet/AppletSectionsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    const-class p2, Lcom/squareup/ui/balance/BalanceMasterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceMasterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/balance/BalanceMasterScreen$Component;->inject(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;)V

    .line 81
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->addBalanceCard()V

    return-void
.end method

.method private addBalanceCard()V
    .locals 3

    .line 220
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/balance/applet/impl/R$layout;->balance_header_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    .line 221
    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 223
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->bindViews()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 231
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->available_balance_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->availableBalanceTitle:Landroid/widget/TextView;

    .line 232
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->available_balance:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->availableBalance:Landroid/widget/TextView;

    .line 233
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_to_bank_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    .line 234
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->add_money_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->addMoneyButton:Lcom/squareup/noho/NohoButton;

    .line 235
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->instant_deposit_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->instantDepositHint:Lcom/squareup/widgets/MessageView;

    .line 236
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->balance_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->balanceCard:Landroid/view/View;

    .line 237
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->balance_spinner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->spinner:Landroid/view/View;

    .line 238
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->error_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 239
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->recent_activty_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityTitle:Landroid/widget/TextView;

    .line 240
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->recent_activity_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    .line 241
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->recent_activity_empty:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->emptyRecentActivityContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method private getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method private maybeHideEmptyRecentActivity()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->emptyRecentActivityContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private maybeHideRecentActivity()V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 199
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected appletListRow()I
    .locals 1

    .line 85
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->balance_applet_list_row:I

    return v0
.end method

.method public hideRefreshButton()V
    .locals 2

    .line 142
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 145
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 143
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public synthetic lambda$setAndShowBalance$0$BalanceAppletSectionsListView(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->presenter:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->onTransferToBankClicked(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V

    return-void
.end method

.method public setAndShowBalance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Z)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->availableBalanceTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->availableBalance:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->availableBalance:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->ACCOUNT_FROZEN:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    if-ne p4, v0, :cond_0

    sget v0, Lcom/squareup/noho/R$color;->noho_text_warning:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/noho/R$color;->noho_text_body:I

    :goto_0
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    if-nez p1, :cond_2

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    if-eq p4, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    new-instance v1, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListView$wDX_KgBIc8GJtIbnDkh7TJL7YOc;

    invoke-direct {v1, p0, p4}, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListView$wDX_KgBIc8GJtIbnDkh7TJL7YOc;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V

    .line 101
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p4

    .line 100
    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->addMoneyButton:Lcom/squareup/noho/NohoButton;

    invoke-static {p1, p6}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p6, :cond_3

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->addMoneyButton:Lcom/squareup/noho/NohoButton;

    iget-object p4, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->presenter:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p6, Lcom/squareup/ui/balance/-$$Lambda$ULEpHDYPSijb-emUMO7R9pJwbAo;

    invoke-direct {p6, p4}, Lcom/squareup/ui/balance/-$$Lambda$ULEpHDYPSijb-emUMO7R9pJwbAo;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    invoke-static {p6}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p4

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    sget-object p4, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    goto :goto_2

    .line 109
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    sget-object p4, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 112
    :goto_2
    invoke-static {p5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->instantDepositHint:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->transferToBankButton:Lcom/squareup/noho/NohoButton;

    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    xor-int/2addr p3, v0

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->instantDepositHint:Lcom/squareup/widgets/MessageView;

    invoke-static {p5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    xor-int/2addr p3, v0

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->balanceCard:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->spinner:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method public showEmptyRecentActivity()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->emptyRecentActivityContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 194
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->maybeHideRecentActivity()V

    return-void
.end method

.method public showLoading()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->balanceCard:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->spinner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->maybeHideRecentActivity()V

    return-void
.end method

.method public showLoadingError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->balanceCard:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->spinner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 213
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 214
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 215
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->maybeHideRecentActivity()V

    return-void
.end method

.method public showRecentActivity(Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;",
            ">;)V"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    .line 156
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    if-ge p1, v0, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/balance/applet/impl/R$layout;->recent_activity_row:I

    iget-object v3, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    invoke-static {v1, v2, v3}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    .line 162
    iget-object v2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    add-int/lit8 v3, v1, 0x1

    .line 163
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoRow;

    .line 164
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    .line 165
    iget-object v4, v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->title:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v4, v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->subtitle:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v4, v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->value:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 170
    iget-boolean v4, v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->applyPositiveAmountDecorator:Z

    if-eqz v4, :cond_1

    .line 171
    sget v4, Lcom/squareup/balance/applet/impl/R$style;->RecentActivityRowPositiveAmount:I

    goto :goto_2

    .line 173
    :cond_1
    sget v4, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Row_Value:I

    .line 175
    :goto_2
    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 177
    iget-object v4, v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->subValue:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setSubValue(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v1, v1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v1, v3

    goto :goto_1

    .line 182
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    :goto_3
    add-int/lit8 v0, v0, 0x1

    if-ge v0, p2, :cond_3

    .line 184
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    .line 185
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    .line 186
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 188
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->recentActivityContainer:Landroid/view/ViewGroup;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 189
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->maybeHideEmptyRecentActivity()V

    return-void
.end method

.method public showRefreshButton()V
    .locals 4

    .line 132
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 133
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->reload_balance:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    .line 135
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RELOAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 136
    invoke-virtual {v2, v3, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->presenter:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/balance/-$$Lambda$krXkiC4Ux8xFIYWFBa3cFiOdXFY;

    invoke-direct {v3, v2}, Lcom/squareup/ui/balance/-$$Lambda$krXkiC4Ux8xFIYWFBa3cFiOdXFY;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    .line 137
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 138
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
