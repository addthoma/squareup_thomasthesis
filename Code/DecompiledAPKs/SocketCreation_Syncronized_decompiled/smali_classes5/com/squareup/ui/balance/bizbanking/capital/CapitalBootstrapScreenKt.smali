.class public final Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreenKt;
.super Ljava/lang/Object;
.source "CapitalBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "getCapitalFlexLoanWorkflowRunner",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;",
        "Lmortar/MortarScope;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getCapitalFlexLoanWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreenKt;->getCapitalFlexLoanWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;

    move-result-object p0

    return-object p0
.end method

.method private static final getCapitalFlexLoanWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;->Companion:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p0

    check-cast p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;

    return-object p0
.end method
