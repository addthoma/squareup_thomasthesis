.class public interface abstract Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;
.super Ljava/lang/Object;
.source "BalanceTransactionDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract balanceTransactionDetailData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onBackFromTransactionDetail()V
.end method

.method public abstract onExpenseTypeClicked(Ljava/lang/String;Z)V
.end method
