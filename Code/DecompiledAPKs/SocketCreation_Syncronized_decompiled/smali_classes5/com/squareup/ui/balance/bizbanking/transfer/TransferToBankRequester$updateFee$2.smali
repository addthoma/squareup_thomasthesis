.class final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->updateFee(Lcom/squareup/protos/common/Money;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "balanceResponse",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $transferAmount:Lcom/squareup/protos/common/Money;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/common/Money;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->$transferAmount:Lcom/squareup/protos/common/Money;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "balanceResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object p1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->$transferAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-static {v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$getMinTransfer$p(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMathOperatorsKt;->compareTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->$transferAmount:Lcom/squareup/protos/common/Money;

    const-string v1, "maxDeposit"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMathOperatorsKt;->compareTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)I

    move-result p1

    if-gtz p1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->$transferAmount:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$tryToUpdateFee(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_0
    invoke-static {}, Lio/reactivex/Single;->never()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.never()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;->apply(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
