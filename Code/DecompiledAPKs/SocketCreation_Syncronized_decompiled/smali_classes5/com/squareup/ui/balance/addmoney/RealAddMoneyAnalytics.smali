.class public final Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;
.super Ljava/lang/Object;
.source "RealAddMoneyAnalytics.kt"

# interfaces
.implements Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0008\u0010\u0008\u001a\u00020\u0006H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010\r\u001a\u00020\u0006H\u0016J\u0010\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logAddMoneyButtonClick",
        "",
        "logAddMoneyConfirmClick",
        "logAddMoneyFailureScreen",
        "logAddMoneySuccessScreen",
        "logClick",
        "description",
        "",
        "logDepositSettingsClick",
        "logImpression",
        "logSeeAddMoneyButton",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v1, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    invoke-direct {v1, p1}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method


# virtual methods
.method public logAddMoneyButtonClick()V
    .locals 1

    const-string v0, "Deposits: Add Money"

    .line 23
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logAddMoneyConfirmClick()V
    .locals 1

    const-string v0, "Deposits: Add Money Confirm Amount"

    .line 31
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logAddMoneyFailureScreen()V
    .locals 1

    const-string v0, "Deposits: Add Money Error"

    .line 39
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logAddMoneySuccessScreen()V
    .locals 1

    const-string v0, "Deposits: Add Money Success"

    .line 35
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logDepositSettingsClick()V
    .locals 1

    const-string v0, "Deposits: Add Money Deposit Settings"

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSeeAddMoneyButton()V
    .locals 1

    const-string v0, "Deposits: Add Money Button"

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method
