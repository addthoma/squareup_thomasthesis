.class public final Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;
.super Ljava/lang/Object;
.source "ConfirmTransferCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->settingsAppletGatewayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/settings/SettingsAppletGateway;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ")",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/settings/SettingsAppletGateway;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->settingsAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/settings/SettingsAppletGateway;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->get()Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    move-result-object v0

    return-object v0
.end method
