.class final Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;
.super Lcom/squareup/applet/AppletSectionsListEntry;
.source "RealCapitalFlexLoanSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ManageCapitalEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B5\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\tH\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u000f\u001a\u0004\u0018\u00010\n2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\n@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;",
        "Lcom/squareup/applet/AppletSectionsListEntry;",
        "section",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "analytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "flexStatus",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "capitalOfferFormatter",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
        "(Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/util/Res;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lio/reactivex/Observable;Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;)V",
        "<set-?>",
        "displayedFlexStatus",
        "getDisplayedFlexStatus",
        "()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "getValueTextObservable",
        "",
        "logManageCapitalEntryView",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

.field private final capitalOfferFormatter:Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;

.field private displayedFlexStatus:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

.field private final flexStatus:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/util/Res;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lio/reactivex/Observable;Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
            ")V"
        }
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flexStatus"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalOfferFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    check-cast p1, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->square_capital:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->flexStatus:Lio/reactivex/Observable;

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->capitalOfferFormatter:Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;

    return-void
.end method

.method public static final synthetic access$getCapitalOfferFormatter$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->capitalOfferFormatter:Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;

    return-object p0
.end method

.method public static final synthetic access$getDisplayedFlexStatus$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->displayedFlexStatus:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)Lcom/squareup/util/Res;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$logManageCapitalEntryView(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->logManageCapitalEntryView()V

    return-void
.end method

.method public static final synthetic access$setDisplayedFlexStatus$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->displayedFlexStatus:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    return-void
.end method

.method private final logManageCapitalEntryView()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->displayedFlexStatus:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    .line 93
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    if-eqz v1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalActiveOfferRow()V

    goto :goto_0

    .line 95
    :cond_0
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;

    if-eqz v1, :cond_1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalActivePlanRow()V

    goto :goto_0

    .line 97
    :cond_1
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;

    if-eqz v1, :cond_2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalClosedPlanRow()V

    goto :goto_0

    .line 99
    :cond_2
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    if-eqz v1, :cond_3

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalPendingApplicationRow()V

    goto :goto_0

    .line 101
    :cond_3
    instance-of v0, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;

    if-eqz v0, :cond_4

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalIneligibleRow()V

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public final getDisplayedFlexStatus()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->displayedFlexStatus:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    return-object v0
.end method

.method public getValueTextObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->flexStatus:Lio/reactivex/Observable;

    .line 69
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;-><init>(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$2;-><init>(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "flexStatus\n          .do\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
