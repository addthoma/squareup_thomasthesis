.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;
.super Ljava/lang/Object;
.source "BalanceTransactionsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0008\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u00c6\u0003J%\u0010\n\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0010\u0008\u0002\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;",
        "",
        "cardActivityState",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
        "transactions",
        "",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
        "(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData$Companion;

.field public static final SENTINEL:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;


# instance fields
.field public final cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

.field public final transactions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->Companion:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData$Companion;

    .line 46
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    new-instance v8, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v8, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cardActivityState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->copy(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "cardActivityState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(cardActivityState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
