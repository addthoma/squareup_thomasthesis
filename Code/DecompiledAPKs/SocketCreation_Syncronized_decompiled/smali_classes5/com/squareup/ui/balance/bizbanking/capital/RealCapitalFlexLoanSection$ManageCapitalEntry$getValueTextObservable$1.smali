.class final Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanSection.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->getValueTextObservable()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-static {v0, p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->access$setDisplayedFlexStatus$p(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->access$logManageCapitalEntryView(Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry$getValueTextObservable$1;->accept(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)V

    return-void
.end method
