.class public Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;
.super Ljava/lang/Object;
.source "ConfirmTransferScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenData"
.end annotation


# instance fields
.field public final balance:Lcom/squareup/protos/common/Money;

.field public final bankAccount:Ljava/lang/CharSequence;

.field public final brandNameId:I

.field public final desiredDeposit:Lcom/squareup/protos/common/Money;

.field public final feeAmount:Lcom/squareup/protos/common/Money;

.field public final instantDepositRequiresLinkedCard:Z

.field public final isInstant:Z

.field public final panSuffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;ZLjava/lang/CharSequence;Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->balance:Lcom/squareup/protos/common/Money;

    .line 60
    iget-object p5, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->desiredDeposit:Lcom/squareup/protos/common/Money;

    .line 61
    iget-object p5, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    iget-object p5, p5, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;->fee_total_amount:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->feeAmount:Lcom/squareup/protos/common/Money;

    .line 62
    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    sget-object p5, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    if-ne p1, p5, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->isInstant:Z

    .line 63
    iput-boolean p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->instantDepositRequiresLinkedCard:Z

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->bankAccount:Ljava/lang/CharSequence;

    if-nez p4, :cond_1

    const/4 p1, -0x1

    goto :goto_1

    .line 65
    :cond_1
    iget-object p1, p4, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 66
    invoke-static {p1}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p1

    iget p1, p1, Lcom/squareup/text/CardBrandResources;->brandNameId:I

    :goto_1
    iput p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->brandNameId:I

    if-nez p4, :cond_2

    const/4 p1, 0x0

    goto :goto_2

    .line 67
    :cond_2
    iget-object p1, p4, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    :goto_2
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->panSuffix:Ljava/lang/String;

    return-void
.end method
