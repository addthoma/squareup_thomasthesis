.class public Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;
.super Ljava/lang/Object;
.source "BalanceAppletSectionsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceAppletSectionsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecentActivityRow"
.end annotation


# instance fields
.field public final applyPositiveAmountDecorator:Z

.field public final onClickListener:Landroid/view/View$OnClickListener;

.field public final subValue:Ljava/lang/CharSequence;

.field public final subtitle:Ljava/lang/CharSequence;

.field public final title:Ljava/lang/String;

.field public final value:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->title:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->subtitle:Ljava/lang/CharSequence;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->value:Ljava/lang/CharSequence;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->subValue:Ljava/lang/CharSequence;

    .line 71
    iput-boolean p5, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->applyPositiveAmountDecorator:Z

    .line 72
    iput-object p6, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method
