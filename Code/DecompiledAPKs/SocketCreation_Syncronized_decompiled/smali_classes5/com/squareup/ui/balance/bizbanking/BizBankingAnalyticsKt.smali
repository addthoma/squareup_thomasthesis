.class public final Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsKt;
.super Ljava/lang/Object;
.source "BizBankingAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001e\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0017\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001d\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "ACTIVE_SALES",
        "",
        "BALANCE_CARD_SPEND",
        "BALANCE_HEADER",
        "BALANCE_LOAD_FAILURE",
        "BALANCE_SHOW_CLICK",
        "BALANCE_TRANSFER_TO_BANK",
        "BANK_TRANSFER_RESULT_ERROR",
        "BANK_TRANSFER_RESULT_IN_PROGRESS",
        "BANK_TRANSFER_RESULT_SUCCESSFUL",
        "CANNOT_LOAD_CARD_ACTIVITY",
        "CARD_ACTIVITY",
        "CARD_SPEND_TRANSACTION_DETAILS",
        "CONFIRMATION_BACK",
        "CONFIRMATION_CHANGE_BANK",
        "CONFIRMATION_CHANGE_CARD",
        "CONFIRMATION_TRANSFER",
        "DEPOSITS_REPORT",
        "FREEZE_CLICK_LEARN_MORE",
        "FREEZE_CLICK_VERIFY",
        "FREEZE_VIEW_DEPOSITS_SUSPENDED",
        "MANAGE_SQUARE_CARD",
        "NO_CARD_ACTIVITY",
        "PENDING_DEPOSIT",
        "TRANSFER_EXIT",
        "TRANSFER_INSTANT_MAX",
        "TRANSFER_INSTANT_PARTIAL",
        "TRANSFER_REGULAR_MAX",
        "TRANSFER_REGULAR_PARTIAL",
        "TRANSFER_RESULT_DONE",
        "TRANSFER_RESULT_EXIT",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTIVE_SALES:Ljava/lang/String; = "Deposits: Active Sales"

.field private static final BALANCE_CARD_SPEND:Ljava/lang/String; = "Balance: Card Spend"

.field private static final BALANCE_HEADER:Ljava/lang/String; = "Balance: Balance"

.field private static final BALANCE_LOAD_FAILURE:Ljava/lang/String; = "Balance: Unable To Load Balance"

.field private static final BALANCE_SHOW_CLICK:Ljava/lang/String; = "Balance: Balance"

.field private static final BALANCE_TRANSFER_TO_BANK:Ljava/lang/String; = "Balance: Transfer to Bank"

.field private static final BANK_TRANSFER_RESULT_ERROR:Ljava/lang/String; = "Bank Transfer Result: Transfer Error"

.field private static final BANK_TRANSFER_RESULT_IN_PROGRESS:Ljava/lang/String; = "Bank Transfer Result: Transfer in Progress"

.field private static final BANK_TRANSFER_RESULT_SUCCESSFUL:Ljava/lang/String; = "Bank Transfer Result: Successful"

.field private static final CANNOT_LOAD_CARD_ACTIVITY:Ljava/lang/String; = "Card Spend: Cannot Load Transactions"

.field private static final CARD_ACTIVITY:Ljava/lang/String; = "Card Spend: Transactions"

.field private static final CARD_SPEND_TRANSACTION_DETAILS:Ljava/lang/String; = "Card Spend: Transaction Details"

.field private static final CONFIRMATION_BACK:Ljava/lang/String; = "Bank Transfer Confirmation: Back"

.field private static final CONFIRMATION_CHANGE_BANK:Ljava/lang/String; = "Bank Transfer Confirmation: Change Bank Account"

.field private static final CONFIRMATION_CHANGE_CARD:Ljava/lang/String; = "Bank Transfer Confirmation: Change Debit Card"

.field private static final CONFIRMATION_TRANSFER:Ljava/lang/String; = "Bank Transfer Confirmation: Transfer"

.field private static final DEPOSITS_REPORT:Ljava/lang/String; = "Deposits: Deposit Reports"

.field private static final FREEZE_CLICK_LEARN_MORE:Ljava/lang/String; = "Account Freeze Deposits Suspended Overview: Learn More"

.field private static final FREEZE_CLICK_VERIFY:Ljava/lang/String; = "Account Freeze Deposits Suspended Overview: Verify Account"

.field private static final FREEZE_VIEW_DEPOSITS_SUSPENDED:Ljava/lang/String; = "Account Freeze Deposits Suspended Overview"

.field private static final MANAGE_SQUARE_CARD:Ljava/lang/String; = "Deposits: Square Card"

.field private static final NO_CARD_ACTIVITY:Ljava/lang/String; = "Card Spend: No Transactions"

.field private static final PENDING_DEPOSIT:Ljava/lang/String; = "Deposits: Pending Deposit"

.field private static final TRANSFER_EXIT:Ljava/lang/String; = "Bank Transfer: Exit"

.field private static final TRANSFER_INSTANT_MAX:Ljava/lang/String; = "Bank Transfer: Continue With Instant Max Amount"

.field private static final TRANSFER_INSTANT_PARTIAL:Ljava/lang/String; = "Bank Transfer: Continue With Instant Partial Amount"

.field private static final TRANSFER_REGULAR_MAX:Ljava/lang/String; = "Bank Transfer: Continue With Regular Max Amount"

.field private static final TRANSFER_REGULAR_PARTIAL:Ljava/lang/String; = "Bank Transfer: Continue With Regular Partial Amount"

.field private static final TRANSFER_RESULT_DONE:Ljava/lang/String; = "Bank Transfer Result: Done"

.field private static final TRANSFER_RESULT_EXIT:Ljava/lang/String; = "Bank Transfer Result: Exit"
