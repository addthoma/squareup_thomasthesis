.class public final enum Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;
.super Ljava/lang/Enum;
.source "BalanceMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum ACCOUNT_FROZEN:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum CONFIRM_INSTANT_TRANSFER:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum DEPOSIT_TO_BANK:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum INSTANTLY_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum RESEND_EMAIL:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public static final enum SET_UP_INSTANT_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 70
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 71
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v2, 0x1

    const-string v3, "DEPOSIT_TO_BANK"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->DEPOSIT_TO_BANK:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 72
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v3, 0x2

    const-string v4, "INSTANTLY_DEPOSIT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->INSTANTLY_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 73
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v4, 0x3

    const-string v5, "CONFIRM_INSTANT_TRANSFER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->CONFIRM_INSTANT_TRANSFER:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 74
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v5, 0x4

    const-string v6, "SET_UP_INSTANT_DEPOSIT"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->SET_UP_INSTANT_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 75
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v6, 0x5

    const-string v7, "RESEND_EMAIL"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->RESEND_EMAIL:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 76
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v7, 0x6

    const-string v8, "ACCOUNT_FROZEN"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->ACCOUNT_FROZEN:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 69
    sget-object v8, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->DEPOSIT_TO_BANK:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->INSTANTLY_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->CONFIRM_INSTANT_TRANSFER:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->SET_UP_INSTANT_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->RESEND_EMAIL:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->ACCOUNT_FROZEN:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->$VALUES:[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;
    .locals 1

    .line 69
    const-class v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->$VALUES:[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    invoke-virtual {v0}, [Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    return-object v0
.end method
