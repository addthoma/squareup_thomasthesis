.class public interface abstract Lcom/squareup/ui/TouchEventMonitor;
.super Ljava/lang/Object;
.source "TouchEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/TouchEventMonitor$NoOp;
    }
.end annotation


# virtual methods
.method public abstract observeTouchEvents()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onTouchEvent()V
.end method
