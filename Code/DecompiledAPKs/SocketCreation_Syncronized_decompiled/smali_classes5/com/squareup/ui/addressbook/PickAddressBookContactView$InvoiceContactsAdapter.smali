.class Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;
.super Landroidx/cursoradapter/widget/CursorAdapter;
.source "PickAddressBookContactView.java"

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/addressbook/PickAddressBookContactView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InvoiceContactsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter$HeaderViewHolder;
    }
.end annotation


# static fields
.field private static final ALPHABET:Ljava/lang/String; = " ABCDEFGHIJKLMNOPQRSTUVWXYZ"

.field private static final DEFAULT_HEADER_ID:J


# instance fields
.field private final alphabetIndexer:Landroid/widget/AlphabetIndexer;

.field private final context:Landroid/content/Context;

.field private defaultHeaderText:Ljava/lang/String;

.field private final displayNameIndex:I

.field private final emailIndex:I

.field private final inflater:Landroid/view/LayoutInflater;

.field private final phoneIndex:I

.field private final photoUriIndex:I


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/database/MatrixCursor;Lcom/squareup/CountryCode;)V
    .locals 2

    const/4 v0, 0x0

    .line 164
    invoke-direct {p0, p1, p2, v0}, Landroidx/cursoradapter/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    if-eqz p2, :cond_0

    const-string v0, "display_name"

    .line 167
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->displayNameIndex:I

    const-string v0, "photo_uri"

    .line 168
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->photoUriIndex:I

    const-string v0, "data1"

    .line 169
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const-string v1, "data2"

    .line 170
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 171
    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->emailIndex:I

    .line 172
    iput v1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->phoneIndex:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 174
    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->displayNameIndex:I

    .line 175
    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->photoUriIndex:I

    .line 176
    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->emailIndex:I

    .line 177
    iput v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->phoneIndex:I

    .line 179
    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->context:Landroid/content/Context;

    .line 180
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 181
    sget-object p1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-eq p3, p1, :cond_2

    sget-object p1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    if-ne p3, p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 185
    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    .line 186
    invoke-direct {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getDefaultHeaderText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->defaultHeaderText:Ljava/lang/String;

    goto :goto_2

    .line 182
    :cond_2
    :goto_1
    new-instance p1, Landroid/widget/AlphabetIndexer;

    iget p3, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->displayNameIndex:I

    const-string v0, " ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    invoke-direct {p1, p2, p3, v0}, Landroid/widget/AlphabetIndexer;-><init>(Landroid/database/Cursor;ILjava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    :goto_2
    return-void
.end method

.method private getDefaultHeaderText()Ljava/lang/String;
    .locals 3

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->context:Landroid/content/Context;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->invoice_default_headertext:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const-string v2, "count"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 329
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .line 195
    sget p2, Lcom/squareup/crmupdatecustomer/R$id;->contact_container:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    .line 196
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->contact_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 197
    iget v1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->displayNameIndex:I

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 199
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 202
    :cond_0
    invoke-virtual {p2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 203
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    :goto_0
    sget p2, Lcom/squareup/crmupdatecustomer/R$id;->contact_email:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 207
    iget v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->emailIndex:I

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 208
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 209
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 212
    :cond_1
    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :goto_1
    sget p2, Lcom/squareup/crmupdatecustomer/R$id;->contact_phone:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 217
    iget p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->phoneIndex:I

    invoke-interface {p3, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 218
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_2

    .line 219
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 222
    :cond_2
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1

    .line 282
    invoke-super {p0, p1}, Landroidx/cursoradapter/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {v0, p1}, Landroid/widget/AlphabetIndexer;->setCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 286
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getDefaultHeaderText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->defaultHeaderText:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method getEmail(I)Ljava/lang/String;
    .locals 1

    .line 296
    invoke-virtual {p0, p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/database/Cursor;

    .line 297
    iget v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->emailIndex:I

    if-lez v0, :cond_0

    .line 298
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method public getHeaderId(I)J
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {v0, p1}, Landroid/widget/AlphabetIndexer;->getSectionForPosition(I)I

    move-result p1

    int-to-long v0, p1

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    .line 258
    new-instance p2, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter$HeaderViewHolder;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter$HeaderViewHolder;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactView$1;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->inflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/squareup/crmupdatecustomer/R$layout;->pick_invoice_contact_header:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 260
    move-object v0, p3

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p2, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter$HeaderViewHolder;->header:Landroid/widget/TextView;

    .line 261
    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, p3

    move-object p3, p2

    move-object p2, v3

    goto :goto_0

    .line 263
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter$HeaderViewHolder;

    .line 265
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->defaultHeaderText:Ljava/lang/String;

    .line 266
    iget-object v1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v1, :cond_1

    .line 267
    invoke-virtual {p0, p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getSectionForPosition(I)I

    move-result p1

    const-string v0, " ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    .line 268
    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 270
    :cond_1
    iget-object p1, p3, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter$HeaderViewHolder;->header:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method getName(I)Ljava/lang/String;
    .locals 1

    .line 291
    invoke-virtual {p0, p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/database/Cursor;

    .line 292
    iget v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->displayNameIndex:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getPhoneNumber(I)Ljava/lang/String;
    .locals 1

    .line 305
    invoke-virtual {p0, p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/database/Cursor;

    .line 306
    iget v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->phoneIndex:I

    if-lez v0, :cond_0

    .line 307
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method getPhoto(I)Landroid/net/Uri;
    .locals 2

    .line 314
    invoke-virtual {p0, p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/database/Cursor;

    .line 315
    iget v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->photoUriIndex:I

    const/4 v1, 0x0

    if-lez v0, :cond_0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-nez p1, :cond_1

    return-object v1

    .line 319
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    return-object v1

    .line 322
    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method public getPositionForSection(I)I
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {v0, p1}, Landroid/widget/AlphabetIndexer;->getPositionForSection(I)I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getSectionForPosition(I)I
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    return v1

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    invoke-virtual {v0, p1}, Landroid/widget/AlphabetIndexer;->getSectionForPosition(I)I

    move-result p1

    return p1

    :cond_1
    return v1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->alphabetIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    .line 229
    invoke-virtual {v0}, Landroid/widget/AlphabetIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "DefaultSection"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactView$InvoiceContactsAdapter;->inflater:Landroid/view/LayoutInflater;

    sget p2, Lcom/squareup/crmupdatecustomer/R$layout;->pick_invoice_contact_row:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
