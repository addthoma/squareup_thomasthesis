.class public Lcom/squareup/ui/invoices/InvoiceSentSavedView;
.super Landroid/widget/FrameLayout;
.source "InvoiceSentSavedView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private copyLink:Lcom/squareup/marketfont/MarketButton;

.field private customerImage:Landroid/widget/ImageView;

.field private moreOptions:Lcom/squareup/marketfont/MarketButton;

.field presenter:Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private shareLinkMessageView:Lcom/squareup/widgets/MessageView;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen$Component;->inject(Lcom/squareup/ui/invoices/InvoiceSentSavedView;)V

    return-void
.end method


# virtual methods
.method clearSubtitle()V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->subtitle:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 80
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->presenter:Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->presenter:Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->presenter:Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->dropView(Ljava/lang/Object;)V

    .line 86
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 40
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 42
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 44
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    .line 45
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 46
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    .line 48
    sget v1, Lcom/squareup/ui/buyerflow/R$id;->customer_image:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->customerImage:Landroid/widget/ImageView;

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->customerImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 51
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->title:Lcom/squareup/widgets/MessageView;

    .line 52
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 53
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->copy_link:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->copyLink:Lcom/squareup/marketfont/MarketButton;

    .line 54
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->more_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->moreOptions:Lcom/squareup/marketfont/MarketButton;

    .line 55
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->share_link_email_sent_helper:I

    .line 56
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->shareLinkMessageView:Lcom/squareup/widgets/MessageView;

    .line 58
    new-instance v0, Lcom/squareup/ui/invoices/InvoiceSentSavedView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView$1;-><init>(Lcom/squareup/ui/invoices/InvoiceSentSavedView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->copyLink:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/invoices/InvoiceSentSavedView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView$2;-><init>(Lcom/squareup/ui/invoices/InvoiceSentSavedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->moreOptions:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/invoices/InvoiceSentSavedView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView$3;-><init>(Lcom/squareup/ui/invoices/InvoiceSentSavedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->title:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showShareLinkButtons(Z)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->copyLink:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->moreOptions:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showShareLinkHelper()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->shareLinkMessageView:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method
