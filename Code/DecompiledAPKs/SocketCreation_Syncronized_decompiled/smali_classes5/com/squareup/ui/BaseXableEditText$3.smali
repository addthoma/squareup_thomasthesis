.class Lcom/squareup/ui/BaseXableEditText$3;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "BaseXableEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/BaseXableEditText;->setNextFocusView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/BaseXableEditText;

.field final synthetic val$nextView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/ui/BaseXableEditText;Landroid/view/View;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/ui/BaseXableEditText$3;->this$0:Lcom/squareup/ui/BaseXableEditText;

    iput-object p2, p0, Lcom/squareup/ui/BaseXableEditText$3;->val$nextView:Landroid/view/View;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x2

    if-eq p2, p1, :cond_0

    const/4 p1, 0x5

    if-eq p2, p1, :cond_0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_1

    .line 291
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText$3;->val$nextView:Landroid/view/View;

    if-eqz p1, :cond_1

    .line 292
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
