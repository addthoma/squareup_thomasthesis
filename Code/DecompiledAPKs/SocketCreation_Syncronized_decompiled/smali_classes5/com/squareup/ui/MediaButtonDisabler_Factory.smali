.class public final Lcom/squareup/ui/MediaButtonDisabler_Factory;
.super Ljava/lang/Object;
.source "MediaButtonDisabler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/MediaButtonDisabler;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/MediaButtonDisabler_Factory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/MediaButtonDisabler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/ui/MediaButtonDisabler_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/MediaButtonDisabler_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/MediaButtonDisabler_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;)Lcom/squareup/ui/MediaButtonDisabler;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-direct {v0, p0}, Lcom/squareup/ui/MediaButtonDisabler;-><init>(Landroid/app/Application;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/MediaButtonDisabler;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/MediaButtonDisabler_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/ui/MediaButtonDisabler_Factory;->newInstance(Landroid/app/Application;)Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/MediaButtonDisabler_Factory;->get()Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    return-object v0
.end method
