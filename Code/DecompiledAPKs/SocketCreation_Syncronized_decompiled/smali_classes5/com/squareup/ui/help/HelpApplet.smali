.class public Lcom/squareup/ui/help/HelpApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "HelpApplet.java"


# static fields
.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "HELP_CENTER"


# instance fields
.field private final entryPoint:Lcom/squareup/ui/help/api/HelpAppletEntryPoint;

.field private final helpBadge:Lcom/squareup/ui/help/HelpBadge;

.field private final sections:Lcom/squareup/ui/help/HelpAppletSectionsList;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletEntryPoint;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/help/HelpBadge;",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/help/HelpApplet;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/help/HelpApplet;->sections:Lcom/squareup/ui/help/HelpAppletSectionsList;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/help/HelpApplet;->entryPoint:Lcom/squareup/ui/help/api/HelpAppletEntryPoint;

    return-void
.end method


# virtual methods
.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/help/HelpApplet;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpBadge;->count()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/-$$Lambda$JyM0djAmbcEiFah4MVar-ihOwZ0;->INSTANCE:Lcom/squareup/ui/help/-$$Lambda$JyM0djAmbcEiFah4MVar-ihOwZ0;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    const-string v0, "help"

    return-object v0
.end method

.method public getEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/applet/AppletEntryPoint;
    .locals 1

    .line 62
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope;

    invoke-static {p1, p2, v0}, Lcom/squareup/container/ContainerTreeKey;->isEnteringScope(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/help/HelpApplet;->entryPoint:Lcom/squareup/ui/help/api/HelpAppletEntryPoint;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 56
    sget-object p1, Lcom/squareup/ui/help/HelpAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/help/HelpAppletMasterScreen;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/help/HelpApplet;->entryPoint:Lcom/squareup/ui/help/api/HelpAppletEntryPoint;

    invoke-virtual {v0}, Lcom/squareup/ui/help/api/HelpAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "HELP_CENTER"

    return-object v0
.end method

.method getSections()Lcom/squareup/applet/AppletSectionsList;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/help/HelpApplet;->sections:Lcom/squareup/ui/help/HelpAppletSectionsList;

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 48
    sget v0, Lcom/squareup/applet/help/R$string;->titlecase_support:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
