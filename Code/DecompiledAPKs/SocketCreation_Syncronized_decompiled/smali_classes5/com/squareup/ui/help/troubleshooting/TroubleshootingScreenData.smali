.class public final Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;
.super Ljava/lang/Object;
.source "TroubleshootingScreenData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;
    }
.end annotation


# instance fields
.field public final isSending:Z

.field public final showEmailSupportLedger:Z

.field public final showSendDiagnosticReport:Z

.field public final showUploadSupportLedger:Z


# direct methods
.method private constructor <init>(ZZZZ)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showUploadSupportLedger:Z

    .line 14
    iput-boolean p2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showEmailSupportLedger:Z

    .line 15
    iput-boolean p3, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showSendDiagnosticReport:Z

    .line 16
    iput-boolean p4, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->isSending:Z

    return-void
.end method

.method synthetic constructor <init>(ZZZZLcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$1;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;-><init>(ZZZZ)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 21
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 23
    :cond_1
    check-cast p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;

    .line 24
    iget-boolean v2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showUploadSupportLedger:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showUploadSupportLedger:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showEmailSupportLedger:Z

    .line 25
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showEmailSupportLedger:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showSendDiagnosticReport:Z

    .line 26
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showSendDiagnosticReport:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->isSending:Z

    .line 27
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean p1, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->isSending:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 31
    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showUploadSupportLedger:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showEmailSupportLedger:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showSendDiagnosticReport:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->isSending:Z

    .line 32
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 31
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 36
    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showUploadSupportLedger:Z

    .line 38
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showEmailSupportLedger:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showSendDiagnosticReport:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->isSending:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "TroubleshootingScreenData{showUploadSupportLedger=%s, showEmailSupportLedger=%s, showSendDiagnosticReport=%s, isSending=%s}"

    .line 36
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
