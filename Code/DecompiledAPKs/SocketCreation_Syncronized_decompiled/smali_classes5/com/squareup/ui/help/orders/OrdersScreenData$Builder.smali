.class public Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;
.super Ljava/lang/Object;
.source "OrdersScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrdersScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private showOrderHistory:Z

.field private showOrderLegacyReader:Z

.field private showOrderReader:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/ui/help/orders/OrdersScreenData;
    .locals 5

    .line 36
    new-instance v0, Lcom/squareup/ui/help/orders/OrdersScreenData;

    iget-boolean v1, p0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderReader:Z

    iget-boolean v2, p0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderLegacyReader:Z

    iget-boolean v3, p0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderHistory:Z

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/help/orders/OrdersScreenData;-><init>(ZZZLcom/squareup/ui/help/orders/OrdersScreenData$1;)V

    return-object v0
.end method

.method public showOrderHistory(Z)Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;
    .locals 0

    .line 31
    iput-boolean p1, p0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderHistory:Z

    return-object p0
.end method

.method public showOrderLegacyReader(Z)Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderLegacyReader:Z

    return-object p0
.end method

.method public showOrderReader(Z)Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderReader:Z

    return-object p0
.end method
