.class public Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;
.super Ljava/lang/Object;
.source "JumbotronMessageProducer.java"

# interfaces
.implements Lmortar/Scoped;


# static fields
.field static final EMPTY_MESSAGES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field static final MESSAGE_LIMIT:I = 0xa

.field private static final REFRESH_ALLOWED_EXPECT:Z = true

.field private static final REFRESH_ALLOWED_UPDATE:Z

.field static final REFRESH_INTERVAL_MS:J

.field public static final toUnreadMessageCount:Lio/reactivex/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Function<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final jumbotronServiceKey:Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;

.field private lastLocale:Ljava/util/Locale;

.field private lastRefreshTime:J

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;"
        }
    .end annotation
.end field

.field private final messagesService:Lcom/squareup/server/messages/MessagesService;

.field private final messagesSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onInAppMessageRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 48
    sget-object v0, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$LFJmpr5JOwH6vmu1OXCrcUNp0N4;->INSTANCE:Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$LFJmpr5JOwH6vmu1OXCrcUNp0N4;

    sput-object v0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->toUnreadMessageCount:Lio/reactivex/functions/Function;

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->REFRESH_INTERVAL_MS:J

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->EMPTY_MESSAGES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/messages/MessagesService;Lcom/squareup/CountryCode;Ljavax/inject/Provider;Lcom/squareup/util/Clock;Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;",
            "Lcom/squareup/server/messages/MessagesService;",
            "Lcom/squareup/CountryCode;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 69
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->onInAppMessageRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messagesSetting:Lcom/squareup/settings/LocalSetting;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messagesService:Lcom/squareup/server/messages/MessagesService;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->countryCode:Lcom/squareup/CountryCode;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->localeProvider:Ljavax/inject/Provider;

    .line 87
    iput-object p6, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->clock:Lcom/squareup/util/Clock;

    .line 88
    iput-object p7, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->jumbotronServiceKey:Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;

    return-void
.end method

.method private jumbotronMessageResponse(Lio/reactivex/Observable;Ljava/lang/String;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/messages/MessagesResponse;",
            ">;"
        }
    .end annotation

    .line 137
    new-instance v0, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$Rs4NnHUVoMZffctJIX7f97GNR8A;

    invoke-direct {v0, p3}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$Rs4NnHUVoMZffctJIX7f97GNR8A;-><init>(Lio/reactivex/Observable;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->delay(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 139
    new-instance p3, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-direct {p3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 140
    new-instance v0, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$M8haTGcQYlU1FP4sQIn3wOHLYdo;

    invoke-direct {v0, p3}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$M8haTGcQYlU1FP4sQIn3wOHLYdo;-><init>(Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 144
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$FXcPCkAR0Xqb3piA6f_Fux6FXwE;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$FXcPCkAR0Xqb3piA6f_Fux6FXwE;-><init>(Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 145
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$jumbotronMessageResponse$5(Lio/reactivex/Observable;Lkotlin/Unit;)Lio/reactivex/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method

.method static synthetic lambda$jumbotronMessageResponse$6(Ljava/util/concurrent/atomic/AtomicBoolean;Lkotlin/Unit;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 144
    invoke-virtual {p0, p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$null$7(Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 148
    invoke-virtual {p0, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 149
    instance-of p0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    return p0
.end method

.method static synthetic lambda$null$9(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/messages/MessagesResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/messages/MessagesResponse;

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$2(Ljava/lang/Throwable;)Lcom/squareup/server/messages/MessagesResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic lambda$static$0(Ljava/util/List;)Ljava/lang/Integer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 50
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/messages/Message;

    .line 51
    iget-boolean v1, v1, Lcom/squareup/server/messages/Message;->read:Z

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method private static protectReadState(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;)V"
        }
    .end annotation

    .line 212
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 213
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/messages/Message;

    .line 214
    iget-boolean v2, v1, Lcom/squareup/server/messages/Message;->read:Z

    if-eqz v2, :cond_0

    .line 215
    iget-object v1, v1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 219
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result p0

    if-lez p0, :cond_3

    .line 220
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/messages/Message;

    .line 221
    iget-object v1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 222
    iput-boolean v1, p1, Lcom/squareup/server/messages/Message;->read:Z

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method getMessages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;"
        }
    .end annotation

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method getUnreadCount()I
    .locals 2

    .line 204
    :try_start_0
    sget-object v0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->toUnreadMessageCount:Lio/reactivex/functions/Function;

    invoke-virtual {p0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->getMessages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/functions/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 206
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public synthetic lambda$jumbotronMessageResponse$10$JumbotronMessageProducer(Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Lkotlin/Unit;)Lio/reactivex/MaybeSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    iget-object p3, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messagesService:Lcom/squareup/server/messages/MessagesService;

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->countryCode:Lcom/squareup/CountryCode;

    .line 146
    invoke-virtual {v0}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {p3, v1, v0, p1}, Lcom/squareup/server/messages/MessagesService;->getMessages(ILjava/lang/String;Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance p3, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$nXcs51rmKRnye0IvUsAtPj-PiZs;

    invoke-direct {p3, p2}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$nXcs51rmKRnye0IvUsAtPj-PiZs;-><init>(Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 148
    invoke-virtual {p1, p3}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$XMw1Yw2L5Q_6HTcp-7ivpC6fHtE;->INSTANCE:Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$XMw1Yw2L5Q_6HTcp-7ivpC6fHtE;

    .line 149
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$NwOvVwkCDIPbwDatE2rVmQovtr4;->INSTANCE:Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$NwOvVwkCDIPbwDatE2rVmQovtr4;

    .line 150
    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$JumbotronMessageProducer()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messagesSetting:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->EMPTY_MESSAGES:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$3$JumbotronMessageProducer(Lcom/squareup/server/messages/MessagesResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    if-eqz p1, :cond_1

    .line 111
    invoke-virtual {p1}, Lcom/squareup/server/messages/MessagesResponse;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->lastRefreshTime:J

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iput-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->lastLocale:Ljava/util/Locale;

    .line 115
    iget-object p1, p1, Lcom/squareup/server/messages/MessagesResponse;->messages:Ljava/util/List;

    if-nez p1, :cond_0

    .line 117
    sget-object p1, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->EMPTY_MESSAGES:Ljava/util/List;

    goto :goto_0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0, p1}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->protectReadState(Ljava/util/List;Ljava/util/List;)V

    .line 122
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$onEnterScope$4$JumbotronMessageProducer(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->refreshIfNeeded()V

    return-void
.end method

.method public markMessageRead(Lcom/squareup/server/messages/Message;)V
    .locals 5

    .line 165
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/messages/Message;

    .line 169
    iget-object v3, v1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 170
    iget-boolean p1, v1, Lcom/squareup/server/messages/Message;->read:Z

    if-nez p1, :cond_1

    .line 171
    iput-boolean v2, v1, Lcom/squareup/server/messages/Message;->read:Z

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    .line 179
    iget-object p1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public messages()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;"
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$DqYIMuoK-rTW4BN0gPfp6Ot3F2k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$DqYIMuoK-rTW4BN0gPfp6Ot3F2k;-><init>(Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;)V

    .line 94
    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 95
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 93
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const-wide/16 v1, 0x1

    .line 101
    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messagesSetting:Lcom/squareup/settings/LocalSetting;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/help/jumbotron/-$$Lambda$edD1mhEPwRVbrbTMVlgeHnF0bqM;

    invoke-direct {v2, v1}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$edD1mhEPwRVbrbTMVlgeHnF0bqM;-><init>(Lcom/squareup/settings/LocalSetting;)V

    .line 102
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 98
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->onInAppMessageRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->jumbotronServiceKey:Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;

    .line 106
    invoke-interface {v1}, Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;->inAppMessageKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->jumbotronMessageResponse(Lio/reactivex/Observable;Ljava/lang/String;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$EF0YTEw3LjTVFmDE6jSIE40wKG8;->INSTANCE:Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$EF0YTEw3LjTVFmDE6jSIE40wKG8;

    .line 108
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$6rGnD_APQ1xsn35W5rOjpgGxtAs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$6rGnD_APQ1xsn35W5rOjpgGxtAs;-><init>(Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;)V

    .line 109
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 105
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 128
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$cogyEUk9eszVPZ0FVTH2AiqO2k4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/jumbotron/-$$Lambda$JumbotronMessageProducer$cogyEUk9eszVPZ0FVTH2AiqO2k4;-><init>(Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;)V

    .line 129
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 127
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method refresh()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->onInAppMessageRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method refreshIfNeeded()V
    .locals 5

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->lastRefreshTime:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->REFRESH_INTERVAL_MS:J

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->lastLocale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->localeProvider:Ljavax/inject/Provider;

    .line 186
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->refresh()V

    :cond_1
    return-void
.end method
