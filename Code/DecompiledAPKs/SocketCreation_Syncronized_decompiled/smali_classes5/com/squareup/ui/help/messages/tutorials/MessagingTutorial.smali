.class public final Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;
.super Ljava/lang/Object;
.source "MessagingTutorial.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\'B/\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u0018H\u0016J\u0008\u0010\u001c\u001a\u00020\u0018H\u0016J\u001a\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u0018\u0010\"\u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010#\u001a\u00020$H\u0016J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\r0&H\u0016R\u001c\u0010\u000c\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0013\u0010\u000f\u001a\u0004\u0008\u0014\u0010\u0011R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "completedMessagingTutorial",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "messagesVisibility",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/util/Res;)V",
        "bannerFirstTutorial",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "bannerFirstTutorial$annotations",
        "()V",
        "getBannerFirstTutorial",
        "()Lcom/squareup/tutorialv2/TutorialState;",
        "bannerReplyTutorial",
        "bannerReplyTutorial$annotations",
        "getBannerReplyTutorial",
        "output",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitRequested",
        "onExitScope",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "Creator",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bannerFirstTutorial:Lcom/squareup/tutorialv2/TutorialState;

.field private final bannerReplyTutorial:Lcom/squareup/tutorialv2/TutorialState;

.field private final completedMessagingTutorial:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final output:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/util/Res;)V
    .locals 1
    .param p1    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/ui/help/messages/tutorials/CompletedMessagingTutorial;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/help/MessagingController;",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "completedMessagingTutorial"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingController"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesVisibility"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->completedMessagingTutorial:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object p3, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 38
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    sget p2, Lcom/squareup/applet/help/R$string;->tap_get_help_to_view_messaging:I

    invoke-virtual {p1, p2}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerFirstTutorial:Lcom/squareup/tutorialv2/TutorialState;

    .line 41
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 42
    sget p2, Lcom/squareup/applet/help/R$string;->tap_to_view_message_reply:I

    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 45
    sget p3, Lcom/squareup/applet/help/R$string;->tap_to_view_conversation:I

    invoke-interface {p4, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    new-instance p4, Landroid/text/style/UnderlineSpan;

    invoke-direct {p4}, Landroid/text/style/UnderlineSpan;-><init>()V

    check-cast p4, Landroid/text/style/CharacterStyle;

    invoke-static {p3, p4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    const-string p4, "tap_to_view"

    .line 43
    invoke-virtual {p2, p4, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 47
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "res.phrase(R.string.tap_\u2026  )\n            .format()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1, p2}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 49
    sget p2, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, p2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->backgroundColor(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerReplyTutorial:Lcom/squareup/tutorialv2/TutorialState;

    return-void
.end method

.method public static synthetic bannerFirstTutorial$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic bannerReplyTutorial$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getBannerFirstTutorial()Lcom/squareup/tutorialv2/TutorialState;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerFirstTutorial:Lcom/squareup/tutorialv2/TutorialState;

    return-object v0
.end method

.method public final getBannerReplyTutorial()Lcom/squareup/tutorialv2/TutorialState;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerReplyTutorial:Lcom/squareup/tutorialv2/TutorialState;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/MessagesVisibility;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$onEnterScope$1;-><init>(Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "messagesVisibility.badge\u2026          }\n            }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitRequested()V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialState;

    iget-object v1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerFirstTutorial:Lcom/squareup/tutorialv2/TutorialState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->completedMessagingTutorial:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    const-string p2, "name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    sparse-switch p2, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string p2, "Messaging activity has been triggered"

    .line 78
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_1
    const-string p2, "How to reply to Square Support"

    .line 74
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerReplyTutorial:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_2
    const-string p2, "Tutorial content tapped"

    .line 83
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tutorialv2/TutorialState;

    iget-object p2, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerReplyTutorial:Lcom/squareup/tutorialv2/TutorialState;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->messagingController:Lcom/squareup/ui/help/MessagingController;

    invoke-interface {p1}, Lcom/squareup/ui/help/MessagingController;->launchMessagingActivity()V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p2, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_3
    const-string p2, "How to message Square Support"

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->completedMessagingTutorial:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tutorialv2/TutorialState;

    iget-object p2, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerReplyTutorial:Lcom/squareup/tutorialv2/TutorialState;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->bannerFirstTutorial:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_4
    const-string p2, "Messaging out of scope"

    .line 78
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 80
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p2, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x66fb48e7 -> :sswitch_4
        -0x220e97a6 -> :sswitch_3
        0x3d22a0f5 -> :sswitch_2
        0x3dad77a6 -> :sswitch_1
        0x694d6d1e -> :sswitch_0
    .end sparse-switch
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
