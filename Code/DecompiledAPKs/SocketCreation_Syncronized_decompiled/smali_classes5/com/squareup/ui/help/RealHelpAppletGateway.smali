.class public final Lcom/squareup/ui/help/RealHelpAppletGateway;
.super Ljava/lang/Object;
.source "RealHelpAppletGateway.kt"

# interfaces
.implements Lcom/squareup/ui/help/HelpAppletGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/help/RealHelpAppletGateway;",
        "Lcom/squareup/ui/help/HelpAppletGateway;",
        "helpApplet",
        "Lcom/squareup/ui/help/HelpApplet;",
        "(Lcom/squareup/ui/help/HelpApplet;)V",
        "getHelpApplet",
        "()Lcom/squareup/ui/help/HelpApplet;",
        "activate",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final helpApplet:Lcom/squareup/ui/help/HelpApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/HelpApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "helpApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpAppletGateway;->helpApplet:Lcom/squareup/ui/help/HelpApplet;

    return-void
.end method


# virtual methods
.method public activate()V
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpAppletGateway;->helpApplet:Lcom/squareup/ui/help/HelpApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/help/HelpApplet;->activate()V

    return-void
.end method

.method public final getHelpApplet()Lcom/squareup/ui/help/HelpApplet;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpAppletGateway;->helpApplet:Lcom/squareup/ui/help/HelpApplet;

    return-object v0
.end method
