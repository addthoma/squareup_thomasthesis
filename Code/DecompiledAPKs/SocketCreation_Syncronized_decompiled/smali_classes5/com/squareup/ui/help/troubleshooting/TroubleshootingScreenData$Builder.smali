.class public Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;
.super Ljava/lang/Object;
.source "TroubleshootingScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private isSending:Z

.field private showEmailSupportLedger:Z

.field private showSendDiagnosticReport:Z

.field private showUploadSupportLedger:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;
    .locals 7

    .line 68
    new-instance v6, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;

    iget-boolean v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showUploadSupportLedger:Z

    iget-boolean v2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showEmailSupportLedger:Z

    iget-boolean v3, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showSendDiagnosticReport:Z

    iget-boolean v4, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->isSending:Z

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;-><init>(ZZZZLcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$1;)V

    return-object v6
.end method

.method public isSending(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;
    .locals 0

    .line 63
    iput-boolean p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->isSending:Z

    return-object p0
.end method

.method public showEmailSupportLedger(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;
    .locals 0

    .line 53
    iput-boolean p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showEmailSupportLedger:Z

    return-object p0
.end method

.method public showSendDiagnosticReport(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;
    .locals 0

    .line 58
    iput-boolean p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showSendDiagnosticReport:Z

    return-object p0
.end method

.method public showUploadSupportLedger(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;
    .locals 0

    .line 48
    iput-boolean p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showUploadSupportLedger:Z

    return-object p0
.end method
