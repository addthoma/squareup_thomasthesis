.class final Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingEligibilityState$1;
.super Ljava/lang/Object;
.source "MessagehubState.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/MessagehubState;->singleMessagingEligibilityState()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "Lcom/squareup/ui/help/messages/ConversationAllowableState;",
        "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
        "Lcom/squareup/ui/help/messages/MessagingEligibilityState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/help/messages/MessagingEligibilityState;",
        "allowState",
        "Lcom/squareup/ui/help/messages/ConversationAllowableState;",
        "credentialState",
        "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/MessagehubState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/MessagehubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingEligibilityState$1;->this$0:Lcom/squareup/ui/help/messages/MessagehubState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/help/messages/ConversationAllowableState;Lcom/squareup/ui/help/messages/MessagingCredentialsState;)Lcom/squareup/ui/help/messages/MessagingEligibilityState;
    .locals 1

    const-string v0, "allowState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "credentialState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingEligibilityState$1;->this$0:Lcom/squareup/ui/help/messages/MessagehubState;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/help/messages/MessagehubState;->access$processMessagingEligibilityState(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/ui/help/messages/ConversationAllowableState;Lcom/squareup/ui/help/messages/MessagingCredentialsState;)Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/help/messages/ConversationAllowableState;

    check-cast p2, Lcom/squareup/ui/help/messages/MessagingCredentialsState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingEligibilityState$1;->apply(Lcom/squareup/ui/help/messages/ConversationAllowableState;Lcom/squareup/ui/help/messages/MessagingCredentialsState;)Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    move-result-object p1

    return-object p1
.end method
