.class public final Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;
.super Ljava/lang/Object;
.source "HelpTroubleshootingRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final diagnosticCrasherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            ">;"
        }
    .end annotation
.end field

.field private final diagnosticsReporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/DiagnosticsReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final troubleshootingVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/DiagnosticsReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->diagnosticCrasherProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->diagnosticsReporterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->troubleshootingVisibilityProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p10, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/DiagnosticsReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;"
        }
    .end annotation

    .line 80
    new-instance v11, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/payment/ledger/DiagnosticsReporter;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;Lcom/squareup/util/ToastFactory;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;
    .locals 12

    .line 88
    new-instance v11, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;-><init>(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/payment/ledger/DiagnosticsReporter;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;Lcom/squareup/util/ToastFactory;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;
    .locals 11

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->diagnosticCrasherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/DiagnosticCrasher;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->diagnosticsReporterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/ledger/DiagnosticsReporter;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->troubleshootingVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->newInstance(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/payment/ledger/DiagnosticsReporter;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;Lcom/squareup/util/ToastFactory;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner_Factory;->get()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    move-result-object v0

    return-object v0
.end method
