.class final Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry$badgeCount$1;
.super Ljava/lang/Object;
.source "AnnouncementsSection.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->badgeCount()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "count",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Integer;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry$badgeCount$1;->this$0:Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Integer;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry$badgeCount$1;->this$0:Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;

    invoke-static {v0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->access$setNewCount$p(Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry$badgeCount$1;->accept(Ljava/lang/Integer;)V

    return-void
.end method
