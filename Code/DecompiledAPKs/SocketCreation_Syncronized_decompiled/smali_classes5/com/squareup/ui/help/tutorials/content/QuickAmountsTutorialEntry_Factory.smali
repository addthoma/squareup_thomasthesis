.class public final Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialEntry_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsTutorialVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->quickAmountsTutorialVisibilityProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->quickAmountsTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;"
        }
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Ldagger/Lazy;Lcom/squareup/util/Res;)Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;"
        }
    .end annotation

    .line 62
    new-instance v6, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;-><init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Ldagger/Lazy;Lcom/squareup/util/Res;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;
    .locals 5

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->quickAmountsTutorialVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->quickAmountsTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    iget-object v2, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v3, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->newInstance(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Ldagger/Lazy;Lcom/squareup/util/Res;)Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry_Factory;->get()Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    move-result-object v0

    return-object v0
.end method
