.class final Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2$1;
.super Ljava/lang/Object;
.source "JediHelpCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screenData",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 2

    .line 78
    instance-of v0, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;->$screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

    move-object v1, p1

    check-cast v1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->setScreenData(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getJediPanelView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/jedi/ui/JediPanelView;

    move-result-object v0

    const-string v1, "screenData"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;

    iget-object v1, v1, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getJediHelpScopeRunner$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Lcom/squareup/jedi/JediComponentInputHandler;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/jedi/ui/JediPanelView;->update(Lcom/squareup/jedi/JediHelpScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.jedi.JediComponentInputHandler"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2$1;->accept(Lcom/squareup/jedi/JediHelpScreenData;)V

    return-void
.end method
