.class final Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;
.super Lkotlin/jvm/internal/Lambda;
.source "JediHelpCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getOnLaunchHelpshift$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getGlassSpinner$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$1;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$1;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    const-string v2, "glassSpinner.spinnerTran\u2026Spinner(true)\n          }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 124
    new-instance v1, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$2;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getGlassSpinner$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    const-string v2, "glassSpinner.spinnerTran\u2026pinner(false)\n          }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 131
    new-instance v1, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$4;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onLaunchHelpshift\n      \u2026agingActivity(it, flow) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
