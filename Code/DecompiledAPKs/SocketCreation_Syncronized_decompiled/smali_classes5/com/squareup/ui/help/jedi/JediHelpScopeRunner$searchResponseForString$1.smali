.class final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner.kt"

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->searchResponseForString(Lcom/squareup/ui/help/jedi/JediHelpScreen;)Lio/reactivex/ObservableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Upstream:",
        "Ljava/lang/Object;",
        "Downstream:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableTransformer<",
        "Ljava/lang/String;",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00050\u00050\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "kotlin.jvm.PlatformType",
        "searchText",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->$screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 246
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$2;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 0

    .line 76
    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    return-object p1
.end method
