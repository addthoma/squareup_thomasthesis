.class public final Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;
.super Ljava/lang/Object;
.source "SupportMessagingNotificationNotifier.kt"

# interfaces
.implements Lcom/squareup/ui/help/SupportMessagingNotifier;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSupportMessagingNotificationNotifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SupportMessagingNotificationNotifier.kt\ncom/squareup/ui/help/messages/SupportMessagingNotificationNotifier\n+ 2 PushMessageDelegate.kt\ncom/squareup/pushmessages/PushMessageDelegateKt\n*L\n1#1,88:1\n16#2:89\n*E\n*S KotlinDebug\n*F\n+ 1 SupportMessagingNotificationNotifier.kt\ncom/squareup/ui/help/messages/SupportMessagingNotificationNotifier\n*L\n42#1:89\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001BA\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0012H\u0016J\u0008\u0010\u0016\u001a\u00020\u0012H\u0016J\u001c\u0010\u0017\u001a\u00020\u00122\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;",
        "Lcom/squareup/ui/help/SupportMessagingNotifier;",
        "delegate",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "notificationManager",
        "Landroid/app/NotificationManager;",
        "notificationWrapper",
        "Lcom/squareup/notification/NotificationWrapper;",
        "context",
        "Landroid/app/Application;",
        "messagesVisibility",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        "foregroundedActivityListener",
        "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/pushmessages/PushMessageDelegate;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lio/reactivex/Scheduler;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "removeNotification",
        "showNotification",
        "title",
        "",
        "body",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Application;

.field private final delegate:Lcom/squareup/pushmessages/PushMessageDelegate;

.field private final foregroundedActivityListener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;


# direct methods
.method public constructor <init>(Lcom/squareup/pushmessages/PushMessageDelegate;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationWrapper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesVisibility"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "foregroundedActivityListener"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->delegate:Lcom/squareup/pushmessages/PushMessageDelegate;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->notificationManager:Landroid/app/NotificationManager;

    iput-object p3, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iput-object p4, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->context:Landroid/app/Application;

    iput-object p5, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    iput-object p6, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->foregroundedActivityListener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    iput-object p7, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getMessagesVisibility$p(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;)Lcom/squareup/ui/help/messages/MessagesVisibility;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    return-object p0
.end method

.method public static final synthetic access$showNotification(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->showNotification(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final showNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->context:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    sget-object v1, Lcom/squareup/ui/help/HelpDeepLinksHandler;->HELP_MESSAGES_DEEP_LINK:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/squareup/ui/PaymentActivity;->createPendingIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v2, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->context:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    sget-object v3, Lcom/squareup/notification/Channels;->SUPPORT_MESSAGE:Lcom/squareup/notification/Channels;

    check-cast v3, Lcom/squareup/notification/Channel;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 75
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 76
    invoke-virtual {v1, p1}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 77
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 80
    invoke-virtual {p1, p2}, Landroidx/core/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 83
    iget-object p2, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 84
    sget v0, Lcom/squareup/applet/help/R$id;->notification_support_messages:I

    .line 83
    invoke-virtual {p2, v0, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->delegate:Lcom/squareup/pushmessages/PushMessageDelegate;

    .line 89
    const-class v1, Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;

    invoke-interface {v0, v1}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->foregroundedActivityListener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-virtual {v1}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->getForegroundedActivity()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 45
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 44
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;-><init>(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "delegate.observe<Support\u2026      }\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->removeNotification()V

    return-void
.end method

.method public removeNotification()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/applet/help/R$id;->notification_support_messages:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method
