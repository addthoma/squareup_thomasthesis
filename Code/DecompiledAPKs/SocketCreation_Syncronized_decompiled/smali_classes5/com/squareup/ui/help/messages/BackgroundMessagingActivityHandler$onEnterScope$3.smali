.class final Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;
.super Ljava/lang/Object;
.source "BackgroundMessagingActivityHandler.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;->this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/help/messages/ForegroundedActivity;)V
    .locals 1

    .line 52
    instance-of v0, p1, Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsHelpshiftActivity;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;->this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-static {p1}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->access$getMessagingController$p(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)Lcom/squareup/ui/help/MessagingController;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/help/MessagingController;->updateUnreadMessagesCount(I)V

    goto :goto_0

    .line 56
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsMainActivity;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;->this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-static {p1}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->access$beginIntervalPolling(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    goto :goto_0

    .line 59
    :cond_1
    instance-of p1, p1, Lcom/squareup/ui/help/messages/ForegroundedActivity$Background;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;->this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-static {p1}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->access$stopIntervalPolling(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/ui/help/messages/ForegroundedActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;->accept(Lcom/squareup/ui/help/messages/ForegroundedActivity;)V

    return-void
.end method
