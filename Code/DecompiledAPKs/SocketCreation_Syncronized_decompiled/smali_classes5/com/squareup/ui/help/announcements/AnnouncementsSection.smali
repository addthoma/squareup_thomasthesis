.class public final Lcom/squareup/ui/help/announcements/AnnouncementsSection;
.super Lcom/squareup/ui/help/AbstractHelpSection;
.source "AnnouncementsSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\tB\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/help/announcements/AnnouncementsSection;",
        "Lcom/squareup/ui/help/AbstractHelpSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "tapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "ListEntry",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsSection$1;-><init>(Lcom/squareup/settings/server/Features;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/AbstractHelpSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/help/announcements/AnnouncementsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 2

    .line 31
    sget-object v0, Lcom/squareup/ui/help/announcements/AnnouncementsScreen;->INSTANCE:Lcom/squareup/ui/help/announcements/AnnouncementsScreen;

    const-string v1, "AnnouncementsScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public tapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method
