.class public Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;
.super Ljava/lang/Object;
.source "OrderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Snapshot"
.end annotation


# instance fields
.field final hasError:Z

.field final orders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Order;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Order;",
            ">;)V"
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-boolean p1, p0, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->hasError:Z

    .line 75
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->orders:Ljava/util/List;

    return-void
.end method

.method static toSnapshot(Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;)Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 80
    new-instance p0, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;-><init>(ZLjava/util/List;)V

    return-object p0

    .line 82
    :cond_0
    invoke-static {}, Lcom/squareup/ui/help/orders/OrderHistory;->access$000()Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;

    move-result-object v1

    if-ne p0, v1, :cond_1

    .line 83
    new-instance p0, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;

    const/4 v0, 0x1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;-><init>(ZLjava/util/List;)V

    return-object p0

    .line 85
    :cond_1
    new-instance v1, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;

    iget-object p0, p0, Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;->order:Ljava/util/List;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;-><init>(ZLjava/util/List;)V

    return-object v1
.end method
