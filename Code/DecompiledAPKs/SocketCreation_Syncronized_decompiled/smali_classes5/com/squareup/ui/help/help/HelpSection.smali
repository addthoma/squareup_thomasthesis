.class public final Lcom/squareup/ui/help/help/HelpSection;
.super Lcom/squareup/ui/help/AbstractHelpSection;
.source "HelpSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/help/HelpSection$ListEntry;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u000bB\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/help/help/HelpSection;",
        "Lcom/squareup/ui/help/AbstractHelpSection;",
        "jediHelpSettings",
        "Lcom/squareup/jedi/JediHelpSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/Features;)V",
        "getInitialScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "tapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "ListEntry",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "jediHelpSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {v0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/AbstractHelpSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpSection;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    iput-object p2, p0, Lcom/squareup/ui/help/help/HelpSection;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 4

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpSection;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    invoke-interface {v0}, Lcom/squareup/jedi/JediHelpSettings;->isJediEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpSection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;

    new-instance v1, Lcom/squareup/jedi/JediWorkflowProps;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2, v3}, Lcom/squareup/jedi/JediWorkflowProps;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, v1}, Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;-><init>(Lcom/squareup/jedi/JediWorkflowProps;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 32
    :cond_0
    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->Companion:Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;->firstScreen()Lcom/squareup/ui/help/jedi/JediHelpScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 35
    :cond_1
    sget-object v0, Lcom/squareup/ui/help/help/HelpScreen;->INSTANCE:Lcom/squareup/ui/help/help/HelpScreen;

    const-string v1, "HelpScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    return-object v0
.end method

.method public tapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_HELP:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method
