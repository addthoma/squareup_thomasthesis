.class public interface abstract Lcom/squareup/ui/help/jedi/JediHelpScope$Component;
.super Ljava/lang/Object;
.source "JediHelpScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/help/jedi/JediHelpScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/jedi/JediHelpScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract jediHelpCallUsCoordinator()Lcom/squareup/ui/help/contact/ContactCoordinator;
.end method

.method public abstract jediHelpCoordinator()Lcom/squareup/ui/help/jedi/JediHelpCoordinator;
.end method

.method public abstract jediSession()Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;
.end method
