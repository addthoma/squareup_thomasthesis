.class public final Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHardwareCoordinator.java"


# instance fields
.field private marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private orderAllHardware:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

.field private orderContactless:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

.field private orderMagstripe:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

.field private runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/orders/OrderHardwareRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 54
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 55
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 56
    sget v0, Lcom/squareup/applet/help/R$id;->order_reader_magstripe:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderMagstripe:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    .line 57
    sget v0, Lcom/squareup/applet/help/R$id;->order_reader_contactless:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderContactless:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    .line 58
    sget v0, Lcom/squareup/applet/help/R$id;->order_reader_all_hardware:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderAllHardware:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    return-void
.end method

.method public static synthetic lambda$eYgbXSPRyFWhMUmvOVIuNpn5DKw(Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->updateState(Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;)V

    return-void
.end method

.method private updateState(Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderMagstripe:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    sget v1, Lcom/squareup/common/strings/R$string;->order_reader_magstripe:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->setText(I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderMagstripe:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    iget-boolean v1, p1, Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;->showR4:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderContactless:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    iget-boolean p1, p1, Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;->showR12:Z

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->bindViews(Landroid/view/View;)V

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v2, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;

    sget v3, Lcom/squareup/common/strings/R$string;->order_reader:I

    .line 34
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->getActionBarConfig(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 33
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderMagstripe:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$KHx-pr8F2r6vf581W_Y5yNz-Qcc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$KHx-pr8F2r6vf581W_Y5yNz-Qcc;-><init>(Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;)V

    .line 37
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderContactless:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$d0p3yoYrrw-w9gMN7KPwR_MF_cE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$d0p3yoYrrw-w9gMN7KPwR_MF_cE;-><init>(Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;)V

    .line 39
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->orderAllHardware:Lcom/squareup/ui/help/orders/OrderHardwareItemView;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$IvxglAb3QeVKL_mRKT58SjDAYL0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$IvxglAb3QeVKL_mRKT58SjDAYL0;-><init>(Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;)V

    .line 41
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    new-instance v0, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$VGA7s8DFkFTRSGrRfjWb03enXM4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$VGA7s8DFkFTRSGrRfjWb03enXM4;-><init>(Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$OrderHardwareCoordinator(Landroid/view/View;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->onMagstripeTapped(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$attach$1$OrderHardwareCoordinator(Landroid/view/View;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->onContactlessTapped(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$attach$2$OrderHardwareCoordinator(Landroid/view/View;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->onAllHardwareTapped(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$attach$3$OrderHardwareCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;->runner:Lcom/squareup/ui/help/orders/OrderHardwareRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->state()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$eYgbXSPRyFWhMUmvOVIuNpn5DKw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHardwareCoordinator$eYgbXSPRyFWhMUmvOVIuNpn5DKw;-><init>(Lcom/squareup/ui/help/orders/OrderHardwareCoordinator;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
