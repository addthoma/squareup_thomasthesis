.class public final enum Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;
.super Ljava/lang/Enum;
.source "OrdersVisibility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrdersVisibility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScreenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

.field public static final enum HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

.field public static final enum LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

.field public static final enum MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

.field public static final enum NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 70
    new-instance v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    const/4 v1, 0x0

    const-string v2, "HARDWARE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    .line 73
    new-instance v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    const/4 v2, 0x1

    const-string v3, "MAGSTRIPE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    .line 76
    new-instance v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    const/4 v3, 0x2

    const-string v4, "LEGACY"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    .line 79
    new-instance v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    const/4 v4, 0x3

    const-string v5, "NONE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    .line 68
    sget-object v5, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->$VALUES:[Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;
    .locals 1

    .line 68
    const-class v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->$VALUES:[Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v0}, [Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object v0
.end method


# virtual methods
.method public isLegacy()Z
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNative()Z
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
