.class public abstract Lcom/squareup/ui/help/HelpAppletContent;
.super Ljava/lang/Object;
.source "HelpAppletContent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008&\u0018\u00002\u00020\u0001B;\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0003\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\rH\u0017J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "",
        "titleStringId",
        "",
        "subtitle",
        "",
        "linkStringId",
        "registerTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "badgeStringId",
        "(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;)V",
        "Ljava/lang/Integer;",
        "handleClick",
        "",
        "shouldDisplay",
        "shouldDisplayObservable",
        "Lio/reactivex/Observable;",
        "updateBadge",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public badgeStringId:Ljava/lang/Integer;

.field public final linkStringId:Ljava/lang/Integer;

.field public final registerTapName:Lcom/squareup/analytics/RegisterTapName;

.field public final subtitle:Ljava/lang/CharSequence;

.field public final titleStringId:I


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;)V
    .locals 1

    const-string v0, "registerTapName"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/help/HelpAppletContent;->titleStringId:I

    iput-object p2, p0, Lcom/squareup/ui/help/HelpAppletContent;->subtitle:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/ui/help/HelpAppletContent;->linkStringId:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/squareup/ui/help/HelpAppletContent;->registerTapName:Lcom/squareup/analytics/RegisterTapName;

    iput-object p5, p0, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 9
    move-object p2, v0

    check-cast p2, Ljava/lang/CharSequence;

    :cond_0
    move-object v3, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    .line 10
    move-object p3, v0

    check-cast p3, Ljava/lang/Integer;

    :cond_1
    move-object v4, p3

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_2

    .line 12
    move-object p5, v0

    check-cast p5, Ljava/lang/Integer;

    :cond_2
    move-object v6, p5

    move-object v1, p0

    move v2, p1

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public handleClick()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldDisplay()Z
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Please use shouldDisplayObservable() instead."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "shouldDisplayObservable()"
            imports = {}
        .end subannotation
    .end annotation

    const/4 v0, 0x1

    return v0
.end method

.method public shouldDisplayObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletContent;->shouldDisplay()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(shouldDisplay())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public updateBadge()V
    .locals 0

    return-void
.end method
