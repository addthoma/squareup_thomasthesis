.class public Lcom/squareup/ui/help/HelpAppletSectionsList;
.super Lcom/squareup/applet/AppletSectionsList;
.source "HelpAppletSectionsList.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final helpAppletSelectedSectionSetting:Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;

.field private final referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/api/HelpAppletEntryPoint;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/help/referrals/ReferralsVisibility;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletSectionsList;-><init>(Lcom/squareup/applet/AppletEntryPoint;)V

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->helpAppletSelectedSectionSetting:Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;

    .line 29
    iput-object p4, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->analytics:Lcom/squareup/analytics/Analytics;

    .line 30
    iput-object p5, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    .line 32
    invoke-interface {p2}, Lcom/squareup/ui/help/api/HelpAppletSettings;->helpAppletSectionsEntries()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/applet/AppletSectionsListEntry;

    .line 33
    invoke-direct {p0, p2}, Lcom/squareup/ui/help/HelpAppletSectionsList;->isVisible(Lcom/squareup/applet/AppletSectionsListEntry;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 34
    iget-object p3, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->visibleEntries:Ljava/util/List;

    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private isVisible(Lcom/squareup/applet/AppletSectionsListEntry;)Z
    .locals 0

    .line 60
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    return p1
.end method


# virtual methods
.method public setSelectedSection(Lcom/squareup/applet/AppletSection;)V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->helpAppletSelectedSectionSetting:Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;

    invoke-virtual {v0}, Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/AbstractHelpSection;

    .line 46
    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0}, Lcom/squareup/ui/help/AbstractHelpSection;->tapName()Lcom/squareup/analytics/RegisterTapName;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 47
    instance-of v0, p1, Lcom/squareup/ui/help/referrals/ReferralsSection;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsList;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->markReferralViewed()V

    .line 51
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/help/setupguide/SetupGuideSection;

    if-eqz v0, :cond_1

    .line 52
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/setupguide/SetupGuideSection;

    invoke-virtual {v0}, Lcom/squareup/ui/help/setupguide/SetupGuideSection;->logSetupGuideOpened()V

    .line 56
    :cond_1
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsList;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
