.class public abstract Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;
.super Ljava/lang/Object;
.source "LibrariesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/about/LibrariesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Divider;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Footer;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u00020\u0001:\u0004\u0007\u0008\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
        "",
        "type",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;",
        "(Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;)V",
        "getType",
        "()Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;",
        "Divider",
        "Footer",
        "LibraryItem",
        "LicenseHeader",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Divider;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Footer;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final type:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;->type:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;-><init>(Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;)V

    return-void
.end method


# virtual methods
.method public final getType()Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;->type:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    return-object v0
.end method
