.class public final Lcom/squareup/ui/help/HelpApplet_Factory;
.super Ljava/lang/Object;
.source "HelpApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/HelpApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final entryPointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final helpBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->helpBadgeProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->sectionsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->entryPointProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/HelpApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ">;)",
            "Lcom/squareup/ui/help/HelpApplet_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/ui/help/HelpApplet_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/HelpApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletEntryPoint;)Lcom/squareup/ui/help/HelpApplet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/help/HelpBadge;",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ")",
            "Lcom/squareup/ui/help/HelpApplet;"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/ui/help/HelpApplet;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/HelpApplet;-><init>(Ldagger/Lazy;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletEntryPoint;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/HelpApplet;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->helpBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/HelpBadge;

    iget-object v2, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->sectionsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/help/HelpAppletSectionsList;

    iget-object v3, p0, Lcom/squareup/ui/help/HelpApplet_Factory;->entryPointProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/help/api/HelpAppletEntryPoint;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/help/HelpApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletEntryPoint;)Lcom/squareup/ui/help/HelpApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpApplet_Factory;->get()Lcom/squareup/ui/help/HelpApplet;

    move-result-object v0

    return-object v0
.end method
