.class public Lcom/squareup/ui/help/jedi/JediHelpScope;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "JediHelpScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/help/jedi/JediHelpScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/jedi/JediHelpScope$Component;,
        Lcom/squareup/ui/help/jedi/JediHelpScope$ParentComponent;,
        Lcom/squareup/ui/help/jedi/JediHelpScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/jedi/JediHelpScope;",
            ">;"
        }
    .end annotation
.end field

.field static final INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScope;

    invoke-direct {v0}, Lcom/squareup/ui/help/jedi/JediHelpScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpScope;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpScope;

    .line 40
    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScope;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;

    .line 19
    invoke-interface {v0}, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;->jediSession()Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
