.class public final Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;
.super Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.source "BuyerCartAdapterItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BranSectionHeader"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0008H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u000cH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00088WX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\n\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "displayItem",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "(Lcom/squareup/comms/protos/seller/DisplayItem;)V",
        "getDisplayItem",
        "()Lcom/squareup/comms/protos/seller/DisplayItem;",
        "id",
        "",
        "getId",
        "()I",
        "label",
        "",
        "getLabel",
        "()Ljava/lang/String;",
        "layoutId",
        "getLayoutId",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

.field private final id:I

.field private final label:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/protos/seller/DisplayItem;)V
    .locals 1

    const-string v0, "displayItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    iget-object p1, p1, Lcom/squareup/comms/protos/seller/DisplayItem;->client_id:Ljava/lang/String;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->id:I

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    iget-object p1, p1, Lcom/squareup/comms/protos/seller/DisplayItem;->name:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->label:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;Lcom/squareup/comms/protos/seller/DisplayItem;ILjava/lang/Object;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->copy(Lcom/squareup/comms/protos/seller/DisplayItem;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    return-object v0
.end method

.method public final copy(Lcom/squareup/comms/protos/seller/DisplayItem;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;
    .locals 1

    const-string v0, "displayItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;-><init>(Lcom/squareup/comms/protos/seller/DisplayItem;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    iget-object p1, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->id:I

    return v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 48
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_section_header:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BranSectionHeader(displayItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
