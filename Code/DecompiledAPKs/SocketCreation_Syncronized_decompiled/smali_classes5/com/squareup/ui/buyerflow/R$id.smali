.class public final Lcom/squareup/ui/buyerflow/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyerflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final agreement_label:I = 0x7f0a01ab

.field public static final agreement_label_signature_buttons_container:I = 0x7f0a01ac

.field public static final bottom_line:I = 0x7f0a0244

.field public static final bottom_panel:I = 0x7f0a0246

.field public static final buyer_action_bar:I = 0x7f0a027c

.field public static final buyer_actionbar_call_to_action:I = 0x7f0a027f

.field public static final buyer_actionbar_customer_add_card_button:I = 0x7f0a0280

.field public static final buyer_actionbar_customer_button:I = 0x7f0a0281

.field public static final buyer_actionbar_left_flank:I = 0x7f0a0282

.field public static final buyer_actionbar_name:I = 0x7f0a0283

.field public static final buyer_actionbar_new_sale_button:I = 0x7f0a0284

.field public static final buyer_actionbar_subtitle:I = 0x7f0a0285

.field public static final buyer_actionbar_total:I = 0x7f0a0286

.field public static final buyer_actionbar_up_glyph:I = 0x7f0a0287

.field public static final buyer_loyalty_account_container:I = 0x7f0a028c

.field public static final buyer_send_receipt_digital_hint:I = 0x7f0a0292

.field public static final buyer_ticket_name:I = 0x7f0a0293

.field public static final charge_and_continue_button:I = 0x7f0a0306

.field public static final content_frame:I = 0x7f0a03a6

.field public static final copy_link:I = 0x7f0a03af

.field public static final coupon_content:I = 0x7f0a03b6

.field public static final coupon_redeem_cancel:I = 0x7f0a03ba

.field public static final coupon_redeem_claim_button:I = 0x7f0a03bb

.field public static final coupon_redeem_title:I = 0x7f0a03bc

.field public static final coupon_ribbon:I = 0x7f0a03bd

.field public static final custom_tip:I = 0x7f0a052b

.field public static final custom_tip_amount:I = 0x7f0a052c

.field public static final custom_tip_amount_submit:I = 0x7f0a052d

.field public static final custom_tip_button:I = 0x7f0a052e

.field public static final custom_tip_row_entry:I = 0x7f0a0530

.field public static final customer_add_card_button:I = 0x7f0a0531

.field public static final customer_button:I = 0x7f0a0533

.field public static final customer_image:I = 0x7f0a0537

.field public static final earned_points_container:I = 0x7f0a060a

.field public static final earned_rewards_container:I = 0x7f0a060b

.field public static final email_collection_all_done_container:I = 0x7f0a06b5

.field public static final email_collection_all_done_message:I = 0x7f0a06b6

.field public static final email_collection_customer:I = 0x7f0a06b7

.field public static final email_collection_email:I = 0x7f0a06b8

.field public static final email_collection_enroll_container:I = 0x7f0a06b9

.field public static final email_collection_new_sale:I = 0x7f0a06ba

.field public static final email_collection_no_thanks:I = 0x7f0a06bb

.field public static final email_collection_prompt:I = 0x7f0a06bc

.field public static final email_collection_submit_button:I = 0x7f0a06bd

.field public static final email_disclaimer:I = 0x7f0a06be

.field public static final email_receipt_field:I = 0x7f0a06c4

.field public static final email_send_button:I = 0x7f0a06cb

.field public static final emv_application_wrapper:I = 0x7f0a06f7

.field public static final emv_options_container:I = 0x7f0a06f9

.field public static final focus_grabber:I = 0x7f0a0766

.field public static final glyph:I = 0x7f0a07ae

.field public static final glyph_subtitle:I = 0x7f0a07b6

.field public static final hide_on_click_layout:I = 0x7f0a07d7

.field public static final initial_button:I = 0x7f0a0831

.field public static final loyalty_all_done_container:I = 0x7f0a0965

.field public static final loyalty_all_done_glyph:I = 0x7f0a0966

.field public static final loyalty_all_done_text:I = 0x7f0a0967

.field public static final loyalty_cash_app_banner:I = 0x7f0a0968

.field public static final loyalty_cash_app_text:I = 0x7f0a0969

.field public static final loyalty_claim_your_star:I = 0x7f0a096a

.field public static final loyalty_claim_your_star_help:I = 0x7f0a096b

.field public static final loyalty_congratulations_points_amount:I = 0x7f0a096c

.field public static final loyalty_congratulations_points_term:I = 0x7f0a096d

.field public static final loyalty_congratulations_rewards_amount:I = 0x7f0a096e

.field public static final loyalty_congratulations_rewards_term:I = 0x7f0a096f

.field public static final loyalty_congratulations_title:I = 0x7f0a0970

.field public static final loyalty_customer:I = 0x7f0a0972

.field public static final loyalty_customer_add_card_button:I = 0x7f0a0973

.field public static final loyalty_enroll_container:I = 0x7f0a097b

.field public static final loyalty_join:I = 0x7f0a097c

.field public static final loyalty_new_sale:I = 0x7f0a097d

.field public static final loyalty_no_thanks:I = 0x7f0a097e

.field public static final loyalty_phone_edit:I = 0x7f0a097f

.field public static final loyalty_program_name:I = 0x7f0a0982

.field public static final loyalty_reward_container:I = 0x7f0a0991

.field public static final loyalty_reward_requirement:I = 0x7f0a0992

.field public static final loyalty_reward_status:I = 0x7f0a0993

.field public static final loyalty_tier_requirement:I = 0x7f0a09a1

.field public static final loyalty_tier_status:I = 0x7f0a09a2

.field public static final loyalty_view_reward_tiers:I = 0x7f0a09a6

.field public static final more_options:I = 0x7f0a09f4

.field public static final new_sale:I = 0x7f0a0a1e

.field public static final no_receipt_button:I = 0x7f0a0a2d

.field public static final no_tip_button:I = 0x7f0a0a31

.field public static final order_name_cardholder_name_status:I = 0x7f0a0ad8

.field public static final order_name_edit_text:I = 0x7f0a0ad9

.field public static final order_name_fetch_progress:I = 0x7f0a0ada

.field public static final order_name_next_button:I = 0x7f0a0adb

.field public static final partial_auth_message:I = 0x7f0a0bb1

.field public static final please_sign_here:I = 0x7f0a0c39

.field public static final points_amount:I = 0x7f0a0c3a

.field public static final post_receipt_glyph:I = 0x7f0a0c3e

.field public static final print_formal_receipt_button:I = 0x7f0a0c66

.field public static final print_receipt_button:I = 0x7f0a0c6e

.field public static final receipt_frame:I = 0x7f0a0d08

.field public static final receipt_options:I = 0x7f0a0d0f

.field public static final receipt_subtitle:I = 0x7f0a0d11

.field public static final receipt_title:I = 0x7f0a0d12

.field public static final receipt_top_panel:I = 0x7f0a0d13

.field public static final refund_policy:I = 0x7f0a0d41

.field public static final refund_policy_popup_title:I = 0x7f0a0d42

.field public static final refund_policy_x_button:I = 0x7f0a0d43

.field public static final remaining_balance:I = 0x7f0a0d54

.field public static final reward_tier_container:I = 0x7f0a0d85

.field public static final reward_tiers_brand_image:I = 0x7f0a0d86

.field public static final rewards_points_brand_image:I = 0x7f0a0d88

.field public static final send_buyer_loyalty_status_button:I = 0x7f0a0e64

.field public static final share_link_email_sent_helper:I = 0x7f0a0e75

.field public static final sign_clear_button:I = 0x7f0a0e82

.field public static final sign_done_button:I = 0x7f0a0e83

.field public static final sign_line:I = 0x7f0a0e85

.field public static final sign_line_x:I = 0x7f0a0e86

.field public static final sign_x:I = 0x7f0a0e8d

.field public static final signature_buttons:I = 0x7f0a0e8f

.field public static final signature_canvas:I = 0x7f0a0e90

.field public static final sms_container:I = 0x7f0a0eab

.field public static final sms_disclaimer:I = 0x7f0a0eac

.field public static final sms_receipt_field:I = 0x7f0a0eb1

.field public static final sms_send_button:I = 0x7f0a0eb2

.field public static final subtitle:I = 0x7f0a0f49

.field public static final switch_language_button:I = 0x7f0a0f58

.field public static final tier_desc:I = 0x7f0a0fda

.field public static final tip_animator:I = 0x7f0a1035

.field public static final tip_animator_container:I = 0x7f0a1036

.field public static final tip_bar:I = 0x7f0a1037

.field public static final title:I = 0x7f0a103f

.field public static final top_line:I = 0x7f0a1051


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
