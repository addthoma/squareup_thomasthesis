.class Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;
.super Ljava/lang/Object;
.source "NfcProcessor.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/NfcProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultNfcListenerOverrider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/NfcProcessor;)V
    .locals 0

    .line 1030
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setPaymentCompletionListener()V
    .locals 2

    .line 1032
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$700(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    return-void
.end method

.method public unsetPaymentCompletionListener()V
    .locals 2

    .line 1037
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$700(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;->this$0:Lcom/squareup/ui/NfcProcessor;

    if-ne v0, v1, :cond_0

    .line 1038
    invoke-static {v1}, Lcom/squareup/ui/NfcProcessor;->access$700(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPaymentCompletionListener()V

    :cond_0
    return-void
.end method
