.class final enum Lcom/squareup/ui/items/EditItemState$ItemImageState;
.super Ljava/lang/Enum;
.source "EditItemState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ItemImageState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/EditItemState$ItemImageState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/EditItemState$ItemImageState;

.field public static final enum CLEAN:Lcom/squareup/ui/items/EditItemState$ItemImageState;

.field public static final enum DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

.field public static final enum DIRTY:Lcom/squareup/ui/items/EditItemState$ItemImageState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 76
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const/4 v1, 0x0

    const-string v2, "CLEAN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/EditItemState$ItemImageState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->CLEAN:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    new-instance v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const/4 v2, 0x1

    const-string v3, "DIRTY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/items/EditItemState$ItemImageState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DIRTY:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    new-instance v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const/4 v3, 0x2

    const-string v4, "DELETED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/items/EditItemState$ItemImageState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/items/EditItemState$ItemImageState;

    sget-object v4, Lcom/squareup/ui/items/EditItemState$ItemImageState;->CLEAN:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DIRTY:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->$VALUES:[Lcom/squareup/ui/items/EditItemState$ItemImageState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/EditItemState$ItemImageState;
    .locals 1

    .line 76
    const-class v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/EditItemState$ItemImageState;
    .locals 1

    .line 76
    sget-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->$VALUES:[Lcom/squareup/ui/items/EditItemState$ItemImageState;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/EditItemState$ItemImageState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/EditItemState$ItemImageState;

    return-object v0
.end method
