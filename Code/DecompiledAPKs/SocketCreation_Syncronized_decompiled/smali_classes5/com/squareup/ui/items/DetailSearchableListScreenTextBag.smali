.class public final Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
.super Ljava/lang/Object;
.source "DetailSearchableListScreenTextBag.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0018\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 !2\u00020\u0001:\u0001!BC\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0003\u0012\n\u0008\u0003\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJL\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u00032\n\u0008\u0003\u0010\u0008\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001aJ\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000bR\u0015\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000b\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "",
        "searchHintId",
        "",
        "titleId",
        "createButtonTextId",
        "emptyViewTitleId",
        "emptyViewSubtitleLearnMoreLinkId",
        "objectNumberLimitHelpTextId",
        "(IIIIILjava/lang/Integer;)V",
        "getCreateButtonTextId",
        "()I",
        "getEmptyViewSubtitleLearnMoreLinkId",
        "getEmptyViewTitleId",
        "getObjectNumberLimitHelpTextId",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getSearchHintId",
        "getTitleId",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "(IIIIILjava/lang/Integer;)Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "Companion",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

.field private static final DetailSearchableListScreenTextBagForDiscounts:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

.field private static final DetailSearchableListScreenTextBagForItems:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

.field private static final DetailSearchableListScreenTextBagForServices:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

.field private static final DetailSearchableListScreenTextBagForUnits:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;


# instance fields
.field private final createButtonTextId:I

.field private final emptyViewSubtitleLearnMoreLinkId:I

.field private final emptyViewTitleId:I

.field private final objectNumberLimitHelpTextId:Ljava/lang/Integer;

.field private final searchHintId:I

.field private final titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

    .line 21
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 22
    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_search_units:I

    .line 23
    sget v4, Lcom/squareup/itemsapplet/R$string;->items_applet_units_title:I

    .line 24
    sget v5, Lcom/squareup/itemsapplet/R$string;->create_unit_button_text:I

    .line 25
    sget v6, Lcom/squareup/itemsapplet/R$string;->items_applet_detail_searchable_list_empty_view_title_for_units:I

    .line 26
    sget v7, Lcom/squareup/edititem/R$string;->unit_type_hint_url:I

    .line 27
    sget v1, Lcom/squareup/editunit/R$string;->unit_number_limit_help_text:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object v2, v0

    .line 21
    invoke-direct/range {v2 .. v8}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForUnits:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 30
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 31
    sget v10, Lcom/squareup/itemsapplet/R$string;->items_applet_search_discounts:I

    .line 32
    sget v11, Lcom/squareup/itemsapplet/R$string;->items_applet_discounts_title:I

    .line 33
    sget v12, Lcom/squareup/registerlib/R$string;->create_discount:I

    .line 34
    sget v13, Lcom/squareup/itemsapplet/R$string;->items_applet_discounts_null_title:I

    .line 35
    sget v14, Lcom/squareup/itemsapplet/R$string;->discounts_hint_url:I

    const/4 v15, 0x0

    const/16 v16, 0x20

    const/16 v17, 0x0

    move-object v9, v0

    .line 30
    invoke-direct/range {v9 .. v17}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForDiscounts:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 38
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 39
    sget v2, Lcom/squareup/configure/item/R$string;->search_library_hint_all_items:I

    .line 40
    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_all_items_title:I

    .line 41
    sget v4, Lcom/squareup/registerlib/R$string;->create_item:I

    .line 42
    sget v5, Lcom/squareup/itemsapplet/R$string;->items_applet_items_null_title:I

    .line 43
    sget v6, Lcom/squareup/registerlib/R$string;->items_hint_url:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, v0

    .line 38
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForItems:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 46
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 47
    sget v11, Lcom/squareup/registerlib/R$string;->search_library_hint_all_services:I

    .line 48
    sget v12, Lcom/squareup/itemsapplet/R$string;->items_applet_all_services_title:I

    .line 49
    sget v13, Lcom/squareup/registerlib/R$string;->create_service:I

    .line 50
    sget v14, Lcom/squareup/itemsapplet/R$string;->items_applet_services_null_title:I

    .line 51
    sget v15, Lcom/squareup/registerlib/R$string;->items_hint_url:I

    const/16 v16, 0x0

    const/16 v17, 0x20

    const/16 v18, 0x0

    move-object v10, v0

    .line 46
    invoke-direct/range {v10 .. v18}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForServices:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-void
.end method

.method public constructor <init>(IIIIILjava/lang/Integer;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    iput p2, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    iput p3, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    iput p4, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    iput p5, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    return-void
.end method

.method public synthetic constructor <init>(IIIIILjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    .line 18
    check-cast p6, Ljava/lang/Integer;

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;)V

    return-void
.end method

.method public static final synthetic access$getDetailSearchableListScreenTextBagForDiscounts$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForDiscounts:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public static final synthetic access$getDetailSearchableListScreenTextBagForItems$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForItems:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public static final synthetic access$getDetailSearchableListScreenTextBagForServices$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForServices:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public static final synthetic access$getDetailSearchableListScreenTextBagForUnits$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->DetailSearchableListScreenTextBagForUnits:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;IIIIILjava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget p1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget p2, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->copy(IIIIILjava/lang/Integer;)Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    return v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(IIIIILjava/lang/Integer;)Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 8

    new-instance v7, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-object v0, v7

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    iget v1, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    iget v1, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    iget v1, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    iget v1, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    iget v1, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCreateButtonTextId()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    return v0
.end method

.method public final getEmptyViewSubtitleLearnMoreLinkId()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    return v0
.end method

.method public final getEmptyViewTitleId()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    return v0
.end method

.method public final getObjectNumberLimitHelpTextId()Ljava/lang/Integer;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSearchHintId()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    return v0
.end method

.method public final getTitleId()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetailSearchableListScreenTextBag(searchHintId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->searchHintId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->titleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", createButtonTextId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->createButtonTextId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", emptyViewTitleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewTitleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", emptyViewSubtitleLearnMoreLinkId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->emptyViewSubtitleLearnMoreLinkId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", objectNumberLimitHelpTextId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->objectNumberLimitHelpTextId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
