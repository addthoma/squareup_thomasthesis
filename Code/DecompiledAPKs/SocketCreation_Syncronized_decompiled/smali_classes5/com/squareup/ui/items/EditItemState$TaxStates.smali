.class final Lcom/squareup/ui/items/EditItemState$TaxStates;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TaxStates"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemState$TaxStates;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dirty:Z

.field itemId:Ljava/lang/String;

.field private final table:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1151
    new-instance v0, Lcom/squareup/ui/items/EditItemState$TaxStates$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemState$TaxStates$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$TaxStates;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 1057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 1055
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    .line 1058
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private constructor <init>(Ljava/util/LinkedHashMap;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 1062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 1055
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    .line 1063
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    .line 1064
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->itemId:Ljava/lang/String;

    .line 1065
    iput-boolean p3, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/LinkedHashMap;Ljava/lang/String;ZLcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 1052
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/EditItemState$TaxStates;-><init>(Ljava/util/LinkedHashMap;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemState$TaxStates;)Ljava/util/List;
    .locals 0

    .line 1052
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemState$TaxStates;->getDeletedMemberships()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/EditItemState$TaxStates;)Ljava/util/List;
    .locals 0

    .line 1052
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemState$TaxStates;->getNewMemberships()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private createTaxState(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;Z)Lcom/squareup/ui/items/EditItemState$TaxState;
    .locals 9

    if-nez p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, p3

    .line 1119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "itemFeeMembership is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "null"

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not null, while isRelated is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1123
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getPercentage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v4

    if-nez p2, :cond_2

    const/4 p2, 0x0

    goto :goto_2

    .line 1124
    :cond_2
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;->toByteArray()[B

    move-result-object p2

    :goto_2
    move-object v6, p2

    .line 1125
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v7

    .line 1126
    new-instance p2, Lcom/squareup/ui/items/EditItemState$TaxState;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x0

    move-object v1, p2

    move v5, p3

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/EditItemState$TaxState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BZLcom/squareup/ui/items/EditItemState$1;)V

    return-object p2
.end method

.method private getDeletedMemberships()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 1141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1142
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/EditItemState$TaxState;

    .line 1143
    iget-boolean v3, v2, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    if-nez v3, :cond_0

    iget-boolean v3, v2, Lcom/squareup/ui/items/EditItemState$TaxState;->originallyApplied:Z

    if-nez v3, :cond_1

    goto :goto_0

    .line 1145
    :cond_1
    invoke-static {v2}, Lcom/squareup/ui/items/EditItemState$TaxState;->access$900(Lcom/squareup/ui/items/EditItemState$TaxState;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    move-result-object v2

    .line 1146
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private getNewMemberships()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 1130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1131
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/EditItemState$TaxState;

    .line 1132
    iget-boolean v3, v2, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/squareup/ui/items/EditItemState$TaxState;->originallyApplied:Z

    if-eqz v3, :cond_1

    goto :goto_0

    .line 1133
    :cond_1
    new-instance v3, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    invoke-direct {v3}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;-><init>()V

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$TaxState;->id:Ljava/lang/String;

    .line 1134
    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->setFeeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->itemId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    move-result-object v2

    .line 1135
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getStates()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;"
        }
    .end annotation

    .line 1093
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method isDirty()Z
    .locals 1

    .line 1097
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    return v0
.end method

.method isTaxAppliedToItem(Ljava/lang/String;)Z
    .locals 1

    .line 1088
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemState$TaxState;

    if-eqz p1, :cond_0

    .line 1089
    iget-boolean p1, p1, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method load(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)V"
        }
    .end annotation

    .line 1107
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1108
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/Related;

    .line 1109
    iget-object v1, v0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 1110
    iget-object v2, v0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    iget-boolean v0, v0, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/items/EditItemState$TaxStates;->createTaxState(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;Z)Lcom/squareup/ui/items/EditItemState$TaxState;

    move-result-object v0

    .line 1111
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method loadTaxesForNewItem(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;)V"
        }
    .end annotation

    .line 1069
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1070
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1072
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/items/EditItemState$TaxStates;->createTaxState(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;Z)Lcom/squareup/ui/items/EditItemState$TaxState;

    move-result-object v1

    .line 1074
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v2

    iput-boolean v2, v1, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    .line 1075
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method reset(Ljava/lang/String;)V
    .locals 1

    .line 1101
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1102
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->itemId:Ljava/lang/String;

    const/4 p1, 0x0

    .line 1103
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    return-void
.end method

.method setApplied(Ljava/lang/String;Z)V
    .locals 1

    .line 1080
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemState$TaxState;

    .line 1081
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    if-eq v0, p2, :cond_0

    const/4 v0, 0x1

    .line 1082
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    .line 1083
    iput-boolean p2, p1, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1170
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1171
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->itemId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1172
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$TaxStates;->dirty:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
