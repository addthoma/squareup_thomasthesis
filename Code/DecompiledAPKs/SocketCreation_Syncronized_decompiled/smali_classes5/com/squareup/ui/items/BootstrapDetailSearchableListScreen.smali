.class public final Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "BootstrapDetailSearchableListScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00172\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u0017B\u000f\u0008\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0012\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\u0006\u0012\u0002\u0008\u00030\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "Lcom/squareup/container/spot/HasSpot;",
        "configuration",
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getName",
        "",
        "getParentKey",
        "",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "context",
        "Landroid/content/Context;",
        "Companion",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ALL_DISCOUNTS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

.field private static final ALL_ITEMS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

.field private static final ALL_SERVICES_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

.field private static final ALL_UNITS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

.field public static final Companion:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;


# instance fields
.field private final configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;

    .line 40
    new-instance v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getUNITS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    sput-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_UNITS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    .line 41
    new-instance v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getDISCOUNTS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    sput-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_DISCOUNTS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    .line 42
    new-instance v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getITEMS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    sput-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_ITEMS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    .line 43
    new-instance v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getSERVICES()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    sput-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_SERVICES_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-void
.end method

.method public static final synthetic access$getALL_DISCOUNTS_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_DISCOUNTS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    return-object v0
.end method

.method public static final synthetic access$getALL_ITEMS_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_ITEMS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    return-object v0
.end method

.method public static final synthetic access$getALL_SERVICES_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_SERVICES_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    return-object v0
.end method

.method public static final synthetic access$getALL_UNITS_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->ALL_UNITS_INSTANCE:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    return-object v0
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->Companion:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/container/BootstrapTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;

    sget-object v1, Lcom/squareup/ui/items/ItemsAppletScope;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletScope;

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object v2, p0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 29
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->Companion:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;->getSection(Lcom/squareup/ui/items/DetailSearchableListDataType;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 23
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const-string v0, "Spots.RIGHT_STABLE_ACTION_BAR"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
