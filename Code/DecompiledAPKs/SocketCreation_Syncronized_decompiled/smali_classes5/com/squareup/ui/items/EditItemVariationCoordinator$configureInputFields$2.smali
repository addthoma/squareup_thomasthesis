.class public final Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditItemVariationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData:Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

.field final synthetic $unitAbbreviation:Ljava/lang/String;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->$view:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->$screenData:Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    iput-object p4, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->$unitAbbreviation:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getEditItemVariationRunner$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->variationPriceChanged(Ljava/lang/String;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->$view:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->$screenData:Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;->$unitAbbreviation:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$updateHelpText(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;ZLjava/lang/String;)V

    return-void
.end method
