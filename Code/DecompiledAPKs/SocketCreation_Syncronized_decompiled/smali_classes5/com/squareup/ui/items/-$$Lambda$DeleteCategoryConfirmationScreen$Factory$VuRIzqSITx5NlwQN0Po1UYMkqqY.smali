.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/analytics/Analytics;

.field private final synthetic f$1:Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

.field private final synthetic f$2:Landroid/content/Context;

.field private final synthetic f$3:Lcom/squareup/ui/items/LibraryDeleter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;Landroid/content/Context;Lcom/squareup/ui/items/LibraryDeleter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$0:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$1:Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$2:Landroid/content/Context;

    iput-object p4, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$3:Lcom/squareup/ui/items/LibraryDeleter;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$0:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$1:Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$2:Landroid/content/Context;

    iget-object v3, p0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;->f$3:Lcom/squareup/ui/items/LibraryDeleter;

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Factory;->lambda$create$1(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;Landroid/content/Context;Lcom/squareup/ui/items/LibraryDeleter;Landroid/content/DialogInterface;I)V

    return-void
.end method
