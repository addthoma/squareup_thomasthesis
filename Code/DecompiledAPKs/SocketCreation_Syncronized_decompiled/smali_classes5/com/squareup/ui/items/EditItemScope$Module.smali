.class public abstract Lcom/squareup/ui/items/EditItemScope$Module;
.super Ljava/lang/Object;
.source "EditItemScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCatalogServiceEndpoint(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/catalog/CatalogService;Lcom/squareup/analytics/Analytics;)Lcom/squareup/catalog/CatalogServiceEndpoint;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 140
    new-instance v0, Lcom/squareup/catalog/CatalogServiceEndpoint;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/catalog/CatalogServiceEndpoint;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/catalog/CatalogService;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method static provideEditItemState(Lcom/squareup/ui/items/ImageUploader;Ljava/io/File;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/concurrent/Executor;)Lcom/squareup/ui/items/EditItemState;
    .locals 8
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 119
    new-instance v7, Lcom/squareup/ui/items/EditItemState;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/EditItemState;-><init>(Lcom/squareup/ui/items/ImageUploader;Ljava/io/File;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/concurrent/Executor;)V

    return-object v7
.end method

.method static provideEditItemStateObservable(Lcom/squareup/ui/items/EditItemScopeRunner;)Lrx/Single;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemStateLoaded()Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method static provideModiferSetsKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;>;"
        }
    .end annotation

    .line 127
    new-instance v0, Lcom/squareup/ui/items/EditItemScope$Module$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemScope$Module$1;-><init>()V

    .line 129
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScope$Module$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "ORDER_MODIFIER_SETS"

    .line 127
    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract provideAdjustInventoryHost(Lcom/squareup/ui/items/EditItemScopeRunner;)Lcom/squareup/ui/inventory/AdjustInventoryHost;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAssignOptionToItemOutputHandler(Lcom/squareup/ui/items/EditItemScopeRunner;)Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideImageUploader(Lcom/squareup/ui/items/RealImageUploader;)Lcom/squareup/ui/items/ImageUploader;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
