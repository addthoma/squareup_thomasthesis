.class public final Lcom/squareup/ui/items/ItemsAppletSection$Units;
.super Lcom/squareup/ui/items/ItemsAppletSection;
.source "ItemsAppletSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Units"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;

    .line 79
    invoke-virtual {v0}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;->getALL_UNITS_INSTANCE()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    move-result-object v2

    invoke-static {}, Lcom/squareup/ui/items/ItemsAppletSection;->access$000()Lcom/squareup/applet/SectionAccess;

    move-result-object v3

    sget v4, Lcom/squareup/itemsapplet/R$string;->items_applet_units_title:I

    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_UNITS:Lcom/squareup/analytics/RegisterTapName;

    const/4 v6, 0x0

    move-object v1, p0

    .line 77
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/items/ItemsAppletSection;-><init>(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/SectionAccess;ILcom/squareup/analytics/RegisterTapName;Lcom/squareup/ui/items/ItemsAppletSection$1;)V

    return-void
.end method
