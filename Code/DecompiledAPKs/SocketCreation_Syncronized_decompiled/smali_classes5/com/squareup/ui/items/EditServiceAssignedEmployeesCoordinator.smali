.class public final Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditServiceAssignedEmployeesCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditServiceAssignedEmployeesCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditServiceAssignedEmployeesCoordinator.kt\ncom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,92:1\n1642#2,2:93\n1360#2:95\n1429#2,3:96\n*E\n*S KotlinDebug\n*F\n+ 1 EditServiceAssignedEmployeesCoordinator.kt\ncom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator\n*L\n45#1,2:93\n62#1:95\n62#1,3:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010\u000e\u001a\u00020\nH\u0002J\u0018\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0008\u0010\u0014\u001a\u00020\nH\u0002J\u000c\u0010\u0015\u001a\u00020\n*\u00020\u000cH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;",
        "(Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "employeesGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "configureActionBar",
        "handleBack",
        "populateStaffView",
        "context",
        "Landroid/content/Context;",
        "staffItemState",
        "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
        "saveAssignedEmployees",
        "bindViews",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final runner:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;)V
    .locals 1

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->runner:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->handleBack()V

    return-void
.end method

.method public static final synthetic access$populateStaffView(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;Landroid/content/Context;Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->populateStaffView(Landroid/content/Context;Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;)V

    return-void
.end method

.method public static final synthetic access$saveAssignedEmployees(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->saveAssignedEmployees()V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 72
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 73
    sget v0, Lcom/squareup/edititem/R$id;->employees_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;)V
    .locals 4

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 81
    new-instance v2, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;Landroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 85
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v3, Lcom/squareup/edititem/R$string;->edit_service_select_employees:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 84
    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 89
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final handleBack()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->runner:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;->assignedEmployeesFinish()V

    return-void
.end method

.method private final populateStaffView(Landroid/content/Context;Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;)V
    .locals 10

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

    const-string v1, "employeesGroup"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->removeAllViews()V

    .line 45
    iget-object v0, p2, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;->staffInfos:Ljava/util/List;

    const-string v2, "staffItemState.staffInfos"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 93
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/appointmentsapi/StaffInfo;

    .line 46
    new-instance v9, Lcom/squareup/noho/NohoCheckableRow;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v9

    move-object v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 47
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v3

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoCheckableRow;->setId(I)V

    .line 48
    invoke-virtual {v2}, Lcom/squareup/appointmentsapi/StaffInfo;->getEmployeeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoCheckableRow;->setTag(Ljava/lang/Object;)V

    .line 49
    invoke-virtual {v2}, Lcom/squareup/appointmentsapi/StaffInfo;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v3, p2, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;->selectedStaffIds:Ljava/util/List;

    invoke-virtual {v2}, Lcom/squareup/appointmentsapi/StaffInfo;->getEmployeeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v9, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 51
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 52
    :cond_1
    check-cast v9, Landroid/view/View;

    new-instance v3, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(II)V

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    .line 51
    invoke-virtual {v2, v9, v3}, Lcom/squareup/noho/NohoCheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 55
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance p2, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$populateStaffView$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$populateStaffView$2;-><init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;)V

    check-cast p2, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method private final saveAssignedEmployees()V
    .locals 6

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

    const-string v1, "employeesGroup"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->getCheckedIds()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 95
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 96
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 97
    check-cast v3, Ljava/lang/Integer;

    .line 62
    iget-object v4, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->employeesGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v4, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const-string v5, "it"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/squareup/noho/NohoCheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "employeesGroup.findViewById<NohoCheckableRow>(it)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoCheckableRow;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_4
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 63
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->runner:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;

    invoke-interface {v1, v0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;->setEmployeeAssigned(Ljava/util/Set;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->bindViews(Landroid/view/View;)V

    .line 29
    new-instance v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$attach$1;-><init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->configureActionBar(Landroid/view/View;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->runner:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;->getStaffItemState()Lrx/Observable;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$attach$2;-><init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method
