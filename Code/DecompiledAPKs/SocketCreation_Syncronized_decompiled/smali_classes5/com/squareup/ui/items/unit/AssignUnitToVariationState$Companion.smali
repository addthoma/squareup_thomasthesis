.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;
.super Ljava/lang/Object;
.source "AssignUnitToVariationState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationState.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,118:1\n180#2:119\n158#2,3:121\n161#2:126\n158#2,3:127\n161#2:132\n165#2:133\n165#2:134\n56#3:120\n99#3:135\n1591#4,2:124\n1591#4,2:130\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationState.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationState$Companion\n*L\n80#1:119\n105#1,3:121\n105#1:126\n106#1,3:127\n106#1:132\n112#1:133\n113#1:134\n80#1:120\n114#1:135\n105#1,2:124\n106#1,2:130\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\tH\u0002J\u0014\u0010\n\u001a\u00020\u000b*\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008H\u0002\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "byteString",
        "Lokio/ByteString;",
        "readSelectUnit",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;",
        "Lokio/BufferedSource;",
        "writeSelectUnit",
        "",
        "Lokio/BufferedSink;",
        "selectUnit",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$writeSelectUnit(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->writeSelectUnit(Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    return-void
.end method

.method private final readSelectUnit(Lokio/BufferedSource;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;
    .locals 6

    .line 111
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    .line 112
    sget-object v5, Lcom/squareup/items/unit/SelectableUnit;->Companion:Lcom/squareup/items/unit/SelectableUnit$Companion;

    invoke-virtual {v5, p1}, Lcom/squareup/items/unit/SelectableUnit$Companion;->readSelectableUnit(Lokio/BufferedSource;)Lcom/squareup/items/unit/SelectableUnit;

    move-result-object v5

    .line 133
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    check-cast v2, Ljava/util/List;

    .line 134
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    if-ge v3, v1, :cond_1

    .line 113
    sget-object v5, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v5, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    .line 134
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    check-cast v4, Ljava/util/List;

    .line 135
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v1, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object p1

    .line 110
    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-direct {v1, v0, v2, v4, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v1
.end method

.method private final writeSelectUnit(Lokio/BufferedSink;Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V
    .locals 3

    .line 104
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 105
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectableUnits()Ljava/util/List;

    move-result-object v0

    .line 122
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 123
    check-cast v0, Ljava/lang/Iterable;

    .line 124
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 123
    check-cast v1, Lcom/squareup/items/unit/SelectableUnit;

    .line 105
    sget-object v2, Lcom/squareup/items/unit/SelectableUnit;->Companion:Lcom/squareup/items/unit/SelectableUnit$Companion;

    invoke-virtual {v2, p1, v1}, Lcom/squareup/items/unit/SelectableUnit$Companion;->writeSelectableUnit(Lokio/BufferedSink;Lcom/squareup/items/unit/SelectableUnit;)V

    goto :goto_0

    .line 106
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getCreatedUnits()Ljava/util/List;

    move-result-object v0

    .line 128
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 129
    check-cast v0, Ljava/lang/Iterable;

    .line 130
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 129
    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 106
    sget-object v2, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v2, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    goto :goto_1

    .line 107
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getDefaultStandardUnits()Ljava/util/List;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;

    return-void
.end method


# virtual methods
.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState;
    .locals 3

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 81
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 82
    const-class v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->readSelectUnit(Lokio/BufferedSource;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    goto :goto_0

    .line 84
    :cond_0
    const-class v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    .line 85
    sget-object v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->readSelectUnit(Lokio/BufferedSource;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object p1

    .line 84
    invoke-direct {v0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    goto :goto_0

    .line 88
    :cond_1
    const-class v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    .line 89
    sget-object v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->readSelectUnit(Lokio/BufferedSource;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v1

    .line 90
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 88
    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    goto :goto_0

    .line 93
    :cond_2
    const-class v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->readSelectUnit(Lokio/BufferedSource;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    .line 120
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 93
    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    :goto_0
    return-object p1

    .line 98
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
