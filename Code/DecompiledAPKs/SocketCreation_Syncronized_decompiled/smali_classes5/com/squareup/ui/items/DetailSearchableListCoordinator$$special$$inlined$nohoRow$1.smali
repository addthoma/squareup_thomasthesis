.class public final Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;",
        "Lcom/squareup/noho/NohoRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$2\n+ 2 DetailSearchableListCoordinator.kt\ncom/squareup/ui/items/DetailSearchableListCoordinator\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,173:1\n256#2,2:174\n260#2:183\n1103#3,7:176\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u00042\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "<anonymous parameter 0>",
        "",
        "item",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "invoke",
        "(ILjava/lang/Object;Lcom/squareup/noho/NohoRow;)V",
        "com/squareup/noho/dsl/RecyclerNohoKt$nohoRow$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p3, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$1;->invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoRow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;",
            "Lcom/squareup/noho/NohoRow;",
            ")V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;

    .line 174
    invoke-virtual {p2, p3}, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;->configureRow(Lcom/squareup/noho/NohoRow;)V

    .line 175
    check-cast p3, Landroid/view/View;

    .line 176
    new-instance p1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$1$lambda$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$1$lambda$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
