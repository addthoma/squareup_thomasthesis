.class public abstract Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
.super Lcom/squareup/ui/items/DetailSearchableListPresenter;
.source "AllItemsOrServicesDetailPresenter.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
        "Lcom/squareup/ui/items/AllItemsOrServicesDetailView;",
        ">;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;"
    }
.end annotation


# instance fields
.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 11

    move-object v10, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    .line 53
    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/DetailSearchableListPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    move-object v0, p2

    .line 55
    iput-object v0, v10, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->bus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v0, p8

    .line 56
    iput-object v0, v10, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v0, p9

    .line 57
    iput-object v0, v10, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object/from16 v0, p10

    .line 58
    iput-object v0, v10, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    move-object/from16 v0, p11

    .line 59
    iput-object v0, v10, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    return-void
.end method

.method private isScreenInForeground()Z
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getScreenInstance()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/TopScreenChecker;->isUnobscured(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$vbzQnMFNx1wRIEhQhYi-VrYjkHE(Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 130
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->forceRefresh()V

    :cond_0
    return-void
.end method


# virtual methods
.method public barcodeScanned(Ljava/lang/String;)V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->isScreenInForeground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$AllItemsOrServicesDetailPresenter$AuMtwMDwpXFNZmV9Eic9pUWGtCQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$AllItemsOrServicesDetailPresenter$AuMtwMDwpXFNZmV9Eic9pUWGtCQ;-><init>(Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$AllItemsOrServicesDetailPresenter$4xMGUJ9bRR33u3gO78E5YByKTJE;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$AllItemsOrServicesDetailPresenter$4xMGUJ9bRR33u3gO78E5YByKTJE;-><init>(Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    .line 111
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->isScreenInForeground()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "Ignoring barcode scanned. Feature enabled: %b, screen in foreground: %b"

    .line 110
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public dropView(Lcom/squareup/ui/items/AllItemsOrServicesDetailView;)V
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 76
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/ui/items/AllItemsOrServicesDetailView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->dropView(Lcom/squareup/ui/items/AllItemsOrServicesDetailView;)V

    return-void
.end method

.method abstract getAnalyticsTapName()Lcom/squareup/analytics/RegisterTapName;
.end method

.method getButtonCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method

.method abstract getNullHint(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method

.method abstract getNullTitle()Ljava/lang/String;
.end method

.method abstract getScreenInstance()Lcom/squareup/container/ContainerTreeKey;
.end method

.method abstract getSearchHint()Ljava/lang/String;
.end method

.method public synthetic lambda$barcodeScanned$0$AllItemsOrServicesDetailPresenter(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 1

    .line 102
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 103
    invoke-interface {p2, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 104
    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getItemTypes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$barcodeScanned$1$AllItemsOrServicesDetailPresenter(Ljava/lang/String;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 106
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->onResultOfVariationsMatchingBarcode(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$AllItemsOrServicesDetailPresenter$vbzQnMFNx1wRIEhQhYi-VrYjkHE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$AllItemsOrServicesDetailPresenter$vbzQnMFNx1wRIEhQhYi-VrYjkHE;-><init>(Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 68
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {p1, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    return-void
.end method

.method onRowClicked(I)V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v2

    .line 83
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    .line 82
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p0}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->getAnalyticsTapName()Lcom/squareup/analytics/RegisterTapName;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public rowsHaveThumbnails()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
