.class final Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCoordinator;->updateList(Lcom/squareup/ui/items/DetailSearchableListScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isSearching:Z

.field final synthetic $recyclerData:Lcom/squareup/cycler/DataSource;

.field final synthetic $screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Lcom/squareup/cycler/DataSource;ZLcom/squareup/ui/items/DetailSearchableListScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$recyclerData:Lcom/squareup/cycler/DataSource;

    iput-boolean p3, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$isSearching:Z

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/ui/items/DetailSearchableListRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$recyclerData:Lcom/squareup/cycler/DataSource;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 373
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$recyclerData:Lcom/squareup/cycler/DataSource;

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$isSearching:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 374
    :goto_0
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    invoke-static {v3}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$getRes$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/squareup/util/Res;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)V

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Update;->setExtraItem(Ljava/lang/Object;)V

    return-void
.end method
