.class public final Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;
.super Ljava/lang/Object;
.source "DetailSearchableListBehaviorFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListBehaviorFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListBehaviorFactory.kt\ncom/squareup/ui/items/DetailSearchableListBehaviorFactory\n*L\n1#1,86:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0089\u0001\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0007\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0007\u0012\u000e\u0008\u0001\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0007\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0007\u0012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0007\u00a2\u0006\u0002\u0010\u0017J\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bR\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;",
        "",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "resProvider",
        "Lcom/squareup/util/Res;",
        "discountCursorFactoryProvider",
        "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
        "discountBundleFactoryProvider",
        "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
        "timeZoneProvider",
        "Ljava/util/TimeZone;",
        "catalogLocalizerProvider",
        "Lcom/squareup/shared/i18n/Localizer;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V",
        "getBehaviorFor",
        "Lcom/squareup/ui/items/DetailSearchableListBehavior;",
        "dataType",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final catalogLocalizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;"
        }
    .end annotation
.end field

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final discountBundleFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final discountCursorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final timeZoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p8    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/catalog/CatalogLocalizer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/badbus/BadBus;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountCursorFactoryProvider"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountBundleFactoryProvider"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeZoneProvider"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogLocalizerProvider"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->localeProvider:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->resProvider:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->discountCursorFactoryProvider:Ljavax/inject/Provider;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->discountBundleFactoryProvider:Ljavax/inject/Provider;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->timeZoneProvider:Ljavax/inject/Provider;

    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->catalogLocalizerProvider:Ljavax/inject/Provider;

    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->analytics:Ljavax/inject/Provider;

    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->settings:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public final getBehaviorFor(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListBehavior;
    .locals 12

    const-string v0, "dataType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v1, 0x2

    const-string v2, "analytics.get()"

    if-eq p1, v1, :cond_2

    const/4 v3, 0x3

    if-eq p1, v3, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 75
    new-instance p1, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;

    .line 76
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;->getDetailSearchableListScreenTextBagForServices()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 79
    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->cogs:Lcom/squareup/cogs/Cogs;

    .line 80
    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->badBus:Lcom/squareup/badbus/BadBus;

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->analytics:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    move-object v3, p1

    .line 75
    invoke-direct/range {v3 .. v9}, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;-><init>(ZLjava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/analytics/Analytics;)V

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListBehavior;

    goto/16 :goto_0

    .line 83
    :cond_0
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "An operation is not implemented: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "CATALOG-8676: refactor old lists to use the workflow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 64
    :cond_1
    new-instance p1, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;

    .line 65
    sget-object v3, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

    invoke-virtual {v3}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;->getDetailSearchableListScreenTextBagForItems()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 68
    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->cogs:Lcom/squareup/cogs/Cogs;

    .line 69
    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->badBus:Lcom/squareup/badbus/BadBus;

    .line 70
    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->analytics:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/squareup/analytics/Analytics;

    .line 71
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->settings:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    const/4 v9, 0x0

    .line 72
    sget-object v10, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v10, v1, v9

    sget-object v9, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    aput-object v9, v1, v0

    .line 71
    invoke-virtual {v2, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v9

    const-string v0, "settings.get().supported\u2026NTS_SERVICE\n            )"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    move v1, v4

    move-object v2, v5

    move-object v4, v9

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    .line 64
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;-><init>(ZLjava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/analytics/Analytics;)V

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListBehavior;

    goto/16 :goto_0

    .line 52
    :cond_2
    new-instance p1, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;

    .line 53
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;->getDetailSearchableListScreenTextBagForDiscounts()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 56
    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->cogs:Lcom/squareup/cogs/Cogs;

    .line 57
    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->badBus:Lcom/squareup/badbus/BadBus;

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->discountCursorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v7, "discountCursorFactoryProvider.get()"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    check-cast v7, Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->discountBundleFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v8, "discountBundleFactoryProvider.get()"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v0

    check-cast v8, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->timeZoneProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v9, "timeZoneProvider.get()"

    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v9, v0

    check-cast v9, Ljava/util/TimeZone;

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->catalogLocalizerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v10, "catalogLocalizerProvider.get()"

    invoke-static {v0, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v10, v0

    check-cast v10, Lcom/squareup/shared/i18n/Localizer;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->analytics:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v11, v0

    check-cast v11, Lcom/squareup/analytics/Analytics;

    move-object v0, p1

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    .line 52
    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;)V

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListBehavior;

    goto :goto_0

    .line 43
    :cond_3
    new-instance p1, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;

    .line 44
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;->getDetailSearchableListScreenTextBagForUnits()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v1

    const/4 v2, 0x1

    const/16 v0, 0xfa

    .line 46
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 47
    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->cogs:Lcom/squareup/cogs/Cogs;

    .line 48
    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->badBus:Lcom/squareup/badbus/BadBus;

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v6, "localeProvider.get()"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v0

    check-cast v6, Ljava/util/Locale;

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v7, "resProvider.get()"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    move-object v0, p1

    .line 43
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;)V

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListBehavior;

    :goto_0
    return-object p1
.end method
