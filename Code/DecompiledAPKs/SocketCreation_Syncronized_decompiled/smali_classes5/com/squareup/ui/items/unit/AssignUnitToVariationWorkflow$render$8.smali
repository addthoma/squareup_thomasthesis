.class final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;
.super Lkotlin/jvm/internal/Lambda;
.source "AssignUnitToVariationWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->render(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "+",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        "event",
        "Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

.field final synthetic $state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;->$input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    sget-object v0, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event$Resume;->INSTANCE:Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event$Resume;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 283
    :cond_0
    sget-object v0, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event$Discard;->INSTANCE:Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event$Discard;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 284
    iget-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;->$input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getSelectedUnitId()Ljava/lang/String;

    move-result-object p1

    .line 285
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 286
    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;

    .line 287
    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getCreatedUnits()Ljava/util/List;

    move-result-object v2

    .line 286
    invoke-direct {v1, p1, v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 285
    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;->invoke(Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
