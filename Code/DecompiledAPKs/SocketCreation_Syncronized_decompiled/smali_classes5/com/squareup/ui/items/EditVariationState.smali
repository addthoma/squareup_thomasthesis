.class public final Lcom/squareup/ui/items/EditVariationState;
.super Ljava/lang/Object;
.source "EditVariationState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditVariationState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0018\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 02\u00020\u0001:\u00010BA\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rJ\u0008\u0010$\u001a\u00020%H\u0016J\u0006\u0010&\u001a\u00020\'J\u000e\u0010(\u001a\u00020\'2\u0006\u0010)\u001a\u00020\u0005J2\u0010*\u001a\u00020\'2\u0006\u0010+\u001a\u00020\u00052\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cJ\u0018\u0010,\u001a\u00020\'2\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020%H\u0016R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001dR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\"\u0004\u0008 \u0010!R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010\u001f\"\u0004\u0008#\u0010!\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditVariationState;",
        "Landroid/os/Parcelable;",
        "variationEditingState",
        "Lcom/squareup/ui/items/ItemVariationEditingState;",
        "variationInEditing",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        "variationInEditingOriginal",
        "inventoryAssignmentCountOriginal",
        "Ljava/math/BigDecimal;",
        "inventoryAssignmentUnitCostOriginal",
        "Lcom/squareup/protos/common/Money;",
        "measurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "(Lcom/squareup/ui/items/ItemVariationEditingState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V",
        "getInventoryAssignmentCountOriginal",
        "()Ljava/math/BigDecimal;",
        "setInventoryAssignmentCountOriginal",
        "(Ljava/math/BigDecimal;)V",
        "getInventoryAssignmentUnitCostOriginal",
        "()Lcom/squareup/protos/common/Money;",
        "setInventoryAssignmentUnitCostOriginal",
        "(Lcom/squareup/protos/common/Money;)V",
        "getMeasurementUnit",
        "()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "setMeasurementUnit",
        "(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V",
        "getVariationEditingState",
        "()Lcom/squareup/ui/items/ItemVariationEditingState;",
        "setVariationEditingState",
        "(Lcom/squareup/ui/items/ItemVariationEditingState;)V",
        "getVariationInEditing",
        "()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        "setVariationInEditing",
        "(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V",
        "getVariationInEditingOriginal",
        "setVariationInEditingOriginal",
        "describeContents",
        "",
        "finishEditingVariation",
        "",
        "startCreatingNewVariation",
        "newVariation",
        "startEditingExistingVariation",
        "existingVariation",
        "writeToParcel",
        "dest",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditVariationState;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/items/EditVariationState$Companion;


# instance fields
.field private inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

.field private inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

.field private measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

.field private variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

.field private variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

.field private variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/EditVariationState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditVariationState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/EditVariationState;->Companion:Lcom/squareup/ui/items/EditVariationState$Companion;

    .line 125
    new-instance v0, Lcom/squareup/ui/items/EditVariationState$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditVariationState$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/items/EditVariationState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/items/ItemVariationEditingState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    iput-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iput-object p3, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iput-object p4, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    iput-object p5, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    iput-object p6, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/ItemVariationEditingState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 48
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/items/EditVariationState;-><init>(Lcom/squareup/ui/items/ItemVariationEditingState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-void
.end method

.method public static synthetic startEditingExistingVariation$default(Lcom/squareup/ui/items/EditVariationState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 70
    move-object p2, v0

    check-cast p2, Ljava/math/BigDecimal;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 71
    move-object p3, v0

    check-cast p3, Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 72
    move-object p4, v0

    check-cast p4, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/items/EditVariationState;->startEditingExistingVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final finishEditingVariation()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 85
    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    const/4 v0, 0x0

    .line 86
    move-object v1, v0

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iput-object v1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 87
    iput-object v1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 88
    move-object v1, v0

    check-cast v1, Ljava/math/BigDecimal;

    iput-object v1, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    .line 89
    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/Money;

    iput-object v1, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    .line 90
    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public final getInventoryAssignmentCountOriginal()Ljava/math/BigDecimal;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final getInventoryAssignmentUnitCostOriginal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-object v0
.end method

.method public final getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    return-object v0
.end method

.method public final getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-object v0
.end method

.method public final getVariationInEditingOriginal()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-object v0
.end method

.method public final setInventoryAssignmentCountOriginal(Ljava/math/BigDecimal;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    return-void
.end method

.method public final setInventoryAssignmentUnitCostOriginal(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public final setMeasurementUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public final setVariationEditingState(Lcom/squareup/ui/items/ItemVariationEditingState;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    return-void
.end method

.method public final setVariationInEditing(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method

.method public final setVariationInEditingOriginal(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method

.method public final startCreatingNewVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V
    .locals 2

    const-string v0, "newVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 60
    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->CREATING_NEW_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    const/4 v0, 0x0

    .line 61
    move-object v1, v0

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iput-object v1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 63
    move-object p1, v0

    check-cast p1, Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    .line 64
    move-object p1, v0

    check-cast p1, Lcom/squareup/protos/common/Money;

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    .line 65
    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public final startEditingExistingVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 2

    const-string v0, "existingVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 75
    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->EDITING_EXISTING_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 77
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 78
    iput-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    .line 79
    iput-object p3, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    .line 80
    iput-object p4, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const-string p2, "dest"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    invoke-virtual {p2}, Lcom/squareup/ui/items/ItemVariationEditingState;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->variationEditingState:Lcom/squareup/ui/items/ItemVariationEditingState;

    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq p2, v0, :cond_6

    .line 103
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditing:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->toByteArray()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 104
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 105
    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    if-eqz p2, :cond_3

    .line 107
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->variationInEditingOriginal:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->toByteArray()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 109
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentCountOriginal:Ljava/math/BigDecimal;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 110
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->inventoryAssignmentUnitCostOriginal:Lcom/squareup/protos/common/Money;

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 111
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    if-eqz p2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    .line 112
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    if-eqz v0, :cond_6

    .line 114
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationState;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    if-nez p2, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->toByteArray()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    :cond_6
    return-void
.end method
