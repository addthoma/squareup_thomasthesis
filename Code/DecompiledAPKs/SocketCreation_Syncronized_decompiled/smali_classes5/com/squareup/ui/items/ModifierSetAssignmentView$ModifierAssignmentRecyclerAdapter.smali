.class public Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ModifierSetAssignmentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifierSetAssignmentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ModifierAssignmentRecyclerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field final synthetic this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/ModifierSetAssignmentView;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getItemCount()I
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    :goto_0
    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;I)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    .line 150
    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->bindItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 104
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;
    .locals 2

    .line 142
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 143
    sget v0, Lcom/squareup/itemsapplet/R$layout;->category_assignment_item_row:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 144
    new-instance p2, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;-><init>(Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;Landroid/view/View;)V

    return-object p2
.end method
