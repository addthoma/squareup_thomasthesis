.class Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditModifierSetRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindModifierOption(Lcom/squareup/api/items/ItemModifierOption$Builder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

.field final synthetic val$option:Lcom/squareup/api/items/ItemModifierOption$Builder;

.field final synthetic val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Lcom/squareup/api/items/ItemModifierOption$Builder;Lcom/squareup/ui/items/ModifierOptionRow;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->val$option:Lcom/squareup/api/items/ItemModifierOption$Builder;

    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 217
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->getPosition()I

    move-result p1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$000(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->val$option:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->deleteClicked(Lcom/squareup/api/items/ItemModifierOption$Builder;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->removeHolderTextChangeListeners()V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->clearFocus()V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->notifyItemRemoved(I)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v0

    sub-int/2addr p1, v0

    if-nez p1, :cond_1

    .line 227
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->notifyItemChanged(I)V

    :cond_1
    return-void
.end method
