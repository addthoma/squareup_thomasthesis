.class public Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;
.super Lcom/squareup/applet/AppletSectionsListPresenter;
.source "ItemsAppletMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemsAppletSectionsListPresenter"
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/applet/AppletSectionsListPresenter;-><init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;)V

    .line 88
    iput-object p4, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 89
    iput-object p5, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public onSectionClicked(I)V
    .locals 2

    .line 99
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/ItemsAppletSection;

    iget-object v1, v1, Lcom/squareup/ui/items/ItemsAppletSection;->tapName:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    const-string v1, "Items Applet Section Tapped"

    invoke-interface {v0, v1, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public rowDisplayed(I)V
    .locals 2

    .line 93
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->rowDisplayed(I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "Items Applet Loaded"

    invoke-interface {v0, v1, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
