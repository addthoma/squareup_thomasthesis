.class public Lcom/squareup/ui/items/CategoryDetailView;
.super Lcom/squareup/ui/items/DetailSearchableListView;
.source "CategoryDetailView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "Lcom/squareup/ui/items/CategoryDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method getPresenter()Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailView;->presenter:Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;

    return-object v0
.end method

.method bridge synthetic getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailView;->getPresenter()Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method protected inject()V
    .locals 2

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/items/CategoryDetailScreen$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/CategoryDetailScreen$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Component;->inject(Lcom/squareup/ui/items/CategoryDetailView;)V

    return-void
.end method

.method updateCategoryName(Ljava/lang/String;)V
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_search_category:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "category_name"

    .line 25
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 27
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_no_items_in_category:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 29
    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 30
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const/4 v0, 0x0

    .line 28
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/items/CategoryDetailView;->setNullState(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method
