.class public final Lcom/squareup/ui/items/DetailSearchableListViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "DetailSearchableListViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListViewFactory.kt\ncom/squareup/ui/items/DetailSearchableListViewFactory\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,104:1\n37#2,2:105\n*E\n*S KotlinDebug\n*F\n+ 1 DetailSearchableListViewFactory.kt\ncom/squareup/ui/items/DetailSearchableListViewFactory\n*L\n68#1,2:105\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001 B\u0085\u0001\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0008\u0008\u0001\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\n\u0010\u001d\u001a\u0006\u0012\u0002\u0008\u00030\u001e\u00a2\u0006\u0002\u0010\u001f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "device",
        "Lcom/squareup/util/Device;",
        "res",
        "Lcom/squareup/util/Res;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "barcodeScannerTracker",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "editUnitViewBindings",
        "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
        "section",
        "Ljava/lang/Class;",
        "(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;Ljava/lang/Class;)V",
        "Factory",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;Ljava/lang/Class;)V
    .locals 18
    .param p6    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .param p12    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    .line 35
    new-instance v0, Lkotlin/jvm/internal/SpreadBuilder;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lkotlin/jvm/internal/SpreadBuilder;-><init>(I)V

    .line 51
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 52
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 53
    sget v3, Lcom/squareup/itemsapplet/R$layout;->detail_searchable_list:I

    .line 54
    new-instance v17, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;

    move-object/from16 v4, v17

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p2

    move-object/from16 v14, p10

    move-object/from16 v15, p11

    move-object/from16 v16, p12

    invoke-direct/range {v4 .. v16}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$1;-><init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Device;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V

    move-object/from16 v4, v17

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 63
    new-instance v5, Lcom/squareup/workflow/ScreenHint;

    .line 65
    invoke-interface/range {p2 .. p2}, Lcom/squareup/util/Device;->isMasterDetail()Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v6, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object v6, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    :goto_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1d7

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 p1, v5

    move-object/from16 p2, v12

    move-object/from16 p3, v13

    move/from16 p4, v14

    move-object/from16 p5, p14

    move-object/from16 p6, v15

    move-object/from16 p7, v6

    move/from16 p8, v7

    move/from16 p9, v8

    move-object/from16 p10, v9

    move/from16 p11, v10

    move-object/from16 p12, v11

    .line 63
    invoke-direct/range {p1 .. p12}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    move/from16 p3, v3

    move-object/from16 p4, v5

    move-object/from16 p5, v8

    move-object/from16 p6, v4

    move/from16 p7, v6

    move-object/from16 p8, v7

    .line 51
    invoke-static/range {p1 .. p8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    .line 68
    invoke-virtual/range {p13 .. p13}, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->getBindings()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 106
    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lkotlin/jvm/internal/SpreadBuilder;->addSpread(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lkotlin/jvm/internal/SpreadBuilder;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    invoke-virtual {v0, v1}, Lkotlin/jvm/internal/SpreadBuilder;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-object/from16 v1, p0

    .line 50
    invoke-direct {v1, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void

    :cond_1
    move-object/from16 v1, p0

    .line 106
    new-instance v0, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;Ljava/lang/Class;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct/range {p0 .. p14}, Lcom/squareup/ui/items/DetailSearchableListViewFactory;-><init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;Ljava/lang/Class;)V

    return-void
.end method
