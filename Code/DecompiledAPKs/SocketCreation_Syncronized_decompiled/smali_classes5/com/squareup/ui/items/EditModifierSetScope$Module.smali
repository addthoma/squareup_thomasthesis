.class public Lcom/squareup/ui/items/EditModifierSetScope$Module;
.super Ljava/lang/Object;
.source "EditModifierSetScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditModifierSetScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideEditModifierSetStateObservable(Lcom/squareup/ui/items/EditModifierSetScopeRunner;)Lrx/Observable;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->getEditModifierSetStateObservable()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static provideModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-direct {v0}, Lcom/squareup/ui/items/ModifierSetEditState;-><init>()V

    return-object v0
.end method
