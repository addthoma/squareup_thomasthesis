.class public final enum Lcom/squareup/ui/items/DetailSearchableListDataType;
.super Ljava/lang/Enum;
.source "DetailSearchableListWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "",
        "(Ljava/lang/String;I)V",
        "Items",
        "Services",
        "Categories",
        "Discounts",
        "Modifiers",
        "Units",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/DetailSearchableListDataType;

.field public static final enum Categories:Lcom/squareup/ui/items/DetailSearchableListDataType;

.field public static final enum Discounts:Lcom/squareup/ui/items/DetailSearchableListDataType;

.field public static final enum Items:Lcom/squareup/ui/items/DetailSearchableListDataType;

.field public static final enum Modifiers:Lcom/squareup/ui/items/DetailSearchableListDataType;

.field public static final enum Services:Lcom/squareup/ui/items/DetailSearchableListDataType;

.field public static final enum Units:Lcom/squareup/ui/items/DetailSearchableListDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/items/DetailSearchableListDataType;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDataType;

    const/4 v2, 0x0

    const-string v3, "Items"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/DetailSearchableListDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Items:Lcom/squareup/ui/items/DetailSearchableListDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDataType;

    const/4 v2, 0x1

    const-string v3, "Services"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/DetailSearchableListDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Services:Lcom/squareup/ui/items/DetailSearchableListDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDataType;

    const/4 v2, 0x2

    const-string v3, "Categories"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/DetailSearchableListDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Categories:Lcom/squareup/ui/items/DetailSearchableListDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDataType;

    const/4 v2, 0x3

    const-string v3, "Discounts"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/DetailSearchableListDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Discounts:Lcom/squareup/ui/items/DetailSearchableListDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDataType;

    const/4 v2, 0x4

    const-string v3, "Modifiers"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/DetailSearchableListDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Modifiers:Lcom/squareup/ui/items/DetailSearchableListDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDataType;

    const/4 v2, 0x5

    const-string v3, "Units"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/DetailSearchableListDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Units:Lcom/squareup/ui/items/DetailSearchableListDataType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListDataType;->$VALUES:[Lcom/squareup/ui/items/DetailSearchableListDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/DetailSearchableListDataType;
    .locals 1

    const-class v0, Lcom/squareup/ui/items/DetailSearchableListDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/DetailSearchableListDataType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/DetailSearchableListDataType;
    .locals 1

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListDataType;->$VALUES:[Lcom/squareup/ui/items/DetailSearchableListDataType;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/DetailSearchableListDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/DetailSearchableListDataType;

    return-object v0
.end method
