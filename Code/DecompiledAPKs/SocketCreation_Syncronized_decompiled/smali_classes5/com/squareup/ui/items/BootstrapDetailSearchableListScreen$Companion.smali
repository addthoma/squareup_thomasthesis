.class public final Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;
.super Ljava/lang/Object;
.source "BootstrapDetailSearchableListScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006R\u0011\u0010\t\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;",
        "",
        "()V",
        "ALL_DISCOUNTS_INSTANCE",
        "Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;",
        "getALL_DISCOUNTS_INSTANCE",
        "()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;",
        "ALL_ITEMS_INSTANCE",
        "getALL_ITEMS_INSTANCE",
        "ALL_SERVICES_INSTANCE",
        "getALL_SERVICES_INSTANCE",
        "ALL_UNITS_INSTANCE",
        "getALL_UNITS_INSTANCE",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getALL_DISCOUNTS_INSTANCE()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 41
    invoke-static {}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->access$getALL_DISCOUNTS_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final getALL_ITEMS_INSTANCE()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 42
    invoke-static {}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->access$getALL_ITEMS_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final getALL_SERVICES_INSTANCE()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 43
    invoke-static {}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->access$getALL_SERVICES_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final getALL_UNITS_INSTANCE()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;
    .locals 1

    .line 40
    invoke-static {}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->access$getALL_UNITS_INSTANCE$cp()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method
