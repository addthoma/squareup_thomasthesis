.class public Lcom/squareup/ui/items/ItemsAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ItemsAppletScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/ItemsAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ItemsAppletScope$Component;,
        Lcom/squareup/ui/items/ItemsAppletScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ItemsAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field static final INSTANCE:Lcom/squareup/ui/items/ItemsAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/items/ItemsAppletScope;

    invoke-direct {v0}, Lcom/squareup/ui/items/ItemsAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/ItemsAppletScope;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletScope;

    .line 55
    sget-object v0, Lcom/squareup/ui/items/ItemsAppletScope;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ItemsAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 22
    const-class v0, Lcom/squareup/ui/items/ItemsAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ItemsAppletScope$Component;

    .line 23
    invoke-interface {v0}, Lcom/squareup/ui/items/ItemsAppletScope$Component;->scopeRunner()Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
