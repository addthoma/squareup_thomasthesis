.class public Lcom/squareup/ui/items/EditItemMainView;
.super Landroid/widget/LinearLayout;
.source "EditItemMainView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/items/BaseEditObjectView;


# instance fields
.field private adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

.field private container:Landroid/view/View;

.field durationFormatter:Lcom/squareup/text/DurationFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private errorsBarView:Lcom/squareup/ui/ErrorsBarView;

.field moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private overlay:Landroid/widget/ImageView;

.field presenter:Lcom/squareup/ui/items/EditItemMainPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private requestLayoutDisposable:Lio/reactivex/disposables/Disposable;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private spinnerContainer:Landroid/view/View;

.field private final transitionTime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->requestLayoutDisposable:Lio/reactivex/disposables/Disposable;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->inject(Lcom/squareup/ui/items/EditItemMainView;)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/items/EditItemMainView;->transitionTime:I

    return-void
.end method

.method static synthetic lambda$onFinishInflate$1()Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x1

    .line 86
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method initializeView(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 4

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->updateScreenData(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Lcom/squareup/ui/items/RecyclerViewDragController;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainView;->overlay:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainView;->adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    sget v3, Lcom/squareup/edititem/R$id;->draggable_item_variation_row_drag_handle:I

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/ui/items/RecyclerViewDragController;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/ImageView;Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;I)V

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;)V

    return-void
.end method

.method public synthetic lambda$null$2$EditItemMainView(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 94
    sget-object p1, Lcom/squareup/tutorialv2/RequestLayoutTimeout;->KEYBOARD_VISIBILITY_CHANGE:Lcom/squareup/tutorialv2/RequestLayoutTimeout;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainView;->requestLayoutsForTutorial(Lcom/squareup/tutorialv2/RequestLayoutTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$3$EditItemMainView()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 93
    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2Views;->onKeyboardVisibilityChanged(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$rUBDoENScztG3anixVEyTUaHQqE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$rUBDoENScztG3anixVEyTUaHQqE;-><init>(Lcom/squareup/ui/items/EditItemMainView;)V

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$EditItemMainView()Lkotlin/Unit;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->editItemShown()V

    .line 85
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 91
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 92
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$3WvSLQMk7qjOZfVdlwabBRsDL64;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$3WvSLQMk7qjOZfVdlwabBRsDL64;-><init>(Lcom/squareup/ui/items/EditItemMainView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->spinnerContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->showConfirmDiscardDialogOrFinish()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->dropView(Lcom/squareup/ui/items/EditItemMainView;)V

    .line 101
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->requestLayoutDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 66
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 68
    sget v0, Lcom/squareup/marin/R$id;->primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/squareup/edititem/R$id;->edit_item_save_button:I

    .line 69
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 71
    sget v0, Lcom/squareup/edititem/R$id;->container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->container:Landroid/view/View;

    .line 72
    sget v0, Lcom/squareup/edititem/R$id;->save_spinner_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->spinnerContainer:Landroid/view/View;

    .line 73
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_errors_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->errorsBarView:Lcom/squareup/ui/ErrorsBarView;

    .line 75
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 76
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 77
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setAutoMeasureEnabled(Z)V

    .line 78
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 80
    new-instance v0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainView;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainView;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemMainView;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;-><init>(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$1obYnXt08rhJ4-i9Ao2DkidYpw4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$1obYnXt08rhJ4-i9Ao2DkidYpw4;-><init>(Lcom/squareup/ui/items/EditItemMainView;)V

    sget-object v2, Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$Ue0Ok23dN9o3fdjzblhoU4yyFk0;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemMainView$Ue0Ok23dN9o3fdjzblhoU4yyFk0;

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2}, Lcom/squareup/tutorialv2/TutorialUtilities;->setupTutorialScrollListener(Landroidx/recyclerview/widget/RecyclerView;ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 87
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_variation_overlay:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditItemMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->overlay:Landroid/widget/ImageView;

    return-void
.end method

.method refreshView(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->updateScreenData(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->adapter:Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public requestInitialFocus()V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->container:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public requestLayoutsForTutorial(Lcom/squareup/tutorialv2/RequestLayoutTimeout;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView;->requestLayoutDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 110
    invoke-static {p0, p1}, Lcom/squareup/tutorialv2/TutorialUtilities;->requestLayouts(Landroid/view/View;Lcom/squareup/tutorialv2/RequestLayoutTimeout;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->requestLayoutDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public showMultiUnitContent()V
    .locals 0

    return-void
.end method

.method showSaveSpinnerView(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->container:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->spinnerContainer:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/items/EditItemMainView;->transitionTime:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 130
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->spinnerContainer:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/items/EditItemMainView;->transitionTime:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->container:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method
