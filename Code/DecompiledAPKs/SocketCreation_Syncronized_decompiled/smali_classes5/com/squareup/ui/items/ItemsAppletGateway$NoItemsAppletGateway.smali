.class public Lcom/squareup/ui/items/ItemsAppletGateway$NoItemsAppletGateway;
.super Ljava/lang/Object;
.source "ItemsAppletGateway.java"

# interfaces
.implements Lcom/squareup/ui/items/ItemsAppletGateway;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletGateway;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoItemsAppletGateway"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private noItemsAppletAccess()V
    .locals 2

    .line 30
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot launch the Items Applet from device without an Items Applet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public activateItemsApplet()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/items/ItemsAppletGateway$NoItemsAppletGateway;->noItemsAppletAccess()V

    return-void
.end method

.method public goBackToItemsAppletWithHistoryPreserved()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/items/ItemsAppletGateway$NoItemsAppletGateway;->noItemsAppletAccess()V

    return-void
.end method
