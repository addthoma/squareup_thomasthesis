.class public final Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;
.super Ljava/lang/Object;
.source "EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/items/EditItemMainScreen$Module;

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final stringsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemEditingStringIds;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditItemMainScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemMainScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemEditingStringIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->module:Lcom/squareup/ui/items/EditItemMainScreen$Module;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->stringsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->presenterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/items/EditItemMainScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemMainScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemEditingStringIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;-><init>(Lcom/squareup/ui/items/EditItemMainScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideAppliedLocationsBannerPresenter(Lcom/squareup/ui/items/EditItemMainScreen$Module;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;Ljava/lang/Object;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 6

    .line 65
    move-object v4, p4

    check-cast v4, Lcom/squareup/ui/items/EditItemMainPresenter;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemMainScreen$Module;->provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->module:Lcom/squareup/ui/items/EditItemMainScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->stringsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/items/ItemEditingStringIds;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->provideAppliedLocationsBannerPresenter(Lcom/squareup/ui/items/EditItemMainScreen$Module;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;Ljava/lang/Object;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->get()Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object v0

    return-object v0
.end method
