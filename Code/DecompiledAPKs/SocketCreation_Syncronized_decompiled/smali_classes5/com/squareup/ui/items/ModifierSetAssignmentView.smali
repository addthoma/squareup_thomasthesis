.class public Lcom/squareup/ui/items/ModifierSetAssignmentView;
.super Lcom/squareup/widgets/PairLayout;
.source "ModifierSetAssignmentView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

.field itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private nullStateContainer:Landroid/view/View;

.field private nullStateSubtitle:Lcom/squareup/widgets/MessageView;

.field private nullStateTitle:Landroid/widget/TextView;

.field presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class p2, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Component;->inject(Lcom/squareup/ui/items/ModifierSetAssignmentView;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 86
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public onBackPressed()Z
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 82
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 52
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 54
    sget v0, Lcom/squareup/itemsapplet/R$id;->recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 56
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 59
    new-instance v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;-><init>(Lcom/squareup/ui/items/ModifierSetAssignmentView;)V

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->adapter:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->adapter:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 62
    sget v0, Lcom/squareup/itemsapplet/R$id;->null_state_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateContainer:Landroid/view/View;

    .line 63
    sget v0, Lcom/squareup/itemsapplet/R$id;->null_state_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateTitle:Landroid/widget/TextView;

    .line 64
    sget v0, Lcom/squareup/itemsapplet/R$id;->null_state_subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateSubtitle:Lcom/squareup/widgets/MessageView;

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateTitle:Landroid/widget/TextView;

    sget v1, Lcom/squareup/registerlib/R$string;->modifier_set_null_title:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 67
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/registerlib/R$string;->modifier_set_null_subtitle:I

    const-string v2, "learn_more"

    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/registerlib/R$string;->items_hint_url:I

    .line 69
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 70
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateSubtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public updateCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->adapter:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {p1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->adapter:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->getItemCount()I

    move-result p1

    if-nez p1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateContainer:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 96
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->nullStateContainer:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method
