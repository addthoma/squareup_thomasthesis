.class Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DetailSearchableListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DetailSearchableListAdapter"
.end annotation


# static fields
.field private static final BUTTON_TYPE:I = 0x0

.field private static final ITEM_ROW_TYPE:I = 0x1


# instance fields
.field backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private staticTopRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/DetailSearchableListView$TopRow;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 211
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->setCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method


# virtual methods
.method protected buildAndBindButtonRow(Landroid/view/View;)Landroid/view/View;
    .locals 6

    if-nez p1, :cond_3

    .line 275
    new-instance p1, Lcom/squareup/ui/ReorientingLinearLayout;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/ui/ReorientingLinearLayout;-><init>(Landroid/content/Context;)V

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->rowsHaveThumbnails()Z

    move-result v0

    .line 278
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    if-eqz v0, :cond_0

    iget v1, v1, Lcom/squareup/ui/items/DetailSearchableListView;->gutter:I

    goto :goto_0

    :cond_0
    iget v1, v1, Lcom/squareup/ui/items/DetailSearchableListView;->gutterHalf:I

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget v0, v0, Lcom/squareup/ui/items/DetailSearchableListView;->gutterHalf:I

    .line 284
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget v3, v3, Lcom/squareup/ui/items/DetailSearchableListView;->gutterHalf:I

    invoke-virtual {p1, v0, v3, v0, v1}, Lcom/squareup/ui/ReorientingLinearLayout;->setPadding(IIII)V

    const/4 v0, 0x2

    .line 286
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ReorientingLinearLayout;->setShowDividers(I)V

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_divider_square_clear_medium:I

    .line 288
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 287
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ReorientingLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    .line 290
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getButtonCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 292
    new-instance v1, Lcom/squareup/marketfont/MarketButton;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v3}, Lcom/squareup/ui/items/DetailSearchableListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    .line 293
    sget v3, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_view_add_button:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketButton;->setTag(Ljava/lang/Object;)V

    const/16 v3, 0x11

    .line 294
    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    .line 295
    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v3}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getButtonText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 296
    new-instance v3, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;I)V

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$DetailSearchableListAdapter$T1_LIAkABqWDVceb88azK3o1JiA;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$DetailSearchableListAdapter$T1_LIAkABqWDVceb88azK3o1JiA;-><init>(Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;I)V

    invoke-static {v1, v3}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 308
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p1, v1, v3}, Lcom/squareup/ui/ReorientingLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object v0, v0, Lcom/squareup/ui/items/DetailSearchableListView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-nez v0, :cond_3

    .line 312
    invoke-virtual {p1}, Lcom/squareup/ui/ReorientingLinearLayout;->reorientVertical()V

    :cond_3
    return-object p1
.end method

.method protected buildAndBindItemRow(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 0

    if-nez p1, :cond_0

    .line 264
    sget p1, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    .line 265
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    :cond_0
    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 268
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/ui/items/DetailSearchableListView;->bindItemRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    .line 269
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getPosition()I

    move-result p2

    const/4 p3, 0x1

    if-nez p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2, p3}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setHorizontalBorders(ZZ)V

    return-object p1
.end method

.method public getCount()I
    .locals 2

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getStaticTopRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 2

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getStaticTopRowsCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    sub-int/2addr p1, v0

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 202
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->hasSearchFilter()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->staticTopRows:Ljava/util/List;

    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListView$TopRow;->BUTTON:Lcom/squareup/ui/items/DetailSearchableListView$TopRow;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    return v1
.end method

.method getStaticTopRowsCount()I
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->staticTopRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 251
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 253
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->buildAndBindButtonRow(Landroid/view/View;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-boolean v0, v0, Lcom/squareup/ui/items/DetailSearchableListView;->useTemporaryOrdinal:Z

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object v1, v1, Lcom/squareup/ui/items/DetailSearchableListView;->temporaryOrdinalMap:Landroid/util/SparseIntArray;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getStaticTopRowsCount()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    goto :goto_0

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getStaticTopRowsCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 260
    :goto_0
    invoke-virtual {p0, p2, p3, p1}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->buildAndBindItemRow(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 215
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getStaticTopRowsCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$buildAndBindButtonRow$0$DetailSearchableListView$DetailSearchableListAdapter(ILandroid/view/View;II)V
    .locals 0

    .line 305
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object p2, p2, Lcom/squareup/ui/items/DetailSearchableListView;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p3, "Items Section List Add Button Shown"

    invoke-interface {p2, p3, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 2

    .line 321
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListView;->access$100(Lcom/squareup/ui/items/DetailSearchableListView;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListView;->access$200(Lcom/squareup/ui/items/DetailSearchableListView;)Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 325
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->hasSearchFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->staticTopRows:Ljava/util/List;

    .line 328
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListView;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Items Section List Search Started"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->staticTopRows:Ljava/util/List;

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->staticTopRows:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListView$TopRow;->BUTTON:Lcom/squareup/ui/items/DetailSearchableListView$TopRow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result p1

    if-nez p1, :cond_1

    .line 334
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListView;->access$100(Lcom/squareup/ui/items/DetailSearchableListView;)Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 335
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListView;->access$200(Lcom/squareup/ui/items/DetailSearchableListView;)Lcom/squareup/widgets/MessageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 337
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListView;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Items Section List Search Ended"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 340
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->notifyDataSetChanged()V

    return-void
.end method
