.class public Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "EditItemPriceChangedDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemPriceChangedDialogScreen$89uYgn22s11SdxI8HJkpEkiIrKg;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemPriceChangedDialogScreen$89uYgn22s11SdxI8HJkpEkiIrKg;

    .line 55
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;->message:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;->message:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;
    .locals 2

    .line 56
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScope;

    .line 57
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 58
    new-instance v1, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 51
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
