.class public final Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;
.super Ljava/lang/Object;
.source "EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/items/EditDiscountScreen$Module;

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditDiscountScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditDiscountScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->module:Lcom/squareup/ui/items/EditDiscountScreen$Module;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/items/EditDiscountScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditDiscountScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAppliedLocationsBannerPresenter(Lcom/squareup/ui/items/EditDiscountScreen$Module;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 0

    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditDiscountScreen$Module;->provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->module:Lcom/squareup/ui/items/EditDiscountScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->provideAppliedLocationsBannerPresenter(Lcom/squareup/ui/items/EditDiscountScreen$Module;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen_Module_ProvideAppliedLocationsBannerPresenterFactory;->get()Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    move-result-object v0

    return-object v0
.end method
