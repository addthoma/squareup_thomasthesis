.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreenModel$rdYFnRvczjutQosTy3HmLtuvDpI;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final synthetic f$0:Ljava/util/TimeZone;


# direct methods
.method public synthetic constructor <init>(Ljava/util/TimeZone;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreenModel$rdYFnRvczjutQosTy3HmLtuvDpI;->f$0:Ljava/util/TimeZone;

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreenModel$rdYFnRvczjutQosTy3HmLtuvDpI;->f$0:Ljava/util/TimeZone;

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/items/EditDiscountScreenModel;->lambda$getScheduleRuleRows$0(Ljava/util/TimeZone;Lcom/squareup/shared/catalog/models/CatalogTimePeriod;Lcom/squareup/shared/catalog/models/CatalogTimePeriod;)I

    move-result p1

    return p1
.end method
