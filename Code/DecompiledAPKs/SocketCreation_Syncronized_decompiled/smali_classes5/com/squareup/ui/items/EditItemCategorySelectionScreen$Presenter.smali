.class Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EditItemCategorySelectionScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemCategorySelectionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/items/EditItemCategorySelectionView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SELECTED_CATEGORY_ID_KEY:Ljava/lang/String; = "SELECTED_CATEGORY_INDEX"

.field private static final SELECTED_NONE_ID:Ljava/lang/String; = ""


# instance fields
.field private categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private restoredSelectedCategoryId:Ljava/lang/String;

.field private screen:Lcom/squareup/ui/items/EditItemCategorySelectionScreen;

.field private selectedCategoryIndex:I

.field private final state:Lcom/squareup/ui/items/EditItemState;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/ui/items/EditItemState;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/ui/items/EditItemState;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->flow:Lflow/Flow;

    .line 71
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 0

    .line 77
    invoke-interface {p0}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllCategories()Lcom/squareup/shared/catalog/CatalogObjectCursor;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->toList()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private showCategoryList()V
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->restoredSelectedCategoryId:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    .line 95
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 97
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 98
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, -0x1

    .line 105
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemCategorySelectionView;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->setCategories(Ljava/util/List;I)V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemCategorySelectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->getSelectedCategoryIndex()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->setItemCategory(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lcom/squareup/ui/items/EditItemState$ItemData;->setItemCategory(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method getSelectedCategoryIndex()I
    .locals 1

    .line 171
    iget v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->selectedCategoryIndex:I

    return v0
.end method

.method public synthetic lambda$onEnterScope$1$EditItemCategorySelectionScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 78
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->showCategoryList()V

    return-void
.end method

.method public newCategoryButtonShown()V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Edit Category Screen Shown"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method onCategoryIndexClicked(I)V
    .locals 0

    .line 166
    iput p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->selectedCategoryIndex:I

    .line 167
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemCategorySelectionView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->notifyAdapter()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 75
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->screen:Lcom/squareup/ui/items/EditItemCategorySelectionScreen;

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionScreen$Presenter$_8tfk3cLbnmFiXvZc5d5MGzZSII;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionScreen$Presenter$_8tfk3cLbnmFiXvZc5d5MGzZSII;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionScreen$Presenter$ydPSvsCU1EpktckQO52MAtwQKv4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionScreen$Presenter$ydPSvsCU1EpktckQO52MAtwQKv4;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;)V

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 109
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "SELECTED_CATEGORY_INDEX"

    .line 111
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->restoredSelectedCategoryId:Ljava/lang/String;

    .line 115
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    if-eqz p1, :cond_1

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->showCategoryList()V

    .line 119
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemCategorySelectionView;

    .line 120
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->edit_item_select_category_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    .line 122
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$zv2Ebz-Wa_5GP98l95I22Get7Ds;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$zv2Ebz-Wa_5GP98l95I22Get7Ds;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    return-void
.end method

.method onNewCategoryClicked()V
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/NewCategoryNameScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->screen:Lcom/squareup/ui/items/EditItemCategorySelectionScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/NewCategoryNameScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemCategorySelectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->getSelectedCategoryIndex()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->categories:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 132
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    const-string v1, "SELECTED_CATEGORY_INDEX"

    .line 136
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public screenDismissed()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Edit Category Screen Dismissed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method setSelectedCategoryIndex(I)V
    .locals 0

    .line 175
    iput p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->selectedCategoryIndex:I

    return-void
.end method
