.class synthetic Lcom/squareup/ui/items/EditItemScopeRunner$2;
.super Ljava/lang/Object;
.source "EditItemScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$items$EditItemScopeRunner$VariationSavePath:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 665
    invoke-static {}, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->values()[Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$2;->$SwitchMap$com$squareup$ui$items$EditItemScopeRunner$VariationSavePath:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$2;->$SwitchMap$com$squareup$ui$items$EditItemScopeRunner$VariationSavePath:[I

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_VIA_ITEM_V3_GLOBALLY:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$2;->$SwitchMap$com$squareup$ui$items$EditItemScopeRunner$VariationSavePath:[I

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_VIA_ITEM_V3_WITH_LOCATION_OVERRIDE:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$2;->$SwitchMap$com$squareup$ui$items$EditItemScopeRunner$VariationSavePath:[I

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_WITH_COGS:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
