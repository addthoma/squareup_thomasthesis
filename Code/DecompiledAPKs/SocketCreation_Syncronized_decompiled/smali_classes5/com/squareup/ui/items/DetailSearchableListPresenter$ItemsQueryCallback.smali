.class public Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;
.super Ljava/lang/Object;
.source "DetailSearchableListPresenter.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemsQueryCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        ">;"
    }
.end annotation


# instance fields
.field private canceled:Z

.field final searchText:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;Ljava/lang/String;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->this$0:Lcom/squareup/ui/items/DetailSearchableListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 2

    .line 57
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->canceled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->this$0:Lcom/squareup/ui/items/DetailSearchableListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->access$000(Lcom/squareup/ui/items/DetailSearchableListPresenter;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->this$0:Lcom/squareup/ui/items/DetailSearchableListPresenter;

    iget-object v0, v0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->this$0:Lcom/squareup/ui/items/DetailSearchableListPresenter;

    invoke-static {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->access$100(Lcom/squareup/ui/items/DetailSearchableListPresenter;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    if-eqz v0, :cond_2

    .line 62
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result p1

    if-nez p1, :cond_2

    .line 63
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    goto :goto_1

    .line 58
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 66
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->this$0:Lcom/squareup/ui/items/DetailSearchableListPresenter;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->access$202(Lcom/squareup/ui/items/DetailSearchableListPresenter;Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->call(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->canceled:Z

    return-void
.end method
