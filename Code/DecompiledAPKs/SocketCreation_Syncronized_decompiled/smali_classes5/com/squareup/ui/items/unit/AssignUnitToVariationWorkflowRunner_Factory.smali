.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "AssignUnitToVariationWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final assignUnitToVariationWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final layoutConfigProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final resultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->resultHandlerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->assignUnitToVariationWorkflowProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->layoutConfigProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;",
            ">;)",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->resultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;

    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->assignUnitToVariationWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    iget-object v4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->layoutConfigProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner_Factory;->get()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
