.class public final Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final behaviorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final modificationHandlerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;->modificationHandlerFactoryProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;->behaviorFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;)Lcom/squareup/ui/items/DetailSearchableListWorkflow;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;-><init>(Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/DetailSearchableListWorkflow;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;->modificationHandlerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;->behaviorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;->newInstance(Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;)Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflow_Factory;->get()Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    move-result-object v0

    return-object v0
.end method
