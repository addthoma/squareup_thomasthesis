.class Lcom/squareup/ui/items/EditItemScopeRunner$1;
.super Ljava/lang/Object;
.source "EditItemScopeRunner.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemOptionsAndValues(Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
        "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
        "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemScopeRunner;

.field final synthetic val$idempotencyKey:Ljava/lang/String;

.field final synthetic val$optionsToUpsert:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemScopeRunner;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .line 548
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner$1;->this$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner$1;->val$optionsToUpsert:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemScopeRunner$1;->val$idempotencyKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;)Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;
    .locals 1

    .line 558
    iget-object v0, p2, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;->objects:Ljava/util/List;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->writeConnectV2Objects(Ljava/util/List;)V

    return-object p2
.end method

.method public perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Online;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
            ">;"
        }
    .end annotation

    .line 551
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner$1;->val$optionsToUpsert:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner$1;->val$idempotencyKey:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Online;->batchUpsertCatalogConnectV2Objects(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 548
    check-cast p2, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditItemScopeRunner$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;)Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;

    move-result-object p1

    return-object p1
.end method
