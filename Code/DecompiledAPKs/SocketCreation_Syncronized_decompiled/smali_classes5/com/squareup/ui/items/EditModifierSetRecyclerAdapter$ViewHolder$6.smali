.class Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditModifierSetRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindModifierOption(Lcom/squareup/api/items/ItemModifierOption$Builder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

.field final synthetic val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Lcom/squareup/ui/items/ModifierOptionRow;)V
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->access$000(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->getOption()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-virtual {v1}, Lcom/squareup/ui/items/ModifierOptionRow;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;->val$optionRow:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierOptionRow;->getPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->updateOption(Lcom/squareup/api/items/ItemModifierOption$Builder;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
