.class public final Lcom/squareup/ui/items/DetailSearchableListState$ListData;
.super Ljava/lang/Object;
.source "DetailSearchableListState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008!\u0008\u0086\u0008\u0018\u00002\u00020\u0001B]\u0012\u0010\u0008\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\u0010J\u0011\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010 \u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001dJ\t\u0010!\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\"\u001a\u00020\nH\u00c6\u0003J\t\u0010#\u001a\u00020\u000cH\u00c6\u0003J\t\u0010$\u001a\u00020\u000cH\u00c6\u0003J\t\u0010%\u001a\u00020\u000cH\u00c6\u0003J\t\u0010&\u001a\u00020\u000cH\u00c6\u0003Jh\u0010\'\u001a\u00020\u00002\u0010\u0008\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u000cH\u00c6\u0001\u00a2\u0006\u0002\u0010(J\u0013\u0010)\u001a\u00020\u000c2\u0008\u0010*\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010+\u001a\u00020\u0006H\u00d6\u0001J\t\u0010,\u001a\u00020\nH\u00d6\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000e\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u000f\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0016R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0016R\u0011\u0010\r\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010\u001e\u001a\u0004\u0008\u001c\u0010\u001d\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListState$ListData;",
        "",
        "listDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "unfilteredListSize",
        "",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "searchTerm",
        "",
        "showCreateFromSearchButton",
        "",
        "supportSetUpItemGridButton",
        "shouldDisableCreateButton",
        "shouldShowCovertItemButton",
        "(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)V",
        "getListDataSource",
        "()Lcom/squareup/cycler/DataSource;",
        "getSearchTerm",
        "()Ljava/lang/String;",
        "getShouldDisableCreateButton",
        "()Z",
        "getShouldShowCovertItemButton",
        "getShowCreateFromSearchButton",
        "getSupportSetUpItemGridButton",
        "getTextBag",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "getUnfilteredListSize",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)Lcom/squareup/ui/items/DetailSearchableListState$ListData;",
        "equals",
        "other",
        "hashCode",
        "toString",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final listDataSource:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation
.end field

.field private final searchTerm:Ljava/lang/String;

.field private final shouldDisableCreateButton:Z

.field private final shouldShowCovertItemButton:Z

.field private final showCreateFromSearchButton:Z

.field private final supportSetUpItemGridButton:Z

.field private final textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

.field private final unfilteredListSize:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
            "Ljava/lang/String;",
            "ZZZZ)V"
        }
    .end annotation

    const-string/jumbo v0, "textBag"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchTerm"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    iput-boolean p6, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    iput-boolean p7, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    iput-boolean p8, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 67
    move-object v1, v2

    check-cast v1, Lcom/squareup/cycler/DataSource;

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object v4, p1

    :goto_0
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    .line 68
    move-object v1, v2

    check-cast v1, Ljava/lang/Integer;

    move-object v5, v1

    goto :goto_1

    :cond_1
    move-object v5, p2

    :goto_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    const-string v1, ""

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object/from16 v7, p4

    :goto_2
    and-int/lit8 v1, v0, 0x10

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    move/from16 v8, p5

    :goto_3
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    const/4 v9, 0x1

    goto :goto_4

    :cond_4
    move/from16 v9, p6

    :goto_4
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_5

    const/4 v10, 0x0

    goto :goto_5

    :cond_5
    move/from16 v10, p7

    :goto_5
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    const/4 v11, 0x0

    goto :goto_6

    :cond_6
    move/from16 v11, p8

    :goto_6
    move-object v3, p0

    move-object v6, p3

    .line 74
    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;-><init>(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    return v0
.end method

.method public final copy(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)Lcom/squareup/ui/items/DetailSearchableListState$ListData;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
            "Ljava/lang/String;",
            "ZZZZ)",
            "Lcom/squareup/ui/items/DetailSearchableListState$ListData;"
        }
    .end annotation

    const-string/jumbo v0, "textBag"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchTerm"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;-><init>(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    iget-object v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iget-object v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    iget-boolean v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    iget-boolean v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    iget-boolean v1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    iget-boolean p1, p1, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getListDataSource()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final getSearchTerm()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    return-object v0
.end method

.method public final getShouldDisableCreateButton()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    return v0
.end method

.method public final getShouldShowCovertItemButton()Z
    .locals 1

    .line 74
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    return v0
.end method

.method public final getShowCreateFromSearchButton()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    return v0
.end method

.method public final getSupportSetUpItemGridButton()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    return v0
.end method

.method public final getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public final getUnfilteredListSize()Ljava/lang/Integer;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ListData(listDataSource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->listDataSource:Lcom/squareup/cycler/DataSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unfilteredListSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->unfilteredListSize:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", textBag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchTerm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->searchTerm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", showCreateFromSearchButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->showCreateFromSearchButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", supportSetUpItemGridButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->supportSetUpItemGridButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldDisableCreateButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldDisableCreateButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowCovertItemButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->shouldShowCovertItemButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
