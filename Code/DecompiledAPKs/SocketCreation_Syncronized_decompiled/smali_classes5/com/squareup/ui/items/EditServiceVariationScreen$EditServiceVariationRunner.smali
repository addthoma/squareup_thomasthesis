.class public interface abstract Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;
.super Ljava/lang/Object;
.source "EditServiceVariationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditServiceVariationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EditServiceVariationRunner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0007H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0008\u0010\u000e\u001a\u00020\u0003H&J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010H&J\u0008\u0010\u0012\u001a\u00020\u0003H&J\u0008\u0010\u0013\u001a\u00020\u0003H&J\u0008\u0010\u0014\u001a\u00020\u0003H&J\u0010\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0007H&J\u0008\u0010\u0017\u001a\u00020\u0003H&J\u0008\u0010\u0018\u001a\u00020\u0019H&J\u0010\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u000cH&J\u0008\u0010\u001c\u001a\u00020\u0003H&J\u0008\u0010\u001d\u001a\u00020\u0003H&J\u0008\u0010\u001e\u001a\u00020\u0003H&J\u0010\u0010\u001f\u001a\u00020\u00032\u0006\u0010 \u001a\u00020\u000cH&J\u0010\u0010!\u001a\u00020\u00032\u0006\u0010\"\u001a\u00020\u000cH&\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
        "",
        "assignEmployeesClicked",
        "",
        "backClickedWhenEditingItemVariation",
        "blockExtraTimeToggled",
        "blocked",
        "",
        "bookableByCustomerToggled",
        "bookable",
        "cancellationFeeChanged",
        "newCancellationFee",
        "",
        "doneClickedWhenEditingItemVariation",
        "durationRowClicked",
        "editServiceVariationScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
        "extraTimeRowClicked",
        "finalDurationRowClicked",
        "gapDurationRowClicked",
        "gapTimeToggled",
        "checked",
        "initialDurationRowClicked",
        "maxCancellationFeeMoney",
        "Lcom/squareup/protos/common/Money;",
        "priceDescriptionChanged",
        "newPriceDescription",
        "removeButtonClickedWhenEditingItemVariation",
        "serviceVariationPriceTypeClicked",
        "unitTypeSelectClicked",
        "variationNameChanged",
        "newName",
        "variationPriceChanged",
        "newPrice",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract assignEmployeesClicked()V
.end method

.method public abstract backClickedWhenEditingItemVariation()V
.end method

.method public abstract blockExtraTimeToggled(Z)V
.end method

.method public abstract bookableByCustomerToggled(Z)V
.end method

.method public abstract cancellationFeeChanged(Ljava/lang/String;)V
.end method

.method public abstract doneClickedWhenEditingItemVariation()V
.end method

.method public abstract durationRowClicked()V
.end method

.method public abstract editServiceVariationScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract extraTimeRowClicked()V
.end method

.method public abstract finalDurationRowClicked()V
.end method

.method public abstract gapDurationRowClicked()V
.end method

.method public abstract gapTimeToggled(Z)V
.end method

.method public abstract initialDurationRowClicked()V
.end method

.method public abstract maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;
.end method

.method public abstract priceDescriptionChanged(Ljava/lang/String;)V
.end method

.method public abstract removeButtonClickedWhenEditingItemVariation()V
.end method

.method public abstract serviceVariationPriceTypeClicked()V
.end method

.method public abstract unitTypeSelectClicked()V
.end method

.method public abstract variationNameChanged(Ljava/lang/String;)V
.end method

.method public abstract variationPriceChanged(Ljava/lang/String;)V
.end method
