.class final Lcom/squareup/ui/items/EditItemState$TaxStates$1;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState$TaxStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/EditItemState$TaxStates;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemState$TaxStates;
    .locals 4

    .line 1153
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1154
    const-class v1, Lcom/squareup/ui/items/EditItemState$TaxState;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1156
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 1157
    :goto_0
    new-instance v2, Lcom/squareup/ui/items/EditItemState$TaxStates;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, p1, v3}, Lcom/squareup/ui/items/EditItemState$TaxStates;-><init>(Ljava/util/LinkedHashMap;Ljava/lang/String;ZLcom/squareup/ui/items/EditItemState$1;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1151
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemState$TaxStates$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemState$TaxStates;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/EditItemState$TaxStates;
    .locals 0

    .line 1161
    new-array p1, p1, [Lcom/squareup/ui/items/EditItemState$TaxStates;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1151
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemState$TaxStates$1;->newArray(I)[Lcom/squareup/ui/items/EditItemState$TaxStates;

    move-result-object p1

    return-object p1
.end method
