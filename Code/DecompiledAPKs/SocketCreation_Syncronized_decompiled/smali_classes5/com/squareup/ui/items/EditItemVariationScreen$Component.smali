.class public interface abstract Lcom/squareup/ui/items/EditItemVariationScreen$Component;
.super Ljava/lang/Object;
.source "EditItemVariationScreen.kt"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/items/EditItemVariationScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/ErrorsBarPresenter$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/ui/items/DuplicateSkuValidator$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/items/EditItemVariationScreen$LayoutModule;,
        Lcom/squareup/ui/items/EditItemVariationScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemVariationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&J\u0008\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemVariationScreen$Component;",
        "Lcom/squareup/ui/ErrorsBarView$Component;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;",
        "duplicateSkuValidator",
        "Lcom/squareup/ui/items/DuplicateSkuValidator;",
        "editItemVariationCoordinator",
        "Lcom/squareup/ui/items/EditItemVariationCoordinator;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract duplicateSkuValidator()Lcom/squareup/ui/items/DuplicateSkuValidator;
.end method

.method public abstract editItemVariationCoordinator()Lcom/squareup/ui/items/EditItemVariationCoordinator;
.end method
