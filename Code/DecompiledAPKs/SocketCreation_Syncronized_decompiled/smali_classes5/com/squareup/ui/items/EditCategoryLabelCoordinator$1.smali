.class Lcom/squareup/ui/items/EditCategoryLabelCoordinator$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "EditCategoryLabelCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->lambda$null$1(Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$1;->this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$1;->this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->access$000(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$1;->this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->access$100(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
