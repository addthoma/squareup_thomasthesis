.class public Lcom/squareup/ui/items/ModifierOptionRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "ModifierOptionRow.java"


# instance fields
.field private delete:Landroid/view/View;

.field private dragHandle:Landroid/view/View;

.field private nameView:Lcom/squareup/widgets/SelectableEditText;

.field private option:Lcom/squareup/api/items/ItemModifierOption$Builder;

.field private priceView:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierOptionRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 30
    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    .line 31
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 30
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/ModifierOptionRow;->setBorderWidth(I)V

    .line 32
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 33
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ModifierOptionRow;->setHorizontalInsets(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/ModifierOptionRow;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNameView()Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    return-object v0
.end method

.method public getOption()Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->option:Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPriceView()Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    return-object v0
.end method

.method public hideControls()V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->dragHandle:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->delete:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 37
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    .line 38
    sget v0, Lcom/squareup/itemsapplet/R$id;->modifier_option_drag_handle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->dragHandle:Landroid/view/View;

    .line 39
    sget v0, Lcom/squareup/itemsapplet/R$id;->modifier_option_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    .line 40
    sget v0, Lcom/squareup/itemsapplet/R$id;->modifier_option_price:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    .line 41
    sget v0, Lcom/squareup/itemsapplet/R$id;->modifier_option_delete:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->delete:Landroid/view/View;

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/items/ModifierOptionRow$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/ModifierOptionRow$1;-><init>(Lcom/squareup/ui/items/ModifierOptionRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public removeAllTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setContent(Lcom/squareup/api/items/ItemModifierOption$Builder;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierOptionRow;->option:Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 74
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 78
    :cond_0
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public setDeleteClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->delete:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setNewModifierContent()V
    .locals 2

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->option:Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->priceView:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    sget v1, Lcom/squareup/itemsapplet/R$string;->modifier_new_hint:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    return-void
.end method

.method public setNewOptionContent()V
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierOptionRow;->setNewModifierContent()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    sget v1, Lcom/squareup/registerlib/R$string;->modifier_options_hint:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    return-void
.end method

.method public showControls()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->dragHandle:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierOptionRow;->delete:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
