.class public Lcom/squareup/ui/items/AppliedLocationsBanner;
.super Landroid/widget/LinearLayout;
.source "AppliedLocationsBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/AppliedLocationsBanner$Component;
    }
.end annotation


# instance fields
.field private changesUnavailableHeader:Landroid/widget/TextView;

.field private changesUnavailableSubtext:Landroid/widget/TextView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private sharedItemBanner:Landroid/widget/TextView;

.field private spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-class p2, Lcom/squareup/ui/items/AppliedLocationsBanner$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/AppliedLocationsBanner$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/AppliedLocationsBanner$Component;->inject(Lcom/squareup/ui/items/AppliedLocationsBanner;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 90
    sget v0, Lcom/squareup/editbaseobject/R$id;->shared_item_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->sharedItemBanner:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/squareup/editbaseobject/R$id;->spinner_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 92
    sget v0, Lcom/squareup/editbaseobject/R$id;->changes_unavailable_subtext:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableSubtext:Landroid/widget/TextView;

    .line 93
    sget v0, Lcom/squareup/editbaseobject/R$id;->changes_unavailable_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableHeader:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public synthetic lambda$showItemChangesUnavailableBanner$1$AppliedLocationsBanner()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableHeader:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableSubtext:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$updateBannerText$0$AppliedLocationsBanner()V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->sharedItemBanner:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/items/AppliedLocationsBanner;->bindViews()V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->presenter:Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->useGreyBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_ultra_light_gray_border_bottom_light_gray_1dp:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setBackgroundResource(I)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->sharedItemBanner:Landroid/widget/TextView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_ultra_light_gray_border_bottom_light_gray_1dp:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableHeader:Landroid/widget/TextView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_ultra_light_gray:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableSubtext:Landroid/widget/TextView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_ultra_light_gray_border_bottom_light_gray_1dp:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->presenter:Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->presenter:Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->dropView(Ljava/lang/Object;)V

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method showItemChangesUnavailableBanner(Ljava/lang/String;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableHeader:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$AppliedLocationsBanner$GDomq3d36eBJk5mXKDnz_mvkQpY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$AppliedLocationsBanner$GDomq3d36eBJk5mXKDnz_mvkQpY;-><init>(Lcom/squareup/ui/items/AppliedLocationsBanner;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {p1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method showingChangesUnavailableBanner()Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableHeader:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    return v0
.end method

.method updateBannerText(Ljava/lang/String;)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->sharedItemBanner:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {p1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->getVisibility()I

    move-result p1

    if-nez p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$AppliedLocationsBanner$wsIDW4ll7lkRZhits5aZQwIBano;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$AppliedLocationsBanner$wsIDW4ll7lkRZhits5aZQwIBano;-><init>(Lcom/squareup/ui/items/AppliedLocationsBanner;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->spinner:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {p1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    goto :goto_0

    .line 67
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableHeader:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->changesUnavailableSubtext:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->sharedItemBanner:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method
