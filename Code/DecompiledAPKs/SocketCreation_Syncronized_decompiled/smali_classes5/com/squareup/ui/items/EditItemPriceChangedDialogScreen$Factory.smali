.class public Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen$Factory;
.super Ljava/lang/Object;
.source "EditItemPriceChangedDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/items/EditItemScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->showConfirmGlobalPriceDialog()V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/items/EditItemScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 41
    sget-object p1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_VIA_ITEM_V3_WITH_LOCATION_OVERRIDE:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveChanges(Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 30
    const-class v0, Lcom/squareup/ui/items/EditItemScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 31
    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object v0

    .line 32
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;

    .line 34
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/edititem/R$string;->item_editing_select_location_update_price_dialog_title:I

    .line 35
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    invoke-static {v1}, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;->access$000(Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/edititem/R$string;->item_editing_update_all_locations:I

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemPriceChangedDialogScreen$Factory$7x9wHwex-OKS2kKHoNghtUD7qZY;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemPriceChangedDialogScreen$Factory$7x9wHwex-OKS2kKHoNghtUD7qZY;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 38
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/edititem/R$string;->item_editing_update_this_location_only:I

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemPriceChangedDialogScreen$Factory$1uC3HsAs4reEdYuLTTCvMdg_3pw;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemPriceChangedDialogScreen$Factory$1uC3HsAs4reEdYuLTTCvMdg_3pw;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 40
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 43
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
