.class public final Lcom/squareup/ui/items/DetailSearchableListConfiguration;
.super Ljava/lang/Object;
.source "DetailSearchableListConfiguration.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "Landroid/os/Parcelable;",
        "dataType",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "(Lcom/squareup/ui/items/DetailSearchableListDataType;)V",
        "getDataType",
        "()Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

.field private static final DISCOUNTS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

.field private static final ITEMS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

.field private static final SERVICES:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

.field private static final UNITS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;


# instance fields
.field private final dataType:Lcom/squareup/ui/items/DetailSearchableListDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    .line 32
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 43
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 44
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Units:Lcom/squareup/ui/items/DetailSearchableListDataType;

    .line 43
    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;-><init>(Lcom/squareup/ui/items/DetailSearchableListDataType;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->UNITS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 47
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 48
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Discounts:Lcom/squareup/ui/items/DetailSearchableListDataType;

    .line 47
    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;-><init>(Lcom/squareup/ui/items/DetailSearchableListDataType;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->DISCOUNTS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 51
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 52
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Items:Lcom/squareup/ui/items/DetailSearchableListDataType;

    .line 51
    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;-><init>(Lcom/squareup/ui/items/DetailSearchableListDataType;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->ITEMS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 55
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 56
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListDataType;->Services:Lcom/squareup/ui/items/DetailSearchableListDataType;

    .line 55
    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;-><init>(Lcom/squareup/ui/items/DetailSearchableListDataType;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->SERVICES:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/items/DetailSearchableListDataType;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->dataType:Lcom/squareup/ui/items/DetailSearchableListDataType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListDataType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;-><init>(Lcom/squareup/ui/items/DetailSearchableListDataType;)V

    return-void
.end method

.method public static final synthetic access$getDISCOUNTS$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->DISCOUNTS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method

.method public static final synthetic access$getITEMS$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->ITEMS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method

.method public static final synthetic access$getSERVICES$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->SERVICES:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method

.method public static final synthetic access$getUNITS$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->UNITS:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->dataType:Lcom/squareup/ui/items/DetailSearchableListDataType;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->dataType:Lcom/squareup/ui/items/DetailSearchableListDataType;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
