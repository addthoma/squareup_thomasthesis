.class public final Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;
.super Ljava/lang/Object;
.source "DetailSearchableListViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \"2\u00020\u0001:\u0001\"By\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0008\u0008\u0001\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0002\u0010\u001dJ\u000e\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "device",
        "Lcom/squareup/util/Device;",
        "res",
        "Lcom/squareup/util/Res;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "barcodeScannerTracker",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "editUnitViewBindings",
        "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
        "(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V",
        "build",
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory;",
        "dataType",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "Companion",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;


# instance fields
.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final device:Lcom/squareup/util/Device;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final editUnitViewBindings:Lcom/squareup/items/unit/ui/EditUnitViewBindings;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->Companion:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V
    .locals 1
    .param p6    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .param p12    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "barcodeScannerTracker"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editUnitViewBindings"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iput-object p11, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p12, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p13, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->editUnitViewBindings:Lcom/squareup/items/unit/ui/EditUnitViewBindings;

    return-void
.end method


# virtual methods
.method public final build(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListViewFactory;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "dataType"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListViewFactory;

    .line 87
    iget-object v4, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iget-object v5, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->device:Lcom/squareup/util/Device;

    iget-object v6, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->res:Lcom/squareup/util/Res;

    iget-object v7, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v8, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v9, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 88
    iget-object v10, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v11, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v12, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v13, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    .line 89
    iget-object v14, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v15, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iget-object v3, v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->editUnitViewBindings:Lcom/squareup/items/unit/ui/EditUnitViewBindings;

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->Companion:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;->getSection(Lcom/squareup/ui/items/DetailSearchableListDataType;)Ljava/lang/Class;

    move-result-object v17

    const/16 v18, 0x0

    move-object v0, v3

    move-object v3, v2

    move-object/from16 v16, v0

    .line 86
    invoke-direct/range {v3 .. v18}, Lcom/squareup/ui/items/DetailSearchableListViewFactory;-><init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;Ljava/lang/Class;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v2
.end method
