.class public final Lcom/squareup/ui/items/DetailSearchableListCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DetailSearchableListCoordinator.kt"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListCoordinator.kt\ncom/squareup/ui/items/DetailSearchableListCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,458:1\n49#2:459\n50#2,3:465\n53#2:523\n599#3,4:460\n601#3:464\n310#4,3:468\n313#4,3:477\n310#4,3:480\n313#4,3:489\n328#4:496\n342#4,5:497\n344#4,4:502\n329#4:506\n355#4,3:509\n358#4,3:518\n35#5,6:471\n35#5,6:483\n35#5,6:512\n48#6:492\n61#6,3:493\n65#6:507\n49#6:508\n43#7,2:521\n*E\n*S KotlinDebug\n*F\n+ 1 DetailSearchableListCoordinator.kt\ncom/squareup/ui/items/DetailSearchableListCoordinator\n*L\n184#1:459\n184#1,3:465\n184#1:523\n184#1,4:460\n184#1:464\n184#1,3:468\n184#1,3:477\n184#1,3:480\n184#1,3:489\n184#1:496\n184#1,5:497\n184#1,4:502\n184#1:506\n184#1,3:509\n184#1,3:518\n184#1,6:471\n184#1,6:483\n184#1,6:512\n184#1:492\n184#1,3:493\n184#1:507\n184#1:508\n184#1,2:521\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ca\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u00012\u00020\u0002B{\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0008\u0008\u0001\u0010\u001d\u001a\u00020\u001e\u00a2\u0006\u0002\u0010\u001fJ\u0010\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020(H\u0016J\u0012\u0010=\u001a\u00020;2\u0008\u0010>\u001a\u0004\u0018\u00010&H\u0016J\u0010\u0010?\u001a\u00020;2\u0006\u0010<\u001a\u00020(H\u0002J\u0010\u0010@\u001a\u00020;2\u0006\u0010A\u001a\u00020(H\u0002J\u0010\u0010B\u001a\u00020;2\u0006\u0010<\u001a\u00020(H\u0016J\u0016\u0010C\u001a\u0008\u0012\u0004\u0012\u0002060D2\u0006\u0010E\u001a\u00020\u0005H\u0002J\u0008\u0010F\u001a\u00020.H\u0002J\u0018\u0010G\u001a\u00020;2\u0006\u0010<\u001a\u00020(2\u0006\u0010E\u001a\u00020\u0005H\u0002J\u0010\u0010H\u001a\u00020;2\u0006\u0010E\u001a\u00020\u0005H\u0002J\u0010\u0010I\u001a\u00020;2\u0006\u0010J\u001a\u00020KH\u0002J\u0018\u0010L\u001a\u00020;2\u0006\u0010<\u001a\u00020(2\u0006\u0010J\u001a\u00020KH\u0002J\u0010\u0010M\u001a\u00020;2\u0006\u0010E\u001a\u00020\u0005H\u0002J\u0018\u0010N\u001a\u00020;2\u0006\u0010O\u001a\u00020&2\u0006\u0010J\u001a\u00020KH\u0002J\u001c\u0010P\u001a\u00020Q*\u00020R2\u0006\u0010E\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\tH\u0002R\u000e\u0010 \u001a\u00020!X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020(X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082.\u00a2\u0006\u0002\n\u0000R2\u0010-\u001a&\u0012\u000c\u0012\n /*\u0004\u0018\u00010.0. /*\u0012\u0012\u000c\u0012\n /*\u0004\u0018\u00010.0.\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020.X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020.X\u0082\u000e\u00a2\u0006\u0002\n\u0000R2\u00102\u001a&\u0012\u000c\u0012\n /*\u0004\u0018\u00010.0. /*\u0012\u0012\u000c\u0012\n /*\u0004\u0018\u00010.0.\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020(X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00104\u001a\u0008\u0012\u0004\u0012\u00020605X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000208X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u00109\u001a\u0008\u0012\u0004\u0012\u00020&0%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006S"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "device",
        "Lcom/squareup/util/Device;",
        "barcodeScannerTracker",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Device;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V",
        "SEARCH_DELAY_MS",
        "",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "barcodeRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "emptyView",
        "Landroid/view/View;",
        "emptyViewSubtitle",
        "Lcom/squareup/widgets/MessageView;",
        "emptyViewTitle",
        "Landroid/widget/TextView;",
        "isPhoneOrPortraitLessThan10Inches",
        "",
        "kotlin.jvm.PlatformType",
        "isSmallScreen",
        "isTablet",
        "isTabletObservable",
        "loadingSpinner",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        "searchBar",
        "Lcom/squareup/ui/XableEditText;",
        "searchTextRelay",
        "attach",
        "",
        "view",
        "barcodeScanned",
        "barcode",
        "bindView",
        "createRecycler",
        "coordinatorView",
        "detach",
        "getRecyclerData",
        "Lcom/squareup/cycler/DataSource;",
        "screen",
        "isNested",
        "onScreenUpdate",
        "updateActionBar",
        "updateActionBarTitle",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "updateEmptyView",
        "updateList",
        "updateSearchBar",
        "searchTerm",
        "toDetailSearchableObjectRow",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final SEARCH_DELAY_MS:J

.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final barcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private emptyView:Landroid/view/View;

.field private emptyViewSubtitle:Lcom/squareup/widgets/MessageView;

.field private emptyViewTitle:Landroid/widget/TextView;

.field private final isPhoneOrPortraitLessThan10Inches:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isSmallScreen:Z

.field private isTablet:Z

.field private final isTabletObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private loadingSpinner:Landroid/view/View;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/items/DetailSearchableListRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            ">;"
        }
    .end annotation
.end field

.field private searchBar:Lcom/squareup/ui/XableEditText;

.field private final searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Device;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p13    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "barcodeScannerTracker"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p11, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iput-object p12, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p13, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 88
    invoke-interface {p10}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object p1

    .line 89
    sget-object p2, Lcom/squareup/ui/items/DetailSearchableListCoordinator$isPhoneOrPortraitLessThan10Inches$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$isPhoneOrPortraitLessThan10Inches$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isPhoneOrPortraitLessThan10Inches:Lio/reactivex/Observable;

    .line 91
    invoke-interface {p10}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object p1

    .line 92
    sget-object p2, Lcom/squareup/ui/items/DetailSearchableListCoordinator$isTabletObservable$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$isTabletObservable$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isTabletObservable:Lio/reactivex/Observable;

    .line 104
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    const-wide/16 p3, 0x64

    .line 105
    iput-wide p3, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->SEARCH_DELAY_MS:J

    .line 107
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->barcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getSearchTextRelay$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getTutorialCore$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method

.method public static final synthetic access$isSmallScreen$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isSmallScreen:Z

    return p0
.end method

.method public static final synthetic access$isTablet$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isTablet:Z

    return p0
.end method

.method public static final synthetic access$onScreenUpdate(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Landroid/view/View;Lcom/squareup/ui/items/DetailSearchableListScreen;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->onScreenUpdate(Landroid/view/View;Lcom/squareup/ui/items/DetailSearchableListScreen;)V

    return-void
.end method

.method public static final synthetic access$setSmallScreen$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Z)V
    .locals 0

    .line 72
    iput-boolean p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isSmallScreen:Z

    return-void
.end method

.method public static final synthetic access$setTablet$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Z)V
    .locals 0

    .line 72
    iput-boolean p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isTablet:Z

    return-void
.end method

.method public static final synthetic access$toDetailSearchableObjectRow(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->toDetailSearchableObjectRow(Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    move-result-object p0

    return-object p0
.end method

.method private final bindView(Landroid/view/View;)V
    .locals 2

    .line 172
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_search_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.d\u2026archable_list_search_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 173
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 174
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_loading_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.d\u2026le_list_loading_progress)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->loadingSpinner:Landroid/view/View;

    .line 175
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_empty_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->emptyView:Landroid/view/View;

    .line 176
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_empty_view_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->emptyViewTitle:Landroid/widget/TextView;

    .line 177
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_empty_view_subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->emptyViewSubtitle:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 6

    .line 182
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_recycler_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 181
    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 459
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 460
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 461
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 465
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 466
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 185
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$1$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 469
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 192
    sget v2, Lcom/squareup/itemsapplet/R$layout;->detail_searchable_list_create_button_row:I

    .line 471
    new-instance v3, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/items/DetailSearchableListCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 238
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 468
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 481
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$row$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 243
    sget v3, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    .line 483
    new-instance v4, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$create$2;

    invoke-direct {v4, v3}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$create$2;-><init>(I)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 481
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 480
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 492
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$1;-><init>()V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 494
    sget-object v3, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 498
    new-instance v4, Lcom/squareup/cycler/BinderRowSpec;

    .line 502
    sget-object v5, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$3;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$nohoRow$3;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 498
    invoke-direct {v4, v5, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 496
    invoke-virtual {v4, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 501
    check-cast v4, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 497
    invoke-virtual {v1, v4}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 510
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$extraItem$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 264
    sget v3, Lcom/squareup/itemsapplet/R$layout;->detail_searchable_list_no_search_results_row:I

    .line 512
    new-instance v4, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v4, v3, p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/ui/items/DetailSearchableListCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 302
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 509
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 521
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0xa

    .line 305
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 521
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 463
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    .line 310
    new-instance v0, Lcom/squareup/items/unit/ui/RecyclerViewTopSpacingDecoration;

    .line 311
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/noho/R$dimen;->noho_gutter_detail:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    .line 310
    invoke-direct {v0, v1}, Lcom/squareup/items/unit/ui/RecyclerViewTopSpacingDecoration;-><init>(I)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    .line 309
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 317
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    move-result-object p1

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;->setRemoveDuration(J)V

    :cond_0
    return-void

    .line 460
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final getRecyclerData(Lcom/squareup/ui/items/DetailSearchableListScreen;)Lcom/squareup/cycler/DataSource;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            ")",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListRow;",
            ">;"
        }
    .end annotation

    .line 380
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getListDataSource()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 383
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getSearchTerm()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/2addr v4, v2

    if-nez v3, :cond_1

    if-nez v4, :cond_1

    const/4 v1, 0x1

    .line 387
    :cond_1
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;

    invoke-direct {v2, p0, v1, p1, v0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;ZLcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/cycler/DataSource;)V

    check-cast v2, Lcom/squareup/cycler/DataSource;

    return-object v2
.end method

.method private final isNested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method private final onScreenUpdate(Landroid/view/View;Lcom/squareup/ui/items/DetailSearchableListScreen;)V
    .locals 2

    .line 324
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->updateSearchBar(Ljava/lang/String;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;)V

    .line 325
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->updateActionBar(Lcom/squareup/ui/items/DetailSearchableListScreen;)V

    .line 326
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->updateEmptyView(Landroid/view/View;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;)V

    .line 327
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->updateList(Lcom/squareup/ui/items/DetailSearchableListScreen;)V

    return-void
.end method

.method private final toDetailSearchableObjectRow(Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;
    .locals 11

    .line 444
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/items/UnitRow;

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    invoke-direct {v0, p2, p3, p1}, Lcom/squareup/ui/items/UnitRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    goto :goto_0

    .line 445
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/ui/items/DiscountRow;

    .line 446
    move-object v4, p1

    check-cast v4, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    invoke-virtual {v4}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;->getDiscountDescription()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 447
    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v9, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v10, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-object v1, v0

    move-object v2, p2

    move-object v3, p3

    .line 445
    invoke-direct/range {v1 .. v10}, Lcom/squareup/ui/items/DiscountRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    goto :goto_0

    .line 449
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/ui/items/ItemRow;

    .line 450
    move-object v4, p1

    check-cast v4, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, v0

    move-object v2, p2

    move-object v3, p3

    .line 449
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/ItemRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    goto :goto_0

    .line 452
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/ui/items/ServiceRow;

    .line 453
    move-object v4, p1

    check-cast v4, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 454
    iget-object v9, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, v0

    move-object v2, p2

    move-object v3, p3

    .line 452
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/items/ServiceRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    :goto_0
    return-object v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final updateActionBar(Lcom/squareup/ui/items/DetailSearchableListScreen;)V
    .locals 4

    .line 404
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isSmallScreen:Z

    const-string v1, "actionBar"

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isNested()Z

    move-result v0

    if-nez v0, :cond_1

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hideUpButton()V

    goto :goto_0

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateActionBar$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateActionBar$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 409
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->updateActionBarTitle(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;)V

    .line 412
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isTablet:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getPossiblyShowSetUpItemGridButton()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 413
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    .line 414
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_setup_item_grid:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V

    .line 415
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateActionBar$2;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateActionBar$2;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 417
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    :goto_1
    return-void
.end method

.method private final updateActionBarTitle(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;)V
    .locals 3

    .line 422
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isSmallScreen:Z

    const-string v1, "actionBar"

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isNested()Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getTitleId()I

    move-result p1

    invoke-interface {v2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getTitleId()I

    move-result p1

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private final updateEmptyView(Landroid/view/View;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;)V
    .locals 3

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->emptyViewTitle:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "emptyViewTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getEmptyViewTitleId()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->emptyViewSubtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "emptyViewSubtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 335
    :cond_1
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 336
    sget p1, Lcom/squareup/itemsapplet/R$string;->items_applet_detail_searchable_list_empty_view_message:I

    const-string v2, "learn_more"

    invoke-virtual {v1, p1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 337
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getEmptyViewSubtitleLearnMoreLinkId()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 338
    sget v1, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 339
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 340
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getCreateButtonTextId()I

    move-result p2

    invoke-interface {v1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "create_button"

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 341
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateList(Lcom/squareup/ui/items/DetailSearchableListScreen;)V
    .locals 9

    .line 360
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->getRecyclerData(Lcom/squareup/ui/items/DetailSearchableListScreen;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    .line 361
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getListDataSource()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 364
    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->isEmpty()Z

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 365
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getSearchTerm()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v5}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/2addr v5, v2

    .line 367
    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->loadingSpinner:Landroid/view/View;

    if-nez v6, :cond_2

    const-string v7, "loadingSpinner"

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/16 v7, 0x8

    if-eqz v4, :cond_3

    const/4 v8, 0x0

    goto :goto_2

    :cond_3
    const/16 v8, 0x8

    :goto_2
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 368
    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->emptyView:Landroid/view/View;

    if-nez v6, :cond_4

    const-string v8, "emptyView"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    if-eqz v1, :cond_5

    if-nez v5, :cond_5

    goto :goto_3

    :cond_5
    const/16 v3, 0x8

    :goto_3
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 369
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez v1, :cond_6

    const-string v3, "searchBar"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    xor-int/2addr v2, v4

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    .line 371
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v1, :cond_7

    const-string v2, "recycler"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;

    invoke-direct {v2, p0, v0, v5, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$updateList$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Lcom/squareup/cycler/DataSource;ZLcom/squareup/ui/items/DetailSearchableListScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateSearchBar(Ljava/lang/String;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;)V
    .locals 3

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    const-string v1, "searchBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getSearchHintId()I

    move-result p2

    invoke-interface {v2, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 350
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez p2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_2

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_5

    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-lez p2, :cond_4

    const/4 p2, 0x1

    goto :goto_2

    :cond_4
    const/4 p2, 0x0

    :goto_2
    if-nez p2, :cond_9

    :cond_5
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez p2, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p2}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    if-eqz p2, :cond_b

    check-cast p2, Ljava/lang/CharSequence;

    .line 351
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-lez p2, :cond_7

    const/4 p2, 0x1

    goto :goto_3

    :cond_7
    const/4 p2, 0x0

    :goto_3
    if-ne p2, v2, :cond_b

    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_8

    const/4 v0, 0x1

    :cond_8
    if-eqz v0, :cond_b

    .line 355
    :cond_9
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez p2, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 112
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->bindView(Landroid/view/View;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->barcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->screens:Lio/reactivex/Observable;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 121
    iget-wide v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->SEARCH_DELAY_MS:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/jakewharton/rxrelay2/PublishRelay;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->screens:Lio/reactivex/Observable;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 122
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$2;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "searchTextRelay\n        \u2026d(searchQuery))\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "searchBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$3;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 146
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->createRecycler(Landroid/view/View;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isPhoneOrPortraitLessThan10Inches:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$4;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "isPhoneOrPortraitLessTha\u2026be { isSmallScreen = it }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->isTabletObservable:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$5;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "isTabletObservable.subscribe { isTablet = it }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$6;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$6;-><init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { s -> onScreenUpdate(view, s) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object v0, p0

    check-cast v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {p1, v0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    return-void
.end method

.method public barcodeScanned(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->barcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object v1, p0

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 162
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method
