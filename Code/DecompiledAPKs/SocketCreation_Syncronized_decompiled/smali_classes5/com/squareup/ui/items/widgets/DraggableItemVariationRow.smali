.class public Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;
.super Lcom/squareup/ui/items/widgets/DraggableVariationRow;
.source "DraggableItemVariationRow.java"


# instance fields
.field private priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

.field private priceViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

.field private skuText:Ljava/lang/CharSequence;

.field private skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

.field private stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getStockCountRow()Lcom/squareup/ui/items/widgets/StockCountRow;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 28
    invoke-super {p0}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->onFinishInflate()V

    .line 30
    sget v0, Lcom/squareup/edititem/R$id;->draggable_item_variation_row_price:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    .line 31
    sget v0, Lcom/squareup/edititem/R$id;->draggable_item_variation_row_sku:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    .line 32
    sget v0, Lcom/squareup/edititem/R$id;->draggable_item_variation_row_price_sku:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    .line 33
    sget v0, Lcom/squareup/edititem/R$id;->draggable_item_variation_row_stock_count:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/widgets/StockCountRow;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    return-void
.end method

.method public setPriceAndSkuText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    if-eqz v1, :cond_2

    .line 39
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->name:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x8

    if-eqz p3, :cond_0

    .line 43
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const-string v2, "skuText should be empty when shouldNameTakeExtraSpace is true."

    invoke-static {p3, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 45
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p3}, Lcom/squareup/marketfont/MarketTextView;->getVisibility()I

    move-result p3

    if-nez p3, :cond_6

    .line 46
    iget p3, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    add-float/2addr p3, v0

    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 47
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->name:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 51
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p3}, Lcom/squareup/marketfont/MarketTextView;->getVisibility()I

    move-result p3

    if-ne p3, v1, :cond_1

    .line 52
    iget p3, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    sub-float/2addr p3, v0

    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 53
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->name:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 56
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 58
    :cond_2
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    if-eqz p3, :cond_6

    .line 59
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 60
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 63
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 66
    :cond_4
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_5

    .line 67
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 69
    :cond_5
    iget-object p3, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    :cond_6
    :goto_0
    iput-object p2, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuText:Ljava/lang/CharSequence;

    return-void
.end method

.method public setSkuTextColor(Z)V
    .locals 5

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuViewOnTablet:Lcom/squareup/marketfont/MarketTextView;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    goto :goto_0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 78
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    goto :goto_1

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuText:Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 83
    new-instance p1, Landroid/text/SpannableString;

    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 84
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->skuText:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v1

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 84
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;IILandroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 88
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->priceAndSkuViewOnPhone:Lcom/squareup/marketfont/MarketTextView;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 88
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    :cond_3
    :goto_1
    return-void
.end method
