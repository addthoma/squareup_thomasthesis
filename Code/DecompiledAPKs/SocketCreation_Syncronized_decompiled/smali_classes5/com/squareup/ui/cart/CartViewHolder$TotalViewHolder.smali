.class Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TotalViewHolder"
.end annotation


# instance fields
.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 243
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 244
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 4

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_total:I

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$TotalRow;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$TotalRow;->money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x1

    .line 249
    invoke-interface {v1, v2, p1, v3}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->total(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 248
    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-void
.end method
