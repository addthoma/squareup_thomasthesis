.class Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CartTaxesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->lambda$updateActionBar$0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$2;->this$0:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$2;->this$0:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->access$000(Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;)Lcom/squareup/ui/cart/CartFeesModel$Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->resetTaxes()V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$2;->this$0:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->update()V

    return-void
.end method
