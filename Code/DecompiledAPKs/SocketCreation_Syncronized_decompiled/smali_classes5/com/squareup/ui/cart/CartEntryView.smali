.class public Lcom/squareup/ui/cart/CartEntryView;
.super Landroid/widget/RelativeLayout;
.source "CartEntryView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0014J\u0006\u0010\'\u001a\u00020$J0\u0010(\u001a\u00020$2\u0006\u0010)\u001a\u00020\u000e2\u0006\u0010*\u001a\u00020\u00102\u0006\u0010+\u001a\u00020\u00102\u0006\u0010,\u001a\u00020\u00102\u0006\u0010-\u001a\u00020\u0010H\u0014J\u0018\u0010.\u001a\u00020$2\u0006\u0010/\u001a\u00020\u00102\u0006\u00100\u001a\u00020\u0010H\u0014J\u0010\u00101\u001a\u00020$2\u0006\u00102\u001a\u000203H\u0016J\u0006\u00104\u001a\u00020$J\u0006\u00105\u001a\u00020$J\u0006\u00106\u001a\u00020$J\u0006\u00107\u001a\u00020$J\u0012\u00108\u001a\u00020$2\u0008\u00102\u001a\u0004\u0018\u000109H\u0002J\u0010\u0010:\u001a\u00020$2\u0006\u00102\u001a\u00020;H\u0002J\u0012\u0010<\u001a\u00020$2\u0008\u00102\u001a\u0004\u0018\u00010=H\u0002J\u0012\u0010>\u001a\u00020$2\u0008\u00102\u001a\u0004\u0018\u00010?H\u0002R\u0014\u0010\u0007\u001a\u00020\u0008X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u00020\u0010X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0010X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0012R\u0014\u0010\u0015\u001a\u00020\u0010X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0012R\u0014\u0010\u0017\u001a\u00020\u0018X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001eR\u0014\u0010\u001f\u001a\u00020 X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryView;",
        "Landroid/widget/RelativeLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "amountField",
        "Lcom/squareup/marin/widgets/MarinGlyphTextView;",
        "getAmountField",
        "()Lcom/squareup/marin/widgets/MarinGlyphTextView;",
        "backgroundPaint",
        "Landroid/graphics/Paint;",
        "drawHalfGutterBlocks",
        "",
        "marinGapMedium",
        "",
        "getMarinGapMedium",
        "()I",
        "marinGapMultiline",
        "getMarinGapMultiline",
        "marinMinHeight",
        "getMarinMinHeight",
        "nameAndQuantityField",
        "Lcom/squareup/widgets/PreservedLabelView;",
        "getNameAndQuantityField",
        "()Lcom/squareup/widgets/PreservedLabelView;",
        "regularTextPaint",
        "Landroid/text/TextPaint;",
        "getRegularTextPaint",
        "()Landroid/text/TextPaint;",
        "subLabelField",
        "Landroid/widget/TextView;",
        "getSubLabelField",
        "()Landroid/widget/TextView;",
        "dispatchDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "fadeIn",
        "onLayout",
        "changed",
        "l",
        "t",
        "r",
        "b",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "present",
        "model",
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "pulseAmount",
        "pulsePreservedLabel",
        "pulseTitle",
        "reset",
        "updateAmount",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Amount;",
        "updateName",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Name;",
        "updateQuantity",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;",
        "updateSubLabel",
        "Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private final backgroundPaint:Landroid/graphics/Paint;

.field private final drawHalfGutterBlocks:Z

.field private final marinGapMedium:I

.field private final marinGapMultiline:I

.field private final marinMinHeight:I

.field private final nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

.field private final subLabelField:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 63
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->setClipChildren(Z)V

    .line 64
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->setClipToPadding(Z)V

    .line 66
    sget v1, Lcom/squareup/checkout/R$layout;->cart_entry_row_contents:I

    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-static {p1, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 67
    move-object v1, p0

    check-cast v1, Landroid/view/View;

    sget v2, Lcom/squareup/checkout/R$id;->cart_entry_row_name_and_quantity:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/widgets/PreservedLabelView;

    iput-object v2, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    .line 68
    sget v2, Lcom/squareup/checkout/R$id;->cart_entry_row_amount:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v2, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 69
    sget v2, Lcom/squareup/checkout/R$id;->cart_entry_row_sub_label:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    .line 71
    sget-object v1, Lcom/squareup/checkout/R$styleable;->CartEntryView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 72
    sget p2, Lcom/squareup/checkout/R$styleable;->CartEntryView_halfGutterBlocks:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/cart/CartEntryView;->drawHalfGutterBlocks:Z

    .line 73
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMedium:I

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 77
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_multiline_padding:I

    .line 76
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMultiline:I

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/cart/CartEntryView;->marinMinHeight:I

    .line 81
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->backgroundPaint:Landroid/graphics/Paint;

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$color;->marin_window_background:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public static final synthetic access$getRegularTextPaint$p(Lcom/squareup/ui/cart/CartEntryView;)Landroid/text/TextPaint;
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartEntryView;->getRegularTextPaint()Landroid/text/TextPaint;

    move-result-object p0

    return-object p0
.end method

.method private final getRegularTextPaint()Landroid/text/TextPaint;
    .locals 2

    .line 58
    new-instance v0, Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    check-cast v1, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    return-object v0
.end method

.method private final updateAmount(Lcom/squareup/ui/cart/CartEntryViewModel$Amount;)V
    .locals 3

    if-nez p1, :cond_0

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getColor()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    .line 268
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    if-nez v0, :cond_1

    .line 269
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeAllGlyphs()V

    goto :goto_0

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->getGlyphColor()Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, v2, v1, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V

    :goto_0
    return-void
.end method

.method private final updateName(Lcom/squareup/ui/cart/CartEntryViewModel$Name;)V
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->getLabel()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->getColor()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "nameAndQuantityField.titleView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->getEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->getEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setEnabled(Z)V

    return-void
.end method

.method private final updateQuantity(Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;)V
    .locals 2

    if-nez p1, :cond_0

    .line 248
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->hidePreservedLabel()V

    .line 249
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->showPreservedLabel()V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;->getLabel()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;->getColor()Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V

    :goto_0
    return-void
.end method

.method private final updateSubLabel(Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V
    .locals 3

    if-nez p1, :cond_0

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;->getEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;->getLabelSize()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 285
    new-instance v0, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/cart/CartEntryView$updateSubLabel$1;-><init>(Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    check-cast v0, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 184
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryView;->drawHalfGutterBlocks:Z

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 186
    iget v0, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMedium:I

    int-to-float v4, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/ui/cart/CartEntryView;->backgroundPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 188
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMedium:I

    sub-int/2addr v0, v1

    int-to-float v2, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v4, v0

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v5, v0

    .line 190
    iget-object v6, p0, Lcom/squareup/ui/cart/CartEntryView;->backgroundPaint:Landroid/graphics/Paint;

    move-object v1, p1

    .line 187
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public final fadeIn()V
    .locals 3

    const/4 v0, 0x0

    .line 204
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->setVisibility(I)V

    .line 205
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0xc8

    .line 206
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 207
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    check-cast v1, Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 208
    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method protected final getAmountField()Lcom/squareup/marin/widgets/MarinGlyphTextView;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    return-object v0
.end method

.method protected final getMarinGapMedium()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMedium:I

    return v0
.end method

.method protected final getMarinGapMultiline()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMultiline:I

    return v0
.end method

.method protected final getMarinMinHeight()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/ui/cart/CartEntryView;->marinMinHeight:I

    return v0
.end method

.method protected final getNameAndQuantityField()Lcom/squareup/widgets/PreservedLabelView;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    return-object v0
.end method

.method protected final getSubLabelField()Landroid/widget/TextView;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .line 143
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getMeasuredHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    .line 145
    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getVisibility()I

    move-result p2

    const/16 p3, 0x8

    if-eq p2, p3, :cond_0

    .line 146
    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p2}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p2

    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {p3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p3

    add-int/2addr p2, p3

    .line 147
    div-int/lit8 p2, p2, 0x2

    .line 149
    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    .line 150
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result p4

    sub-int p5, p1, p2

    .line 152
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v1}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v1}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p5

    .line 149
    invoke-virtual {p3, p4, p5, v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->layout(IIII)V

    .line 156
    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result p4

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr p5, v0

    .line 159
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr p1, p2

    .line 156
    invoke-virtual {p3, p4, p5, v0, p1}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0

    .line 163
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result p3

    .line 165
    iget-object p4, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p4}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p4

    div-int/lit8 p4, p4, 0x2

    sub-int p4, p1, p4

    .line 166
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result p5

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr p5, v0

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p1, v0

    .line 163
    invoke-virtual {p2, p3, p4, p5, p1}, Lcom/squareup/widgets/PreservedLabelView;->layout(IIII)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2, p2, p2, p2}, Landroid/widget/TextView;->layout(IIII)V

    .line 174
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 175
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingRight()I

    move-result p3

    sub-int/2addr p2, p3

    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredWidth()I

    move-result p3

    sub-int/2addr p2, p3

    .line 176
    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p3}, Lcom/squareup/widgets/PreservedLabelView;->getTop()I

    move-result p3

    .line 177
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getMeasuredWidth()I

    move-result p4

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingRight()I

    move-result p5

    sub-int/2addr p4, p5

    .line 178
    iget-object p5, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p5}, Lcom/squareup/widgets/PreservedLabelView;->getBottom()I

    move-result p5

    .line 174
    invoke-virtual {p1, p2, p3, p4, p5}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .line 89
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 92
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, 0x0

    invoke-static {p1, v3, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p1

    .line 93
    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v3, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    .line 91
    invoke-virtual {v1, p1, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->measure(II)V

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingLeft()I

    move-result p1

    sub-int p1, v0, p1

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    .line 102
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, p1, v1

    iget v2, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMedium:I

    sub-int/2addr v1, v2

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 104
    iget-object v4, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    .line 105
    invoke-virtual {v4}, Lcom/squareup/widgets/PreservedLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1, v3, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 107
    iget-object v5, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v5}, Lcom/squareup/widgets/PreservedLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 106
    invoke-static {p2, v3, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v5

    .line 104
    invoke-virtual {v4, v1, v5}, Lcom/squareup/widgets/PreservedLabelView;->measure(II)V

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_0

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    .line 115
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 118
    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 116
    invoke-static {p2, v3, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p2

    .line 114
    invoke-virtual {v1, p1, p2}, Landroid/widget/TextView;->measure(II)V

    goto :goto_0

    .line 122
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    const/high16 p2, 0x40000000    # 2.0f

    invoke-static {v3, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {p1, v1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 125
    :goto_0
    iget p1, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMultiline:I

    .line 126
    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p2}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p2

    add-int/2addr p1, p2

    .line 127
    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryView;->subLabelField:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p2

    add-int/2addr p1, p2

    .line 128
    iget p2, p0, Lcom/squareup/ui/cart/CartEntryView;->marinGapMultiline:I

    add-int/2addr p1, p2

    .line 130
    iget p2, p0, Lcom/squareup/ui/cart/CartEntryView;->marinMinHeight:I

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 133
    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->setMeasuredDimension(II)V

    return-void
.end method

.method public present(Lcom/squareup/ui/cart/CartEntryViewModel;)V
    .locals 1

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel;->getName()Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->updateName(Lcom/squareup/ui/cart/CartEntryViewModel$Name;)V

    .line 198
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel;->getQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->updateQuantity(Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;)V

    .line 199
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel;->getAmount()Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->updateAmount(Lcom/squareup/ui/cart/CartEntryViewModel$Amount;)V

    .line 200
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModel;->getSubLabel()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartEntryView;->updateSubLabel(Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-void
.end method

.method public final pulseAmount()V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->clearAnimation()V

    .line 227
    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final pulsePreservedLabel()V
    .locals 2

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getPreservedLabelView()Landroid/widget/TextView;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 221
    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    .line 222
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final pulseTitle()V
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 214
    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    .line 215
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final reset()V
    .locals 1

    const/4 v0, 0x0

    .line 232
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartEntryView;->setVisibility(I)V

    .line 233
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryView;->clearAnimation()V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->amountField:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->clearAnimation()V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryView;->nameAndQuantityField:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getPreservedLabelView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    return-void
.end method
