.class Lcom/squareup/ui/cart/CartSwipeController;
.super Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;
.source "CartSwipeController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;
    }
.end annotation


# instance fields
.field private currentButton:Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

.field private currentPosition:I

.field private final deleteTextBounds:Landroid/graphics/Rect;

.field private final deleteTextPaint:Landroid/text/TextPaint;

.field private final gestureDetector:Landroid/view/GestureDetector;

.field private onTouchListener:Landroid/view/View$OnTouchListener;

.field private final presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private previousPosition:I

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method private constructor <init>(IILandroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V
    .locals 1

    .line 57
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;-><init>(II)V

    .line 43
    new-instance p1, Landroid/text/TextPaint;

    const/16 p2, 0x81

    invoke-direct {p1, p2}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    .line 45
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextBounds:Landroid/graphics/Rect;

    const/4 p1, 0x0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentButton:Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    const/4 p1, -0x1

    .line 48
    iput p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentPosition:I

    .line 49
    iput p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->previousPosition:I

    .line 92
    new-instance p1, Lcom/squareup/ui/cart/CartSwipeController$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/cart/CartSwipeController$2;-><init>(Lcom/squareup/ui/cart/CartSwipeController;)V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->onTouchListener:Landroid/view/View$OnTouchListener;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/cart/CartSwipeController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/cart/CartSwipeController;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 60
    new-instance p1, Landroid/view/GestureDetector;

    .line 61
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance p4, Lcom/squareup/ui/cart/CartSwipeController$1;

    invoke-direct {p4, p0, p3}, Lcom/squareup/ui/cart/CartSwipeController$1;-><init>(Lcom/squareup/ui/cart/CartSwipeController;Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-direct {p1, p2, p4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->gestureDetector:Landroid/view/GestureDetector;

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p3, p1}, Landroidx/recyclerview/widget/RecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 81
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 83
    sget p3, Lcom/squareup/common/strings/R$string;->delete:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 84
    iget-object p4, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, v0}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 85
    iget-object p4, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p4, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    sget p4, Lcom/squareup/marin/R$dimen;->marin_text_default:I

    .line 87
    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float p2, p2

    .line 86
    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    sget-object p2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    iget-object p4, p0, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextBounds:Landroid/graphics/Rect;

    const/4 v0, 0x0

    invoke-virtual {p1, p3, v0, p2, p4}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    return-void
.end method

.method constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x4

    .line 52
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/squareup/ui/cart/CartSwipeController;-><init>(IILandroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/CartSwipeController;)Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentButton:Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/cart/CartSwipeController;)I
    .locals 0

    .line 37
    iget p0, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentPosition:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/cart/CartSwipeController;)Landroid/view/GestureDetector;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/cart/CartSwipeController;->gestureDetector:Landroid/view/GestureDetector;

    return-object p0
.end method


# virtual methods
.method public getSwipeEscapeVelocity(F)F
    .locals 1

    const v0, 0x3dcccccd    # 0.1f

    mul-float p1, p1, v0

    return p1
.end method

.method public onChildDraw(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;FFIZ)V
    .locals 17

    move-object/from16 v8, p0

    move-object/from16 v3, p3

    move/from16 v0, p4

    .line 123
    instance-of v1, v3, Lcom/squareup/ui/cart/CartViewHolder;

    const-string/jumbo v2, "viewHolder expected to be CartViewHolder"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 125
    invoke-virtual/range {p3 .. p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    if-gez v1, :cond_0

    .line 127
    iput v1, v8, Lcom/squareup/ui/cart/CartSwipeController;->currentPosition:I

    const/4 v0, 0x0

    .line 128
    iput-object v0, v8, Lcom/squareup/ui/cart/CartSwipeController;->currentButton:Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    return-void

    .line 132
    :cond_0
    move-object v1, v3

    check-cast v1, Lcom/squareup/ui/cart/CartViewHolder;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartViewHolder;->shouldAllowSwipeToDelete()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    move/from16 v6, p6

    if-ne v6, v1, :cond_1

    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 136
    iget-object v14, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 137
    invoke-virtual {v14}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 139
    sget v2, Lcom/squareup/orderentry/R$dimen;->cart_swipe_controller_delete_button_padding:I

    .line 140
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 141
    sget v4, Lcom/squareup/marin/R$dimen;->marin_cart_button_width:I

    .line 142
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v2, v2, 0x2

    .line 144
    iget-object v4, v8, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextBounds:Landroid/graphics/Rect;

    .line 145
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v2, v4

    .line 144
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    neg-float v0, v0

    int-to-float v1, v1

    .line 149
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    neg-float v0, v0

    .line 152
    new-instance v1, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    iget-object v10, v8, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextPaint:Landroid/text/TextPaint;

    iget-object v11, v8, Lcom/squareup/ui/cart/CartSwipeController;->deleteTextBounds:Landroid/graphics/Rect;

    iget-object v12, v8, Lcom/squareup/ui/cart/CartSwipeController;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 153
    invoke-virtual/range {p3 .. p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v16

    move-object v9, v1

    move-object/from16 v13, p1

    move v15, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;-><init>(Landroid/text/TextPaint;Landroid/graphics/Rect;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Landroid/graphics/Canvas;Landroid/view/View;FI)V

    iput-object v1, v8, Lcom/squareup/ui/cart/CartSwipeController;->currentButton:Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    :cond_1
    move v4, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    .line 156
    invoke-super/range {v0 .. v7}, Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;->onChildDraw(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;FFIZ)V

    :cond_2
    return-void
.end method

.method public onMove(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onSwiped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 113
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartSwipeController;->recoverPreviousSwipe(I)V

    return-void
.end method

.method recoverPreviousSwipe(I)V
    .locals 1

    .line 163
    iget v0, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentPosition:I

    if-ne v0, p1, :cond_0

    return-void

    .line 165
    :cond_0
    iput v0, p0, Lcom/squareup/ui/cart/CartSwipeController;->previousPosition:I

    .line 166
    iput p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentPosition:I

    const/4 p1, 0x0

    .line 167
    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->currentButton:Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    .line 169
    iget p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->previousPosition:I

    const/4 v0, -0x1

    if-le p1, v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/cart/CartSwipeController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 171
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    .line 172
    instance-of v0, p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    if-eqz v0, :cond_1

    .line 174
    check-cast p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->recoveringSwipe:Z

    .line 177
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object p1

    iget v0, p0, Lcom/squareup/ui/cart/CartSwipeController;->previousPosition:I

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_2
    return-void
.end method
