.class public Lcom/squareup/ui/cart/menu/CartMenuArrowButton;
.super Lcom/squareup/marin/widgets/MarinVerticalCaretView;
.source "CartMenuArrowButton.java"


# instance fields
.field presenter:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/ui/cart/menu/CartMenuArrowButton;)V

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->cart_menu_drop_down_button_content_description:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 22
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->presenter:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->dropView(Lcom/squareup/ui/cart/menu/CartMenuArrowButton;)V

    .line 38
    invoke-super {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 27
    invoke-super {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->onFinishInflate()V

    .line 28
    new-instance v0, Lcom/squareup/ui/cart/menu/CartMenuArrowButton$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton$1;-><init>(Lcom/squareup/ui/cart/menu/CartMenuArrowButton;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->presenter:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
