.class public final Lcom/squareup/ui/cart/CartDiscountsScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "CartDiscountsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/cart/CartDiscountsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartDiscountsScreen$Component;,
        Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/cart/CartDiscountsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/cart/CartDiscountsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/cart/CartDiscountsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/cart/CartDiscountsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/cart/CartDiscountsScreen;->INSTANCE:Lcom/squareup/ui/cart/CartDiscountsScreen;

    .line 98
    sget-object v0, Lcom/squareup/ui/cart/CartDiscountsScreen;->INSTANCE:Lcom/squareup/ui/cart/CartDiscountsScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/cart/CartDiscountsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 37
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 33
    sget v0, Lcom/squareup/orderentry/R$layout;->cart_discounts_view:I

    return v0
.end method
