.class public Lcom/squareup/ui/cart/CartEntryViewsFactory;
.super Ljava/lang/Object;
.source "CartEntryViewsFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCartEntryViewsFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CartEntryViewsFactory.kt\ncom/squareup/ui/cart/CartEntryViewsFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,260:1\n1360#2:261\n1429#2,3:262\n1360#2:265\n1429#2,3:266\n704#2:269\n777#2,2:270\n1587#2,3:272\n1360#2:275\n1429#2,3:276\n1360#2:279\n1429#2,3:280\n*E\n*S KotlinDebug\n*F\n+ 1 CartEntryViewsFactory.kt\ncom/squareup/ui/cart/CartEntryViewsFactory\n*L\n128#1:261\n128#1,3:262\n146#1:265\n146#1,3:266\n163#1:269\n163#1,2:270\n164#1,3:272\n179#1:275\n179#1,3:276\n236#1:279\n236#1,3:280\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0016\u0018\u00002\u00020\u0001:\u0001%B+\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\u001c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000fH\u0002J \u0010\u0016\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000fH\u0002J\u0016\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0018\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u001c\u001a\u00020\u001dJ\u0010\u0010\u001e\u001a\u00020\u001f2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010!\u001a\u00020\"2\u0006\u0010\u001c\u001a\u00020\u001dH\u0014J\u0018\u0010#\u001a\u0004\u0018\u00010\"*\u0004\u0018\u00010\u00102\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0014\u0010$\u001a\u00020\u001b*\u00020\u001f2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J \u0010$\u001a\u0008\u0012\u0004\u0012\u00020\"0\u000f*\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002R\u0016\u0010\u0002\u001a\u00020\u00038\u0005X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u000bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
        "",
        "cartEntryViewModelFactory",
        "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "inflater",
        "Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;",
        "(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;)V",
        "()Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "addDiscounts",
        "",
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "addOrderItems",
        "cartItems",
        "Lcom/squareup/checkout/CartItem;",
        "addSubTotal",
        "addTaxes",
        "addTip",
        "addTotal",
        "buildCartEntries",
        "Lcom/squareup/ui/cart/CartEntryViews;",
        "container",
        "Landroid/view/ViewGroup;",
        "buildCartEntryModel",
        "Lcom/squareup/ui/cart/CartEntryViewModels;",
        "buildCartItems",
        "inflateCartEntryView",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "mapToEntryView",
        "mapToEntryViews",
        "CartEntryViewInflater",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final inflater:Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;)V
    .locals 1
    .param p1    # Lcom/squareup/ui/cart/CartEntryViewModelFactory;
        .annotation runtime Lcom/squareup/ui/cart/CartEntryModule$TwoTone;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;
        .annotation runtime Lcom/squareup/ui/cart/CartEntryModule$Checkout;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cartEntryViewModelFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->inflater:Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;

    return-void
.end method

.method private final addOrderItems(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    .line 146
    check-cast p1, Ljava/lang/Iterable;

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 266
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 267
    move-object v3, v1

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 147
    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v8

    .line 147
    invoke-interface/range {v2 .. v8}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->orderItem(Lcom/squareup/checkout/CartItem;ZLjava/lang/String;ZZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v1

    .line 154
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 268
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final addSubTotal(Lcom/squareup/protos/client/bills/Cart;Ljava/util/List;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/ui/cart/CartEntryViewModel;"
        }
    .end annotation

    .line 162
    check-cast p2, Ljava/lang/Iterable;

    .line 269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 270
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 163
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v3

    xor-int/2addr v2, v3

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 271
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    const-wide/16 v3, 0x0

    .line 273
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 164
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v0

    add-long/2addr v3, v0

    goto :goto_1

    .line 166
    :cond_2
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez p2, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long p2, v3, v0

    if-eqz p2, :cond_4

    .line 167
    :goto_2
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string p2, "cart.amounts.total_money.currency_code"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 168
    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    .line 169
    sget v0, Lcom/squareup/utilities/R$string;->subtotal:I

    .line 168
    invoke-interface {p2, v0, p1, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->subTotal(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    return-object p1

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method private final addTip(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 5

    .line 212
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 213
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    .line 214
    sget v1, Lcom/squareup/utilities/R$string;->tip:I

    .line 215
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    const-string v2, "cart.amounts.tip_money"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 213
    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->tip(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method private final addTotal(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 3

    .line 224
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    .line 226
    sget v1, Lcom/squareup/utilities/R$string;->total:I

    .line 227
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const-string v2, "cart.amounts.total_money"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 225
    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->total(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final buildCartItems(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 236
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    const-string v1, "cart.line_items.itemization"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 279
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 280
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 281
    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization;

    .line 237
    iget-object v2, v4, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v2, :cond_0

    .line 238
    new-instance v2, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v2}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 241
    iget-object v3, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->res:Lcom/squareup/util/Res;

    .line 242
    sget-object v5, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    check-cast v5, Lcom/squareup/checkout/OrderVariationNamer;

    .line 243
    sget-object v6, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v7, p1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v6, v7}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v6

    .line 239
    invoke-virtual {v2, v4, v3, v5, v6}, Lcom/squareup/checkout/CartItem$Builder;->fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 244
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    goto :goto_1

    .line 246
    :cond_0
    new-instance v3, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v3}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 249
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v5

    .line 250
    iget-object v6, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->res:Lcom/squareup/util/Res;

    .line 251
    sget-object v2, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    move-object v7, v2

    check-cast v7, Lcom/squareup/checkout/OrderVariationNamer;

    .line 252
    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v8

    .line 253
    sget-object v2, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v9, p1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v2, v9}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v9

    .line 247
    invoke-virtual/range {v3 .. v9}, Lcom/squareup/checkout/CartItem$Builder;->fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Ljava/util/Map;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 255
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 256
    :goto_1
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 282
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method private final mapToEntryView(Lcom/squareup/ui/cart/CartEntryViewModel;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;
    .locals 0

    if-eqz p1, :cond_0

    .line 135
    invoke-virtual {p0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->inflateCartEntryView(Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p2

    .line 136
    invoke-virtual {p2, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return-object p2
.end method

.method private final mapToEntryViews(Lcom/squareup/ui/cart/CartEntryViewModels;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;
    .locals 9

    .line 108
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getOrderItemModels()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryViews(Ljava/util/List;Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v2

    .line 109
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getSubTotalModel()Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryView(Lcom/squareup/ui/cart/CartEntryViewModel;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v3

    .line 110
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getDiscountModels()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryViews(Ljava/util/List;Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v4

    .line 111
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getTaxModels()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryViews(Ljava/util/List;Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v5

    .line 112
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getTipModel()Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryView(Lcom/squareup/ui/cart/CartEntryViewModel;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v6

    .line 113
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getSurchargeModel()Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryView(Lcom/squareup/ui/cart/CartEntryViewModel;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v7

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels;->getTotalModel()Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryView(Lcom/squareup/ui/cart/CartEntryViewModel;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v8

    .line 116
    new-instance p1, Lcom/squareup/ui/cart/CartEntryViews;

    move-object v1, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/cart/CartEntryViews;-><init>(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryView;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryView;)V

    return-object p1
.end method

.method private final mapToEntryViews(Ljava/util/List;Landroid/view/ViewGroup;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation

    .line 128
    check-cast p1, Ljava/lang/Iterable;

    .line 261
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 262
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 263
    check-cast v1, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 129
    invoke-direct {p0, v1, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryView(Lcom/squareup/ui/cart/CartEntryViewModel;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected addDiscounts(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    const-string v0, "cart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    const-string v0, "cart.line_items.discount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 275
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 276
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 277
    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 180
    iget-object v2, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    if-eqz v2, :cond_0

    .line 181
    iget-object v2, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    goto :goto_1

    .line 183
    :cond_0
    iget-object v2, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object v2, v2, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    .line 186
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    const-string v4, "name"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const-string v4, "it.amounts.applied_money"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    invoke-interface {v3, v2, v1, v4}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->discountItem(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 278
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected addTaxes(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    const-string v0, "cart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 192
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 193
    iget-object v2, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    if-eqz v2, :cond_0

    .line 194
    iget-object v2, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    goto :goto_1

    .line 196
    :cond_0
    iget-object v2, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v2, v2, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    .line 199
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    const-string v4, "name"

    .line 200
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 201
    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const-string v4, "feeLineItem.amounts.applied_money"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    .line 199
    invoke-interface {v3, v2, v1, v4, v4}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->taxes(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v1

    .line 205
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 208
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final buildCartEntries(Lcom/squareup/protos/client/bills/Cart;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;
    .locals 1

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->buildCartEntryModel(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/ui/cart/CartEntryViewModels;

    move-result-object p1

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->mapToEntryViews(Lcom/squareup/ui/cart/CartEntryViewModels;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;

    move-result-object p1

    return-object p1
.end method

.method public final buildCartEntryModel(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/ui/cart/CartEntryViewModels;
    .locals 9

    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 82
    sget-object p1, Lcom/squareup/ui/cart/CartEntryViewModels;->Companion:Lcom/squareup/ui/cart/CartEntryViewModels$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViewModels$Companion;->empty()Lcom/squareup/ui/cart/CartEntryViewModels;

    move-result-object p1

    return-object p1

    .line 85
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->buildCartItems(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;

    move-result-object v0

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->addOrderItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 88
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->addSubTotal(Lcom/squareup/protos/client/bills/Cart;Ljava/util/List;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v3

    .line 89
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->addDiscounts(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;

    move-result-object v4

    .line 90
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->addTaxes(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;

    move-result-object v5

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->addTip(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v6

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->addTotal(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v8

    .line 94
    new-instance p1, Lcom/squareup/ui/cart/CartEntryViewModels;

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/cart/CartEntryViewModels;-><init>(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-object p1
.end method

.method protected final cartEntryViewModelFactory()Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    return-object v0
.end method

.method protected final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method protected inflateCartEntryView(Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryView;
    .locals 2

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewsFactory;->inflater:Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "container.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;->inflate(Landroid/content/Context;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    return-object p1
.end method
