.class Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EmptyCustomAmountViewHolder"
.end annotation


# instance fields
.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;

.field private final parent:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 130
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 131
    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;->parent:Landroid/view/View;

    .line 132
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 3

    .line 136
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$EmptyCustomAmountRow;

    .line 137
    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$EmptyCustomAmountRow;->cartItem:Lcom/squareup/checkout/CartItem;

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;->parent:Landroid/view/View;

    .line 140
    invoke-static {v1}, Lcom/squareup/ui/cart/CartViewHolder;->access$000(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->emptyCustomAmount(Lcom/squareup/checkout/CartItem;IZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-void
.end method
