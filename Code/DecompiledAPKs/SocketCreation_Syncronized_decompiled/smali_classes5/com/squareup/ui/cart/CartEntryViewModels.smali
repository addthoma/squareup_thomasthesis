.class public final Lcom/squareup/ui/cart/CartEntryViewModels;
.super Ljava/lang/Object;
.source "CartEntryViewModels.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartEntryViewModels$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 $2\u00020\u0001:\u0001$BW\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0004\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000bJ\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000f\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003Ji\u0010\u001c\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0004H\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\rR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0010\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewModels;",
        "",
        "orderItemModels",
        "",
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "subTotalModel",
        "discountModels",
        "taxModels",
        "tipModel",
        "surchargeModel",
        "totalModel",
        "(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)V",
        "getDiscountModels",
        "()Ljava/util/List;",
        "getOrderItemModels",
        "getSubTotalModel",
        "()Lcom/squareup/ui/cart/CartEntryViewModel;",
        "getSurchargeModel",
        "getTaxModels",
        "getTipModel",
        "getTotalModel",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/cart/CartEntryViewModels$Companion;


# instance fields
.field private final discountModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private final orderItemModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private final subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

.field private final surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

.field private final taxModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private final tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

.field private final totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModels$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/CartEntryViewModels$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/cart/CartEntryViewModels;->Companion:Lcom/squareup/ui/cart/CartEntryViewModels$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ")V"
        }
    .end annotation

    const-string v0, "orderItemModels"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountModels"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxModels"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iput-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iput-object p6, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iput-object p7, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/cart/CartEntryViewModels;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;ILjava/lang/Object;)Lcom/squareup/ui/cart/CartEntryViewModels;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ui/cart/CartEntryViewModels;->copy(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)Lcom/squareup/ui/cart/CartEntryViewModels;

    move-result-object p0

    return-object p0
.end method

.method public static final empty()Lcom/squareup/ui/cart/CartEntryViewModels;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/cart/CartEntryViewModels;->Companion:Lcom/squareup/ui/cart/CartEntryViewModels$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartEntryViewModels$Companion;->empty()Lcom/squareup/ui/cart/CartEntryViewModels;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final component7()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)Lcom/squareup/ui/cart/CartEntryViewModels;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ")",
            "Lcom/squareup/ui/cart/CartEntryViewModels;"
        }
    .end annotation

    const-string v0, "orderItemModels"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountModels"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxModels"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModels;

    move-object v1, v0

    move-object v3, p2

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/cart/CartEntryViewModels;-><init>(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/CartEntryViewModels;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModels;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDiscountModels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    return-object v0
.end method

.method public final getOrderItemModels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    return-object v0
.end method

.method public final getSubTotalModel()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final getSurchargeModel()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final getTaxModels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    return-object v0
.end method

.method public final getTipModel()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public final getTotalModel()Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CartEntryViewModels(orderItemModels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->orderItemModels:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subTotalModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->subTotalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discountModels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->discountModels:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", taxModels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->taxModels:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->tipModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", surchargeModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->surchargeModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModels;->totalModel:Lcom/squareup/ui/cart/CartEntryViewModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
