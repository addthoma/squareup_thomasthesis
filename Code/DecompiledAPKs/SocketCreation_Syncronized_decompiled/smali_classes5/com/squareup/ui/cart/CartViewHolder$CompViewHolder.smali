.class Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CompViewHolder"
.end annotation


# instance fields
.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 187
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 188
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$CompRow;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$CompRow;->money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->comp(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-void
.end method
