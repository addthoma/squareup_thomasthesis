.class Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;
.super Lcom/squareup/ui/cart/AbstractCartFeesPresenter;
.source "CartDiscountsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartDiscountsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/cart/AbstractCartFeesPresenter<",
        "Lcom/squareup/ui/cart/CartDiscountsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

.field private final cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/DiscountFormatter;Lcom/squareup/log/cart/TransactionInteractionsLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    return-void
.end method


# virtual methods
.method protected executeDeletes()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->executeDiscountDeletes()V

    return-void
.end method

.method goBack()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartScreenRunner;->finishCartDiscounts()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_DISCOUNTS_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    .line 77
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method onStartVisualTransition()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logAllDiscountsEvent(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method protected onUpPressed()V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_DISCOUNTS_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    .line 83
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onUpPressed()V

    return-void
.end method

.method title()Ljava/lang/CharSequence;
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->cart_discounts:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
