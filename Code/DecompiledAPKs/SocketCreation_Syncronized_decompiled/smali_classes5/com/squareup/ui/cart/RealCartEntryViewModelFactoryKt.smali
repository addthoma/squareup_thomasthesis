.class public final Lcom/squareup/ui/cart/RealCartEntryViewModelFactoryKt;
.super Ljava/lang/Object;
.source "RealCartEntryViewModelFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "appearance",
        "Lcom/squareup/ui/cart/Appearance;",
        "Lcom/squareup/checkout/CartItem;",
        "allowGlyph",
        "",
        "checkout_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final appearance(Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/ui/cart/Appearance;
    .locals 1

    const-string v0, "$this$appearance"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 742
    sget-object p0, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    return-object p0

    .line 745
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p0, Lcom/squareup/ui/cart/Appearance;->VOIDED:Lcom/squareup/ui/cart/Appearance;

    goto :goto_0

    .line 746
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p0, Lcom/squareup/ui/cart/Appearance;->COMPED:Lcom/squareup/ui/cart/Appearance;

    goto :goto_0

    .line 747
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->hasDiscountThatCanBeAppliedToOnlyOneItem()Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p0, Lcom/squareup/ui/cart/Appearance;->REWARDED:Lcom/squareup/ui/cart/Appearance;

    goto :goto_0

    .line 748
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->hasPerItemCapableDiscounts()Z

    move-result p0

    if-eqz p0, :cond_4

    sget-object p0, Lcom/squareup/ui/cart/Appearance;->DISCOUNTED:Lcom/squareup/ui/cart/Appearance;

    goto :goto_0

    .line 749
    :cond_4
    sget-object p0, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    :goto_0
    return-object p0
.end method
