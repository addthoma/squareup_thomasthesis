.class Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DiscountViewHolder"
.end annotation


# instance fields
.field private final cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private discountRow:Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 154
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 155
    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 156
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .locals 0

    .line 146
    iget-object p0, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    return-object p0
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 4

    .line 160
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    sget v0, Lcom/squareup/transaction/R$string;->cart_discounts:I

    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

    iget-object v1, v1, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;->money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

    iget-boolean v2, v2, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem:Z

    const/4 v3, 0x1

    .line 162
    invoke-interface {p1, v0, v1, v3, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->discount(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    .line 167
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    new-instance v0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder$1;-><init>(Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method getEntryView()Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-object v0
.end method

.method isDiscountEvent()Z
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

    iget-boolean v0, v0, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;->isDiscountEvent:Z

    return v0
.end method
