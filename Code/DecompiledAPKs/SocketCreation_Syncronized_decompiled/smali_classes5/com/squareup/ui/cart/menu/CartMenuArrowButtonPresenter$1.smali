.class Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter$1;
.super Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;
.source "CartMenuArrowButtonPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter$1;->this$0:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;

    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropDownHiding(Landroid/view/View;)V
    .locals 0

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter$1;->this$0:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;

    invoke-static {p1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->access$100(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->animateArrowDown()V

    return-void
.end method

.method public onDropDownOpening(Landroid/view/View;)V
    .locals 0

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter$1;->this$0:Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;

    invoke-static {p1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;->access$000(Lcom/squareup/ui/cart/menu/CartMenuArrowButtonPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/menu/CartMenuArrowButton;->animateArrowUp()V

    return-void
.end method
