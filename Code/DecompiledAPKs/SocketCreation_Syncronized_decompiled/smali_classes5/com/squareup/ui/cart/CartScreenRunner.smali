.class public Lcom/squareup/ui/cart/CartScreenRunner;
.super Ljava/lang/Object;
.source "CartScreenRunner.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final flow:Lflow/Flow;

.field private final redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/cart/CartScreenRunner;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    .line 22
    iput-object p3, p0, Lcom/squareup/ui/cart/CartScreenRunner;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public finishCartDiscounts()V
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/cart/CartDiscountsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public finishCartTaxes()V
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/cart/CartTaxesScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public goToAddEligibleItemWorkflow(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponBootstrapScreen;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponBootstrapScreen;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToCartDiscounts()V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/cart/CartDiscountsScreen;->INSTANCE:Lcom/squareup/ui/cart/CartDiscountsScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToCartTaxes()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/cart/CartTaxesScreen;->INSTANCE:Lcom/squareup/ui/cart/CartTaxesScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToLoyaltyRedeemPointsScreen()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_OPENED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartScreenRunner;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    invoke-interface {v1}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;->getRedeemPointsScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
