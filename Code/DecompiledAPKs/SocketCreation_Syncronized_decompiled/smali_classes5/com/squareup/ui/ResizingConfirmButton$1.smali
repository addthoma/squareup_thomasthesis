.class Lcom/squareup/ui/ResizingConfirmButton$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ResizingConfirmButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ResizingConfirmButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ResizingConfirmButton;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ResizingConfirmButton;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->access$000(Lcom/squareup/ui/ResizingConfirmButton;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/ResizingConfirmButton;->access$100(Lcom/squareup/ui/ResizingConfirmButton;Z)V

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->access$200(Lcom/squareup/ui/ResizingConfirmButton;)Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->access$200(Lcom/squareup/ui/ResizingConfirmButton;)Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;->onConfirm()V

    goto :goto_0

    .line 98
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->access$300(Lcom/squareup/ui/ResizingConfirmButton;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->access$400(Lcom/squareup/ui/ResizingConfirmButton;)Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton$1;->this$0:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->access$400(Lcom/squareup/ui/ResizingConfirmButton;)Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;->onInitialClick()V

    :cond_1
    :goto_0
    return-void
.end method
