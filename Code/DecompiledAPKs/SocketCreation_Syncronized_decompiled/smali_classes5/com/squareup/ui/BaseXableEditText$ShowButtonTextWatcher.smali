.class Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "BaseXableEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/BaseXableEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowButtonTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/BaseXableEditText;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/BaseXableEditText;)V
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;->this$0:Lcom/squareup/ui/BaseXableEditText;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/BaseXableEditText;Lcom/squareup/ui/BaseXableEditText$1;)V
    .locals 0

    .line 388
    invoke-direct {p0, p1}, Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;-><init>(Lcom/squareup/ui/BaseXableEditText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;->this$0:Lcom/squareup/ui/BaseXableEditText;

    iget-object v1, v0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/BaseXableEditText;->access$800(Lcom/squareup/ui/BaseXableEditText;ZZ)V

    return-void
.end method
