.class public final Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;
.super Ljava/lang/Object;
.source "DuplicateSkuResultController.kt"

# interfaces
.implements Lcom/squareup/sku/DuplicateSkuResultController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sku/DuplicateSkuResultController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoDuplicateSkuResultController"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0002J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J \u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0016\u0010\r\u001a\u00020\u00062\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0016J\u0014\u0010\u0011\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00130\u0012H\u0016J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u0012H\u0016\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;",
        "Lcom/squareup/sku/DuplicateSkuResultController;",
        "()V",
        "noDuplicateSkuResultControllerAccess",
        "",
        "onBackPressedOnDuplicateSkuResultScreen",
        "",
        "onVariationSelectedOnDuplicateSkuResultScreen",
        "itemId",
        "",
        "variationId",
        "type",
        "Lcom/squareup/api/items/Item$Type;",
        "showDuplicateSkuResult",
        "variationsWithDuplicateSku",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "variationSelection",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;",
        "sku_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final noDuplicateSkuResultControllerAccess()Ljava/lang/Throwable;
    .locals 2

    .line 51
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot get the DuplicateSkuResultController without the feature enabled"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public onBackPressedOnDuplicateSkuResultScreen()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;->noDuplicateSkuResultControllerAccess()Ljava/lang/Throwable;

    return-void
.end method

.method public onVariationSelectedOnDuplicateSkuResultScreen(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "variationId"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "type"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;->noDuplicateSkuResultControllerAccess()Ljava/lang/Throwable;

    return-void
.end method

.method public showDuplicateSkuResult(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "variationsWithDuplicateSku"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;->noDuplicateSkuResultControllerAccess()Ljava/lang/Throwable;

    return-void
.end method

.method public variationSelection()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;",
            ">;>;"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;->noDuplicateSkuResultControllerAccess()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->error(Ljava/lang/Throwable;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.error(noDupli\u2026ResultControllerAccess())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public variationsWithDuplicateSku()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;>;"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;->noDuplicateSkuResultControllerAccess()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->error(Ljava/lang/Throwable;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.error(noDupli\u2026ResultControllerAccess())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
