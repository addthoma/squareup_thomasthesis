.class public abstract Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;
.super Ljava/lang/Object;
.source "StatusBarEventManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/statusbar/event/StatusBarEventManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SwitcherState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ShowNothing;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ShowPullDownHandle;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ShowSwitcher;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$Disable;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ConfigureSwitcher;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$HighlightActiveApplet;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u0003\u0004\u0005\u0006\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0006\t\n\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;",
        "",
        "()V",
        "ConfigureSwitcher",
        "Disable",
        "HighlightActiveApplet",
        "ShowNothing",
        "ShowPullDownHandle",
        "ShowSwitcher",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ShowNothing;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ShowPullDownHandle;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ShowSwitcher;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$Disable;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$ConfigureSwitcher;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState$HighlightActiveApplet;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 147
    invoke-direct {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;-><init>()V

    return-void
.end method
