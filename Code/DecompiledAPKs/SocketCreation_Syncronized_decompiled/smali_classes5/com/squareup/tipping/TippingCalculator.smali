.class public Lcom/squareup/tipping/TippingCalculator;
.super Ljava/lang/Object;
.source "TippingCalculator.java"


# instance fields
.field private final remainingBalance:Lcom/squareup/protos/common/Money;

.field private final tipSettings:Lcom/squareup/settings/server/TipSettings;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/TipSettings;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/tipping/TippingCalculator;->remainingBalance:Lcom/squareup/protos/common/Money;

    .line 30
    iput-object p2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/TipSettings;)V
    .locals 1

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, v0, p1}, Lcom/squareup/tipping/TippingCalculator;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/TipSettings;)V

    return-void
.end method

.method private getTipPercentageThreshold()J
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->getSmartTippingThresholdMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private isValidTip(J)Z
    .locals 6

    .line 178
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->remainingBalance:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    return v1

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntryLargestMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_1

    cmp-long v0, p1, v2

    if-gtz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static shouldAskForCustomAmount(Lcom/squareup/settings/server/TipSettings;Lcom/squareup/protos/common/Money;)Z
    .locals 3

    .line 34
    invoke-virtual {p0}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result p0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 35
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-lez v2, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public askForTip(Lcom/squareup/protos/common/Money;Z)Z
    .locals 6

    .line 44
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 49
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/tipping/TippingCalculator;->customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 52
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-lez v5, :cond_1

    return v2

    .line 57
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tipping/TippingCalculator;->tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object p1

    .line 58
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    return p1
.end method

.method public customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 109
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntryMaxPercentage()Lcom/squareup/util/Percentage;

    move-result-object v0

    .line 111
    iget-object v1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/Percentage;->percentOf(J)J

    move-result-wide v0

    .line 113
    iget-object v2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntrySmallestMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    move-wide v0, v2

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntryLargestMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    move-wide v0, v2

    .line 123
    :cond_1
    iget-object v2, p0, Lcom/squareup/tipping/TippingCalculator;->remainingBalance:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    .line 124
    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    :cond_2
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_3

    move-wide v0, v2

    .line 130
    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public fillReceiptDisplayDetails(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;Lcom/squareup/protos/common/Money;Z)V
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntryMaxPercentage()Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->remainingBalance:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 148
    invoke-virtual {p0, p2}, Lcom/squareup/tipping/TippingCalculator;->customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/tipping/TippingCalculator;->shouldAskForCustomAmount(Lcom/squareup/settings/server/TipSettings;Lcom/squareup/protos/common/Money;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_custom_amount(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 150
    invoke-virtual {v2}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntryLargestMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v1

    .line 151
    invoke-virtual {v1, v0}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_max_percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 152
    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->getManualTipEntrySmallestMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 153
    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->getCustomPercentageOptions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 155
    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->getSmartTippingUnderThresholdOptions()Ljava/util/List;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 156
    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->getSmartTippingThresholdMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v0

    .line 157
    invoke-virtual {p0, p2, p3}, Lcom/squareup/tipping/TippingCalculator;->tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 158
    invoke-virtual {p3}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result p3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->allow_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object p2

    .line 159
    invoke-virtual {p2}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->build()Lcom/squareup/protos/common/tipping/TippingPreferences;

    move-result-object p2

    .line 149
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences(Lcom/squareup/protos/common/tipping/TippingPreferences;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    return-void
.end method

.method public tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p2, :cond_0

    goto/16 :goto_5

    .line 73
    :cond_0
    iget-object p2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {p2}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomPercentages()Z

    move-result p2

    const/4 v0, 0x1

    xor-int/2addr p2, v0

    .line 75
    iget-object v1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x0

    if-eqz p2, :cond_1

    .line 76
    invoke-direct {p0}, Lcom/squareup/tipping/TippingCalculator;->getTipPercentageThreshold()J

    move-result-wide v4

    cmp-long p2, v1, v4

    if-gez p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 78
    iget-object p2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 79
    invoke-virtual {p2}, Lcom/squareup/settings/server/TipSettings;->getSmartTippingUnderThresholdOptions()Ljava/util/List;

    move-result-object p2

    goto :goto_1

    :cond_2
    iget-object p2, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 80
    invoke-virtual {p2}, Lcom/squareup/settings/server/TipSettings;->getCustomPercentageOptions()Ljava/util/List;

    move-result-object p2

    .line 81
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    .line 83
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_6

    if-eqz v0, :cond_3

    .line 85
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/common/tipping/TipOption;

    iget-object v6, v6, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v6, v6, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto :goto_3

    .line 86
    :cond_3
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/common/tipping/TipOption;

    iget-object v6, v6, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Lcom/squareup/util/Percentage;->percentOf(J)J

    move-result-wide v6

    .line 88
    :goto_3
    invoke-direct {p0, v6, v7}, Lcom/squareup/tipping/TippingCalculator;->isValidTip(J)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 89
    iget-object v8, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v6, v7, v8}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 92
    new-instance v7, Lcom/squareup/protos/common/tipping/TipOption$Builder;

    invoke-direct {v7}, Lcom/squareup/protos/common/tipping/TipOption$Builder;-><init>()V

    .line 93
    invoke-virtual {v7, v6}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object v6

    if-eqz v0, :cond_4

    const/4 v7, 0x0

    goto :goto_4

    .line 94
    :cond_4
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/common/tipping/TipOption;

    iget-object v7, v7, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    :goto_4
    invoke-virtual {v6, v7}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object v6

    .line 95
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object v6

    .line 96
    invoke-virtual {v6}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->build()Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v6

    .line 92
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_6
    return-object v4

    .line 70
    :cond_7
    :goto_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/squareup/tipping/TippingCalculator;->remainingBalance:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Lcom/squareup/tipping/TippingCalculator;

    iget-object v1, p0, Lcom/squareup/tipping/TippingCalculator;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-direct {v0, p1, v1}, Lcom/squareup/tipping/TippingCalculator;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/TipSettings;)V

    return-object v0

    :cond_0
    return-object p0
.end method
