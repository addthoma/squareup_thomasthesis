.class public final Lcom/squareup/squarewave/util/Base64;
.super Ljava/lang/Object;
.source "Base64.java"


# static fields
.field private static final BASE64_CHARS:Ljava/lang/String; = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendTriple([CII)V
    .locals 2

    shr-int/lit8 v0, p2, 0x12

    and-int/lit8 v0, v0, 0x3f

    .line 97
    invoke-static {v0}, Lcom/squareup/squarewave/util/Base64;->lookup(I)C

    move-result v0

    aput-char v0, p0, p1

    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0xc

    and-int/lit8 v1, v1, 0x3f

    .line 98
    invoke-static {v1}, Lcom/squareup/squarewave/util/Base64;->lookup(I)C

    move-result v1

    aput-char v1, p0, v0

    add-int/lit8 v0, p1, 0x2

    shr-int/lit8 v1, p2, 0x6

    and-int/lit8 v1, v1, 0x3f

    .line 99
    invoke-static {v1}, Lcom/squareup/squarewave/util/Base64;->lookup(I)C

    move-result v1

    aput-char v1, p0, v0

    add-int/lit8 p1, p1, 0x3

    and-int/lit8 p2, p2, 0x3f

    .line 100
    invoke-static {p2}, Lcom/squareup/squarewave/util/Base64;->lookup(I)C

    move-result p2

    aput-char p2, p0, p1

    return-void
.end method

.method public static encode(Ljava/nio/ByteBuffer;)[C
    .locals 7

    .line 61
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 62
    div-int/lit8 v1, v0, 0x3

    .line 66
    rem-int/lit8 v2, v0, 0x3

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    mul-int/lit8 v2, v1, 0x4

    .line 69
    new-array v2, v2, [C

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    .line 72
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    add-int/lit8 v6, v3, 0x1

    if-ge v6, v0, :cond_1

    .line 73
    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    :cond_1
    add-int/lit8 v6, v3, 0x2

    if-ge v6, v0, :cond_2

    .line 74
    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    .line 75
    :cond_2
    invoke-static {v2, v4, v5}, Lcom/squareup/squarewave/util/Base64;->appendTriple([CII)V

    add-int/lit8 v3, v3, 0x3

    add-int/lit8 v4, v4, 0x4

    goto :goto_0

    :cond_3
    mul-int/lit8 v1, v1, 0x3

    sub-int/2addr v1, v0

    const/16 p0, 0x3d

    .line 80
    invoke-static {v2, v1, p0}, Lcom/squareup/squarewave/util/Base64;->setPadding([CIC)V

    return-object v2
.end method

.method public static encode([B)[C
    .locals 7

    .line 28
    array-length v0, p0

    div-int/lit8 v0, v0, 0x3

    .line 32
    array-length v1, p0

    rem-int/lit8 v1, v1, 0x3

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    mul-int/lit8 v1, v0, 0x4

    .line 35
    new-array v1, v1, [C

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 37
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_3

    .line 38
    aget-byte v4, p0, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    add-int/lit8 v5, v2, 0x1

    .line 39
    array-length v6, p0

    if-ge v5, v6, :cond_1

    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    :cond_1
    add-int/lit8 v5, v2, 0x2

    .line 40
    array-length v6, p0

    if-ge v5, v6, :cond_2

    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    .line 41
    :cond_2
    invoke-static {v1, v3, v4}, Lcom/squareup/squarewave/util/Base64;->appendTriple([CII)V

    add-int/lit8 v2, v2, 0x3

    add-int/lit8 v3, v3, 0x4

    goto :goto_0

    :cond_3
    mul-int/lit8 v0, v0, 0x3

    .line 45
    array-length p0, p0

    sub-int/2addr v0, p0

    const/16 p0, 0x3d

    .line 46
    invoke-static {v1, v0, p0}, Lcom/squareup/squarewave/util/Base64;->setPadding([CIC)V

    return-object v1
.end method

.method private static lookup(I)C
    .locals 1

    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    .line 104
    invoke-virtual {v0, p0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    return p0
.end method

.method private static setPadding([CIC)V
    .locals 1

    .line 87
    array-length v0, p0

    sub-int/2addr v0, p1

    :goto_0
    array-length p1, p0

    if-ge v0, p1, :cond_0

    .line 88
    aput-char p2, p0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
