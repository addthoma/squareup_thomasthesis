.class public Lcom/squareup/squarewave/o1/O1SignalDecoder;
.super Ljava/lang/Object;
.source "O1SignalDecoder.java"

# interfaces
.implements Lcom/squareup/squarewave/SignalDecoder;


# instance fields
.field private final pipeline:Lcom/squareup/squarewave/o1/O1SwipeFilter;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/squareup/squarewave/o1/O1NoiseFilter;

    new-instance v1, Lcom/squareup/squarewave/o1/O1DerivativeFilter;

    new-instance v2, Lcom/squareup/squarewave/o1/O1BinaryFilter;

    new-instance v3, Lcom/squareup/squarewave/o1/O1Decoder;

    invoke-direct {v3}, Lcom/squareup/squarewave/o1/O1Decoder;-><init>()V

    invoke-direct {v2, v3}, Lcom/squareup/squarewave/o1/O1BinaryFilter;-><init>(Lcom/squareup/squarewave/o1/O1SwipeFilter;)V

    invoke-direct {v1, v2}, Lcom/squareup/squarewave/o1/O1DerivativeFilter;-><init>(Lcom/squareup/squarewave/o1/O1SwipeFilter;)V

    invoke-direct {v0, v1}, Lcom/squareup/squarewave/o1/O1NoiseFilter;-><init>(Lcom/squareup/squarewave/o1/O1SwipeFilter;)V

    iput-object v0, p0, Lcom/squareup/squarewave/o1/O1SignalDecoder;->pipeline:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    return-void
.end method

.method private addDemodInfo(Lcom/squareup/squarewave/Signal;Lcom/squareup/squarewave/o1/O1DemodResult;I)V
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;-><init>()V

    .line 56
    iget-object p2, p2, Lcom/squareup/squarewave/o1/O1DemodResult;->detailedResult:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    iput-object p2, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 57
    new-instance p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;-><init>()V

    .line 58
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    iput-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 59
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    .line 60
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    .line 61
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private clearSwipe(Lcom/squareup/squarewave/o1/O1Swipe;)V
    .locals 0

    .line 66
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->zeroize()V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 6

    .line 43
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 44
    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->fromSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object v2

    .line 45
    iget-object v3, p0, Lcom/squareup/squarewave/o1/O1SignalDecoder;->pipeline:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    invoke-interface {v3, v2}, Lcom/squareup/squarewave/o1/O1SwipeFilter;->filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object v3

    .line 46
    invoke-direct {p0, v2}, Lcom/squareup/squarewave/o1/O1SignalDecoder;->clearSwipe(Lcom/squareup/squarewave/o1/O1Swipe;)V

    .line 47
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/16 v0, 0x3e8

    div-long/2addr v4, v0

    long-to-int v0, v4

    .line 49
    invoke-direct {p0, p1, v3, v0}, Lcom/squareup/squarewave/o1/O1SignalDecoder;->addDemodInfo(Lcom/squareup/squarewave/Signal;Lcom/squareup/squarewave/o1/O1DemodResult;I)V

    .line 51
    iget-object p1, v3, Lcom/squareup/squarewave/o1/O1DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    return-object p1
.end method
