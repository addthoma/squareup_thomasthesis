.class final Lcom/squareup/squarewave/o1/O1Decoder$Message;
.super Ljava/lang/Object;
.source "O1Decoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/o1/O1Decoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Message"
.end annotation


# instance fields
.field final authenticatedLength:I

.field final brand:Lcom/squareup/Card$Brand;

.field final counter:I

.field final encryptedBody:[B

.field final endPeriod:I

.field final entropy:I

.field final error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field final hardwareId:Ljava/lang/String;

.field final last4OfPan:[C

.field final length:I

.field final resets:I

.field final startPeriod:I

.field final status:I

.field final swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

.field final wakeups:I


# direct methods
.method private constructor <init>(IIILjava/lang/String;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;ILcom/squareup/Card$Brand;[C[BIIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)V
    .locals 0

    .line 343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344
    iput p1, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->length:I

    .line 345
    iput p2, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->counter:I

    .line 346
    iput p3, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->entropy:I

    .line 347
    iput-object p4, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->hardwareId:Ljava/lang/String;

    .line 348
    iput p6, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->authenticatedLength:I

    .line 349
    iput-object p5, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 350
    iput-object p7, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->brand:Lcom/squareup/Card$Brand;

    .line 351
    iput-object p8, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->last4OfPan:[C

    .line 352
    iput-object p9, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->encryptedBody:[B

    .line 353
    iput p10, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->resets:I

    .line 354
    iput p11, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->wakeups:I

    .line 355
    iput p12, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->status:I

    .line 356
    iput p13, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->startPeriod:I

    .line 357
    iput p14, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->endPeriod:I

    .line 358
    iput-object p15, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-void
.end method

.method private static directionFromByte(B)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;
    .locals 1

    if-nez p0, :cond_0

    .line 466
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0

    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 469
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0

    .line 471
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->UNKNOWN_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0
.end method

.method public static error(ILjava/lang/String;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;IIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)Lcom/squareup/squarewave/o1/O1Decoder$Message;
    .locals 17

    .line 318
    new-instance v16, Lcom/squareup/squarewave/o1/O1Decoder$Message;

    const v2, 0xffffff

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v16

    move/from16 v1, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    move-object/from16 v15, p8

    invoke-direct/range {v0 .. v15}, Lcom/squareup/squarewave/o1/O1Decoder$Message;-><init>(IIILjava/lang/String;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;ILcom/squareup/Card$Brand;[C[BIIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)V

    return-object v16
.end method

.method private static errorCodeFromByte(B)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;
    .locals 1

    int-to-char p0, p0

    const/16 v0, 0x21

    if-eq p0, v0, :cond_8

    const/16 v0, 0x23

    if-eq p0, v0, :cond_7

    const/16 v0, 0x25

    if-eq p0, v0, :cond_6

    const/16 v0, 0x3c

    if-eq p0, v0, :cond_5

    const/16 v0, 0x70

    if-eq p0, v0, :cond_4

    const/16 v0, 0x78

    if-eq p0, v0, :cond_3

    const/16 v0, 0x7a

    if-eq p0, v0, :cond_2

    const/16 v0, 0x3e

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3f

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 448
    :cond_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 442
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 456
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->NO_LEADING_ZEROS_DETECTED:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 458
    :cond_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->OTHER_INTERNAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 450
    :cond_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_PARITY_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 444
    :cond_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 446
    :cond_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 454
    :cond_7
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->SWIPE_COUNTER_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0

    .line 452
    :cond_8
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0
.end method

.method static fromBytes([B)Lcom/squareup/squarewave/o1/O1Decoder$Message;
    .locals 19

    move-object/from16 v0, p0

    const/4 v1, 0x0

    .line 367
    aget-byte v2, v0, v1

    and-int/lit16 v3, v2, 0xff

    const/4 v2, 0x5

    const/16 v4, 0x9

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, 0xf

    const/4 v8, 0x3

    const/16 v9, 0xa

    const/4 v10, 0x1

    const/4 v11, 0x4

    const/4 v12, 0x2

    if-ne v3, v7, :cond_0

    .line 372
    aget-byte v13, v0, v2

    invoke-static {v13}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->errorCodeFromByte(B)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    move-result-object v13

    if-eqz v13, :cond_0

    new-array v2, v11, [B

    .line 374
    aget-byte v7, v0, v11

    aput-byte v7, v2, v1

    aget-byte v1, v0, v8

    aput-byte v1, v2, v10

    aget-byte v1, v0, v12

    aput-byte v1, v2, v12

    aget-byte v1, v0, v10

    aput-byte v1, v2, v8

    invoke-static {v2}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToHexString([B)Ljava/lang/String;

    move-result-object v1

    .line 375
    aget-byte v2, v0, v5

    and-int/lit16 v2, v2, 0xff

    .line 376
    invoke-static {v0, v6, v12}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v7

    .line 377
    aget-byte v4, v0, v4

    and-int/lit16 v8, v4, 0xff

    .line 378
    invoke-static {v0, v9, v12}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v9

    const/16 v4, 0xc

    .line 379
    invoke-static {v0, v4, v12}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v10

    const/16 v4, 0xe

    .line 380
    aget-byte v0, v0, v4

    invoke-static {v0}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->directionFromByte(B)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    move-result-object v11

    move-object v4, v1

    move-object v5, v13

    move v6, v2

    .line 381
    invoke-static/range {v3 .. v11}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->error(ILjava/lang/String;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;IIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)Lcom/squareup/squarewave/o1/O1Decoder$Message;

    move-result-object v0

    return-object v0

    .line 387
    :cond_0
    invoke-static {v0, v10, v8}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v13

    .line 390
    aget-byte v14, v0, v11

    and-int/lit16 v14, v14, 0xff

    new-array v15, v11, [B

    const/16 v16, 0x8

    .line 393
    aget-byte v16, v0, v16

    aput-byte v16, v15, v1

    aget-byte v16, v0, v6

    aput-byte v16, v15, v10

    aget-byte v10, v0, v5

    aput-byte v10, v15, v12

    aget-byte v2, v0, v2

    aput-byte v2, v15, v8

    invoke-static {v15}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToHexString([B)Ljava/lang/String;

    move-result-object v2

    .line 396
    aget-byte v8, v0, v4

    and-int/lit16 v8, v8, 0xff

    .line 397
    sget-object v10, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    const/4 v15, 0x0

    if-lez v8, :cond_1

    .line 400
    aget-byte v10, v0, v9

    invoke-static {v10}, Lcom/squareup/Card$Brand;->fromO1Code(I)Lcom/squareup/Card$Brand;

    move-result-object v10

    new-array v15, v11, [C

    const/16 v16, 0xb

    const/16 v11, 0xb

    :goto_0
    if-ge v11, v7, :cond_1

    add-int/lit8 v16, v11, -0xb

    .line 407
    aget-byte v18, v0, v11

    and-int/lit8 v18, v18, 0xf

    add-int/lit8 v7, v18, 0x30

    int-to-char v7, v7

    aput-char v7, v15, v16

    add-int/lit8 v11, v11, 0x1

    const/16 v7, 0xf

    goto :goto_0

    :cond_1
    add-int/lit8 v7, v8, 0xa

    .line 413
    array-length v11, v0

    sub-int/2addr v11, v9

    sub-int/2addr v11, v7

    .line 415
    new-array v5, v11, [B

    .line 416
    invoke-static {v0, v7, v5, v1, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 422
    array-length v1, v0

    sub-int/2addr v1, v9

    aget-byte v1, v0, v1

    and-int/lit16 v11, v1, 0xff

    .line 423
    array-length v1, v0

    sub-int/2addr v1, v4

    invoke-static {v0, v1, v12}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v1

    .line 424
    array-length v4, v0

    sub-int/2addr v4, v6

    aget-byte v4, v0, v4

    and-int/lit16 v9, v4, 0xff

    .line 425
    array-length v4, v0

    const/4 v6, 0x6

    sub-int/2addr v4, v6

    invoke-static {v0, v4, v12}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v16

    .line 426
    array-length v4, v0

    const/4 v6, 0x4

    sub-int/2addr v4, v6

    invoke-static {v0, v4, v12}, Lcom/squareup/squarewave/o1/O1Decoder;->access$000([BII)I

    move-result v17

    .line 427
    array-length v4, v0

    sub-int/2addr v4, v12

    aget-byte v0, v0, v4

    invoke-static {v0}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->directionFromByte(B)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    move-result-object v0

    move v4, v13

    move-object v12, v5

    move v5, v14

    move-object v6, v2

    move v7, v8

    move-object v8, v10

    move v2, v9

    move-object v9, v15

    move-object v10, v12

    move v12, v1

    move v13, v2

    move/from16 v14, v16

    move/from16 v15, v17

    move-object/from16 v16, v0

    .line 429
    invoke-static/range {v3 .. v16}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->success(IIILjava/lang/String;ILcom/squareup/Card$Brand;[C[BIIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)Lcom/squareup/squarewave/o1/O1Decoder$Message;

    move-result-object v0

    return-object v0
.end method

.method public static success(IIILjava/lang/String;ILcom/squareup/Card$Brand;[C[BIIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)Lcom/squareup/squarewave/o1/O1Decoder$Message;
    .locals 17

    .line 335
    new-instance v16, Lcom/squareup/squarewave/o1/O1Decoder$Message;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    move/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v14, p12

    move-object/from16 v15, p13

    invoke-direct/range {v0 .. v15}, Lcom/squareup/squarewave/o1/O1Decoder$Message;-><init>(IIILjava/lang/String;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;ILcom/squareup/Card$Brand;[C[BIIIIILcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)V

    return-object v16
.end method


# virtual methods
.method public isError()Z
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public zeroize()V
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Decoder$Message;->encryptedBody:[B

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    :cond_0
    return-void
.end method
