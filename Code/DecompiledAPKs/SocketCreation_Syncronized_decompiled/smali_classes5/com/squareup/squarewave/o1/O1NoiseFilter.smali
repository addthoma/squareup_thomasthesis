.class public Lcom/squareup/squarewave/o1/O1NoiseFilter;
.super Ljava/lang/Object;
.source "O1NoiseFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/o1/O1SwipeFilter;


# static fields
.field private static final BUFFER_SIZE:I = 0xc8

.field private static final FLIP_THRESHOLD:S = 0x444s


# instance fields
.field private final next:Lcom/squareup/squarewave/o1/O1SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/o1/O1SwipeFilter;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1NoiseFilter;->next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    return-void
.end method

.method private flipNear([SI)I
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x9

    if-le p2, v2, :cond_0

    add-int/lit8 v2, p2, -0xa

    .line 104
    aget-short v2, p1, v2

    const/4 v3, 0x1

    goto :goto_0

    .line 106
    :cond_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0xb

    if-ge p2, v2, :cond_3

    add-int/lit8 v2, p2, 0xa

    .line 107
    aget-short v2, p1, v2

    const/4 v3, 0x0

    .line 114
    :goto_0
    aget-short v4, p1, p2

    sub-int/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/16 v5, 0x444

    if-le v4, v5, :cond_3

    .line 115
    aget-short p1, p1, p2

    const/4 p2, -0x1

    if-ge v2, p1, :cond_1

    const/4 p1, -0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    :goto_1
    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 p2, 0x1

    :goto_2
    mul-int p2, p2, p1

    return p2

    :cond_3
    return v0
.end method


# virtual methods
.method public filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;
    .locals 12

    .line 27
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->samples()[S

    move-result-object v1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->samplesPerCycle()I

    move-result v2

    const/4 v3, 0x1

    shl-int/2addr v2, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 38
    :goto_0
    array-length v9, v1

    const/4 v10, 0x5

    if-ge v5, v9, :cond_3

    .line 39
    invoke-direct {p0, v1, v5}, Lcom/squareup/squarewave/o1/O1NoiseFilter;->flipNear([SI)I

    move-result v9

    if-eqz v9, :cond_1

    if-eq v9, v6, :cond_1

    add-int/lit8 v8, v8, 0x1

    if-le v8, v10, :cond_0

    goto :goto_2

    :cond_0
    add-int/lit8 v6, v5, 0xa

    move v7, v5

    move v5, v6

    move v6, v9

    goto :goto_1

    :cond_1
    sub-int v9, v5, v7

    if-le v9, v2, :cond_2

    sub-int v7, v5, v2

    .line 47
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    const/4 v8, 0x0

    :cond_2
    :goto_1
    add-int/2addr v5, v3

    goto :goto_0

    .line 52
    :cond_3
    :goto_2
    array-length v5, v1

    sub-int/2addr v5, v3

    .line 55
    array-length v6, v1

    sub-int/2addr v6, v3

    move v8, v5

    const/4 v5, 0x0

    const/4 v9, 0x0

    :goto_3
    if-lez v6, :cond_7

    .line 56
    invoke-direct {p0, v1, v6}, Lcom/squareup/squarewave/o1/O1NoiseFilter;->flipNear([SI)I

    move-result v11

    if-eqz v11, :cond_5

    if-eq v11, v5, :cond_5

    add-int/lit8 v9, v9, 0x1

    if-le v9, v10, :cond_4

    goto :goto_5

    :cond_4
    add-int/lit8 v5, v6, -0xa

    move v8, v6

    move v6, v5

    move v5, v11

    goto :goto_4

    :cond_5
    sub-int v11, v8, v6

    if-le v11, v2, :cond_6

    .line 64
    array-length v8, v1

    sub-int/2addr v8, v3

    add-int v9, v6, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    const/4 v9, 0x0

    :cond_6
    :goto_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    :cond_7
    :goto_5
    const/4 v2, 0x0

    if-ge v7, v8, :cond_8

    const/4 v5, 0x1

    goto :goto_6

    :cond_8
    const/4 v5, 0x0

    :goto_6
    if-gtz v7, :cond_a

    .line 71
    array-length v6, v1

    sub-int/2addr v6, v3

    if-ge v8, v6, :cond_9

    goto :goto_7

    :cond_9
    const/4 v6, 0x0

    goto :goto_8

    :cond_a
    :goto_7
    const/4 v6, 0x1

    :goto_8
    if-eqz v6, :cond_b

    if-eqz v5, :cond_b

    add-int/lit16 v7, v7, -0xc8

    .line 74
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 75
    array-length v5, v1

    sub-int/2addr v5, v3

    add-int/lit16 v8, v8, 0xc8

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int/2addr v3, v2

    .line 77
    new-array v3, v3, [S

    .line 78
    array-length v5, v3

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->buildUpon()Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/squareup/squarewave/Signal$Builder;->samples([S)Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal$Builder;->build()Lcom/squareup/squarewave/Signal;

    move-result-object v0

    .line 80
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->buildUpon()Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->build()Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object p1

    move-object v2, v3

    .line 83
    :cond_b
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1NoiseFilter;->next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/o1/O1SwipeFilter;->filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 84
    iget-object v0, p1, Lcom/squareup/squarewave/o1/O1DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v0}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v0

    if-eqz v0, :cond_c

    if-eqz v2, :cond_c

    .line 85
    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([SS)V

    :cond_c
    return-object p1
.end method
