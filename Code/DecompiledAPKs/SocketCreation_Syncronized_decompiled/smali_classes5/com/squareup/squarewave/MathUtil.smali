.class public Lcom/squareup/squarewave/MathUtil;
.super Ljava/lang/Object;
.source "MathUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bitCount(I)I
    .locals 2

    shr-int/lit8 v0, p0, 0x1

    const v1, 0x55555555

    and-int/2addr v0, v1

    sub-int/2addr p0, v0

    const v0, 0x33333333

    and-int v1, p0, v0

    shr-int/lit8 p0, p0, 0x2

    and-int/2addr p0, v0

    add-int/2addr v1, p0

    shr-int/lit8 p0, v1, 0x4

    add-int/2addr p0, v1

    const v0, 0xf0f0f0f

    and-int/2addr p0, v0

    shr-int/lit8 v0, p0, 0x8

    add-int/2addr p0, v0

    shr-int/lit8 v0, p0, 0x10

    add-int/2addr p0, v0

    and-int/lit8 p0, p0, 0x3f

    return p0
.end method

.method public static isOdd(I)Z
    .locals 1

    const/4 v0, 0x1

    and-int/2addr p0, v0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isPowerOfTwo(I)Z
    .locals 1

    neg-int v0, p0

    and-int/2addr v0, p0

    if-ne v0, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static percentError(II)I
    .locals 0

    .line 80
    invoke-static {p0, p1}, Lcom/squareup/squarewave/MathUtil;->percentageChange(II)I

    move-result p0

    return p0
.end method

.method public static percentageChange(II)I
    .locals 0

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const p0, 0x7fffffff

    :goto_0
    return p0

    :cond_1
    sub-int/2addr p1, p0

    .line 70
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    mul-int/lit8 p1, p1, 0x64

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    div-int/2addr p1, p0

    return p1
.end method

.method public static reverse(I)I
    .locals 2

    const v0, 0x55555555

    and-int v1, p0, v0

    shl-int/lit8 v1, v1, 0x1

    shr-int/lit8 p0, p0, 0x1

    and-int/2addr p0, v0

    or-int/2addr p0, v1

    const v0, 0x33333333

    and-int v1, p0, v0

    shl-int/lit8 v1, v1, 0x2

    shr-int/lit8 p0, p0, 0x2

    and-int/2addr p0, v0

    or-int/2addr p0, v1

    const v0, 0xf0f0f0f

    and-int v1, p0, v0

    shl-int/lit8 v1, v1, 0x4

    shr-int/lit8 p0, p0, 0x4

    and-int/2addr p0, v0

    or-int/2addr p0, v1

    .line 38
    invoke-static {p0}, Lcom/squareup/squarewave/MathUtil;->reverseBytes(I)I

    move-result p0

    return p0
.end method

.method public static reverseBytes(I)I
    .locals 3

    ushr-int/lit8 v0, p0, 0x18

    ushr-int/lit8 v1, p0, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    and-int/2addr v2, p0

    shl-int/lit8 v2, v2, 0x8

    shl-int/lit8 p0, p0, 0x18

    or-int/2addr p0, v2

    or-int/2addr p0, v1

    or-int/2addr p0, v0

    return p0
.end method
