.class public Lcom/squareup/squarewave/wav/AudioRingBuffer;
.super Ljava/lang/Object;
.source "AudioRingBuffer.java"

# interfaces
.implements Lcom/squareup/squarewave/AudioFilter;


# static fields
.field private static final BUFFER_DURATION_SECONDS:I = 0xa


# instance fields
.field private buffer:[S

.field private bufferWrapped:Z

.field private volatile position:I

.field private sampleRate:I

.field private samplesDropped:I

.field private startedProcessing:Z


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkStarted()V
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->startedProcessing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->startedProcessing:Z

    .line 63
    iget v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->sampleRate:I

    mul-int/lit8 v0, v0, 0xa

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    const/4 v0, 0x0

    .line 64
    iput v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    .line 65
    iput v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->samplesDropped:I

    .line 66
    iput-boolean v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->bufferWrapped:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public extractMinMax([S[S[I)F
    .locals 11

    .line 85
    monitor-enter p0

    .line 86
    :try_start_0
    iget v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    .line 87
    iget-object v1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    .line 88
    iget v2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->samplesDropped:I

    .line 89
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 98
    :cond_0
    array-length v3, p1

    .line 99
    array-length v4, v1

    div-int/2addr v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_7

    mul-int v6, v5, v4

    add-int v7, v6, v4

    .line 105
    aget-short v8, v1, v6

    aput-short v8, p2, v5

    aput-short v8, p1, v5

    move v8, v6

    :goto_1
    if-ge v8, v7, :cond_3

    .line 108
    aget-short v9, v1, v8

    aget-short v10, p1, v5

    if-ge v9, v10, :cond_1

    .line 109
    aget-short v9, v1, v8

    aput-short v9, p1, v5

    goto :goto_2

    .line 110
    :cond_1
    aget-short v9, v1, v8

    aget-short v10, p2, v5

    if-le v9, v10, :cond_2

    .line 111
    aget-short v9, v1, v8

    aput-short v9, p2, v5

    :cond_2
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    if-nez v2, :cond_5

    if-lt v6, v0, :cond_4

    const/4 v6, -0x1

    .line 117
    aput v6, p3, v5

    goto :goto_3

    .line 119
    :cond_4
    aput v6, p3, v5

    goto :goto_3

    :cond_5
    if-lt v6, v0, :cond_6

    add-int/2addr v6, v2

    sub-int/2addr v6, v0

    .line 123
    aput v6, p3, v5

    goto :goto_3

    :cond_6
    add-int/2addr v6, v2

    .line 125
    array-length v7, v1

    add-int/2addr v6, v7

    sub-int/2addr v6, v0

    aput v6, p3, v5

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_7
    int-to-float p1, v0

    .line 131
    array-length p2, v1

    int-to-float p2, p2

    div-float/2addr p1, p2

    return p1

    :catchall_0
    move-exception p1

    .line 89
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public finish()V
    .locals 1

    const/4 v0, 0x0

    .line 71
    iput-boolean v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->startedProcessing:Z

    return-void
.end method

.method public declared-synchronized process(Ljava/nio/ByteBuffer;I)V
    .locals 4

    monitor-enter p0

    .line 33
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/squarewave/wav/AudioRingBuffer;->checkStarted()V

    .line 34
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 35
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object p2

    .line 36
    invoke-virtual {p2}, Ljava/nio/ShortBuffer;->limit()I

    move-result v0

    .line 37
    iget-boolean v1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->bufferWrapped:Z

    if-eqz v1, :cond_0

    .line 38
    iget v1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->samplesDropped:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->samplesDropped:I

    .line 40
    :cond_0
    iget-object v1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    array-length v1, v1

    iget v2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lez v1, :cond_1

    .line 43
    iget-object v2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    iget v3, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    invoke-virtual {p2, v2, v3, v1}, Ljava/nio/ShortBuffer;->get([SII)Ljava/nio/ShortBuffer;

    :cond_1
    sub-int v1, v0, v1

    if-lez v1, :cond_2

    .line 48
    iget-object v2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3, v1}, Ljava/nio/ShortBuffer;->get([SII)Ljava/nio/ShortBuffer;

    .line 50
    :cond_2
    iget-boolean p2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->bufferWrapped:Z

    if-nez p2, :cond_3

    .line 51
    iget p2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    add-int/2addr p2, v0

    iget-object v2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    array-length v2, v2

    if-lt p2, v2, :cond_3

    const/4 p2, 0x1

    .line 52
    iput-boolean p2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->bufferWrapped:Z

    .line 53
    iput v1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->samplesDropped:I

    .line 56
    :cond_3
    iget p2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    add-int/2addr p2, v0

    iget-object v0, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->buffer:[S

    array-length v0, v0

    rem-int/2addr p2, v0

    iput p2, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->position:I

    .line 57
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized start(I)V
    .locals 0

    monitor-enter p0

    .line 29
    :try_start_0
    iput p1, p0, Lcom/squareup/squarewave/wav/AudioRingBuffer;->sampleRate:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
