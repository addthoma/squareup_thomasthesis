.class public final Lcom/squareup/squarewave/m1/MessengerR4Decoder;
.super Ljava/lang/Object;
.source "MessengerR4Decoder.kt"

# interfaces
.implements Lcom/squareup/squarewave/m1/R4Decoder;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0017\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u0006H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
        "Lcom/squareup/squarewave/m1/R4Decoder;",
        "messenger",
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "(Lcom/squareup/cardreader/SingleCardreaderMessenger;)V",
        "sequence",
        "",
        "decodeR4Packet",
        "Lcom/squareup/squarewave/gum/StatsAndMaybePacket;",
        "linkType",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;",
        "samples",
        "",
        "nextSequence",
        "squarewave_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

.field private sequence:I


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SingleCardreaderMessenger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "messenger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    return-void
.end method

.method private final declared-synchronized nextSequence()I
    .locals 2

    monitor-enter p0

    .line 19
    :try_start_0
    iget v0, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder;->sequence:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder;->sequence:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public decodeR4Packet(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[S)Lcom/squareup/squarewave/gum/StatsAndMaybePacket;
    .locals 3

    const-string v0, "linkType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "samples"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    invoke-direct {p0}, Lcom/squareup/squarewave/m1/MessengerR4Decoder;->nextSequence()I

    move-result v2

    invoke-direct {v1, p1, p2, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SI)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SingleCardreaderMessenger;->send(Lcom/squareup/cardreader/ReaderMessage$ReaderInput;)V

    .line 29
    iget-object p1, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    invoke-interface {p1}, Lcom/squareup/cardreader/SingleCardreaderMessenger;->getResponses()Lio/reactivex/Observable;

    move-result-object p1

    .line 31
    const-class p2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p1

    .line 29
    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse;

    .line 35
    instance-of p2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$DecodedR4Packet;

    if-eqz p2, :cond_1

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$DecodedR4Packet;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$DecodedR4Packet;->getPacket()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.squarewave.gum.StatsAndMaybePacket"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 36
    :cond_1
    instance-of p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$NotReady;

    if-eqz p1, :cond_2

    .line 37
    new-instance p1, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;

    const/4 p2, 0x0

    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INCOMPLETE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->result(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;-><init>(Lcom/squareup/squarewave/gum/CardDataPacket;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
