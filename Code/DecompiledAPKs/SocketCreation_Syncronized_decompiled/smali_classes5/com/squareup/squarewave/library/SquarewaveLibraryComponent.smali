.class public interface abstract Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;
.super Ljava/lang/Object;
.source "SquarewaveLibraryComponent.java"


# annotations
.annotation runtime Ldagger/Component;
    dependencies = {
        Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;
    }
    modules = {
        Lcom/squareup/squarewave/library/SquarewaveLibraryModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;
    }
.end annotation


# virtual methods
.method public abstract squarewave()Lcom/squareup/squarewave/library/SquarewaveLibrary;
.end method
