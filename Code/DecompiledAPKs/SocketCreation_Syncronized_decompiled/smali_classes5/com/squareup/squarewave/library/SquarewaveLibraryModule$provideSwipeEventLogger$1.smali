.class public final Lcom/squareup/squarewave/library/SquarewaveLibraryModule$provideSwipeEventLogger$1;
.super Ljava/lang/Object;
.source "SquarewaveLibraryModule.kt"

# interfaces
.implements Lcom/squareup/logging/SwipeEventLogger;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->provideSwipeEventLogger()Lcom/squareup/logging/SwipeEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/squarewave/library/SquarewaveLibraryModule$provideSwipeEventLogger$1",
        "Lcom/squareup/logging/SwipeEventLogger;",
        "logReaderCarrierDetectEvent",
        "",
        "event",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;",
        "squarewave_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public logReaderCarrierDetectEvent(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
