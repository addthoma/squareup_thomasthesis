.class public Lcom/squareup/squarewave/gen2/Gen2DemodResult;
.super Ljava/lang/Object;
.source "Gen2DemodResult.java"


# instance fields
.field public final detailedResult:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public final globalResult:Lcom/squareup/squarewave/decode/DemodResult;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    .line 13
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->detailedResult:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-void
.end method
