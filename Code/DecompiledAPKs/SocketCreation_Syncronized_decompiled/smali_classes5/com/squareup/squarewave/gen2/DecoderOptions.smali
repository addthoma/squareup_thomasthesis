.class public Lcom/squareup/squarewave/gen2/DecoderOptions;
.super Ljava/lang/Object;
.source "DecoderOptions.java"


# static fields
.field private static final BEST_OPTIONS:[Lcom/squareup/squarewave/gen2/DecoderOptions;


# instance fields
.field public final clockingTolerance:I

.field public final cutoff:I

.field public final denoiserOrder:Lcom/squareup/squarewave/gen2/Denoiser$Order;

.field public final peakFinderDelay:I

.field public final windowSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/squarewave/gen2/DecoderOptions;

    .line 15
    new-instance v7, Lcom/squareup/squarewave/gen2/DecoderOptions;

    sget-object v3, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_5:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v2, 0x2

    const/4 v4, 0x5

    const/16 v5, 0x2e

    const/16 v6, 0x15

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/squarewave/gen2/DecoderOptions;-><init>(ILcom/squareup/squarewave/gen2/Denoiser$Order;III)V

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/squarewave/gen2/DecoderOptions;

    sget-object v10, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_15:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    const/4 v9, 0x3

    const/16 v11, 0x10

    const/16 v12, 0x21

    const/16 v13, 0x2a

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/squareup/squarewave/gen2/DecoderOptions;-><init>(ILcom/squareup/squarewave/gen2/Denoiser$Order;III)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/squarewave/gen2/DecoderOptions;->BEST_OPTIONS:[Lcom/squareup/squarewave/gen2/DecoderOptions;

    return-void
.end method

.method public constructor <init>(ILcom/squareup/squarewave/gen2/Denoiser$Order;III)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->peakFinderDelay:I

    .line 32
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->denoiserOrder:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    .line 33
    iput p3, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->windowSize:I

    .line 34
    iput p4, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->cutoff:I

    .line 35
    iput p5, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->clockingTolerance:I

    return-void
.end method

.method public static getBest()[Lcom/squareup/squarewave/gen2/DecoderOptions;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/squarewave/gen2/DecoderOptions;->BEST_OPTIONS:[Lcom/squareup/squarewave/gen2/DecoderOptions;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->peakFinderDelay:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->denoiserOrder:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", windowSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->windowSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", cutoff="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->cutoff:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", clockingTolerance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gen2/DecoderOptions;->clockingTolerance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
