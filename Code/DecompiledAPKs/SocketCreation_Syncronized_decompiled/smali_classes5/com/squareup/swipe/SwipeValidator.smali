.class public Lcom/squareup/swipe/SwipeValidator;
.super Ljava/lang/Object;
.source "SwipeValidator.java"


# instance fields
.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/swipe/SwipeValidator;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 20
    iput-object p2, p0, Lcom/squareup/swipe/SwipeValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 21
    iput-object p3, p0, Lcom/squareup/swipe/SwipeValidator;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method


# virtual methods
.method public isInAuthRange(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Z
    .locals 6

    .line 28
    iget-object v0, p0, Lcom/squareup/swipe/SwipeValidator;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 29
    iget-object p1, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    if-lez v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 32
    :cond_1
    iget-object p1, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object p1, p0, Lcom/squareup/swipe/SwipeValidator;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-ltz p1, :cond_2

    iget-object p1, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 33
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iget-object v2, p0, Lcom/squareup/swipe/SwipeValidator;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v2

    cmp-long v4, p1, v2

    if-gtz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public swipeHasEnoughData(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Z
    .locals 3

    .line 43
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    new-instance v1, Lcom/squareup/swipe/SwipeValidator$1;

    invoke-direct {v1, p0}, Lcom/squareup/swipe/SwipeValidator$1;-><init>(Lcom/squareup/swipe/SwipeValidator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/Card;->handleInputType(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 80
    :cond_0
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    sget-object v2, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    if-ne v0, v2, :cond_1

    return v1

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/squareup/swipe/SwipeValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->requiresTrack2OnNonAmexSwipe()Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 90
    :cond_2
    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->hasTrack2Data:Z

    return p1
.end method
