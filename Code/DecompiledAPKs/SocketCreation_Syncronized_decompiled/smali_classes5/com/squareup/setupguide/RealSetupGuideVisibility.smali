.class public final Lcom/squareup/setupguide/RealSetupGuideVisibility;
.super Ljava/lang/Object;
.source "RealSetupGuideVisibility.kt"

# interfaces
.implements Lcom/squareup/setupguide/SetupGuideVisibility;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSetupGuideVisibility.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSetupGuideVisibility.kt\ncom/squareup/setupguide/RealSetupGuideVisibility\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,42:1\n92#2,4:43\n*E\n*S KotlinDebug\n*F\n+ 1 RealSetupGuideVisibility.kt\ncom/squareup/setupguide/RealSetupGuideVisibility\n*L\n19#1,4:43\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0016J\u0008\u0010\u000e\u001a\u00020\rH\u0016J\u000c\u0010\u000f\u001a\u00020\r*\u00020\u0010H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/setupguide/RealSetupGuideVisibility;",
        "Lcom/squareup/setupguide/SetupGuideVisibility;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "setupGuideCache",
        "Lcom/squareup/setupguide/SetupGuideCache;",
        "setupGuideDismissal",
        "Lcom/squareup/setupguide/SetupGuideDismissal;",
        "employeeManagementModeDecider",
        "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/setupguide/SetupGuideCache;Lcom/squareup/setupguide/SetupGuideDismissal;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V",
        "shouldShowSetupGuideEntryPoint",
        "Lio/reactivex/Observable;",
        "",
        "shouldShowSetupGuideInSupport",
        "isOwner",
        "Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final setupGuideCache:Lcom/squareup/setupguide/SetupGuideCache;

.field private final setupGuideDismissal:Lcom/squareup/setupguide/SetupGuideDismissal;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/setupguide/SetupGuideCache;Lcom/squareup/setupguide/SetupGuideDismissal;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideCache"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideDismissal"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagementModeDecider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->setupGuideCache:Lcom/squareup/setupguide/SetupGuideCache;

    iput-object p3, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->setupGuideDismissal:Lcom/squareup/setupguide/SetupGuideDismissal;

    iput-object p4, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    return-void
.end method

.method public static final synthetic access$getSetupGuideDismissal$p(Lcom/squareup/setupguide/RealSetupGuideVisibility;)Lcom/squareup/setupguide/SetupGuideDismissal;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->setupGuideDismissal:Lcom/squareup/setupguide/SetupGuideDismissal;

    return-object p0
.end method

.method public static final synthetic access$isOwner(Lcom/squareup/setupguide/RealSetupGuideVisibility;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Z
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/setupguide/RealSetupGuideVisibility;->isOwner(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Z

    move-result p0

    return p0
.end method

.method private final isOwner(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Z
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public shouldShowSetupGuideEntryPoint()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 19
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 20
    iget-object v0, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SETUP_GUIDE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(SETUP_GUIDE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v1, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->setupGuideCache:Lcom/squareup/setupguide/SetupGuideCache;

    invoke-interface {v1}, Lcom/squareup/setupguide/SetupGuideCache;->getHasIncompleteItem()Lio/reactivex/Observable;

    move-result-object v1

    .line 22
    iget-object v2, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v2}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/setupguide/RealSetupGuideVisibility$shouldShowSetupGuideEntryPoint$1;

    invoke-direct {v3, p0}, Lcom/squareup/setupguide/RealSetupGuideVisibility$shouldShowSetupGuideEntryPoint$1;-><init>(Lcom/squareup/setupguide/RealSetupGuideVisibility;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "employeeManagementModeDe\u2026de().map { it.isOwner() }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 45
    new-instance v3, Lcom/squareup/setupguide/RealSetupGuideVisibility$shouldShowSetupGuideEntryPoint$$inlined$combineLatest$1;

    invoke-direct {v3, p0}, Lcom/squareup/setupguide/RealSetupGuideVisibility$shouldShowSetupGuideEntryPoint$$inlined$combineLatest$1;-><init>(Lcom/squareup/setupguide/RealSetupGuideVisibility;)V

    check-cast v3, Lio/reactivex/functions/Function3;

    .line 43
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026unction(t1, t2, t3) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public shouldShowSetupGuideInSupport()Z
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SETUP_GUIDE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/squareup/setupguide/RealSetupGuideVisibility;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    const-string v1, "employeeManagementModeDecider.mode"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/setupguide/RealSetupGuideVisibility;->isOwner(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
