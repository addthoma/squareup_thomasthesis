.class public final Lcom/squareup/setupguide/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final default_setup_guide_intro_card_subtitle:I = 0x7f1207e4

.field public static final done:I = 0x7f1208d3

.field public static final get_set_up:I = 0x7f120af2

.field public static final onboarding_modal_button_later:I = 0x7f121107

.field public static final onboarding_modal_button_setup_payments:I = 0x7f121108

.field public static final onboarding_modal_idv_estimate_error_message:I = 0x7f121109

.field public static final onboarding_modal_idv_estimate_error_title:I = 0x7f12110a

.field public static final onboarding_modal_idv_invoice_error_message:I = 0x7f12110b

.field public static final onboarding_modal_idv_invoice_error_title:I = 0x7f12110c

.field public static final onboarding_modal_idv_invoice_reminder_message:I = 0x7f12110d

.field public static final onboarding_modal_idv_invoice_reminder_title:I = 0x7f12110e

.field public static final onboarding_modal_send_with_no_idv_message:I = 0x7f12110f

.field public static final onboarding_modal_send_with_no_idv_title:I = 0x7f121110

.field public static final primary_setup:I = 0x7f12149e

.field public static final send_anyways:I = 0x7f1217bf

.field public static final setup_guide_free_reader_card_message:I = 0x7f1217db

.field public static final setup_guide_free_reader_card_title:I = 0x7f1217dc


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
