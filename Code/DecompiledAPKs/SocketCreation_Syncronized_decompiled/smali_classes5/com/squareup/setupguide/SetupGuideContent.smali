.class public abstract Lcom/squareup/setupguide/SetupGuideContent;
.super Ljava/lang/Object;
.source "SetupGuideConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupGuideContent$PreCheck;,
        Lcom/squareup/setupguide/SetupGuideContent$LegacyFlowContent;,
        Lcom/squareup/setupguide/SetupGuideContent$IDVContent;,
        Lcom/squareup/setupguide/SetupGuideContent$BankLinkingContent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0018\u0019\u001a\u001bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0012\u0010\u000e\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\rR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u0012\u0010\u0014\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\rR\u0012\u0010\u0016\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\r\u0082\u0001\u0003\u001c\u001d\u001e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideContent;",
        "",
        "()V",
        "actionItemName",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        "getActionItemName",
        "()Lcom/squareup/protos/checklist/common/ActionItemName;",
        "completionMessageId",
        "",
        "getCompletionMessageId",
        "()Ljava/lang/Integer;",
        "completionTitleId",
        "getCompletionTitleId",
        "()I",
        "iconId",
        "getIconId",
        "preCheck",
        "Lcom/squareup/setupguide/SetupGuideContent$PreCheck;",
        "getPreCheck",
        "()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;",
        "subtitleId",
        "getSubtitleId",
        "titleId",
        "getTitleId",
        "BankLinkingContent",
        "IDVContent",
        "LegacyFlowContent",
        "PreCheck",
        "Lcom/squareup/setupguide/SetupGuideContent$LegacyFlowContent;",
        "Lcom/squareup/setupguide/SetupGuideContent$IDVContent;",
        "Lcom/squareup/setupguide/SetupGuideContent$BankLinkingContent;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/setupguide/SetupGuideContent;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getActionItemName()Lcom/squareup/protos/checklist/common/ActionItemName;
.end method

.method public abstract getCompletionMessageId()Ljava/lang/Integer;
.end method

.method public abstract getCompletionTitleId()I
.end method

.method public abstract getIconId()I
.end method

.method public abstract getPreCheck()Lcom/squareup/setupguide/SetupGuideContent$PreCheck;
.end method

.method public abstract getSubtitleId()I
.end method

.method public abstract getTitleId()I
.end method
