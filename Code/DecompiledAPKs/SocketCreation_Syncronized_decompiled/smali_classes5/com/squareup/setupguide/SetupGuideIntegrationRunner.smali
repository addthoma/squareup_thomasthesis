.class public interface abstract Lcom/squareup/setupguide/SetupGuideIntegrationRunner;
.super Ljava/lang/Object;
.source "SetupGuideIntegrationRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J&\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\'\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
        "",
        "handleCompletion",
        "Lio/reactivex/Completable;",
        "name",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        "parentTreeKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "doIfCompleted",
        "Lkotlin/Function0;",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation
.end method
