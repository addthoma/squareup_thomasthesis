.class public interface abstract Lcom/squareup/setupguide/SetupGuideLogger;
.super Ljava/lang/Object;
.source "SetupGuideLogger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\tH&J\u0008\u0010\r\u001a\u00020\tH&J\u0010\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0003H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideLogger;",
        "",
        "homeAppletName",
        "",
        "getHomeAppletName",
        "()Ljava/lang/String;",
        "supportAppletName",
        "getSupportAppletName",
        "logActionItemClicked",
        "",
        "name",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        "logGuideAutoPresented",
        "logGuideDismissed",
        "logGuideOpened",
        "openLocation",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getHomeAppletName()Ljava/lang/String;
.end method

.method public abstract getSupportAppletName()Ljava/lang/String;
.end method

.method public abstract logActionItemClicked(Lcom/squareup/protos/checklist/common/ActionItemName;)V
.end method

.method public abstract logGuideAutoPresented()V
.end method

.method public abstract logGuideDismissed()V
.end method

.method public abstract logGuideOpened(Ljava/lang/String;)V
.end method
