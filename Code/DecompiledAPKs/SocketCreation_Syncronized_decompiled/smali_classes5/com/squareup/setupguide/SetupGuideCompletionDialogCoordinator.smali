.class public final Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SetupGuideCompletionDialogCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSetupGuideCompletionDialogCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SetupGuideCompletionDialogCoordinator.kt\ncom/squareup/setupguide/SetupGuideCompletionDialogCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,67:1\n1103#2,7:68\n*E\n*S KotlinDebug\n*F\n+ 1 SetupGuideCompletionDialogCoordinator.kt\ncom/squareup/setupguide/SetupGuideCompletionDialogCoordinator\n*L\n62#1,7:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;",
        "(Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;)V",
        "iconImageView",
        "Landroid/widget/ImageView;",
        "messageTextView",
        "Lcom/squareup/widgets/MessageView;",
        "primaryButtonView",
        "Lcom/squareup/noho/NohoButton;",
        "secondaryButtonView",
        "titleTextView",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "data",
        "Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private iconImageView:Landroid/widget/ImageView;

.field private messageTextView:Lcom/squareup/widgets/MessageView;

.field private primaryButtonView:Lcom/squareup/noho/NohoButton;

.field private final runner:Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;

.field private secondaryButtonView:Lcom/squareup/noho/NohoButton;

.field private titleTextView:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->runner:Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;)Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->runner:Lcom/squareup/setupguide/SetupGuideCompletionDialog$Runner;

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 37
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_icon:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->iconImageView:Landroid/widget/ImageView;

    .line 38
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    .line 39
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->messageTextView:Lcom/squareup/widgets/MessageView;

    .line 40
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_primary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->primaryButtonView:Lcom/squareup/noho/NohoButton;

    .line 41
    sget v0, Lcom/squareup/setupguide/R$id;->setup_guide_dialog_secondary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->secondaryButtonView:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final update(Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;)V
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->iconImageView:Landroid/widget/ImageView;

    const-string v1, "iconImageView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 47
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->iconImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->getIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 50
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    const-string/jumbo v1, "titleTextView"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->getTitleId()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 51
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->messageTextView:Lcom/squareup/widgets/MessageView;

    const-string v3, "messageTextView"

    if-nez v0, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->getMessageId()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_4

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 52
    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->getMessageId()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_6

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->messageTextView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 55
    :cond_6
    iget-object p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/widgets/MessageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_b

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 56
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->titleTextView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    sget v1, Lcom/squareup/noho/R$dimen;->noho_title_over_text_padding:I

    .line 56
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 61
    iget-object p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->primaryButtonView:Lcom/squareup/noho/NohoButton;

    const-string v0, "primaryButtonView"

    if-nez p1, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    sget v1, Lcom/squareup/setupguide/R$string;->done:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 62
    iget-object p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->primaryButtonView:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_a

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast p1, Landroid/view/View;

    .line 68
    new-instance v0, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0}, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 55
    :cond_b
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->bindViews(Landroid/view/View;)V

    .line 32
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog;

    invoke-virtual {p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialog;->getData()Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    move-result-object p1

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/setupguide/SetupGuideCompletionDialogCoordinator;->update(Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;)V

    return-void
.end method
