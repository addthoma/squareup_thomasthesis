.class Lcom/squareup/stickylistheaders/AdapterWrapper$2;
.super Ljava/lang/Object;
.source "AdapterWrapper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/stickylistheaders/AdapterWrapper;->configureHeader(Lcom/squareup/stickylistheaders/WrapperView;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/stickylistheaders/AdapterWrapper;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/squareup/stickylistheaders/AdapterWrapper;I)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->this$0:Lcom/squareup/stickylistheaders/AdapterWrapper;

    iput p2, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 134
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->this$0:Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->access$300(Lcom/squareup/stickylistheaders/AdapterWrapper;)Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->this$0:Lcom/squareup/stickylistheaders/AdapterWrapper;

    iget-object v0, v0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    iget v1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->val$position:I

    invoke-interface {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderId(I)J

    move-result-wide v0

    .line 136
    iget-object v2, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->this$0:Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-static {v2}, Lcom/squareup/stickylistheaders/AdapterWrapper;->access$300(Lcom/squareup/stickylistheaders/AdapterWrapper;)Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    move-result-object v2

    iget v3, p0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;->val$position:I

    invoke-interface {v2, p1, v3, v0, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;->onHeaderClick(Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method
