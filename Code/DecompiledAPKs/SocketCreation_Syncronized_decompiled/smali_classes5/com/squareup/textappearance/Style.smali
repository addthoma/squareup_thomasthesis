.class public final enum Lcom/squareup/textappearance/Style;
.super Ljava/lang/Enum;
.source "FontFamily.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/textappearance/Style;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/textappearance/Style;",
        "",
        "attributeValue",
        "",
        "(Ljava/lang/String;II)V",
        "getAttributeValue",
        "()I",
        "NORMAL",
        "ITALIC",
        "textappearance_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/textappearance/Style;

.field public static final enum ITALIC:Lcom/squareup/textappearance/Style;

.field public static final enum NORMAL:Lcom/squareup/textappearance/Style;


# instance fields
.field private final attributeValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/textappearance/Style;

    new-instance v1, Lcom/squareup/textappearance/Style;

    const/4 v2, 0x0

    const-string v3, "NORMAL"

    .line 84
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/textappearance/Style;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/textappearance/Style;->NORMAL:Lcom/squareup/textappearance/Style;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/textappearance/Style;

    const/4 v2, 0x1

    const-string v3, "ITALIC"

    .line 85
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/textappearance/Style;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/textappearance/Style;->ITALIC:Lcom/squareup/textappearance/Style;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/textappearance/Style;->$VALUES:[Lcom/squareup/textappearance/Style;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/textappearance/Style;->attributeValue:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/textappearance/Style;
    .locals 1

    const-class v0, Lcom/squareup/textappearance/Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/textappearance/Style;

    return-object p0
.end method

.method public static values()[Lcom/squareup/textappearance/Style;
    .locals 1

    sget-object v0, Lcom/squareup/textappearance/Style;->$VALUES:[Lcom/squareup/textappearance/Style;

    invoke-virtual {v0}, [Lcom/squareup/textappearance/Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/textappearance/Style;

    return-object v0
.end method


# virtual methods
.method public final getAttributeValue()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/squareup/textappearance/Style;->attributeValue:I

    return v0
.end method
