.class public final Lcom/squareup/tmn/TmnTimings;
.super Ljava/lang/Object;
.source "TmnTimings.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0002\u0010\rJ\u000e\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001dJ\u000e\u0010$\u001a\u00020\"2\u0006\u0010%\u001a\u00020&J\u0016\u0010\'\u001a\u00020\"2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u000fJ\u0006\u0010(\u001a\u00020\"J\u0006\u0010)\u001a\u00020\"R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnTimings;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "handlerFactory",
        "Lcom/squareup/tmn/HandlerFactory;",
        "connectivityProvider",
        "Ljavax/inject/Provider;",
        "Landroid/net/ConnectivityManager;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/tmn/HandlerFactory;Ljavax/inject/Provider;)V",
        "connectionIntervalMillis",
        "",
        "currentPhase",
        "Lcom/squareup/tmn/Phase;",
        "dateTimeFactory",
        "Lcom/squareup/util/DateTimeFactory;",
        "events",
        "",
        "Lcom/squareup/tmn/Event;",
        "handler",
        "Landroid/os/Handler;",
        "mtu",
        "startTimeNanos",
        "",
        "tapConnectivity",
        "",
        "tapTimeNanos",
        "verbose",
        "",
        "complete",
        "",
        "transactionId",
        "event",
        "what",
        "Lcom/squareup/tmn/What;",
        "params",
        "start",
        "tapped",
        "tmn-timings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private connectionIntervalMillis:I

.field private final connectivityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field private currentPhase:Lcom/squareup/tmn/Phase;

.field private final dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tmn/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private handler:Landroid/os/Handler;

.field private final handlerFactory:Lcom/squareup/tmn/HandlerFactory;

.field private mtu:I

.field private startTimeNanos:J

.field private tapConnectivity:Ljava/lang/String;

.field private tapTimeNanos:J

.field private verbose:Z


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/tmn/HandlerFactory;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/tmn/HandlerFactory;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityProvider"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/tmn/TmnTimings;->clock:Lcom/squareup/util/Clock;

    iput-object p3, p0, Lcom/squareup/tmn/TmnTimings;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p4, p0, Lcom/squareup/tmn/TmnTimings;->handlerFactory:Lcom/squareup/tmn/HandlerFactory;

    iput-object p5, p0, Lcom/squareup/tmn/TmnTimings;->connectivityProvider:Ljavax/inject/Provider;

    .line 58
    new-instance p1, Lcom/squareup/util/DateTimeFactory;

    invoke-direct {p1}, Lcom/squareup/util/DateTimeFactory;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    .line 67
    sget-object p1, Lcom/squareup/tmn/Phase;->BEFORE_TAP:Lcom/squareup/tmn/Phase;

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings;->currentPhase:Lcom/squareup/tmn/Phase;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/tmn/TmnTimings;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getConnectionIntervalMillis$p(Lcom/squareup/tmn/TmnTimings;)I
    .locals 0

    .line 51
    iget p0, p0, Lcom/squareup/tmn/TmnTimings;->connectionIntervalMillis:I

    return p0
.end method

.method public static final synthetic access$getCurrentPhase$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/tmn/Phase;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/tmn/TmnTimings;->currentPhase:Lcom/squareup/tmn/Phase;

    return-object p0
.end method

.method public static final synthetic access$getDateTimeFactory$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/util/DateTimeFactory;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/tmn/TmnTimings;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    return-object p0
.end method

.method public static final synthetic access$getEvents$p(Lcom/squareup/tmn/TmnTimings;)Ljava/util/List;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/tmn/TmnTimings;->events:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/tmn/TmnTimings;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getMtu$p(Lcom/squareup/tmn/TmnTimings;)I
    .locals 0

    .line 51
    iget p0, p0, Lcom/squareup/tmn/TmnTimings;->mtu:I

    return p0
.end method

.method public static final synthetic access$getStartTimeNanos$p(Lcom/squareup/tmn/TmnTimings;)J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimings;->startTimeNanos:J

    return-wide v0
.end method

.method public static final synthetic access$getTapConnectivity$p(Lcom/squareup/tmn/TmnTimings;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/tmn/TmnTimings;->tapConnectivity:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getTapTimeNanos$p(Lcom/squareup/tmn/TmnTimings;)J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimings;->tapTimeNanos:J

    return-wide v0
.end method

.method public static final synthetic access$getVerbose$p(Lcom/squareup/tmn/TmnTimings;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lcom/squareup/tmn/TmnTimings;->verbose:Z

    return p0
.end method

.method public static final synthetic access$setConnectionIntervalMillis$p(Lcom/squareup/tmn/TmnTimings;I)V
    .locals 0

    .line 51
    iput p1, p0, Lcom/squareup/tmn/TmnTimings;->connectionIntervalMillis:I

    return-void
.end method

.method public static final synthetic access$setCurrentPhase$p(Lcom/squareup/tmn/TmnTimings;Lcom/squareup/tmn/Phase;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings;->currentPhase:Lcom/squareup/tmn/Phase;

    return-void
.end method

.method public static final synthetic access$setEvents$p(Lcom/squareup/tmn/TmnTimings;Ljava/util/List;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings;->events:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$setMtu$p(Lcom/squareup/tmn/TmnTimings;I)V
    .locals 0

    .line 51
    iput p1, p0, Lcom/squareup/tmn/TmnTimings;->mtu:I

    return-void
.end method

.method public static final synthetic access$setStartTimeNanos$p(Lcom/squareup/tmn/TmnTimings;J)V
    .locals 0

    .line 51
    iput-wide p1, p0, Lcom/squareup/tmn/TmnTimings;->startTimeNanos:J

    return-void
.end method

.method public static final synthetic access$setTapConnectivity$p(Lcom/squareup/tmn/TmnTimings;Ljava/lang/String;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings;->tapConnectivity:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setTapTimeNanos$p(Lcom/squareup/tmn/TmnTimings;J)V
    .locals 0

    .line 51
    iput-wide p1, p0, Lcom/squareup/tmn/TmnTimings;->tapTimeNanos:J

    return-void
.end method

.method public static final synthetic access$setVerbose$p(Lcom/squareup/tmn/TmnTimings;Z)V
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/tmn/TmnTimings;->verbose:Z

    return-void
.end method


# virtual methods
.method public final complete(Ljava/lang/String;)V
    .locals 8

    const-string/jumbo v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v3, p0, Lcom/squareup/tmn/TmnTimings;->events:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 148
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtimeNanos()J

    move-result-wide v5

    .line 149
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v7, Lcom/squareup/tmn/TmnTimings$complete$1;

    move-object v1, v7

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tmn/TmnTimings$complete$1;-><init>(Lcom/squareup/tmn/TmnTimings;Ljava/util/List;Ljava/lang/String;J)V

    check-cast v7, Ljava/lang/Runnable;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final event(Lcom/squareup/tmn/What;)V
    .locals 4

    const-string/jumbo v0, "what"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->events:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtimeNanos()J

    move-result-wide v0

    .line 135
    iget-object v2, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    new-instance v3, Lcom/squareup/tmn/TmnTimings$event$1;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/squareup/tmn/TmnTimings$event$1;-><init>(Lcom/squareup/tmn/TmnTimings;Lcom/squareup/tmn/What;J)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public final params(II)V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/tmn/TmnTimings$params$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/tmn/TmnTimings$params$1;-><init>(Lcom/squareup/tmn/TmnTimings;II)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final start()V
    .locals 4

    .line 82
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TMN_RECORD_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 83
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/tmn/TmnTimings;->events:Ljava/util/List;

    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtimeNanos()J

    move-result-wide v0

    .line 89
    iget-object v2, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/squareup/tmn/TmnTimings;->handlerFactory:Lcom/squareup/tmn/HandlerFactory;

    invoke-virtual {v2}, Lcom/squareup/tmn/HandlerFactory;->create()Landroid/os/Handler;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    .line 93
    :cond_1
    iget-object v2, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    new-instance v3, Lcom/squareup/tmn/TmnTimings$start$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/tmn/TmnTimings$start$1;-><init>(Lcom/squareup/tmn/TmnTimings;J)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public final tapped()V
    .locals 5

    .line 117
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->events:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtimeNanos()J

    move-result-wide v0

    .line 120
    iget-object v2, p0, Lcom/squareup/tmn/TmnTimings;->connectivityProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "connectivityProvider.get()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-static {v2}, Lcom/squareup/util/ConnectivityKt;->provideNetworkConnectivity(Landroid/net/ConnectivityManager;)Ljava/lang/String;

    move-result-object v2

    .line 121
    iget-object v3, p0, Lcom/squareup/tmn/TmnTimings;->handler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    new-instance v4, Lcom/squareup/tmn/TmnTimings$tapped$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/squareup/tmn/TmnTimings$tapped$1;-><init>(Lcom/squareup/tmn/TmnTimings;JLjava/lang/String;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method
