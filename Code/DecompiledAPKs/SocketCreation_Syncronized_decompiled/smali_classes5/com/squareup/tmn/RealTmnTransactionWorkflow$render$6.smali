.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->render(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "+",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "it",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    instance-of v0, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    if-eqz v0, :cond_1

    .line 236
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTransactionId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getConnId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 237
    :goto_0
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v10

    .line 238
    new-instance v11, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 239
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_READER_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x34

    const/4 v9, 0x0

    move-object v1, v11

    move-object v5, v0

    .line 238
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 237
    invoke-virtual {v10, v11}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 242
    iget-object v7, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 243
    new-instance v8, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    .line 244
    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTmnData()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v4

    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v5

    move-object v1, v8

    move-object v6, v0

    .line 243
    invoke-direct/range {v1 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;-><init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    move-object v2, v8

    check-cast v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 246
    sget-object v3, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    .line 242
    invoke-static/range {v1 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 249
    :cond_1
    instance-of v0, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;

    if-eqz v0, :cond_3

    .line 250
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v0

    .line 251
    new-instance v10, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 252
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_READER_COMPLETION:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 253
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getConnId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x34

    const/4 v9, 0x0

    move-object v1, v10

    .line 251
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 250
    invoke-virtual {v0, v10}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getCardReaderHelper$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/CardReaderHelper;

    move-result-object v0

    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;->getTmnTransactionResult()Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;->getPaymentTimings()Lcom/squareup/cardreader/PaymentTimings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tmn/CardReaderHelper;->onTmnCompletion$impl_release(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;->getTmnTransactionResult()Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$isSuccess(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/felica/Status;->SUCCESS:Lcom/squareup/protos/client/felica/Status;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/protos/client/felica/Status;->FAILURE:Lcom/squareup/protos/client/felica/Status;

    .line 259
    :goto_1
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 260
    new-instance v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;

    .line 261
    new-instance v3, Lcom/squareup/tmn/TmnTransactionOutput$Completed;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;->getTmnTransactionResult()Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object p1

    iget-object v4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Lcom/squareup/tmn/TmnTransactionOutput$Completed;-><init>(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    check-cast v3, Lcom/squareup/tmn/TmnTransactionOutput;

    .line 262
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object p1

    .line 263
    iget-object v4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getConnId()Ljava/lang/String;

    move-result-object v4

    .line 260
    invoke-direct {v2, v0, v3, p1, v4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;-><init>(Lcom/squareup/protos/client/felica/Status;Lcom/squareup/tmn/TmnTransactionOutput;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 264
    sget-object v3, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_COMPLETION:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 259
    invoke-static/range {v1 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 267
    :cond_3
    instance-of v0, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    if-eqz v0, :cond_4

    .line 268
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->getBalanceBefore()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->getAmount()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->getData()[B

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;-><init>(II[B)V

    .line 269
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object p1

    .line 270
    new-instance v10, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 271
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_READER_WRITE_NOTIFY:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 272
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getConnId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x34

    const/4 v9, 0x0

    move-object v1, v10

    .line 270
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 269
    invoke-virtual {p1, v10}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 275
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getMiryoWorkerDelayer()Lcom/squareup/tmn/MiryoWorkerDelayer;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/tmn/MiryoWorkerDelayer;->maybeDelayBeforeWriteNotify()V

    .line 276
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getCardReaderHelper$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/CardReaderHelper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tmn/CardReaderHelper;->ackTmnWriteNotify$impl_release()V

    .line 277
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getMiryoWorkerDelayer()Lcom/squareup/tmn/MiryoWorkerDelayer;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/tmn/MiryoWorkerDelayer;->maybeDelayAfterWriteNotify()V

    .line 278
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 279
    new-instance v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    .line 280
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getConnId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v3, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v4

    .line 279
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 282
    sget-object v0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    .line 283
    sget-object v2, Lcom/squareup/tmn/TmnTransactionOutput$AfterWriteNotify;->INSTANCE:Lcom/squareup/tmn/TmnTransactionOutput$AfterWriteNotify;

    check-cast v2, Lcom/squareup/tmn/TmnTransactionOutput;

    .line 278
    invoke-static {p1, v1, v0, v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    .line 286
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    invoke-static {v1, v2, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getIllegalStateMessage(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/dipper/events/TmnEvent;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;->invoke(Lcom/squareup/dipper/events/TmnEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
