.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->call()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $retryCount:Lkotlin/jvm/internal/Ref$LongRef;

.field final synthetic $tmnDataByteString:Lokio/ByteString;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;Lkotlin/jvm/internal/Ref$LongRef;Lokio/ByteString;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->$retryCount:Lkotlin/jvm/internal/Ref$LongRef;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->$tmnDataByteString:Lokio/ByteString;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lio/reactivex/disposables/Disposable;)V
    .locals 10

    .line 508
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "TMN: sendProxyMessageToServer retryCount: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->$retryCount:Lkotlin/jvm/internal/Ref$LongRef;

    iget-wide v0, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " starting transId: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    iget-object v0, v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$transactionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " connId: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    iget-object v0, v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$connId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\nencrypted bytes: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->$tmnDataByteString:Lokio/ByteString;

    invoke-virtual {v0}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 507
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    iget-object p1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object p1

    .line 512
    new-instance v9, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 513
    sget-object v0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENDING_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v0}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 514
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    iget-object v2, v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$transactionId:Ljava/lang/String;

    .line 515
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    iget-object v4, v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$connId:Ljava/lang/String;

    .line 516
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->$retryCount:Lkotlin/jvm/internal/Ref$LongRef;

    iget-wide v5, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iput-wide v5, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    iget-wide v5, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v0, v9

    .line 512
    invoke-direct/range {v0 .. v8}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v9, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 511
    invoke-virtual {p1, v9}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 95
    check-cast p1, Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;->accept(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
