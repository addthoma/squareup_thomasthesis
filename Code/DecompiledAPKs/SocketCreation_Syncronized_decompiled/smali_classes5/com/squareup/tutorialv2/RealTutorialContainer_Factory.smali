.class public final Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;
.super Ljava/lang/Object;
.source "RealTutorialContainer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tutorialv2/RealTutorialContainer;",
        ">;"
    }
.end annotation


# instance fields
.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;->posContainerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/tutorialv2/RealTutorialContainer;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialContainer;

    invoke-direct {v0, p0}, Lcom/squareup/tutorialv2/RealTutorialContainer;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tutorialv2/RealTutorialContainer;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/tutorialv2/RealTutorialContainer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/RealTutorialContainer_Factory;->get()Lcom/squareup/tutorialv2/RealTutorialContainer;

    move-result-object v0

    return-object v0
.end method
