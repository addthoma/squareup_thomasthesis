.class Lcom/squareup/tutorialv2/view/TutorialTooltipController;
.super Ljava/lang/Object;
.source "TutorialTooltipController.java"


# instance fields
.field private final anchorScreenLocation:[I

.field private final arrowMarginX:I

.field private final arrowWidth:I

.field private final displayFrame:Landroid/graphics/Rect;

.field private downArrow:Landroid/view/View;

.field private final popup:Landroid/widget/PopupWindow;

.field private upArrow:Landroid/view/View;

.field private verticalOverflow:I


# direct methods
.method constructor <init>(Landroid/widget/PopupWindow;Landroid/content/res/Resources;)V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 49
    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    .line 60
    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->popup:Landroid/widget/PopupWindow;

    .line 61
    sget p1, Lcom/squareup/common/tutorial/R$dimen;->tooltip_tip_margin_horizontal:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->arrowMarginX:I

    .line 62
    sget p1, Lcom/squareup/common/tutorial/R$dimen;->tooltip_arrow_width:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->arrowWidth:I

    return-void
.end method

.method private align(Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;Landroid/view/WindowManager$LayoutParams;IIII)V
    .locals 4

    .line 161
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->checkingOrder()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 162
    sget-object v1, Lcom/squareup/tutorialv2/view/TutorialTooltipController$1;->$SwitchMap$com$squareup$tutorialv2$TutorialState$TooltipAlignment:[I

    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    aget v1, v1, v3

    div-int/lit8 v2, p4, 0x2

    add-int/2addr v1, v2

    div-int/lit8 v2, p3, 0x2

    sub-int/2addr v1, v2

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_0

    .line 167
    :cond_2
    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    aget v1, v1, v3

    sub-int/2addr v1, p3

    add-int/2addr v1, p4

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_0

    .line 164
    :cond_3
    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    aget v1, v1, v3

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 173
    :goto_0
    invoke-direct {p0, p2, p3, p5, p6}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->fitsHorizontal(Landroid/view/WindowManager$LayoutParams;III)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    iget p1, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-direct {p0, v0, p1, p4}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->alignArrows(Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;II)V

    :cond_4
    return-void
.end method

.method private alignArrows(Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;II)V
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    sub-int/2addr v0, p2

    .line 182
    sget-object p2, Lcom/squareup/tutorialv2/view/TutorialTooltipController$1;->$SwitchMap$com$squareup$tutorialv2$TutorialState$TooltipAlignment:[I

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 p2, 0x2

    if-eq p1, p2, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_1

    .line 189
    :cond_0
    iget p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->arrowWidth:I

    sub-int/2addr p3, p1

    div-int/2addr p3, p2

    iget p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->arrowMarginX:I

    goto :goto_0

    .line 186
    :cond_1
    iget p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->arrowWidth:I

    sub-int/2addr p3, p1

    iget p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->arrowMarginX:I

    mul-int/lit8 p1, p1, 0x2

    :goto_0
    sub-int/2addr p3, p1

    add-int/2addr v0, p3

    .line 192
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->upArrow:Landroid/view/View;

    int-to-float p2, v0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 193
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->downArrow:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    return-void
.end method

.method private determineGravity(Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Landroid/view/WindowManager$LayoutParams;IIII)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 132
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->opposite()Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 133
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->positionWithGravity(Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Landroid/view/WindowManager$LayoutParams;II)V

    .line 134
    invoke-direct {p0, p2, p3, p5, p6}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->fitsVertical(Landroid/view/WindowManager$LayoutParams;III)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private fitsHorizontal(Landroid/view/WindowManager$LayoutParams;III)Z
    .locals 1

    .line 198
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v0, p2

    if-gt v0, p4, :cond_0

    iget p1, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    if-lt p1, p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private fitsVertical(Landroid/view/WindowManager$LayoutParams;III)Z
    .locals 1

    .line 203
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v0, p2

    if-gt v0, p4, :cond_0

    iget p1, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    if-lt p1, p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private positionWithGravity(Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Landroid/view/WindowManager$LayoutParams;II)V
    .locals 3

    .line 143
    sget-object v0, Lcom/squareup/tutorialv2/view/TutorialTooltipController$1;->$SwitchMap$com$squareup$tutorialv2$TutorialState$TooltipGravity:[I

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/16 v0, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v1, :cond_1

    const/4 p3, 0x2

    if-eq p1, p3, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    .line 151
    :cond_0
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->upArrow:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->downArrow:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    neg-int p4, p3

    .line 146
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->upArrow:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->downArrow:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 155
    :goto_0
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    aget p1, p1, v1

    add-int/2addr p1, p4

    iput p1, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    return-void
.end method


# virtual methods
.method public alignToAnchor(Landroid/view/View;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;)V
    .locals 10

    if-eqz p1, :cond_2

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->popup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 92
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 94
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 95
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 96
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 100
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    .line 102
    iget-object v3, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->anchorScreenLocation:[I

    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 104
    iget-object v3, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 105
    iget-object v2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->verticalOverflow:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 106
    iget-object v2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->verticalOverflow:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 108
    iget-object v2, p2, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;->alignment:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 109
    invoke-virtual {p1}, Landroid/view/View;->getLayoutDirection()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 110
    invoke-virtual {v2}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->rtl()Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    move-result-object v2

    :cond_0
    move-object v3, v2

    const/4 v2, -0x1

    if-eq v5, v2, :cond_1

    .line 113
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    iget v7, p1, Landroid/graphics/Rect;->left:I

    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    iget v8, p1, Landroid/graphics/Rect;->right:I

    move-object v2, p0

    move-object v4, v1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->align(Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;Landroid/view/WindowManager$LayoutParams;IIII)V

    goto :goto_0

    .line 116
    :cond_1
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-direct {p0, v3, v2, p1}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->alignArrows(Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;II)V

    .line 119
    :goto_0
    iget-object v3, p2, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;->gravity:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    iget v7, p1, Landroid/graphics/Rect;->top:I

    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->displayFrame:Landroid/graphics/Rect;

    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    move-object v2, p0

    move-object v4, v1

    move v5, v0

    move v6, v9

    invoke-direct/range {v2 .. v8}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->determineGravity(Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Landroid/view/WindowManager$LayoutParams;IIII)V

    const p1, 0x800033

    .line 123
    iput p1, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 125
    iget-object v2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->popup:Landroid/widget/PopupWindow;

    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Landroid/widget/PopupWindow;->update(IIIIZ)V

    :cond_2
    return-void
.end method

.method public bindArrowViews(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->upArrow:Landroid/view/View;

    .line 67
    iput-object p2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->downArrow:Landroid/view/View;

    return-void
.end method

.method public setVerticalOverflow(I)V
    .locals 0

    .line 80
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->verticalOverflow:I

    return-void
.end method

.method public unbindArrowViews()V
    .locals 1

    const/4 v0, 0x0

    .line 71
    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->upArrow:Landroid/view/View;

    .line 72
    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->downArrow:Landroid/view/View;

    return-void
.end method
