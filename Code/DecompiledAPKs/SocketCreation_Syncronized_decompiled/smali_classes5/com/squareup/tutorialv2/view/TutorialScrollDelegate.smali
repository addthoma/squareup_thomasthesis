.class public final Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;
.super Ljava/lang/Object;
.source "TutorialScrollDelegate.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0002\u0010\tJ\u001e\u0010\u000c\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0003R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;",
        "",
        "position",
        "",
        "onItemVisible",
        "Lkotlin/Function0;",
        "",
        "condition",
        "",
        "(ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "lastFirstVisibleItem",
        "lastVisibleItemCount",
        "onScroll",
        "firstVisibleItem",
        "visibleItemCount",
        "totalItemCount",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final condition:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lastFirstVisibleItem:I

.field private lastVisibleItemCount:I

.field private final onItemVisible:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final position:I


# direct methods
.method public constructor <init>(ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onItemVisible"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "condition"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->position:I

    iput-object p2, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->onItemVisible:Lkotlin/jvm/functions/Function0;

    iput-object p3, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->condition:Lkotlin/jvm/functions/Function0;

    const/4 p1, -0x1

    .line 8
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->lastFirstVisibleItem:I

    .line 9
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->lastVisibleItemCount:I

    return-void
.end method


# virtual methods
.method public final onScroll(III)V
    .locals 1

    if-lez p2, :cond_2

    if-lez p3, :cond_2

    .line 21
    iget p3, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->lastFirstVisibleItem:I

    if-ne p3, p1, :cond_0

    iget p3, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->lastVisibleItemCount:I

    if-eq p3, p2, :cond_1

    :cond_0
    iget p3, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->position:I

    if-lt p3, p1, :cond_1

    add-int v0, p1, p2

    if-gt p3, v0, :cond_1

    iget-object p3, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->condition:Lkotlin/jvm/functions/Function0;

    invoke-interface {p3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 22
    iget-object p3, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->onItemVisible:Lkotlin/jvm/functions/Function0;

    invoke-interface {p3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 24
    :cond_1
    iput p1, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->lastFirstVisibleItem:I

    .line 25
    iput p2, p0, Lcom/squareup/tutorialv2/view/TutorialScrollDelegate;->lastVisibleItemCount:I

    :cond_2
    return-void
.end method
