.class abstract Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;
.super Ljava/lang/Object;
.source "RealTutorialCore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/RealTutorialCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "TutorialEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ValueEvent;,
        Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0002\u000b\u000cB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "",
        "name",
        "",
        "(Ljava/lang/String;)V",
        "getName",
        "()Ljava/lang/String;",
        "sendToTutorial",
        "",
        "tutorial",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "ActionEvent",
        "ValueEvent",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ValueEvent;",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;->name:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 296
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;->name:Ljava/lang/String;

    return-object v0
.end method

.method public abstract sendToTutorial(Lcom/squareup/tutorialv2/Tutorial;)V
.end method
