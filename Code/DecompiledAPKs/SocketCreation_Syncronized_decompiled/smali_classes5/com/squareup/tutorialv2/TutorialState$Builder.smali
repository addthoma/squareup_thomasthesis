.class public final Lcom/squareup/tutorialv2/TutorialState$Builder;
.super Ljava/lang/Object;
.source "TutorialState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\u0008\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0011\u0008\u0012\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u0006J\u0006\u0010\u0018\u001a\u00020\u0019J\u0010\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u0006J\u0018\u0010\u000c\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u001cJ \u0010\u000c\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\u0018\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eJ\u0018\u0010 \u001a\u00020\u00002\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000eJ\u0016\u0010\u0012\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0006J\u0016\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u00012\u0006\u0010\u001b\u001a\u00020\u001cJ\u001e\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u00012\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00020\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u00020\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialState$Builder;",
        "",
        "content",
        "",
        "(Ljava/lang/CharSequence;)V",
        "contentId",
        "",
        "(I)V",
        "anchorState",
        "Lcom/squareup/tutorialv2/TutorialState$AnchorState;",
        "backgroundColor",
        "closeButtonColor",
        "idAnchor",
        "primaryButtonEvent",
        "",
        "primaryButtonTextId",
        "secondaryButtonEvent",
        "secondaryButtonTextId",
        "step",
        "stepCount",
        "tagAnchor",
        "tooltipPosition",
        "Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;",
        "color",
        "build",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "anchor",
        "tooltipGravity",
        "Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;",
        "tooltipAlignment",
        "Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;",
        "primaryButton",
        "secondaryButton",
        "Companion",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;


# instance fields
.field private anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

.field private backgroundColor:I

.field private closeButtonColor:I

.field private final content:Ljava/lang/CharSequence;

.field private final contentId:I

.field private idAnchor:I

.field private primaryButtonEvent:Ljava/lang/String;

.field private primaryButtonTextId:I

.field private secondaryButtonEvent:Ljava/lang/String;

.field private secondaryButtonTextId:I

.field private step:I

.field private stepCount:I

.field private tagAnchor:Ljava/lang/Object;

.field private tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->Companion:Lcom/squareup/tutorialv2/TutorialState$Builder$Companion;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->NO_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    iput-object v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    const/4 v0, -0x1

    .line 187
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor:I

    .line 192
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButtonTextId:I

    .line 194
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButtonTextId:I

    .line 196
    sget v0, Lcom/squareup/marin/R$color;->marin_green:I

    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->backgroundColor:I

    .line 197
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->closeButtonColor:I

    .line 205
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->contentId:I

    const/4 p1, 0x0

    .line 206
    check-cast p1, Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->content:Ljava/lang/CharSequence;

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;-><init>(I)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;)V
    .locals 2

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->NO_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    iput-object v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    const/4 v0, -0x1

    .line 187
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor:I

    .line 192
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButtonTextId:I

    .line 194
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButtonTextId:I

    .line 196
    sget v1, Lcom/squareup/marin/R$color;->marin_green:I

    iput v1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->backgroundColor:I

    .line 197
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    iput v1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->closeButtonColor:I

    .line 200
    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->content:Ljava/lang/CharSequence;

    .line 201
    iput v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->contentId:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/CharSequence;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;-><init>(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final backgroundColor(I)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 0

    .line 273
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->backgroundColor:I

    return-object p0
.end method

.method public final build()Lcom/squareup/tutorialv2/TutorialState;
    .locals 18

    move-object/from16 v0, p0

    .line 283
    new-instance v17, Lcom/squareup/tutorialv2/TutorialState;

    .line 284
    iget-object v2, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    .line 285
    iget-object v3, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->content:Ljava/lang/CharSequence;

    .line 286
    iget v4, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->contentId:I

    .line 287
    iget v5, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor:I

    .line 288
    iget-object v6, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor:Ljava/lang/Object;

    .line 289
    iget-object v7, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    .line 290
    iget v8, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->step:I

    .line 291
    iget v9, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->stepCount:I

    .line 292
    iget v10, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButtonTextId:I

    .line 293
    iget-object v11, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButtonEvent:Ljava/lang/String;

    .line 294
    iget v12, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButtonTextId:I

    .line 295
    iget-object v13, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButtonEvent:Ljava/lang/String;

    .line 296
    iget v14, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->backgroundColor:I

    .line 297
    iget v15, v0, Lcom/squareup/tutorialv2/TutorialState$Builder;->closeButtonColor:I

    const/16 v16, 0x0

    move-object/from16 v1, v17

    .line 283
    invoke-direct/range {v1 .. v16}, Lcom/squareup/tutorialv2/TutorialState;-><init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v17
.end method

.method public final closeButtonColor(I)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 0

    .line 278
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->closeButtonColor:I

    return-object p0
.end method

.method public final idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1

    const-string/jumbo v0, "tooltipGravity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    return-object p0
.end method

.method public final idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 2

    const-string/jumbo v0, "tooltipGravity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipAlignment"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->NO_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    if-ne v0, v1, :cond_0

    .line 230
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->ID_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    iput-object v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    .line 231
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor:I

    .line 233
    new-instance p1, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    invoke-direct {p1, p2, p3}, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;-><init>(Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    return-object p0

    .line 228
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "idAnchor or tagAnchor has already been set."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1

    const-string v0, "primaryButtonEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButtonTextId:I

    .line 262
    iput-object p2, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButtonEvent:Ljava/lang/String;

    return-object p0
.end method

.method public final secondaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1

    const-string v0, "secondaryButtonEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButtonTextId:I

    .line 268
    iput-object p2, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButtonEvent:Ljava/lang/String;

    return-object p0
.end method

.method public final step(II)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 0

    .line 213
    iput p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->step:I

    .line 214
    iput p2, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->stepCount:I

    return-object p0
.end method

.method public final tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1

    const-string v0, "tagAnchor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipGravity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    return-object p0
.end method

.method public final tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 2

    const-string v0, "tagAnchor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipGravity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipAlignment"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->NO_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    if-ne v0, v1, :cond_0

    .line 253
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->TAG_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    iput-object v0, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    .line 254
    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor:Ljava/lang/Object;

    .line 256
    new-instance p1, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    invoke-direct {p1, p2, p3}, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;-><init>(Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialState$Builder;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    return-object p0

    .line 251
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "idAnchor or tagAnchor has already been set."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
