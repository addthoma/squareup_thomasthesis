.class public final Lcom/squareup/tutorialv2/RealTutorialCoordinator;
.super Lcom/squareup/tutorialv2/TutorialCoordinator;
.source "RealTutorialCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0008\u0010\u0010\u001a\u00020\rH\u0002J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0015\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\nH\u0016J\u000c\u0010\u0016\u001a\u00020\u0017*\u00020\u0013H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/RealTutorialCoordinator;",
        "Lcom/squareup/tutorialv2/TutorialCoordinator;",
        "core",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V",
        "banner",
        "Lcom/squareup/tutorialv2/view/TutorialView;",
        "rootView",
        "Landroid/view/View;",
        "tooltip",
        "attach",
        "",
        "view",
        "detach",
        "hideAllViews",
        "maybeFindAnchor",
        "state",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "onTutorialState",
        "setRootView",
        "stepsText",
        "",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private banner:Lcom/squareup/tutorialv2/view/TutorialView;

.field private final core:Lcom/squareup/tutorialv2/TutorialCore;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private rootView:Landroid/view/View;

.field private tooltip:Lcom/squareup/tutorialv2/view/TutorialView;


# direct methods
.method public constructor <init>(Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "core"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCoordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->core:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getCore$p(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->core:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method

.method public static final synthetic access$onTutorialState(Lcom/squareup/tutorialv2/RealTutorialCoordinator;Lcom/squareup/tutorialv2/TutorialState;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->onTutorialState(Lcom/squareup/tutorialv2/TutorialState;)V

    return-void
.end method

.method private final hideAllViews()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->banner:Lcom/squareup/tutorialv2/view/TutorialView;

    if-nez v0, :cond_0

    const-string v1, "banner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/tutorialv2/view/TutorialView;->hide()V

    .line 108
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->tooltip:Lcom/squareup/tutorialv2/view/TutorialView;

    if-nez v0, :cond_1

    const-string/jumbo v1, "tooltip"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v0}, Lcom/squareup/tutorialv2/view/TutorialView;->hide()V

    return-void
.end method

.method private final maybeFindAnchor(Lcom/squareup/tutorialv2/TutorialState;)Landroid/view/View;
    .locals 2

    .line 99
    iget-object v0, p1, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 102
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->rootView:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object p1, p1, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->rootView:Landroid/view/View;

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    iget p1, p1, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_4
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final onTutorialState(Lcom/squareup/tutorialv2/TutorialState;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Received TutorialState: %s"

    .line 50
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState;->hasNoContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->hideAllViews()V

    return-void

    .line 55
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->maybeFindAnchor(Lcom/squareup/tutorialv2/TutorialState;)Landroid/view/View;

    move-result-object v0

    const-string v1, "banner"

    const-string/jumbo v2, "tooltip"

    if-eqz v0, :cond_2

    .line 58
    iget-object v3, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->banner:Lcom/squareup/tutorialv2/view/TutorialView;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v3}, Lcom/squareup/tutorialv2/view/TutorialView;->hide()V

    .line 59
    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->tooltip:Lcom/squareup/tutorialv2/view/TutorialView;

    if-nez v1, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->rootView:Landroid/view/View;

    .line 62
    iget-object v3, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->tooltip:Lcom/squareup/tutorialv2/view/TutorialView;

    if-nez v3, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-interface {v3}, Lcom/squareup/tutorialv2/view/TutorialView;->hide()V

    .line 63
    iget-object v2, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->banner:Lcom/squareup/tutorialv2/view/TutorialView;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    move-object v1, v2

    .line 65
    :cond_5
    :goto_0
    iget v2, p1, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_7

    .line 66
    iget v2, p1, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    new-instance v4, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;

    invoke-direct {v4, p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;-><init>(Lcom/squareup/tutorialv2/RealTutorialCoordinator;Lcom/squareup/tutorialv2/TutorialState;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-interface {v1, v2, v4}, Lcom/squareup/tutorialv2/view/TutorialView;->setPrimaryButton(ILandroid/view/View$OnClickListener;)V

    .line 69
    iget v2, p1, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    if-eq v2, v3, :cond_6

    .line 70
    iget v2, p1, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    new-instance v3, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$2;

    invoke-direct {v3, p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$2;-><init>(Lcom/squareup/tutorialv2/RealTutorialCoordinator;Lcom/squareup/tutorialv2/TutorialState;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-interface {v1, v2, v3}, Lcom/squareup/tutorialv2/view/TutorialView;->setSecondaryButton(ILandroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 74
    :cond_6
    invoke-interface {v1}, Lcom/squareup/tutorialv2/view/TutorialView;->hideSecondaryButton()V

    goto :goto_1

    .line 77
    :cond_7
    invoke-interface {v1}, Lcom/squareup/tutorialv2/view/TutorialView;->hidePrimaryButton()V

    .line 79
    invoke-interface {v1}, Lcom/squareup/tutorialv2/view/TutorialView;->hideSecondaryButton()V

    .line 81
    :goto_1
    iget v2, p1, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    invoke-interface {v1, v2}, Lcom/squareup/tutorialv2/view/TutorialView;->setViewBackgroundColor(I)V

    .line 82
    iget v2, p1, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    invoke-interface {v1, v2}, Lcom/squareup/tutorialv2/view/TutorialView;->setCloseButtonColor(I)V

    .line 83
    invoke-interface {v1}, Lcom/squareup/tutorialv2/view/TutorialView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "tutorialView.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/squareup/tutorialv2/TutorialState;->content(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/tutorialv2/view/TutorialView;->setContentText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->stepsText(Lcom/squareup/tutorialv2/TutorialState;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/tutorialv2/view/TutorialView;->setStepsText(Ljava/lang/CharSequence;)V

    .line 85
    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3;

    invoke-direct {v2, p0}, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3;-><init>(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)V

    check-cast v2, Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;

    invoke-interface {v1, v2}, Lcom/squareup/tutorialv2/view/TutorialView;->setOnInteractionListener(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V

    .line 94
    iget-object p1, p1, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    invoke-interface {v1, v0, p1}, Lcom/squareup/tutorialv2/view/TutorialView;->show(Landroid/view/View;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;)V

    return-void
.end method

.method private final stepsText(Lcom/squareup/tutorialv2/TutorialState;)Ljava/lang/CharSequence;
    .locals 3

    .line 112
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState;->hasStep()Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, ""

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->rootView:Landroid/view/View;

    sget v1, Lcom/squareup/common/tutorial/R$string;->tutorial_progress:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 114
    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->step:I

    const-string v2, "step"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 115
    iget p1, p1, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    const-string v1, "step_count"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "Phrase.from(rootView, R.\u2026pCount)\n        .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    move-object v0, p1

    check-cast v0, Lcom/squareup/tutorialv2/view/TutorialBannerView;

    check-cast v0, Lcom/squareup/tutorialv2/view/TutorialView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->banner:Lcom/squareup/tutorialv2/view/TutorialView;

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->create(Landroid/content/Context;)Lcom/squareup/tutorialv2/view/TutorialView;

    move-result-object v0

    const-string v1, "TutorialTooltipView.create(view.getContext())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->tooltip:Lcom/squareup/tutorialv2/view/TutorialView;

    .line 34
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->core:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-interface {v0}, Lcom/squareup/tutorialv2/TutorialCore;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "core.state()\n        .observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCoordinator$attach$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/tutorialv2/RealTutorialCoordinator$attach$1;-><init>(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-super {p0, p1}, Lcom/squareup/tutorialv2/TutorialCoordinator;->detach(Landroid/view/View;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->hideAllViews()V

    const/4 p1, 0x0

    .line 42
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->rootView:Landroid/view/View;

    return-void
.end method

.method public setRootView(Landroid/view/View;)V
    .locals 1

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->rootView:Landroid/view/View;

    return-void
.end method
