.class public final Lcom/squareup/tutorialv2/TutorialV2DialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "TutorialV2DialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;,
        Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Factory;,
        Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\u0008\u0007\u0018\u0000 \u000c2\u00020\u00012\u00020\u0002:\u0003\u000c\r\u000eB\u0019\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0008\u001a\u00020\u0006X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "prompt",
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;",
        "isX2",
        "",
        "(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V",
        "isPersistent",
        "()Z",
        "getPrompt",
        "()Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;",
        "Companion",
        "Factory",
        "Prompt",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Companion;

.field public static final PRIMARY_TAPPED:Ljava/lang/String; = "TutorialV2DialogScreen primary tapped"

.field public static final SECONDARY_TAPPED:Ljava/lang/String; = "TutorialV2DialogScreen secondary tapped"


# instance fields
.field private final isPersistent:Z

.field private final isX2:Z

.field private final prompt:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->Companion:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V
    .locals 1

    const-string v0, "prompt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->prompt:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    iput-boolean p2, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->isX2:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 22
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V

    return-void
.end method

.method public static final synthetic access$isX2$p(Lcom/squareup/tutorialv2/TutorialV2DialogScreen;)Z
    .locals 0

    .line 20
    iget-boolean p0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->isX2:Z

    return p0
.end method


# virtual methods
.method public final getPrompt()Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->prompt:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->isPersistent:Z

    return v0
.end method
