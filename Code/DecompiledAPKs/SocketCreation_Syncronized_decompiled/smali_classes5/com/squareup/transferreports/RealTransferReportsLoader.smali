.class public final Lcom/squareup/transferreports/RealTransferReportsLoader;
.super Ljava/lang/Object;
.source "RealTransferReportsLoader.kt"

# interfaces
.implements Lcom/squareup/transferreports/TransferReportsLoader;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTransferReportsLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTransferReportsLoader.kt\ncom/squareup/transferreports/RealTransferReportsLoader\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,294:1\n1360#2:295\n1429#2,3:296\n1360#2:299\n1429#2,3:300\n*E\n*S KotlinDebug\n*F\n+ 1 RealTransferReportsLoader.kt\ncom/squareup/transferreports/RealTransferReportsLoader\n*L\n256#1:295\n256#1,3:296\n268#1:299\n268#1,3:300\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0014\u0010\u0012\u001a\u00020\u00132\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0011H\u0002J\u0016\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J2\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0011H\u0016J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u0017H\u0016J\u0016\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u0017H\u0016J\u0010\u0010\"\u001a\u00020#2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J2\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0011H\u0002J\u0010\u0010%\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J\u0018\u0010&\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u000e\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0002J\u0016\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J\u0010\u0010)\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J(\u0010*\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020#H\u0002J\u0016\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J\u0010\u0010/\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0002J\u0018\u00100\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u00101\u001a\u000202H\u0002J\u0018\u00103\u001a\u00020\u00172\u0006\u00104\u001a\u0002052\u0006\u00101\u001a\u000202H\u0002J\u001c\u00106\u001a\u0008\u0012\u0004\u0012\u000208072\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020\u001d07H\u0002J*\u0010:\u001a\u0008\u0012\u0004\u0012\u00020;072\u000c\u0010<\u001a\u0008\u0012\u0004\u0012\u00020;0=2\u000c\u0010>\u001a\u0008\u0012\u0004\u0012\u00020?07H\u0002J4\u0010@\u001a&\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010A0A \u000f*\u0012\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010A0A\u0018\u0001070=2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u000f*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006B"
    }
    d2 = {
        "Lcom/squareup/transferreports/RealTransferReportsLoader;",
        "Lcom/squareup/transferreports/TransferReportsLoader;",
        "transferReportsService",
        "Lcom/squareup/server/reporting/TransferReportsService;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/server/reporting/TransferReportsService;Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V",
        "_pendingSettlementsRequest",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;",
        "_requestParams",
        "Lcom/squareup/protos/client/settlements/RequestParams;",
        "kotlin.jvm.PlatformType",
        "_tzName",
        "",
        "createRequest",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;",
        "batchToken",
        "getDepositDetails",
        "Lio/reactivex/Single;",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "currentSnapshot",
        "getTransferDetails",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "settlementReport",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "settlementToken",
        "getTransferReports",
        "loadBillEntries",
        "loadMore",
        "maxIndex",
        "",
        "onGetDepositDetails",
        "onGetDepositDetailsFailure",
        "onGetDepositDetailsSuccess",
        "onGetTransferReports",
        "onLoadBillEntries",
        "onLoadBillEntriesFailure",
        "onLoadBillEntriesSuccess",
        "settledBillEntryResponse",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
        "settledBillStartIndex",
        "onLoadMore",
        "onLoadMoreFailure",
        "onLoadMoreSuccess",
        "settlementReportLiteResponse",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "onSuccess",
        "pendingSettlementsResponse",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
        "parsePendingSettlements",
        "",
        "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
        "pendingSettlementReport",
        "parseSettlementsReport",
        "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
        "settlementReportList",
        "",
        "settlements",
        "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
        "settledBillEntryTokenGroup",
        "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _pendingSettlementsRequest:Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;

.field private final _requestParams:Lcom/squareup/protos/client/settlements/RequestParams;

.field private final _tzName:Ljava/lang/String;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/reporting/TransferReportsService;Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transferReportsService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;

    iput-object p3, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p4, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->features:Lcom/squareup/settings/server/Features;

    .line 51
    invoke-interface {p2}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object p1

    const-string p2, "clock.timeZone"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_tzName:Ljava/lang/String;

    .line 52
    new-instance p1, Lcom/squareup/protos/client/settlements/RequestParams$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/settlements/RequestParams$Builder;-><init>()V

    .line 53
    iget-object p2, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_tzName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->tz_name(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/RequestParams$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->build()Lcom/squareup/protos/client/settlements/RequestParams;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_requestParams:Lcom/squareup/protos/client/settlements/RequestParams;

    .line 55
    new-instance p1, Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;

    iget-object p2, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_requestParams:Lcom/squareup/protos/client/settlements/RequestParams;

    invoke-direct {p1, p2}, Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;-><init>(Lcom/squareup/protos/client/settlements/RequestParams;)V

    iput-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_pendingSettlementsRequest:Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;

    return-void
.end method

.method public static final synthetic access$onGetDepositDetailsFailure(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onGetDepositDetailsFailure(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetDepositDetailsSuccess(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onGetDepositDetailsSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadBillEntriesFailure(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadBillEntriesFailure(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadBillEntriesSuccess(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;I)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadBillEntriesSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;I)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadMoreFailure(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadMoreFailure(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadMoreSuccess(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadMoreSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onSuccess(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onSuccess(Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method private final createRequest(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
    .locals 3

    .line 281
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;-><init>()V

    .line 282
    iget-object v1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_requestParams:Lcom/squareup/protos/client/settlements/RequestParams;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params(Lcom/squareup/protos/client/settlements/RequestParams;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 285
    new-instance v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;-><init>()V

    .line 286
    sget-object v2, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;->DEFAULT_BATCH_SIZE:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->batch_size(Ljava/lang/Integer;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;

    move-result-object v1

    .line 287
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->batch_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;

    move-result-object p1

    .line 288
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 283
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    move-result-object p1

    .line 291
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object p1

    const-string v0, "SettlementReportLiteRequ\u2026       )\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic createRequest$default(Lcom/squareup/transferreports/RealTransferReportsLoader;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 280
    check-cast p1, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->createRequest(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object p0

    return-object p0
.end method

.method private final getDepositDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 160
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_requestParams:Lcom/squareup/protos/client/settlements/RequestParams;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->request_params(Lcom/squareup/protos/client/settlements/RequestParams;)Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;

    move-result-object v0

    .line 162
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettlementToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->settlement_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/squareup/protos/client/settlements/SettlementReportRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportRequest;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/reporting/TransferReportsService;->getSettlementReport(Lcom/squareup/protos/client/settlements/SettlementReportRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 167
    new-instance v1, Lcom/squareup/transferreports/RealTransferReportsLoader$getDepositDetails$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader$getDepositDetails$1;-><init>(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "transferReportsService.g\u2026ot)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final maxIndex(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)I
    .locals 0

    .line 252
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->settledBillEntryTokenGroup(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    return p1
.end method

.method private final onGetDepositDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 109
    sget-object v7, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    .line 110
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1f

    const/4 v15, 0x0

    move-object/from16 v1, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    .line 108
    invoke-static/range {v1 .. v15}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettlementReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 119
    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onGetDepositDetailsSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 120
    :cond_0
    invoke-direct {v0, v1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->getDepositDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;

    move-result-object v2

    :goto_0
    return-object v2
.end method

.method private final onGetDepositDetailsFailure(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 18

    move-object/from16 v0, p0

    .line 196
    iget-object v1, v0, Lcom/squareup/transferreports/RealTransferReportsLoader;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterErrorName;->DEPOSITS_REPORT_DETAIL_LOAD_DEPOSIT_DETAILS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 197
    sget-object v9, Lcom/squareup/transferreports/TransferReportsLoader$State;->FAILURE:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xfdf

    const/16 v17, 0x0

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v17}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    return-object v1
.end method

.method private final onGetDepositDetailsSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v11, p2

    .line 181
    invoke-direct {v0, v11}, Lcom/squareup/transferreports/RealTransferReportsLoader;->maxIndex(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    const/4 v13, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v13, 0x0

    .line 183
    :goto_0
    iget-object v1, v0, Lcom/squareup/transferreports/RealTransferReportsLoader;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_CARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v13, :cond_1

    .line 184
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    goto :goto_1

    .line 186
    :cond_1
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    :goto_1
    move-object v7, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/16 v14, 0x5df

    const/4 v15, 0x0

    move-object/from16 v1, p1

    move-object/from16 v11, p2

    .line 188
    invoke-static/range {v1 .. v15}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    return-object v1
.end method

.method private final onGetTransferReports()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;

    iget-object v1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_pendingSettlementsRequest:Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;

    invoke-interface {v0, v1}, Lcom/squareup/server/reporting/TransferReportsService;->getPendingSettlements(Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    check-cast v0, Lio/reactivex/SingleSource;

    .line 73
    iget-object v1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {p0, v2, v3, v2}, Lcom/squareup/transferreports/RealTransferReportsLoader;->createRequest$default(Lcom/squareup/transferreports/RealTransferReportsLoader;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/server/reporting/TransferReportsService;->getSettlementReportLite(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    check-cast v1, Lio/reactivex/SingleSource;

    .line 74
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 71
    invoke-static {v0, v1, v2}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/transferreports/RealTransferReportsLoader$onGetTransferReports$1;

    invoke-direct {v1, p0}, Lcom/squareup/transferreports/RealTransferReportsLoader$onGetTransferReports$1;-><init>(Lcom/squareup/transferreports/RealTransferReportsLoader;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string/jumbo v1, "zip(\n        transferRep\u2026RE)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onLoadBillEntries(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 201
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettlementReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 203
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettledBillStartIndex()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    invoke-direct {p0, v0}, Lcom/squareup/transferreports/RealTransferReportsLoader;->maxIndex(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 204
    new-instance v2, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;-><init>()V

    .line 206
    invoke-direct {p0, v0}, Lcom/squareup/transferreports/RealTransferReportsLoader;->settledBillEntryTokenGroup(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Ljava/util/List;

    move-result-object v3

    .line 207
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettledBillStartIndex()I

    move-result v4

    .line 206
    invoke-interface {v3, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    .line 205
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->settled_bill_entry_token_group(Ljava/util/List;)Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;

    move-result-object v2

    .line 210
    iget-object v3, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->_tzName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->tz_name(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;

    move-result-object v2

    .line 211
    invoke-virtual {v2}, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;

    move-result-object v2

    .line 213
    iget-object v3, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;

    const-string v4, "request"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/server/reporting/TransferReportsService;->getSettledBillEntries(Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v2

    .line 214
    invoke-virtual {v2}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    .line 215
    new-instance v3, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;-><init>(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;I)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "transferReportsService.g\u2026ot)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onLoadBillEntriesFailure(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 18

    move-object/from16 v0, p0

    .line 244
    iget-object v1, v0, Lcom/squareup/transferreports/RealTransferReportsLoader;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterErrorName;->DEPOSITS_REPORT_DETAIL_LOAD_BILL_ENTRIES_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 245
    sget-object v9, Lcom/squareup/transferreports/TransferReportsLoader$State;->FAILURE:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xfdf

    const/16 v17, 0x0

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v17}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    return-object v1
.end method

.method private final onLoadBillEntriesSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;I)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 16

    .line 232
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettledBillEntriesResponseList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v8

    move-object/from16 v0, p3

    .line 233
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    sget-object v7, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-object/from16 v0, p0

    move-object/from16 v11, p2

    .line 239
    invoke-direct {v0, v11}, Lcom/squareup/transferreports/RealTransferReportsLoader;->maxIndex(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)I

    move-result v1

    move/from16 v9, p4

    if-ge v9, v1, :cond_0

    const/4 v1, 0x1

    const/4 v13, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v13, 0x0

    :goto_0
    const/16 v14, 0x51f

    const/4 v15, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v1, p1

    move/from16 v9, p4

    move-object/from16 v11, p2

    .line 234
    invoke-static/range {v1 .. v15}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    return-object v1
.end method

.method private final onLoadMore(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 87
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasNextBatchToken()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(currentSnapshot)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/transferreports/RealTransferReportsLoader;->transferReportsService:Lcom/squareup/server/reporting/TransferReportsService;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->nextBatchToken()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->createRequest(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object v1

    .line 90
    invoke-interface {v0, v1}, Lcom/squareup/server/reporting/TransferReportsService;->getSettlementReportLite(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 94
    new-instance v1, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadMore$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadMore$1;-><init>(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "transferReportsService.g\u2026ot)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onLoadMoreFailure(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 15

    .line 142
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->FAILURE:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xffe

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static/range {v0 .. v14}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    return-object v0
.end method

.method private final onLoadMoreSuccess(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 16

    .line 150
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettlementReportList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v4, p2

    .line 154
    iget-object v2, v4, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    const-string v3, "settlementReportLiteResponse.settlements"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v15, p0

    .line 152
    invoke-direct {v15, v0, v2}, Lcom/squareup/transferreports/RealTransferReportsLoader;->parseSettlementsReport(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfe6

    const/4 v14, 0x0

    move-object/from16 v0, p1

    .line 149
    invoke-static/range {v0 .. v14}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    return-object v0
.end method

.method private final onSuccess(Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 17

    move-object/from16 v0, p0

    .line 128
    sget-object v2, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-object/from16 v3, p1

    .line 131
    iget-object v1, v3, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;->pending_settlement_report:Ljava/util/List;

    const-string v4, "pendingSettlementsRespon\u2026pending_settlement_report"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-direct {v0, v1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->parsePendingSettlements(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    move-object/from16 v5, p2

    .line 136
    iget-object v6, v5, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    const-string v7, "settlementReportLiteResponse.settlements"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-direct {v0, v1, v6}, Lcom/squareup/transferreports/RealTransferReportsLoader;->parseSettlementsReport(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 127
    new-instance v16, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xfe0

    const/4 v15, 0x0

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v16
.end method

.method private final parsePendingSettlements(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
            ">;"
        }
    .end annotation

    .line 256
    check-cast p1, Ljava/lang/Iterable;

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 296
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 297
    check-cast v1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    .line 258
    iget-object v2, v1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    const-string v3, "it.settlement.settlement_money"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, v3}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    .line 257
    :goto_1
    new-instance v3, Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;

    invoke-direct {v3, v2, v1}, Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/Date;)V

    .line 260
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final parseSettlementsReport(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;"
        }
    .end annotation

    .line 268
    check-cast p2, Ljava/lang/Iterable;

    .line 299
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 300
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 301
    check-cast v1, Lcom/squareup/protos/client/settlements/SettlementReportLite;

    .line 269
    new-instance v2, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;

    .line 270
    iget-object v3, v1, Lcom/squareup/protos/client/settlements/SettlementReportLite;->settlement_money:Lcom/squareup/protos/common/Money;

    const-string v4, "it.settlement_money"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    iget-object v4, v1, Lcom/squareup/protos/client/settlements/SettlementReportLite;->created_at:Lcom/squareup/protos/common/time/DateTime;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v4, v5}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v4

    const-string v5, "asDate(it.created_at, US)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    iget-object v5, v1, Lcom/squareup/protos/client/settlements/SettlementReportLite;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    const-string v6, "it.settlement_type"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReportLite;->token:Ljava/lang/String;

    const-string v6, "it.token"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/protos/client/settlements/SettlementType;Ljava/lang/String;)V

    .line 274
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 302
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 276
    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p1
.end method

.method private final settledBillEntryTokenGroup(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;"
        }
    .end annotation

    .line 249
    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getTransferDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    const-string v0, "currentSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onGetDepositDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getTransferReports()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onGetTransferReports()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public loadBillEntries(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    const-string v0, "currentSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadBillEntries(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public loadMore(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    const-string v0, "currentSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadMore(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
