.class final Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsWorkflow;->render(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/transferreports/TransferReportsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/transferreports/TransferReportsState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/transferreports/TransferReportsState;",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/transferreports/TransferReportsProps;

.field final synthetic $state:Lcom/squareup/transferreports/TransferReportsState;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/transferreports/TransferReportsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;->$props:Lcom/squareup/transferreports/TransferReportsProps;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;->$state:Lcom/squareup/transferreports/TransferReportsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/transferreports/TransferReportsState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;->$props:Lcom/squareup/transferreports/TransferReportsProps;

    .line 86
    sget-object v0, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsSummaryProps;->INSTANCE:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsSummaryProps;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;

    .line 87
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;->$state:Lcom/squareup/transferreports/TransferReportsState;

    check-cast v0, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;->getDetailProps()Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;->$state:Lcom/squareup/transferreports/TransferReportsState;

    check-cast v1, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;->getShowCurrentBalanceAndActiveSales()Z

    move-result v1

    .line 86
    invoke-direct {p1, v0, v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Z)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 89
    :cond_0
    instance-of p1, p1, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/transferreports/TransferReportsWorkflow$Action$Finish;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
