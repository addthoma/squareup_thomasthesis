.class public interface abstract Lcom/squareup/transferreports/TransferReportsLoader;
.super Ljava/lang/Object;
.source "TransferReportsLoader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsLoader$State;,
        Lcom/squareup/transferreports/TransferReportsLoader$DepositType;,
        Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;,
        Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;,
        Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008f\u0018\u00002\u00020\u0001:\u0005\u000f\u0010\u0011\u0012\u0013J2\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH&J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0004H&J\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0004H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsLoader;",
        "",
        "getTransferDetails",
        "Lio/reactivex/Single;",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "currentSnapshot",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "settlementReport",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "settlementToken",
        "",
        "getTransferReports",
        "loadBillEntries",
        "loadMore",
        "DepositType",
        "PendingSettlements",
        "SettlementReport",
        "Snapshot",
        "State",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getTransferDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTransferReports()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadBillEntries(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadMore(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation
.end method
