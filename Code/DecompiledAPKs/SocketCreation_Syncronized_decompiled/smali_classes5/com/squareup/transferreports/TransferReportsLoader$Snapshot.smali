.class public final Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
.super Ljava/lang/Object;
.source "TransferReportsLoader.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Snapshot"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsLoader$Snapshot$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsLoader.kt\ncom/squareup/transferreports/TransferReportsLoader$Snapshot\n*L\n1#1,102:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u0000\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0097\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0007\u0012\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0011\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u0012\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u0006\u00100\u001a\u000201J\u0006\u00102\u001a\u00020\u0015J\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\u000b\u00104\u001a\u0004\u0018\u00010\u0015H\u00c6\u0003J\u000b\u00105\u001a\u0004\u0018\u00010\u0017H\u00c6\u0003J\t\u00106\u001a\u00020\u0019H\u00c6\u0003J\u000b\u00107\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000f\u00108\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\u000b\u00109\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u000f\u0010:\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007H\u00c6\u0003J\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010<\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0007H\u00c6\u0003J\t\u0010=\u001a\u00020\u0011H\u00c6\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003J\u009d\u0001\u0010?\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00072\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00072\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0019H\u00c6\u0001J\t\u0010@\u001a\u00020\u0011H\u00d6\u0001J\u0013\u0010A\u001a\u00020\u00192\u0008\u0010B\u001a\u0004\u0018\u00010CH\u00d6\u0003J\u0006\u0010D\u001a\u00020\u0019J\u0006\u0010E\u001a\u00020\u0019J\u0006\u0010F\u001a\u00020\u0019J\t\u0010G\u001a\u00020\u0011H\u00d6\u0001J\u0006\u0010H\u001a\u00020\u0019J\u0008\u0010I\u001a\u0004\u0018\u00010\u0017J\u000c\u0010J\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0007J\t\u0010K\u001a\u00020\u0017H\u00d6\u0001J\u0019\u0010L\u001a\u00020M2\u0006\u0010N\u001a\u00020O2\u0006\u0010P\u001a\u00020\u0011H\u00d6\u0001R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0017\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\"R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\"R\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010,R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010\u001c\u00a8\u0006Q"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "Landroid/os/Parcelable;",
        "state",
        "Lcom/squareup/transferreports/TransferReportsLoader$State;",
        "pendingSettlementsResponse",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
        "pendingSettlementsList",
        "",
        "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
        "settlementReportLiteResponse",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "settlementReportList",
        "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
        "depositDetailsState",
        "settledBillEntriesResponseList",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
        "settledBillStartIndex",
        "",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "settlementReport",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "settlementToken",
        "",
        "hasMoreBillEntries",
        "",
        "(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;Z)V",
        "getDepositDetailsState",
        "()Lcom/squareup/transferreports/TransferReportsLoader$State;",
        "getDepositType",
        "()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "getHasMoreBillEntries",
        "()Z",
        "getPendingSettlementsList",
        "()Ljava/util/List;",
        "getPendingSettlementsResponse",
        "()Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
        "getSettledBillEntriesResponseList",
        "getSettledBillStartIndex",
        "()I",
        "getSettlementReport",
        "()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "getSettlementReportList",
        "getSettlementReportLiteResponse",
        "()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "getSettlementToken",
        "()Ljava/lang/String;",
        "getState",
        "activeSalesMoney",
        "Lcom/squareup/protos/common/Money;",
        "activeSalesReport",
        "component1",
        "component10",
        "component11",
        "component12",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "describeContents",
        "equals",
        "other",
        "",
        "hasActiveSalesReport",
        "hasNextBatchToken",
        "hasPendingSettlements",
        "hashCode",
        "isHistoryEmpty",
        "nextBatchToken",
        "pendingSettlementReport",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

.field private final depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

.field private final hasMoreBillEntries:Z

.field private final pendingSettlementsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

.field private final settledBillEntriesResponseList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final settledBillStartIndex:I

.field private final settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

.field private final settlementReportList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;"
        }
    .end annotation
.end field

.field private final settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

.field private final settlementToken:Ljava/lang/String;

.field private final state:Lcom/squareup/transferreports/TransferReportsLoader$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot$Creator;

    invoke-direct {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot$Creator;-><init>()V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$State;",
            "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
            ">;",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;",
            "Lcom/squareup/transferreports/TransferReportsLoader$State;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;I",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pendingSettlementsList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settlementReportList"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositDetailsState"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settledBillEntriesResponseList"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    iput-object p5, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    iput-object p7, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    iput p8, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    iput-object p9, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    iput-object p10, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iput-object p11, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    iput-boolean p12, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 13

    move/from16 v0, p13

    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 57
    move-object v1, v2

    check-cast v1, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    goto :goto_0

    :cond_0
    move-object v1, p2

    :goto_0
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_1

    .line 58
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object/from16 v3, p3

    :goto_1
    and-int/lit8 v4, v0, 0x8

    if-eqz v4, :cond_2

    .line 59
    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p4

    :goto_2
    and-int/lit8 v5, v0, 0x10

    if-eqz v5, :cond_3

    .line 60
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v5

    goto :goto_3

    :cond_3
    move-object/from16 v5, p5

    :goto_3
    and-int/lit8 v6, v0, 0x20

    if-eqz v6, :cond_4

    .line 61
    sget-object v6, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p6

    :goto_4
    and-int/lit8 v7, v0, 0x40

    if-eqz v7, :cond_5

    .line 62
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v7

    goto :goto_5

    :cond_5
    move-object/from16 v7, p7

    :goto_5
    and-int/lit16 v8, v0, 0x80

    const/4 v9, 0x0

    if-eqz v8, :cond_6

    const/4 v8, 0x0

    goto :goto_6

    :cond_6
    move/from16 v8, p8

    :goto_6
    and-int/lit16 v10, v0, 0x100

    if-eqz v10, :cond_7

    .line 64
    move-object v10, v2

    check-cast v10, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    goto :goto_7

    :cond_7
    move-object/from16 v10, p9

    :goto_7
    and-int/lit16 v11, v0, 0x200

    if-eqz v11, :cond_8

    .line 65
    move-object v11, v2

    check-cast v11, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    goto :goto_8

    :cond_8
    move-object/from16 v11, p10

    :goto_8
    and-int/lit16 v12, v0, 0x400

    if-eqz v12, :cond_9

    .line 66
    check-cast v2, Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v2, p11

    :goto_9
    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_a

    goto :goto_a

    :cond_a
    move/from16 v9, p12

    :goto_a
    move-object p2, p0

    move-object/from16 p3, p1

    move-object/from16 p4, v1

    move-object/from16 p5, v3

    move-object/from16 p6, v4

    move-object/from16 p7, v5

    move-object/from16 p8, v6

    move-object/from16 p9, v7

    move/from16 p10, v8

    move-object/from16 p11, v10

    move-object/from16 p12, v11

    move-object/from16 p13, v2

    move/from16 p14, v9

    .line 67
    invoke-direct/range {p2 .. p14}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 13

    move-object v0, p0

    move/from16 v1, p13

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_b

    iget-boolean v1, v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    goto :goto_b

    :cond_b
    move/from16 v1, p12

    :goto_b
    move-object p1, v2

    move-object p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move/from16 p12, v1

    invoke-virtual/range {p0 .. p12}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->copy(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;Z)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final activeSalesMoney()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;->active_sales_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iget-object v0, v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object v0, v0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    const-string v1, "pendingSettlementsRespon\u2026ttlement.settlement_money"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final activeSalesReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;->active_sales_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    const-string v1, "pendingSettlementsResponse!!.active_sales_report"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final component1()Lcom/squareup/transferreports/TransferReportsLoader$State;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    return-object v0
.end method

.method public final component10()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    return-object v0
.end method

.method public final component11()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component12()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    return v0
.end method

.method public final component2()Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Lcom/squareup/transferreports/TransferReportsLoader$State;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    return-object v0
.end method

.method public final component7()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    return-object v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    return v0
.end method

.method public final component9()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    return-object v0
.end method

.method public final copy(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;Z)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$State;",
            "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
            ">;",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;",
            "Lcom/squareup/transferreports/TransferReportsLoader$State;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;I",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;"
        }
    .end annotation

    const-string v0, "state"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pendingSettlementsList"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settlementReportList"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositDetailsState"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settledBillEntriesResponseList"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-object v1, v0

    move-object/from16 v3, p2

    move-object/from16 v5, p4

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move/from16 v13, p12

    invoke-direct/range {v1 .. v13}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    iget v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    iget-boolean p1, p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDepositDetailsState()Lcom/squareup/transferreports/TransferReportsLoader$State;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    return-object v0
.end method

.method public final getDepositType()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    return-object v0
.end method

.method public final getHasMoreBillEntries()Z
    .locals 1

    .line 67
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    return v0
.end method

.method public final getPendingSettlementsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    return-object v0
.end method

.method public final getPendingSettlementsResponse()Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    return-object v0
.end method

.method public final getSettledBillEntriesResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    return-object v0
.end method

.method public final getSettledBillStartIndex()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    return v0
.end method

.method public final getSettlementReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    return-object v0
.end method

.method public final getSettlementReportList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    return-object v0
.end method

.method public final getSettlementReportLiteResponse()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    return-object v0
.end method

.method public final getSettlementToken()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getState()Lcom/squareup/transferreports/TransferReportsLoader$State;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    return-object v0
.end method

.method public final hasActiveSalesReport()Z
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;->active_sales_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final hasNextBatchToken()Z
    .locals 2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->nextBatchToken()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    xor-int/2addr v0, v1

    return v0
.end method

.method public final hasPendingSettlements()Z
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :cond_a
    add-int/2addr v0, v1

    return v0
.end method

.method public final isHistoryEmpty()Z
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final nextBatchToken()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final pendingSettlementReport()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;->pending_settlement_report:Ljava/util/List;

    const-string v1, "pendingSettlementsRespon\u2026pending_settlement_report"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Snapshot(state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pendingSettlementsResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pendingSettlementsList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", settlementReportLiteResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", settlementReportList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", depositDetailsState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", settledBillEntriesResponseList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", settledBillStartIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", depositType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", settlementReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", settlementToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", hasMoreBillEntries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->state:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsResponse:Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementsList:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportLiteResponse:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReportList:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositDetailsState:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillEntriesResponseList:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    goto :goto_2

    :cond_2
    iget p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settledBillStartIndex:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    if-eqz p2, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    :goto_3
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->settlementToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasMoreBillEntries:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
