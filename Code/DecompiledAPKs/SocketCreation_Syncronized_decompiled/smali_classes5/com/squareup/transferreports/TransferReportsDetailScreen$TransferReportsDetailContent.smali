.class public final Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;
.super Ljava/lang/Object;
.source "TransferReportsDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransferReportsDetailContent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u000b0\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0008H\u00c6\u0003J\u000f\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u000f\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u000f\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u0015\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u000b0\u000fH\u00c6\u0003Jw\u0010&\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000e\u0008\u0002\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u000b0\u000fH\u00c6\u0001J\u0013\u0010\'\u001a\u00020\u00032\u0008\u0010(\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010)\u001a\u00020*H\u00d6\u0001J\t\u0010+\u001a\u00020,H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u000b0\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016R\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0014\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;",
        "",
        "showCard",
        "",
        "reportsSnapshot",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "feeDetailsAvailable",
        "checkedButtonType",
        "Lcom/squareup/transferreports/ButtonType;",
        "onBack",
        "Lkotlin/Function0;",
        "",
        "onLoadMore",
        "onViewFees",
        "onCheck",
        "Lkotlin/Function1;",
        "(ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V",
        "getCheckedButtonType",
        "()Lcom/squareup/transferreports/ButtonType;",
        "getFeeDetailsAvailable",
        "()Z",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnCheck",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnLoadMore",
        "getOnViewFees",
        "getReportsSnapshot",
        "()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "getShowCard",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final checkedButtonType:Lcom/squareup/transferreports/ButtonType;

.field private final feeDetailsAvailable:Z

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCheck:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onLoadMore:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onViewFees:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

.field private final showCard:Z


# direct methods
.method public constructor <init>(ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Z",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reportsSnapshot"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkedButtonType"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLoadMore"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onViewFees"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCheck"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iput-boolean p3, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    iput-object p5, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    iput-object p6, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    iput-object p7, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    iput-object p8, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move p1, v2

    move-object p2, v3

    move p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->copy(ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    return v0
.end method

.method public final component2()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    return v0
.end method

.method public final component4()Lcom/squareup/transferreports/ButtonType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Z",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;"
        }
    .end annotation

    const-string v0, "reportsSnapshot"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkedButtonType"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLoadMore"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onViewFees"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCheck"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    move-object v1, v0

    move v2, p1

    move v4, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;-><init>(ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    iget-boolean v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    iget-boolean v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCheckedButtonType()Lcom/squareup/transferreports/ButtonType;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    return-object v0
.end method

.method public final getFeeDetailsAvailable()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    return v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnCheck()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/transferreports/ButtonType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnLoadMore()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnViewFees()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public final getShowCard()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_7
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransferReportsDetailContent(showCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->showCard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", reportsSnapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", feeDetailsAvailable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->feeDetailsAvailable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", checkedButtonType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onLoadMore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onViewFees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onViewFees:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCheck="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->onCheck:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
