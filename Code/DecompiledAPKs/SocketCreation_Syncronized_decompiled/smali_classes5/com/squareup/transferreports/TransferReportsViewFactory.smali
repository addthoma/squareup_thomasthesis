.class public final Lcom/squareup/transferreports/TransferReportsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "TransferReportsViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\tB#\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "transferReportsLayoutRunnerFactory",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
        "transferReportsDetailLayoutRunnerFactory",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;",
        "(Ljava/lang/Class;Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V",
        "Factory",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "transferReportsLayoutRunnerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferReportsDetailLayoutRunnerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    new-instance v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Binding;

    invoke-direct {v1, p1, p2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Binding;-><init>(Ljava/lang/Class;Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x0

    aput-object v1, v0, p2

    .line 12
    new-instance p2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Binding;

    invoke-direct {p2, p1, p3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Binding;-><init>(Ljava/lang/Class;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V

    check-cast p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 13
    sget-object p1, Lcom/squareup/transferreports/TransferReportsFeeDetailDialogFactory;->Companion:Lcom/squareup/transferreports/TransferReportsFeeDetailDialogFactory$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x2

    aput-object p1, v0, p2

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
