.class final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AccessibleKeypadCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lio/reactivex/Scheduler;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/accessibility/AccessibilitySettings;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 4

    .line 268
    new-instance v0, Lcom/squareup/securetouch/SecureTouchPoint;

    .line 269
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$getRootLayout$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {v2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$getRootLayout$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 270
    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {v2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$getRootLayout$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {v3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$getRootLayout$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroidx/constraintlayout/widget/ConstraintLayout;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 268
    invoke-direct {v0, v1, v2}, Lcom/squareup/securetouch/SecureTouchPoint;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$centerOfScreen$2;->invoke()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object v0

    return-object v0
.end method
