.class final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;
.super Ljava/lang/Object;
.source "SecureTouchAccessibilityPinEntryAudioPlayer.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->playAudio(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchAccessibilityPinEntryAudioPlayer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchAccessibilityPinEntryAudioPlayer.kt\ncom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $resId:I

.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->this$0:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    iput p2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->$resId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 52
    iget v1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->$resId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "SecureTouchAccessibilityPinEntryAudioPlayer: playing audio %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->this$0:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    invoke-static {v0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->access$getPlayer$p(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;)Landroid/media/MediaPlayer;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 57
    :cond_0
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 59
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->this$0:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    invoke-static {v1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->access$getApplication$p(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;)Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->$resId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    const-string v2, "assetFileDescriptor"

    .line 61
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    .line 62
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v3

    .line 63
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v5

    move-object v1, v0

    .line 60
    invoke-virtual/range {v1 .. v6}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 65
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 66
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 69
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to play audio: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer$playAudio$1;->$resId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
