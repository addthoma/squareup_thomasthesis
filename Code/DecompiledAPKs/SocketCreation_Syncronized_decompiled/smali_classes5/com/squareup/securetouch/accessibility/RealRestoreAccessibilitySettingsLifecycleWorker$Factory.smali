.class public final Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;
.super Ljava/lang/Object;
.source "RealRestoreAccessibilitySettingsLifecycleWorker.kt"

# interfaces
.implements Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;",
        "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "(Lcom/squareup/accessibility/AccessibilitySettings;)V",
        "create",
        "Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;",
        "setTalkBackEnabledOnStopped",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;


# direct methods
.method public constructor <init>(Lcom/squareup/accessibility/AccessibilitySettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accessibilitySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    return-void
.end method


# virtual methods
.method public create(Z)Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;
    .locals 2

    .line 16
    new-instance v0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;

    .line 17
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    .line 16
    invoke-direct {v0, v1, p1}, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;-><init>(Lcom/squareup/accessibility/AccessibilitySettings;Z)V

    return-object v0
.end method

.method public bridge synthetic create(Z)Lcom/squareup/workflow/LifecycleWorker;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;->create(Z)Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/LifecycleWorker;

    return-object p1
.end method
