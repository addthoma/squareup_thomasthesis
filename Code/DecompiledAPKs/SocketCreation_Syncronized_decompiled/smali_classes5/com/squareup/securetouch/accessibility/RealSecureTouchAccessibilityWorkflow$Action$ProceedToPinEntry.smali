.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;
.super Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action;
.source "RealSecureTouchAccessibilityWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProceedToPinEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action;",
        "talkBackEnabled",
        "",
        "(Z)V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final talkBackEnabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 122
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;->talkBackEnabled:Z

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$DisplayAccessibilityPinEntry;

    .line 125
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    invoke-virtual {v1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    .line 126
    iget-boolean v2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;->talkBackEnabled:Z

    .line 124
    invoke-direct {v0, v1, v2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$DisplayAccessibilityPinEntry;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/squareup/securetouch/EnableSecureTouchAccessibilityMode;->INSTANCE:Lcom/squareup/securetouch/EnableSecureTouchAccessibilityMode;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
