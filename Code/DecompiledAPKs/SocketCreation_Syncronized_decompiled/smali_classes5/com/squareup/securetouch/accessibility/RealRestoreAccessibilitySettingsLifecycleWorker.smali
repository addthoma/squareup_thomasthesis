.class public final Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "RealRestoreAccessibilitySettingsLifecycleWorker.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "setTalkBackEnabledOnStopped",
        "",
        "(Lcom/squareup/accessibility/AccessibilitySettings;Z)V",
        "onStopped",
        "",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private final setTalkBackEnabledOnStopped:Z


# direct methods
.method public constructor <init>(Lcom/squareup/accessibility/AccessibilitySettings;Z)V
    .locals 1

    const-string v0, "accessibilitySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iput-boolean p2, p0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;->setTalkBackEnabledOnStopped:Z

    return-void
.end method


# virtual methods
.method public onStopped()V
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iget-boolean v1, p0, Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker;->setTalkBackEnabledOnStopped:Z

    invoke-virtual {v0, v1}, Lcom/squareup/accessibility/AccessibilitySettings;->setTalkBackEnabled(Z)V

    return-void
.end method
