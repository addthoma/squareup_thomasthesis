.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealSecureTouchAccessibilityWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accessibility/AccessibilitySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accessibility/AccessibilitySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accessibility/AccessibilitySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;)",
            "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;
    .locals 8

    .line 59
    new-instance v7, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;-><init>(Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/accessibility/AccessibilitySettings;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-static/range {v1 .. v6}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->newInstance(Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow_Factory;->get()Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    move-result-object v0

    return-object v0
.end method
