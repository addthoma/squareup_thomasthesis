.class public final Lcom/squareup/securetouch/UsingKeypad;
.super Lcom/squareup/securetouch/SecureTouchState;
.source "SecureTouchState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/UsingKeypad$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchState.kt\ncom/squareup/securetouch/UsingKeypad\n*L\n1#1,100:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB#\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0006\u0010\u0013\u001a\u00020\u0000J\u0013\u0010\u0014\u001a\u00020\u00072\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\u0006\u0010\u0018\u001a\u00020\u0000J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/securetouch/UsingKeypad;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "dotsShowing",
        "",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "useHighContrastMode",
        "",
        "(ILcom/squareup/securetouch/SecureTouchPinEntryState;Z)V",
        "getDotsShowing",
        "()I",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "getUseHighContrastMode",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "decrement",
        "equals",
        "other",
        "",
        "hashCode",
        "increment",
        "toString",
        "",
        "Companion",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/UsingKeypad$Companion;


# instance fields
.field private final dotsShowing:I

.field private final pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

.field private final useHighContrastMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/securetouch/UsingKeypad$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/UsingKeypad$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/UsingKeypad;->Companion:Lcom/squareup/securetouch/UsingKeypad$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/securetouch/UsingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;Z)V
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    iput-object p2, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    iput-boolean p3, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 26
    sget-object p2, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    check-cast p2, Lcom/squareup/securetouch/SecureTouchPinEntryState;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    .line 27
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/securetouch/UsingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/UsingKeypad;ILcom/squareup/securetouch/SecureTouchPinEntryState;ZILjava/lang/Object;)Lcom/squareup/securetouch/UsingKeypad;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/UsingKeypad;->copy(ILcom/squareup/securetouch/SecureTouchPinEntryState;Z)Lcom/squareup/securetouch/UsingKeypad;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    return v0
.end method

.method public final component2()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    return v0
.end method

.method public final copy(ILcom/squareup/securetouch/SecureTouchPinEntryState;Z)Lcom/squareup/securetouch/UsingKeypad;
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/UsingKeypad;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/UsingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    return-object v0
.end method

.method public final decrement()Lcom/squareup/securetouch/UsingKeypad;
    .locals 8

    .line 34
    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 35
    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    check-cast v0, Lcom/squareup/securetouch/SecureTouchPinEntryState;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    :goto_0
    move-object v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    .line 33
    invoke-static/range {v2 .. v7}, Lcom/squareup/securetouch/UsingKeypad;->copy$default(Lcom/squareup/securetouch/UsingKeypad;ILcom/squareup/securetouch/SecureTouchPinEntryState;ZILjava/lang/Object;)Lcom/squareup/securetouch/UsingKeypad;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/UsingKeypad;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/UsingKeypad;

    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    iget v1, p1, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    iget-object v1, p1, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    iget-boolean p1, p1, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDotsShowing()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    return v0
.end method

.method public final getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public final getUseHighContrastMode()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final increment()Lcom/squareup/securetouch/UsingKeypad;
    .locals 7

    .line 39
    iget v0, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    add-int/lit8 v2, v0, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/squareup/securetouch/UsingKeypad;->copy$default(Lcom/squareup/securetouch/UsingKeypad;ILcom/squareup/securetouch/SecureTouchPinEntryState;ZILjava/lang/Object;)Lcom/squareup/securetouch/UsingKeypad;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UsingKeypad(dotsShowing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/UsingKeypad;->dotsShowing:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pinEntryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/UsingKeypad;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", useHighContrastMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/UsingKeypad;->useHighContrastMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
