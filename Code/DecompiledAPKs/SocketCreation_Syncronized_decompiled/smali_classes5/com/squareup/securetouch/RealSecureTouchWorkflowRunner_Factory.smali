.class public final Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealSecureTouchWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/RealSecureTouchWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/RealSecureTouchWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p7, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p8, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/RealSecureTouchWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;",
            ">;)",
            "Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;"
        }
    .end annotation

    .line 57
    new-instance v9, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/payment/Transaction;Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchViewFactory;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;
    .locals 10

    .line 64
    new-instance v9, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/payment/Transaction;Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchViewFactory;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;
    .locals 9

    .line 49
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/statusbar/event/StatusBarEventManager;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/securetouch/SecureTouchViewFactory;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;

    invoke-static/range {v1 .. v8}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/payment/Transaction;Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchViewFactory;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner_Factory;->get()Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
