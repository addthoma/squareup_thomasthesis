.class public final Lcom/squareup/securetouch/SecureTouchCoordinatorKt;
.super Ljava/lang/Object;
.source "SecureTouchCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchCoordinator.kt\ncom/squareup/securetouch/SecureTouchCoordinatorKt\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,275:1\n453#2,7:276\n*E\n*S KotlinDebug\n*F\n+ 1 SecureTouchCoordinator.kt\ncom/squareup/securetouch/SecureTouchCoordinatorKt\n*L\n266#1,7:276\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a4\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00052\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0007H\u0002\u001a\u0014\u0010\u000b\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "MAX_DISPLAYABLE_STARS",
        "",
        "allButtonsOnThisScreen",
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        "keymap",
        "",
        "Lcom/squareup/securetouch/SecureKey;",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "cancelButtonRect",
        "accessibilityButtonRect",
        "doneButtonRect",
        "reportTouchEventsDevOnly",
        "",
        "Landroid/view/View;",
        "screen",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "secure-touch_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MAX_DISPLAYABLE_STARS:I = 0xc


# direct methods
.method public static final synthetic access$allButtonsOnThisScreen(Ljava/util/Map;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->allButtonsOnThisScreen(Ljava/util/Map;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V

    return-void
.end method

.method private static final allButtonsOnThisScreen(Ljava/util/Map;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ")",
            "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;"
        }
    .end annotation

    .line 276
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 277
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 278
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/securetouch/SecureKey;

    .line 266
    sget-object v3, Lcom/squareup/securetouch/SecureKey;->EmptySpace:Lcom/squareup/securetouch/SecureKey;

    if-eq v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    .line 279
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 282
    :cond_2
    check-cast v0, Ljava/util/Map;

    .line 267
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    .line 269
    sget-object v0, Lcom/squareup/securetouch/SecureKey;->Cancel:Lcom/squareup/securetouch/SecureKey;

    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Accessibility:Lcom/squareup/securetouch/SecureKey;

    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Done:Lcom/squareup/securetouch/SecureKey;

    invoke-interface {p0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    new-instance p1, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;-><init>(Ljava/util/Map;)V

    return-object p1
.end method

.method private static final reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V
    .locals 4

    .line 228
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "status_bar_height"

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 230
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 234
    :goto_0
    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinatorKt$reportTouchEventsDevOnly$1;

    invoke-direct {v1, p1, v0}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt$reportTouchEventsDevOnly$1;-><init>(Lcom/squareup/securetouch/SecureTouchScreen;I)V

    check-cast v1, Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
