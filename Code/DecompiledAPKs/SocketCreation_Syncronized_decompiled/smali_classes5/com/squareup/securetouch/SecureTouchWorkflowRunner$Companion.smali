.class public final Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "SecureTouchWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "getNAME",
        "()Ljava/lang/String;",
        "get",
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner;",
        "scope",
        "Lmortar/MortarScope;",
        "secure-touch-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    new-instance v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

    invoke-direct {v0}, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

    .line 19
    const-class v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SecureTouchWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Lmortar/MortarScope;)Lcom/squareup/securetouch/SecureTouchWorkflowRunner;
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->NAME:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    sget-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->NAME:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;

    goto :goto_0

    .line 25
    :cond_0
    sget-object p1, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;->INSTANCE:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;

    check-cast p1, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;

    :goto_0
    return-object p1
.end method

.method public final getNAME()Ljava/lang/String;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->NAME:Ljava/lang/String;

    return-object v0
.end method
