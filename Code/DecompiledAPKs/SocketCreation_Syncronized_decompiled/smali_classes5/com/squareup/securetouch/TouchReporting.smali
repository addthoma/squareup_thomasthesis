.class public interface abstract Lcom/squareup/securetouch/TouchReporting;
.super Ljava/lang/Object;
.source "TouchReporting.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/securetouch/TouchReporting;",
        "",
        "disableTouchReporting",
        "Lio/reactivex/Single;",
        "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingDisabled;",
        "enableTouchReporting",
        "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingEnabled;",
        "secure-touch-reporting_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract disableTouchReporting()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingDisabled;",
            ">;"
        }
    .end annotation
.end method

.method public abstract enableTouchReporting()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingEnabled;",
            ">;"
        }
    .end annotation
.end method
