.class public final enum Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;
.super Ljava/lang/Enum;
.source "SecureTouchAnalyticsLogger.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;",
        "",
        "eventName",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "toString",
        "ACTION",
        "READER",
        "SQUID",
        "APP",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

.field public static final enum ACTION:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

.field public static final enum APP:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

.field public static final enum READER:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

.field public static final enum SQUID:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    new-instance v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    const/4 v2, 0x0

    const-string v3, "ACTION"

    const-string v4, "Action"

    .line 12
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->ACTION:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    const/4 v2, 0x1

    const-string v3, "READER"

    const-string v4, "Reader"

    .line 13
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->READER:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    const/4 v2, 0x2

    const-string v3, "SQUID"

    const-string v4, "Squid"

    .line 14
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->SQUID:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    const/4 v2, 0x3

    const-string v3, "APP"

    const-string v4, "App"

    .line 15
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->APP:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->$VALUES:[Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->eventName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;
    .locals 1

    const-class v0, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;
    .locals 1

    sget-object v0, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->$VALUES:[Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    invoke-virtual {v0}, [Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->eventName:Ljava/lang/String;

    return-object v0
.end method
