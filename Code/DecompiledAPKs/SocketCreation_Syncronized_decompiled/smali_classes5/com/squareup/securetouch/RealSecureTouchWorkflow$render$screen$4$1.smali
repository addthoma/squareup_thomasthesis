.class final Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;->invoke(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "-",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $headphoneConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

.field final synthetic this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;

    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4$1;->$headphoneConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;

    iget-object v0, v0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;->$state:Lcom/squareup/securetouch/SecureTouchState;

    move-object v1, v0

    check-cast v1, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4$1;->$headphoneConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v2, v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->copy$default(Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;ZLcom/squareup/securetouch/SecureTouchPinEntryState;ZILjava/lang/Object;)Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
