.class public final Lcom/squareup/sqwidgets/time/RealTimeFormatChooser;
.super Ljava/lang/Object;
.source "RealTimeFormatChooser.kt"

# interfaces
.implements Lcom/squareup/sqwidgets/time/TimeFormatChooser;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/RealTimeFormatChooser;",
        "Lcom/squareup/sqwidgets/time/TimeFormatChooser;",
        "application",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "is24HourFormat",
        "",
        "()Z",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/RealTimeFormatChooser;->application:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public is24HourFormat()Z
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/RealTimeFormatChooser;->application:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
