.class public final Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;
.super Lcom/squareup/sqwidgets/time/PickerStrategy;
.source "PickerStrategy.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPickerStrategy.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PickerStrategy.kt\ncom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019Bp\u0012U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0013H\u0016R\u0014\u0010\u0012\u001a\u00020\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;",
        "Lcom/squareup/sqwidgets/time/PickerStrategy;",
        "pickerFactory",
        "Lkotlin/Function3;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "id",
        "",
        "",
        "values",
        "",
        "isInfiniteScroll",
        "Lcom/squareup/sqwidgets/time/Picker;",
        "Lcom/squareup/sqwidgets/time/PickerFactory;",
        "hourPickerId",
        "minutePickerId",
        "(Lkotlin/jvm/functions/Function3;II)V",
        "time",
        "Ljava/util/Calendar;",
        "getTime",
        "()Ljava/util/Calendar;",
        "setDisplayedTime",
        "",
        "currentTime",
        "Companion",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy$Companion;

.field private static final HOUR_PICKER_END:I = 0x17

.field private static final HOUR_PICKER_START:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;->Companion:Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy$Companion;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function3;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;II)V"
        }
    .end annotation

    const-string v0, "pickerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/16 v4, 0x17

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    move v6, p3

    .line 83
    invoke-direct/range {v1 .. v6}, Lcom/squareup/sqwidgets/time/PickerStrategy;-><init>(Lkotlin/jvm/functions/Function3;IIII)V

    return-void
.end method


# virtual methods
.method public getTime()Ljava/util/Calendar;
    .locals 3

    .line 92
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;->getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getHour()I

    move-result v1

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 95
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;->getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getMinute()I

    move-result v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const-string v1, "Calendar.getInstance()\n \u2026ckers.minute)\n          }"

    .line 93
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public setDisplayedTime(Ljava/util/Calendar;)V
    .locals 3

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;->getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-result-object v0

    const/16 v1, 0xb

    .line 101
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0xc

    .line 102
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result p1

    .line 100
    invoke-virtual {v0, v1, p1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->selectValues(II)V

    return-void
.end method
