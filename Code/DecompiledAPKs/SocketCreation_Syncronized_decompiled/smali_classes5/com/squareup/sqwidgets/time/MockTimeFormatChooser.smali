.class public final Lcom/squareup/sqwidgets/time/MockTimeFormatChooser;
.super Ljava/lang/Object;
.source "MockTimeFormatChooser.kt"

# interfaces
.implements Lcom/squareup/sqwidgets/time/TimeFormatChooser;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0003\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/MockTimeFormatChooser;",
        "Lcom/squareup/sqwidgets/time/TimeFormatChooser;",
        "()V",
        "is24HourFormat",
        "",
        "()Z",
        "set24HourFormat",
        "(Z)V",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private is24HourFormat:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public is24HourFormat()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/sqwidgets/time/MockTimeFormatChooser;->is24HourFormat:Z

    return v0
.end method

.method public set24HourFormat(Z)V
    .locals 0

    .line 10
    iput-boolean p1, p0, Lcom/squareup/sqwidgets/time/MockTimeFormatChooser;->is24HourFormat:Z

    return-void
.end method
