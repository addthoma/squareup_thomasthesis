.class public final Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;
.super Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
.source "ClickSequenceTrigger.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClickSequenceInProgress"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;",
        "leftClickCount",
        "",
        "rightClickCount",
        "previousClickTimestamp",
        "",
        "(IIJ)V",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(IIJ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-wide v3, p3

    .line 144
    invoke-direct/range {v0 .. v5}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;-><init>(IIJLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
