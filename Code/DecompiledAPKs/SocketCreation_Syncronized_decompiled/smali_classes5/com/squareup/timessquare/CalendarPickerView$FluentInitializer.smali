.class public Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
.super Ljava/lang/Object;
.source "CalendarPickerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/timessquare/CalendarPickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FluentInitializer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/timessquare/CalendarPickerView;


# direct methods
.method public constructor <init>(Lcom/squareup/timessquare/CalendarPickerView;)V
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public displayOnly()Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 2

    .line 398
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$702(Lcom/squareup/timessquare/CalendarPickerView;Z)Z

    return-object p0
.end method

.method public inMode(Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iput-object p1, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    .line 344
    invoke-static {v0}, Lcom/squareup/timessquare/CalendarPickerView;->access$300(Lcom/squareup/timessquare/CalendarPickerView;)V

    return-object p0
.end method

.method public setShortWeekdays([Ljava/lang/String;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 4

    .line 390
    new-instance v0, Ljava/text/DateFormatSymbols;

    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$500(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 391
    invoke-virtual {v0, p1}, Ljava/text/DateFormatSymbols;->setShortWeekdays([Ljava/lang/String;)V

    .line 392
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 393
    invoke-virtual {p1}, Lcom/squareup/timessquare/CalendarPickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/timessquare/R$string;->day_name_format:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/text/DateFormatSymbols;)V

    .line 392
    invoke-static {p1, v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$602(Lcom/squareup/timessquare/CalendarPickerView;Ljava/text/DateFormat;)Ljava/text/DateFormat;

    return-object p0
.end method

.method public withHighlightedDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 0

    .line 385
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->withHighlightedDates(Ljava/util/Collection;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    return-object p1
.end method

.method public withHighlightedDates(Ljava/util/Collection;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/util/Date;",
            ">;)",
            "Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;"
        }
    .end annotation

    .line 380
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->highlightDates(Ljava/util/Collection;)V

    return-object p0
.end method

.method public withMonthsReverseOrder(Z)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->access$802(Lcom/squareup/timessquare/CalendarPickerView;Z)Z

    return-object p0
.end method

.method public withSelectedDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 0

    .line 353
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->withSelectedDates(Ljava/util/Collection;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    return-object p1
.end method

.method public withSelectedDates(Ljava/util/Collection;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/util/Date;",
            ">;)",
            "Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;"
        }
    .end annotation

    .line 361
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v0, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    sget-object v1, Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;->SINGLE:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 362
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "SINGLE mode can\'t be used with multiple selectedDates"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 364
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v0, v0, Lcom/squareup/timessquare/CalendarPickerView;->selectionMode:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    sget-object v1, Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;->RANGE:Lcom/squareup/timessquare/CalendarPickerView$SelectionMode;

    if-ne v0, v1, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_2

    goto :goto_1

    .line 365
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RANGE mode only allows two selectedDates.  You tried to pass "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_1
    if-eqz p1, :cond_4

    .line 369
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 370
    iget-object v1, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-virtual {v1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->selectDate(Ljava/util/Date;)Z

    goto :goto_2

    .line 373
    :cond_4
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {p1}, Lcom/squareup/timessquare/CalendarPickerView;->access$400(Lcom/squareup/timessquare/CalendarPickerView;)V

    .line 375
    iget-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {p1}, Lcom/squareup/timessquare/CalendarPickerView;->access$300(Lcom/squareup/timessquare/CalendarPickerView;)V

    return-object p0
.end method
