.class public Lcom/squareup/text/PercentageFormatter;
.super Ljava/lang/Object;
.source "PercentageFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/util/Percentage;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_MAXIMUM_FRACTION_DIGITS:I = 0x2

.field private static final DEFAULT_MINIMUM_FRACTION_DIGITS:I


# instance fields
.field private locale:Ljava/util/Locale;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private maximumFractionDigits:Ljava/lang/Integer;

.field private minimumFractionDigits:Ljava/lang/Integer;

.field private numberFormat:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljava/lang/Integer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/text/PercentageFormatter;->localeProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/text/PercentageFormatter;->minimumFractionDigits:Ljava/lang/Integer;

    .line 39
    iput-object p3, p0, Lcom/squareup/text/PercentageFormatter;->maximumFractionDigits:Ljava/lang/Integer;

    return-void
.end method

.method private doFormat(Lcom/squareup/util/Percentage;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/text/PercentageFormatter;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iput-object p2, p0, Lcom/squareup/text/PercentageFormatter;->locale:Ljava/util/Locale;

    .line 50
    iget-object v0, p0, Lcom/squareup/text/PercentageFormatter;->minimumFractionDigits:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/squareup/text/PercentageFormatter;->maximumFractionDigits:Ljava/lang/Integer;

    invoke-static {p2, v0, v1}, Lcom/squareup/text/PercentageFormatter;->setUpNumberFormat(Ljava/util/Locale;Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/text/NumberFormat;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/text/PercentageFormatter;->numberFormat:Ljava/text/NumberFormat;

    .line 53
    :cond_0
    iget-object p2, p0, Lcom/squareup/text/PercentageFormatter;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->doubleRate()D

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static setUpNumberFormat(Ljava/util/Locale;Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/text/NumberFormat;
    .locals 2

    .line 58
    invoke-static {p0}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p0

    .line 59
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, p2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 60
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 63
    instance-of p1, p0, Ljava/text/DecimalFormat;

    if-eqz p1, :cond_0

    .line 64
    move-object p1, p0

    check-cast p1, Ljava/text/DecimalFormat;

    .line 65
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->getPositivePrefix()Ljava/lang/String;

    move-result-object p2

    const/16 v0, 0xa0

    const/16 v1, 0x20

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setPositivePrefix(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->getPositiveSuffix()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setPositiveSuffix(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->getNegativePrefix()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setNegativePrefix(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->getNegativeSuffix()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setNegativeSuffix(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/util/Percentage;

    invoke-virtual {p0, p1}, Lcom/squareup/text/PercentageFormatter;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/PercentageFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {p0, p1, v0}, Lcom/squareup/text/PercentageFormatter;->doFormat(Lcom/squareup/util/Percentage;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
