.class public Lcom/squareup/text/SimpleSellerTipOptionFormatter;
.super Ljava/lang/Object;
.source "SimpleSellerTipOptionFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/text/SimpleSellerTipOptionFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 18
    iput-object p2, p0, Lcom/squareup/text/SimpleSellerTipOptionFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/protos/common/tipping/TipOption;)Ljava/lang/CharSequence;
    .locals 3

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 24
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 25
    iget-object v0, p0, Lcom/squareup/text/SimpleSellerTipOptionFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/squareup/text/SimpleSellerTipOptionFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/protos/common/tipping/TipOption;

    invoke-virtual {p0, p1}, Lcom/squareup/text/SimpleSellerTipOptionFormatter;->format(Lcom/squareup/protos/common/tipping/TipOption;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
