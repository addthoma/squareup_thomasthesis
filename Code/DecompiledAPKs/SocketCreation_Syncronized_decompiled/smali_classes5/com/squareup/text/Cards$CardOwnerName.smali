.class public final Lcom/squareup/text/Cards$CardOwnerName;
.super Ljava/lang/Object;
.source "Cards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/Cards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardOwnerName"
.end annotation


# instance fields
.field public final firstName:Ljava/lang/String;

.field public final lastName:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/squareup/util/Strings;->forceTitleCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    .line 37
    invoke-static {p2}, Lcom/squareup/util/Strings;->forceTitleCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    .line 38
    invoke-static {p3}, Lcom/squareup/util/Strings;->forceTitleCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_8

    .line 85
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_3

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/text/Cards$CardOwnerName;

    .line 89
    iget-object v2, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 92
    :cond_5
    iget-object v2, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_2

    :cond_6
    if-eqz p1, :cond_7

    :goto_2
    return v1

    :cond_7
    return v0

    :cond_8
    :goto_3
    return v1
.end method

.method public format(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 4

    .line 44
    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "last_name"

    const-string v2, "first_name"

    if-nez v0, :cond_2

    .line 45
    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string/jumbo v3, "title"

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    sget v0, Lcom/squareup/common/card/R$string;->card_formatted_title_first_last:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    .line 47
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    .line 48
    invoke-virtual {p1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    .line 49
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 51
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    sget v0, Lcom/squareup/common/card/R$string;->card_formatted_title_first:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    .line 54
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    .line 55
    invoke-virtual {p1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 57
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 59
    :cond_1
    sget v0, Lcom/squareup/common/card/R$string;->card_formatted_title_last:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    .line 60
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    .line 61
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 63
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 67
    sget v0, Lcom/squareup/common/card/R$string;->card_formatted_first_last:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    .line 68
    invoke-virtual {p1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    .line 69
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 71
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 72
    :cond_3
    iget-object p1, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 73
    iget-object p1, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    goto :goto_0

    .line 75
    :cond_4
    iget-object p1, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    :goto_0
    return-object p1
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/text/Cards$CardOwnerName;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 99
    iget-object v2, p0, Lcom/squareup/text/Cards$CardOwnerName;->firstName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 100
    iget-object v2, p0, Lcom/squareup/text/Cards$CardOwnerName;->lastName:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method
