.class public Lcom/squareup/text/LanguageUtils;
.super Ljava/lang/Object;
.source "LanguageUtils.java"


# static fields
.field public static final ACCENTED_LATIN_END:C = '\u00ff'

.field public static final ACCENTED_LATIN_START:C = '\u00c0'

.field public static final FULLWIDTH_ASCII_END:C = '\uff5e'

.field private static final FULLWIDTH_ASCII_OFFSET:I = 0xfee0

.field public static final FULLWIDTH_ASCII_START:C = '\uff01'

.field private static final FULLWIDTH_KATAKANA:Ljava/lang/String; = "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30cf\u30d2\u30d5\u30d8\u30db\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

.field private static final FULLWIDTH_KATAKANA_DAKUTEN:Ljava/lang/String; = "\u30fb\u30fa\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30f4\u30a8\u30aa\u30ac\u30ae\u30b0\u30b2\u30b4\u30b6\u30b8\u30ba\u30bc\u30be\u30c0\u30c2\u30c5\u30c7\u30c9\u30ca\u30cb\u30cc\u30cd\u30ce\u30d0\u30d3\u30d6\u30d9\u30dc\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30f7\u30f3"

.field private static final FULLWIDTH_KATAKANA_HANDAKUTEN:Ljava/lang/String; = "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30d1\u30d4\u30d7\u30da\u30dd\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

.field public static final HALFWIDTH_KATAKANA_DAKUTEN:I = 0xff9e

.field public static final HALFWIDTH_KATAKANA_END:I = 0xff9f

.field public static final HALFWIDTH_KATAKANA_HANDAKUTEN:I = 0xff9f

.field public static final HALFWIDTH_KATAKANA_START:I = 0xff65

.field public static final HIRAGANA_END:I = 0x309f

.field public static final HIRAGANA_START:I = 0x3041

.field public static final HIRAGANA_SYMBOLS_START:I = 0x3097

.field public static final KATAKANA_END:I = 0x30ff

.field public static final KATAKANA_START:I = 0x30a1

.field public static final KATAKANA_SYMBOLS_START:I = 0x30fb

.field private static final ROMAJI:[Ljava/lang/String;

.field private static final UNACCENTED_CHARACTERS:Ljava/lang/String; = "aaaaaaaceeeeiiiidnooooo ouuuuytsaaaaaaaceeeeiiiidnooooo ouuuuyty"


# direct methods
.method static constructor <clinit>()V
    .locals 90

    const-string v0, "a"

    const-string v1, "a"

    const-string v2, "i"

    const-string v3, "i"

    const-string/jumbo v4, "u"

    const-string/jumbo v5, "u"

    const-string v6, "e"

    const-string v7, "e"

    const-string v8, "o"

    const-string v9, "o"

    const-string v10, "ka"

    const-string v11, "ga"

    const-string v12, "ki"

    const-string v13, "gi"

    const-string v14, "ku"

    const-string v15, "gu"

    const-string v16, "ke"

    const-string v17, "ge"

    const-string v18, "ko"

    const-string v19, "go"

    const-string v20, "sa"

    const-string/jumbo v21, "za"

    const-string v22, "shi"

    const-string v23, "ji"

    const-string v24, "su"

    const-string/jumbo v25, "zu"

    const-string v26, "se"

    const-string/jumbo v27, "ze"

    const-string v28, "so"

    const-string/jumbo v29, "zo"

    const-string v30, "ta"

    const-string v31, "da"

    const-string v32, "chi"

    const-string v33, "ji"

    const-string/jumbo v34, "tsu"

    const-string/jumbo v35, "tsu"

    const-string/jumbo v36, "zu"

    const-string v37, "te"

    const-string v38, "de"

    const-string/jumbo v39, "to"

    const-string v40, "do"

    const-string v41, "na"

    const-string v42, "ni"

    const-string v43, "nu"

    const-string v44, "ne"

    const-string v45, "no"

    const-string v46, "ha"

    const-string v47, "ba"

    const-string v48, "pa"

    const-string v49, "hi"

    const-string v50, "bi"

    const-string v51, "pi"

    const-string v52, "fu"

    const-string v53, "bu"

    const-string v54, "pu"

    const-string v55, "he"

    const-string v56, "be"

    const-string v57, "pe"

    const-string v58, "ho"

    const-string v59, "bo"

    const-string v60, "po"

    const-string v61, "ma"

    const-string v62, "mi"

    const-string v63, "mu"

    const-string v64, "me"

    const-string v65, "mo"

    const-string v66, "a"

    const-string/jumbo v67, "ya"

    const-string/jumbo v68, "u"

    const-string/jumbo v69, "yu"

    const-string v70, "o"

    const-string/jumbo v71, "yo"

    const-string v72, "ra"

    const-string v73, "ri"

    const-string v74, "ru"

    const-string v75, "re"

    const-string v76, "ro"

    const-string/jumbo v77, "wa"

    const-string/jumbo v78, "wa"

    const-string/jumbo v79, "wi"

    const-string/jumbo v80, "we"

    const-string/jumbo v81, "wo"

    const-string v82, "n"

    const-string/jumbo v83, "vu"

    const-string v84, "ka"

    const-string v85, "ke"

    const-string/jumbo v86, "va"

    const-string/jumbo v87, "vi"

    const-string/jumbo v88, "ve"

    const-string/jumbo v89, "vo"

    .line 26
    filled-new-array/range {v0 .. v89}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/LanguageUtils;->ROMAJI:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static endsWithI(Ljava/lang/String;)Z
    .locals 2

    .line 291
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 v0, 0x69

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static isAccentedLatin(C)Z
    .locals 1

    const/16 v0, 0xc0

    if-lt p0, v0, :cond_0

    const/16 v0, 0xff

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isFullwidthAscii(C)Z
    .locals 1

    const v0, 0xff01

    if-lt p0, v0, :cond_0

    const v0, 0xff5e

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHalfwidthKatakana(C)Z
    .locals 1

    const v0, 0xff65

    if-lt p0, v0, :cond_0

    const v0, 0xff9f

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHalfwidthKatakanaLetter(C)Z
    .locals 1

    const v0, 0xff66

    if-lt p0, v0, :cond_0

    const v0, 0xff9d

    if-gt p0, v0, :cond_0

    const v0, 0xff70

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHiragana(C)Z
    .locals 1

    const/16 v0, 0x3041

    if-lt p0, v0, :cond_0

    const/16 v0, 0x309f

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isHiraganaLetter(C)Z
    .locals 1

    const/16 v0, 0x3041

    if-lt p0, v0, :cond_0

    const/16 v0, 0x3097

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isKatakana(C)Z
    .locals 1

    const/16 v0, 0x30a1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x30ff

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isKatakanaLetter(C)Z
    .locals 1

    const/16 v0, 0x30a1

    if-lt p0, v0, :cond_0

    const/16 v0, 0x30fb

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static isVowel(Ljava/lang/String;I)Z
    .locals 2

    .line 297
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lt p1, v0, :cond_0

    return v1

    .line 300
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    .line 301
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isHiraganaLetter(C)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isKatakanaLetter(C)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isHalfwidthKatakana(C)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    .line 305
    :cond_1
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->romanize(C)Ljava/lang/String;

    move-result-object p0

    .line 306
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_2

    return v1

    .line 309
    :cond_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 p1, 0x61

    if-eq p0, p1, :cond_3

    const/16 p1, 0x65

    if-eq p0, p1, :cond_3

    const/16 p1, 0x69

    if-eq p0, p1, :cond_3

    const/16 p1, 0x6f

    if-eq p0, p1, :cond_3

    const/16 p1, 0x75

    if-eq p0, p1, :cond_3

    const/16 p1, 0x79

    if-ne p0, p1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 214
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->toFullwidthKatakana(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 215
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_9

    .line 217
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 219
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isFullwidthAscii(C)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0xfee0

    sub-int/2addr v4, v5

    int-to-char v4, v4

    .line 224
    :cond_0
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isHiragana(C)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isKatakana(C)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    .line 249
    :cond_1
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isAccentedLatin(C)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit16 v4, v4, -0xc0

    const-string v5, "aaaaaaaceeeeiiiidnooooo ouuuuytsaaaaaaaceeeeiiiidnooooo ouuuuyty"

    .line 250
    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-eq v4, v5, :cond_8

    .line 252
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 254
    :cond_2
    invoke-static {v4}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 255
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 226
    :cond_3
    :goto_1
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isHiraganaLetter(C)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isKatakanaLetter(C)Z

    move-result v5

    if-nez v5, :cond_4

    goto :goto_3

    .line 230
    :cond_4
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->romanize(C)Ljava/lang/String;

    move-result-object v4

    .line 236
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->endsWithI(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    add-int/lit8 v5, v3, 0x1

    invoke-static {p0, v5}, Lcom/squareup/text/LanguageUtils;->isVowel(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v5, "ji"

    .line 237
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v4, "j"

    goto :goto_2

    .line 239
    :cond_5
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 241
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v4, "y"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 244
    :cond_6
    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 248
    :cond_7
    :goto_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 259
    :cond_9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static romanize(C)Ljava/lang/String;
    .locals 2

    .line 322
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isKatakana(C)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_1

    .line 323
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isKatakanaLetter(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    sget-object v0, Lcom/squareup/text/LanguageUtils;->ROMAJI:[Ljava/lang/String;

    add-int/lit16 p0, p0, -0x30a1

    aget-object p0, v0, p0

    return-object p0

    :cond_0
    return-object v1

    .line 328
    :cond_1
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isHiragana(C)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 329
    invoke-static {p0}, Lcom/squareup/text/LanguageUtils;->isHiraganaLetter(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330
    sget-object v0, Lcom/squareup/text/LanguageUtils;->ROMAJI:[Ljava/lang/String;

    add-int/lit16 p0, p0, -0x3041

    aget-object p0, v0, p0

    return-object p0

    :cond_2
    return-object v1

    .line 335
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static toFullwidthKatakana(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_6

    .line 168
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 169
    invoke-static {v4}, Lcom/squareup/text/LanguageUtils;->isHalfwidthKatakana(C)Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0xff9e

    if-eq v4, v5, :cond_5

    const v6, 0xff9f

    if-ne v4, v6, :cond_0

    goto :goto_3

    :cond_0
    add-int/lit8 v7, v3, 0x1

    if-ge v7, v1, :cond_1

    .line 180
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    const v9, 0xff65

    sub-int/2addr v4, v9

    if-ne v8, v5, :cond_2

    const-string/jumbo v3, "\u30fb\u30fa\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30f4\u30a8\u30aa\u30ac\u30ae\u30b0\u30b2\u30b4\u30b6\u30b8\u30ba\u30bc\u30be\u30c0\u30c2\u30c5\u30c7\u30c9\u30ca\u30cb\u30cc\u30cd\u30ce\u30d0\u30d3\u30d6\u30d9\u30dc\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30f7\u30f3"

    .line 183
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    move v3, v7

    goto :goto_3

    :cond_2
    if-ne v8, v6, :cond_3

    const-string/jumbo v3, "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30d1\u30d4\u30d7\u30da\u30dd\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

    .line 186
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const-string/jumbo v5, "\u30fb\u30f2\u30a1\u30a3\u30a5\u30a7\u30a9\u30e3\u30e5\u30e7\u30c3\u30fc\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30cf\u30d2\u30d5\u30d8\u30db\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f3"

    .line 189
    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 193
    :cond_4
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 197
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static wideToNarrow(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 269
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_5

    .line 270
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    .line 272
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    const/4 v5, -0x1

    if-eqz v4, :cond_0

    const/16 v3, 0x20

    goto :goto_1

    .line 274
    :cond_0
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    const v6, 0xff41

    if-lt v4, v6, :cond_1

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    const v7, 0xff5a

    if-gt v4, v7, :cond_1

    .line 275
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    sub-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x61

    goto :goto_1

    .line 276
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    const v6, 0xff21

    if-lt v4, v6, :cond_2

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    const v7, 0xff3a

    if-gt v4, v7, :cond_2

    .line 277
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    sub-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x41

    goto :goto_1

    .line 278
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    const v6, 0xff10

    if-lt v4, v6, :cond_3

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v4

    const v7, 0xff19

    if-gt v4, v7, :cond_3

    .line 279
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    sub-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x30

    goto :goto_1

    .line 281
    :cond_3
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, -0x1

    :goto_1
    if-le v3, v5, :cond_4

    .line 284
    invoke-static {v3}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 287
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
