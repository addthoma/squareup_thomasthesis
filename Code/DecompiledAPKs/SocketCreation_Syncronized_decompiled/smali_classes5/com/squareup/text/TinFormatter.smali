.class public Lcom/squareup/text/TinFormatter;
.super Ljava/lang/Object;
.source "TinFormatter.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# instance fields
.field private final hyphenLocations:[I


# direct methods
.method private varargs constructor <init>([I)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/text/TinFormatter;->hyphenLocations:[I

    return-void
.end method

.method public static createEinFormatter()Lcom/squareup/text/TinFormatter;
    .locals 4

    .line 33
    new-instance v0, Lcom/squareup/text/TinFormatter;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x2

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lcom/squareup/text/TinFormatter;-><init>([I)V

    return-object v0
.end method

.method public static createSsnFormatter()Lcom/squareup/text/TinFormatter;
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/text/TinFormatter;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lcom/squareup/text/TinFormatter;-><init>([I)V

    return-object v0

    :array_0
    .array-data 4
        0x3
        0x6
    .end array-data
.end method


# virtual methods
.method public format(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    .line 44
    invoke-static {p1}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 47
    iget-object p1, p0, Lcom/squareup/text/TinFormatter;->hyphenLocations:[I

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget v3, p1, v2

    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-le v4, v3, :cond_0

    const-string v4, "-"

    .line 49
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    iget-object v1, p0, Lcom/squareup/text/TinFormatter;->hyphenLocations:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x9

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/text/TinFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
