.class public final Lcom/squareup/text/ExpirationDateScrubber;
.super Ljava/lang/Object;
.source "ExpirationDateScrubber.kt"

# interfaces
.implements Lcom/squareup/text/InsertingScrubber;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nExpirationDateScrubber.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ExpirationDateScrubber.kt\ncom/squareup/text/ExpirationDateScrubber\n*L\n1#1,153:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002B\u0015\u0008\u0000\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0008\u0010\u0010\u001a\u00020\u0005H\u0002J\u0018\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u000fH\u0016J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/text/ExpirationDateScrubber;",
        "Lcom/squareup/text/InsertingScrubber;",
        "()V",
        "calendarProvider",
        "Lkotlin/Function0;",
        "Ljava/util/Calendar;",
        "(Lkotlin/jvm/functions/Function0;)V",
        "MMYY_FORMAT",
        "Ljava/text/SimpleDateFormat;",
        "onInvalidContentListener",
        "Lcom/squareup/text/OnInvalidContentListener;",
        "isThisMonthOrLater",
        "",
        "beginningOfTheMonth",
        "text",
        "",
        "newMonthStart",
        "scrub",
        "current",
        "proposed",
        "setOnInvalidContentListener",
        "",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final MMYY_FORMAT:Ljava/text/SimpleDateFormat;

.field private final calendarProvider:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field private onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/text/ExpirationDateScrubber$1;->INSTANCE:Lcom/squareup/text/ExpirationDateScrubber$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0}, Lcom/squareup/text/ExpirationDateScrubber;-><init>(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Ljava/util/Calendar;",
            ">;)V"
        }
    .end annotation

    const-string v0, "calendarProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/ExpirationDateScrubber;->calendarProvider:Lkotlin/jvm/functions/Function0;

    .line 35
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "MMyy"

    invoke-direct {p1, v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/text/ExpirationDateScrubber;->MMYY_FORMAT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private final isThisMonthOrLater(Ljava/util/Calendar;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 144
    :try_start_0
    iget-object v2, p0, Lcom/squareup/text/ExpirationDateScrubber;->MMYY_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    const-string v3, "MMYY_FORMAT.parse(text)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/util/Dates;->toCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    if-gtz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    new-array p1, v0, [Ljava/lang/Object;

    aput-object p2, p1, v1

    const-string p2, "Could not parse %s."

    .line 146
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return v1
.end method

.method private final newMonthStart()Ljava/util/Calendar;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/text/ExpirationDateScrubber;->calendarProvider:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/4 v1, 0x1

    .line 42
    invoke-static {v0, v1}, Lcom/squareup/util/Calendars;->setDay(Ljava/util/Calendar;I)V

    const/4 v1, 0x0

    .line 43
    invoke-static {v0, v1}, Lcom/squareup/util/Calendars;->setHour(Ljava/util/Calendar;I)V

    .line 44
    invoke-static {v0, v1}, Lcom/squareup/util/Calendars;->setMinute(Ljava/util/Calendar;I)V

    .line 45
    invoke-static {v0, v1}, Lcom/squareup/util/Calendars;->setSecond(Ljava/util/Calendar;I)V

    .line 46
    invoke-static {v0, v1}, Lcom/squareup/util/Calendars;->setMillisecond(Ljava/util/Calendar;I)V

    return-object v0
.end method


# virtual methods
.method public scrub(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    const-string v0, "current"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proposed"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 60
    invoke-static {p1, p2, v1, v0, v2}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v3

    if-ne v2, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 61
    :goto_0
    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 62
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    const-string p1, ""

    return-object p1

    .line 63
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    const/4 v6, 0x4

    const-string v7, "null cannot be cast to non-null type java.lang.String"

    if-le v4, v6, :cond_3

    if-eqz p2, :cond_2

    .line 64
    invoke-virtual {p2, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v7}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-eqz v2, :cond_5

    .line 70
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result p1

    if-nez p1, :cond_5

    .line 71
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p1, v3

    if-eqz p2, :cond_4

    invoke-virtual {p2, v1, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v7}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 76
    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/squareup/text/ExpirationDateScrubber;->newMonthStart()Ljava/util/Calendar;

    move-result-object p1

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_15

    .line 79
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    const-string v4, "(this as java.lang.String).toCharArray()"

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v4, p2

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_3
    if-ge v5, v4, :cond_11

    aget-char v7, p2, v5

    .line 80
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    const/16 v9, 0x30

    if-eqz v8, :cond_d

    const-string v10, "sb.append(ch)"

    if-eq v8, v3, :cond_a

    if-eq v8, v0, :cond_8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_6

    goto/16 :goto_6

    .line 126
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8}, Lcom/squareup/text/ExpirationDateScrubber;->isThisMonthOrLater(Ljava/util/Calendar;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 127
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    :goto_4
    const/4 v6, 0x1

    goto :goto_6

    :cond_8
    add-int/lit8 v8, v7, -0x30

    .line 115
    invoke-static {p1}, Lcom/squareup/util/Calendars;->getYear(Ljava/util/Calendar;)I

    move-result v9

    rem-int/lit8 v9, v9, 0x64

    invoke-static {p1}, Lcom/squareup/util/Calendars;->getYear(Ljava/util/Calendar;)I

    move-result v11

    rem-int/lit8 v11, v11, 0xa

    sub-int/2addr v9, v11

    div-int/lit8 v9, v9, 0xa

    if-eq v8, v9, :cond_9

    add-int/lit8 v9, v9, 0x1

    if-ne v8, v9, :cond_7

    .line 119
    :cond_9
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 95
    :cond_a
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    if-eq v8, v9, :cond_b

    packed-switch v7, :pswitch_data_0

    goto :goto_4

    .line 104
    :pswitch_0
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    :cond_b
    if-ne v7, v9, :cond_c

    goto :goto_4

    .line 101
    :cond_c
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    :cond_d
    if-eq v7, v9, :cond_f

    const/16 v8, 0x31

    if-ne v7, v8, :cond_e

    goto :goto_5

    .line 88
    :cond_e
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 85
    :cond_f
    :goto_5
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_6
    if-eqz v6, :cond_10

    goto :goto_7

    :cond_10
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    :cond_11
    :goto_7
    if-eqz v6, :cond_13

    .line 135
    iget-object p1, p0, Lcom/squareup/text/ExpirationDateScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    if-eqz p1, :cond_13

    if-nez p1, :cond_12

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_12
    invoke-interface {p1}, Lcom/squareup/text/OnInvalidContentListener;->onInvalidContent()V

    .line 137
    :cond_13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-lt p1, v0, :cond_14

    const/16 p1, 0x2f

    invoke-virtual {v2, v0, p1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 139
    :cond_14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "sb.toString()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 79
    :cond_15
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v7}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final setOnInvalidContentListener(Lcom/squareup/text/OnInvalidContentListener;)V
    .locals 1

    const-string v0, "onInvalidContentListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/text/ExpirationDateScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    return-void
.end method
