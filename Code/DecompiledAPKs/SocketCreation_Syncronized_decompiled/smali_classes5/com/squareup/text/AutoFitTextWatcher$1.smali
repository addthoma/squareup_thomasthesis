.class Lcom/squareup/text/AutoFitTextWatcher$1;
.super Ljava/lang/Object;
.source "AutoFitTextWatcher.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/text/AutoFitTextWatcher;->afterTextChanged(Landroid/text/Editable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/text/AutoFitTextWatcher;

.field final synthetic val$editable:Landroid/text/Editable;


# direct methods
.method constructor <init>(Lcom/squareup/text/AutoFitTextWatcher;Landroid/text/Editable;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->this$0:Lcom/squareup/text/AutoFitTextWatcher;

    iput-object p2, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->val$editable:Landroid/text/Editable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 6

    .line 33
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->this$0:Lcom/squareup/text/AutoFitTextWatcher;

    invoke-static {v0}, Lcom/squareup/text/AutoFitTextWatcher;->access$000(Lcom/squareup/text/AutoFitTextWatcher;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    return v2

    .line 37
    :cond_0
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->this$0:Lcom/squareup/text/AutoFitTextWatcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/text/AutoFitTextWatcher;->access$102(Lcom/squareup/text/AutoFitTextWatcher;Landroid/view/ViewTreeObserver$OnPreDrawListener;)Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 40
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->this$0:Lcom/squareup/text/AutoFitTextWatcher;

    invoke-static {v0}, Lcom/squareup/text/AutoFitTextWatcher;->access$000(Lcom/squareup/text/AutoFitTextWatcher;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->this$0:Lcom/squareup/text/AutoFitTextWatcher;

    invoke-virtual {v3}, Lcom/squareup/text/AutoFitTextWatcher;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->val$editable:Landroid/text/Editable;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/text/AutoFitTextWatcher;->autoFitText(Landroid/graphics/Rect;Landroid/text/Editable;)F

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 43
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher$1;->this$0:Lcom/squareup/text/AutoFitTextWatcher;

    invoke-static {v0}, Lcom/squareup/text/AutoFitTextWatcher;->access$000(Lcom/squareup/text/AutoFitTextWatcher;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestLayout()V

    return v2
.end method
