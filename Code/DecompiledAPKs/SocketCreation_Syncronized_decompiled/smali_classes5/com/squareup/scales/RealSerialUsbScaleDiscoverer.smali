.class public final Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;
.super Ljava/lang/Object;
.source "SerialUsbScaleDiscoverer.kt"

# interfaces
.implements Lcom/squareup/scales/SerialUsbScaleDiscoverer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;,
        Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSerialUsbScaleDiscoverer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SerialUsbScaleDiscoverer.kt\ncom/squareup/scales/RealSerialUsbScaleDiscoverer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,82:1\n1642#2,2:83\n*E\n*S KotlinDebug\n*F\n+ 1 SerialUsbScaleDiscoverer.kt\ncom/squareup/scales/RealSerialUsbScaleDiscoverer\n*L\n46#1,2:83\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001a2\u00020\u0001:\u0002\u001a\u001bB=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0012\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0016H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;",
        "Lcom/squareup/scales/SerialUsbScaleDiscoverer;",
        "usbDiscoverer",
        "Lcom/squareup/usb/UsbDiscoverer;",
        "manager",
        "Lcom/squareup/hardware/usb/UsbManager;",
        "realScaleTracker",
        "Lcom/squareup/scales/RealScaleTracker;",
        "usbScaleInterpreter",
        "Lcom/squareup/scales/UsbScaleInterpreter;",
        "analytics",
        "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
        "backgroundThreadExecutor",
        "Ljava/util/concurrent/Executor;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)V",
        "deviceNameToCable",
        "",
        "",
        "Lcom/squareup/scales/UsbCable;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "Companion",
        "DiscovererListener",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FTDI_VENDOR_ID:I = 0x403

.field public static final PROLIFIC_TECHNOLOGY_VENDOR_ID:I = 0x67b


# instance fields
.field private final analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

.field private final backgroundThreadExecutor:Ljava/util/concurrent/Executor;

.field private final deviceNameToCable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/scales/UsbCable;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final manager:Lcom/squareup/hardware/usb/UsbManager;

.field private final realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

.field private final usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

.field private final usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->Companion:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)V
    .locals 1

    const-string/jumbo v0, "usbDiscoverer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realScaleTracker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usbScaleInterpreter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backgroundThreadExecutor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    iput-object p2, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->manager:Lcom/squareup/hardware/usb/UsbManager;

    iput-object p3, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    iput-object p4, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    iput-object p5, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    iput-object p6, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    iput-object p7, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 36
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->deviceNameToCable:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getBackgroundThreadExecutor$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method public static final synthetic access$getDeviceNameToCable$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/Map;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->deviceNameToCable:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method public static final synthetic access$getManager$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/hardware/usb/UsbManager;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->manager:Lcom/squareup/hardware/usb/UsbManager;

    return-object p0
.end method

.method public static final synthetic access$getRealScaleTracker$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/scales/RealScaleTracker;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    return-object p0
.end method

.method public static final synthetic access$getUsbScaleInterpreter$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/scales/UsbScaleInterpreter;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 39
    iget-object p1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    new-instance v0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;

    invoke-direct {v0, p0}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;-><init>(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)V

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    const/16 v1, 0x67b

    invoke-virtual {p1, v1, v0}, Lcom/squareup/usb/UsbDiscoverer;->setDeviceListenerForVendorId(ILcom/squareup/usb/UsbDiscoverer$DeviceListener;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    new-instance v0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;

    invoke-direct {v0, p0}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;-><init>(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)V

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    const/16 v1, 0x403

    invoke-virtual {p1, v1, v0}, Lcom/squareup/usb/UsbDiscoverer;->setDeviceListenerForVendorId(ILcom/squareup/usb/UsbDiscoverer$DeviceListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0x67b

    invoke-virtual {v0, v1}, Lcom/squareup/usb/UsbDiscoverer;->removeDeviceListenerForVendorId(I)V

    .line 45
    iget-object v0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0x403

    invoke-virtual {v0, v1}, Lcom/squareup/usb/UsbDiscoverer;->removeDeviceListenerForVendorId(I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->deviceNameToCable:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 83
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/scales/UsbCable;

    .line 46
    invoke-virtual {v1}, Lcom/squareup/scales/UsbCable;->onDisconnected()V

    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->deviceNameToCable:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method
