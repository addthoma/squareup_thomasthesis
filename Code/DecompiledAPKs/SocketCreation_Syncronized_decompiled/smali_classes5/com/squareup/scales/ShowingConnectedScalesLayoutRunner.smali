.class public final Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;
.super Ljava/lang/Object;
.source "ShowingConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Factory;,
        Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;,
        Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/scales/ShowingConnectedScalesScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nShowingConnectedScalesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n+ 4 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpec\n*L\n1#1,222:1\n1360#2:223\n1429#2,3:224\n15#3,8:227\n20#3,4:235\n15#3,8:241\n20#3,4:249\n135#4,2:239\n*E\n*S KotlinDebug\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner\n*L\n117#1:223\n117#1,3:224\n180#1,8:227\n180#1,4:235\n197#1,8:241\n197#1,4:249\n188#1,2:239\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003*+,B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u0018\u0010\u0017\u001a\u00020\u000e*\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00190\u0018H\u0002J\"\u0010\u001a\u001a\u00020\u000e\"\u0008\u0008\u0000\u0010\u001b*\u00020\u001c*\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u0002H\u001b0\u001dH\u0002J\"\u0010\u001f\u001a\u00020\u000e\"\u0008\u0008\u0000\u0010\u001b*\u00020\u001c*\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u0002H\u001b0\u001dH\u0002J\u000c\u0010 \u001a\u00020!*\u00020\"H\u0002J\u000c\u0010#\u001a\u00020!*\u00020$H\u0002J\u000c\u0010%\u001a\u00020\u0019*\u00020&H\u0002J\u0018\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\r0(*\u0008\u0012\u0004\u0012\u00020&0)H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R%\u0010\n\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0004\u0012\u00020\u000e0\u000b\u00a2\u0006\u0002\u0008\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/scales/ShowingConnectedScalesScreen;",
        "view",
        "Landroid/view/View;",
        "scalesActionBarConfig",
        "Lcom/squareup/scales/ScalesActionBarConfig;",
        "(Landroid/view/View;Lcom/squareup/scales/ScalesActionBarConfig;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "recyclerConfig",
        "Lkotlin/Function1;",
        "Lcom/squareup/cycler/Recycler$Config;",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "root",
        "Lcom/squareup/mosaic/core/Root;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "scaleRowModel",
        "Lcom/squareup/cycler/mosaic/MosaicRowSpec;",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ConnectedScaleRow;",
        "scalesFooter",
        "P",
        "",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "scalesHeaderModel",
        "toAbbreviationString",
        "",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "toFullName",
        "Lcom/squareup/scales/ScaleTracker$ConnectionType;",
        "toRow",
        "Lcom/squareup/connectedscalesdata/ConnectedScale;",
        "toRowsDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "",
        "Binding",
        "Factory",
        "Row",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recyclerConfig:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cycler/Recycler$Config<",
            "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final root:Lcom/squareup/mosaic/core/Root;

.field private final scalesActionBarConfig:Lcom/squareup/scales/ScalesActionBarConfig;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/scales/ScalesActionBarConfig;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scalesActionBarConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->scalesActionBarConfig:Lcom/squareup/scales/ScalesActionBarConfig;

    .line 66
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 72
    iget-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 70
    new-instance p2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 71
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/connectedscales/impl/R$string;->scales_action_bar_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 72
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/connectedscales/impl/R$id;->connected_scales_container:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.connected_scales_container)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/mosaic/core/Root;

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->root:Lcom/squareup/mosaic/core/Root;

    .line 146
    new-instance p1, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;

    invoke-direct {p1, p0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;-><init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->recyclerConfig:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getRecyclerConfig$p(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->recyclerConfig:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$scaleRowModel(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/cycler/mosaic/MosaicRowSpec;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->scaleRowModel(Lcom/squareup/cycler/mosaic/MosaicRowSpec;)V

    return-void
.end method

.method public static final synthetic access$scalesFooter(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->scalesFooter(Lcom/squareup/blueprint/BlueprintContext;)V

    return-void
.end method

.method public static final synthetic access$scalesHeaderModel(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->scalesHeaderModel(Lcom/squareup/blueprint/BlueprintContext;)V

    return-void
.end method

.method public static final synthetic access$toRowsDataSource(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Ljava/util/List;)Lcom/squareup/cycler/DataSource;
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->toRowsDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method private final scaleRowModel(Lcom/squareup/cycler/mosaic/MosaicRowSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/mosaic/MosaicRowSpec<",
            "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
            "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ConnectedScaleRow;",
            ">;)V"
        }
    .end annotation

    .line 239
    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$scaleRowModel$$inlined$model$1;

    invoke-direct {v0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$scaleRowModel$$inlined$model$1;-><init>()V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method private final scalesFooter(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    .line 245
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 249
    invoke-interface {p1}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 245
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 198
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    sget-object v2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$scalesFooter$1$1;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$scalesFooter$1$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/LabelUiModelKt;->label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    .line 202
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 245
    invoke-interface {p1, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private final scalesHeaderModel(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    .line 231
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 235
    invoke-interface {p1}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 231
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 181
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    sget-object v2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$scalesHeaderModel$1$1;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$scalesHeaderModel$1$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/LabelUiModelKt;->label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    .line 185
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 231
    invoke-interface {p1, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private final toAbbreviationString(Lcom/squareup/scales/UnitOfMeasurement;)Ljava/lang/String;
    .locals 1

    .line 138
    sget-object v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/scales/UnitOfMeasurement;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 143
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->weight_unit_abbreviation_ounce_lowercase:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 142
    :cond_1
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->weight_unit_abbreviation_milligram_lowercase:I

    goto :goto_0

    .line 141
    :cond_2
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->weight_unit_abbreviation_pound_lowercase:I

    goto :goto_0

    .line 140
    :cond_3
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->weight_unit_abbreviation_kilogram_lowercase:I

    goto :goto_0

    .line 139
    :cond_4
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->weight_unit_abbreviation_gram_lowercase:I

    .line 144
    :goto_0
    iget-object v0, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "view.context.getString(it)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "when (this) {\n      G ->\u2026w.context.getString(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toFullName(Lcom/squareup/scales/ScaleTracker$ConnectionType;)Ljava/lang/String;
    .locals 1

    .line 131
    sget-object v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/scales/ScaleTracker$ConnectionType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 134
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid connectivity type"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 133
    :cond_1
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->connection_type_bluetooth:I

    goto :goto_0

    .line 132
    :cond_2
    sget p1, Lcom/squareup/connectedscales/impl/R$string;->connection_type_usb:I

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "when (this) {\n    USB ->\u2026w.context.getString(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toRow(Lcom/squareup/connectedscalesdata/ConnectedScale;)Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ConnectedScaleRow;
    .locals 5

    .line 121
    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ConnectedScaleRow;

    .line 122
    invoke-virtual {p1}, Lcom/squareup/connectedscalesdata/ConnectedScale;->getName()Ljava/lang/String;

    move-result-object v1

    .line 124
    iget-object v2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    sget v3, Lcom/squareup/connectedscales/impl/R$string;->connection_type_and_weight_unit_pattern:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 125
    invoke-virtual {p1}, Lcom/squareup/connectedscalesdata/ConnectedScale;->getConnectionType()Lcom/squareup/scales/ScaleTracker$ConnectionType;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->toFullName(Lcom/squareup/scales/ScaleTracker$ConnectionType;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, "connection_type"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 126
    invoke-virtual {p1}, Lcom/squareup/connectedscalesdata/ConnectedScale;->getUnitOfMeasurement()Lcom/squareup/scales/UnitOfMeasurement;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->toAbbreviationString(Lcom/squareup/scales/UnitOfMeasurement;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string/jumbo v3, "weight_unit"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 128
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 121
    invoke-direct {v0, v1, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ConnectedScaleRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final toRowsDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/connectedscalesdata/ConnectedScale;",
            ">;)",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
            ">;"
        }
    .end annotation

    .line 117
    sget-object v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ScalesHeaderRow;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ScalesHeaderRow;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    .line 223
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 224
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 225
    check-cast v2, Lcom/squareup/connectedscalesdata/ConnectedScale;

    .line 117
    invoke-direct {p0, v2}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->toRow(Lcom/squareup/connectedscalesdata/ConnectedScale;)Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$ConnectedScaleRow;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 117
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    sget-object v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 118
    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    return-object p1
.end method

.method private final updateActionBar(Lcom/squareup/scales/ShowingConnectedScalesScreen;)V
    .locals 4

    .line 110
    iget-object v0, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 102
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 104
    iget-object v2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->scalesActionBarConfig:Lcom/squareup/scales/ScalesActionBarConfig;

    invoke-interface {v2}, Lcom/squareup/scales/ScalesActionBarConfig;->getShowBackButton()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/scales/ShowingConnectedScalesScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 107
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 110
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/scales/ShowingConnectedScalesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object p2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$1;-><init>(Lcom/squareup/scales/ShowingConnectedScalesScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->updateActionBar(Lcom/squareup/scales/ShowingConnectedScalesScreen;)V

    .line 84
    iget-object p2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->root:Lcom/squareup/mosaic/core/Root;

    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;-><init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/scales/ShowingConnectedScalesScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/mosaic/core/Root;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/scales/ShowingConnectedScalesScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->showRendering(Lcom/squareup/scales/ShowingConnectedScalesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
