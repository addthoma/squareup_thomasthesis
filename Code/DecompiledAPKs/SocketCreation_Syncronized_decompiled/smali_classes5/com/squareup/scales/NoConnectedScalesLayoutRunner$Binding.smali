.class public final Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;
.super Ljava/lang/Object;
.source "NoConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/NoConnectedScalesLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Binding"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007JQ\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u00172\u0018\u0010\u0018\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001aj\u0002`\u001b0\u00192\u0006\u0010\u001c\u001a\u00020\u001dH\u0096\u0001R\u0012\u0010\u0008\u001a\u00020\tX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u001e\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\rX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "Lcom/squareup/scales/NoConnectedScalesScreen;",
        "",
        "Lcom/squareup/workflow/V2LayoutBinding;",
        "factory",
        "Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;",
        "(Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;)V",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "inflate",
        "Landroid/view/View;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;


# direct methods
.method public constructor <init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;)V
    .locals 9

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 109
    const-class v0, Lcom/squareup/scales/NoConnectedScalesScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 110
    sget v3, Lcom/squareup/connectedscales/impl/R$layout;->no_connected_scales_layout:I

    .line 111
    new-instance v0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding$1;

    invoke-direct {v0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding$1;-><init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 108
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    return-void
.end method


# virtual methods
.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method

.method public inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "contextForNewView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
