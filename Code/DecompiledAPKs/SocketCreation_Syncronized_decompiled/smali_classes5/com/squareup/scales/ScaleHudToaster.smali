.class public final Lcom/squareup/scales/ScaleHudToaster;
.super Ljava/lang/Object;
.source "ScaleHudToaster.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScaleHudToaster.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScaleHudToaster.kt\ncom/squareup/scales/ScaleHudToaster\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B9\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0015H\u0016J\u0008\u0010\u0019\u001a\u00020\u0015H\u0002J\u0008\u0010\u001a\u001a\u00020\u0015H\u0002J\u001a\u0010\u001b\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/scales/ScaleHudToaster;",
        "Lmortar/Scoped;",
        "scaleTracker",
        "Lcom/squareup/scales/ScaleTracker;",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "scaleToastDelay",
        "",
        "(Lcom/squareup/scales/ScaleTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/executor/MainThread;Z)V",
        "SCALE_HUD_TOAST_DELAY_MS",
        "",
        "currentScaleKeys",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "showScaleConnected",
        "showScaleDisconnected",
        "showToast",
        "drawableId",
        "",
        "title",
        "",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final SCALE_HUD_TOAST_DELAY_MS:J

.field private final currentScaleKeys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final res:Lcom/squareup/util/Res;

.field private final scaleToastDelay:Z

.field private final scaleTracker:Lcom/squareup/scales/ScaleTracker;


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScaleTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/executor/MainThread;Z)V
    .locals 1
    .param p6    # Z
        .annotation runtime Lcom/squareup/scales/ScaleToastDelay;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scaleTracker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hudToaster"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/ScaleHudToaster;->scaleTracker:Lcom/squareup/scales/ScaleTracker;

    iput-object p2, p0, Lcom/squareup/scales/ScaleHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p3, p0, Lcom/squareup/scales/ScaleHudToaster;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/scales/ScaleHudToaster;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/scales/ScaleHudToaster;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-boolean p6, p0, Lcom/squareup/scales/ScaleHudToaster;->scaleToastDelay:Z

    const-wide/16 p1, 0x3e8

    .line 26
    iput-wide p1, p0, Lcom/squareup/scales/ScaleHudToaster;->SCALE_HUD_TOAST_DELAY_MS:J

    .line 29
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/scales/ScaleHudToaster;->currentScaleKeys:Ljava/util/Set;

    return-void
.end method

.method public static final synthetic access$getCurrentScaleKeys$p(Lcom/squareup/scales/ScaleHudToaster;)Ljava/util/Set;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/scales/ScaleHudToaster;->currentScaleKeys:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic access$getHudToaster$p(Lcom/squareup/scales/ScaleHudToaster;)Lcom/squareup/hudtoaster/HudToaster;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/scales/ScaleHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-object p0
.end method

.method public static final synthetic access$showScaleConnected(Lcom/squareup/scales/ScaleHudToaster;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/scales/ScaleHudToaster;->showScaleConnected()V

    return-void
.end method

.method public static final synthetic access$showScaleDisconnected(Lcom/squareup/scales/ScaleHudToaster;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/scales/ScaleHudToaster;->showScaleDisconnected()V

    return-void
.end method

.method private final showScaleConnected()V
    .locals 3

    .line 59
    sget v0, Lcom/squareup/hardware/R$drawable;->icon_scale_connected:I

    iget-object v1, p0, Lcom/squareup/scales/ScaleHudToaster;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/hardware/R$string;->scale_connected:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/squareup/scales/ScaleHudToaster;->showToast(ILjava/lang/CharSequence;)V

    return-void
.end method

.method private final showScaleDisconnected()V
    .locals 3

    .line 63
    sget v0, Lcom/squareup/hardware/R$drawable;->icon_scale_disconnected:I

    iget-object v1, p0, Lcom/squareup/scales/ScaleHudToaster;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/hardware/R$string;->scale_disconnected:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/squareup/scales/ScaleHudToaster;->showToast(ILjava/lang/CharSequence;)V

    return-void
.end method

.method private final showToast(ILjava/lang/CharSequence;)V
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/scales/ScaleHudToaster$showToast$toastRunnable$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/scales/ScaleHudToaster$showToast$toastRunnable$1;-><init>(Lcom/squareup/scales/ScaleHudToaster;ILjava/lang/CharSequence;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 69
    iget-boolean p1, p0, Lcom/squareup/scales/ScaleHudToaster;->scaleToastDelay:Z

    if-eqz p1, :cond_0

    .line 71
    iget-object p1, p0, Lcom/squareup/scales/ScaleHudToaster;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance p2, Lcom/squareup/scales/ScaleHudToaster$showToast$1;

    invoke-direct {p2, v0}, Lcom/squareup/scales/ScaleHudToaster$showToast$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Ljava/lang/Runnable;

    iget-wide v0, p0, Lcom/squareup/scales/ScaleHudToaster;->SCALE_HUD_TOAST_DELAY_MS:J

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 74
    :cond_0
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_0
    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster;->scaleTracker:Lcom/squareup/scales/ScaleTracker;

    invoke-interface {v0}, Lcom/squareup/scales/ScaleTracker;->getConnectedScales()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    sget-object v1, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$1;->INSTANCE:Lcom/squareup/scales/ScaleHudToaster$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "scaleTracker\n        .co\u2026\n        .map { it.keys }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v1, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/scales/ScaleHudToaster$onEnterScope$2;-><init>(Lcom/squareup/scales/ScaleHudToaster;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
