.class public final Lcom/squareup/scales/ScaleTrackerModule;
.super Ljava/lang/Object;
.source "ScaleTrackerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/scales/ScaleTrackerModule;",
        "",
        "()V",
        "provideScaleHudDelay",
        "",
        "provideScaleTracker",
        "Lcom/squareup/scales/ScaleTracker;",
        "scaleTracker",
        "Lcom/squareup/scales/RealScaleTracker;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/scales/ScaleTrackerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/scales/ScaleTrackerModule;

    invoke-direct {v0}, Lcom/squareup/scales/ScaleTrackerModule;-><init>()V

    sput-object v0, Lcom/squareup/scales/ScaleTrackerModule;->INSTANCE:Lcom/squareup/scales/ScaleTrackerModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideScaleHudDelay()Z
    .locals 1
    .annotation runtime Lcom/squareup/scales/ScaleToastDelay;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x1

    return v0
.end method

.method public static final provideScaleTracker(Lcom/squareup/scales/RealScaleTracker;)Lcom/squareup/scales/ScaleTracker;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scaleTracker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    check-cast p0, Lcom/squareup/scales/ScaleTracker;

    return-object p0
.end method
