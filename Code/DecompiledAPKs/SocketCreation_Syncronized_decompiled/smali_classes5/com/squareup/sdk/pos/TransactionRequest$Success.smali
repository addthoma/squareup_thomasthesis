.class public Lcom/squareup/sdk/pos/TransactionRequest$Success;
.super Ljava/lang/Object;
.source "TransactionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/TransactionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Success"
.end annotation


# instance fields
.field public final state:Ljava/lang/String;

.field public final transaction:Lcom/squareup/sdk/pos/transaction/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/sdk/pos/transaction/Transaction;Ljava/lang/String;)V
    .locals 0

    .line 351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 352
    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Success;->transaction:Lcom/squareup/sdk/pos/transaction/Transaction;

    .line 353
    iput-object p2, p0, Lcom/squareup/sdk/pos/TransactionRequest$Success;->state:Ljava/lang/String;

    return-void
.end method
