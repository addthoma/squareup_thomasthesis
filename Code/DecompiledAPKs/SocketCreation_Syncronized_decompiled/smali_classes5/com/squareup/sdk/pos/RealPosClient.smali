.class final Lcom/squareup/sdk/pos/RealPosClient;
.super Ljava/lang/Object;
.source "RealPosClient.java"

# interfaces
.implements Lcom/squareup/sdk/pos/PosClient;


# static fields
.field private static final API_VERSION:Ljava/lang/String; = "v3.0"

.field private static final PLAY_STORE_APP_URL:Landroid/net/Uri;

.field private static final PLAY_STORE_WEB_URL:Landroid/net/Uri;

.field private static final POINT_OF_SALE_FINGERPRINT:Ljava/lang/String; = "EA:54:A3:62:C8:5B:F4:34:F2:9F:B6:B0:42:D8:3E:5C:7D:C3:8A:D3"

.field private static final POINT_OF_SALE_PACKAGE_NAME:Ljava/lang/String; = "com.squareup"

.field private static final SDK_VERSION:Ljava/lang/String; = "point-of-sale-sdk-3.0-SQUARE-7-ac368ef"


# instance fields
.field private final clientId:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private final packageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://play.google.com/store/apps/details?id=com.squareup"

    .line 53
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/squareup/sdk/pos/RealPosClient;->PLAY_STORE_WEB_URL:Landroid/net/Uri;

    const-string v0, "market://details?id=com.squareup"

    .line 55
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/squareup/sdk/pos/RealPosClient;->PLAY_STORE_APP_URL:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/sdk/pos/RealPosClient;->context:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/squareup/sdk/pos/RealPosClient;->clientId:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/pos/RealPosClient;->packageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private createPinnedChargeIntent(Lcom/squareup/sdk/pos/TransactionRequest;Landroid/content/pm/PackageInfo;)Landroid/content/Intent;
    .locals 4

    .line 152
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.squareup.pos.action.CHARGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/squareup/sdk/pos/RealPosClient;->clientId:Ljava/lang/String;

    const-string v2, "com.squareup.pos.CLIENT_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    iget v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->totalAmount:I

    const-string v2, "com.squareup.pos.TOTAL_AMOUNT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 155
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    const-string v2, "com.squareup.pos.NOTE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.squareup.pos.API_VERSION"

    const-string/jumbo v2, "v3.0"

    .line 156
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.squareup.pos.SDK_VERSION"

    const-string v2, "point-of-sale-sdk-3.0-SQUARE-7-ac368ef"

    .line 157
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/sdk/pos/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.squareup.pos.CURRENCY_CODE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    const-string v2, "com.squareup.pos.STATE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    iget-boolean v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->autoReturn:Z

    const-string v2, "com.squareup.pos.AUTO_RETURN"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    iget-boolean v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->skipReceipt:Z

    const-string v2, "com.squareup.pos.SKIP_RECEIPT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    iget-boolean v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->allowSplitTender:Z

    const-string v2, "com.squareup.pos.ALLOW_SPLIT_TENDER"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 163
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 164
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    const-string v2, "com.squareup.pos.CUSTOMER_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    iget-object v2, p1, Lcom/squareup/sdk/pos/TransactionRequest;->tenderTypes:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    .line 169
    iget-object v3, v3, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;->apiExtraName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v2, "com.squareup.pos.TENDER_TYPES"

    .line 171
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 173
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 174
    iget-object p1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    const-string v1, "com.squareup.pos.LOCATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    :cond_2
    iget-object p1, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private findPointOfSaleWithHighestVersion(Ljava/util/List;)Landroid/content/pm/PackageInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Landroid/content/pm/PackageInfo;"
        }
    .end annotation

    .line 128
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 129
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 130
    invoke-direct {p0, v1}, Lcom/squareup/sdk/pos/RealPosClient;->isPointOfSale(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 135
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/squareup/sdk/pos/RealPosClient;->packageManager:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    .line 140
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v2, v3, :cond_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    return-object v0

    .line 145
    :cond_4
    new-instance p1, Landroid/content/ActivityNotFoundException;

    const-string v0, "Square Point of Sale is not installed on this device."

    invoke-direct {p1, v0}, Landroid/content/ActivityNotFoundException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private isPlayStoreInstalled()Z
    .locals 3

    const/4 v0, 0x0

    .line 250
    :try_start_0
    iget-object v1, p0, Lcom/squareup/sdk/pos/RealPosClient;->packageManager:Landroid/content/pm/PackageManager;

    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :catch_0
    return v0
.end method

.method private isPointOfSale(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "com.squareup"

    .line 182
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EA:54:A3:62:C8:5B:F4:34:F2:9F:B6:B0:42:D8:3E:5C:7D:C3:8A:D3"

    .line 183
    invoke-direct {p0, p1, v0}, Lcom/squareup/sdk/pos/RealPosClient;->matchesExpectedFingerprint(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private matchesExpectedFingerprint(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    const/4 v0, 0x0

    .line 197
    :try_start_0
    iget-object v1, p0, Lcom/squareup/sdk/pos/RealPosClient;->packageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 202
    iget-object p1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz p1, :cond_3

    .line 203
    array-length v1, p1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    :try_start_1
    const-string v1, "X509"

    .line 209
    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    const-string v2, "SHA1"

    .line 215
    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    .line 219
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_2

    aget-object v5, p1, v4

    .line 220
    invoke-virtual {v5}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v5

    .line 221
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 224
    :try_start_3
    invoke-virtual {v1, v6}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v5

    check-cast v5, Ljava/security/cert/X509Certificate;
    :try_end_3
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_0

    .line 230
    :try_start_4
    invoke-virtual {v5}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v5
    :try_end_4
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_4 .. :try_end_4} :catch_0

    .line 234
    invoke-virtual {v2, v5}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v5

    .line 235
    invoke-static {v5}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v5

    .line 239
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    return v0

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    return v0

    :cond_2
    const/4 p1, 0x1

    return p1

    :catch_1
    :cond_3
    :goto_1
    return v0
.end method

.method private queryChargeActivities()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .line 122
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.squareup.pos.action.CHARGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/sdk/pos/RealPosClient;->packageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public createTransactionIntent(Lcom/squareup/sdk/pos/TransactionRequest;)Landroid/content/Intent;
    .locals 1

    const-string v0, "chargeRequest"

    .line 71
    invoke-static {p1, v0}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 72
    invoke-direct {p0}, Lcom/squareup/sdk/pos/RealPosClient;->queryChargeActivities()Ljava/util/List;

    move-result-object v0

    .line 73
    invoke-direct {p0, v0}, Lcom/squareup/sdk/pos/RealPosClient;->findPointOfSaleWithHighestVersion(Ljava/util/List;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 74
    invoke-direct {p0, p1, v0}, Lcom/squareup/sdk/pos/RealPosClient;->createPinnedChargeIntent(Lcom/squareup/sdk/pos/TransactionRequest;Landroid/content/pm/PackageInfo;)Landroid/content/Intent;

    move-result-object p1

    return-object p1
.end method

.method public isPointOfSaleInstalled()Z
    .locals 2

    .line 78
    invoke-direct {p0}, Lcom/squareup/sdk/pos/RealPosClient;->queryChargeActivities()Ljava/util/List;

    move-result-object v0

    .line 79
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 80
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 81
    invoke-direct {p0, v1}, Lcom/squareup/sdk/pos/RealPosClient;->isPointOfSale(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public launchPointOfSale()V
    .locals 2

    .line 89
    invoke-direct {p0}, Lcom/squareup/sdk/pos/RealPosClient;->queryChargeActivities()Ljava/util/List;

    move-result-object v0

    .line 90
    invoke-direct {p0, v0}, Lcom/squareup/sdk/pos/RealPosClient;->findPointOfSaleWithHighestVersion(Ljava/util/List;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/squareup/sdk/pos/RealPosClient;->packageManager:Landroid/content/pm/PackageManager;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 92
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/squareup/sdk/pos/RealPosClient;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public openPointOfSalePlayStoreListing()V
    .locals 3

    .line 97
    invoke-direct {p0}, Lcom/squareup/sdk/pos/RealPosClient;->isPlayStoreInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/sdk/pos/RealPosClient;->PLAY_STORE_APP_URL:Landroid/net/Uri;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/sdk/pos/RealPosClient;->PLAY_STORE_WEB_URL:Landroid/net/Uri;

    .line 98
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x10080000

    .line 100
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 102
    iget-object v0, p0, Lcom/squareup/sdk/pos/RealPosClient;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public parseTransactionError(Landroid/content/Intent;)Lcom/squareup/sdk/pos/TransactionRequest$Error;
    .locals 4

    const-string v0, "data"

    .line 114
    invoke-static {p1, v0}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 115
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$Error;

    const-string v1, "com.squareup.pos.ERROR_CODE"

    .line 116
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->parse(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    move-result-object v1

    const-string v2, "com.squareup.pos.ERROR_DESCRIPTION"

    .line 117
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.squareup.pos.STATE"

    .line 118
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/sdk/pos/TransactionRequest$Error;-><init>(Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public parseTransactionSuccess(Landroid/content/Intent;)Lcom/squareup/sdk/pos/TransactionRequest$Success;
    .locals 3

    const-string v0, "data"

    .line 107
    invoke-static {p1, v0}, Lcom/squareup/sdk/pos/internal/PosSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$Success;

    const-string v1, "com.squareup.pos.TRANSACTION"

    .line 109
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/sdk/pos/transaction/Transaction;

    const-string v2, "com.squareup.pos.STATE"

    .line 110
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/sdk/pos/TransactionRequest$Success;-><init>(Lcom/squareup/sdk/pos/transaction/Transaction;Ljava/lang/String;)V

    return-object v0
.end method
