.class public final Lcom/squareup/sdk/pos/PosApi;
.super Ljava/lang/Object;
.source "PosApi.java"


# static fields
.field public static final ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED:Ljava/lang/String; = "com.squareup.pos.ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

.field public static final ERROR_DISABLED:Ljava/lang/String; = "com.squareup.pos.ERROR_DISABLED"

.field public static final ERROR_GIFT_CARDS_NOT_SUPPORTED:Ljava/lang/String; = "com.squareup.pos.ERROR_GIFT_CARDS_NOT_SUPPORTED"

.field public static final ERROR_ILLEGAL_LOCATION_ID:Ljava/lang/String; = "com.squareup.pos.ERROR_ILLEGAL_LOCATION_ID"

.field public static final ERROR_INSUFFICIENT_CARD_BALANCE:Ljava/lang/String; = "com.squareup.pos.ERROR_INSUFFICIENT_CARD_BALANCE"

.field public static final ERROR_INVALID_CUSTOMER_ID:Ljava/lang/String; = "com.squareup.pos.ERROR_INVALID_CUSTOMER_ID"

.field public static final ERROR_INVALID_REQUEST:Ljava/lang/String; = "com.squareup.pos.ERROR_INVALID_REQUEST"

.field public static final ERROR_NO_EMPLOYEE_LOGGED_IN:Ljava/lang/String; = "com.squareup.pos.ERROR_NO_EMPLOYEE_LOGGED_IN"

.field public static final ERROR_NO_NETWORK:Ljava/lang/String; = "com.squareup.pos.ERROR_NO_NETWORK"

.field public static final ERROR_NO_RESULT:Ljava/lang/String; = "com.squareup.pos.ERROR_NO_RESULT"

.field public static final ERROR_TRANSACTION_ALREADY_IN_PROGRESS:Ljava/lang/String; = "com.squareup.pos.ERROR_TRANSACTION_ALREADY_IN_PROGRESS"

.field public static final ERROR_TRANSACTION_CANCELED:Ljava/lang/String; = "com.squareup.pos.ERROR_TRANSACTION_CANCELED"

.field public static final ERROR_UNAUTHORIZED_CLIENT_ID:Ljava/lang/String; = "com.squareup.pos.ERROR_UNAUTHORIZED_CLIENT_ID"

.field public static final ERROR_UNEXPECTED:Ljava/lang/String; = "com.squareup.pos.ERROR_UNEXPECTED"

.field public static final ERROR_UNSUPPORTED_API_VERSION:Ljava/lang/String; = "com.squareup.pos.UNSUPPORTED_API_VERSION"

.field public static final ERROR_USER_NOT_ACTIVATED:Ljava/lang/String; = "com.squareup.pos.ERROR_USER_NOT_ACTIVATED"

.field public static final ERROR_USER_NOT_LOGGED_IN:Ljava/lang/String; = "com.squareup.pos.ERROR_USER_NOT_LOGGED_IN"

.field public static final EXTRA_ALLOW_SPLIT_TENDER:Ljava/lang/String; = "com.squareup.pos.ALLOW_SPLIT_TENDER"

.field public static final EXTRA_API_VERSION:Ljava/lang/String; = "com.squareup.pos.API_VERSION"

.field public static final EXTRA_AUTO_RETURN:Ljava/lang/String; = "com.squareup.pos.AUTO_RETURN"

.field public static final EXTRA_CURRENCY_CODE:Ljava/lang/String; = "com.squareup.pos.CURRENCY_CODE"

.field public static final EXTRA_CUSTOMER_ID:Ljava/lang/String; = "com.squareup.pos.CUSTOMER_ID"

.field public static final EXTRA_LOCATION_ID:Ljava/lang/String; = "com.squareup.pos.LOCATION_ID"

.field public static final EXTRA_NOTE:Ljava/lang/String; = "com.squareup.pos.NOTE"

.field public static final EXTRA_POINT_OF_SALE_CLIENT_ID:Ljava/lang/String; = "com.squareup.pos.CLIENT_ID"

.field public static final EXTRA_REQUEST_STATE:Ljava/lang/String; = "com.squareup.pos.STATE"

.field public static final EXTRA_SDK_VERSION:Ljava/lang/String; = "com.squareup.pos.SDK_VERSION"

.field public static final EXTRA_SKIP_RECEIPT:Ljava/lang/String; = "com.squareup.pos.SKIP_RECEIPT"

.field public static final EXTRA_TENDER_CARD_FROM_READER:Ljava/lang/String; = "com.squareup.pos.TENDER_CARD_FROM_READER"

.field public static final EXTRA_TENDER_CARD_ON_FILE:Ljava/lang/String; = "com.squareup.pos.TENDER_CARD_ON_FILE"

.field public static final EXTRA_TENDER_CASH:Ljava/lang/String; = "com.squareup.pos.TENDER_CASH"

.field public static final EXTRA_TENDER_GIFT_CARD:Ljava/lang/String; = "com.squareup.pos.TENDER_GIFT_CARD"

.field public static final EXTRA_TENDER_KEYED_IN_CARD:Ljava/lang/String; = "com.squareup.pos.TENDER_KEYED_IN_CARD"

.field public static final EXTRA_TENDER_OTHER:Ljava/lang/String; = "com.squareup.pos.TENDER_OTHER"

.field public static final EXTRA_TENDER_TYPES:Ljava/lang/String; = "com.squareup.pos.TENDER_TYPES"

.field public static final EXTRA_TOTAL_AMOUNT:Ljava/lang/String; = "com.squareup.pos.TOTAL_AMOUNT"

.field public static final INTENT_ACTION_CHARGE:Ljava/lang/String; = "com.squareup.pos.action.CHARGE"

.field private static final NAMESPACE:Ljava/lang/String; = "com.squareup.pos."

.field public static final NOTE_MAX_LENGTH:I = 0x1f4

.field public static final RESULT_ERROR_CODE:Ljava/lang/String; = "com.squareup.pos.ERROR_CODE"

.field public static final RESULT_ERROR_DESCRIPTION:Ljava/lang/String; = "com.squareup.pos.ERROR_DESCRIPTION"

.field public static final RESULT_STATE:Ljava/lang/String; = "com.squareup.pos.STATE"

.field public static final RESULT_TRANSACTION:Ljava/lang/String; = "com.squareup.pos.TRANSACTION"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
