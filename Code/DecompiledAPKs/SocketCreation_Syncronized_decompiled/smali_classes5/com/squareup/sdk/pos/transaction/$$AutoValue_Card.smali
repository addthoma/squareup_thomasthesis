.class abstract Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;
.super Lcom/squareup/sdk/pos/transaction/Card;
.source "$$AutoValue_Card.java"


# instance fields
.field private final cardBrand:Lcom/squareup/sdk/pos/transaction/Card$Brand;

.field private final last4:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/sdk/pos/transaction/Card$Brand;Ljava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Card;-><init>()V

    if-eqz p1, :cond_1

    .line 15
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->cardBrand:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    if-eqz p2, :cond_0

    .line 19
    iput-object p2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->last4:Ljava/lang/String;

    return-void

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null last4"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null cardBrand"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public cardBrand()Lcom/squareup/sdk/pos/transaction/Card$Brand;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->cardBrand:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 45
    :cond_0
    instance-of v1, p1, Lcom/squareup/sdk/pos/transaction/Card;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 46
    check-cast p1, Lcom/squareup/sdk/pos/transaction/Card;

    .line 47
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->cardBrand:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Card;->cardBrand()Lcom/squareup/sdk/pos/transaction/Card$Brand;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/sdk/pos/transaction/Card$Brand;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->last4:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Card;->last4()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public hashCode()I
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->cardBrand:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    invoke-virtual {v0}, Lcom/squareup/sdk/pos/transaction/Card$Brand;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 59
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->last4:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public last4()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->last4:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Card{cardBrand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->cardBrand:Lcom/squareup/sdk/pos/transaction/Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", last4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Card;->last4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
