.class public abstract Lcom/squareup/sdk/pos/transaction/Order;
.super Ljava/lang/Object;
.source "Order.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/Order$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/squareup/sdk/pos/transaction/Order$Builder;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Order$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Order;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract totalMoney()Lcom/squareup/sdk/pos/transaction/Money;
.end method

.method public abstract totalTaxMoney()Lcom/squareup/sdk/pos/transaction/Money;
.end method

.method public abstract totalTipMoney()Lcom/squareup/sdk/pos/transaction/Money;
.end method
