.class public abstract Lcom/squareup/sdk/pos/transaction/Transaction;
.super Ljava/lang/Object;
.source "Transaction.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1

    .line 73
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Transaction;",
            ">;"
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Transaction$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Transaction$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method abstract autoTenders()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation
.end method

.method public abstract clientId()Ljava/lang/String;
.end method

.method public abstract createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;
.end method

.method public abstract locationId()Ljava/lang/String;
.end method

.method public abstract order()Lcom/squareup/sdk/pos/transaction/Order;
.end method

.method public abstract serverId()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public final tenders()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation

    .line 64
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/Transaction;->autoTenders()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public abstract toBuilder()Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.end method
