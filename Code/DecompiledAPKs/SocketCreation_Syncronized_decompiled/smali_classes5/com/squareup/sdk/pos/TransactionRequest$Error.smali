.class public Lcom/squareup/sdk/pos/TransactionRequest$Error;
.super Ljava/lang/Object;
.source "TransactionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/TransactionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Error"
.end annotation


# instance fields
.field public final code:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public final debugDescription:Ljava/lang/String;

.field public final state:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest$Error;->code:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 387
    iput-object p2, p0, Lcom/squareup/sdk/pos/TransactionRequest$Error;->debugDescription:Ljava/lang/String;

    .line 388
    iput-object p3, p0, Lcom/squareup/sdk/pos/TransactionRequest$Error;->state:Ljava/lang/String;

    return-void
.end method
