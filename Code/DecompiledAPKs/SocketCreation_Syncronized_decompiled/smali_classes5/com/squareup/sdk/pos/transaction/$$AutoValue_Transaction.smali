.class abstract Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;
.super Lcom/squareup/sdk/pos/transaction/Transaction;
.source "$$AutoValue_Transaction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;
    }
.end annotation


# instance fields
.field private final autoTenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation
.end field

.field private final clientId:Ljava/lang/String;

.field private final createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

.field private final locationId:Ljava/lang/String;

.field private final order:Lcom/squareup/sdk/pos/transaction/Order;

.field private final serverId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Order;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/sdk/pos/transaction/DateTime;",
            "Lcom/squareup/sdk/pos/transaction/Order;",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Transaction;-><init>()V

    if-eqz p1, :cond_4

    .line 27
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->clientId:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->serverId:Ljava/lang/String;

    if-eqz p3, :cond_3

    .line 32
    iput-object p3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->locationId:Ljava/lang/String;

    if-eqz p4, :cond_2

    .line 36
    iput-object p4, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    if-eqz p5, :cond_1

    .line 40
    iput-object p5, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->order:Lcom/squareup/sdk/pos/transaction/Order;

    if-eqz p6, :cond_0

    .line 44
    iput-object p6, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->autoTenders:Ljava/util/Set;

    return-void

    .line 42
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null autoTenders"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 38
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null order"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 34
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null createdAt"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 30
    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null locationId"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 25
    :cond_4
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null clientId"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method autoTenders()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->autoTenders:Ljava/util/Set;

    return-object v0
.end method

.method public clientId()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 97
    :cond_0
    instance-of v1, p1, Lcom/squareup/sdk/pos/transaction/Transaction;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 98
    check-cast p1, Lcom/squareup/sdk/pos/transaction/Transaction;

    .line 99
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->clientId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->clientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->serverId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->serverId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->serverId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->locationId:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->locationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    .line 102
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->order:Lcom/squareup/sdk/pos/transaction/Order;

    .line 103
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->order()Lcom/squareup/sdk/pos/transaction/Order;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->autoTenders:Ljava/util/Set;

    .line 104
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->autoTenders()Ljava/util/Set;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_3
    return v2
.end method

.method public hashCode()I
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->clientId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 115
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->serverId:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 117
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->locationId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 119
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 121
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->order:Lcom/squareup/sdk/pos/transaction/Order;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 123
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->autoTenders:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public locationId()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->locationId:Ljava/lang/String;

    return-object v0
.end method

.method public order()Lcom/squareup/sdk/pos/transaction/Order;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->order:Lcom/squareup/sdk/pos/transaction/Order;

    return-object v0
.end method

.method public serverId()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->serverId:Ljava/lang/String;

    return-object v0
.end method

.method public toBuilder()Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;-><init>(Lcom/squareup/sdk/pos/transaction/Transaction;Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$1;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction{clientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", serverId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", locationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->locationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->order:Lcom/squareup/sdk/pos/transaction/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", autoTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;->autoTenders:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
