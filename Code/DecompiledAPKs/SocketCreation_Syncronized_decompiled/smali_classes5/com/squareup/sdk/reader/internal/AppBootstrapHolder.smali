.class public final Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;
.super Ljava/lang/Object;
.source "AppBootstrapHolder.java"


# static fields
.field private static volatile appBootstrap:Lcom/squareup/sdk/reader/internal/AppBootstrap;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;
    .locals 2

    .line 38
    sget-object v0, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->appBootstrap:Lcom/squareup/sdk/reader/internal/AppBootstrap;

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->appBootstrap:Lcom/squareup/sdk/reader/internal/AppBootstrap;

    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/AppBootstrap;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    return-object v0

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call ReaderSdk#initialize first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static loadAppBootstrapFactory(Landroid/app/Application;)Lcom/squareup/sdk/reader/internal/BootstrapFactory;
    .locals 1

    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    const-string v0, "com.squareup.AppBootstrapFactory"

    .line 30
    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    .line 31
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/reader/internal/BootstrapFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 33
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static onCreate(Landroid/app/Application;)V
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->checkMainThread()V

    .line 18
    sget-object v0, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->appBootstrap:Lcom/squareup/sdk/reader/internal/AppBootstrap;

    if-nez v0, :cond_0

    .line 21
    invoke-static {p0}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->loadAppBootstrapFactory(Landroid/app/Application;)Lcom/squareup/sdk/reader/internal/BootstrapFactory;

    move-result-object v0

    .line 22
    invoke-interface {v0, p0}, Lcom/squareup/sdk/reader/internal/BootstrapFactory;->create(Landroid/app/Application;)Lcom/squareup/sdk/reader/internal/AppBootstrap;

    move-result-object p0

    sput-object p0, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->appBootstrap:Lcom/squareup/sdk/reader/internal/AppBootstrap;

    .line 23
    sget-object p0, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->appBootstrap:Lcom/squareup/sdk/reader/internal/AppBootstrap;

    invoke-interface {p0}, Lcom/squareup/sdk/reader/internal/AppBootstrap;->onCreate()V

    return-void

    .line 19
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Should not call ReaderSdk#initialize twice"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
