.class public final Lcom/squareup/sdk/reader/internal/InternalSdkHelper;
.super Ljava/lang/Object;
.source "InternalSdkHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static checkMainThread()V
    .locals 2

    .line 19
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    return-void

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Must be called from the main thread."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static currentCurrency()Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 29
    invoke-static {}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/SdkFactory;->currencyCodeMemoryCache()Lcom/squareup/sdk/reader/internal/CurrencyCodeMemoryCache;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/CurrencyCodeMemoryCache;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No authorized location. Reader SDK must be authorized in order to get the currency of the currently authorized location."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static format(Lcom/squareup/sdk/reader/checkout/Money;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 25
    invoke-static {}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/SdkFactory;->moneyFormatHelper()Lcom/squareup/sdk/reader/internal/MoneyFormatHelper;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/squareup/sdk/reader/internal/MoneyFormatHelper;->format(Lcom/squareup/sdk/reader/checkout/Money;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    if-eqz p0, :cond_0

    return-object p0

    .line 13
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must not be null"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
