.class public interface abstract Lcom/squareup/sdk/reader/internal/SdkFactory;
.super Ljava/lang/Object;
.source "SdkFactory.java"


# virtual methods
.method public abstract authorizationManager()Lcom/squareup/sdk/reader/authorization/AuthorizationManager;
.end method

.method public abstract checkoutManager()Lcom/squareup/sdk/reader/checkout/CheckoutManager;
.end method

.method public abstract currencyCodeMemoryCache()Lcom/squareup/sdk/reader/internal/CurrencyCodeMemoryCache;
.end method

.method public abstract customerCardManager()Lcom/squareup/sdk/reader/crm/CustomerCardManager;
.end method

.method public abstract moneyFormatHelper()Lcom/squareup/sdk/reader/internal/MoneyFormatHelper;
.end method

.method public abstract readerManager()Lcom/squareup/sdk/reader/hardware/ReaderManager;
.end method
