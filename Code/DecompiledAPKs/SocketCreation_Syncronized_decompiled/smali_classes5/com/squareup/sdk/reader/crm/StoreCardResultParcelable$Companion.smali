.class public final Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;
.super Ljava/lang/Object;
.source "StoreCardResultParcelable.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStoreCardResultParcelable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StoreCardResultParcelable.kt\ncom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion\n*L\n1#1,97:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u000c\u0010\u000f\u001a\u00020\t*\u00020\u0010H\u0002R\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;",
        "STORE_CARD_RESULT_EXTRA_KEY",
        "",
        "readStoredCardFromIntent",
        "Lcom/squareup/sdk/reader/checkout/Card;",
        "intent",
        "Landroid/content/Intent;",
        "writeStoredCardToIntent",
        "",
        "result",
        "readStoredCardFromParcel",
        "Landroid/os/Parcel;",
        "reader-sdk_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$readStoredCardFromParcel(Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable$Companion;->readStoredCardFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object p0

    return-object p0
.end method

.method private final readStoredCardFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 7

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "readString()!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-static {v0}, Lcom/squareup/sdk/reader/checkout/Card$Brand;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object v0

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v0, v2}, Lcom/squareup/sdk/reader/checkout/Card;->newCardBuilder(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    move-result-object v0

    const-string v2, "Card.newCardBuilder(brand, readString()!!)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    .line 79
    move-object v4, v2

    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-lez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_5

    .line 80
    invoke-virtual {v0, v2}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->id(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    .line 82
    :cond_5
    move-object v2, v3

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_6

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_7

    .line 83
    invoke-virtual {v0, v3}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->cardholderName(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    :cond_7
    if-lez v1, :cond_8

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationMonth(I)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    :cond_8
    if-ltz p1, :cond_9

    .line 91
    invoke-virtual {v0, p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationYear(I)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    .line 93
    :cond_9
    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->build()Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object p1

    const-string v0, "builder.build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final readStoredCardFromIntent(Landroid/content/Intent;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "com.squareup.sdk.reader.STORE_CARD_RESULT"

    .line 64
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;

    if-nez p1, :cond_0

    .line 67
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {p1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->access$getResult$p(Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;)Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object p1

    return-object p1
.end method

.method public final writeStoredCardToIntent(Lcom/squareup/sdk/reader/checkout/Card;Landroid/content/Intent;)V
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;-><init>(Lcom/squareup/sdk/reader/checkout/Card;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/os/Parcelable;

    const-string p1, "com.squareup.sdk.reader.STORE_CARD_RESULT"

    .line 56
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-void
.end method
