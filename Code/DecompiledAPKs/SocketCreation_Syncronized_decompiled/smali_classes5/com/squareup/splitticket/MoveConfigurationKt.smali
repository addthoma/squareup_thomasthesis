.class public final Lcom/squareup/splitticket/MoveConfigurationKt;
.super Ljava/lang/Object;
.source "MoveConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMoveConfiguration.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MoveConfiguration.kt\ncom/squareup/splitticket/MoveConfigurationKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,31:1\n1609#2,5:32\n1360#2:37\n1429#2,3:38\n704#2:41\n777#2,2:42\n1616#2:44\n*E\n*S KotlinDebug\n*F\n+ 1 MoveConfiguration.kt\ncom/squareup/splitticket/MoveConfigurationKt\n*L\n22#1,5:32\n22#1:37\n22#1,3:38\n22#1:41\n22#1,2:42\n22#1:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0002H\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "merge",
        "Lcom/squareup/splitticket/MoveConfiguration;",
        "",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final merge(Ljava/util/List;)Lcom/squareup/splitticket/MoveConfiguration;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/splitticket/MoveConfiguration;",
            ">;)",
            "Lcom/squareup/splitticket/MoveConfiguration;"
        }
    .end annotation

    const-string v0, "$this$merge"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/squareup/splitticket/MoveConfiguration;->Companion:Lcom/squareup/splitticket/MoveConfiguration$Companion;

    invoke-virtual {v0}, Lcom/squareup/splitticket/MoveConfiguration$Companion;->getEMPTY()Lcom/squareup/splitticket/MoveConfiguration;

    move-result-object v0

    .line 33
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 34
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object p0

    .line 35
    :goto_0
    invoke-interface {p0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    invoke-interface {p0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/splitticket/MoveConfiguration;

    .line 23
    invoke-virtual {v0}, Lcom/squareup/splitticket/MoveConfiguration;->getAssociatedCourses()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 37
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 38
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 39
    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 23
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 40
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 24
    invoke-virtual {v1}, Lcom/squareup/splitticket/MoveConfiguration;->getAssociatedCourses()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 41
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 42
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 24
    iget-object v6, v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 43
    :cond_2
    check-cast v4, Ljava/util/List;

    .line 25
    new-instance v2, Lcom/squareup/splitticket/MoveConfiguration;

    .line 26
    invoke-virtual {v0}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-virtual {v1}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    invoke-static {v3, v5}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    .line 27
    invoke-virtual {v0}, Lcom/squareup/splitticket/MoveConfiguration;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-virtual {v1}, Lcom/squareup/splitticket/MoveConfiguration;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v5, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 28
    invoke-virtual {v0}, Lcom/squareup/splitticket/MoveConfiguration;->getAssociatedCourses()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    check-cast v4, Ljava/lang/Iterable;

    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 25
    invoke-direct {v2, v3, v1, v0}, Lcom/squareup/splitticket/MoveConfiguration;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    move-object v0, v2

    goto/16 :goto_0

    :cond_3
    return-object v0
.end method
