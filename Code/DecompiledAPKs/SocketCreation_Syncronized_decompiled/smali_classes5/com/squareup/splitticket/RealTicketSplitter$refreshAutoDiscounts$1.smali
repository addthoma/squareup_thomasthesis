.class final Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;
.super Ljava/lang/Object;
.source "RealTicketSplitter.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/splitticket/RealTicketSplitter;->refreshAutoDiscounts(Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/prices/PricingEngineServiceResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTicketSplitter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTicketSplitter.kt\ncom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1\n*L\n1#1,695:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/prices/PricingEngineServiceResult;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $id:Ljava/lang/String;

.field final synthetic $order:Lcom/squareup/payment/Order;

.field final synthetic $state:Lcom/squareup/splitticket/RealSplitState;

.field final synthetic this$0:Lcom/squareup/splitticket/RealTicketSplitter;


# direct methods
.method constructor <init>(Lcom/squareup/splitticket/RealTicketSplitter;Lcom/squareup/splitticket/RealSplitState;Ljava/lang/String;Lcom/squareup/payment/Order;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    iput-object p2, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$state:Lcom/squareup/splitticket/RealSplitState;

    iput-object p3, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$id:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$order:Lcom/squareup/payment/Order;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/prices/PricingEngineServiceResult;)V
    .locals 9

    if-eqz p1, :cond_7

    .line 371
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$state:Lcom/squareup/splitticket/RealSplitState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 374
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/prices/PricingEngineServiceResult;->getCartDiscountsToRemove()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 375
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$order:Lcom/squareup/payment/Order;

    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/squareup/payment/Order;->manuallyRemoveDiscountFromAllItems(Ljava/lang/String;)Z

    goto :goto_0

    .line 377
    :cond_1
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$order:Lcom/squareup/payment/Order;

    .line 378
    invoke-virtual {p1}, Lcom/squareup/prices/PricingEngineServiceResult;->getCatalogDiscounts()Ljava/util/Map;

    move-result-object v4

    .line 379
    invoke-virtual {p1}, Lcom/squareup/prices/PricingEngineServiceResult;->getBlocks()Ljava/util/Map;

    move-result-object v5

    .line 380
    invoke-virtual {p1}, Lcom/squareup/prices/PricingEngineServiceResult;->getWholeBlocks()Ljava/util/List;

    move-result-object v6

    .line 381
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    invoke-static {v0}, Lcom/squareup/splitticket/RealTicketSplitter;->access$getFeatures$p(Lcom/squareup/splitticket/RealTicketSplitter;)Lcom/squareup/settings/server/Features;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    .line 382
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    invoke-static {v0}, Lcom/squareup/splitticket/RealTicketSplitter;->access$getFeatures$p(Lcom/squareup/splitticket/RealTicketSplitter;)Lcom/squareup/settings/server/Features;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v8

    .line 377
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/payment/Order;->applyPricingRuleBlocks(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZ)Lcom/squareup/payment/ApplyAutomaticDiscountsResult;

    move-result-object v0

    const-string v1, "order.applyPricingRuleBl\u2026_DISCOUNTS)\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Lcom/squareup/prices/PricingEngineServiceResult;->getCartDiscountsToRemove()Ljava/util/Set;

    move-result-object p1

    const-string v1, "result.cartDiscountsToRemove"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-nez p1, :cond_2

    invoke-virtual {v0}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->anyChanged()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 385
    :cond_2
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    invoke-virtual {p1}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStatesById$impl_release()Ljava/util/Map;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$id:Ljava/lang/String;

    new-instance v2, Lcom/squareup/splitticket/RealSplitState;

    .line 386
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$order:Lcom/squareup/payment/Order;

    invoke-static {v3}, Lcom/squareup/splitticket/OrdersKt;->explodeStackedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$state:Lcom/squareup/splitticket/RealSplitState;

    invoke-virtual {v4}, Lcom/squareup/splitticket/RealSplitState;->getOrdinal()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$state:Lcom/squareup/splitticket/RealSplitState;

    invoke-virtual {v5}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v5

    .line 385
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/splitticket/RealSplitState;-><init>(Lcom/squareup/payment/Order;II)V

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    invoke-static {p1}, Lcom/squareup/splitticket/RealTicketSplitter;->access$getOnAsyncStateChangeRelay$p(Lcom/squareup/splitticket/RealTicketSplitter;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->$state:Lcom/squareup/splitticket/RealSplitState;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 394
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getSplit()I

    move-result p1

    if-lez p1, :cond_7

    .line 395
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    invoke-virtual {p1}, Lcom/squareup/splitticket/RealTicketSplitter;->getFosterState$impl_release()Lcom/squareup/splitticket/FosterState;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 396
    invoke-interface {p1}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    const-string v2, "currentFosterState.order.items"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 397
    invoke-virtual {v0}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->getReplacements()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 398
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 399
    move-object v4, v1

    check-cast v4, Ljava/util/Collection;

    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 400
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    .line 399
    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 403
    :cond_5
    invoke-interface {p1}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 404
    check-cast v1, Ljava/util/Collection;

    invoke-virtual {p1, v1}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 405
    invoke-virtual {p1}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p1

    .line 406
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->this$0:Lcom/squareup/splitticket/RealTicketSplitter;

    new-instance v1, Lcom/squareup/splitticket/RealFosterState;

    const-string v2, "newFosterOrder"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/splitticket/OrdersKt;->withSortedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/splitticket/RealFosterState;-><init>(Lcom/squareup/payment/Order;)V

    check-cast v1, Lcom/squareup/splitticket/FosterState;

    invoke-virtual {v0, v1}, Lcom/squareup/splitticket/RealTicketSplitter;->setFosterState$impl_release(Lcom/squareup/splitticket/FosterState;)V

    goto :goto_2

    .line 395
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_7
    :goto_2
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/prices/PricingEngineServiceResult;

    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;->call(Lcom/squareup/prices/PricingEngineServiceResult;)V

    return-void
.end method
