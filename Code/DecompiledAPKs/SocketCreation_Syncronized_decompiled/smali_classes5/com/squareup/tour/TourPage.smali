.class public Lcom/squareup/tour/TourPage;
.super Ljava/lang/Object;
.source "TourPage.java"


# instance fields
.field public final imageGravityBottom:Z

.field public final imageResId:I

.field public final subtitleResId:I

.field public final titleResId:I


# direct methods
.method private constructor <init>(IZII)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/squareup/tour/TourPage;->imageResId:I

    .line 16
    iput-boolean p2, p0, Lcom/squareup/tour/TourPage;->imageGravityBottom:Z

    .line 17
    iput p3, p0, Lcom/squareup/tour/TourPage;->titleResId:I

    .line 18
    iput p4, p0, Lcom/squareup/tour/TourPage;->subtitleResId:I

    return-void
.end method

.method public static pageBottom(III)Lcom/squareup/tour/TourPage;
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/tour/TourPage;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/squareup/tour/TourPage;-><init>(IZII)V

    return-object v0
.end method

.method public static pageCenter(III)Lcom/squareup/tour/TourPage;
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/tour/TourPage;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/squareup/tour/TourPage;-><init>(IZII)V

    return-object v0
.end method
