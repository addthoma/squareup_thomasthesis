.class public final Lcom/squareup/tour/LegacyWhatsNewTourProvider;
.super Ljava/lang/Object;
.source "WhatsNewTourProvider.kt"

# interfaces
.implements Lcom/squareup/tour/WhatsNewTourProvider;


# annotations
.annotation runtime Lkotlin/Deprecated;
    message = "Create or add to an existing vertical specific subclass of [WhatsNewTourProvider]"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\u0008X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00128VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/tour/LegacyWhatsNewTourProvider;",
        "Lcom/squareup/tour/WhatsNewTourProvider;",
        "locale",
        "Ljava/util/Locale;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)V",
        "displayAfterOnboarding",
        "",
        "getDisplayAfterOnboarding",
        "()Z",
        "setDisplayAfterOnboarding",
        "(Z)V",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "getLocale",
        "()Ljava/util/Locale;",
        "tourPages",
        "",
        "Lcom/squareup/tour/SerializableHasTourPages;",
        "getTourPages",
        "()Ljava/util/List;",
        "pos-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private displayAfterOnboarding:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tour/LegacyWhatsNewTourProvider;->locale:Ljava/util/Locale;

    iput-object p2, p0, Lcom/squareup/tour/LegacyWhatsNewTourProvider;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getDisplayAfterOnboarding()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/tour/LegacyWhatsNewTourProvider;->displayAfterOnboarding:Z

    return v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/tour/LegacyWhatsNewTourProvider;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/tour/LegacyWhatsNewTourProvider;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getTourPages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayAfterOnboarding(Z)V
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/squareup/tour/LegacyWhatsNewTourProvider;->displayAfterOnboarding:Z

    return-void
.end method
