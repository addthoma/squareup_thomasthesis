.class public Lcom/squareup/tour/WhatsNewTourView;
.super Landroid/widget/RelativeLayout;
.source "WhatsNewTourView.java"


# instance fields
.field private final adapter:Lcom/squareup/tour/TourAdapter;

.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private nextButton:Landroid/widget/Button;

.field private pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

.field private pager:Landroidx/viewpager/widget/ViewPager;

.field presenter:Lcom/squareup/tour/WhatsNewTourScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const-class p2, Lcom/squareup/tour/WhatsNewTourScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/tour/WhatsNewTourScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/tour/WhatsNewTourScreen$Component;->inject(Lcom/squareup/tour/WhatsNewTourView;)V

    .line 36
    new-instance p2, Lcom/squareup/tour/TourAdapter;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    invoke-direct {p2, p1, v0}, Lcom/squareup/tour/TourAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p2, p0, Lcom/squareup/tour/WhatsNewTourView;->adapter:Lcom/squareup/tour/TourAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/tour/WhatsNewTourView;)Lcom/squareup/tour/TourAdapter;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/tour/WhatsNewTourView;->adapter:Lcom/squareup/tour/TourAdapter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/tour/WhatsNewTourView;)Landroidx/viewpager/widget/ViewPager;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/tour/WhatsNewTourView;)Landroid/widget/Button;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/tour/WhatsNewTourView;->nextButton:Landroid/widget/Button;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 96
    sget v0, Lcom/squareup/tour/R$id;->tour_pager:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    .line 97
    sget v0, Lcom/squareup/tour/R$id;->tour_next_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->nextButton:Landroid/widget/Button;

    .line 98
    sget v0, Lcom/squareup/tour/R$id;->tour_close_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 99
    sget v0, Lcom/squareup/tour/R$id;->page_indicator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinPageIndicator;

    iput-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$WhatsNewTourView(Landroid/view/View;F)V
    .locals 2

    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 60
    sget v1, Lcom/squareup/tour/common/R$id;->tour_image:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_1

    int-to-float v0, v0

    mul-float v0, v0, p2

    .line 64
    iget-object p2, p0, Lcom/squareup/tour/WhatsNewTourView;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p2

    if-eqz p2, :cond_0

    const p2, 0x3f4ccccd    # 0.8f

    mul-float v0, v0, p2

    .line 69
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->presenter:Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 86
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 40
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 41
    invoke-direct {p0}, Lcom/squareup/tour/WhatsNewTourView;->bindViews()V

    .line 43
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewTourView;->adapter:Lcom/squareup/tour/TourAdapter;

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewTourView;->presenter:Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->nextButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/tour/WhatsNewTourView$1;

    invoke-direct {v1, p0}, Lcom/squareup/tour/WhatsNewTourView$1;-><init>(Lcom/squareup/tour/WhatsNewTourView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/tour/WhatsNewTourView$2;

    invoke-direct {v1, p0}, Lcom/squareup/tour/WhatsNewTourView$2;-><init>(Lcom/squareup/tour/WhatsNewTourView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    new-instance v1, Lcom/squareup/tour/-$$Lambda$WhatsNewTourView$4E3tL7KTdKE_Gxb9-KeKXVDZtfY;

    invoke-direct {v1, p0}, Lcom/squareup/tour/-$$Lambda$WhatsNewTourView$4E3tL7KTdKE_Gxb9-KeKXVDZtfY;-><init>(Lcom/squareup/tour/WhatsNewTourView;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    new-instance v1, Lcom/squareup/tour/WhatsNewTourView$3;

    invoke-direct {v1, p0}, Lcom/squareup/tour/WhatsNewTourView$3;-><init>(Lcom/squareup/tour/WhatsNewTourView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->presenter:Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;)V"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->adapter:Lcom/squareup/tour/TourAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/tour/TourAdapter;->updatePages(Ljava/util/List;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/tour/WhatsNewTourView;->nextButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewTourView;->adapter:Lcom/squareup/tour/TourAdapter;

    iget-object v1, v1, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 92
    iget-object p1, p0, Lcom/squareup/tour/WhatsNewTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView;->adapter:Lcom/squareup/tour/TourAdapter;

    iget-object v0, v0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
