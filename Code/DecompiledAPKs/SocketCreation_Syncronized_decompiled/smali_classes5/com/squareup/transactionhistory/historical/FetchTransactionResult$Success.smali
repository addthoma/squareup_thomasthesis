.class public final Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;
.super Lcom/squareup/transactionhistory/historical/FetchTransactionResult;
.source "HistoricalTransactionsStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transactionhistory/historical/FetchTransactionResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Success"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;",
        "Lcom/squareup/transactionhistory/historical/FetchTransactionResult;",
        "transaction",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
        "(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)V",
        "getTransaction",
        "()Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final transaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;


# direct methods
.method public constructor <init>(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)V
    .locals 1

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, v0}, Lcom/squareup/transactionhistory/historical/FetchTransactionResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;->transaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    return-void
.end method


# virtual methods
.method public final getTransaction()Lcom/squareup/transactionhistory/historical/HistoricalTransaction;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;->transaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    return-object v0
.end method
