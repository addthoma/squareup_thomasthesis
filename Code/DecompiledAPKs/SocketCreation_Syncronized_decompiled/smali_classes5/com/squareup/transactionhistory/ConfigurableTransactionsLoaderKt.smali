.class public final Lcom/squareup/transactionhistory/ConfigurableTransactionsLoaderKt;
.super Ljava/lang/Object;
.source "ConfigurableTransactionsLoader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "UNFILTERED_CONFIGURATION",
        "Lcom/squareup/transactionhistory/Configuration;",
        "getUNFILTERED_CONFIGURATION",
        "()Lcom/squareup/transactionhistory/Configuration;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final UNFILTERED_CONFIGURATION:Lcom/squareup/transactionhistory/Configuration;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 150
    new-instance v6, Lcom/squareup/transactionhistory/Configuration;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/transactionhistory/Configuration;-><init>(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v6, Lcom/squareup/transactionhistory/ConfigurableTransactionsLoaderKt;->UNFILTERED_CONFIGURATION:Lcom/squareup/transactionhistory/Configuration;

    return-void
.end method

.method public static final getUNFILTERED_CONFIGURATION()Lcom/squareup/transactionhistory/Configuration;
    .locals 1

    .line 150
    sget-object v0, Lcom/squareup/transactionhistory/ConfigurableTransactionsLoaderKt;->UNFILTERED_CONFIGURATION:Lcom/squareup/transactionhistory/Configuration;

    return-object v0
.end method
