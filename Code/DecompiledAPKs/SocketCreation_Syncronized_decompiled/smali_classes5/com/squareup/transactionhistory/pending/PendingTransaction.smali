.class public final Lcom/squareup/transactionhistory/pending/PendingTransaction;
.super Ljava/lang/Object;
.source "PendingTransaction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        "",
        "billHistory",
        "(Ljava/lang/Object;)V",
        "getBillHistory",
        "()Ljava/lang/Object;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billHistory:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    const-string v0, "billHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transactionhistory/pending/PendingTransaction;->billHistory:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getBillHistory()Ljava/lang/Object;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/transactionhistory/pending/PendingTransaction;->billHistory:Ljava/lang/Object;

    return-object v0
.end method
