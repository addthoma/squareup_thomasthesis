.class public final Lcom/squareup/transactionhistory/processed/TransactionSearchMatchKt;
.super Ljava/lang/Object;
.source "TransactionSearchMatch.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "EMPTY_SEARCH_MATCH",
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "getEMPTY_SEARCH_MATCH",
        "()Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EMPTY_SEARCH_MATCH:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 62
    sget-object v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->Companion:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2, v1, v1}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;->newTransactionSearchMatch(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object v0

    sput-object v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatchKt;->EMPTY_SEARCH_MATCH:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    return-void
.end method

.method public static final getEMPTY_SEARCH_MATCH()Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatchKt;->EMPTY_SEARCH_MATCH:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    return-object v0
.end method
