.class public final Lcom/squareup/transactionhistory/mappings/processed/ProcessedTransactionMappersKt;
.super Ljava/lang/Object;
.source "ProcessedTransactionMappers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nProcessedTransactionMappers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ProcessedTransactionMappers.kt\ncom/squareup/transactionhistory/mappings/processed/ProcessedTransactionMappersKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,76:1\n1360#2:77\n1429#2,3:78\n1360#2:81\n1429#2,3:82\n*E\n*S KotlinDebug\n*F\n+ 1 ProcessedTransactionMappers.kt\ncom/squareup/transactionhistory/mappings/processed/ProcessedTransactionMappersKt\n*L\n65#1:77\n65#1,3:78\n68#1:81\n68#1,3:82\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u001a\u0018\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n*\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e\u001a\u0012\u0010\u000f\u001a\u00020\u000b*\u00020\u00102\u0006\u0010\r\u001a\u00020\u000e\u001a\u000c\u0010\u0011\u001a\u00020\u0012*\u00020\u0013H\u0000\u001a\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00120\n*\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\nH\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "toProcessedTransaction",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransaction;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "res",
        "Lcom/squareup/util/Res;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "toProcessedTransactionSummaries",
        "",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
        "locale",
        "Ljava/util/Locale;",
        "toProcessedTransactionSummary",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
        "toSearchMatch",
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
        "toSearchMatches",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toProcessedTransaction(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)Lcom/squareup/transactionhistory/processed/ProcessedTransaction;
    .locals 1

    const-string v0, "$this$toProcessedTransaction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    const-string v0, "bill_family"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    .line 38
    invoke-virtual {p3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result p3

    .line 39
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    .line 35
    invoke-static {p0, p2, p3, p1}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p0

    .line 41
    new-instance p1, Lcom/squareup/transactionhistory/processed/ProcessedTransaction;

    const-string p2, "billHistory"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransaction;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public static final toProcessedTransactionSummaries(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;Ljava/util/Locale;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toProcessedTransactionSummaries"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object p0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    const-string/jumbo v0, "transaction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 78
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 79
    check-cast v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    const-string v2, "it"

    .line 65
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/transactionhistory/mappings/processed/ProcessedTransactionMappersKt;->toProcessedTransactionSummary(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;Ljava/util/Locale;)Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final toProcessedTransactionSummary(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;Ljava/util/Locale;)Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "$this$toProcessedTransactionSummary"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "locale"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v3, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->Companion:Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;

    .line 47
    iget-object v2, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 48
    iget-object v2, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 49
    iget-object v2, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v6

    const-string v1, "asDate(date, locale)"

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const-string/jumbo v2, "type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMappingKt;->toTransactionType(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;)Lcom/squareup/transactionhistory/TransactionType;

    move-result-object v7

    .line 51
    iget-object v8, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    const-string v1, "amount"

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v9, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    .line 53
    iget-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/TenderInfoMappingKt;->toTenderInfo(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 54
    iget-object v12, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    .line 55
    iget-object v13, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    .line 56
    iget-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    const/4 v2, 0x0

    const/4 v10, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 57
    iget-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    const-string/jumbo v14, "tender_information"

    invoke-static {v1, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    iget-object v1, v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    sget-object v14, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    if-ne v1, v14, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 58
    iget-object v15, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    .line 59
    iget-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    move-object/from16 v16, v1

    .line 60
    iget-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    move-object/from16 v17, v1

    .line 61
    iget-object v0, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/transactionhistory/mappings/processed/ProcessedTransactionMappersKt;->toSearchMatches(Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    const/16 v19, 0x40

    const/16 v20, 0x0

    const/4 v10, 0x0

    .line 46
    invoke-static/range {v3 .. v20}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;->newProcessedTransactionSummary$default(Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    move-result-object v0

    return-object v0
.end method

.method public static final toSearchMatch(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
    .locals 3

    const-string v0, "$this$toSearchMatch"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->Companion:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    .line 73
    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    .line 74
    iget-object p0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    .line 71
    invoke-virtual {v0, v1, v2, p0}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;->newTransactionSearchMatch(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object p0

    return-object p0
.end method

.method public static final toSearchMatches(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 68
    check-cast p0, Ljava/lang/Iterable;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 82
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 83
    check-cast v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    .line 68
    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/processed/ProcessedTransactionMappersKt;->toSearchMatch(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    .line 68
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0
.end method
