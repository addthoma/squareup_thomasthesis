.class public final enum Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;
.super Ljava/lang/Enum;
.source "TenderTypeMapping.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;",
        "",
        "protoTenderType",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        "billsTenderType",
        "Lcom/squareup/billhistory/model/TenderHistory$Type;",
        "transactionHistoryTenderType",
        "Lcom/squareup/transactionhistory/TenderType;",
        "(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V",
        "getBillsTenderType$public_release",
        "()Lcom/squareup/billhistory/model/TenderHistory$Type;",
        "getProtoTenderType$public_release",
        "()Lcom/squareup/protos/client/bills/Tender$Type;",
        "getTransactionHistoryTenderType$public_release",
        "()Lcom/squareup/transactionhistory/TenderType;",
        "UNKNOWN",
        "CARD",
        "CASH",
        "NO_SALE",
        "OTHER",
        "WALLET",
        "GENERIC",
        "ZERO_AMOUNT",
        "EXTERNAL",
        "ADJUSTMENT",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum ADJUSTMENT:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum CARD:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum CASH:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum EXTERNAL:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum GENERIC:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum NO_SALE:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum OTHER:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum UNKNOWN:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum WALLET:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

.field public static final enum ZERO_AMOUNT:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;


# instance fields
.field private final billsTenderType:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field private final protoTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

.field private final transactionHistoryTenderType:Lcom/squareup/transactionhistory/TenderType;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    new-instance v7, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 20
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 21
    sget-object v5, Lcom/squareup/billhistory/model/TenderHistory$Type;->UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 22
    sget-object v6, Lcom/squareup/transactionhistory/TenderType;->UNKNOWN:Lcom/squareup/transactionhistory/TenderType;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    move-object v1, v7

    .line 19
    invoke-direct/range {v1 .. v6}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v7, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->UNKNOWN:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 25
    sget-object v11, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 26
    sget-object v12, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 27
    sget-object v13, Lcom/squareup/transactionhistory/TenderType;->CARD:Lcom/squareup/transactionhistory/TenderType;

    const-string v9, "CARD"

    const/4 v10, 0x1

    move-object v8, v1

    .line 24
    invoke-direct/range {v8 .. v13}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->CARD:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 30
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 31
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 32
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->CASH:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "CASH"

    const/4 v5, 0x2

    move-object v3, v1

    .line 29
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->CASH:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 35
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 36
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->NO_SALE:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 37
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->NO_SALE:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "NO_SALE"

    const/4 v5, 0x3

    move-object v3, v1

    .line 34
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->NO_SALE:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 40
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 41
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->OTHER:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 42
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->OTHER:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "OTHER"

    const/4 v5, 0x4

    move-object v3, v1

    .line 39
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->OTHER:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 45
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->WALLET:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 47
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->TAB:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 48
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->WALLET:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "WALLET"

    const/4 v5, 0x5

    move-object v3, v1

    .line 44
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->WALLET:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 51
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->GENERIC:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 53
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 54
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->GENERIC:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "GENERIC"

    const/4 v5, 0x6

    move-object v3, v1

    .line 50
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->GENERIC:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 57
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->ZERO_AMOUNT:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 58
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->ZERO_AMOUNT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 59
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->ZERO_AMOUNT:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "ZERO_AMOUNT"

    const/4 v5, 0x7

    move-object v3, v1

    .line 56
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->ZERO_AMOUNT:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 62
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->EXTERNAL:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 64
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 65
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->EXTERNAL:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "EXTERNAL"

    const/16 v5, 0x8

    move-object v3, v1

    .line 61
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->EXTERNAL:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    .line 69
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 70
    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->ADJUSTMENT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 72
    sget-object v8, Lcom/squareup/transactionhistory/TenderType;->UNKNOWN:Lcom/squareup/transactionhistory/TenderType;

    const-string v4, "ADJUSTMENT"

    const/16 v5, 0x9

    move-object v3, v1

    .line 67
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->ADJUSTMENT:Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/transactionhistory/TenderType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Tender$Type;",
            "Lcom/squareup/billhistory/model/TenderHistory$Type;",
            "Lcom/squareup/transactionhistory/TenderType;",
            ")V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->protoTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object p4, p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->billsTenderType:Lcom/squareup/billhistory/model/TenderHistory$Type;

    iput-object p5, p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->transactionHistoryTenderType:Lcom/squareup/transactionhistory/TenderType;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    return-object v0
.end method


# virtual methods
.method public final getBillsTenderType$public_release()Lcom/squareup/billhistory/model/TenderHistory$Type;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->billsTenderType:Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-object v0
.end method

.method public final getProtoTenderType$public_release()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->protoTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method

.method public final getTransactionHistoryTenderType$public_release()Lcom/squareup/transactionhistory/TenderType;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->transactionHistoryTenderType:Lcom/squareup/transactionhistory/TenderType;

    return-object v0
.end method
