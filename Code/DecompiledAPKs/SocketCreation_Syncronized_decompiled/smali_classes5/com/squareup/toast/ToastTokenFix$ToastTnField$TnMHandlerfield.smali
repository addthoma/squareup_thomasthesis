.class public final Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;
.super Lcom/squareup/toast/ToastTokenFix$ToastTnField;
.source "ToastTokenFix.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/toast/ToastTokenFix$ToastTnField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TnMHandlerfield"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;",
        "Lcom/squareup/toast/ToastTokenFix$ToastTnField;",
        "field",
        "Ljava/lang/reflect/Field;",
        "(Ljava/lang/reflect/Field;)V",
        "getField",
        "()Ljava/lang/reflect/Field;",
        "swallowBadTokenException",
        "",
        "tn",
        "",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final field:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;)V
    .locals 1

    const-string v0, "field"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 175
    invoke-direct {p0, v0}, Lcom/squareup/toast/ToastTokenFix$ToastTnField;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;->field:Ljava/lang/reflect/Field;

    return-void
.end method


# virtual methods
.method public getField()Ljava/lang/reflect/Field;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;->field:Ljava/lang/reflect/Field;

    return-object v0
.end method

.method public swallowBadTokenException(Ljava/lang/Object;)V
    .locals 2

    .line 177
    invoke-virtual {p0}, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;->getField()Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 181
    new-instance v1, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield$swallowBadTokenException$catchingHandler$1;

    invoke-direct {v1, v0}, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield$swallowBadTokenException$catchingHandler$1;-><init>(Landroid/os/Handler;)V

    .line 190
    invoke-virtual {p0}, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;->getField()Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
