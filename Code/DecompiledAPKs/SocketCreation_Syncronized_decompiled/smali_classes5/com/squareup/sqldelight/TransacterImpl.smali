.class public abstract Lcom/squareup/sqldelight/TransacterImpl;
.super Ljava/lang/Object;
.source "Transacter.kt"

# interfaces
.implements Lcom/squareup/sqldelight/Transacter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransacter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Transacter.kt\ncom/squareup/sqldelight/TransacterImpl\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,203:1\n1591#2,2:204\n1591#2,2:206\n1591#2,2:220\n1591#2,2:222\n1591#2,2:224\n1591#2,2:238\n1591#2,2:240\n1591#2,2:242\n1591#2,2:256\n1591#2,2:258\n1591#2,2:260\n1591#2,2:274\n1591#2,2:276\n44#3,12:208\n44#3,12:226\n44#3,12:244\n44#3,12:262\n*E\n*S KotlinDebug\n*F\n+ 1 Transacter.kt\ncom/squareup/sqldelight/TransacterImpl\n*L\n113#1,2:204\n173#1,2:206\n185#1,2:220\n188#1,2:222\n173#1,2:224\n185#1,2:238\n188#1,2:240\n173#1,2:242\n185#1,2:256\n188#1,2:258\n173#1,2:260\n185#1,2:274\n188#1,2:276\n183#1,12:208\n183#1,12:226\n183#1,12:244\n183#1,12:262\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H\u0004J(\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00082\u0016\u0010\r\u001a\u0012\u0012\u000e\u0012\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00100\u000f0\u000eH\u0004J)\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u00132\u0017\u0010\u0014\u001a\u0013\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u000b0\u0015\u00a2\u0006\u0002\u0008\u0017H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/sqldelight/TransacterImpl;",
        "Lcom/squareup/sqldelight/Transacter;",
        "driver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "(Lcom/squareup/sqldelight/db/SqlDriver;)V",
        "createArguments",
        "",
        "count",
        "",
        "offset",
        "notifyQueries",
        "",
        "identifier",
        "queryList",
        "Lkotlin/Function0;",
        "",
        "Lcom/squareup/sqldelight/Query;",
        "transaction",
        "noEnclosing",
        "",
        "body",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "Lkotlin/ExtensionFunctionType;",
        "runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final driver:Lcom/squareup/sqldelight/db/SqlDriver;


# direct methods
.method public constructor <init>(Lcom/squareup/sqldelight/db/SqlDriver;)V
    .locals 1

    const-string v0, "driver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqldelight/TransacterImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    return-void
.end method


# virtual methods
.method protected final createArguments(II)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, "()"

    return-object p1

    .line 127
    :cond_0
    invoke-static {p1, p2}, Lcom/squareup/sqldelight/internal/MathKt;->presizeArguments(II)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "(?"

    .line 128
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, p2, 0x1

    add-int/2addr p2, p1

    :goto_0
    if-ge v0, p2, :cond_1

    const-string p1, ",?"

    .line 131
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/16 p1, 0x29

    .line 134
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "StringBuilder(capacity).\u2026builderAction).toString()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected final notifyQueries(ILkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/sqldelight/Query<",
            "*>;>;>;)V"
        }
    .end annotation

    const-string v0, "queryList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/sqldelight/TransacterImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    invoke-interface {v0}, Lcom/squareup/sqldelight/db/SqlDriver;->currentTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 110
    invoke-virtual {v0}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Lcom/squareup/sqldelight/internal/FunctionsJvmKt;->threadLocalRef(Ljava/lang/Object;)Lkotlin/jvm/functions/Function0;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 113
    :cond_0
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 204
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/sqldelight/Query;

    .line 113
    invoke-virtual {p2}, Lcom/squareup/sqldelight/Query;->notifyDataChanged()V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public transaction(ZLkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/Transacter$Transaction;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "\n\nRollback exception: "

    const-string v1, "\nwith cause "

    const-string v2, "Exception while rolling back from an exception.\nOriginal exception: "

    const-string v3, "body"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    iget-object v3, p0, Lcom/squareup/sqldelight/TransacterImpl;->driver:Lcom/squareup/sqldelight/db/SqlDriver;

    invoke-interface {v3}, Lcom/squareup/sqldelight/db/SqlDriver;->newTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v3

    .line 150
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->enclosingTransaction$runtime()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v4

    if-eqz v4, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 153
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Already in a transaction"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 156
    check-cast p1, Ljava/lang/Throwable;

    const/4 p1, 0x0

    const/4 v5, 0x1

    .line 159
    :try_start_0
    move-object v6, p0

    check-cast v6, Lcom/squareup/sqldelight/Transacter;

    invoke-virtual {v3, v6}, Lcom/squareup/sqldelight/Transacter$Transaction;->setTransacter$runtime(Lcom/squareup/sqldelight/Transacter;)V

    .line 160
    invoke-interface {p2, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    invoke-virtual {v3, v5}, Lcom/squareup/sqldelight/Transacter$Transaction;->setSuccessful$runtime(Z)V
    :try_end_0
    .catch Lcom/squareup/sqldelight/RollbackException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 168
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->endTransaction$runtime()V

    if-nez v4, :cond_8

    .line 170
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result p1

    if-nez p1, :cond_2

    goto/16 :goto_4

    .line 182
    :cond_2
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    .line 208
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 215
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 216
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 183
    invoke-static {v0}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 217
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 219
    :cond_3
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 184
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->distinct(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 220
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/sqldelight/Query;

    .line 185
    invoke-virtual {p2}, Lcom/squareup/sqldelight/Query;->notifyDataChanged()V

    goto :goto_2

    .line 187
    :cond_4
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 188
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 222
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 188
    invoke-static {p2}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    goto :goto_3

    .line 189
    :cond_5
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto/16 :goto_12

    .line 173
    :cond_6
    :goto_4
    :try_start_1
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 206
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 173
    invoke-static {p2}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 180
    :cond_7
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto/16 :goto_12

    :catchall_0
    move-exception p1

    .line 178
    throw p1

    .line 192
    :cond_8
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result p2

    if-eqz p2, :cond_9

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result p2

    if-eqz p2, :cond_9

    const/4 p1, 0x1

    :cond_9
    invoke-virtual {v4, p1}, Lcom/squareup/sqldelight/Transacter$Transaction;->setChildrenSuccessful$runtime(Z)V

    .line 193
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p1, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p1, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 195
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto/16 :goto_12

    :catchall_1
    move-exception p2

    .line 168
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->endTransaction$runtime()V

    if-nez v4, :cond_10

    .line 170
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result p1

    if-eqz p1, :cond_e

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result p1

    if-nez p1, :cond_a

    goto/16 :goto_9

    .line 182
    :cond_a
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 251
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 252
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 183
    invoke-static {v1}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 253
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_6

    .line 255
    :cond_b
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 184
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->distinct(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 256
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/Query;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/sqldelight/Query;->notifyDataChanged()V

    goto :goto_7

    .line 187
    :cond_c
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 188
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 258
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 188
    invoke-static {v0}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    goto :goto_8

    .line 189
    :cond_d
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto/16 :goto_b

    .line 173
    :cond_e
    :goto_9
    :try_start_2
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 242
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_a
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 173
    invoke-static {v4}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_a

    .line 180
    :cond_f
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto :goto_b

    :catchall_2
    move-exception p1

    .line 176
    new-instance v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v3, p2, p1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 192
    :cond_10
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 p1, 0x1

    :cond_11
    invoke-virtual {v4, p1}, Lcom/squareup/sqldelight/Transacter$Transaction;->setChildrenSuccessful$runtime(Z)V

    .line 193
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 195
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 198
    :goto_b
    instance-of p1, p2, Lcom/squareup/sqldelight/RollbackException;

    if-eqz p1, :cond_12

    goto/16 :goto_12

    .line 199
    :cond_12
    throw p2

    :catch_0
    move-exception p2

    if-nez v4, :cond_1a

    .line 164
    :try_start_3
    check-cast p2, Ljava/lang/Throwable;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 168
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->endTransaction$runtime()V

    .line 170
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result p1

    if-eqz p1, :cond_17

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result p1

    if-nez p1, :cond_13

    goto/16 :goto_f

    .line 182
    :cond_13
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 233
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 234
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 183
    invoke-static {v1}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 235
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_c

    .line 237
    :cond_14
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 184
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->distinct(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 238
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_d
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/Query;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/sqldelight/Query;->notifyDataChanged()V

    goto :goto_d

    .line 187
    :cond_15
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 188
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 240
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_e
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 188
    invoke-static {v0}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    goto :goto_e

    .line 189
    :cond_16
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto :goto_11

    .line 173
    :cond_17
    :goto_f
    :try_start_4
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 224
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_10
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 173
    invoke-static {v4}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_10

    .line 180
    :cond_18
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    .line 198
    :goto_11
    instance-of p1, p2, Lcom/squareup/sqldelight/RollbackException;

    if-eqz p1, :cond_19

    :goto_12
    return-void

    .line 199
    :cond_19
    throw p2

    :catchall_3
    move-exception p1

    .line 176
    new-instance v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v3, p2, p1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :catchall_4
    move-exception p2

    goto :goto_13

    .line 163
    :cond_1a
    :try_start_5
    check-cast p2, Ljava/lang/Throwable;

    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 168
    :goto_13
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->endTransaction$runtime()V

    if-nez v4, :cond_21

    .line 170
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result p1

    if-eqz p1, :cond_1f

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result p1

    if-nez p1, :cond_1b

    goto/16 :goto_17

    .line 182
    :cond_1b
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    .line 262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 269
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_14
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 270
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 183
    invoke-static {v1}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 271
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_14

    .line 273
    :cond_1c
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 184
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->distinct(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 274
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_15
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/Query;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/sqldelight/Query;->notifyDataChanged()V

    goto :goto_15

    .line 187
    :cond_1d
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 188
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 276
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_16
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 188
    invoke-static {v0}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    goto :goto_16

    .line 189
    :cond_1e
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto :goto_19

    .line 173
    :cond_1f
    :goto_17
    :try_start_6
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 260
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_18
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 173
    invoke-static {v0}, Lcom/squareup/sqldelight/TransacterKt;->access$run(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    goto :goto_18

    .line 180
    :cond_20
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto :goto_19

    :catchall_5
    move-exception p1

    .line 178
    throw p1

    .line 192
    :cond_21
    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getSuccessful$runtime()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getChildrenSuccessful$runtime()Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 p1, 0x1

    :cond_22
    invoke-virtual {v4, p1}, Lcom/squareup/sqldelight/Transacter$Transaction;->setChildrenSuccessful$runtime(Z)V

    .line 193
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostCommitHooks$runtime()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 194
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getPostRollbackHooks$runtime()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 195
    invoke-virtual {v4}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v3}, Lcom/squareup/sqldelight/Transacter$Transaction;->getQueriesFuncs$runtime()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 199
    :goto_19
    throw p2
.end method
