.class public final Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;
.super Ljava/lang/Object;
.source "EnumColumnAdapter.java"

# interfaces
.implements Lcom/squareup/sqldelight/prerelease/ColumnAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Enum<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lcom/squareup/sqldelight/prerelease/ColumnAdapter<",
        "TT;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final cls:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;->cls:Ljava/lang/Class;

    return-void
.end method

.method public static create(Ljava/lang/Class;)Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter<",
            "TT;>;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 24
    new-instance v0, Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;-><init>(Ljava/lang/Class;)V

    return-object v0

    .line 23
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "cls == null"

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public decode(Ljava/lang/String;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;->cls:Ljava/lang/Class;

    invoke-static {v0, p1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;->decode(Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Enum;

    invoke-virtual {p0, p1}, Lcom/squareup/sqldelight/prerelease/EnumColumnAdapter;->encode(Ljava/lang/Enum;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encode(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 38
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
