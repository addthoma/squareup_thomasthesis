.class public final Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;
.super Ljava/lang/Object;
.source "LogSqliteDriver.kt"

# interfaces
.implements Lcom/squareup/sqldelight/db/SqlPreparedStatement;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLogSqliteDriver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LogSqliteDriver.kt\ncom/squareup/sqldelight/logs/StatementParameterInterceptor\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0006\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016J\u001f\u0010\u000c\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\rH\u0016\u00a2\u0006\u0002\u0010\u000eJ\u001f\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011J\u001a\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u0013H\u0016J\u000e\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0015R\u0016\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "()V",
        "values",
        "",
        "",
        "bindBytes",
        "",
        "index",
        "",
        "value",
        "",
        "bindDouble",
        "",
        "(ILjava/lang/Double;)V",
        "bindLong",
        "",
        "(ILjava/lang/Long;)V",
        "bindString",
        "",
        "getAndClearParameters",
        "",
        "runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bindBytes(I[B)V
    .locals 0

    .line 85
    iget-object p1, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public bindDouble(ILjava/lang/Double;)V
    .locals 0

    .line 92
    iget-object p1, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public bindLong(ILjava/lang/Long;)V
    .locals 0

    .line 99
    iget-object p1, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public bindString(ILjava/lang/String;)V
    .locals 0

    .line 106
    iget-object p1, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final getAndClearParameters()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/squareup/sqldelight/logs/StatementParameterInterceptor;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    return-object v0
.end method
