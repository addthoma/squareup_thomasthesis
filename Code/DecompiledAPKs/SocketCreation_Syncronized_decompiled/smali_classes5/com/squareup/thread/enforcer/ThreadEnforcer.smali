.class public interface abstract Lcom/squareup/thread/enforcer/ThreadEnforcer;
.super Ljava/lang/Object;
.source "ThreadEnforcer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;,
        Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008f\u0018\u0000 \n2\u00020\u0001:\u0001\nJ\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0002\u0010\u0004\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "",
        "isTargetThread",
        "",
        "()Z",
        "confine",
        "",
        "message",
        "",
        "forbid",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;->$$INSTANCE:Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;

    sput-object v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;->Companion:Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;

    return-void
.end method


# virtual methods
.method public abstract confine()V
.end method

.method public abstract confine(Ljava/lang/String;)V
.end method

.method public abstract forbid()V
.end method

.method public abstract forbid(Ljava/lang/String;)V
.end method

.method public abstract isTargetThread()Z
.end method
