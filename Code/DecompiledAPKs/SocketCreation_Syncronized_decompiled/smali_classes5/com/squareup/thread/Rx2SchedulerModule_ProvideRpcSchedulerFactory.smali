.class public final Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;
.super Ljava/lang/Object;
.source "Rx2SchedulerModule_ProvideRpcSchedulerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lio/reactivex/Scheduler;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory$InstanceHolder;->access$000()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideRpcScheduler()Lio/reactivex/Scheduler;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/thread/Rx2SchedulerModule;->INSTANCE:Lcom/squareup/thread/Rx2SchedulerModule;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx2SchedulerModule;->provideRpcScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    return-object v0
.end method


# virtual methods
.method public get()Lio/reactivex/Scheduler;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->provideRpcScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->get()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method
