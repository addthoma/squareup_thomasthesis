.class public interface abstract Lcom/squareup/thread/executor/SerialExecutor;
.super Ljava/lang/Object;
.source "SerialExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# virtual methods
.method public abstract cancel(Ljava/lang/Runnable;)V
.end method

.method public abstract execute(Ljava/lang/Runnable;)V
.end method

.method public abstract executeDelayed(Ljava/lang/Runnable;J)Z
.end method

.method public abstract post(Ljava/lang/Runnable;)V
.end method
