.class public final Lcom/squareup/thread/Rx2SchedulerModule;
.super Ljava/lang/Object;
.source "Rx2SchedulerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0001J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0001J\u0008\u0010\u0008\u001a\u00020\u0004H\u0001J\u0008\u0010\t\u001a\u00020\u0004H\u0001J\u0008\u0010\n\u001a\u00020\u0004H\u0001\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/thread/Rx2SchedulerModule;",
        "",
        "()V",
        "provideComputationScheduler",
        "Lio/reactivex/Scheduler;",
        "provideFileThreadScheduler",
        "scheduler",
        "Lcom/squareup/thread/Rx2FileScheduler;",
        "provideLegacyMainScheduler",
        "provideMainScheduler",
        "provideRpcScheduler",
        "impl-rx2-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/Rx2SchedulerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/thread/Rx2SchedulerModule;

    invoke-direct {v0}, Lcom/squareup/thread/Rx2SchedulerModule;-><init>()V

    sput-object v0, Lcom/squareup/thread/Rx2SchedulerModule;->INSTANCE:Lcom/squareup/thread/Rx2SchedulerModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideComputationScheduler()Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Computation;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 11
    sget-object v0, Lcom/squareup/thread/Rx2Schedulers;->INSTANCE:Lcom/squareup/thread/Rx2Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx2Schedulers;->getComputation()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public final provideFileThreadScheduler(Lcom/squareup/thread/Rx2FileScheduler;)Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "scheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lcom/squareup/thread/Rx2FileScheduler;->getFile()Lio/reactivex/Scheduler;

    move-result-object p1

    return-object p1
.end method

.method public final provideLegacyMainScheduler()Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 22
    sget-object v0, Lcom/squareup/thread/Rx2Schedulers;->INSTANCE:Lcom/squareup/thread/Rx2Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx2Schedulers;->getLegacyMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public final provideMainScheduler()Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/thread/Rx2Schedulers;->INSTANCE:Lcom/squareup/thread/Rx2Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx2Schedulers;->getMain()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public final provideRpcScheduler()Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 19
    sget-object v0, Lcom/squareup/thread/Rx2Schedulers;->INSTANCE:Lcom/squareup/thread/Rx2Schedulers;

    invoke-virtual {v0}, Lcom/squareup/thread/Rx2Schedulers;->getIo()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method
