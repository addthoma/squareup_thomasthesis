.class public final Lcom/squareup/thread/AndroidMainThread$Companion;
.super Ljava/lang/Object;
.source "AndroidMainThread.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/thread/AndroidMainThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u0004H\u0087\u0002\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/thread/AndroidMainThread$Companion;",
        "",
        "()V",
        "invoke",
        "Lcom/squareup/thread/AndroidMainThread;",
        "create",
        "impl-main-thread_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/thread/AndroidMainThread$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final create()Lcom/squareup/thread/AndroidMainThread;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/thread/AndroidMainThread;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/thread/AndroidMainThread;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/thread/executor/StoppableSerialExecutor;

    invoke-static {v0}, Lcom/squareup/thread/executor/ExecutorRegistry;->register(Lcom/squareup/thread/executor/StoppableSerialExecutor;)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v0

    const-string v1, "ExecutorRegistry.register(AndroidMainThread())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/thread/AndroidMainThread;

    return-object v0
.end method
