.class public final Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory;
.super Ljava/lang/Object;
.source "CoroutineDispatcherModule_ProvideMainDispatcherFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory$InstanceHolder;->access$000()Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/thread/CoroutineDispatcherModule;->provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory;->get()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public get()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory;->provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method
