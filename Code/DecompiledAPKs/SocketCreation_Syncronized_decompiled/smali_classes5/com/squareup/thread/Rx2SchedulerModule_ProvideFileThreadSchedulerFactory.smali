.class public final Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;
.super Ljava/lang/Object;
.source "Rx2SchedulerModule_ProvideFileThreadSchedulerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lio/reactivex/Scheduler;",
        ">;"
    }
.end annotation


# instance fields
.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx2FileScheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx2FileScheduler;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;->schedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx2FileScheduler;",
            ">;)",
            "Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFileThreadScheduler(Lcom/squareup/thread/Rx2FileScheduler;)Lio/reactivex/Scheduler;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/thread/Rx2SchedulerModule;->INSTANCE:Lcom/squareup/thread/Rx2SchedulerModule;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/Rx2SchedulerModule;->provideFileThreadScheduler(Lcom/squareup/thread/Rx2FileScheduler;)Lio/reactivex/Scheduler;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lio/reactivex/Scheduler;

    return-object p0
.end method


# virtual methods
.method public get()Lio/reactivex/Scheduler;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;->schedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/Rx2FileScheduler;

    invoke-static {v0}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;->provideFileThreadScheduler(Lcom/squareup/thread/Rx2FileScheduler;)Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;->get()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method
