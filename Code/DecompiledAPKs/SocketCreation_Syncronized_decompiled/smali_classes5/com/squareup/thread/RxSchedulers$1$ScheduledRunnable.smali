.class final Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;
.super Ljava/lang/Object;
.source "RxSchedulers.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/thread/RxSchedulers$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ScheduledRunnable"
.end annotation


# instance fields
.field private final delegate:Ljava/lang/Runnable;

.field private volatile disposed:Z

.field final synthetic this$0:Lcom/squareup/thread/RxSchedulers$1;


# direct methods
.method constructor <init>(Lcom/squareup/thread/RxSchedulers$1;Ljava/lang/Runnable;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p2, p0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;->delegate:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;->this$0:Lcom/squareup/thread/RxSchedulers$1;

    invoke-static {v0}, Lcom/squareup/thread/RxSchedulers$1;->access$100(Lcom/squareup/thread/RxSchedulers$1;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    .line 118
    iput-boolean v0, p0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;->disposed:Z

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 123
    iget-boolean v0, p0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;->disposed:Z

    return v0
.end method

.method public run()V
    .locals 1

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/squareup/thread/RxSchedulers$1$ScheduledRunnable;->delegate:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 111
    invoke-static {v0}, Lio/reactivex/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
