.class public final Lcom/squareup/thread/CoroutineDispatcherModule$Companion;
.super Ljava/lang/Object;
.source "CoroutineDispatcherModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/thread/CoroutineDispatcherModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0001J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0001J\u0008\u0010\u0008\u001a\u00020\u0004H\u0001J\u0008\u0010\t\u001a\u00020\u0004H\u0001J\u0008\u0010\n\u001a\u00020\u0004H\u0001\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/thread/CoroutineDispatcherModule$Companion;",
        "",
        "()V",
        "provideComputationDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "provideFileThreadDispatcher",
        "dispatchers",
        "Lcom/squareup/thread/CoroutineFileDispatcher;",
        "provideMainDispatcher",
        "provideMainImmediateDispatcher",
        "provideRpcDispatcher",
        "impl-coroutines-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideComputationDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Computation;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 36
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatchers;->getComputation()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public final provideFileThreadDispatcher(Lcom/squareup/thread/CoroutineFileDispatcher;)Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "dispatchers"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/thread/CoroutineFileDispatcher;->getFile()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p1

    return-object p1
.end method

.method public final provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 39
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatchers;->getMain()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public final provideMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main$Immediate;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 43
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatchers;->getMainImmediate()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public final provideRpcDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 46
    sget-object v0, Lcom/squareup/thread/CoroutineDispatchers;->INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatchers;->getIo()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method
