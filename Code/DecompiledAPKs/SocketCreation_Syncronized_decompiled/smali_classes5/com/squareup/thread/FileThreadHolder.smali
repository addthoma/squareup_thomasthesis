.class public final Lcom/squareup/thread/FileThreadHolder;
.super Ljava/lang/Object;
.source "FileThreadHolder.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFileThreadHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FileThreadHolder.kt\ncom/squareup/thread/FileThreadHolder\n*L\n1#1,24:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/thread/FileThreadHolder;",
        "",
        "()V",
        "enforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "getEnforcer",
        "()Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "executor",
        "Ljava/util/concurrent/Executor;",
        "getExecutor",
        "()Ljava/util/concurrent/Executor;",
        "handler",
        "Landroid/os/Handler;",
        "handlerThread",
        "Landroid/os/HandlerThread;",
        "getHandlerThread",
        "()Landroid/os/HandlerThread;",
        "impl-file-thread_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final handler:Landroid/os/Handler;

.field private final handlerThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Sq--FileIO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    iput-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->handlerThread:Landroid/os/HandlerThread;

    .line 18
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/thread/FileThreadHolder;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->handler:Landroid/os/Handler;

    .line 20
    new-instance v0, Lcom/squareup/thread/FileThreadHolder$executor$1;

    invoke-direct {v0, p0}, Lcom/squareup/thread/FileThreadHolder$executor$1;-><init>(Lcom/squareup/thread/FileThreadHolder;)V

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->executor:Ljava/util/concurrent/Executor;

    .line 22
    sget-object v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;->Companion:Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;

    new-instance v1, Lcom/squareup/thread/FileThreadHolder$enforcer$1;

    invoke-direct {v1, p0}, Lcom/squareup/thread/FileThreadHolder$enforcer$1;-><init>(Lcom/squareup/thread/FileThreadHolder;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;->invoke(Lkotlin/jvm/functions/Function0;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->enforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method public static final synthetic access$getHandler$p(Lcom/squareup/thread/FileThreadHolder;)Landroid/os/Handler;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/thread/FileThreadHolder;->handler:Landroid/os/Handler;

    return-object p0
.end method


# virtual methods
.method public final getEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->enforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object v0
.end method

.method public final getExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final getHandlerThread()Landroid/os/HandlerThread;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/thread/FileThreadHolder;->handlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method
