.class public final Lcom/squareup/sqlbrite3/ExtensionsKt;
.super Ljava/lang/Object;
.source "extensions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nextensions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 extensions.kt\ncom/squareup/sqlbrite3/ExtensionsKt\n*L\n1#1,106:1\n97#1,9:107\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000F\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001aA\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u001d\u0010\u0005\u001a\u0019\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u0002H\u00010\u0006\u00a2\u0006\u0002\u0008\u0008H\u0086\u0008\u00a2\u0006\u0002\u0010\t\u001aE\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00010\u000c0\u000b\"\u0004\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u00020\r0\u000b2\u001e\u0008\u0008\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00010\u000fj\u0008\u0012\u0004\u0012\u0002H\u0001`\u0011H\u0086\u0008\u001a?\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\u000b\"\u0004\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u00020\r0\u000b2\u001e\u0008\u0008\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00010\u000fj\u0008\u0012\u0004\u0012\u0002H\u0001`\u0011H\u0086\u0008\u001aL\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\u000b\"\u0004\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u00020\r0\u000b2\u0006\u0010\u0014\u001a\u0002H\u00012\u001e\u0008\u0008\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00010\u000fj\u0008\u0012\u0004\u0012\u0002H\u0001`\u0011H\u0086\u0008\u00a2\u0006\u0002\u0010\u0015\u001aE\u0010\u0016\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00010\u00170\u000b\"\u0004\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u00020\r0\u000b2\u001e\u0008\u0008\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00010\u000fj\u0008\u0012\u0004\u0012\u0002H\u0001`\u0011H\u0087\u0008*(\u0010\u0018\u001a\u0004\u0008\u0000\u0010\u0001\"\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00010\u000f2\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00010\u000f\u00a8\u0006\u0019"
    }
    d2 = {
        "inTransaction",
        "T",
        "Lcom/squareup/sqlbrite3/BriteDatabase;",
        "exclusive",
        "",
        "body",
        "Lkotlin/Function2;",
        "Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;",
        "Lkotlin/ExtensionFunctionType;",
        "(Lcom/squareup/sqlbrite3/BriteDatabase;ZLkotlin/jvm/functions/Function2;)Ljava/lang/Object;",
        "mapToList",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "mapper",
        "Lkotlin/Function1;",
        "Landroid/database/Cursor;",
        "Lcom/squareup/sqlbrite3/Mapper;",
        "mapToOne",
        "mapToOneOrDefault",
        "default",
        "(Lio/reactivex/Observable;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;",
        "mapToOptional",
        "Ljava/util/Optional;",
        "Mapper",
        "sqlbrite-kotlin_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x8
    }
.end annotation


# direct methods
.method public static final inTransaction(Lcom/squareup/sqlbrite3/BriteDatabase;ZLkotlin/jvm/functions/Function2;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/sqlbrite3/BriteDatabase;",
            "Z",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/sqlbrite3/BriteDatabase;",
            "-",
            "Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;",
            "+TT;>;)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->newTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->newNonExclusiveTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    move-result-object p1

    :goto_0
    const/4 v0, 0x1

    :try_start_0
    const-string/jumbo v1, "transaction"

    .line 99
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    .line 100
    invoke-interface {p1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->markSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 103
    invoke-interface {p1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-object p0

    :catchall_0
    move-exception p0

    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    invoke-interface {p1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p0
.end method

.method public static bridge synthetic inTransaction$default(Lcom/squareup/sqlbrite3/BriteDatabase;ZLkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    const/4 p4, 0x1

    and-int/2addr p3, p4

    if-eqz p3, :cond_0

    const/4 p1, 0x1

    :cond_0
    if-eqz p1, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->newTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->newNonExclusiveTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    move-result-object p1

    :goto_0
    :try_start_0
    const-string/jumbo p3, "transaction"

    .line 109
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    .line 110
    invoke-interface {p1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->markSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    invoke-static {p4}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 113
    invoke-interface {p1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    invoke-static {p4}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-object p0

    :catchall_0
    move-exception p0

    invoke-static {p4}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    invoke-interface {p1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    invoke-static {p4}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p0
.end method

.method public static final mapToList(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/database/Cursor;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 85
    new-instance v0, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;

    invoke-direct {v0, p1}, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-static {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToList(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "lift(Query.mapToList(mapper))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapToOne(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/database/Cursor;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 41
    new-instance v0, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;

    invoke-direct {v0, p1}, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-static {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToOne(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "lift(Query.mapToOne(mapper))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapToOneOrDefault(Lio/reactivex/Observable;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/database/Cursor;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 56
    new-instance v0, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;

    invoke-direct {v0, p2}, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p2, v0

    :cond_0
    check-cast p2, Lio/reactivex/functions/Function;

    invoke-static {p2, p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToOneOrDefault(Lio/reactivex/functions/Function;Ljava/lang/Object;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "lift(Query.mapToOneOrDefault(mapper, default))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapToOptional(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/database/Cursor;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 71
    new-instance v0, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;

    invoke-direct {v0, p1}, Lcom/squareup/sqlbrite3/ExtensionsKt$sam$Function$i$5e743c9a;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-static {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToOptional(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "lift(Query.mapToOptional(mapper))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
