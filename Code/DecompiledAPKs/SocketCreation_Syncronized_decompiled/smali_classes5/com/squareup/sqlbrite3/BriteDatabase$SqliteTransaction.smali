.class final Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;
.super Ljava/util/LinkedHashSet;
.source "BriteDatabase.java"

# interfaces
.implements Landroid/database/sqlite/SQLiteTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/BriteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SqliteTransaction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashSet<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/database/sqlite/SQLiteTransactionListener;"
    }
.end annotation


# instance fields
.field commit:Z

.field final parent:Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;)V
    .locals 0

    .line 755
    invoke-direct {p0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 756
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->parent:Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    return-void
.end method


# virtual methods
.method public onBegin()V
    .locals 0

    return-void
.end method

.method public onCommit()V
    .locals 1

    const/4 v0, 0x1

    .line 763
    iput-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->commit:Z

    return-void
.end method

.method public onRollback()V
    .locals 0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 770
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "%08x"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 771
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->parent:Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->parent:Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
