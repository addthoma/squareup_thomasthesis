.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
.super Ljava/lang/Object;
.source "PricingRuleFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;,
        Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;,
        Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;,
        Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;
    }
.end annotation


# static fields
.field public static final name:Ljava/lang/String; = "blah"


# virtual methods
.method public abstract getApplicationMode()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;
.end method

.method public abstract getApplyOrMatchProductSetId()Ljava/lang/String;
.end method

.method public abstract getApplyProductSetId()Ljava/lang/String;
.end method

.method public abstract getDiscountId()Ljava/lang/String;
.end method

.method public abstract getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;
.end method

.method public abstract getExcludeProductSetId()Ljava/lang/String;
.end method

.method public abstract getExcludeStrategy()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getMatchProductSetId()Ljava/lang/String;
.end method

.method public abstract getMaxApplicationsPerAttachment()I
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;
.end method

.method public abstract getValidFrom()Ljava/lang/String;
.end method

.method public abstract getValidUntil()Ljava/lang/String;
.end method

.method public abstract getValidity()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
