.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;
.super Ljava/lang/Object;
.source "DiscountFacade.java"


# virtual methods
.method public abstract getAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;
.end method

.method public abstract getMaximumAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;
.end method

.method public abstract getPercentage()Ljava/lang/String;
.end method

.method public abstract isPercentageDiscount()Z
.end method
