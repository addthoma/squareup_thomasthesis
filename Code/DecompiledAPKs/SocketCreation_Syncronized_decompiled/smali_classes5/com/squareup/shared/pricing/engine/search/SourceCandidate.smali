.class public Lcom/squareup/shared/pricing/engine/search/SourceCandidate;
.super Ljava/lang/Object;
.source "SourceCandidate.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/Candidate;


# instance fields
.field private final itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

.field private offset:Ljava/math/BigDecimal;

.field private final preMatched:Z

.field private quantity:Ljava/math/BigDecimal;

.field private quantityCap:Ljava/math/BigDecimal;

.field private final unitValue:J


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;JZ)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 30
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantityCap:Ljava/math/BigDecimal;

    .line 31
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->offset:Ljava/math/BigDecimal;

    .line 32
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    .line 33
    iput-wide p5, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->unitValue:J

    .line 34
    iput-boolean p7, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->preMatched:Z

    return-void
.end method


# virtual methods
.method public alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getExistingAutoDiscounts()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public canSplit()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public filter(Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Candidate;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v0

    .line 118
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 121
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-ltz v1, :cond_1

    return-object p0

    .line 124
    :cond_1
    new-instance v1, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Ljava/math/BigDecimal;

    iget-object v5, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->offset:Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantityCap:Ljava/math/BigDecimal;

    iget-wide v7, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->unitValue:J

    iget-boolean v9, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->preMatched:Z

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;-><init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;JZ)V

    return-object v1
.end method

.method public getApplications()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getCaps()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantityCap:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getFractional()Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getFractional()Z

    move-result v0

    return v0
.end method

.method public getMaxUnitPrice()J
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMinUnitPrice()J
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getOffsets()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->offset:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getPreMatched()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->preMatched:Z

    return v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getTiebreakers()Ljava/lang/String;
    .locals 2

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getCategoryID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getCategoryID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getItemID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getItemID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getVariationID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 90
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getVariationID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :cond_2
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 93
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnitQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 58
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getUnitValue()J
    .locals 2

    .line 54
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->unitValue:J

    return-wide v0
.end method

.method public take(Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/search/Candidate;
    .locals 9

    .line 103
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_2

    .line 106
    invoke-virtual {p1}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getFractional()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 107
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot split non-fractional candidate by fraction"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 110
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantity:Ljava/math/BigDecimal;

    .line 112
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->offset:Ljava/math/BigDecimal;

    iget-object v5, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->quantityCap:Ljava/math/BigDecimal;

    iget-wide v6, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->unitValue:J

    iget-boolean v8, p0, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;->preMatched:Z

    move-object v1, v0

    move-object v3, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;-><init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;JZ)V

    return-object v0

    .line 104
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot split more than available quantity"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
