.class public Lcom/squareup/shared/pricing/engine/util/Comparator;
.super Ljava/lang/Object;
.source "Comparator.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static emptyIfNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method public static forClientServerIds(Lcom/squareup/shared/pricing/models/ClientServerIds;Lcom/squareup/shared/pricing/models/ClientServerIds;)I
    .locals 2

    .line 16
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/pricing/engine/util/Comparator;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/pricing/engine/util/Comparator;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getServerId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/shared/pricing/engine/util/Comparator;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getServerId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/pricing/engine/util/Comparator;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method
