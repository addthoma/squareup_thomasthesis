.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;
.super Ljava/lang/Object;
.source "CatalogFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Local"
.end annotation


# virtual methods
.method public abstract findActivePricingRules(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findActivePricingRules(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findDiscountsByIds(Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findPricingRulesByIds(Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findProductSetsByIds(Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findTimePeriodsByIds(Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllProductSets()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;"
        }
    .end annotation
.end method
