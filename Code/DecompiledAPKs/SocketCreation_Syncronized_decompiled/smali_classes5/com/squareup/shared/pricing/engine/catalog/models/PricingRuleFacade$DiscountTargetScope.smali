.class public final enum Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;
.super Ljava/lang/Enum;
.source "PricingRuleFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DiscountTargetScope"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

.field public static final enum LINE_ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

.field public static final enum WHOLE_PURCHASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 59
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    const/4 v1, 0x0

    const-string v2, "LINE_ITEM"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    .line 60
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    const/4 v2, 0x1

    const-string v3, "WHOLE_PURCHASE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    .line 58
    sget-object v3, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->$VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;
    .locals 1

    .line 58
    const-class v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->$VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    invoke-virtual {v0}, [Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    return-object v0
.end method
