.class public Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;
.super Ljava/lang/Object;
.source "MonetaryAmount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/models/MonetaryAmount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field amount:J

.field currency:Ljava/util/Currency;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(J)Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;
    .locals 0

    .line 43
    iput-wide p1, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;->amount:J

    return-object p0
.end method

.method public build()Lcom/squareup/shared/pricing/models/MonetaryAmount;
    .locals 4

    .line 53
    new-instance v0, Lcom/squareup/shared/pricing/models/MonetaryAmount;

    iget-wide v1, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;->amount:J

    iget-object v3, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;->currency:Ljava/util/Currency;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/shared/pricing/models/MonetaryAmount;-><init>(JLjava/util/Currency;)V

    return-object v0
.end method

.method public currency(Ljava/util/Currency;)Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;->currency:Ljava/util/Currency;

    return-object p0
.end method
