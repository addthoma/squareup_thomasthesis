.class public Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;
.super Ljava/lang/Object;
.source "WholePurchaseRuleNode.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/RuleNode;


# instance fields
.field private final discount:Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;

.field private final itemizations:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

.field private final rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 33
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->discount:Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;

    .line 34
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 35
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->itemizations:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public bestApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 12

    .line 44
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 45
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v0

    .line 46
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 51
    :cond_0
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 52
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 54
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->itemizations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v2, 0x0

    move-wide v4, v2

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 56
    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getBlacklistedDiscounts()Ljava/util/Set;

    move-result-object v7

    iget-object v8, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-interface {v8}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 57
    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v7

    .line 58
    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v7

    const/4 v8, 0x0

    sget-object v11, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    .line 59
    invoke-virtual {v7, v8, v11}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v7

    .line 60
    invoke-virtual {v7}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v7

    add-long/2addr v4, v7

    .line 63
    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v7

    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v8

    invoke-interface {v9, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-virtual {v6}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v6

    sget-object v7, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-interface {v10, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->discount:Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;

    invoke-static {v0, v4, v5}, Lcom/squareup/shared/pricing/engine/search/helpers/DiscountHelper;->getDiscountValueForAmount(Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;J)J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_3

    return-object v1

    .line 78
    :cond_3
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    const-wide/16 v6, 0x1

    move-object v2, v0

    move-object v8, v9

    invoke-direct/range {v2 .. v10}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method public commitMatch(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)V
    .locals 0

    return-void
.end method

.method public getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    return-object v0
.end method
