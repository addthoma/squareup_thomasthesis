.class public Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;
.super Ljava/lang/Object;
.source "RecurrenceRuleImpl.java"

# interfaces
.implements Lcom/squareup/shared/ical/rrule/RecurrenceRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    }
.end annotation


# instance fields
.field private final byDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation
.end field

.field private final byMonth:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final byMonthDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final bySetPos:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final byWeekNo:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final byYearDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final count:Ljava/lang/Integer;

.field private final dtStart:Ljava/util/Date;

.field private final exDates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field private final interval:I

.field private final timeZone:Ljava/util/TimeZone;

.field private final until:Ljava/util/Date;

.field private final wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;


# direct methods
.method public constructor <init>(Ljava/util/Date;Ljava/util/TimeZone;Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;Ljava/util/Set;Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;ILjava/lang/Integer;Ljava/util/Date;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;",
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;",
            "I",
            "Ljava/lang/Integer;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->dtStart:Ljava/util/Date;

    .line 44
    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->timeZone:Ljava/util/TimeZone;

    .line 45
    iput-object p3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 46
    iput-object p4, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->exDates:Ljava/util/Set;

    .line 47
    iput-object p5, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    .line 48
    iput p6, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->interval:I

    .line 49
    iput-object p7, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->count:Ljava/lang/Integer;

    .line 50
    iput-object p9, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonth:Ljava/util/Set;

    .line 51
    iput-object p10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byWeekNo:Ljava/util/Set;

    .line 52
    iput-object p11, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byYearDay:Ljava/util/Set;

    .line 53
    iput-object p12, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonthDay:Ljava/util/Set;

    .line 54
    iput-object p13, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byDay:Ljava/util/Set;

    .line 55
    iput-object p14, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->bySetPos:Ljava/util/Set;

    .line 56
    iput-object p8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->until:Ljava/util/Date;

    return-void
.end method

.method public static newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 1

    .line 120
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;-><init>()V

    return-object v0
.end method

.method private setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;
    .locals 2

    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_1

    .line 295
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 296
    instance-of v1, p2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    if-eqz v1, :cond_0

    .line 297
    check-cast p2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    invoke-virtual {p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 299
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string p2, ", "

    .line 301
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string p1, "null"

    .line 304
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string p1, "\n"

    .line 306
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 246
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    .line 247
    :cond_1
    check-cast p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;

    .line 248
    iget v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->interval:I

    iget v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->interval:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->dtStart:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->dtStart:Ljava/util/Date;

    .line 249
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->timeZone:Ljava/util/TimeZone;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->timeZone:Ljava/util/TimeZone;

    .line 250
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->exDates:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->exDates:Ljava/util/Set;

    .line 252
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->count:Ljava/lang/Integer;

    .line 254
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonth:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonth:Ljava/util/Set;

    .line 255
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byWeekNo:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byWeekNo:Ljava/util/Set;

    .line 256
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byYearDay:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byYearDay:Ljava/util/Set;

    .line 257
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonthDay:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonthDay:Ljava/util/Set;

    .line 258
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byDay:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byDay:Ljava/util/Set;

    .line 259
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->bySetPos:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->bySetPos:Ljava/util/Set;

    .line 260
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->until:Ljava/util/Date;

    iget-object p1, p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->until:Ljava/util/Date;

    .line 261
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getByDay()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byDay:Ljava/util/Set;

    return-object v0
.end method

.method public getByMonth()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonth:Ljava/util/Set;

    return-object v0
.end method

.method public getByMonthDay()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonthDay:Ljava/util/Set;

    return-object v0
.end method

.method public getBySetPos()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->bySetPos:Ljava/util/Set;

    return-object v0
.end method

.method public getByWeekNo()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byWeekNo:Ljava/util/Set;

    return-object v0
.end method

.method public getByYearDay()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byYearDay:Ljava/util/Set;

    return-object v0
.end method

.method public getCount()Ljava/lang/Integer;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->count:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDtStart()Ljava/util/Date;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->dtStart:Ljava/util/Date;

    return-object v0
.end method

.method public getExDates()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->exDates:Ljava/util/Set;

    return-object v0
.end method

.method public getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    return-object v0
.end method

.method public getInterval()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->interval:I

    return v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public getUntil()Ljava/util/Date;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->until:Ljava/util/Date;

    return-object v0
.end method

.method public getWkst()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    .line 286
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->dtStart:Ljava/util/Date;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->timeZone:Ljava/util/TimeZone;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->exDates:Ljava/util/Set;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->interval:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->count:Ljava/lang/Integer;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonth:Ljava/util/Set;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byWeekNo:Ljava/util/Set;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byYearDay:Ljava/util/Set;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonthDay:Ljava/util/Set;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byDay:Ljava/util/Set;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->bySetPos:Ljava/util/Set;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->until:Ljava/util/Date;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;-><init>(Lcom/squareup/shared/ical/rrule/RecurrenceRule;Ljava/util/Date;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dtStart: "

    .line 267
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->dtStart:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "timeZone: "

    .line 268
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v2}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "wkst: "

    .line 269
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->exDates:Ljava/util/Set;

    const-string v3, "exDates: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "frequency: "

    .line 271
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "interval: "

    .line 272
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->interval:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "count: "

    .line 273
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->count:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonth:Ljava/util/Set;

    const-string v3, "byMonth: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byWeekNo:Ljava/util/Set;

    const-string v3, "byWeekNo: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byYearDay:Ljava/util/Set;

    const-string v3, "byYearDay: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byMonthDay:Ljava/util/Set;

    const-string v3, "byMonthDay: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->byDay:Ljava/util/Set;

    const-string v3, "byDay: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->bySetPos:Ljava/util/Set;

    const-string v3, "bySetPos: "

    invoke-direct {p0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->setToString(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "until: "

    .line 280
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;->until:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
