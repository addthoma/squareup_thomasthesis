.class public Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;
.super Ljava/lang/Object;
.source "RecurrenceRuleIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;,
        Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DAYS_IN_WEEK:I = 0x7


# instance fields
.field private final byDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation
.end field

.field private final byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

.field private final byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

.field private final bySetPos:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final byWeekNo:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

.field private final byYearDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

.field private final calendar:Ljava/util/Calendar;

.field private final count:Ljava/lang/Integer;

.field private final currentDateSet:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final currentWorkingDates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private datesGenerated:I

.field private final dtStart:Ljava/util/Date;

.field private final effectiveUntil:Ljava/util/Date;

.field private final exDates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field private final interval:I

.field private intervalMult:I

.field private lastStartTime:Ljava/util/Date;

.field private final ordinalByDaySet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation
.end field

.field private final simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

.field private final timeZone:Ljava/util/TimeZone;

.field private final until:Ljava/util/Date;

.field private final wkstValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/ical/rrule/RecurrenceRule;Ljava/util/Date;)V
    .locals 3

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    const/4 v0, 0x0

    .line 55
    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    .line 59
    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->intervalMult:I

    const/4 v0, 0x0

    .line 64
    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->lastStartTime:Ljava/util/Date;

    .line 69
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getDtStart()Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    .line 70
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->timeZone:Ljava/util/TimeZone;

    .line 71
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getExDates()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->exDates:Ljava/util/Set;

    .line 72
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->timeZone:Ljava/util/TimeZone;

    invoke-static {v1}, Ljava/util/GregorianCalendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    .line 73
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getWkst()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->getDayValue()I

    move-result v1

    iput v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->wkstValue:I

    .line 75
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    .line 76
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    iget v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->wkstValue:I

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 77
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 79
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    .line 80
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getInterval()I

    move-result v1

    iput v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->interval:I

    .line 81
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getUntil()Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->until:Ljava/util/Date;

    .line 82
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->until:Ljava/util/Date;

    invoke-static {v1, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->computeEffectiveUntil(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    .line 83
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getCount()Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->count:Ljava/lang/Integer;

    .line 84
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getBySetPos()Ljava/util/Set;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->bySetPos:Ljava/util/Set;

    .line 86
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonth()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->forValues(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    .line 87
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByDay()Ljava/util/Set;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byDay:Ljava/util/Set;

    .line 88
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonthDay()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->forValues(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    .line 89
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByYearDay()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->forValues(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byYearDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    .line 90
    invoke-interface {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByWeekNo()Ljava/util/Set;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->forValues(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byWeekNo:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    .line 92
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byDay:Ljava/util/Set;

    if-eqz p1, :cond_4

    .line 93
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 94
    iget-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byDay:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 95
    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getOrdwk()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_0

    .line 96
    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->getDayValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_1
    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->forValues(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    .line 103
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 104
    iget-object p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byDay:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 105
    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getOrdwk()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 106
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 109
    :cond_3
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    goto :goto_2

    .line 111
    :cond_4
    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    .line 112
    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    :goto_2
    return-void
.end method

.method static synthetic access$200(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;I)I
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->watchFieldForCalendarField(I)I

    move-result p0

    return p0
.end method

.method private static checkArgument(ZLjava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    .line 709
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static computeEffectiveUntil(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;
    .locals 4

    .line 118
    new-instance v0, Ljava/util/Date;

    const-wide v1, 0x7fffffffffffffffL

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    if-nez p1, :cond_0

    .line 120
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v1, v2}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    :cond_0
    move-object v3, p1

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    move-object p0, v0

    .line 126
    :goto_1
    invoke-virtual {p0, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object p1

    :cond_2
    return-object p0
.end method

.method private consumeExDates()V
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 256
    :goto_0
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->exDates:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 257
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 258
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    .line 259
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private evaluateByDay()V
    .locals 6

    .line 396
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-nez v0, :cond_0

    return-void

    .line 399
    :cond_0
    sget-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$1;->$SwitchMap$com$squareup$shared$ical$rrule$RecurrenceRule$Frequency:[I

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x7

    if-eq v0, v1, :cond_9

    const/4 v3, 0x2

    if-eq v0, v3, :cond_8

    const/4 v4, 0x3

    const/4 v5, 0x5

    if-eq v0, v4, :cond_6

    const/4 v4, 0x4

    if-eq v0, v4, :cond_1

    goto/16 :goto_1

    .line 432
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byYearDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byWeekNo:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-eqz v0, :cond_3

    .line 437
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const-string v1, "With YEARLY and no BYWEEKNO or BYMONTHDAY DOW ordinals"

    invoke-static {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->checkArgument(ZLjava/lang/String;)V

    .line 439
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    goto :goto_1

    .line 440
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-eqz v0, :cond_4

    .line 441
    invoke-direct {p0, v5, v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->specialExpandByDay(II)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x6

    .line 443
    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->specialExpandByDay(II)V

    goto :goto_1

    .line 433
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const-string v1, "With YEARLY and no BYYEARDAY or BYMONTHDAY DOW ordinals"

    invoke-static {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->checkArgument(ZLjava/lang/String;)V

    .line 435
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->limitDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    goto :goto_1

    .line 418
    :cond_6
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-eqz v0, :cond_7

    .line 419
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const-string v1, "With MONTHLY and no byMonthDay we should not have any DOW ordinals"

    invoke-static {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->checkArgument(ZLjava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->limitDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    goto :goto_1

    .line 423
    :cond_7
    invoke-direct {p0, v5, v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->specialExpandByDay(II)V

    goto :goto_1

    .line 410
    :cond_8
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const-string v1, "With WEEKLY we should not have any ordinal baseRule"

    invoke-static {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->checkArgument(ZLjava/lang/String;)V

    .line 412
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    goto :goto_1

    .line 403
    :cond_9
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const-string v1, "With DAILY we should not have any ordinal baseRule"

    invoke-static {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->checkArgument(ZLjava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->limitDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    :goto_1
    return-void
.end method

.method private evaluateByMonth()V
    .locals 2

    .line 384
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-eqz v0, :cond_2

    .line 385
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v0

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->MONTHLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-eqz v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->limitDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    goto :goto_1

    .line 390
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    :cond_2
    :goto_1
    return-void
.end method

.method private evaluateByMonthDay()V
    .locals 4

    .line 348
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-nez v0, :cond_0

    return-void

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v0

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->DAILY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v1

    const/4 v2, 0x5

    if-gt v0, v1, :cond_1

    .line 353
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->limitDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    goto :goto_1

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v0

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->MONTHLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v1

    const/4 v3, 0x0

    if-lt v0, v1, :cond_3

    .line 356
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->YEARLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonth:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-nez v0, :cond_2

    :goto_0
    const/16 v0, 0xb

    if-gt v3, v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 359
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byMonthDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    :goto_1
    return-void

    .line 364
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    aput-object v2, v1, v3

    const-string v2, "Illegal combination of BYMONTHDAY and frequency %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private evaluateBySetPos()V
    .locals 6

    .line 302
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->bySetPos:Ljava/util/Set;

    if-nez v0, :cond_0

    return-void

    .line 307
    :cond_0
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    .line 309
    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v1

    .line 313
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 315
    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->bySetPos:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 316
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v5, :cond_1

    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    :cond_1
    add-int/2addr v4, v1

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 317
    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 322
    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 323
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 325
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 326
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 332
    :cond_4
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 333
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private evaluateByWeekNo()V
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byWeekNo:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-nez v0, :cond_0

    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->YEARLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const-string v1, "byweekno can only be set if FREQ is yearly"

    invoke-static {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->checkArgument(ZLjava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byWeekNo:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    return-void
.end method

.method private evaluateByYearDay()V
    .locals 4

    .line 371
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byYearDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    if-nez v0, :cond_0

    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v0

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->YEARLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getOrdinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 375
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->byYearDay:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V

    return-void

    .line 377
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    aput-object v3, v1, v2

    const-string v2, "Illegal combination of BYEARDAY and frequency %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private evaluatedCurrentDateSetUntilEnd()Z
    .locals 4

    .line 179
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    invoke-virtual {v3, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return v2

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->lastStartTime:Ljava/util/Date;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    if-eqz v3, :cond_2

    .line 188
    invoke-virtual {v0, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    return v2

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    iget v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    .line 196
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v3, v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    return v2

    :cond_5
    return v1
.end method

.method private expandDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V
    .locals 10

    .line 547
    invoke-direct {p0, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->getFieldUnitOffset(I)I

    move-result v0

    .line 550
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 552
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 554
    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$100(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 557
    new-instance v4, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;

    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$100(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v5

    invoke-direct {v4, p0, p2, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;-><init>(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;ILjava/util/Set;)V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 559
    :goto_0
    iget-object v5, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Date;

    .line 560
    iget-object v7, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v7, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 563
    iget-object v6, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v6, p2}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v6

    .line 565
    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sub-int/2addr v8, v0

    if-gt v8, v6, :cond_2

    .line 569
    iget-object v9, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v9, p2, v8}, Ljava/util/Calendar;->set(II)V

    .line 570
    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_1

    .line 575
    iget-object v6, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->getMaterializedValues(Ljava/util/Calendar;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 576
    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    sub-int/2addr v7, v0

    invoke-virtual {v8, p2, v7}, Ljava/util/Calendar;->set(II)V

    .line 577
    iget-object v7, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 582
    :cond_4
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 583
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 585
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    return-void
.end method

.method private generateNextInterval()V
    .locals 4

    .line 268
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 269
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->getCalendarField()I

    move-result v1

    iget v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->intervalMult:I

    iget v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->interval:I

    mul-int v2, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 271
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->lastStartTime:Ljava/util/Date;

    .line 272
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->lastStartTime:Ljava/util/Date;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->WEEKLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-ne v0, v1, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateByDay()V

    .line 278
    :cond_0
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateByMonth()V

    .line 279
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateByWeekNo()V

    .line 280
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateByYearDay()V

    .line 281
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateByMonthDay()V

    .line 283
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->WEEKLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-eq v0, v1, :cond_1

    .line 284
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateByDay()V

    .line 286
    :cond_1
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluateBySetPos()V

    .line 288
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    .line 289
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 292
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->clear()V

    .line 294
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->intervalMult:I

    if-nez v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 298
    :cond_2
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->intervalMult:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->intervalMult:I

    return-void
.end method

.method private generatedNumDatesBeyondCount()Z
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->count:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getFieldUnitOffset(I)I
    .locals 1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method private limitDates(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;I)V
    .locals 9

    .line 589
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 590
    invoke-direct {p0, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->getFieldUnitOffset(I)I

    move-result v1

    .line 592
    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$100(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 595
    new-instance v3, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;

    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$100(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, p0, p2, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;-><init>(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;ILjava/util/Set;)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 597
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 599
    iget-object v5, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Date;

    .line 600
    iget-object v7, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v7, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 602
    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v7

    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v8, p2}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 603
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_1

    .line 604
    iget-object v7, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    .line 605
    invoke-virtual {v3, v7}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->getMaterializedValues(Ljava/util/Calendar;)Ljava/util/Set;

    move-result-object v7

    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    .line 606
    invoke-virtual {v8, p2}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 607
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 611
    :cond_3
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 612
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {p1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 614
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    return-void
.end method

.method private specialExpandByDay(II)V
    .locals 13

    .line 455
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 456
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 460
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getMinimalDaysInFirstWeek()I

    move-result v2

    .line 463
    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    .line 465
    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Date;

    .line 466
    iget-object v6, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v6, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 467
    iget-object v6, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    const/4 v7, 0x1

    invoke-virtual {v6, p1, v7}, Ljava/util/Calendar;->set(II)V

    const/4 v6, 0x2

    if-ne p2, v6, :cond_1

    const/4 v6, 0x4

    goto :goto_0

    :cond_1
    if-ne p2, v7, :cond_6

    const/4 v6, 0x3

    .line 479
    :goto_0
    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 480
    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v8

    .line 481
    iget-object v9, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->ordinalByDaySet:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 482
    invoke-virtual {v10}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getOrdwk()Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 486
    invoke-virtual {v10}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->getDayValue()I

    move-result v10

    .line 488
    iget-object v12, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v12, v10}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    if-gez v11, :cond_2

    .line 491
    iget-object v12, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v12, v6}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v12

    add-int/lit8 v11, v11, 0x1

    add-int/2addr v11, v12

    .line 497
    :cond_2
    iget-object v12, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v12, v6, v11}, Ljava/util/Calendar;->set(II)V

    .line 498
    iget-object v11, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v11, v4, v10}, Ljava/util/Calendar;->set(II)V

    .line 499
    iget-object v10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v10}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v10

    invoke-interface {v1, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 503
    :cond_3
    iget-object v9, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v9, v8}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 504
    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v8, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 507
    :cond_4
    iget-object v8, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v8, p2}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 510
    new-instance v9, Ljava/util/TreeSet;

    sget-object v10, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$$Lambda$0;->$instance:Ljava/util/Comparator;

    invoke-direct {v9, v10}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 511
    iget-object v10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->simpleByDaySet:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-static {v10}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 514
    invoke-virtual {v9}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 515
    invoke-virtual {v9}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 516
    iget-object v11, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v11, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 518
    iget-object v11, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v11, p1, v7}, Ljava/util/Calendar;->set(II)V

    .line 521
    iget-object v11, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 522
    iget-object v11, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v11, v4, v10}, Ljava/util/Calendar;->set(II)V

    .line 523
    iget-object v10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v10, v6, v7}, Ljava/util/Calendar;->set(II)V

    .line 526
    :goto_3
    iget-object v10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v10, p2}, Ljava/util/Calendar;->get(I)I

    move-result v10

    if-eq v10, v8, :cond_5

    goto :goto_2

    .line 529
    :cond_5
    iget-object v10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {v10}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v10

    .line 530
    invoke-interface {v1, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531
    iget-object v10, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    const/4 v11, 0x5

    invoke-virtual {v10, v11, v4}, Ljava/util/Calendar;->add(II)V

    goto :goto_3

    .line 476
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Should not get here"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 537
    :cond_7
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    .line 540
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 541
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentWorkingDates:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 543
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->calendar:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    return-void
.end method

.method private watchFieldForCalendarField(I)I
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_1

    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    return v1

    .line 627
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should not get here"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p1, 0x2

    return p1

    :cond_2
    return v1
.end method


# virtual methods
.method public getEvaluatedAllInstants()Z
    .locals 4

    .line 209
    invoke-virtual {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->count:Ljava/lang/Integer;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v3, v0, :cond_1

    return v2

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->until:Ljava/util/Date;

    if-nez v0, :cond_2

    return v1

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 223
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->lastStartTime:Ljava/util/Date;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->until:Ljava/util/Date;

    invoke-virtual {v0, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    :cond_3
    return v1

    .line 230
    :cond_4
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 232
    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->until:Ljava/util/Date;

    invoke-virtual {v0, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_5

    return v2

    :cond_5
    return v1
.end method

.method public hasNext()Z
    .locals 4

    .line 134
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->generatedNumDatesBeyondCount()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 138
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->evaluatedCurrentDateSetUntilEnd()Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->generateNextInterval()V

    .line 143
    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->consumeExDates()V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->generatedNumDatesBeyondCount()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 153
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->lastStartTime:Ljava/util/Date;

    .line 154
    invoke-virtual {v3, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    .line 155
    invoke-virtual {v0, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    iget-object v3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    .line 156
    invoke-interface {v2, v3}, Ljava/util/SortedSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 161
    :cond_3
    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->dtStart:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->effectiveUntil:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_4

    return v1

    :cond_4
    const/4 v0, 0x1

    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->next()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/util/Date;
    .locals 2

    .line 240
    invoke-virtual {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 245
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->currentDateSet:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 246
    iget v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->datesGenerated:I

    return-object v0

    .line 241
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Should not call next if we don\'t have next"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
