.class public interface abstract Lcom/squareup/shared/i18n/Localizer;
.super Ljava/lang/Object;
.source "Localizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/i18n/Localizer$PercentageType;,
        Lcom/squareup/shared/i18n/Localizer$MoneyType;,
        Lcom/squareup/shared/i18n/Localizer$DateTimeType;
    }
.end annotation


# virtual methods
.method public abstract dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;
.end method

.method public abstract money(Lcom/squareup/protos/common/Money;Lcom/squareup/shared/i18n/Localizer$MoneyType;)Ljava/lang/String;
.end method

.method public abstract percentage(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$PercentageType;)Ljava/lang/String;
.end method

.method public abstract simpleString(Lcom/squareup/shared/i18n/Key;)Ljava/lang/String;
.end method

.method public abstract string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
.end method
