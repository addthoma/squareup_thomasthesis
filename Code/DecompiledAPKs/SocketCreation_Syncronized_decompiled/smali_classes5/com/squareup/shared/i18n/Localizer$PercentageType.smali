.class public final enum Lcom/squareup/shared/i18n/Localizer$PercentageType;
.super Ljava/lang/Enum;
.source "Localizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/i18n/Localizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PercentageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/i18n/Localizer$PercentageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/i18n/Localizer$PercentageType;

.field public static final enum DEFAULT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

.field public static final enum DISCOUNT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

.field public static final enum TAX:Lcom/squareup/shared/i18n/Localizer$PercentageType;

.field public static final enum WHOLE_NUMBER:Lcom/squareup/shared/i18n/Localizer$PercentageType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 44
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;

    const/4 v1, 0x0

    const-string v2, "DEFAULT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/i18n/Localizer$PercentageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;->DEFAULT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    .line 45
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;

    const/4 v2, 0x1

    const-string v3, "TAX"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/i18n/Localizer$PercentageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;->TAX:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    .line 46
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;

    const/4 v3, 0x2

    const-string v4, "DISCOUNT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/i18n/Localizer$PercentageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;->DISCOUNT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    .line 47
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;

    const/4 v4, 0x3

    const-string v5, "WHOLE_NUMBER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/i18n/Localizer$PercentageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;->WHOLE_NUMBER:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/shared/i18n/Localizer$PercentageType;

    .line 43
    sget-object v5, Lcom/squareup/shared/i18n/Localizer$PercentageType;->DEFAULT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$PercentageType;->TAX:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$PercentageType;->DISCOUNT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$PercentageType;->WHOLE_NUMBER:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;->$VALUES:[Lcom/squareup/shared/i18n/Localizer$PercentageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/i18n/Localizer$PercentageType;
    .locals 1

    .line 43
    const-class v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/i18n/Localizer$PercentageType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/i18n/Localizer$PercentageType;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/shared/i18n/Localizer$PercentageType;->$VALUES:[Lcom/squareup/shared/i18n/Localizer$PercentageType;

    invoke-virtual {v0}, [Lcom/squareup/shared/i18n/Localizer$PercentageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/i18n/Localizer$PercentageType;

    return-object v0
.end method
