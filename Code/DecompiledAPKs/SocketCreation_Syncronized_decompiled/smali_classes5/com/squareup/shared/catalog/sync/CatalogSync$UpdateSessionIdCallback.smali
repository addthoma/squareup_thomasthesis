.class final Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;
.super Ljava/lang/Object;
.source "CatalogSync.java"

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UpdateSessionIdCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/sync/SyncCallback<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V
    .locals 0

    .line 647
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V
    .locals 0

    .line 647
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 649
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 650
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback$1;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;J)V

    .line 655
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v0

    .line 650
    invoke-virtual {p1, v2, v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method
