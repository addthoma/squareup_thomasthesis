.class public Lcom/squareup/shared/catalog/CatalogResults;
.super Ljava/lang/Object;
.source "CatalogResults.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static empty()Lcom/squareup/shared/catalog/CatalogResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 15
    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/CatalogResult;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$of$0$CatalogResults(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public static of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/CatalogResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "TT;>;"
        }
    .end annotation

    .line 11
    new-instance v0, Lcom/squareup/shared/catalog/CatalogResults$$Lambda$0;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/CatalogResults$$Lambda$0;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method
