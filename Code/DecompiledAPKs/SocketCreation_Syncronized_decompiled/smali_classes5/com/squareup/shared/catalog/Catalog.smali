.class public interface abstract Lcom/squareup/shared/catalog/Catalog;
.super Ljava/lang/Object;
.source "Catalog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/Catalog$Online;,
        Lcom/squareup/shared/catalog/Catalog$Local;
    }
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract executeOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract executeOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TS;>;)V"
        }
    .end annotation
.end method

.method public abstract foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract isCloseEnqueued()Z
.end method

.method public abstract isReady()Z
.end method

.method public abstract preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;
.end method

.method public abstract purge()V
.end method

.method public abstract releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
.end method

.method public abstract resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
.end method

.method public abstract shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method

.method public abstract sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract upsertCatalogConnectV2Object(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            "Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation
.end method
