.class public Lcom/squareup/shared/catalog/models/CatalogFloorPlan;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogFloorPlan.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/FloorPlan;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 2

    .line 21
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlan;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlan;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlan;->height:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FloorPlan;->DEFAULT_HEIGHT:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 17
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlan;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlan;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlan;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 29
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlan;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlan;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlan;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FloorPlan;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 2

    .line 25
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlan;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlan;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlan;->width:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FloorPlan;->DEFAULT_WIDTH:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
