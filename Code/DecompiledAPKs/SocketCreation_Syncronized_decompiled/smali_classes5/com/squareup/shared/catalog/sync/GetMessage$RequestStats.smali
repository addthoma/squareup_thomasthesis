.class Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;
.super Ljava/lang/Object;
.source "GetMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/GetMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestStats"
.end annotation


# instance fields
.field private databaseDurationMs:J

.field private deletedObjectsCount:I

.field private final requestStartTime:J

.field private final threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

.field private updatedObjectsCount:I


# direct methods
.method constructor <init>(JLcom/squareup/shared/catalog/CatalogThreadsEnforcer;)V
    .locals 0

    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    iput-wide p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->requestStartTime:J

    .line 377
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;)I
    .locals 0

    .line 368
    iget p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->deletedObjectsCount:I

    return p0
.end method

.method static synthetic access$1100(Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;)J
    .locals 2

    .line 368
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->databaseDurationMs:J

    return-wide v0
.end method

.method static synthetic access$900(Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;)I
    .locals 0

    .line 368
    iget p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->updatedObjectsCount:I

    return p0
.end method


# virtual methods
.method getTotalDurationMs(J)J
    .locals 2

    .line 396
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->requestStartTime:J

    sub-long/2addr p1, v0

    return-wide p1
.end method

.method getTotalObjectsCount()I
    .locals 2

    .line 392
    iget v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->updatedObjectsCount:I

    iget v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->deletedObjectsCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method increaseDatabaseDuration(J)V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 388
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->databaseDurationMs:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->databaseDurationMs:J

    return-void
.end method

.method increaseObjectsCounts(II)V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 382
    iget v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->updatedObjectsCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->updatedObjectsCount:I

    .line 383
    iget p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->deletedObjectsCount:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->deletedObjectsCount:I

    return-void
.end method
