.class public final Lcom/squareup/shared/catalog/models/CatalogTaxRule;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogTaxRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/TaxRule;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static createForTest(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;ZLjava/util/List;Ljava/lang/Boolean;)Lcom/squareup/shared/catalog/models/CatalogTaxRule;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;"
        }
    .end annotation

    .line 43
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 44
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 45
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 46
    new-instance v3, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v3}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 49
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_1
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 50
    new-instance v3, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v3}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 52
    :cond_1
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 53
    invoke-interface {p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p6

    :goto_2
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 54
    new-instance v3, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v3}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 57
    :cond_2
    new-instance p6, Lcom/squareup/shared/catalog/models/CatalogTaxRule;

    new-instance v2, Lcom/squareup/api/items/TaxRule$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/TaxRule$Builder;-><init>()V

    .line 59
    invoke-virtual {v2, p0}, Lcom/squareup/api/items/TaxRule$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p0

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p0

    sget-object p1, Lcom/squareup/api/items/EnablingActionType;->DISABLE:Lcom/squareup/api/items/EnablingActionType;

    .line 61
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->action_type(Lcom/squareup/api/items/EnablingActionType;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    new-instance v2, Lcom/squareup/api/items/ObjectPredicate$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/ObjectPredicate$Builder;-><init>()V

    .line 64
    invoke-virtual {v2, v1}, Lcom/squareup/api/items/ObjectPredicate$Builder;->object_id(Ljava/util/List;)Lcom/squareup/api/items/ObjectPredicate$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/ObjectPredicate$Type;->IN:Lcom/squareup/api/items/ObjectPredicate$Type;

    .line 65
    invoke-virtual {v1, v2}, Lcom/squareup/api/items/ObjectPredicate$Builder;->type(Lcom/squareup/api/items/ObjectPredicate$Type;)Lcom/squareup/api/items/ObjectPredicate$Builder;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lcom/squareup/api/items/ObjectPredicate$Builder;->build()Lcom/squareup/api/items/ObjectPredicate;

    move-result-object v1

    .line 63
    invoke-virtual {p1, v1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate(Lcom/squareup/api/items/ObjectPredicate;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    .line 62
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->condition(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/api/items/TaxSetConfiguration$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;-><init>()V

    if-eqz p3, :cond_3

    .line 69
    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    goto :goto_3

    :cond_3
    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    :goto_3
    invoke-virtual {p1, v1}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->filter(Lcom/squareup/api/items/ObjectFilter;)Lcom/squareup/api/items/TaxSetConfiguration$Builder;

    move-result-object p1

    if-eqz p3, :cond_4

    .line 70
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_4
    invoke-virtual {p1, p2}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->tax(Ljava/util/List;)Lcom/squareup/api/items/TaxSetConfiguration$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->build()Lcom/squareup/api/items/TaxSetConfiguration;

    move-result-object p1

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->tax_config(Lcom/squareup/api/items/TaxSetConfiguration;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/api/items/ItemSetConfiguration$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/ItemSetConfiguration$Builder;-><init>()V

    if-eqz p5, :cond_5

    .line 73
    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    goto :goto_4

    :cond_5
    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    :goto_4
    invoke-virtual {p1, p2}, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->filter(Lcom/squareup/api/items/ObjectFilter;)Lcom/squareup/api/items/ItemSetConfiguration$Builder;

    move-result-object p1

    if-eqz p5, :cond_6

    .line 74
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p4

    :cond_6
    invoke-virtual {p1, p4}, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->item(Ljava/util/List;)Lcom/squareup/api/items/ItemSetConfiguration$Builder;

    move-result-object p1

    .line 75
    invoke-virtual {p1, p7}, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->applies_to_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemSetConfiguration$Builder;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->build()Lcom/squareup/api/items/ItemSetConfiguration;

    move-result-object p1

    .line 72
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->item_config(Lcom/squareup/api/items/ItemSetConfiguration;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p0

    .line 77
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Builder;->build()Lcom/squareup/api/items/TaxRule;

    move-result-object p0

    .line 58
    invoke-virtual {v0, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule(Lcom/squareup/api/items/TaxRule;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {p6, p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object p6
.end method


# virtual methods
.method public getCondition()Lcom/squareup/api/items/TaxRule$Condition;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    return-object v0
.end method

.method public getDiningOptionIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 94
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getCondition()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 95
    iget-object v1, v0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    iget-object v0, v0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    sget-object v1, Lcom/squareup/api/items/ObjectPredicate$Type;->IN:Lcom/squareup/api/items/ObjectPredicate$Type;

    if-ne v0, v1, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    iget-object v0, v0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/sync/ObjectId;

    .line 102
    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1

    .line 106
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getEnablingActionType()Lcom/squareup/api/items/EnablingActionType;
    .locals 2

    .line 137
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    sget-object v1, Lcom/squareup/api/items/TaxRule;->DEFAULT_ACTION_TYPE:Lcom/squareup/api/items/EnablingActionType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/EnablingActionType;

    return-object v0
.end method

.method public getItemSetConfiguration()Lcom/squareup/api/items/ItemSetConfiguration;
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    return-object v0
.end method

.method public getMaxItemPrice()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 110
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getCondition()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 114
    :cond_0
    iget-object v0, v0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_1

    goto :goto_0

    .line 115
    :cond_1
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method public getMaxTotalAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 119
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getCondition()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 123
    :cond_0
    iget-object v0, v0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_1

    goto :goto_0

    .line 124
    :cond_1
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method public getMinItemPrice()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 128
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getCondition()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 132
    :cond_0
    iget-object v0, v0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_1

    goto :goto_0

    .line 133
    :cond_1
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 86
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTaxSetConfiguration()Lcom/squareup/api/items/TaxSetConfiguration;
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    return-object v0
.end method
