.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
.super Ljava/lang/Object;
.source "CatalogConnectV2ItemVariation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 93
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 94
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;)V
    .locals 1

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 100
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-void
.end method


# virtual methods
.method public addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 2

    .line 260
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getItemOptionID()Ljava/lang/String;

    move-result-object v0

    .line 261
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object p1

    .line 262
    new-instance v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;-><init>()V

    .line 264
    invoke-virtual {v1, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;->item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;

    move-result-object v0

    .line 265
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;->item_option_value_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;

    move-result-object p1

    .line 266
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;

    move-result-object p1

    .line 268
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;
    .locals 2

    .line 326
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    .line 328
    new-instance v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v1
.end method

.method public disableAtLocation(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public enableAtLocations(Ljava/util/List;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;"
        }
    .end annotation

    .line 304
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 305
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 306
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public getAllItemOptionValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;",
            ">;"
        }
    .end annotation

    .line 256
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    return-object v0
.end method

.method public getMeasurementUnitId()Ljava/lang/String;
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->measurement_unit_id:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPriceForLocation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 3

    const-string v0, "Location Id"

    .line 175
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 177
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 178
    iget-object v2, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    if-ne p1, v0, :cond_1

    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-nez p1, :cond_1

    goto :goto_0

    .line 183
    :cond_1
    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p1

    .line 187
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p1
.end method

.method public getSku()Ljava/lang/String;
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->sku:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isVariablePricing()Z
    .locals 3

    .line 127
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->VARIABLE_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    sget-object v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->DEFAULT_PRICING_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 128
    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAllOptionValues()Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object p0
.end method

.method public removeOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 2

    .line 286
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getItemOptionID()Ljava/lang/String;

    move-result-object v0

    .line 287
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object p1

    .line 288
    new-instance v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;-><init>()V

    .line 290
    invoke-virtual {v1, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;->item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;

    move-result-object v0

    .line 291
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;->item_option_value_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;

    move-result-object p1

    .line 292
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;

    move-result-object p1

    .line 294
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public reorderOption(Ljava/lang/String;II)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 4

    .line 276
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    .line 277
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;

    .line 278
    iget-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "itemOption with id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is unavailable in the variation."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 280
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 281
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    invoke-interface {p1, p3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    return-object p0
.end method

.method public setMeasurementUnitId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->measurement_unit_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-object p0
.end method

.method public setOrClearPriceDescriptionGlobally(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 3

    .line 156
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->price_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    .line 159
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 160
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 161
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 162
    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v1

    .line 163
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    move-result-object v1

    .line 164
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-object p0
.end method

.method public setOrClearPriceGlobally(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    if-ne p2, v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "A variation with fixed pricing-type must have a nonnull price-money."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    .line 137
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->pricing_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    .line 140
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object p2, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 142
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 143
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 144
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->pricing_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    move-result-object v0

    .line 146
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 150
    :cond_2
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {p2, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-object p0
.end method

.method public setPresentAtAllLocationsForTesting(Z)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_all_locations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setPriceDescriptionForLocation(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 4

    const-string v0, "Location Id"

    .line 219
    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 220
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 221
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 222
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->pricing_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 226
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 228
    iget-object v3, v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    .line 230
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 236
    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object p1

    .line 237
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    move-result-object p1

    .line 239
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object p2, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public setPriceForLocation(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 4

    const-string v0, "Location Id"

    .line 191
    invoke-static {p3, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 192
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    if-ne p2, v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "A price override with a fixed pricing type must have a non null price-money."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 194
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;-><init>()V

    invoke-virtual {v0, p3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 195
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 199
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 200
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 201
    iget-object v3, v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 202
    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    .line 203
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 209
    :cond_3
    invoke-virtual {v0, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->pricing_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object p2

    .line 210
    invoke-virtual {p2, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object p1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    move-result-object p1

    .line 213
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    iget-object p2, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public setSKU(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;->itemVariation:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->sku(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;

    return-object p0
.end method
