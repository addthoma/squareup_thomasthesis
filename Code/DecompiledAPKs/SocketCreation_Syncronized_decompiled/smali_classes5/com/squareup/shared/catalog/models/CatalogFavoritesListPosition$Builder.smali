.class public final Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;
.super Ljava/lang/Object;
.source "CatalogFavoritesListPosition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final position:Lcom/squareup/api/items/FavoritesListPosition$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FAVORITES_LIST_POSITION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 58
    new-instance v0, Lcom/squareup/api/items/FavoritesListPosition$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/FavoritesListPosition$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/FavoritesListPosition$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->position:Lcom/squareup/api/items/FavoritesListPosition$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 63
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {p1}, Lcom/squareup/api/items/FavoritesListPosition;->newBuilder()Lcom/squareup/api/items/FavoritesListPosition$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->position:Lcom/squareup/api/items/FavoritesListPosition$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->position:Lcom/squareup/api/items/FavoritesListPosition$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->build()Lcom/squareup/api/items/FavoritesListPosition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position(Lcom/squareup/api/items/FavoritesListPosition;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 78
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setFavoriteObject(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->position:Lcom/squareup/api/items/FavoritesListPosition$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/FavoritesListPosition$Builder;

    return-object p0
.end method

.method public setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;->position:Lcom/squareup/api/items/FavoritesListPosition$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/FavoritesListPosition$Builder;

    return-object p0
.end method
