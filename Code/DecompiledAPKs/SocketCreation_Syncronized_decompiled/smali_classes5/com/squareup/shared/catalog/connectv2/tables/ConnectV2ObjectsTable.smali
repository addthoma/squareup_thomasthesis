.class public Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;
.super Ljava/lang/Object;
.source "ConnectV2ObjectsTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;
    }
.end annotation


# static fields
.field public static final COLUMN_ID:Ljava/lang/String; = "connectv2_object_id"

.field public static final COLUMN_OBJECT:Ljava/lang/String; = "connectv2_object_blob"

.field static final COLUMN_OBJECT_TYPE:Ljava/lang/String; = "connectv2_object_type"

.field static final COLUMN_SORT_TEXT:Ljava/lang/String; = "connectv2_sort_text"

.field private static INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable; = null

.field public static final NAME:Ljava/lang/String; = "connectv2_objects"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    .line 37
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    return-object v0
.end method


# virtual methods
.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 130
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->CREATE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 135
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->DELETE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 136
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public deleteAllObjectsWithType(Lcom/squareup/shared/sql/SQLDatabase;J)V
    .locals 1

    .line 141
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->DELETE_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 142
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public insertOrReplace(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;JLjava/lang/String;[B)V
    .locals 1

    .line 152
    invoke-static {p5}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 154
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 155
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 156
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 157
    invoke-virtual {p1, p5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 158
    invoke-virtual {p1, p6}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindBlob([B)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public selectObjectById(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;J)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 163
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x1

    aput-object p2, v0, p3

    .line 164
    sget-object p2, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public selectObjectsByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;J)Lcom/squareup/shared/sql/SQLCursor;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;J)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 173
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 175
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    add-int/lit8 v4, v2, 0x1

    .line 176
    aput-object v3, v0, v2

    move v2, v4

    goto :goto_0

    .line 178
    :cond_0
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p3

    aput-object p3, v0, v2

    .line 179
    sget-object p3, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 180
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p2

    const-string p4, "%ids%"

    invoke-virtual {p3, p4, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 179
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public selectObjectsByType(Lcom/squareup/shared/sql/SQLDatabase;J)Lcom/squareup/shared/sql/SQLCursor;
    .locals 1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 168
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, v0, p3

    .line 169
    sget-object p2, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method
