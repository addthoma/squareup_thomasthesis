.class public Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
.super Ljava/lang/Object;
.source "LibraryCursor.java"

# interfaces
.implements Lcom/squareup/shared/sql/SQLCursor;


# static fields
.field private static final orderedLibraryTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final abbreviationColumnIndex:I

.field private final categoryIdIndex:I

.field private final colorColumnIndex:I

.field private final cursor:Lcom/squareup/shared/sql/SQLCursor;

.field private final defaultVariationIdColumnIndex:I

.field private final discountPercentageColumnIndex:I

.field private final discountTypeNameColumnIndex:I

.field private final durationColumnIndex:I

.field private final imageIdColumnIndex:I

.field private final imageUrlColumnIndex:I

.field private final itemTypeColumnIndex:I

.field private final measurementUnitBlobColumnIndex:I

.field private final nameColumnIndex:I

.field private final namingMethodColumnIndex:I

.field private final objectIdColumnIndex:I

.field private final objectTypeColumnIndex:I

.field private final ordinalColumnIndex:I

.field private final priceAmountColumnIndex:I

.field private final priceCurrencyColumnIndex:I

.field private final searchFilter:Ljava/lang/String;

.field private final variationsCountColumnIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 65
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DINING_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 66
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 65
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->orderedLibraryTypes:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 1

    .line 104
    iget-object v0, p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->searchFilter:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/sql/SQLCursor;Ljava/lang/String;)V
    .locals 0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    .line 109
    iput-object p2, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->searchFilter:Ljava/lang/String;

    const-string p1, "object_id"

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectIdColumnIndex:I

    const-string p1, "object_type"

    .line 112
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeColumnIndex:I

    const-string p1, "name"

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->nameColumnIndex:I

    const-string p1, "image_id"

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->imageIdColumnIndex:I

    const-string p1, "image_url"

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->imageUrlColumnIndex:I

    const-string/jumbo p1, "variations"

    .line 116
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->variationsCountColumnIndex:I

    const-string p1, "price_amount"

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->priceAmountColumnIndex:I

    const-string p1, "price_currency"

    .line 118
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->priceCurrencyColumnIndex:I

    const-string p1, "duration"

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->durationColumnIndex:I

    const-string p1, "discount_percentage"

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->discountPercentageColumnIndex:I

    const-string p1, "discount_type"

    .line 121
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->discountTypeNameColumnIndex:I

    const-string p1, "abbreviation"

    .line 122
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->abbreviationColumnIndex:I

    const-string p1, "color"

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->colorColumnIndex:I

    const-string p1, "default_variation"

    .line 124
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->defaultVariationIdColumnIndex:I

    const-string p1, "item_type"

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->itemTypeColumnIndex:I

    const-string p1, "category_id"

    .line 126
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->categoryIdIndex:I

    const-string p1, "ordinal"

    .line 127
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->ordinalColumnIndex:I

    const-string p1, "naming_method"

    .line 128
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->namingMethodColumnIndex:I

    const-string p1, "connectv2_object_blob"

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->measurementUnitBlobColumnIndex:I

    return-void
.end method

.method private getCatalogMeasurementUnitOrNull()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 3

    .line 265
    iget v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->measurementUnitBlobColumnIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 266
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->measurementUnitBlobColumnIndex:I

    .line 267
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 269
    :try_start_0
    new-instance v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    sget-object v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 270
    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 272
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method private getStringOrNull(I)Ljava/lang/String;
    .locals 1

    .line 255
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;
    .locals 2

    .line 57
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->orderedLibraryTypes:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 61
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " not supported in library."

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-void
.end method

.method public getBlob(I)[B
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object p1

    return-object p1
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getCount()I
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1

    return p1
.end method

.method public getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 21

    move-object/from16 v0, p0

    .line 137
    iget v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectIdColumnIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 138
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->orderedLibraryTypes:Ljava/util/List;

    iget v3, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeColumnIndex:I

    .line 139
    invoke-virtual {v0, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 140
    iget v3, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->nameColumnIndex:I

    invoke-virtual {v0, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 141
    iget v4, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->imageIdColumnIndex:I

    invoke-direct {v0, v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v4

    .line 142
    iget v5, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->imageUrlColumnIndex:I

    invoke-direct {v0, v5}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v5

    .line 144
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->variationsCountColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v6

    const/4 v7, -0x1

    if-eqz v6, :cond_0

    const/4 v8, -0x1

    goto :goto_0

    :cond_0
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->variationsCountColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result v6

    move v8, v6

    .line 145
    :goto_0
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->durationColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    const-wide/16 v9, 0x0

    goto :goto_1

    :cond_1
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->durationColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLong(I)J

    move-result-wide v9

    .line 146
    :goto_1
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->ordinalColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_2

    :cond_2
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->ordinalColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result v7

    .line 148
    :goto_2
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->categoryIdIndex:I

    invoke-direct {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v14

    .line 150
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->priceAmountColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x0

    goto :goto_3

    :cond_3
    iget v6, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->priceAmountColumnIndex:I

    invoke-virtual {v0, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :goto_3
    if-eqz v6, :cond_4

    .line 155
    iget v12, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->priceCurrencyColumnIndex:I

    invoke-virtual {v0, v12}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 156
    new-instance v13, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v13}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    invoke-virtual {v13, v6}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v6

    .line 157
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-static {v12}, Lcom/squareup/protos/common/CurrencyCode;->fromValue(I)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v12

    const-string v13, "currencyCode"

    invoke-static {v12, v13}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v6, v12}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v6

    .line 158
    invoke-virtual {v6}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v6

    goto :goto_4

    :cond_4
    const/4 v6, 0x0

    .line 160
    :goto_4
    iget v12, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->discountPercentageColumnIndex:I

    invoke-direct {v0, v12}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v12

    .line 162
    iget v13, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->discountTypeNameColumnIndex:I

    invoke-virtual {v0, v13}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v13

    if-eqz v13, :cond_5

    sget-object v13, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    goto :goto_5

    .line 163
    :cond_5
    iget v13, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->discountTypeNameColumnIndex:I

    invoke-virtual {v0, v13}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result v13

    invoke-static {v13}, Lcom/squareup/api/items/Discount$DiscountType;->fromValue(I)Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v13

    .line 164
    :goto_5
    iget v15, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->abbreviationColumnIndex:I

    invoke-direct {v0, v15}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v15

    .line 165
    iget v11, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->colorColumnIndex:I

    invoke-direct {v0, v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v16, v14

    .line 166
    iget v14, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->defaultVariationIdColumnIndex:I

    invoke-direct {v0, v14}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getStringOrNull(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v17, v14

    .line 167
    iget v14, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->namingMethodColumnIndex:I

    invoke-virtual {v0, v14}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 168
    sget-object v14, Lcom/squareup/api/items/TicketGroup;->DEFAULT_NAMING_METHOD:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    goto :goto_6

    .line 169
    :cond_6
    iget v14, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->namingMethodColumnIndex:I

    invoke-virtual {v0, v14}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Lcom/squareup/api/items/TicketGroup$NamingMethod;->fromValue(I)Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v14

    .line 170
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCatalogMeasurementUnitOrNull()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v18

    move-wide/from16 v19, v9

    .line 172
    sget-object v9, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v9, :cond_7

    move-object v4, v12

    move-object v5, v6

    move-object v6, v11

    move-object v7, v13

    .line 173
    invoke-static/range {v2 .. v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->forDiscount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    return-object v1

    .line 175
    :cond_7
    sget-object v9, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v9, :cond_8

    .line 176
    invoke-static {v2, v3, v15, v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->forCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    return-object v1

    .line 177
    :cond_8
    sget-object v9, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v9, :cond_9

    .line 178
    invoke-static {v2, v3, v8, v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->forModifierList(Ljava/lang/String;Ljava/lang/String;II)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    return-object v1

    .line 179
    :cond_9
    sget-object v9, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v9, :cond_a

    .line 180
    invoke-static {v2, v3, v8, v7, v14}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->forTicketGroup(Ljava/lang/String;Ljava/lang/String;IILcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    return-object v1

    .line 183
    :cond_a
    iget v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->itemTypeColumnIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v13, 0x0

    goto :goto_7

    :cond_b
    iget v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->itemTypeColumnIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/squareup/api/items/Item$Type;->fromValue(I)Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    move-object v13, v1

    :goto_7
    move v7, v8

    move-wide/from16 v8, v19

    move-object/from16 v10, v17

    move-object v1, v11

    move-object v11, v15

    move-object v12, v1

    move-object/from16 v14, v16

    move-object/from16 v15, v18

    .line 184
    invoke-static/range {v2 .. v15}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->forItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    return-object v1
.end method

.method public getLong(I)J
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPosition()I
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->getPosition()I

    move-result v0

    return v0
.end method

.method public getSearchFilter()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->searchFilter:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hasSearchFilter()Z
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->searchFilter:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isClosed()Z
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result p1

    return p1
.end method

.method public varargs mergeWithCursors([Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->mergeWithCursors([Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public moveToFirst()Z
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->cursor:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToPosition(I)Z

    move-result p1

    return p1
.end method
