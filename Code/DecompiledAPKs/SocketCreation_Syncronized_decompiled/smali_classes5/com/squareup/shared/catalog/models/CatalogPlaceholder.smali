.class public final Lcom/squareup/shared/catalog/models/CatalogPlaceholder;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogPlaceholder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Placeholder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 25
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Placeholder;

    iget-object v0, v0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceholderType()Lcom/squareup/api/items/Placeholder$PlaceholderType;
    .locals 2

    .line 29
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Placeholder;

    iget-object v0, v0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    sget-object v1, Lcom/squareup/api/items/Placeholder;->DEFAULT_PLACEHOLDER_TYPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object v0
.end method
