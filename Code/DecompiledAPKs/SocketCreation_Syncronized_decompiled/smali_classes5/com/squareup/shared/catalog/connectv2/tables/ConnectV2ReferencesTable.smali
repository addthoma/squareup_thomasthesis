.class public Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;
.super Ljava/lang/Object;
.source "ConnectV2ReferencesTable.java"


# static fields
.field private static INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;

    .line 12
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ReferencesTable;

    return-object v0
.end method


# virtual methods
.method public deleteAllReferences(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public write(Lcom/squareup/shared/sql/SQLDatabase;JLjava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method
