.class public Lcom/squareup/shared/catalog/Storage;
.super Ljava/lang/Object;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/Storage$Locations;
    }
.end annotation


# static fields
.field static final VERSION:J = 0x11L


# instance fields
.field private final catalogStoreFactory:Lcom/squareup/shared/catalog/CatalogStore$Factory;

.field private final locations:Lcom/squareup/shared/catalog/Storage$Locations;

.field private final metadata:Lcom/squareup/shared/catalog/StorageMetadata;

.field private final storageMetadataFile:Lcom/squareup/shared/catalog/CatalogFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogFile<",
            "Lcom/squareup/shared/catalog/StorageMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

.field private userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/CatalogStore$Factory;Lcom/squareup/shared/catalog/CatalogFile;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Ljava/io/File;Lcom/squareup/shared/catalog/CatalogEndpoint;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogStore$Factory;",
            "Lcom/squareup/shared/catalog/CatalogFile<",
            "Lcom/squareup/shared/catalog/StorageMetadata;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;",
            "Ljava/io/File;",
            "Lcom/squareup/shared/catalog/CatalogEndpoint;",
            ")V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/shared/catalog/Storage;->catalogStoreFactory:Lcom/squareup/shared/catalog/CatalogStore$Factory;

    .line 52
    iput-object p2, p0, Lcom/squareup/shared/catalog/Storage;->storageMetadataFile:Lcom/squareup/shared/catalog/CatalogFile;

    .line 53
    iput-object p3, p0, Lcom/squareup/shared/catalog/Storage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    .line 54
    new-instance p1, Lcom/squareup/shared/catalog/StorageMetadata;

    invoke-virtual {p5}, Lcom/squareup/shared/catalog/CatalogEndpoint;->getSupportedFeatures()Ljava/util/List;

    move-result-object p2

    const-wide/16 v0, 0x11

    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/shared/catalog/StorageMetadata;-><init>(JLjava/util/List;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/Storage;->metadata:Lcom/squareup/shared/catalog/StorageMetadata;

    .line 56
    new-instance p1, Ljava/io/File;

    invoke-virtual {p5}, Lcom/squareup/shared/catalog/CatalogEndpoint;->getDatabasePath()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p4, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 57
    new-instance p2, Ljava/io/File;

    const-string p3, "objects"

    invoke-direct {p2, p1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 58
    new-instance p3, Ljava/io/File;

    const-string p4, "storage_metadata"

    invoke-direct {p3, p1, p4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 59
    new-instance p4, Lcom/squareup/shared/catalog/Storage$Locations;

    const/4 p5, 0x0

    invoke-direct {p4, p1, p2, p3, p5}, Lcom/squareup/shared/catalog/Storage$Locations;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Lcom/squareup/shared/catalog/Storage$1;)V

    iput-object p4, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    return-void
.end method

.method private static delete(Ljava/io/File;)Z
    .locals 1

    const-string v0, "Cannot delete a null file."

    .line 219
    invoke-static {p0, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 220
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static deleteDir(Ljava/io/File;)V
    .locals 5

    .line 164
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 170
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 171
    invoke-static {v3}, Lcom/squareup/shared/catalog/Storage;->deleteDir(Ljava/io/File;)V

    goto :goto_1

    .line 173
    :cond_0
    invoke-static {v3}, Lcom/squareup/shared/catalog/Storage;->delete(Ljava/io/File;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 176
    :cond_1
    invoke-static {p0}, Lcom/squareup/shared/catalog/Storage;->delete(Ljava/io/File;)Z

    return-void

    .line 165
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private enforceOnFileThread()V
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    const-string v1, "All storage operations must be executed on the file thread."

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceFileThread(Ljava/lang/String;)V

    return-void
.end method

.method private isClosed()Z
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private makeDirectory(Ljava/io/File;)V
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    const-string v1, "Makes directory on the file thread."

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceFileThread(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 232
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 233
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    return-void

    .line 236
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " exists but is not a directory."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method catalogStore()Lcom/squareup/shared/catalog/CatalogStore;
    .locals 2

    .line 200
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    return-object v0

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Catalog store is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method close()V
    .locals 3

    .line 121
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->enforceOnFileThread()V

    const/4 v0, 0x0

    .line 123
    :try_start_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 127
    :try_start_1
    iget-object v1, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    invoke-interface {v1}, Lcom/squareup/shared/catalog/CatalogStore;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "There was an error closing the item store."

    .line 129
    invoke-virtual {p0, v2, v1}, Lcom/squareup/shared/catalog/Storage;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    :goto_0
    iput-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    return-void

    .line 124
    :cond_0
    :try_start_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Attempted to close Storage when it is not open."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v1

    .line 132
    iput-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    throw v1
.end method

.method getStorageDirForTests()Ljava/io/File;
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v0, v0, Lcom/squareup/shared/catalog/Storage$Locations;->storageDir:Ljava/io/File;

    return-object v0
.end method

.method initialize()V
    .locals 3

    .line 66
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->enforceOnFileThread()V

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->close()V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v0, v0, Lcom/squareup/shared/catalog/Storage$Locations;->storageDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/Storage;->makeDirectory(Ljava/io/File;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v0, v0, Lcom/squareup/shared/catalog/Storage$Locations;->objectsDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/Storage;->makeDirectory(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    .line 78
    :try_start_1
    iget-object v1, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v1, v1, Lcom/squareup/shared/catalog/Storage$Locations;->metadataFile:Ljava/io/File;

    .line 79
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    iget-object v2, p0, Lcom/squareup/shared/catalog/Storage;->storageMetadataFile:Lcom/squareup/shared/catalog/CatalogFile;

    invoke-interface {v2, v1}, Lcom/squareup/shared/catalog/CatalogFile;->readValue(Ljava/io/File;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/StorageMetadata;

    .line 81
    iget-object v2, p0, Lcom/squareup/shared/catalog/Storage;->metadata:Lcom/squareup/shared/catalog/StorageMetadata;

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/StorageMetadata;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    nop

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    :try_start_2
    const-string v0, "The storage directory version file doesn\'t exist or was written with an outdated version."

    .line 87
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/Storage;->resetStorage(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v0, v0, Lcom/squareup/shared/catalog/Storage$Locations;->objectsDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/Storage;->makeDirectory(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "There was an error initializing the storage directory or reading version."

    .line 92
    invoke-virtual {p0, v1, v0}, Lcom/squareup/shared/catalog/Storage;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_2
    :goto_1
    return-void
.end method

.method open()V
    .locals 2

    .line 103
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->enforceOnFileThread()V

    .line 104
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;

    if-nez v0, :cond_0

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->catalogStoreFactory:Lcom/squareup/shared/catalog/CatalogStore$Factory;

    iget-object v1, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v1, v1, Lcom/squareup/shared/catalog/Storage$Locations;->objectsDir:Ljava/io/File;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore$Factory;->open(Ljava/io/File;)Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/Storage;->userCatalogStore:Lcom/squareup/shared/catalog/CatalogStore;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "There was an error opening the item store."

    .line 110
    invoke-virtual {p0, v1, v0}, Lcom/squareup/shared/catalog/Storage;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V

    :goto_0
    return-void

    .line 105
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Storage is already open."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method purgeIfExists(Ljava/lang/String;)V
    .locals 2

    .line 138
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->enforceOnFileThread()V

    .line 141
    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v0, v0, Lcom/squareup/shared/catalog/Storage$Locations;->storageDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 145
    :cond_0
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/Storage;->close()V

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Catalog: Purging storage. %s"

    .line 149
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    iget-object p1, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object p1, p1, Lcom/squareup/shared/catalog/Storage$Locations;->storageDir:Ljava/io/File;

    invoke-static {p1}, Lcom/squareup/shared/catalog/Storage;->deleteDir(Ljava/io/File;)V

    return-void
.end method

.method resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    .line 181
    invoke-direct {p0}, Lcom/squareup/shared/catalog/Storage;->enforceOnFileThread()V

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Catalog: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 184
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/Storage;->resetStorage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Catalog: Error encountered while interacting with storage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 191
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method resetStorage(Ljava/lang/String;)V
    .locals 2

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/Storage;->purgeIfExists(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Catalog: Recreating storage due to reset. %s"

    .line 157
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    iget-object p1, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object p1, p1, Lcom/squareup/shared/catalog/Storage$Locations;->storageDir:Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/Storage;->makeDirectory(Ljava/io/File;)V

    .line 159
    iget-object p1, p0, Lcom/squareup/shared/catalog/Storage;->storageMetadataFile:Lcom/squareup/shared/catalog/CatalogFile;

    iget-object v0, p0, Lcom/squareup/shared/catalog/Storage;->locations:Lcom/squareup/shared/catalog/Storage$Locations;

    iget-object v0, v0, Lcom/squareup/shared/catalog/Storage$Locations;->metadataFile:Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/shared/catalog/Storage;->metadata:Lcom/squareup/shared/catalog/StorageMetadata;

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/CatalogFile;->writeValue(Ljava/io/File;Ljava/lang/Object;)V

    return-void
.end method
