.class public Lcom/squareup/shared/catalog/StorageMetadata;
.super Ljava/lang/Object;
.source "StorageMetadata.java"


# instance fields
.field private final supportedFeatures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field

.field private final version:J


# direct methods
.method public constructor <init>(JLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/squareup/shared/catalog/StorageMetadata;->version:J

    .line 23
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    return-void
.end method

.method static final synthetic lambda$equals$0$StorageMetadata(Lcom/squareup/api/sync/CatalogFeature;Lcom/squareup/api/sync/CatalogFeature;)I
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/api/sync/CatalogFeature;->getValue()I

    move-result p0

    invoke-virtual {p1}, Lcom/squareup/api/sync/CatalogFeature;->getValue()I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method

.method static final synthetic lambda$equals$1$StorageMetadata(Lcom/squareup/api/sync/CatalogFeature;Lcom/squareup/api/sync/CatalogFeature;)I
    .locals 0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/api/sync/CatalogFeature;->getValue()I

    move-result p0

    invoke-virtual {p1}, Lcom/squareup/api/sync/CatalogFeature;->getValue()I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 35
    instance-of v0, p1, Lcom/squareup/shared/catalog/StorageMetadata;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 39
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/StorageMetadata;

    .line 40
    iget-wide v2, p0, Lcom/squareup/shared/catalog/StorageMetadata;->version:J

    iget-wide v4, p1, Lcom/squareup/shared/catalog/StorageMetadata;->version:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    return v1

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v2, p1, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    if-nez v2, :cond_2

    goto :goto_0

    .line 48
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 49
    sget-object v0, Lcom/squareup/shared/catalog/StorageMetadata$$Lambda$0;->$instance:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    iget-object p1, p1, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 52
    sget-object p1, Lcom/squareup/shared/catalog/StorageMetadata$$Lambda$1;->$instance:Ljava/util/Comparator;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 54
    invoke-interface {v1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 45
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    if-ne v0, p1, :cond_4

    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public getSupportedFeatures()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/shared/catalog/StorageMetadata;->supportedFeatures:Ljava/util/List;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .line 27
    iget-wide v0, p0, Lcom/squareup/shared/catalog/StorageMetadata;->version:J

    return-wide v0
.end method
