.class final Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;
.super Ljava/lang/Object;
.source "CatalogSyncHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Requester"
.end annotation


# instance fields
.field private final batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

.field private final callback:Lcom/squareup/shared/catalog/sync/SyncCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private final httpThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Ljava/util/concurrent/Executor;

.field private final message:Lcom/squareup/api/rpc/RequestBatch;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;",
            ")V"
        }
    .end annotation

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    .line 220
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    .line 221
    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$1300(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;)Lcom/squareup/api/rpc/RequestBatch;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->message:Lcom/squareup/api/rpc/RequestBatch;

    .line 222
    invoke-static {p3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->access$1400(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Lcom/squareup/shared/catalog/CatalogEndpoint;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 223
    invoke-static {p3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->access$1500(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->mainThread:Ljava/util/concurrent/Executor;

    .line 224
    invoke-static {p3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->access$1600(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->fileThread:Ljava/util/concurrent/Executor;

    .line 225
    invoke-static {p3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->access$1700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->httpThread:Ljava/util/concurrent/Executor;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V
    .locals 0

    .line 209
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 230
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->message:Lcom/squareup/api/rpc/RequestBatch;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/CatalogEndpoint;->executeRequest(Lcom/squareup/api/rpc/RequestBatch;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    .line 232
    iget-object v1, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    iget-object v0, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {v1, v2, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-void

    .line 240
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 253
    :try_start_1
    new-instance v6, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    invoke-direct {v6, v0}, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->fileThread:Ljava/util/concurrent/Executor;

    iget-object v5, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->httpThread:Ljava/util/concurrent/Executor;

    iget-object v7, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    const/4 v8, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V

    .line 267
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->run()V

    return-void

    :catch_0
    move-exception v1

    .line 256
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 260
    :catch_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v2, v1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void

    .line 242
    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The response stream must not be null if there is no error or exception."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    .line 246
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v1, v2, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method
