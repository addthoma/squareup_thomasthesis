.class public final Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
.super Ljava/lang/Object;
.source "CatalogTax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogTax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final fee:Lcom/squareup/api/items/Fee$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 102
    new-instance v0, Lcom/squareup/api/items/Fee$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Fee$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 103
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$Builder;->adjustment_type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 104
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 105
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogTax;)V
    .locals 0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 111
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    invoke-virtual {p1}, Lcom/squareup/api/items/Fee;->newBuilder()Lcom/squareup/api/items/Fee$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    return-void
.end method


# virtual methods
.method public appliesToCustomAmounts()Z
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_APPLIES_TO_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public build()Lcom/squareup/shared/catalog/models/CatalogTax;
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee$Builder;->build()Lcom/squareup/api/items/Fee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee(Lcom/squareup/api/items/Fee;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 217
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public getFeeTypeId()Ljava/lang/String;
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->fee_type_id:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee$InclusionType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPercentage()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->percentage:Ljava/lang/String;

    return-object v0
.end method

.method public hasInclusionType()Z
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled()Z
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Fee$Builder;->enabled:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public setAppliesToCustomAmounts(Z)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->applies_to_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setAppliesToProductSet(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 2

    if-nez p1, :cond_0

    .line 173
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/Fee$Builder;->applies_to_product_set(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->applies_to_product_set(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setCalculationPhase(I)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-static {p1}, Lcom/squareup/api/items/CalculationPhase;->fromValue(I)Lcom/squareup/api/items/CalculationPhase;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setCalculationPhase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setFeeTypeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->fee_type_id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setFeeTypeName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->fee_type_name(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 117
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 118
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setInclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method

.method public setPercentage(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 3

    if-eqz p1, :cond_1

    .line 133
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Percentages;->validatePercentage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 134
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Percentage %s does not match the expected input format."

    .line 135
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->fee:Lcom/squareup/api/items/Fee$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Fee$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    return-object p0
.end method
