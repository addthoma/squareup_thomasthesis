.class public Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;
.super Ljava/lang/Object;
.source "CatalogLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/logging/CatalogLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Logger"
.end annotation


# static fields
.field private static logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs debug(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;

    if-eqz v0, :cond_0

    .line 35
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Lcom/squareup/shared/catalog/logging/CatalogLogger;->debug(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static varargs error(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger;->error(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static installLogger(Lcom/squareup/shared/catalog/logging/CatalogLogger;)V
    .locals 0

    .line 29
    sput-object p0, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;

    return-void
.end method

.method public static remoteLog(Ljava/lang/Throwable;)V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {v0, p0}, Lcom/squareup/shared/catalog/logging/CatalogLogger;->remoteLog(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public static remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;

    if-eqz v0, :cond_0

    .line 69
    invoke-interface {v0, p0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static varargs warn(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->logger:Lcom/squareup/shared/catalog/logging/CatalogLogger;

    if-eqz v0, :cond_0

    .line 42
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Lcom/squareup/shared/catalog/logging/CatalogLogger;->warn(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
