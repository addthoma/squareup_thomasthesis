.class public final Lcom/squareup/shared/catalog/synthetictables/LibraryTable;
.super Ljava/lang/Object;
.source "LibraryTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;,
        Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;
    }
.end annotation


# static fields
.field static final COLUMN_ABBREVIATION:Ljava/lang/String; = "abbreviation"

.field static final COLUMN_AVAILABLE_FOR_PICKUP:Ljava/lang/String; = "available_for_pickup"

.field static final COLUMN_CATEGORY_ID:Ljava/lang/String; = "category_id"

.field static final COLUMN_COLLATION_SECTION_INDEX:Ljava/lang/String; = "collation_section_index"

.field static final COLUMN_COLOR:Ljava/lang/String; = "color"

.field static final COLUMN_CURSOR_ADAPTER_ID:Ljava/lang/String; = "_id"

.field static final COLUMN_DEFAULT_VARIATION_ID:Ljava/lang/String; = "default_variation"

.field static final COLUMN_DISCOUNT_PERCENTAGE:Ljava/lang/String; = "discount_percentage"

.field static final COLUMN_DISCOUNT_TYPE:Ljava/lang/String; = "discount_type"

.field static final COLUMN_DURATION:Ljava/lang/String; = "duration"

.field static final COLUMN_IMAGE_ID:Ljava/lang/String; = "image_id"

.field static final COLUMN_IMAGE_URL:Ljava/lang/String; = "image_url"

.field static final COLUMN_ITEM_TYPE:Ljava/lang/String; = "item_type"

.field static final COLUMN_MEASUREMENT_UNIT_TOKEN:Ljava/lang/String; = "measurement_unit_token"

.field static final COLUMN_NAME:Ljava/lang/String; = "name"

.field static final COLUMN_NAMING_METHOD:Ljava/lang/String; = "naming_method"

.field static final COLUMN_OBJECT_ID:Ljava/lang/String; = "object_id"

.field static final COLUMN_OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field static final COLUMN_ORDINAL:Ljava/lang/String; = "ordinal"

.field static final COLUMN_PRICE_AMOUNT:Ljava/lang/String; = "price_amount"

.field static final COLUMN_PRICE_CURRENCY:Ljava/lang/String; = "price_currency"

.field static final COLUMN_SEARCH_WORDS:Ljava/lang/String; = "search_words"

.field static final COLUMN_VARIATIONS_COUNT:Ljava/lang/String; = "variations"

.field private static final IDX_COLLATION_SEARCH:Ljava/lang/String; = "idx_collation_search"

.field private static final IDX_SEARCH_WORDS:Ljava/lang/String; = "idx_search_words"

.field static final NAME:Ljava/lang/String; = "library"

.field private static final VERSION:J = 0x9L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private buildSearchWordsContent(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 891
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, ""

    .line 895
    :cond_0
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/StringUtils;->joinWordsWithLeadingWhitespace(Ljava/util/List;)Ljava/lang/String;

    move-result-object p2

    .line 896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private checkItemExists(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;
    .locals 3

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 913
    sget-object p2, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->READ_BY_ID_WITH_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 915
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    .line 924
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v1

    .line 918
    :cond_0
    :try_start_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    .line 919
    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result p2

    if-eqz p2, :cond_1

    move-object p2, v1

    goto :goto_0

    :cond_1
    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 920
    :goto_0
    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    const/4 v0, 0x2

    .line 921
    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 922
    new-instance v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;

    invoke-direct {v2, p2, v1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 924
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v2

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method private delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 685
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->DELETE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private insertItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 474
    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->insertOrUpdateItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private insertOrUpdateItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10

    .line 484
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 486
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v1

    .line 487
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/api/items/Item$Type;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 488
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->searchKeywords()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->buildSearchWordsContent(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 490
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAbbreviation()Ljava/lang/String;

    move-result-object v5

    .line 491
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v6

    .line 492
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getMenuCategoryId()Ljava/lang/String;

    move-result-object v7

    .line 493
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->isAvailableForPickup()Z

    move-result p2

    .line 494
    invoke-static {v3}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->collationSectionIndex(Ljava/lang/String;)I

    move-result v8

    if-eqz p5, :cond_0

    .line 498
    sget-object v9, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->INSERT_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v9}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v9

    invoke-static {p1, v9}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v9

    goto :goto_0

    .line 500
    :cond_0
    sget-object v9, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v9}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v9

    invoke-static {p1, v9}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v9

    :goto_0
    if-eqz p5, :cond_1

    .line 504
    invoke-virtual {v9, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    .line 506
    :cond_1
    invoke-virtual {v9, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 507
    invoke-virtual {v1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 508
    invoke-virtual {v1, v3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 509
    invoke-virtual {v1, v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 510
    invoke-virtual {v1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p3

    .line 511
    invoke-virtual {p3, p4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p3

    .line 512
    invoke-virtual {p3, v5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p3

    .line 513
    invoke-virtual {p3, v6}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p3

    .line 514
    invoke-virtual {p3, v7}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p3

    .line 515
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 516
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    if-nez p5, :cond_2

    .line 518
    invoke-virtual {v9, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    .line 521
    :cond_2
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 523
    :try_start_0
    invoke-virtual {v9}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    .line 524
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2
.end method

.method private readImageUrlFromCacheOrDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 875
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    if-nez p2, :cond_0

    .line 878
    const-class p2, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    invoke-static {p1, p2, p3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readByIdOrNull(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 883
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getUrl()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private updateItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 478
    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->insertOrUpdateItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private updateLibraryCategories(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;)V"
        }
    .end annotation

    .line 807
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 808
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->writeCategory(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateLibraryDiscounts(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;)V"
        }
    .end annotation

    .line 814
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 816
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isComp()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 819
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->writeDiscount(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateLibraryImages(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
            ">;)V"
        }
    .end annotation

    .line 690
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    .line 691
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->writeImage(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateLibraryItemModifierLists(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            ">;)V"
        }
    .end annotation

    .line 825
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 826
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->writeItemModifierList(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemModifierList;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateLibraryItemModifierOptions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;Z)V"
        }
    .end annotation

    .line 834
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 835
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 836
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz p3, :cond_0

    .line 840
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    .line 842
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1, v0, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->countReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)I

    move-result v0

    :goto_1
    int-to-long v2, v0

    .line 845
    invoke-direct {p0, p1, v1, v2, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryModifierOptions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;J)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateLibraryItemVariations(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;JLjava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 657
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_ITEM_VARIATIONS:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 658
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 659
    invoke-virtual {p1, p5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 660
    invoke-virtual {p1, p6}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 661
    invoke-virtual {p1, p7}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 662
    invoke-virtual {p1, p8}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 663
    invoke-virtual {p1, p9}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 664
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 665
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private updateLibraryItemVariations(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;>;Z)V"
        }
    .end annotation

    .line 750
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 751
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/lang/String;

    .line 752
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 754
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 760
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 761
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {p1, v1, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->findReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 762
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_2

    move-object v0, v1

    goto :goto_1

    :cond_0
    if-eqz p3, :cond_1

    .line 768
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_1

    .line 773
    :cond_1
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1, v1, v4}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->countReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)I

    move-result v3

    :cond_2
    :goto_1
    const/4 v1, 0x0

    if-ne v3, v2, :cond_4

    const/4 v2, 0x0

    .line 781
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 782
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v2

    .line 783
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v5

    .line 784
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getDuration()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 785
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v0, v1

    goto :goto_2

    .line 789
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 790
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 791
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    move-object v8, v0

    move-object v7, v1

    move-object v10, v2

    move-object v11, v5

    move-object v9, v6

    goto :goto_3

    :cond_4
    move-object v7, v1

    move-object v8, v7

    move-object v9, v8

    move-object v10, v9

    move-object v11, v10

    :goto_3
    int-to-long v5, v3

    move-object v2, p0

    move-object v3, p1

    .line 800
    invoke-direct/range {v2 .. v11}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItemVariations(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;JLjava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    return-void
.end method

.method private updateLibraryItems(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;>;Z)V"
        }
    .end annotation

    .line 699
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 700
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 701
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz p5, :cond_0

    move-object v4, v3

    goto :goto_1

    .line 702
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->checkItemExists(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;

    move-result-object v4

    :goto_1
    if-eqz v4, :cond_3

    .line 708
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 709
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getImageId()Ljava/lang/String;

    move-result-object v3

    .line 710
    iget-object v2, v4, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;->imageId:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 711
    iget-object v2, v4, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;->imageUrl:Ljava/lang/String;

    goto :goto_2

    .line 713
    :cond_1
    invoke-direct {p0, p1, p3, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->readImageUrlFromCacheOrDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    move-object v2, v3

    .line 719
    :goto_2
    invoke-direct {p0, p1, v1, v3, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 724
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 725
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getImageId()Ljava/lang/String;

    move-result-object v3

    .line 726
    invoke-direct {p0, p1, p3, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->readImageUrlFromCacheOrDb(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_4
    move-object v4, v3

    .line 731
    :goto_3
    invoke-interface {p4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 734
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v0, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 737
    :cond_5
    invoke-direct {p0, p1, v1, v3, v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->insertItem(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 741
    :cond_6
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_7

    .line 742
    invoke-direct {p0, p1, v0, p5}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItemVariations(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    :cond_7
    return-void
.end method

.method private updateLibraryModifierOptions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;J)V
    .locals 1

    .line 670
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_VARIATION_COUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 671
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 672
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 673
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private updateLibraryTicketGroupTemplateCount(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;J)V
    .locals 1

    .line 678
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_VARIATION_COUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 679
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 680
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 681
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private updateLibraryTicketGroupTemplateCounts(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;Z)V"
        }
    .end annotation

    .line 861
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 862
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 863
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez p3, :cond_0

    .line 866
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1, v0, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->countReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)I

    move-result v0

    :cond_0
    int-to-long v2, v0

    .line 868
    invoke-direct {p0, p1, v1, v2, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryTicketGroupTemplateCount(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;J)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateLibraryTicketGroups(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogTicketGroup;",
            ">;)V"
        }
    .end annotation

    .line 851
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    .line 852
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->writeTicketGroup(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogTicketGroup;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private writeCategory(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V
    .locals 5

    .line 531
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    .line 532
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->searchKeywords()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->buildSearchWordsContent(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 533
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->collationSectionIndex(Ljava/lang/String;)I

    move-result v2

    .line 536
    sget-object v3, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 537
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v3

    .line 538
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v3

    .line 539
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v3

    .line 540
    invoke-virtual {v3, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 541
    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 542
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 543
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getAbbreviation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 544
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getColor()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 545
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 547
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 549
    :try_start_0
    invoke-virtual {p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    .line 550
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 552
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2
.end method

.method private writeDiscount(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 9

    .line 557
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v2, v1

    goto :goto_0

    .line 558
    :cond_0
    iget-object v2, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    .line 559
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 561
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPercentage()Ljava/lang/String;

    move-result-object v0

    .line 562
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v3

    .line 565
    sget-object v4, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-ne v3, v4, :cond_2

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    const-string v0, "0"

    .line 569
    :cond_2
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v4

    .line 570
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->searchKeywords()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->buildSearchWordsContent(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    .line 571
    invoke-static {v4}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->collationSectionIndex(Ljava/lang/String;)I

    move-result v6

    .line 574
    sget-object v7, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_DISCOUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 575
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v7

    .line 576
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v7

    .line 577
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v8

    invoke-static {v8}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v7

    .line 578
    invoke-virtual {v7, v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 579
    invoke-virtual {v4, v5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 580
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getColor()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 581
    invoke-virtual {p2, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 582
    invoke-virtual {p2, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 583
    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 584
    invoke-virtual {v3}, Lcom/squareup/api/items/Discount$DiscountType;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 585
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 587
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 589
    :try_start_0
    invoke-virtual {p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    .line 590
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2
.end method

.method private writeImage(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 648
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->WRITE_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 649
    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 650
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 651
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private writeItemModifierList(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemModifierList;)V
    .locals 6

    .line 598
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v0

    .line 599
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getOrdinal()I

    move-result v1

    .line 600
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->searchKeywords()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->buildSearchWordsContent(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 601
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 602
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->countReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)I

    move-result v3

    .line 605
    sget-object v4, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 606
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 607
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 608
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 609
    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 610
    invoke-virtual {p2, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 611
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 612
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 614
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 616
    :try_start_0
    invoke-virtual {p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    .line 617
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2
.end method

.method private writeTicketGroup(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogTicketGroup;)V
    .locals 3

    .line 624
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 625
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->countReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;)I

    move-result v0

    .line 628
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_TICKET_GROUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 629
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 630
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 631
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->objectTypeToOrderedLibraryType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 632
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 633
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 634
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 635
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/TicketGroup$NamingMethod;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 636
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getSortText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 638
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 640
    :try_start_0
    invoke-virtual {p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    .line 641
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2
.end method


# virtual methods
.method public applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V
    .locals 5

    .line 416
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 418
    invoke-direct {p0, p1, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_0

    .line 422
    :cond_0
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 423
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 424
    iget-object v2, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 425
    iget-object v4, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 427
    iget-object v4, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 428
    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 431
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 434
    :cond_2
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItemVariations(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    .line 437
    :cond_3
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedCategoryIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 438
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedCategoryIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 439
    invoke-direct {p0, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_2

    .line 443
    :cond_4
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedDiscountIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 444
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedDiscountIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 445
    invoke-direct {p0, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_3

    .line 449
    :cond_5
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierListIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 450
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierListIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 451
    invoke-direct {p0, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_4

    .line 455
    :cond_6
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierOptionIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 456
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierOptionIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 457
    invoke-direct {p0, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_5

    .line 461
    :cond_7
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTicketGroupIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 462
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTicketGroupIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 463
    invoke-direct {p0, p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->delete(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_6

    .line 467
    :cond_8
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 468
    iget-object p2, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    invoke-direct {p0, p1, p2, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryTicketGroupTemplateCounts(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    :cond_9
    return-void
.end method

.method public applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
    .locals 6

    if-eqz p4, :cond_0

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 373
    :goto_0
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedImagesById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_1

    .line 374
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedImagesById:Ljava/util/Map;

    invoke-direct {p0, p1, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryImages(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V

    .line 377
    :cond_1
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemsById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_2

    .line 378
    iget-object v2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemsById:Ljava/util/Map;

    iget-object v3, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedImagesById:Ljava/util/Map;

    iget-object v4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItems(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)V

    .line 383
    :cond_2
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_3

    .line 384
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-direct {p0, p1, p4, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItemVariations(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    .line 388
    :cond_3
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedCategoriesById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_4

    .line 389
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedCategoriesById:Ljava/util/Map;

    invoke-direct {p0, p1, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryCategories(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V

    .line 392
    :cond_4
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedDiscountsById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_5

    .line 393
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedDiscountsById:Ljava/util/Map;

    invoke-direct {p0, p1, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryDiscounts(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V

    .line 396
    :cond_5
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemModifierListsById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_6

    .line 397
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemModifierListsById:Ljava/util/Map;

    invoke-direct {p0, p1, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItemModifierLists(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V

    .line 400
    :cond_6
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemModifierOptionsByModifierListId:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_7

    .line 401
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemModifierOptionsByModifierListId:Ljava/util/Map;

    invoke-direct {p0, p1, p4, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryItemModifierOptions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    .line 405
    :cond_7
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTicketGroupsById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_8

    .line 406
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTicketGroupsById:Ljava/util/Map;

    invoke-direct {p0, p1, p4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryTicketGroups(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;)V

    .line 409
    :cond_8
    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_9

    .line 410
    iget-object p2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTicketTemplatesByTicketGroupId:Ljava/util/Map;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;->updateLibraryTicketGroupTemplateCounts(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Map;Z)V

    :cond_9
    return-void
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 362
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 363
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_COLLATION_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 364
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 365
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_IMAGE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 366
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_NAME_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 367
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_TYPE_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public tableName()Ljava/lang/String;
    .locals 1

    const-string v0, "library"

    return-object v0
.end method

.method public tableVersion()J
    .locals 2

    const-wide/16 v0, 0x9

    return-wide v0
.end method
