.class public final Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemPageMembership.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemPageMembership$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ItemPageMembership;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static buildTileRelation(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    invoke-static {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object p0

    return-object p0
.end method

.method public static coordinatesToPosition(III)I
    .locals 0

    mul-int p1, p1, p2

    add-int/2addr p1, p0

    return p1
.end method

.method private refTile()Lcom/squareup/shared/catalog/models/CatalogRelation;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->buildTileRelation(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getColumn()Ljava/lang/Integer;
    .locals 1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->column:Ljava/lang/Integer;

    return-object v0
.end method

.method public getColumnIndex(I)I
    .locals 1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getPosition()I

    move-result v0

    rem-int/2addr v0, p1

    return v0
.end method

.method public getHeight()I
    .locals 2

    .line 71
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->height:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemPageMembership;->DEFAULT_HEIGHT:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPageId()Ljava/lang/String;
    .locals 2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->page:Lcom/squareup/api/sync/ObjectId;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->page:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 2

    .line 67
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->position:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemPageMembership;->DEFAULT_POSITION:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 107
    sget-object v1, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v1, v1, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 101
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_PAGE_MEMBERSHIP_PAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v2, v2, Lcom/squareup/api/items/ItemPageMembership;->page:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-direct {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->refTile()Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v2, v2, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getRow()Ljava/lang/Integer;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->row:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRowIndex(I)I
    .locals 1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getPosition()I

    move-result v0

    div-int/2addr v0, p1

    return v0
.end method

.method public getTileObjectStringId()Ljava/lang/String;
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTileObjectType()Lcom/squareup/api/items/Type;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-nez v0, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 2

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v0, v0, Lcom/squareup/api/items/ItemPageMembership;->width:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemPageMembership;->DEFAULT_WIDTH:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
