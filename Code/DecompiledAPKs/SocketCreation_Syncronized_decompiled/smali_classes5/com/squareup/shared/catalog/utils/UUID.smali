.class public Lcom/squareup/shared/catalog/utils/UUID;
.super Ljava/lang/Object;
.source "UUID.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateId()Ljava/lang/String;
    .locals 7

    .line 20
    :try_start_0
    const-class v0, Lcom/squareup/shared/catalog/utils/UUID;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    .line 21
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 22
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "generateUUIDInJava"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x0

    new-array v1, v2, [Ljava/lang/Object;

    .line 23
    invoke-virtual {v4, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 30
    :cond_1
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateUUIDInObjc()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    .line 27
    new-instance v1, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static generateUUIDInJava()Ljava/lang/String;
    .locals 1

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native generateUUIDInObjc()Ljava/lang/String;
.end method
