.class Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;
.super Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$LocalSyncCallback;
.source "CatalogSyncHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->lambda$run$2$CatalogSyncHandler$ResponseHandler(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

.field final synthetic val$nextResponseHandler:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->val$nextResponseHandler:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$LocalSyncCallback;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 149
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 151
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$900(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$800(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1$$Lambda$0;->get$Lambda(Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$1100(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$1000(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;IZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$400(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1$$Lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 157
    :cond_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$900(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->val$nextResponseHandler:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method final synthetic lambda$call$0$CatalogSyncHandler$ResponseHandler$1(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$500(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$600(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-void
.end method
