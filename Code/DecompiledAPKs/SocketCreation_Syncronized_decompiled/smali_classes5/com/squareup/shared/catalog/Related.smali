.class public final Lcom/squareup/shared/catalog/Related;
.super Ljava/lang/Object;
.source "Related.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/shared/catalog/models/CatalogObject;",
        "J:",
        "Lcom/squareup/shared/catalog/models/CatalogObject;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final object:Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final related:Z

.field public final relation:Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TJ;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogObject;ZLcom/squareup/shared/catalog/models/CatalogObject;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ZTJ;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 42
    iput-boolean p2, p0, Lcom/squareup/shared/catalog/Related;->related:Z

    .line 43
    iput-object p3, p0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 47
    instance-of v1, p1, Lcom/squareup/shared/catalog/Related;

    if-nez v1, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/Related;

    .line 49
    iget-boolean v1, p0, Lcom/squareup/shared/catalog/Related;->related:Z

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    iget-object v2, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 50
    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    iget-object p1, p1, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 51
    invoke-static {v1, p1}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 55
    iget-object v1, p0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 62
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", related="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
