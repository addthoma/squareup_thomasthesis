.class public final Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogMeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 26
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    const-string v0, "Measurement unit data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 42
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-object p0
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object v0
.end method

.method public getPrecision()I
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->precision:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->DEFAULT_PRECISION:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
