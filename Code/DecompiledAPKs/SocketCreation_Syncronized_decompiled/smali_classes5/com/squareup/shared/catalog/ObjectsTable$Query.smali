.class public final enum Lcom/squareup/shared/catalog/ObjectsTable$Query;
.super Ljava/lang/Enum;
.source "ObjectsTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/ObjectsTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/ObjectsTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum COUNT_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum COUNT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum CREATE_OBJECT_AND_ITEM_TYPE_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum CREATE_TOKEN_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum DELETE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_IDS_FOR_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_IDS_FOR_OBJECTS_OF_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_OBJECT_BY_TOKEN:Lcom/squareup/shared/catalog/ObjectsTable$Query;

.field public static final enum SELECT_TOKENS_AND_OBJECTS_BY_TOKENS:Lcom/squareup/shared/catalog/ObjectsTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 54
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "CREATE TABLE {table} ({object_id} TEXT PRIMARY KEY, {object_token} TEXT,{object_type} INTEGER, {item_type} INTEGER, {sort_text} TEXT, {object_blob} BLOB)"

    .line 55
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "objects"

    const-string v3, "table"

    .line 57
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "object_id"

    .line 58
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "object_token"

    .line 59
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "object_type"

    .line 60
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "item_type"

    .line 61
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "sort_text"

    .line 62
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v9, "object_blob"

    .line 63
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 65
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x0

    const-string v11, "CREATE"

    invoke-direct {v0, v11, v10, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 67
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table}({object_type}, {item_type})"

    .line 68
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v11, "idx"

    const-string v12, "idx_object_and_item_type"

    .line 69
    invoke-virtual {v1, v11, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 72
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 74
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x1

    const-string v13, "CREATE_OBJECT_AND_ITEM_TYPE_INDEX"

    invoke-direct {v0, v13, v12, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_OBJECT_AND_ITEM_TYPE_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 76
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({object_type}, {sort_text} COLLATE NOCASE ASC)"

    .line 77
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "idx_object_type_and_sort_text"

    .line 79
    invoke-virtual {v1, v11, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 84
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v13, 0x2

    const-string v14, "CREATE_TYPE_AND_SORT_TEXT_INDEX"

    invoke-direct {v0, v14, v13, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 86
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({object_token})"

    .line 87
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v14, "idx_object_token"

    .line 88
    invoke-virtual {v1, v11, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 89
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 92
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x3

    const-string v14, "CREATE_TOKEN_INDEX"

    invoke-direct {v0, v14, v11, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_TOKEN_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 95
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT count(*) FROM {table} WHERE {object_type} = \'{item}\' AND {item_type} IN (%item_types%)"

    .line 96
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 98
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v14, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 100
    invoke-virtual {v14}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoTypeValue()I

    move-result v14

    const-string v15, "item"

    invoke-virtual {v1, v15, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;I)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 101
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 103
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v14, 0x4

    const-string v15, "COUNT_ITEMS_WITH_TYPES"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->COUNT_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 105
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT count(*) FROM {table} WHERE {object_type} = ?"

    .line 106
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 107
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 108
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 109
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 110
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v15, 0x5

    const-string v14, "COUNT_OBJECTS_BY_TYPE"

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->COUNT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 112
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "DELETE from {table} WHERE {object_id} = ?"

    .line 113
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 114
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 115
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 117
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "DELETE"

    const/4 v15, 0x6

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->DELETE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 119
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({object_id}, {object_token}, {object_type}, {item_type}, {sort_text}, {object_blob}) VALUES (?, ?, ?, ?, ?, ?)"

    .line 120
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 122
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 123
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 124
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 125
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 126
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 127
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 128
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 130
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "INSERT_OR_REPLACE"

    const/4 v15, 0x7

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 132
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_blob} FROM {table} WHERE {object_type} = ? ORDER BY {sort_text} COLLATE NOCASE ASC"

    .line 133
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 135
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 136
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 139
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 140
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "SELECT_OBJECTS_BY_TYPE"

    const/16 v14, 0x8

    invoke-direct {v0, v8, v14, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 142
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_blob} FROM {table} WHERE {object_id} = ?"

    .line 143
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 145
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 146
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 147
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 149
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "SELECT_OBJECT_BY_ID"

    const/16 v14, 0x9

    invoke-direct {v0, v8, v14, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 151
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_blob} FROM {table} WHERE {object_token} = ?"

    .line 152
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 155
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 156
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 157
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 158
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "SELECT_OBJECT_BY_TOKEN"

    const/16 v14, 0xa

    invoke-direct {v0, v8, v14, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECT_BY_TOKEN:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 160
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_token}, {object_blob} FROM {table} WHERE {object_token} IN (%tokens%)"

    .line 161
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 163
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 164
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 165
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 166
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 167
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "SELECT_TOKENS_AND_OBJECTS_BY_TOKENS"

    const/16 v8, 0xb

    invoke-direct {v0, v5, v8, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_TOKENS_AND_OBJECTS_BY_TOKENS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 169
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_id}, {object_blob} FROM {table} WHERE {object_id} IN (%ids%)"

    .line 170
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 172
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 173
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 174
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 175
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 176
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "SELECT_IDS_AND_OBJECTS_BY_IDS"

    const/16 v8, 0xc

    invoke-direct {v0, v5, v8, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 178
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_id} FROM {table} WHERE {object_type} = ?       AND {item_type} IN (%item_types%) "

    .line 179
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 183
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 184
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 185
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 186
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 188
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "SELECT_IDS_FOR_ITEMS_WITH_TYPES"

    const/16 v7, 0xd

    invoke-direct {v0, v5, v7, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_FOR_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 190
    new-instance v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const-string v1, "SELECT {object_id} FROM {table} WHERE {object_type} = ?"

    .line 191
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 194
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 195
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 196
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 197
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 198
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SELECT_IDS_FOR_OBJECTS_OF_TYPE"

    const/16 v3, 0xe

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_FOR_OBJECTS_OF_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/shared/catalog/ObjectsTable$Query;

    .line 53
    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_OBJECT_AND_ITEM_TYPE_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->CREATE_TOKEN_INDEX:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->COUNT_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->COUNT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->DELETE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_OBJECT_BY_TOKEN:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_TOKENS_AND_OBJECTS_BY_TOKENS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_FOR_ITEMS_WITH_TYPES:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ObjectsTable$Query;->SELECT_IDS_FOR_OBJECTS_OF_TYPE:Lcom/squareup/shared/catalog/ObjectsTable$Query;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/ObjectsTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 202
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 203
    iput-object p3, p0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/ObjectsTable$Query;
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/ObjectsTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/ObjectsTable$Query;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/ObjectsTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/ObjectsTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/ObjectsTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/shared/catalog/ObjectsTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
