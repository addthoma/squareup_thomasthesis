.class Lcom/squareup/shared/catalog/CatalogLocal$2;
.super Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
.source "CatalogLocal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/CatalogLocal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
        "Lcom/squareup/shared/catalog/models/CatalogMenu;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;-><init>()V

    return-void
.end method


# virtual methods
.method getName(Lcom/squareup/shared/catalog/models/CatalogMenu;)Ljava/lang/String;
    .locals 0

    .line 100
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogMenu;->getSortText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method bridge synthetic getName(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogMenu;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$2;->getName(Lcom/squareup/shared/catalog/models/CatalogMenu;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getOrdinal(Lcom/squareup/shared/catalog/models/CatalogMenu;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method bridge synthetic getOrdinal(Lcom/squareup/shared/catalog/models/CatalogObject;)I
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogMenu;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$2;->getOrdinal(Lcom/squareup/shared/catalog/models/CatalogMenu;)I

    move-result p1

    return p1
.end method
