.class public Lcom/squareup/shared/catalog/CatalogStoreProvider;
.super Ljava/lang/Object;
.source "CatalogStoreProvider.java"


# instance fields
.field private final closeEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final storage:Lcom/squareup/shared/catalog/Storage;

.field private final threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/Storage;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;)V
    .locals 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 22
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->closeEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v0, "storage"

    .line 25
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/Storage;

    iput-object p1, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->storage:Lcom/squareup/shared/catalog/Storage;

    .line 26
    iput-object p2, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    return-void
.end method

.method private getStorage()Lcom/squareup/shared/catalog/Storage;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    const-string v1, "Cannot perform operation outside of the file I/O thread."

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceFileThread(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->initialize()V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->storage:Lcom/squareup/shared/catalog/Storage;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->storage:Lcom/squareup/shared/catalog/Storage;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/Storage;->initialize()V

    .line 88
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->storage:Lcom/squareup/shared/catalog/Storage;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/Storage;->open()V

    .line 90
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 42
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->getStorage()Lcom/squareup/shared/catalog/Storage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/Storage;->close()V

    return-void
.end method

.method public get()Lcom/squareup/shared/catalog/CatalogStore;
    .locals 1

    .line 34
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->getStorage()Lcom/squareup/shared/catalog/Storage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/Storage;->catalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    return-object v0
.end method

.method public isCloseEnqueued()Z
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->closeEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method isInitialized()Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public purgeIfExists(Ljava/lang/String;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->storage:Lcom/squareup/shared/catalog/Storage;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/Storage;->purgeIfExists(Ljava/lang/String;)V

    return-void
.end method

.method public resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 56
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->getStorage()Lcom/squareup/shared/catalog/Storage;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/shared/catalog/Storage;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method setCloseEnqueued()V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogStoreProvider;->closeEnqueued:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method
