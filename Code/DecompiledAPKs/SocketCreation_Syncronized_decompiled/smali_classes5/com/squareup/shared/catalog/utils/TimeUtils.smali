.class public Lcom/squareup/shared/catalog/utils/TimeUtils;
.super Ljava/lang/Object;
.source "TimeUtils.java"


# static fields
.field static final ISO_8601:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field static final RFC_3339:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/shared/catalog/utils/TimeUtils$1;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/utils/TimeUtils$1;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/utils/TimeUtils;->ISO_8601:Ljava/lang/ThreadLocal;

    .line 29
    new-instance v0, Lcom/squareup/shared/catalog/utils/TimeUtils$2;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/utils/TimeUtils$2;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/utils/TimeUtils;->RFC_3339:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asIso8601(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/shared/catalog/utils/TimeUtils;->ISO_8601:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/TimeUtils;->fixTimeZone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static asRfc3339(J)Ljava/lang/String;
    .locals 2

    .line 45
    sget-object v0, Lcom/squareup/shared/catalog/utils/TimeUtils;->RFC_3339:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/TimeUtils;->fixTimeZone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static fixTimeZone(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getElapsedTime(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/shared/catalog/utils/ElapsedTime;
    .locals 21

    if-eqz p0, :cond_8

    if-eqz p1, :cond_7

    .line 58
    invoke-virtual/range {p0 .. p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 62
    invoke-virtual/range {p1 .. p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual/range {p0 .. p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    .line 64
    div-long v2, v0, v2

    const-wide/16 v4, 0x3c

    .line 65
    div-long v6, v2, v4

    .line 66
    div-long v8, v6, v4

    const-wide/16 v10, 0x18

    .line 67
    div-long v12, v8, v10

    const-wide/16 v14, 0x7

    .line 68
    div-long v10, v12, v14

    const-wide/16 v16, 0x34

    .line 69
    div-long v14, v10, v16

    const-wide/16 v18, 0x0

    cmp-long v20, v2, v18

    if-nez v20, :cond_0

    .line 72
    new-instance v2, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v3, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MILLISECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v2

    :cond_0
    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 74
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->SECONDS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v0

    :cond_1
    cmp-long v0, v6, v4

    if-gez v0, :cond_2

    .line 76
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->MINUTES:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v0, v6, v7, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v0

    :cond_2
    const-wide/16 v0, 0x18

    cmp-long v2, v8, v0

    if-gez v2, :cond_3

    .line 78
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->HOURS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v0

    :cond_3
    const-wide/16 v0, 0x7

    cmp-long v2, v12, v0

    if-gez v2, :cond_4

    .line 80
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v0, v12, v13, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v0

    :cond_4
    cmp-long v0, v10, v16

    if-gez v0, :cond_5

    .line 82
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->WEEKS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v0, v10, v11, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v0

    .line 84
    :cond_5
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->YEARS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    return-object v0

    .line 59
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'From\' date is after \'to\' date."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_7
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "to"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_8
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "from"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "Z"

    const-string v1, "-0000"

    .line 106
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 109
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    .line 110
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3a

    if-ne v1, v2, :cond_0

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 112
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    .line 113
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 112
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 116
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/utils/TimeUtils;->ISO_8601:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;
    .locals 2

    .line 90
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 92
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/TimeUtils;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    return-object v1
.end method
