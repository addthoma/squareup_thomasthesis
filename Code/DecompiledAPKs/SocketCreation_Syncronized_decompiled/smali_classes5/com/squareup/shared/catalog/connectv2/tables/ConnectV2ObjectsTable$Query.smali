.class public final enum Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;
.super Ljava/lang/Enum;
.source "ConnectV2ObjectsTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum DELETE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum DELETE_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

.field public static final enum SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 41
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "CREATE TABLE {table} ({object_id} TEXT PRIMARY KEY,{object_type} INTEGER, {sort_text} TEXT, {object_blob} BLOB)"

    .line 42
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "connectv2_objects"

    const-string v3, "table"

    .line 44
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "connectv2_object_id"

    const-string v5, "object_id"

    .line 45
    invoke-virtual {v1, v5, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "connectv2_object_type"

    const-string v7, "object_type"

    .line 46
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "connectv2_sort_text"

    const-string v9, "sort_text"

    .line 47
    invoke-virtual {v1, v9, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "connectv2_object_blob"

    const-string v11, "object_blob"

    .line 48
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 50
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x0

    const-string v13, "CREATE"

    invoke-direct {v0, v13, v12, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->CREATE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 52
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({object_type}, {sort_text} COLLATE NOCASE ASC)"

    .line 53
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "idx"

    const-string v14, "idx_connectv2_object_type_and_sort_text"

    .line 55
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 56
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 57
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v9, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 60
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v13, 0x1

    const-string v14, "CREATE_TYPE_AND_SORT_TEXT_INDEX"

    invoke-direct {v0, v14, v13, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 62
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "DELETE from {table} WHERE {object_id} = ?"

    .line 63
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v5, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 67
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v14, 0x2

    const-string v15, "DELETE"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->DELETE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 69
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "DELETE from {table} WHERE {object_type} = ?"

    .line 70
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 72
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 74
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v15, 0x3

    const-string v14, "DELETE_BY_TYPE"

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->DELETE_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 76
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({object_id}, {object_type}, {sort_text}, {object_blob}) VALUES (?, ?, ?, ?)"

    .line 77
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v5, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v9, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 83
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 85
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v14, 0x4

    const-string v15, "INSERT_OR_REPLACE"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 87
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "SELECT {object_type}, {object_blob} FROM {table} WHERE {object_type} = ? ORDER BY {sort_text} COLLATE NOCASE ASC"

    .line 88
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 91
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 92
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 93
    invoke-virtual {v1, v9, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 94
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 95
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "SELECT_OBJECTS_BY_TYPE"

    const/4 v9, 0x5

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 97
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "SELECT {object_type}, {object_blob}, {object_id} FROM {table} WHERE {object_id} IN (%ids%) AND {object_type} = ?"

    .line 98
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 100
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 101
    invoke-virtual {v1, v5, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 102
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 103
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 105
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "SELECT_IDS_AND_OBJECTS_BY_IDS"

    const/4 v9, 0x6

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 107
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const-string v1, "SELECT {object_type}, {object_blob} FROM {table} WHERE {object_id} = ? AND {object_type} = ?"

    .line 108
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 110
    invoke-virtual {v1, v7, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 111
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 112
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 113
    invoke-virtual {v1, v5, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 115
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SELECT_OBJECT_BY_ID"

    const/4 v3, 0x7

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    .line 40
    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->CREATE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->CREATE_TYPE_AND_SORT_TEXT_INDEX:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->DELETE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->DELETE_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_OBJECTS_BY_TYPE:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_IDS_AND_OBJECTS_BY_IDS:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->SELECT_OBJECT_BY_ID:Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 120
    iput-object p3, p0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
