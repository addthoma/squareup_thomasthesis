.class public final Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogQuickAmounts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 26
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    const-string v0, "Quick Amounts settings data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getQuickAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->amounts:Ljava/util/List;

    return-object v0
.end method

.method public getQuickAmountsOption()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->option:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->DEFAULT_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object v0
.end method

.method public isEligibleForAutoAmounts()Z
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->eligible_for_auto_amounts:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->DEFAULT_ELIGIBLE_FOR_AUTO_AMOUNTS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
    .locals 2

    .line 43
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$1;)V

    return-object v0
.end method
