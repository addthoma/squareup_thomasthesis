.class public final enum Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;
.super Ljava/lang/Enum;
.source "CatalogConnectV2ObjectType.java"

# interfaces
.implements Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
        ">;",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType<",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum CATEGORY:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum DISCOUNT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum ITEM:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum ITEM_OPTION:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum ITEM_OPTION_VALUE:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum ITEM_VARIATION:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum MEASUREMENT_UNIT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum MODIFIER_LIST:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum QUICK_AMOUNTS_SETTINGS:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum RESOURCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

.field public static final enum TAX:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;


# instance fields
.field private final objectClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation
.end field

.field private final protoType:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 8
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CATEGORY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Category;

    const/4 v3, 0x0

    const-string v4, "CATEGORY"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->CATEGORY:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 9
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DISCOUNT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Discount;

    const/4 v4, 0x1

    const-string v5, "DISCOUNT"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 10
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;

    const/4 v5, 0x2

    const-string v6, "ITEM"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 11
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    const/4 v6, 0x3

    const-string v7, "ITEM_OPTION"

    invoke-direct {v0, v7, v6, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM_OPTION:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 12
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const/4 v7, 0x4

    const-string v8, "ITEM_OPTION_VALUE"

    invoke-direct {v0, v8, v7, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM_OPTION_VALUE:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 13
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;

    const/4 v8, 0x5

    const-string v9, "ITEM_VARIATION"

    invoke-direct {v0, v9, v8, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 14
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    const/4 v9, 0x6

    const-string v10, "MEASUREMENT_UNIT"

    invoke-direct {v0, v10, v9, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->MEASUREMENT_UNIT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 15
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER_LIST:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ModifierList;

    const/4 v10, 0x7

    const-string v11, "MODIFIER_LIST"

    invoke-direct {v0, v11, v10, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->MODIFIER_LIST:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 16
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    const/16 v11, 0x8

    const-string v12, "QUICK_AMOUNTS_SETTINGS"

    invoke-direct {v0, v12, v11, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 17
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->RESOURCE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;

    const/16 v12, 0x9

    const-string v13, "RESOURCE"

    invoke-direct {v0, v13, v12, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->RESOURCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 18
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-class v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Tax;

    const/16 v13, 0xa

    const-string v14, "TAX"

    invoke-direct {v0, v14, v13, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->TAX:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 6
    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->CATEGORY:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM_OPTION:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM_OPTION_VALUE:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->MEASUREMENT_UNIT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->MODIFIER_LIST:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->RESOURCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->TAX:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    aput-object v1, v0, v13

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->$VALUES:[Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-object p3, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->protoType:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 26
    iput-object p4, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->objectClass:Ljava/lang/Class;

    return-void
.end method

.method public static fromStableIdentifier(J)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;
    .locals 7

    .line 38
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->values()[Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 39
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->getStableIdentifier()J

    move-result-wide v4

    cmp-long v6, v4, p0

    if-nez v6, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 43
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not support type with stableIdentifier "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->$VALUES:[Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic compareTo(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;)I
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    invoke-super {p0, p1}, Ljava/lang/Enum;->compareTo(Ljava/lang/Enum;)I

    move-result p1

    return p1
.end method

.method public getObjectClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->objectClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getStableIdentifier()J
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->protoType:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->getValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
