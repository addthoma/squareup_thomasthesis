.class public final Lcom/squareup/tenderpayment/PaymentInputHandlersKt;
.super Ljava/lang/Object;
.source "PaymentInputHandlers.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\u0018\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "awaitPaymentEvent",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "shouldEnableNfcField",
        "",
        "tender-payment_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final awaitPaymentEvent(Lcom/squareup/ui/main/errors/PaymentInputHandler;Z)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            "Z)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$awaitPaymentEvent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;Z)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "Observable.create { emit\u2026izeWithoutNfc()\n    }\n  }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
