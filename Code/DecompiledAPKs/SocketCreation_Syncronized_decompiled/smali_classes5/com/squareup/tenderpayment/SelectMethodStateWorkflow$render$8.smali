.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "+",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "output",
        "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field final synthetic $state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    instance-of v0, p1, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$PaymentEventReceived;

    if-eqz v0, :cond_5

    .line 375
    check-cast p1, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$PaymentEventReceived;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$PaymentEventReceived;->getPaymentEvent()Lcom/squareup/ui/main/errors/PaymentEvent;

    move-result-object p1

    .line 376
    instance-of v0, p1, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onPaymentReceivedTakeSwipePayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeSwipePayment;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 377
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onPaymentReceivedTakeDipPayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeDipPayment;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 378
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onPaymentReceivedTakeTapPayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeTapPayment;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 379
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onPaymentReceivedReportReaderIssue(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/ReportReaderIssue;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 380
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/main/errors/CardFailed;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/CardFailed;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    invoke-static {v0, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onPaymentReceivedCardFailed(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/CardFailed;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 383
    :cond_5
    sget-object v0, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;->INSTANCE:Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;->invoke(Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
