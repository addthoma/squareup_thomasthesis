.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1$1;
.super Ljava/lang/Object;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .line 441
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 442
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$TenderToCancelSelected;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;

    iget-object v1, v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->access$getTender$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$TenderToCancelSelected;-><init>(Lcom/squareup/payment/tender/BaseTender;)V

    .line 441
    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
