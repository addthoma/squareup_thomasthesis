.class public abstract Lcom/squareup/tenderpayment/SelectMethodWorkflowState;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;,
        Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u0008\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0004R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0006\u0004\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "",
        "()V",
        "selectMethodState",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
        "getSelectMethodState",
        "()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
        "copySelectMethodState",
        "CancellingBillCheckingPermissionsState",
        "CancellingBillState",
        "ConfirmingChargeCardOnFileState",
        "SelectMethodState",
        "ShowingCashDialogState",
        "SplitTenderWarningState",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;-><init>()V

    return-void
.end method


# virtual methods
.method public final copySelectMethodState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState;
    .locals 9

    const-string v0, "selectMethodState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    move-object v0, p0

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    .line 71
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 72
    :cond_0
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    if-eqz v1, :cond_1

    .line 73
    move-object v2, v0

    check-cast v2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v3, p1

    invoke-static/range {v2 .. v8}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 75
    :cond_1
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    if-eqz v1, :cond_2

    .line 76
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;->copy(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 78
    :cond_2
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    if-eqz v1, :cond_3

    .line 79
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;->copy(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 81
    :cond_3
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    if-eqz v1, :cond_4

    .line 82
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;->copy(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 84
    :cond_4
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    if-eqz v1, :cond_5

    .line 85
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public abstract getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
.end method
