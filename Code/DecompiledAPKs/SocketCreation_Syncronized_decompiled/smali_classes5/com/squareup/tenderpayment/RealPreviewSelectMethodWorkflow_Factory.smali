.class public final Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealPreviewSelectMethodWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final rendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->rendererProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;-><init>(Lcom/squareup/util/Device;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->rendererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-static/range {v1 .. v6}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow_Factory;->get()Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    move-result-object v0

    return-object v0
.end method
