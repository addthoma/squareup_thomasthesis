.class public final enum Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;
.super Ljava/lang/Enum;
.source "RealSelectMethodWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\n\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;",
        "",
        "(Ljava/lang/String;I)V",
        "SELECT_METHOD",
        "CONFIRMING_CHARGE_CARD_ON_FILE",
        "CANCELLING_BILL",
        "CANCELLING_BILL_CHECKING_PERMISSIONS",
        "SPLIT_TENDER_WARNING",
        "SHOWING_CASH_DIALOG",
        "PROCESSING_TENDER",
        "DONE",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum CANCELLING_BILL_CHECKING_PERMISSIONS:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum CONFIRMING_CHARGE_CARD_ON_FILE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum PROCESSING_TENDER:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum SHOWING_CASH_DIALOG:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

.field public static final enum SPLIT_TENDER_WARNING:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x0

    const-string v3, "SELECT_METHOD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x1

    const-string v3, "CONFIRMING_CHARGE_CARD_ON_FILE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CONFIRMING_CHARGE_CARD_ON_FILE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x2

    const-string v3, "CANCELLING_BILL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x3

    const-string v3, "CANCELLING_BILL_CHECKING_PERMISSIONS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL_CHECKING_PERMISSIONS:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x4

    const-string v3, "SPLIT_TENDER_WARNING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SPLIT_TENDER_WARNING:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x5

    const-string v3, "SHOWING_CASH_DIALOG"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SHOWING_CASH_DIALOG:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x6

    const-string v3, "PROCESSING_TENDER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->PROCESSING_TENDER:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const/4 v2, 0x7

    const-string v3, "DONE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->$VALUES:[Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1625
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;
    .locals 1

    const-class v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;
    .locals 1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->$VALUES:[Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-virtual {v0}, [Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    return-object v0
.end method
