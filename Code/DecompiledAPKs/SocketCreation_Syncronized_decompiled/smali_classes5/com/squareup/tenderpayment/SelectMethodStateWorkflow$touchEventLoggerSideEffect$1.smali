.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1;
.super Ljava/lang/Object;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lio/reactivex/CompletableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/permissionworkflow/PermissionWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/payment/Transaction;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/CompletableEmitter;",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/CompletableEmitter;)V
    .locals 2

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$getTouchEventMonitor$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/ui/TouchEventMonitor;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/TouchEventMonitor;->observeTouchEvents()Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "touchEventMonitor.observeTouchEvents()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 230
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 228
    invoke-interface {p1, v0}, Lio/reactivex/CompletableEmitter;->setDisposable(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
