.class public final Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;
.super Ljava/lang/Object;
.source "RealTenderOptionMap_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/RealTenderOptionMap;",
        ">;"
    }
.end annotation


# instance fields
.field private final tenderOptionFactoriesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
            ">;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;->tenderOptionFactoriesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
            ">;>;)",
            "Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Set;)Lcom/squareup/tenderpayment/RealTenderOptionMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
            ">;)",
            "Lcom/squareup/tenderpayment/RealTenderOptionMap;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/tenderpayment/RealTenderOptionMap;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealTenderOptionMap;-><init>(Ljava/util/Set;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/RealTenderOptionMap;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;->tenderOptionFactoriesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;->newInstance(Ljava/util/Set;)Lcom/squareup/tenderpayment/RealTenderOptionMap;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/RealTenderOptionMap_Factory;->get()Lcom/squareup/tenderpayment/RealTenderOptionMap;

    move-result-object v0

    return-object v0
.end method
