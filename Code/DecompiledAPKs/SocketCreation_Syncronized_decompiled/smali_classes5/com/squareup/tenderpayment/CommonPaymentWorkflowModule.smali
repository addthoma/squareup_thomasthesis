.class public abstract Lcom/squareup/tenderpayment/CommonPaymentWorkflowModule;
.super Ljava/lang/Object;
.source "CommonPaymentWorkflowModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCollectCashModalShown(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "collect-cash-modal-shown"

    .line 85
    invoke-virtual {p0, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getStringSet(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideDefaultCustomerCheckoutEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 79
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "default-customer-checkout-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideSkipCartScreenEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 91
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "skip-cart-screen-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract provideBuyerCheckoutLauncher(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBuyerCheckoutRenderer(Lcom/squareup/buyercheckout/RealBuyerCheckoutRenderer;)Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBuyerCheckoutWorkflow(Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;)Lcom/squareup/buyercheckout/BuyerCheckoutScreenWorkflowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCardSellerWorkflow(Lcom/squareup/ui/seller/RealCardSellerWorkflow;)Lcom/squareup/tenderpayment/CardSellerWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideChangeHudToaster(Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;)Lcom/squareup/tenderpayment/ChangeHudToaster;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCustomerCheckoutSettings(Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;)Lcom/squareup/buyercheckout/CustomerCheckoutSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideLegacySelectMethodV2Workflow(Lcom/squareup/tenderpayment/RealLegacySelectMethodV2Workflow;)Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideManualCardEntryScreenDataHelper(Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;)Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePaymentViewFactory(Lcom/squareup/tenderpayment/RealPaymentViewFactory;)Lcom/squareup/tenderpayment/PaymentViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePreviewSelectMethodWorkflow(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;)Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideRealBuyerCheckoutV2Workflow(Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;)Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSelectMethodBuyerCheckoutEnabled(Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;)Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSelectMethodV2Workflow(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/SelectMethodV2Workflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSelectMethodWorkflow(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSeparateTenderTransaction(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSeparateTenderV2Workflow(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderOptionMap(Lcom/squareup/tenderpayment/RealTenderOptionMap;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderPaymentWorkflow(Lcom/squareup/tenderpayment/TenderPaymentWorkflow;)Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
