.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowStateSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectMethodWorkflowStateSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectMethodWorkflowStateSerializer.kt\ncom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,128:1\n180#2:129\n56#3:130\n*E\n*S KotlinDebug\n*F\n+ 1 SelectMethodWorkflowStateSerializer.kt\ncom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer\n*L\n59#1:129\n59#1:130\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008J\u000e\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0004\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "initialSelectMethodStateProvider",
        "Lkotlin/Function0;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
        "snapshotState",
        "state",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSnapshot(Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function0;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
            ">;)",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;"
        }
    .end annotation

    const-string v0, "snapshot"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialSelectMethodStateProvider"

    move-object/from16 v2, p2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    .line 129
    new-instance v1, Lokio/Buffer;

    invoke-direct {v1}, Lokio/Buffer;-><init>()V

    invoke-virtual {v1, v0}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object v0

    check-cast v0, Lokio/BufferedSource;

    .line 60
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "Class.forName(className)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    .line 65
    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 66
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v7

    .line 68
    invoke-interface/range {p2 .. p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3ffd

    const/16 v21, 0x0

    .line 69
    invoke-static/range {v5 .. v21}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto/16 :goto_0

    .line 72
    :cond_0
    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 73
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v7

    .line 130
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    .line 74
    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 75
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 76
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 78
    new-instance v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    .line 79
    invoke-interface/range {p2 .. p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3ffd

    const/16 v21, 0x0

    .line 80
    invoke-static/range {v5 .. v21}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v2

    .line 78
    invoke-direct {v4, v2, v1, v3, v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;-><init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto/16 :goto_0

    .line 87
    :cond_1
    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 88
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v7

    .line 90
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    .line 91
    invoke-interface/range {p2 .. p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3ffd

    const/16 v21, 0x0

    .line 92
    invoke-static/range {v5 .. v21}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    .line 90
    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;-><init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto/16 :goto_0

    .line 96
    :cond_2
    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 97
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v7

    .line 99
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    .line 100
    invoke-interface/range {p2 .. p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3ffd

    const/16 v21, 0x0

    .line 101
    invoke-static/range {v5 .. v21}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    .line 99
    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;-><init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 105
    :cond_3
    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 106
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v7

    .line 108
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    .line 109
    invoke-interface/range {p2 .. p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3ffd

    const/16 v21, 0x0

    .line 110
    invoke-static/range {v5 .. v21}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    .line 108
    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;-><init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    goto :goto_0

    .line 114
    :cond_4
    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 115
    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    .line 120
    invoke-interface/range {p2 .. p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x3ffd

    const/16 v20, 0x0

    .line 121
    invoke-static/range {v4 .. v20}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    :goto_0
    return-object v0

    .line 124
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized state "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " while deserializing SelectMethodWorkflowState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public final snapshotState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
