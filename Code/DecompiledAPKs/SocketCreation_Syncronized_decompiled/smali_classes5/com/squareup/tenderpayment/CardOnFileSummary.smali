.class public Lcom/squareup/tenderpayment/CardOnFileSummary;
.super Ljava/lang/Object;
.source "CardOnFileSummary.java"


# instance fields
.field public final cardNameAndNumber:Ljava/lang/String;

.field public final instrumentToken:Ljava/lang/String;

.field public final isExpired:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/tenderpayment/CardOnFileSummary;->cardNameAndNumber:Ljava/lang/String;

    .line 10
    iput-boolean p2, p0, Lcom/squareup/tenderpayment/CardOnFileSummary;->isExpired:Z

    .line 11
    iput-object p3, p0, Lcom/squareup/tenderpayment/CardOnFileSummary;->instrumentToken:Ljava/lang/String;

    return-void
.end method
