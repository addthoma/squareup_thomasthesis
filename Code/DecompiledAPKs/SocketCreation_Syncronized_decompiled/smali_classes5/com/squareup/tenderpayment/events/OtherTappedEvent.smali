.class public Lcom/squareup/tenderpayment/events/OtherTappedEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "OtherTappedEvent.java"


# instance fields
.field private final event_id:Ljava/lang/String;

.field private final other_tender_type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 13
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_OTHER:Lcom/squareup/analytics/RegisterTapName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 14
    iput-object p1, p0, Lcom/squareup/tenderpayment/events/OtherTappedEvent;->other_tender_type:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/squareup/tenderpayment/events/OtherTappedEvent;->event_id:Ljava/lang/String;

    return-void
.end method
