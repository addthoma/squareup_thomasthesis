.class public final Lcom/squareup/tenderpayment/events/SplitTenderEvenSplitEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "SplitTenderEvenSplitEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/events/SplitTenderEvenSplitEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "num_splits",
        "",
        "(J)V",
        "getNum_splits",
        "()J",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final num_splits:J


# direct methods
.method public constructor <init>(J)V
    .locals 2

    .line 8
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_EVEN_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    .line 7
    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-wide p1, p0, Lcom/squareup/tenderpayment/events/SplitTenderEvenSplitEvent;->num_splits:J

    return-void
.end method


# virtual methods
.method public final getNum_splits()J
    .locals 2

    .line 7
    iget-wide v0, p0, Lcom/squareup/tenderpayment/events/SplitTenderEvenSplitEvent;->num_splits:J

    return-wide v0
.end method
