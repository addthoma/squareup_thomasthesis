.class public abstract Lcom/squareup/tenderpayment/X2PaymentWorkflowModule;
.super Ljava/lang/Object;
.source "X2PaymentWorkflowModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/tenderpayment/CommonPaymentWorkflowModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideTenderCompleter(Lcom/squareup/tenderpayment/X2TenderCompleter;)Lcom/squareup/tenderpayment/TenderCompleter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
