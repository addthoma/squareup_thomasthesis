.class public interface abstract Lcom/squareup/tenderpayment/ChangeHudToaster;
.super Ljava/lang/Object;
.source "ChangeHudToaster.java"


# virtual methods
.method public abstract clear()V
.end method

.method public abstract prepare(Lcom/squareup/payment/CompletedPayment;)V
.end method

.method public abstract prepareRemainingBalanceToast(Ljava/lang/String;)V
.end method

.method public abstract toastIfAvailable()V
.end method
