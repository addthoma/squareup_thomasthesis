.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field final synthetic $state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->invoke(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 571
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 573
    instance-of v1, p1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 574
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    check-cast v2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v2

    .line 575
    iget-object v3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 576
    check-cast p1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 573
    invoke-static {v1, v2, v3, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onConfirmCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 578
    :cond_0
    sget-object v1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$DismissCollectCash;->INSTANCE:Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$DismissCollectCash;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onDismissCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 571
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 578
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
