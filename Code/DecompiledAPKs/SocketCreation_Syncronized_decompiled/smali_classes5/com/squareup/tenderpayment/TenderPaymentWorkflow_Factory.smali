.class public final Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;
.super Ljava/lang/Object;
.source "TenderPaymentWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerCheckoutWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final legacySelectMethodWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final payOtherTenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final payOtherWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final sellerQuickCashWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final separateTenderHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final separateTenderWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderOptionMapProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 80
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 81
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 82
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 83
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 84
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 85
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 86
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 87
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->separateTenderHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 88
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->tenderOptionMapProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 89
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->payOtherTenderCompleterProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 90
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->buyerCheckoutWorkflowProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 91
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->legacySelectMethodWorkflowProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 92
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->selectMethodWorkflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 93
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->payOtherWorkflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 94
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->sellerQuickCashWorkflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 95
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->separateTenderWorkflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 96
    iput-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;)",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 120
    new-instance v18, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;)Lcom/squareup/tenderpayment/TenderPaymentWorkflow;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            "Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
            ">;",
            "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ")",
            "Lcom/squareup/tenderpayment/TenderPaymentWorkflow;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 135
    new-instance v18, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow;-><init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/TenderPaymentWorkflow;
    .locals 19

    move-object/from16 v0, p0

    .line 101
    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->separateTenderHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->tenderOptionMapProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->payOtherTenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->buyerCheckoutWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->legacySelectMethodWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;

    iget-object v14, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->selectMethodWorkflowProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->payOtherWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->sellerQuickCashWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->separateTenderWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;

    iget-object v1, v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static/range {v2 .. v18}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->newInstance(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Lcom/squareup/tenderpayment/LegacySelectMethodV2Workflow;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;)Lcom/squareup/tenderpayment/TenderPaymentWorkflow;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflow_Factory;->get()Lcom/squareup/tenderpayment/TenderPaymentWorkflow;

    move-result-object v0

    return-object v0
.end method
