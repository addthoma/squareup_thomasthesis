.class public Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowRenderer.java"


# static fields
.field private static final MAX_QUICKCASH_OPTIONS:I = 0x4


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final buyerCheckoutTenderOptionFactory:Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final expirationHelper:Lcom/squareup/card/ExpirationHelper;

.field private final isTablet:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final quickCashCalculator:Lcom/squareup/money/QuickCashCalculator;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderOptionMap:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;


# direct methods
.method public constructor <init>(Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/util/Device;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;Lcom/squareup/crm/CustomerManagementSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/QuickCashCalculator;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/card/ExpirationHelper;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->quickCashCalculator:Lcom/squareup/money/QuickCashCalculator;

    .line 96
    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 97
    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 98
    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->expirationHelper:Lcom/squareup/card/ExpirationHelper;

    .line 99
    invoke-interface {p5}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isTablet:Z

    .line 100
    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->tenderOptionMap:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

    .line 101
    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    .line 102
    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 103
    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->buyerCheckoutTenderOptionFactory:Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;

    .line 104
    iput-object p10, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    return-void
.end method

.method private getTransactionOfflineMaximum()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 693
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getSingleTransactionLimit()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method private isInOfflineMode()Z
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    return v0
.end method

.method private isStoreAndForwardEnabled()Z
    .locals 1

    .line 460
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$actionablePromptText$11(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 641
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$actionablePromptText$12(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 675
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$ReenableContactlessClicked;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ReenableContactlessClicked;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$1(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 211
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$ThirdPartyCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ThirdPartyCardSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$10(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 358
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$ContactlessSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ContactlessSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$2(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V
    .locals 1

    .line 222
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$3(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 229
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$4(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 245
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$5(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 253
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$6(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 269
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$7(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 296
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$InvoiceSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$InvoiceSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$8(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 319
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$tenderViewData$9(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1

    .line 323
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private offlineConnectReaderInstruction()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11

    .line 576
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->connect_reader_offline_instruction:I

    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method

.method private paymentAboveMaximum(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;"
        }
    .end annotation

    .line 596
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_type_above_maximum_card:I

    .line 597
    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v3, "amount"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method

.method private paymentBelowMinimum(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;"
        }
    .end annotation

    .line 588
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_type_below_minimum_card:I

    .line 589
    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v3, "amount"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method

.method private paymentIsAboveMaximum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    .line 685
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method private paymentIsAboveOfflineMaximum(Lcom/squareup/protos/common/Money;)Z
    .locals 1

    .line 697
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->getTransactionOfflineMaximum()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 698
    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private paymentIsBelowMinimum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    .line 689
    invoke-static {p2, p1}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method private paymentOverOfflineMax()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11

    .line 569
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_type_above_maximum_card_offline:I

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 570
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->getTransactionOfflineMaximum()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v3, "amount"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method

.method private paymentPromptNoInternet()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11

    .line 563
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_methods_prompt_offline_mode_help_text:I

    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method

.method private paymentPromptOfflineMode()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11

    .line 582
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_methods_prompt_offline_mode:I

    sget v4, Lcom/squareup/noho/R$color;->noho_text_body:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v10
.end method

.method private supportedCardMethods(Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZ)Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;ZZZ)",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 473
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    .line 474
    iget-boolean v2, v2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_3

    .line 481
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    .line 482
    iget-boolean v5, v5, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 489
    :cond_3
    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_SWIPE:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 491
    sget-object v5, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 492
    invoke-virtual {v0, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    if-nez p6, :cond_4

    const/4 v3, 0x1

    .line 493
    :cond_4
    sget-object v4, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v4}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    if-nez v3, :cond_6

    if-nez v2, :cond_6

    if-nez v1, :cond_6

    if-eqz p7, :cond_5

    .line 497
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_choose_option_x2:I

    goto :goto_1

    :cond_5
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_no_payment_options:I

    :goto_1
    move v2, v0

    .line 501
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget v5, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    :cond_6
    if-nez v0, :cond_8

    if-nez v3, :cond_8

    if-nez v2, :cond_8

    if-eqz p7, :cond_7

    .line 508
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_choose_option_x2:I

    goto :goto_2

    :cond_7
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_choose_option:I

    :goto_2
    move v2, v0

    .line 511
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget v5, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    :cond_8
    move-object/from16 v1, p0

    .line 520
    iget-object v4, v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->buyerCheckoutTenderOptionFactory:Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;

    invoke-interface {v4}, Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;->getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object v4

    move-object/from16 v5, p2

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 521
    invoke-static {}, Lcom/squareup/tenderpayment/SelectMethod$TextData;->empty()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    :cond_9
    if-eqz v0, :cond_b

    if-eqz v2, :cond_b

    if-eqz v3, :cond_b

    .line 526
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_all_options:I

    if-eqz p5, :cond_a

    .line 528
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_all_options_single_message:I

    :cond_a
    move v3, v0

    .line 530
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget v6, Lcom/squareup/noho/R$color;->noho_text_body:I

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    :cond_b
    if-eqz v2, :cond_c

    if-eqz v0, :cond_c

    .line 536
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v13, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_swipe_and_insert:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    sget v16, Lcom/squareup/noho/R$color;->noho_text_body:I

    sget-object v17, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v21}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    :cond_c
    if-eqz v0, :cond_d

    if-eqz v3, :cond_d

    .line 542
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v3, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_insert_and_tap:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget v6, Lcom/squareup/noho/R$color;->noho_text_body:I

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    :cond_d
    if-eqz v0, :cond_f

    .line 547
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_insert:I

    if-eqz p5, :cond_e

    .line 549
    sget v0, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_insert_single_message:I

    :cond_e
    move v3, v0

    .line 552
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget v6, Lcom/squareup/noho/R$color;->noho_text_body:I

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    .line 557
    :cond_f
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v13, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_swipe:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    sget v16, Lcom/squareup/noho/R$color;->noho_text_body:I

    sget-object v17, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v21}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method actionablePromptText(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "ZZZ)",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    .line 629
    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    if-eqz p7, :cond_0

    .line 635
    sget v3, Lcom/squareup/tenderworkflow/R$string;->record_payment_amount:I

    .line 636
    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v5, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 638
    sget v6, Lcom/squareup/noho/R$color;->noho_text_button_secondary_enabled:I

    .line 639
    sget-object v8, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 641
    new-instance v9, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$Xvu77ZZUHyu7rfu3OWStEV6R_WI;

    invoke-direct {v9, v1, v2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$Xvu77ZZUHyu7rfu3OWStEV6R_WI;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/common/Money;)V

    const-string v1, "amount"

    move-object v11, v1

    move-object v10, v5

    move v12, v6

    move-object v13, v8

    move-object v15, v9

    const/4 v14, 0x0

    const/16 v16, 0x1

    move v9, v3

    goto/16 :goto_6

    :cond_0
    if-eqz p3, :cond_7

    .line 642
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isInOfflineMode()Z

    move-result v8

    if-eqz v8, :cond_1

    goto :goto_1

    :cond_1
    move-object/from16 v8, p5

    move-object/from16 v9, p6

    .line 651
    invoke-virtual {v0, v2, v8, v9}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isPaymentInRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-nez v2, :cond_2

    goto/16 :goto_4

    :cond_2
    if-eqz p9, :cond_3

    goto/16 :goto_4

    .line 656
    :cond_3
    sget-object v2, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->ordinal()I

    move-result v8

    aget v2, v2, v8

    if-eq v2, v4, :cond_8

    const/4 v6, 0x2

    if-eq v2, v6, :cond_5

    const/4 v6, 0x3

    if-eq v2, v6, :cond_5

    const/4 v6, 0x4

    if-eq v2, v6, :cond_4

    move-object v13, v3

    move-object v10, v5

    move-object v11, v10

    move-object v15, v11

    const/4 v9, 0x0

    const/4 v12, 0x0

    goto :goto_3

    .line 672
    :cond_4
    sget v2, Lcom/squareup/tenderworkflow/R$string;->contactless_payment_methods_not_ready:I

    .line 673
    sget v3, Lcom/squareup/noho/R$color;->noho_text_button_secondary_enabled:I

    .line 674
    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 675
    new-instance v8, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$g7ytPxqEr86Xe1ld806gjdnPLDI;

    invoke-direct {v8, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$g7ytPxqEr86Xe1ld806gjdnPLDI;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    move v9, v2

    move v12, v3

    move-object v10, v5

    move-object v11, v10

    move-object v13, v6

    move-object v15, v8

    const/4 v14, 0x0

    const/16 v16, 0x1

    goto :goto_6

    :cond_5
    if-eqz p8, :cond_6

    .line 664
    sget v1, Lcom/squareup/tenderworkflow/R$string;->contactless_payment_methods_ready_show_contactless_row:I

    goto :goto_0

    .line 666
    :cond_6
    sget v1, Lcom/squareup/tenderworkflow/R$string;->contactless_payment_methods_ready:I

    .line 668
    :goto_0
    sget v2, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    goto :goto_2

    .line 643
    :cond_7
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isStoreAndForwardEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 644
    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_methods_prompt_offline_mode_help_text:I

    .line 646
    sget v2, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    .line 647
    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    :goto_2
    move v9, v1

    move v12, v2

    move-object v13, v3

    move-object v10, v5

    move-object v11, v10

    move-object v15, v11

    :goto_3
    const/4 v14, 0x0

    goto :goto_5

    :cond_8
    :goto_4
    move-object v13, v3

    move-object v10, v5

    move-object v11, v10

    move-object v15, v11

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/16 v14, 0x8

    :goto_5
    const/16 v16, 0x0

    .line 680
    :goto_6
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/16 v17, 0x0

    move-object v8, v1

    invoke-direct/range {v8 .. v17}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v1
.end method

.method cardsOnFile(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 370
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 373
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 374
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 375
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->expirationHelper:Lcom/squareup/card/ExpirationHelper;

    iget-object v3, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v4, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/card/ExpirationHelper;->isExpired(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Z

    move-result v2

    .line 377
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 378
    invoke-static {v6}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->formatNameAndNumber(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "%s"

    invoke-static {v3, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 379
    new-instance v4, Lcom/squareup/tenderpayment/CardOnFileSummary;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->instrument_token:Ljava/lang/String;

    invoke-direct {v4, v3, v2, v1}, Lcom/squareup/tenderpayment/CardOnFileSummary;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method getQuickCashAmounts(Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 391
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isTablet:Z

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->quickCashCalculator:Lcom/squareup/money/QuickCashCalculator;

    .line 393
    invoke-static {p1}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/money/QuickCashCalculator;->buildQuickCashOptions(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p1

    .line 394
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    const/4 v2, 0x4

    .line 395
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 394
    invoke-interface {p1, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    .line 397
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method isPaymentInRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    .line 386
    invoke-direct {p0, p1, p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentIsBelowMinimum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p3

    if-nez p3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentIsAboveMaximum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$tenderViewData$0$SelectMethodWorkflowRenderer(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->getTapName()Lcom/squareup/analytics/RegisterTapName;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 163
    new-instance p1, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;

    invoke-direct {p1, p3}, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    invoke-interface {p2, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method paymentPromptText(ZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLcom/squareup/CountryCode;)Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;ZZZ",
            "Lcom/squareup/CountryCode;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;"
        }
    .end annotation

    move-object v8, p0

    move-object v0, p3

    move-object v1, p4

    move-object/from16 v2, p5

    if-nez p1, :cond_0

    .line 426
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v1, Lcom/squareup/tenderworkflow/R$string;->payment_methods_prompt_nfc_not_enabled:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object p1, v0

    move p2, v1

    move-object p3, v2

    move-object p4, v3

    move/from16 p5, v4

    move-object/from16 p6, v5

    move/from16 p7, v6

    move-object/from16 p8, v7

    move/from16 p9, v9

    move-object/from16 p10, v10

    invoke-direct/range {p1 .. p10}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-object v0

    .line 431
    :cond_0
    sget-object v3, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    move-object/from16 v4, p13

    invoke-virtual {v4, v3}, Lcom/squareup/CountryCode;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 433
    invoke-direct {p0, p3, v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentIsBelowMinimum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 434
    iget-object v0, v8, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p0, v0, v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentBelowMinimum(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    :cond_1
    if-eqz v3, :cond_2

    .line 436
    invoke-direct {p0, p3, p4}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentIsAboveMaximum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 437
    iget-object v0, v8, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentAboveMaximum(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    :cond_2
    if-eqz p2, :cond_4

    .line 438
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isInOfflineMode()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    move-object/from16 v4, p9

    move/from16 v5, p10

    move/from16 v6, p11

    move/from16 v7, p12

    .line 455
    invoke-direct/range {v0 .. v7}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->supportedCardMethods(Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZ)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    .line 439
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isStoreAndForwardEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 440
    invoke-virtual/range {p6 .. p6}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 442
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->offlineConnectReaderInstruction()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    .line 445
    :cond_5
    invoke-direct {p0, p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentIsAboveOfflineMaximum(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 447
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentOverOfflineMax()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    .line 449
    :cond_6
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentPromptOfflineMode()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0

    .line 452
    :cond_7
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentPromptNoInternet()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0
.end method

.method tenderViewData(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;Lcom/squareup/protos/common/Money;ZZZZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "ZZZZZZ",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v9, p3

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    .line 135
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 137
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_20

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    .line 138
    instance-of v5, v2, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    if-eqz v5, :cond_1

    .line 139
    move-object v14, v2

    check-cast v14, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    .line 140
    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->tenderOptionMap:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;

    invoke-interface {v2, v14}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;->getTenderOption(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    move-result-object v15

    .line 142
    new-instance v8, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    move-object v2, v8

    move-object/from16 v3, p3

    move/from16 v4, p7

    move/from16 v5, p4

    move/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 p2, v13

    move-object v13, v8

    move-object/from16 v8, p11

    invoke-direct/range {v2 .. v8}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;-><init>(Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 150
    invoke-virtual {v15}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->getTitleStrategy()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    invoke-interface {v2, v13}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v25, v2

    check-cast v25, Ljava/lang/String;

    .line 151
    invoke-virtual {v15}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    invoke-interface {v2, v13}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    sget v3, Lcom/squareup/ui/buyercart/R$color;->title_color:I

    goto :goto_1

    :cond_0
    sget v3, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    :goto_1
    move/from16 v20, v3

    .line 157
    new-instance v3, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    sget-object v21, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v16, v3

    move/from16 v24, v2

    invoke-direct/range {v16 .. v25}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    .line 160
    new-instance v4, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$J11kXXGWSXVBvxQthGHjbMrXZGA;

    invoke-direct {v4, v0, v15, v1, v14}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$J11kXXGWSXVBvxQthGHjbMrXZGA;-><init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    .line 165
    new-instance v5, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v16, v5

    move/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v19, v4

    invoke-direct/range {v16 .. v22}, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;-><init>(ZLcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/fsm/SideEffect;ZZZ)V

    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object/from16 v13, p2

    goto/16 :goto_0

    :cond_1
    move-object/from16 p2, v13

    .line 170
    check-cast v2, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$LegacyTenderOption;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$LegacyTenderOption;->getLegacyTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v2

    .line 184
    invoke-static {v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->supportedConnectivityModes(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    move-result-object v5

    .line 185
    sget-object v6, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderAvailability:[I

    invoke-virtual {v5}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ordinal()I

    move-result v7

    aget v6, v6, v7

    const/4 v7, 0x3

    const/4 v8, 0x2

    if-eq v6, v3, :cond_5

    if-eq v6, v8, :cond_4

    if-ne v6, v7, :cond_3

    if-nez p9, :cond_2

    if-eqz p4, :cond_2

    goto :goto_3

    :cond_2
    const/4 v5, 0x0

    goto :goto_4

    .line 196
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-array v2, v3, [Ljava/lang/Object;

    .line 197
    invoke-virtual {v5}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "Unsupported tenderAvailability: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    if-nez p9, :cond_5

    if-eqz p4, :cond_2

    :cond_5
    :goto_3
    const/4 v5, 0x1

    .line 207
    :goto_4
    iget-object v6, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget-object v13, Lcom/squareup/tenderpayment/TenderSettingsManager;->LEGACY_OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v14, 0x0

    if-ne v6, v13, :cond_9

    .line 208
    iget-object v3, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    sget-object v6, Lcom/squareup/tenderpayment/TenderSettingsManager;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iget-object v6, v6, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-static {v3, v6}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 210
    sget v2, Lcom/squareup/tenderworkflow/R$string;->record_card_payment:I

    .line 211
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$vP8AgyKZ78PpiUuFphptlDPOzTU;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$vP8AgyKZ78PpiUuFphptlDPOzTU;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    :cond_6
    :goto_5
    move v4, v5

    :goto_6
    move-object/from16 v22, v14

    :goto_7
    const/4 v6, 0x0

    :goto_8
    const/4 v7, 0x0

    :goto_9
    move v14, v2

    const/4 v2, 0x0

    goto/16 :goto_14

    .line 213
    :cond_7
    iget-object v3, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    sget-object v6, Lcom/squareup/tenderpayment/TenderSettingsManager;->CHECK:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iget-object v6, v6, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-static {v3, v6}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 214
    invoke-static/range {p3 .. p3}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    const/4 v5, 0x0

    goto :goto_a

    :cond_8
    move v3, v5

    .line 218
    :goto_a
    iget-object v6, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v6}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v6

    .line 219
    invoke-virtual {v6}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderTypeNameMap()Ljava/util/Map;

    move-result-object v6

    iget-object v7, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    .line 220
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v14, v6

    check-cast v14, Ljava/lang/String;

    .line 222
    new-instance v6, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$BT-C_27kFxY-eTNUx7AX1Je-HaY;

    invoke-direct {v6, v1, v2, v14}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$BT-C_27kFxY-eTNUx7AX1Je-HaY;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V

    move v4, v3

    move-object v3, v6

    move-object/from16 v22, v14

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v14, 0x0

    goto/16 :goto_14

    .line 225
    :cond_9
    sget-object v6, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    iget-object v13, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v13}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v13

    aget v6, v6, v13

    if-eq v6, v3, :cond_1d

    if-eq v6, v8, :cond_1a

    if-eq v6, v7, :cond_15

    const/4 v7, 0x4

    if-eq v6, v7, :cond_11

    const/4 v7, 0x5

    if-ne v6, v7, :cond_10

    if-eqz v5, :cond_a

    .line 302
    sget v2, Lcom/squareup/tenderworkflow/R$string;->manual_gift_card_entry:I

    .line 304
    invoke-static/range {p3 .. p3}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v6

    if-eqz v6, :cond_b

    const/4 v5, 0x0

    const/4 v6, 0x0

    goto :goto_b

    .line 309
    :cond_a
    sget v2, Lcom/squareup/tenderworkflow/R$string;->manual_gift_card_entry_offline_mode:I

    :cond_b
    move v6, v5

    :goto_b
    if-eqz p6, :cond_d

    .line 314
    iget-object v7, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v7}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v7

    if-eqz v7, :cond_d

    if-eqz p8, :cond_c

    move v4, v6

    move-object v3, v14

    move-object/from16 v22, v3

    const/4 v6, 0x0

    const/4 v7, 0x1

    goto :goto_9

    .line 319
    :cond_c
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$LVsz4M_TmGuDJGAPkGBXKLreNKc;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$LVsz4M_TmGuDJGAPkGBXKLreNKc;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto :goto_c

    :cond_d
    if-eqz v5, :cond_f

    .line 323
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$s-sRnECObzlDCA-mcqSKR0tQoqU;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$s-sRnECObzlDCA-mcqSKR0tQoqU;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 325
    iget-object v7, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v7}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v7

    if-nez v7, :cond_e

    goto/16 :goto_13

    :cond_e
    :goto_c
    move v4, v6

    goto/16 :goto_6

    :cond_f
    move v4, v6

    goto :goto_d

    .line 332
    :cond_10
    new-instance v1, Ljava/lang/IllegalStateException;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 333
    invoke-virtual {v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    const-string v2, "Unsupported tender type: %s"

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_11
    if-nez v5, :cond_12

    .line 283
    sget v2, Lcom/squareup/tenderworkflow/R$string;->invoice_offline_mode:I

    :goto_d
    move-object v3, v14

    move-object/from16 v22, v3

    goto/16 :goto_7

    .line 285
    :cond_12
    invoke-virtual {v0, v9, v10, v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isPaymentInRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 289
    sget v2, Lcom/squareup/tenderworkflow/R$string;->invoice:I

    :goto_e
    move-object v3, v14

    move-object/from16 v22, v3

    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_13
    if-eqz p7, :cond_14

    .line 291
    sget v2, Lcom/squareup/tenderworkflow/R$string;->invoice_unavailable_for_split_tender:I

    goto :goto_e

    .line 295
    :cond_14
    sget v2, Lcom/squareup/tenderworkflow/R$string;->invoice:I

    .line 296
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$CcIRJRawLmMNjK2z1J2SixGQC-U;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$CcIRJRawLmMNjK2z1J2SixGQC-U;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto/16 :goto_5

    .line 253
    :cond_15
    new-instance v2, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$k3ZAm43jlcBp4muKNhfq0LwKdYY;

    invoke-direct {v2, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$k3ZAm43jlcBp4muKNhfq0LwKdYY;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    if-eqz p6, :cond_17

    if-eqz v5, :cond_16

    .line 258
    sget v2, Lcom/squareup/tenderworkflow/R$string;->current_customer_cards_on_file:I

    goto :goto_f

    .line 260
    :cond_16
    sget v2, Lcom/squareup/tenderworkflow/R$string;->current_customer_cards_on_file_offline_mode:I

    :goto_f
    move-object v6, v14

    goto :goto_11

    :cond_17
    if-eqz v5, :cond_18

    .line 268
    sget v2, Lcom/squareup/tenderworkflow/R$string;->customer_card_on_file:I

    .line 269
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$ya97XheBRRf8rAijYvGfmoRbveY;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$ya97XheBRRf8rAijYvGfmoRbveY;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    move-object v6, v3

    goto :goto_10

    .line 271
    :cond_18
    sget v3, Lcom/squareup/tenderworkflow/R$string;->customer_card_on_file_offline_mode:I

    move-object v6, v2

    move v2, v3

    :goto_10
    const/4 v3, 0x0

    .line 275
    :goto_11
    invoke-virtual {v0, v9, v10, v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isPaymentInRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v7

    if-nez v7, :cond_19

    move-object/from16 v22, v14

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v14, v2

    move v2, v3

    move-object/from16 v3, v22

    goto :goto_14

    :cond_19
    move v4, v5

    move-object/from16 v22, v14

    const/4 v7, 0x0

    move v14, v2

    move v2, v3

    move-object v3, v6

    const/4 v6, 0x0

    goto :goto_14

    :cond_1a
    if-eqz v5, :cond_1b

    .line 237
    sget v2, Lcom/squareup/tenderworkflow/R$string;->manual_credit_card_entry:I

    goto :goto_12

    .line 239
    :cond_1b
    sget v2, Lcom/squareup/tenderworkflow/R$string;->manual_credit_card_entry_offline_mode:I

    .line 241
    :goto_12
    invoke-virtual {v0, v9, v10, v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isPaymentInRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v3

    if-nez v3, :cond_1c

    goto :goto_e

    .line 245
    :cond_1c
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$Ow_eg3bbN1JsPTlshftr5h0r6mE;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$Ow_eg3bbN1JsPTlshftr5h0r6mE;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 247
    iget-object v6, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v6}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v6

    if-nez v6, :cond_6

    :goto_13
    goto/16 :goto_6

    .line 227
    :cond_1d
    sget v2, Lcom/squareup/tenderworkflow/R$string;->accept_cash:I

    .line 228
    invoke-virtual {v0, v9}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->getQuickCashAmounts(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 229
    new-instance v3, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$GU0XYQejSaVboErMS1hV5OQ-7YM;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$GU0XYQejSaVboErMS1hV5OQ-7YM;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto/16 :goto_5

    :cond_1e
    move v4, v5

    move-object v3, v14

    move-object/from16 v22, v3

    const/4 v6, 0x1

    goto/16 :goto_8

    :goto_14
    if-eqz v4, :cond_1f

    .line 337
    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color:I

    goto :goto_15

    :cond_1f
    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    :goto_15
    move/from16 v17, v4

    .line 341
    new-instance v4, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v15, 0x0

    const/16 v16, 0x0

    sget-object v18, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v13, v4

    move/from16 v21, v5

    invoke-direct/range {v13 .. v22}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    .line 345
    new-instance v8, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    move-object v15, v8

    move/from16 v16, v5

    move-object/from16 v17, v4

    move-object/from16 v18, v3

    move/from16 v19, v2

    move/from16 v20, v6

    move/from16 v21, v7

    invoke-direct/range {v15 .. v21}, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;-><init>(ZLcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/fsm/SideEffect;ZZZ)V

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_20
    if-eqz p5, :cond_23

    if-eqz p4, :cond_21

    if-nez p9, :cond_21

    goto :goto_16

    :cond_21
    const/4 v3, 0x0

    :goto_16
    if-eqz v3, :cond_22

    .line 352
    sget v2, Lcom/squareup/noho/R$color;->noho_text_body:I

    goto :goto_17

    :cond_22
    sget v2, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    :goto_17
    move/from16 v17, v2

    .line 355
    new-instance v2, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    sget v14, Lcom/squareup/tenderworkflow/R$string;->payment_type_contactless_row_phone:I

    const/4 v15, 0x0

    const/16 v16, 0x0

    sget-object v18, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v22, 0x0

    move-object v13, v2

    move/from16 v21, v3

    invoke-direct/range {v13 .. v22}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    .line 358
    new-instance v5, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$C2-wsBpiZZw2sta3J5QidP1xfAQ;

    invoke-direct {v5, v1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodWorkflowRenderer$C2-wsBpiZZw2sta3J5QidP1xfAQ;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 361
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 p3, v1

    move/from16 p4, v3

    move-object/from16 p5, v2

    move-object/from16 p6, v5

    move/from16 p7, v6

    move/from16 p8, v7

    move/from16 p9, v8

    invoke-direct/range {p3 .. p9}, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;-><init>(ZLcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/fsm/SideEffect;ZZZ)V

    invoke-interface {v12, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_23
    return-object v12
.end method
