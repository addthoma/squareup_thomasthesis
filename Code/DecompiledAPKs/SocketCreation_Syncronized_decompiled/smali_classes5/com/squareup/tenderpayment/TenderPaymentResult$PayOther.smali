.class public final Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;
.super Lcom/squareup/tenderpayment/TenderPaymentResult;
.source "TenderPaymentResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PayOther"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "tenderName",
        "",
        "restartSelectMethodData",
        "Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V",
        "getRestartSelectMethodData",
        "()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "getTender",
        "()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "getTenderName",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

.field private final tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field private final tenderName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V
    .locals 1

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restartSelectMethodData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 128
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/TenderPaymentResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;ILjava/lang/Object;)Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->copy(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;
    .locals 1

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restartSelectMethodData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iget-object v1, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    iget-object p1, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getRestartSelectMethodData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-object v0
.end method

.method public final getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    return-object v0
.end method

.method public final getTenderName()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PayOther(tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->tenderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", restartSelectMethodData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
