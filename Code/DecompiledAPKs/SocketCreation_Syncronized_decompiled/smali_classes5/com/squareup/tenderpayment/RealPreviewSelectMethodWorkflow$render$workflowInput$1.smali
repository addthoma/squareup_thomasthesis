.class final Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealPreviewSelectMethodWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->render(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPreviewSelectMethodWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPreviewSelectMethodWorkflow.kt\ncom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1\n*L\n1#1,284:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

.field final synthetic this$0:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Sink;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->this$0:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p3, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p4, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$state:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->invoke(Lcom/squareup/tenderpayment/SelectMethod$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/SelectMethod$Event;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 120
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->this$0:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$state:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->access$showSecondary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected SecondaryTendersSelected from screenKey: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 124
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 125
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->this$0:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->access$exit(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    :cond_2
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->this$0:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$state:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->access$showPrimary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 127
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected BackPressed from screenKey: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 130
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected workflow input event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
