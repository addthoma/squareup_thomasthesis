.class public final Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;
.super Ljava/lang/Object;
.source "TenderSettingsFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/TenderSettingsFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTenderSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderSettingsManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/payment/Transaction;)Lcom/squareup/tenderpayment/TenderSettingsFactory;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/tenderpayment/TenderSettingsFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/TenderSettingsFactory;-><init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/payment/Transaction;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/TenderSettingsFactory;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/TenderSettingsManager;

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v3, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    iget-object v4, p0, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->newInstance(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/payment/Transaction;)Lcom/squareup/tenderpayment/TenderSettingsFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/TenderSettingsFactory_Factory;->get()Lcom/squareup/tenderpayment/TenderSettingsFactory;

    move-result-object v0

    return-object v0
.end method
