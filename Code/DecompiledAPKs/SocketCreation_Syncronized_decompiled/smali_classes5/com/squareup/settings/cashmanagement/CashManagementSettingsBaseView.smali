.class public abstract Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;
.super Landroid/widget/LinearLayout;
.source "CashManagementSettingsBaseView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private cashManagementOptionsContainer:Landroid/view/ViewGroup;

.field private cashMangementToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private defaultStartingCash:Lcom/squareup/widgets/SelectableEditText;

.field private emailRecipientContainer:Landroid/view/ViewGroup;

.field private emailRecipientField:Landroid/widget/EditText;

.field private emailReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private printReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private scrollView:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Landroid/view/ViewGroup;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashManagementOptionsContainer:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Landroid/widget/ScrollView;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->scrollView:Landroid/widget/ScrollView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Landroid/widget/EditText;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientField:Landroid/widget/EditText;

    return-object p0
.end method


# virtual methods
.method protected abstract enableAutoEmailToggled(Z)V
.end method

.method protected abstract enableCashManagementToggled(Z)V
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 141
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
.end method

.method public getDefaultStartingCash()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->defaultStartingCash:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v0, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getEmailRecipient()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getMoneyFormatter()Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
.end method

.method public isCashManagementEnabledChecked()Z
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashMangementToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method public isEmailReportEnabled()Z
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method public isPrintReportEnabled()Z
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->printReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$CashManagementSettingsBaseView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 49
    invoke-virtual {p0, p2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->enableCashManagementToggled(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$CashManagementSettingsBaseView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 57
    invoke-virtual {p0, p2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->enableAutoEmailToggled(Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 67
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 46
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->scroll_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->scrollView:Landroid/widget/ScrollView;

    .line 47
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->cash_management_enable_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashMangementToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 48
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashMangementToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/settings/cashmanagement/-$$Lambda$CashManagementSettingsBaseView$708XGL-J2W6wIUF7QD1uPLW2TPA;

    invoke-direct {v1, p0}, Lcom/squareup/settings/cashmanagement/-$$Lambda$CashManagementSettingsBaseView$708XGL-J2W6wIUF7QD1uPLW2TPA;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 51
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->default_starting_cash:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->defaultStartingCash:Lcom/squareup/widgets/SelectableEditText;

    .line 52
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->defaultStartingCash:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-static {v3, v4, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->defaultStartingCash:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 55
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->auto_email_enable_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 56
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/settings/cashmanagement/-$$Lambda$CashManagementSettingsBaseView$E6CLHU0ODCZH_s_z61bUG8cHVTQ;

    invoke-direct {v1, p0}, Lcom/squareup/settings/cashmanagement/-$$Lambda$CashManagementSettingsBaseView$E6CLHU0ODCZH_s_z61bUG8cHVTQ;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 59
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->auto_print_enable_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->printReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 61
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->options_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashManagementOptionsContainer:Landroid/view/ViewGroup;

    .line 62
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->cash_management_email_recipient:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientField:Landroid/widget/EditText;

    .line 63
    sget v0, Lcom/squareup/settings/cashmanagement/R$id;->email_recipient_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method public scrollToBottom()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashManagementOptionsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    new-instance v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;

    invoke-direct {v1, p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    .line 123
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    return-void
.end method

.method public setCashManagementEnabled(Z)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashMangementToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 97
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->cashManagementOptionsContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setDefaultStartingCash(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->defaultStartingCash:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setEmailRecipient(Ljava/lang/String;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setEmailRecipientContainerVisible(Z)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setEmailReportEnabled(Z)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 106
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->emailRecipientContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setPrintReportEnabled(Z)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->printReportToggleRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method
