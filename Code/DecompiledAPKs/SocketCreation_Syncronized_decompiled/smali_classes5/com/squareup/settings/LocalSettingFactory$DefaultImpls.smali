.class public final Lcom/squareup/settings/LocalSettingFactory$DefaultImpls;
.super Ljava/lang/Object;
.source "LocalSettingFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/LocalSettingFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic create$default(Lcom/squareup/settings/LocalSettingFactory;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 10
    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/settings/LocalSettingFactory;->create(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: create"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
