.class public Lcom/squareup/settings/server/VideoUrlSettings;
.super Ljava/lang/Object;
.source "VideoUrlSettings.java"


# instance fields
.field private final accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/settings/server/VideoUrlSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method


# virtual methods
.method public getR12GettingStartedVideoYoutubeId()Ljava/lang/String;
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/settings/server/VideoUrlSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    const-string v1, "r12_getting_started_video_url"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hasR12GettingStartedVideoInfo()Z
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/settings/server/VideoUrlSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
