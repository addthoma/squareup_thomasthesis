.class public Lcom/squareup/settings/server/Subscriptions;
.super Ljava/lang/Object;
.source "Subscriptions.java"


# static fields
.field private static final ACTIVE_STATE:Ljava/lang/String; = "active"

.field private static final ACTIVE_SUBSCRIPTION_STATES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ALL_SUBSCRIPTION_STATES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final EM_PRODUCT_KEY:Ljava/lang/String; = "employee_management"

.field private static final FREE_TRIAL_STATE:Ljava/lang/String; = "free_trial"

.field private static final INACTIVE_STATE:Ljava/lang/String; = "canceled"

.field private static final RST_PRODUCT_KEY:Ljava/lang/String; = "restaurants"


# instance fields
.field private final response:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 21
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "active"

    const-string v2, "free_trial"

    filled-new-array {v2, v1}, [Ljava/lang/String;

    move-result-object v3

    .line 22
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/Subscriptions;->ACTIVE_SUBSCRIPTION_STATES:Ljava/util/Set;

    .line 24
    new-instance v0, Ljava/util/HashSet;

    const-string v3, "canceled"

    filled-new-array {v2, v1, v3}, [Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/Subscriptions;->ALL_SUBSCRIPTION_STATES:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/settings/server/Subscriptions;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method

.method private hasSubscriptionInState(Ljava/lang/String;Ljava/util/Set;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/settings/server/Subscriptions;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/Subscription;

    .line 50
    iget-object v2, v1, Lcom/squareup/server/account/protos/Subscription;->product_key:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/squareup/server/account/protos/Subscription;->status:Ljava/lang/String;

    .line 51
    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public hasActiveEmployeeManagementSubscription()Z
    .locals 2

    .line 44
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "active"

    .line 45
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 44
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "employee_management"

    invoke-direct {p0, v1, v0}, Lcom/squareup/settings/server/Subscriptions;->hasSubscriptionInState(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public hasOrHadRestaurantsSubscription()Z
    .locals 2

    .line 38
    sget-object v0, Lcom/squareup/settings/server/Subscriptions;->ALL_SUBSCRIPTION_STATES:Ljava/util/Set;

    const-string v1, "restaurants"

    invoke-direct {p0, v1, v0}, Lcom/squareup/settings/server/Subscriptions;->hasSubscriptionInState(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public hasRestaurantSubscription()Z
    .locals 2

    .line 34
    sget-object v0, Lcom/squareup/settings/server/Subscriptions;->ACTIVE_SUBSCRIPTION_STATES:Ljava/util/Set;

    const-string v1, "restaurants"

    invoke-direct {p0, v1, v0}, Lcom/squareup/settings/server/Subscriptions;->hasSubscriptionInState(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v0

    return v0
.end method
