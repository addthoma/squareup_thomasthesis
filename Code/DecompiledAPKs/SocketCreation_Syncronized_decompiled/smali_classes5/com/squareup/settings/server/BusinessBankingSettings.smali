.class public Lcom/squareup/settings/server/BusinessBankingSettings;
.super Ljava/lang/Object;
.source "BusinessBankingSettings.java"


# instance fields
.field public final accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/settings/server/BusinessBankingSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method

.method private getBusinessBanking()Lcom/squareup/server/account/protos/BusinessBanking;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/settings/server/BusinessBankingSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    return-object v0
.end method


# virtual methods
.method public showCardSpend()Z
    .locals 1

    .line 16
    invoke-direct {p0}, Lcom/squareup/settings/server/BusinessBankingSettings;->getBusinessBanking()Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, v0, Lcom/squareup/server/account/protos/BusinessBanking;->show_card_spend:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
