.class public final enum Lcom/squareup/settings/server/Features$Feature;
.super Ljava/lang/Enum;
.source "Features.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/Features;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/Features$Feature$DeviceType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/server/Features$Feature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ACCEPT_THIRD_PARTY_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ACCOUNT_FREEZE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ACCOUNT_FREEZE_BANNER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ACCOUNT_FREEZE_NOTIFICATIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ADJUST_INVENTORY_AFTER_ORDER_CANCELATION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ALLOW_EXIT_JAIL_SCREEN_ON_ERROR:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ALWAYS_SHOW_ITEMIZED_CART:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_ANDROID_EDIT_RECURRING_APPOINTMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_ANDROID_RESOURCE_BOOKING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_APPOINTMENT_LOCATION_EDIT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_CONFIRMATIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_CONFIRMATIONS_API:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_CONFIRMATIONS_SQUARE_ASSISTANT_EDU_MODAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_FORCE_SYNC:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_LIST_OF_EVENTS_VIEW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_MULTISTAFF_CALENDAR:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_STAFF_HOURS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_USE_PAGINATED_SYNCING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum APPOINTMENTS_USE_SHORT_IDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ATTEMPT_DEVICE_ID_UPDATE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum AUTO_CAPTURE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum AVAILABILITY_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BANK_ACCOUNT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BANK_LINKING_WITH_TWO_ACCOUNTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BANK_LINK_IN_APP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BANK_LINK_POST_SIGNUP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BANK_RESEND_EMAIL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_ACTIVATION_BILLING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_BALANCE_APPLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_CAN_MANAGE_SQUARE_CARD:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_CARD_FREEZE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_COLLECT_MOBILE_PHONE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_GOOGLE_PAY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_SHOW_NOTIFICATION_PREFERENCES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_SHOW_SQUARE_CARD_PAN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_SHOW_UNIFIED_ACTIVITY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_SQUARE_CARD_SHOW_RESET_PIN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_SQUARE_CARD_UPSELL_IN_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_STAMPS_CUSTOMIZATION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BRAN_CART_SCROLL_LOGGING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BRAN_DISPLAY_API:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BRAN_DISPLAY_CART_MONITOR_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BRAN_MULTIPLE_IMAGES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BUNDLE_LOGGING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_ALLOW_SWIPE_FOR_CHIP_CARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_ALWAYS_SKIP_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_CHANGE_EMPLOYEE_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_CHANGE_OT_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_ENABLE_TIPPING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_ORDER_R12:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_PAUSE_NIGHTLY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_PRINT_COMPACT_TICKETS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_PRINT_SINGLE_TICKET_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SEE_COVER_COUNTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SEE_PAYROLL_UPSELL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SHOW_SMS_MARKETING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SHOW_SMS_MARKETING_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SMALL_RED_SEARCH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_SPLIT_EMONEY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_ANDROID_ECR:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_BLE_SCALES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_CASH_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_CASH_QR_CODES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_INSTALLMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAN_USE_TENDER_IN_EDIT_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAPITAL_CAN_MANAGE_FLEX_OFFER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CAPITAL_CAN_MANAGE_FLEX_PLAN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CARD_NOT_PRESENT_SIGN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CARD_ON_FILE_DIP_CRM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CARD_ON_FILE_DIP_CRM_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CARD_ON_FILE_DIP_POST_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CARD_ON_FILE_SIGN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CHECKOUT_APPLET_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CLOCK_SKEW_LOCKOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COLLECT_CNP_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COLLECT_COF_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COLLECT_TIP_BEFORE_AUTH_PREFERRED:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CONDITIONAL_TAXES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COUNTRY_PREFERS_EMV:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_ADHOC_FILTERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_BUYER_EMAIL_COLLECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_CAN_CONFIGURE_PROFILES_ANDROID:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_DELETE_GROUPS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_DISABLE_BUYER_PROFILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_GROUPS_AND_FILTERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_MERGE_CONTACTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CUSTOMERS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CUSTOMERS_APPLET_TABLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DEPOSITS_ALLOW_ADD_MONEY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DEPOSITS_REPORT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DEPOSITS_REPORT_CARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DEPOSITS_REPORT_DETAIL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DEPOSIT_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISABLE_ITEM_EDITING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISABLE_MAGSTRIPE_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISALLOW_ITEMSFE_INVENTORY_API:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISMISS_REFERRAL_BADGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISMISS_TUTORIAL_BADGES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISPLAY_LEARN_MORE_R4:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISPUTES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DISPUTES_CHALLENGES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DO_NOT_USE_TASK_QUEUE_FOR_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum DUPLICATE_SKU_ON_ITEMS_APPLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EGIFT_CARD_IN_POS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EGIFT_CARD_SETTINGS_IN_POS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EMAIL_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EMONEY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EMONEY_SPEED_TEST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_BUY_LINKS_SILENT_AUTH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_EPSON_PRINTERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_PAY_LINKS_ON_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_SERVICES_CATALOG_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_TAX_BASIS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ESTIMATE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FAST_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FEATURE_TOUR_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FEE_TRANSPARENCY_CNP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FEE_TRANSPARENCY_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FIRMWARE_UPDATE_JAIL_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FIXED_PRICE_SERVICE_PRICE_EDITING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum FORCED_OFFLINE_MODE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum GIFT_CARDS_REFUNDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum GIFT_CARDS_SHOW_HISTORY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum GIFT_CARDS_THIRD_PARTY_IME:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum GIFT_CARDS_THIRD_PARTY_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HARDWARE_SECURE_TOUCH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HAS_BAZAAR_ONLINE_STORE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HELP_APPLET_JEDI:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HELP_APPLET_JEDI_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HELP_APPLET_JEDI_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HELP_APPLET_JEDI_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HELP_APPLET_SUPPORT_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HELP_APPLET_SUPPORT_MESSAGING_TABLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HIDE_ANNOUNCEMENTS_SECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HIDE_MODIFIERS_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum IGNORE_ACCESSIBILITY_SCRUBBER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVENTORY_AVAILABILITY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVENTORY_PLUS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_ALLOW_PDF_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_APPLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_APPLET_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_APP_ONBOARDING_FLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_AUTOMATIC_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_AUTOMATIC_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_CUSTOM_REMINDER_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_DOWNLOAD_INVOICE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_EDIT_ESTIMATE_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_EDIT_V1_DEPRECATION_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_ESTIMATES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_ESTIMATES_PACKAGES_READONLY_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_EVENT_TIMELINE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_HOME_TAB:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_PARTIALLY_PAY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_PREVIEW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_RATING_PROMPT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_RECORD_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_RECURRING_CANCEL_NEXT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_TIMELINE_CTA_LINKING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_VISIBLE_PUSH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_WITH_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICES_WITH_MODIFIERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum INVOICE_APP_BANNER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum IPOS_NO_IDV_EXPERIMENT_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ITEM_OPTION_EDIT_MASTER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ITEM_OPTION_GLOBAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ITEM_OPTION_LOCAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum JUMBOTRON_SERVICE_KEY_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LANDING_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LANDING_WORLD:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LIBRARY_LIST_SHOW_SERVICES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LIMIT_VARIATIONS_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_ADJUST_POINTS_SMS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_ALPHANUMERIC_COUPONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_CAN_ADJUST_POINTS_NEGATIVE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_CASH_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_CHECKOUT_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_ENROLLMENT_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_EXPIRE_POINTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_HANDLE_RETURN_ITEMS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_ITEM_SELECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_TUTORIALS_ENROLLMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum LOYALTY_X2_POST_TRANSACTION_DARK_THEME:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum MANDATORY_BREAK_COMPLETION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum MULTIPLE_WAGES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum MULTIPLE_WAGES_BETA:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum O1_DEPRECATION_WARNING_JP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_ALLOW_SCREENSHOT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_ASK_INTENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_BANK:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_CHOOSE_DEFAULT_DEPOSIT_METHOD:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_FREE_READER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_QUOTA:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_REFERRAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_SERVER_DRIVEN_FLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_SUPPRESS_AUTO_FPT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ONBOARDING_X2_VERTICAL_SELECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_BULK_DELETE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_DIP_TAP_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_MERGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_SHOW_BETA_OPT_IN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_SWIPE_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_TRANSFER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_V2_NAME_AND_NOTES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_ALLOW_BAZAAR_ORDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_CAN_SEARCH_ORDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_CHECK_EMPLOYEE_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_SHOW_QUICK_ACTIONS_SETTING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_SUPPORTS_ECOM_DELIVERY_ORDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_SUPPORTS_UPCOMING_ORDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERHUB_THROTTLE_SEARCH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PAPER_SIGNATURE_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PERFORM_CATALOG_SYNC_AFTER_TRANSACTION_ON_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PERSISTENT_BUNDLE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PLAY_PAYMENT_APPROVED_SOUND_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PORTRAIT_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PREDEFINED_TICKETS_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PREDEFINED_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PREDEFINED_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PREFER_SQLITE_TASKS_QUEUE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PRINTER_DITHERING_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PRINTING_DEBUG_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PRINT_ITEMIZED_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PRINT_MERCHANT_LOGO_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PRINT_TAX_PERCENTAGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum R12_EARLY_K400_POWERUP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_209030_QUICKCHIP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_ACCOUNT_TYPE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_COMMON_DEBIT_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_FELICA_NOTIFICATION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_PINBLOCK_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_FW_SPOC_PRNG_SEED:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum READER_ORDER_HISTORY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum RECEIPTS_PRINT_DISPOSITION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REDUCE_PRINTING_WASTE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REFERRAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REMOTELY_CLOSE_OPEN_CASH_DRAWERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REORDER_READER_IN_APP_EMAIL_CONFIRM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REPORTS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REPORTS_SEE_MEASUREMENT_UNIT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REQUEST_BUSINESS_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REQUEST_BUSINESS_ADDRESS_BADGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REQUIRES_TRACK_2_IF_NOT_AMEX:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum RESILIENT_BUS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum RESTART_APP_AFTER_CRASH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum RETAIN_PRINTER_CONNECTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum REWARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum S2_HODOR:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SALES_LIMITS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SALES_REPORT_V2_FEATURE_CAROUSEL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SALES_SUMMARY_CHARTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SAMPLE_APPLET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SANITIZE_EVENTSTREAM_COORDINATES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SEPARATED_PRINTOUTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SETUP_GUIDE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_ACCIDENTAL_CASH_MODAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_BUY_BUTTON:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_CHECKOUT_LINKS_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_CP_PRICING_CHANGE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_FEE_BREAKDOWN_TABLE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_INCLUSIVE_TAXES_IN_CART:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_LOYALTY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_LOYALTY_VALUE_METRICS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_NOHO_RECEIPT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_PAY_ONLINE_TENDER_OPTION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_SPOC_VERSION_NUMBER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_WEEKEND_BALANCE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SHOW_WEEKVIEW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SKIP_MODIFIER_DETAIL_SCREEN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SMS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SPE_FWUP_CRQ:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SPE_TMS_LOGIN_CHECK:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SPLIT_TICKET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SPM_SQUID:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SPM_SQUID_LOG_TO_ES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SQUARE_ASSISTANT_CONVERSATIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SQUID_PTS_COMPLIANCE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum STATUS_BAR_FULLSCREEN_T2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum SWIPE_INSTANT_DEPOSIT_CARD:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TERMINAL_API:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TERMINAL_API_SPM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TIMECARDS_RECEIPT_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TIMECARDS_TEAM_MEMBER_NOTES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TIME_TRACKING_DEVICE_LEVEL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TMN_RECORD_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum TMN_VERBOSE_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum UPLOAD_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum UPLOAD_SUPPORT_LEDGER_STREAM:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum UPLOAD_X2_WIFI_EVENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_ACCESSIBLE_PIN_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_ALTERNATE_SALES_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_API_URL_LIST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_AUTH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_AUTOMATIC_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_BREAK_TRACKING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_BUYER_DISPLAY_SETTINGS_V2_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_CARD_ON_FILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_CARD_ON_FILE_ON_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_CASH_OUT_REASON:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_CASH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_COMP_VOID_ASSIGN_MOVE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_CONFIGURED_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_DEPOSIT_SETTINGS_CARD_LINKING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_FILE_SIZE_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_GIFT_CARDS_V2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_IN_APP_TIMECARDS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_ITEMIZED_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_ITEM_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_LOCAL_PAPER_SIGNATURE_SETTING:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_POS_INTENT_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_R12:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_R6:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SAFETYNET:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SAME_DAY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SELECT_METHOD_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SELECT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SPLIT_TENDER:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SPLIT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_SPLIT_TICKET_RST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_TABLE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_V2_BLE_STATE_MACHINE:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_V2_CARDREADERS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_V3_CATALOG:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum USE_X2_PAYMENT_WORKFLOWS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum VALIDATE_SESSION_ON_START:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum VERIFY_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum VOID_COMP:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum WAIT_FOR_BRAN_X2:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ZERO_ARQC_TEST:Lcom/squareup/settings/server/Features$Feature;

.field public static final enum ZERO_TENDER:Lcom/squareup/settings/server/Features$Feature;


# instance fields
.field public final description:Ljava/lang/String;

.field private final deviceType:Lcom/squareup/settings/server/Features$Feature$DeviceType;

.field private final name:Ljava/lang/String;

.field private final prereqs:[Lcom/squareup/settings/server/Features$Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 34

    .line 33
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v3, "ACCEPT_THIRD_PARTY_GIFT_CARDS"

    const-string v4, "Allow specified merchants to accept gift cards that were not issued by Square."

    invoke-direct {v0, v3, v1, v4, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ACCEPT_THIRD_PARTY_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 35
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v3, 0x1

    const-string v4, "ACCOUNT_FREEZE"

    const-string v5, "Display account freeze status via badge and in deposits applet. (Master Switch)"

    invoke-direct {v0, v4, v3, v5, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE:Lcom/squareup/settings/server/Features$Feature;

    .line 37
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v4, 0x2

    const-string v5, "ACCOUNT_FREEZE_BANNER"

    const-string v6, "Display account freeze status via banner and alert in order entry applet."

    invoke-direct {v0, v5, v4, v6, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_BANNER:Lcom/squareup/settings/server/Features$Feature;

    .line 39
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v5, 0x3

    const-string v6, "ACCOUNT_FREEZE_NOTIFICATIONS"

    const-string v7, "Display account freeze status via Android notifications."

    invoke-direct {v0, v6, v5, v7, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_NOTIFICATIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 41
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v6, 0x4

    const-string v7, "ADJUST_INVENTORY_AFTER_ORDER_CANCELATION"

    const-string v8, "Perform manual inventory adjustment after line items are canceled in Order Hub."

    invoke-direct {v0, v7, v6, v8, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ADJUST_INVENTORY_AFTER_ORDER_CANCELATION:Lcom/squareup/settings/server/Features$Feature;

    .line 43
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v7, 0x5

    const-string v8, "ALLOW_EXIT_JAIL_SCREEN_ON_ERROR"

    const-string v9, "Add a discard button on the JailScreen when there is an error to allow exiting the sign-in flow."

    invoke-direct {v0, v8, v7, v9, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ALLOW_EXIT_JAIL_SCREEN_ON_ERROR:Lcom/squareup/settings/server/Features$Feature;

    .line 46
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v8, 0x6

    const-string v9, "ALWAYS_SHOW_ITEMIZED_CART"

    const-string v10, "Always show the itemized cart screen during buyer checkout, regardless of the number of items in the cart."

    invoke-direct {v0, v9, v8, v10, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ALWAYS_SHOW_ITEMIZED_CART:Lcom/squareup/settings/server/Features$Feature;

    .line 48
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/4 v9, 0x7

    const-string v10, "APPOINTMENTS_FORCE_SYNC"

    const-string v11, "Display the Sync Button on APOS"

    invoke-direct {v0, v10, v9, v11, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_FORCE_SYNC:Lcom/squareup/settings/server/Features$Feature;

    .line 49
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v10, 0x8

    const-string v11, "APPOINTMENTS_BUSINESS_HOURS"

    const-string v12, "Enables Sellers to view and edit their Business Hours on the Android platform."

    invoke-direct {v0, v11, v10, v12, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

    .line 51
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v11, 0x9

    const-string v12, "APPOINTMENTS_STAFF_HOURS"

    const-string v13, "Enables Sellers to view and edit Staff Hours on the Android platform."

    invoke-direct {v0, v12, v11, v13, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_STAFF_HOURS:Lcom/squareup/settings/server/Features$Feature;

    .line 53
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v12, 0xa

    const-string v13, "APPOINTMENTS_APPOINTMENT_LOCATION_EDIT"

    const-string v14, "Enables Sellers to edit the Location of an Appointment on the Android platform."

    invoke-direct {v0, v13, v12, v14, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_APPOINTMENT_LOCATION_EDIT:Lcom/squareup/settings/server/Features$Feature;

    .line 55
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v13, 0xb

    const-string v14, "APPOINTMENTS_CONFIRMATIONS"

    const-string v15, "Enables Sellers to utilize the Appointment Confirmations feature on the Android platform."

    invoke-direct {v0, v14, v13, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 57
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v14, 0xc

    const-string v15, "APPOINTMENTS_CONFIRMATIONS_API"

    const-string v13, "Per-merchant feature flag for the Confirmations feature via the Appointments API."

    invoke-direct {v0, v15, v14, v13, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS_API:Lcom/squareup/settings/server/Features$Feature;

    .line 59
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v13, 0xd

    const-string v15, "APPOINTMENTS_CONFIRMATIONS_SQUARE_ASSISTANT_EDU_MODAL"

    const-string v14, "Enable the Confirmation & Square Assistant Educational Modal."

    invoke-direct {v0, v15, v13, v14, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS_SQUARE_ASSISTANT_EDU_MODAL:Lcom/squareup/settings/server/Features$Feature;

    .line 61
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const/16 v14, 0xe

    const-string v15, "APPOINTMENTS_GAP_TIME"

    const-string v13, "Enable Gap Time / Processing Time / Intermissions for services."

    invoke-direct {v0, v15, v14, v13, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

    .line 62
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "APPOINTMENTS_LIST_OF_EVENTS_VIEW"

    const/16 v15, 0xf

    const-string v14, "Enable list of events view."

    invoke-direct {v0, v13, v15, v14, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_LIST_OF_EVENTS_VIEW:Lcom/squareup/settings/server/Features$Feature;

    .line 63
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "APPOINTMENTS_ANDROID_EDIT_RECURRING_APPOINTMENTS"

    const/16 v14, 0x10

    const-string v15, "Enable creating and editing recurring appointments."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_ANDROID_EDIT_RECURRING_APPOINTMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 65
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "APPOINTMENTS_ANDROID_RESOURCE_BOOKING"

    const/16 v14, 0x11

    const-string v15, "Enables Resource Booking for Sellers enabling them to create, manage, and utilize Resources."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_ANDROID_RESOURCE_BOOKING:Lcom/squareup/settings/server/Features$Feature;

    .line 67
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SQUARE_ASSISTANT_CONVERSATIONS"

    const/16 v14, 0x12

    const-string v15, "Enable the Square Assistant Conversations Settings on Android."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SQUARE_ASSISTANT_CONVERSATIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 69
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "APPOINTMENTS_MULTISTAFF_CALENDAR"

    const/16 v14, 0x13

    const-string v15, "Show the multi staff calendar."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_MULTISTAFF_CALENDAR:Lcom/squareup/settings/server/Features$Feature;

    .line 70
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "APPOINTMENTS_USE_SHORT_IDS"

    const/16 v14, 0x14

    const-string v15, "Use short ids when creating appt objects"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_USE_SHORT_IDS:Lcom/squareup/settings/server/Features$Feature;

    .line 71
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "APPOINTMENTS_USE_PAGINATED_SYNCING"

    const/16 v14, 0x15

    const-string v15, "Should we paginate the initial sync"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_USE_PAGINATED_SYNCING:Lcom/squareup/settings/server/Features$Feature;

    .line 72
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ATTEMPT_DEVICE_ID_UPDATE"

    const/16 v14, 0x16

    const-string v15, "Should we attempt Multipass device id associations updates"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ATTEMPT_DEVICE_ID_UPDATE:Lcom/squareup/settings/server/Features$Feature;

    .line 73
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "AUTO_CAPTURE"

    const/16 v14, 0x17

    const-string v15, "Automatically capture abandoned card payments."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->AUTO_CAPTURE:Lcom/squareup/settings/server/Features$Feature;

    .line 74
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "AVAILABILITY_OVERRIDE"

    const/16 v14, 0x18

    const-string v15, "Enable Sellers to override their ability."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->AVAILABILITY_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    .line 75
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_BALANCE_APPLET"

    const/16 v14, 0x19

    const-string v15, "Kill switch for the Balance applet."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_BALANCE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    .line 76
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_CAN_MANAGE_SQUARE_CARD"

    const/16 v14, 0x1a

    const-string v15, "Used to disable Square card management."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CAN_MANAGE_SQUARE_CARD:Lcom/squareup/settings/server/Features$Feature;

    .line 77
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_STORED_BALANCE"

    const/16 v14, 0x1b

    const-string v15, "Merchant\'s balance is stored with Square."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    .line 78
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_CARD_FREEZE_TOGGLE"

    const/16 v14, 0x1c

    const-string v15, "Allow to freeze/unfreeze their Square Card."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CARD_FREEZE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    .line 79
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_GOOGLE_PAY"

    const/16 v14, 0x1d

    const-string v15, "Allow to add Square Card to Google Pay."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_GOOGLE_PAY:Lcom/squareup/settings/server/Features$Feature;

    .line 80
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_ACTIVATION_BILLING"

    const/16 v14, 0x1e

    const-string v15, "Display Billing Address during Activation."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_ACTIVATION_BILLING:Lcom/squareup/settings/server/Features$Feature;

    .line 81
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_STAMPS_CUSTOMIZATION"

    const/16 v14, 0x1f

    const-string v15, "Allow Stamps Customization."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STAMPS_CUSTOMIZATION:Lcom/squareup/settings/server/Features$Feature;

    .line 82
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_SQUARE_CARD_UPSELL_IN_DEPOSITS"

    const/16 v14, 0x20

    const-string v15, "Allow showing upsell in deposits for Square Card."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    .line 83
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS"

    const/16 v14, 0x21

    const-string v15, "Allow showing upsell in instant deposits for Square Card."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    .line 85
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_SQUARE_CARD_SHOW_RESET_PIN"

    const/16 v14, 0x22

    const-string v15, "Whether to show the Reset Pin menu option in dashboard."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_SHOW_RESET_PIN:Lcom/squareup/settings/server/Features$Feature;

    .line 86
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_SHOW_NOTIFICATION_PREFERENCES"

    const/16 v14, 0x23

    const-string v15, "Whether to show Notification Preferences screen."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_NOTIFICATION_PREFERENCES:Lcom/squareup/settings/server/Features$Feature;

    .line 87
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_COLLECT_MOBILE_PHONE"

    const/16 v14, 0x24

    const-string v15, "Flag will enable or disable collecting phone number after Square Card order flow for SMS notification settings."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_COLLECT_MOBILE_PHONE:Lcom/squareup/settings/server/Features$Feature;

    .line 89
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_SHOW_UNIFIED_ACTIVITY"

    const/16 v14, 0x25

    const-string v15, "Whether to show the new unified activity designs in place of Square Card spend and transfer report, else the old ones will be shown"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_UNIFIED_ACTIVITY:Lcom/squareup/settings/server/Features$Feature;

    .line 91
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BIZBANK_SHOW_SQUARE_CARD_PAN"

    const/16 v14, 0x26

    const-string v15, "Whether we should be displaying the Show Pan option on the Manager Square Card section."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_SQUARE_CARD_PAN:Lcom/squareup/settings/server/Features$Feature;

    .line 93
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BANK_ACCOUNT"

    const/16 v14, 0x27

    const-string v15, "Allow Bank Account to be viewed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BANK_ACCOUNT:Lcom/squareup/settings/server/Features$Feature;

    .line 94
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BANK_LINK_POST_SIGNUP"

    const/16 v14, 0x28

    const-string v15, "Enable bank linking after account activation."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_POST_SIGNUP:Lcom/squareup/settings/server/Features$Feature;

    .line 95
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BANK_LINK_IN_APP"

    const/16 v14, 0x29

    const-string v15, "Enable native bank linking in onboarding flow. When it\'s set to off, bank linking can only be done via web."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    .line 97
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BANK_LINKING_WITH_TWO_ACCOUNTS"

    const/16 v14, 0x2a

    const-string v15, "Enable viewing active and pending bank accounts in Settings > Bank Account"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BANK_LINKING_WITH_TWO_ACCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 99
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BANK_RESEND_EMAIL"

    const/16 v14, 0x2b

    const-string v15, "Show \"Resend Email\" button for bank accounts in AWAITING_EMAIL_CONFIRMATION state."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BANK_RESEND_EMAIL:Lcom/squareup/settings/server/Features$Feature;

    .line 101
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BARCODE_SCANNERS"

    const/16 v14, 0x2c

    const-string v15, "Enable barcode scanners to be used."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    .line 102
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BRAN_CART_SCROLL_LOGGING"

    const/16 v14, 0x2d

    const-string v15, "Enables logging to event stream when customers scroll the cart on Bran, with direction/duration."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BRAN_CART_SCROLL_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    .line 104
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BRAN_DISPLAY_API"

    const/16 v14, 0x2e

    const-string v15, "Enable the Bran Display API on X2."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_API:Lcom/squareup/settings/server/Features$Feature;

    .line 105
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_API:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "BRAN_DISPLAY_CART_MONITOR_WORKFLOW"

    const/16 v14, 0x2f

    const-string v15, "Allows using the Seller Cart Monitor Workflow with the Bran Display API."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_CART_MONITOR_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 108
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BRAN_MULTIPLE_IMAGES"

    const/16 v14, 0x30

    const-string v15, "Allow bran to display multiple merchant images."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BRAN_MULTIPLE_IMAGES:Lcom/squareup/settings/server/Features$Feature;

    .line 110
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT"

    const/16 v14, 0x31

    const-string v15, "When enabled, the payment prompt variations experiment from ABViz is applied. Else, it is ignored and the classic payment prompt is used."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 113
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BUNDLE_LOGGING"

    const/16 v14, 0x32

    const-string v15, "Extra logging for large bundles causing TransactionTooLargeException RA-20937"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BUNDLE_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    .line 115
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BUYER_CHECKOUT"

    const/16 v14, 0x33

    const-string v15, "Enable buyer checkout flow on top of CTO"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 116
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE"

    const/16 v14, 0x34

    const-string v15, "Enable displaying the transaction type in the buyer checkout payment prompt screen."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Lcom/squareup/settings/server/Features$Feature;

    .line 118
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CAN_ALLOW_SWIPE_FOR_CHIP_CARDS"

    const/16 v18, 0x35

    const-string v19, "Allow chip cards to be swiped"

    const-string v20, "Allow chip cards to be swiped\u2014even if your R12 is connected"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_ALLOW_SWIPE_FOR_CHIP_CARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 120
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_ALWAYS_SKIP_SIGNATURE"

    const/16 v14, 0x36

    const-string v15, "Enable device setting to always skipping signatures"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_ALWAYS_SKIP_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    .line 121
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_CHANGE_EMPLOYEE_DEFAULTS_RST"

    const/16 v14, 0x37

    const-string v15, "Whether we can change the default employee lock permissions for Restaurant subscribers"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_EMPLOYEE_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    .line 123
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_CHANGE_OT_DEFAULTS_RST"

    const/16 v14, 0x38

    const-string v15, "Whether we can change the default open ticket settings for Restaurant subscribers"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_OT_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    .line 125
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_ENABLE_TIPPING"

    const/16 v14, 0x39

    const-string v15, "Enable buyer to tip"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_ENABLE_TIPPING:Lcom/squareup/settings/server/Features$Feature;

    .line 126
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_PAUSE_NIGHTLY_DEPOSIT"

    const/16 v14, 0x3a

    const-string v15, "Allow sellers to pause nightly settlement."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_PAUSE_NIGHTLY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    .line 127
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SHOW_ORDER_READER_MENU"

    const/16 v14, 0x3b

    const-string v15, "Show Order Reader menu in app"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    .line 128
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "CAN_ORDER_R12"

    const/16 v14, 0x3c

    const-string v15, "Shows R12 reader in Order Reader menu in app"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R12:Lcom/squareup/settings/server/Features$Feature;

    .line 129
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "CAN_ORDER_R4"

    const/16 v14, 0x3d

    const-string v15, "Shows R4 reader in Order Reader menu in app"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

    .line 130
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CAN_PRINT_COMPACT_TICKETS"

    const/16 v18, 0x3e

    const-string v19, "Allows printing compact kitchen tickets"

    const-string v20, "Enables a printer station setting for trimming vertical space on printed kitchen tickets to save paper as well as make stickers smaller."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_COMPACT_TICKETS:Lcom/squareup/settings/server/Features$Feature;

    .line 135
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_PRINT_SINGLE_TICKET_PER_ITEM"

    const/16 v14, 0x3f

    const-string v15, "When enabled for a printer station, order tickets will print a separate ticket for each item."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_SINGLE_TICKET_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    .line 138
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SPLIT_EMONEY"

    const/16 v14, 0x40

    const-string v15, "Enable emoney payments for split transactions."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SPLIT_EMONEY:Lcom/squareup/settings/server/Features$Feature;

    .line 139
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_ANDROID_ECR"

    const/16 v14, 0x41

    const-string v15, "Whether or not the ECR feature is enabled"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_ANDROID_ECR:Lcom/squareup/settings/server/Features$Feature;

    .line 140
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_BUYER_LANGUAGE_SELECTION"

    const/16 v14, 0x42

    const-string v15, "Allow the buyer to specify the language for the buyer facing screens"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 142
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_CASH_MANAGEMENT"

    const/16 v14, 0x43

    const-string v15, "Whether we can show cash management features or not."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 143
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_CASH_QR_CODES"

    const/16 v14, 0x44

    const-string v15, "Allow for payments via a cash app QR code."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_QR_CODES:Lcom/squareup/settings/server/Features$Feature;

    .line 144
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_INSTALLMENTS"

    const/16 v14, 0x45

    const-string v15, "Shows Monthly Installments row in CTO"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_INSTALLMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 145
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CAN_USE_TENDER_IN_EDIT_REFACTOR"

    const/16 v18, 0x46

    const-string v19, "Use TenderInEdit refactor."

    const-string v20, "Used to determine if the Android POS client can use refactored TenderInEdit code within the payment flow.  Created due to the sensitive nature of making large changes to how the payment flow works."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_TENDER_IN_EDIT_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    .line 149
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_SCALES"

    const/16 v14, 0x47

    const-string v15, "Allows scales to be discovered."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    .line 150
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_USE_BLE_SCALES"

    const/16 v14, 0x48

    const-string v15, "Allows scales to be discovered over BLE. Only works if CAN_USE_SCALES is enabled."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BLE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    .line 152
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SEE_AUTO_GRATUITY"

    const/16 v14, 0x49

    const-string v15, "Whether merchants can see automatic gratuity information that has synced from RST POS"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    .line 154
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SEE_COVER_COUNTS"

    const/16 v14, 0x4a

    const-string v15, "Whether merchants can see cover counts in sales reports."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_COVER_COUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 155
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SEE_SEATING"

    const/16 v14, 0x4b

    const-string v15, "Whether merchants can see seating information that has synced from RST POS"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    .line 156
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CAN_SEE_PAYROLL_UPSELL"

    const/16 v18, 0x4c

    const-string v19, "Can see Payroll Upsell screen"

    const-string v20, "Allow displaying the screen inside Employee Management settings."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_PAYROLL_UPSELL:Lcom/squareup/settings/server/Features$Feature;

    .line 158
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SHOW_SMS_MARKETING"

    const/16 v14, 0x4d

    const-string v15, "Can show an SMS marketing opt-in screen for sellers without loyalty enabled after an SMS receipt was requested"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING:Lcom/squareup/settings/server/Features$Feature;

    .line 160
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SHOW_SMS_MARKETING_V2"

    const/16 v14, 0x4e

    const-string v15, "Can show v2 extension of sms marketing opt-in screen for sellers without loyalty enabled after any selection on noho receipt selection screen"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 162
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAN_SMALL_RED_SEARCH"

    const/16 v14, 0x4f

    const-string v15, "Can search item variations based on combined item and variation names"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SMALL_RED_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    .line 163
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAPITAL_CAN_MANAGE_FLEX_OFFER"

    const/16 v14, 0x50

    const-string v15, "Can manage Capital flex offer in Deposits Applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAPITAL_CAN_MANAGE_FLEX_OFFER:Lcom/squareup/settings/server/Features$Feature;

    .line 164
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CAPITAL_CAN_MANAGE_FLEX_PLAN"

    const/16 v14, 0x51

    const-string v15, "Can manage Capital flex plan in Deposits Applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CAPITAL_CAN_MANAGE_FLEX_PLAN:Lcom/squareup/settings/server/Features$Feature;

    .line 165
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CARD_NOT_PRESENT_SIGN"

    const/16 v18, 0x52

    const-string v19, "Card not present sign"

    const-string v20, "Collect signature for card not present transactions."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CARD_NOT_PRESENT_SIGN:Lcom/squareup/settings/server/Features$Feature;

    .line 167
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "CARD_ON_FILE_DIP_CRM"

    const/16 v23, 0x53

    const-string v24, "Can dip card for Card On File CRM flow"

    const-string v25, "Allow dipping a card to add it as a Card On File through the customer management flow"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM:Lcom/squareup/settings/server/Features$Feature;

    .line 169
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CARD_ON_FILE_DIP_CRM_X2"

    const/16 v18, 0x54

    const-string v19, "Can dip card for X2 Card On File CRM flow"

    const-string v20, "Allow dipping a card to add it as a Card On File via the X2 customer management flow"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 171
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "CARD_ON_FILE_DIP_POST_PAYMENT"

    const/16 v23, 0x55

    const-string v24, "Can dip card for post-payment Card On File flow"

    const-string v25, "Allow dipping a card to add it as a Card On File after the payment flow"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_POST_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 173
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CARD_ON_FILE_SIGN"

    const/16 v18, 0x56

    const-string v19, "Card on file sign"

    const-string v20, "Collect signature for card on file transactions."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_SIGN:Lcom/squareup/settings/server/Features$Feature;

    .line 174
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CHECKOUT_APPLET_V2"

    const/16 v14, 0x57

    const-string v15, "Checkout Applet v2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CHECKOUT_APPLET_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 175
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CLOCK_SKEW_LOCKOUT"

    const/16 v18, 0x58

    const-string v19, "Clock Skew Lockout"

    const-string v20, "Prevent the merchant from using POS if their device time differs from server time by more than an hour."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CLOCK_SKEW_LOCKOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 177
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "COLLECT_CNP_POSTAL_CODE"

    const/16 v14, 0x59

    const-string v15, "Collect postal code in CNP flow"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COLLECT_CNP_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    .line 178
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "COLLECT_COF_POSTAL_CODE"

    const/16 v14, 0x5a

    const-string v15, "Collect postal code in COF flow"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COLLECT_COF_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    .line 179
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "COLLECT_TIP_BEFORE_AUTH_PREFERRED"

    const/16 v18, 0x5b

    const-string v19, "Pre-auth tipping preferred"

    const-string v20, "Collect tip before auth where it doesn\'t change client flows. Only applies if \"pre-auth required\" is false."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_PREFERRED:Lcom/squareup/settings/server/Features$Feature;

    .line 182
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "COLLECT_TIP_BEFORE_AUTH_REQUIRED"

    const/16 v23, 0x5c

    const-string v24, "Pre-auth tipping required"

    const-string v25, "Always collect tip amount before auth."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    .line 184
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CONDITIONAL_TAXES"

    const/16 v14, 0x5d

    const-string v15, "Conditionally apply taxes to items based on dynamic item attributes."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CONDITIONAL_TAXES:Lcom/squareup/settings/server/Features$Feature;

    .line 185
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "COUNTRY_PREFERS_CONTACTLESS_CARDS"

    const/16 v18, 0x5e

    const-string v19, "Country prefers contactless cards"

    const-string v20, "Prefer contactless payments + cards in R12 first run tour."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 187
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "COUNTRY_PREFERS_NO_CONTACTLESS_CARDS"

    const/16 v23, 0x5f

    const-string v24, "Country prefers no contactless cards"

    const-string v25, "Prefer no contactless cards. Skip contactless payments in R12 first run tour."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 189
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "COUNTRY_PREFERS_EMV"

    const/16 v18, 0x60

    const-string v19, "Country prefers EMV"

    const-string v20, "Tweaks the UI in markets with high EMV penetration by replacing magstripe images and customizing R4 prompts."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_EMV:Lcom/squareup/settings/server/Features$Feature;

    .line 191
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "CRM_ADHOC_FILTERS"

    const/16 v23, 0x61

    const-string v24, "CRM Adhoc Filters"

    const-string v25, "Enables navigation from customer list to the Filters screen, selecting and applying filters."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_ADHOC_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    .line 193
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE"

    const/16 v18, 0x62

    const-string v19, "CRM Allows Coupons in Direct Message"

    const-string v20, "Enables merchants to attach coupons to a direct message"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    .line 195
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_BUYER_EMAIL_COLLECTION"

    const/16 v14, 0x63

    const-string v15, "Collect buyer emails in the checkout flow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_BUYER_EMAIL_COLLECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 196
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_CUSTOM_ATTRIBUTES"

    const/16 v14, 0x64

    const-string v15, "Display custom attributes in customer profile"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    .line 197
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_DISABLE_BUYER_PROFILE"

    const/16 v14, 0x65

    const-string v15, "Causes buyer profiles to appear blank"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_DISABLE_BUYER_PROFILE:Lcom/squareup/settings/server/Features$Feature;

    .line 198
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CRM_DELETE_GROUPS"

    const/16 v18, 0x66

    const-string v19, "CRM Delete Groups"

    const-string v20, "Enables group deletion, while keeping its customers."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_DELETE_GROUPS:Lcom/squareup/settings/server/Features$Feature;

    .line 199
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CRM_ADHOC_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v22, "CRM_GROUPS_AND_FILTERS"

    const/16 v23, 0x67

    const-string v24, "CRM Groups & Filters"

    const-string v25, "Enable new features and UI from the Customer Directory groups and filtering project."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_GROUPS_AND_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    .line 202
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CRM_LOCATION_FILTER"

    const/16 v18, 0x68

    const-string v19, "CRM Location Filter"

    const-string v20, "Enables the Visited Location filter."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

    .line 203
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "CRM_MANAGE_LOYALTY_IN_DIRECTORY"

    const/16 v23, 0x69

    const-string v24, "Loyalty and Directory Integration"

    const-string v25, "Allows the merchant to manage loyalty data in directory"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    .line 205
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CRM_MANUAL_GROUP_FILTER"

    const/16 v18, 0x6a

    const-string v19, "CRM Manual Group Filter"

    const-string v20, "Enables the Manual Group filter."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

    .line 206
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_MERGE_CONTACTS"

    const/16 v14, 0x6b

    const-string v15, "CRM Merge Contacts"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MERGE_CONTACTS:Lcom/squareup/settings/server/Features$Feature;

    .line 207
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_MESSAGING"

    const/16 v14, 0x6c

    const-string v15, "CRM Messaging"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    .line 208
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_NOTES"

    const/16 v14, 0x6d

    const-string v15, "CRM Notes"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

    .line 209
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_REMINDERS"

    const/16 v14, 0x6e

    const-string v15, "CRM Reminders"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 210
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "CRM_VISIT_FREQUENCY_FILTER"

    const/16 v18, 0x6f

    const-string v19, "CRM Visit Frequency Filter"

    const-string v20, "Enables the Visit Frequency filter."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

    .line 212
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CRM_REORDER_DEFAULT_PROFILE_FIELDS"

    const/16 v14, 0x70

    const-string v15, "Customer profile section uses the ordering defined in the Merchant Attribute Schema for all fields, including default fields."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    .line 214
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v17, "CRM_CAN_CONFIGURE_PROFILES_ANDROID"

    const/16 v18, 0x71

    const-string v19, "Can Configure Profiles"

    const-string v20, "Allow sellers to configure customer profiles in Android SPOS"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_CAN_CONFIGURE_PROFILES_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    .line 216
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v26, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "CUSTOMER_MANAGEMENT_MOBILE"

    const/16 v23, 0x72

    const-string v24, "Customer Management"

    const-string v25, ""

    move-object/from16 v21, v0

    move-object/from16 v27, v2

    invoke-direct/range {v21 .. v27}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 217
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v32, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v28, "CUSTOMER_MANAGEMENT_TABLET"

    const/16 v29, 0x73

    const-string v30, "Customer Management"

    const-string v31, ""

    move-object/from16 v27, v0

    move-object/from16 v33, v2

    invoke-direct/range {v27 .. v33}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 218
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CUSTOMERS_ACTIVITY_LIST"

    const/16 v14, 0x74

    const-string v15, "Display activity list in customer view"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    .line 219
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v21, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v17, "CUSTOMERS_APPLET_MOBILE"

    const/16 v18, 0x75

    const-string v19, "Customers Applet"

    const-string v20, ""

    move-object/from16 v16, v0

    move-object/from16 v22, v2

    invoke-direct/range {v16 .. v22}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 220
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v27, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v23, "CUSTOMERS_APPLET_TABLET"

    const/16 v24, 0x76

    const-string v25, "Customers Applet"

    const-string v26, ""

    move-object/from16 v22, v0

    move-object/from16 v28, v2

    invoke-direct/range {v22 .. v28}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 221
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "CUSTOMER_INVOICE_LINKING"

    const/16 v14, 0x77

    const-string v15, "Show customer specific invoice information"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    .line 222
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DEPOSIT_SETTINGS"

    const/16 v14, 0x78

    const-string v15, "Enable viewing Settings applet > Deposits."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DEPOSIT_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 223
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DEPOSITS_ALLOW_ADD_MONEY"

    const/16 v14, 0x79

    const-string v15, "Allow to add money from Deposits applet."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_ALLOW_ADD_MONEY:Lcom/squareup/settings/server/Features$Feature;

    .line 224
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DEPOSITS_REPORT"

    const/16 v14, 0x7a

    const-string v15, "Allow Deposits Report to be viewed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT:Lcom/squareup/settings/server/Features$Feature;

    .line 225
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DEPOSITS_REPORT_DETAIL"

    const/16 v14, 0x7b

    const-string v15, "Allow Deposits Report Detail to be viewed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_DETAIL:Lcom/squareup/settings/server/Features$Feature;

    .line 226
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DEPOSITS_REPORT_CARD_PAYMENTS"

    const/16 v14, 0x7c

    const-string v15, "Allow Card Payments in Deposits Report Detail to be viewed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_CARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 227
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DEVICE_SETTINGS"

    const/16 v14, 0x7d

    const-string v15, "Use Device Settings when logged in with device code"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 228
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DO_NOT_USE_TASK_QUEUE_FOR_PAYMENTS"

    const/16 v14, 0x7e

    const-string v15, "Do not use the task queue for payments"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DO_NOT_USE_TASK_QUEUE_FOR_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 229
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS"

    const/16 v14, 0x7f

    const-string v15, "Use Diagnostic Data Reporter for 2 finger bug reports."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Lcom/squareup/settings/server/Features$Feature;

    .line 232
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DINING_OPTIONS"

    const/16 v14, 0x80

    const-string v15, "Allow Dining Options to be set and viewed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 233
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISABLE_ITEM_EDITING"

    const/16 v14, 0x81

    const-string v15, "Disable item editing in legacy Register clients"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISABLE_ITEM_EDITING:Lcom/squareup/settings/server/Features$Feature;

    .line 234
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISABLE_MAGSTRIPE_PROCESSING"

    const/16 v14, 0x82

    const-string v15, "Disable processing magsstripe payments for SPOC compliance"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISABLE_MAGSTRIPE_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    .line 235
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISALLOW_ITEMSFE_INVENTORY_API"

    const/16 v14, 0x83

    const-string v15, "Disallow the use of itemsfe inventory API"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISALLOW_ITEMSFE_INVENTORY_API:Lcom/squareup/settings/server/Features$Feature;

    .line 236
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISCOUNT_APPLY_MULTIPLE_COUPONS"

    const/16 v14, 0x84

    const-string v15, "Allow stacking coupons of the same discount."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 237
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISMISS_REFERRAL_BADGE"

    const/16 v14, 0x85

    const-string v15, "Dismiss badge for referrals. Used by appointments."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISMISS_REFERRAL_BADGE:Lcom/squareup/settings/server/Features$Feature;

    .line 238
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISMISS_TUTORIAL_BADGES"

    const/16 v14, 0x86

    const-string v15, "Dismiss badges for tutorials. Used by appointments."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISMISS_TUTORIAL_BADGES:Lcom/squareup/settings/server/Features$Feature;

    .line 239
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISPLAY_LEARN_MORE_R4"

    const/16 v14, 0x87

    const-string v15, "Enables R4 row in Learn More section of Card Readers Settings"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_LEARN_MORE_R4:Lcom/squareup/settings/server/Features$Feature;

    .line 240
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISPLAY_MODIFIER_INSTEAD_OF_OPTION"

    const/16 v14, 0x88

    const-string v15, "On Dashboard and POS, whether to replace some \"options\" with \"modifiers\"."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Lcom/squareup/settings/server/Features$Feature;

    .line 242
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISPUTES"

    const/16 v14, 0x89

    const-string v15, "Merchant can see disputes in Reports applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISPUTES:Lcom/squareup/settings/server/Features$Feature;

    .line 243
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DISPUTES_CHALLENGES"

    const/16 v14, 0x8a

    const-string v15, "Merchant can challenge disputes in-app"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DISPUTES_CHALLENGES:Lcom/squareup/settings/server/Features$Feature;

    .line 244
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "DUPLICATE_SKU_ON_ITEMS_APPLET"

    const/16 v14, 0x8b

    const-string v15, "Show all the matching variations when sellers scan a duplicate SKU on all items screen"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->DUPLICATE_SKU_ON_ITEMS_APPLET:Lcom/squareup/settings/server/Features$Feature;

    .line 246
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "ELIGIBLE_FOR_SQUARE_CARD_PROCESSING"

    const/16 v18, 0x8c

    const-string v19, "Square Processing"

    const-string v20, "Eligible to process swipes and CNP transactions."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    .line 248
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING"

    const/16 v23, 0x8d

    const-string v24, "Third Party Card"

    const-string v25, "Eligible to record third party card transactions."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    .line 250
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_BUY_LINKS_SILENT_AUTH"

    const/16 v14, 0x8e

    const-string v15, "Used to enable silent onboarding for Online Checkout buy links checkout flow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_BUY_LINKS_SILENT_AUTH:Lcom/squareup/settings/server/Features$Feature;

    .line 252
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_EPSON_PRINTERS"

    const/16 v14, 0x8f

    const-string v15, "Enable epson usb/wifi printers."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_EPSON_PRINTERS:Lcom/squareup/settings/server/Features$Feature;

    .line 253
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_FELICA_CERTIFICATION_ENVIRONMENT"

    const/16 v14, 0x90

    const-string v15, "Enable felica cert environment by attaching a custom header to felica proxy network calls"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 255
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS"

    const/16 v14, 0x91

    const-string v15, "This feature enables sellers to create, view and edit donation links from Online Checkout settings."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 257
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_PAY_LINKS_ON_CHECKOUT"

    const/16 v14, 0x92

    const-string v15, "Used to enable Online Checkout for pay links."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_PAY_LINKS_ON_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 258
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_RATE_BASED_SERVICES"

    const/16 v14, 0x93

    const-string v15, "Enable rate-based services (services with fractional quantities) to be created and added to a cart"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    .line 261
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENABLE_SERVICES_CATALOG_INTEGRATION"

    const/16 v14, 0x94

    const-string v15, "Enable services catalog integration for all verticals"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_SERVICES_CATALOG_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    .line 262
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "ENABLE_TAX_BASIS"

    const/16 v18, 0x95

    const-string v19, "Enable Tax Basis"

    const-string v20, "Enables a toggle button in Discounts that allows a merchant to apply a discount before or after tax has been applied."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_TAX_BASIS:Lcom/squareup/settings/server/Features$Feature;

    .line 265
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "ENABLE_WHOLE_PURCHASE_DISCOUNTS"

    const/16 v23, 0x96

    const-string v24, "Enable whole purchase discounts"

    const-string v25, "Enables the application of whole purchase discounts via pricing engine"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 267
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENHANCED_TRANSACTION_SEARCH"

    const/16 v14, 0x97

    const-string v15, "Enable enhanced transaction search for Merchant."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    .line 268
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "EMAIL_SUPPORT_LEDGER"

    const/16 v18, 0x98

    const-string v19, "Email support ledger"

    const-string v20, "Enable emailing support ledger"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EMAIL_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    .line 269
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "EMONEY"

    const/16 v14, 0x99

    const-string v15, "Enable E-Money for Merchant."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EMONEY:Lcom/squareup/settings/server/Features$Feature;

    .line 270
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "EMONEY_SPEED_TEST"

    const/16 v14, 0x9a

    const-string v15, "Enable E-Money Speed Test For Merchant."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EMONEY_SPEED_TEST:Lcom/squareup/settings/server/Features$Feature;

    .line 271
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "EMPLOYEE_MANAGEMENT"

    const/16 v14, 0x9b

    const-string v15, "Allow employees to access Register with a passcode and for actions to be attributed to employees."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 273
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ENFORCE_LOCATION_FIX"

    const/16 v14, 0x9c

    const-string v15, "Won\'t let you use Register until we have a location fix"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

    .line 274
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "EPSON_DEBUGGING"

    const/16 v14, 0x9d

    const-string v15, "Enable verbose debugging and bugsnag thread dumps for epson printers"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    .line 275
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ESTIMATE_DEFAULTS_MOBILE"

    const/16 v14, 0x9e

    const-string v15, "Enables Estimate defaults"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ESTIMATE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 276
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FAST_CHECKOUT"

    const/16 v14, 0x9f

    const-string v15, "Allow merchants to skip receipt screen for fast checkout."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FAST_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 277
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FEATURE_TOUR_X2"

    const/16 v14, 0xa0

    const-string v15, "Tutorial to demonstrate new features on X2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FEATURE_TOUR_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 278
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FEE_TRANSPARENCY"

    const/16 v14, 0xa1

    const-string v15, "Show fee information overall feature"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    .line 279
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "FEE_TRANSPARENCY_CNP"

    const/16 v14, 0xa2

    const-string v15, "Show fee information on CNP screen"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_CNP:Lcom/squareup/settings/server/Features$Feature;

    .line 280
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "FEE_TRANSPARENCY_TUTORIAL"

    const/16 v14, 0xa3

    const-string v15, "Show fee information in tutorial"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    .line 281
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FIRMWARE_UPDATE_JAIL_T2"

    const/16 v14, 0xa4

    const-string v15, "Hold T2 in the Jail screen until it is known a firmware update is not needed or a firmware update is completed."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FIRMWARE_UPDATE_JAIL_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 283
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FIRST_PAYMENT_TUTORIAL_V2"

    const/16 v14, 0xa5

    const-string v15, "Replaces first card payment tutorial with one implemented uponthe v2 tutorial framework."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 285
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FIXED_PRICE_SERVICE_PRICE_EDITING"

    const/16 v14, 0xa6

    const-string v15, "Allows for editing a fixed price service variation price during cart building."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FIXED_PRICE_SERVICE_PRICE_EDITING:Lcom/squareup/settings/server/Features$Feature;

    .line 287
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "FORCED_OFFLINE_MODE"

    const/16 v14, 0xa7

    const-string v15, "Enable checking a server endpoint to determine if the client should be forced into offline mode."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->FORCED_OFFLINE_MODE:Lcom/squareup/settings/server/Features$Feature;

    .line 289
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "GIFT_CARDS_REFUNDS"

    const/16 v14, 0xa8

    const-string v15, "Refund a purchase paid for by a gift card to any gift card."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_REFUNDS:Lcom/squareup/settings/server/Features$Feature;

    .line 290
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "GIFT_CARDS_SHOW_HISTORY"

    const/16 v14, 0xa9

    const-string v15, "Display history of gift card along with the gift card balance."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_HISTORY:Lcom/squareup/settings/server/Features$Feature;

    .line 291
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS"

    const/16 v14, 0xaa

    const-string v15, "Shows dashboard pages and POS text specific to plastic gift cards"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 293
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "GIFT_CARDS_THIRD_PARTY_IME"

    const/16 v14, 0xab

    const-string v15, "Gates a change for IME behavior for third party gift card PAN entry in the PanEditor class. Only intended for X2/T2 since the change breaks Samsung"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_IME:Lcom/squareup/settings/server/Features$Feature;

    .line 295
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "GIFT_CARDS_THIRD_PARTY_REFACTOR"

    const/16 v14, 0xac

    const-string v15, "Safety switch for the refactor of PanEditor and CardEditor to specialized gift card versions where applicable."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    .line 297
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "EGIFT_CARD_IN_POS"

    const/16 v14, 0xad

    const-string v15, "Allows merchants to access the eGiftCard Activation flow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    .line 298
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "EGIFT_CARD_SETTINGS_IN_POS"

    const/16 v14, 0xae

    const-string v15, "Allows merchants to access the eGiftCard settings page."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_SETTINGS_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    .line 299
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HARDWARE_SECURE_TOUCH"

    const/16 v14, 0xaf

    const-string v15, "Enable hardware secure touch for PIN entry on squid devices."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH:Lcom/squareup/settings/server/Features$Feature;

    .line 300
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE"

    const/16 v14, 0xb0

    const-string v15, "Offer accessibility mode during hardware secure touch for PIN entry on Squid devices."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Lcom/squareup/settings/server/Features$Feature;

    .line 302
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE"

    const/16 v14, 0xb1

    const-string v15, "Offer high-contrast mode during hardware secure touch for PIN entry on Squid devices."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Lcom/squareup/settings/server/Features$Feature;

    .line 304
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HAS_BAZAAR_ONLINE_STORE"

    const/16 v14, 0xb2

    const-string v15, "Indicates whether the seller has a legacy Bazaar online store."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HAS_BAZAAR_ONLINE_STORE:Lcom/squareup/settings/server/Features$Feature;

    .line 305
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HELP_APPLET_JEDI"

    const/16 v14, 0xb3

    const-string v15, "Enable Jedi in the Master Detail Help Applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI:Lcom/squareup/settings/server/Features$Feature;

    .line 306
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HELP_APPLET_JEDI_T2"

    const/16 v14, 0xb4

    const-string v15, "Enable Jedi in the T2 Help Applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 307
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HELP_APPLET_JEDI_WORKFLOW"

    const/16 v14, 0xb5

    const-string v15, "Enables Jedi Workflow in the Help section of Support applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 308
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HELP_APPLET_JEDI_X2"

    const/16 v14, 0xb6

    const-string v15, "Enable Jedi in the X2 Help Applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 309
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HELP_APPLET_SUPPORT_MESSAGING"

    const/16 v14, 0xb7

    const-string v15, "Enable Support Messaging in Help Applet for phone"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_SUPPORT_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    .line 310
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HELP_APPLET_SUPPORT_MESSAGING_TABLET"

    const/16 v14, 0xb8

    const-string v15, "Enable Support Messaging in Help Applet for tablet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_SUPPORT_MESSAGING_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 311
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HIDE_ANNOUNCEMENTS_SECTION"

    const/16 v14, 0xb9

    const-string v15, "Hide announcements section in the help applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HIDE_ANNOUNCEMENTS_SECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 312
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "HIDE_MODIFIERS_ON_RECEIPTS"

    const/16 v18, 0xba

    const-string v19, "Hide modifiers on receipts"

    const-string v20, "Allows merchants to hide Dashboard-specified modifiers on customer receipts."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HIDE_MODIFIERS_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    .line 314
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "HIDE_TIPS"

    const/16 v14, 0xbb

    const-string v15, "Hide tips on reports."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

    .line 315
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "IGNORE_SYSTEM_PERMISSIONS"

    const/16 v14, 0xbc

    const-string v15, "Disable system permissions UI on Android M+"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 316
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "IGNORE_ACCESSIBILITY_SCRUBBER"

    const/16 v14, 0xbd

    const-string v15, "Disable the scrubbing of accessibility events of PII on non-x2 devices"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->IGNORE_ACCESSIBILITY_SCRUBBER:Lcom/squareup/settings/server/Features$Feature;

    .line 318
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INSTANT_DEPOSITS"

    const/16 v14, 0xbe

    const-string v15, "Enables instant deposits for this user."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    .line 319
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INSTANT_DEPOSIT_REQUIRES_LINKED_CARD"

    const/16 v14, 0xbf

    const-string v15, "Whether the merchant is required to have a linked debit card to use instant deposit (instant deposits can be through bank accounts instead). Controls whether toggling on instant deposit in deposit settings starts the link card flow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    .line 323
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2"

    const/16 v14, 0xc0

    const-string v15, "Intercept MagSwipe events when printing on T2."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 324
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVENTORY_AVAILABILITY"

    const/16 v14, 0xc1

    const-string v15, "Supports marking items as available and unavailable for sale."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_AVAILABILITY:Lcom/squareup/settings/server/Features$Feature;

    .line 325
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVENTORY_PLUS"

    const/16 v14, 0xc2

    const-string v15, "Allows users to access Inventory Plus features"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_PLUS:Lcom/squareup/settings/server/Features$Feature;

    .line 326
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICE_APP_BANNER"

    const/16 v14, 0xc3

    const-string v15, "Shows a banner to prompt users to download the Invoices App."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICE_APP_BANNER:Lcom/squareup/settings/server/Features$Feature;

    .line 327
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_ALLOW_PDF_ATTACHMENTS"

    const/16 v14, 0xc4

    const-string v15, "Allows adding PDFs as an attachment to Invoices."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ALLOW_PDF_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 328
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_ANALYTICS"

    const/16 v14, 0xc5

    const-string v15, "Invoice mobile analytics"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 329
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_APP_ONBOARDING_FLOW"

    const/16 v14, 0xc6

    const-string v15, "Onboarding flow specific to the Invoices vertical."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APP_ONBOARDING_FLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 330
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_APPLET"

    const/16 v14, 0xc7

    const-string v15, "Invoices Applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET:Lcom/squareup/settings/server/Features$Feature;

    .line 331
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_APPLET_PAYMENT"

    const/16 v14, 0xc8

    const-string v15, "Allow invoices to be taken payment for in Register"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 332
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_ARCHIVE"

    const/16 v14, 0xc9

    const-string v15, "Allow merchant to view Archived invoices"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

    .line 333
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_AUTOMATIC_PAYMENTS"

    const/16 v14, 0xca

    const-string v15, "Allow customer to add a card on file and be charged automatically for a recurring invoice."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 335
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_AUTOMATIC_REMINDERS"

    const/16 v14, 0xcb

    const-string v15, "Allow merchant to schedule automatic reminders on invoices"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 336
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_COF"

    const/16 v14, 0xcc

    const-string v15, "Allow merchants to charge CoF when sending an invoice."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    .line 337
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_CUSTOM_REMINDER_MESSAGE"

    const/16 v14, 0xcd

    const-string v15, "Allow merchants to send a custom reminder message."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_CUSTOM_REMINDER_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    .line 338
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_DOWNLOAD_INVOICE"

    const/16 v14, 0xce

    const-string v15, "Allows merchants to download a PDF version of an invoice."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_DOWNLOAD_INVOICE:Lcom/squareup/settings/server/Features$Feature;

    .line 339
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_EDIT_ESTIMATE_FLOW_V2"

    const/16 v14, 0xcf

    const-string v15, "Multi-step Estimate Creation flow"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_ESTIMATE_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 340
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_EDIT_FLOW_V2"

    const/16 v14, 0xd0

    const-string v15, "Edit invoice flow v2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 341
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_EDIT_FLOW_V2_WIDGETS"

    const/16 v14, 0xd1

    const-string v15, "Edit invoice flow v2 widgets"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    .line 342
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_ESTIMATES_FILE_ATTACHMENTS"

    const/16 v14, 0xd2

    const-string v15, "Allow file attachments for estimates."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ESTIMATES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 343
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_ESTIMATES_PACKAGES_READONLY_SUPPORT"

    const/16 v14, 0xd3

    const-string v15, "Supports displaying the details of an estimate with multiple packages."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ESTIMATES_PACKAGES_READONLY_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    .line 345
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_EVENT_TIMELINE"

    const/16 v14, 0xd4

    const-string v15, "Show a timeline of events for invoices"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EVENT_TIMELINE:Lcom/squareup/settings/server/Features$Feature;

    .line 346
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_FILE_ATTACHMENTS"

    const/16 v14, 0xd5

    const-string v15, "Attaching files for invoices"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 347
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_HOME_TAB"

    const/16 v14, 0xd6

    const-string v15, "Displays the Home Tab on the navigation bar"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_HOME_TAB:Lcom/squareup/settings/server/Features$Feature;

    .line 348
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "INVOICES_PAYMENT_REQUEST_EDITING"

    const/16 v18, 0xd7

    const-string v19, "Invoice Payment Requests Creation"

    const-string v20, "Allows adding and editing of an invoice\'s payment request schedule."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    .line 350
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "INVOICES_PAYMENT_SCHEDULE"

    const/16 v23, 0xd8

    const-string v24, "Invoice Payment Schedule"

    const-string v25, "Allows making > 2 payment requests."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    .line 352
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_PARTIALLY_PAY"

    const/16 v14, 0xd9

    const-string v15, "Allows merchants to partially pay for an invoice with any tender."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PARTIALLY_PAY:Lcom/squareup/settings/server/Features$Feature;

    .line 353
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_PREVIEW"

    const/16 v14, 0xda

    const-string v15, "Allow the merchant to view an invoice preview when creating an invoice."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PREVIEW:Lcom/squareup/settings/server/Features$Feature;

    .line 354
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_RATING_PROMPT"

    const/16 v14, 0xdb

    const-string v15, "Enables the rating prompt for IPOS, prompting users to leave a rating in the devices app store or through our internal feedback mechanism."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RATING_PROMPT:Lcom/squareup/settings/server/Features$Feature;

    .line 357
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_RECORD_PAYMENT"

    const/16 v14, 0xdc

    const-string v15, "Allows partial payments to be recorded on an invoice."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECORD_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 359
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_RECURRING"

    const/16 v14, 0xdd

    const-string v15, "Allows merchants to view and edit Recurring Invoice Series"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    .line 360
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_RECURRING_CANCEL_NEXT"

    const/16 v14, 0xde

    const-string v15, "merchants will have the ability to cancel the next recurring invoice of a series."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING_CANCEL_NEXT:Lcom/squareup/settings/server/Features$Feature;

    .line 362
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_TIMELINE_CTA_LINKING"

    const/16 v14, 0xdf

    const-string v15, "Links estimates and invoices applet via timeline CTA"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_TIMELINE_CTA_LINKING:Lcom/squareup/settings/server/Features$Feature;

    .line 363
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_VISIBLE_PUSH"

    const/16 v14, 0xe0

    const-string v15, "IPOS Visible Notifications and Settings."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_VISIBLE_PUSH:Lcom/squareup/settings/server/Features$Feature;

    .line 364
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_WITH_DISCOUNTS"

    const/16 v14, 0xe1

    const-string v15, "Can send an invoice with discounts in the cart."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 365
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_WITH_MODIFIERS"

    const/16 v14, 0xe2

    const-string v15, "Can send an invoice with modifiers in the cart."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_MODIFIERS:Lcom/squareup/settings/server/Features$Feature;

    .line 366
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "IPOS_NO_IDV_EXPERIMENT_ROLLOUT"

    const/16 v14, 0xe3

    const-string v15, "Enables the `ipos_skip_onboarding` ABViz experiment."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->IPOS_NO_IDV_EXPERIMENT_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 367
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS"

    const/16 v14, 0xe4

    const-string v15, "Support for editing reminders in the Payment Schedule v2 flow"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 369
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "INVOICES_EDIT_V1_DEPRECATION_CHECKOUT"

    const/16 v14, 0xe5

    const-string v15, "Removes V1 Invoices flow from checkout."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_V1_DEPRECATION_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 371
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ITEM_OPTION_GLOBAL_EDIT"

    const/16 v14, 0xe6

    const-string v15, "Can create, edit and delete Item Options globally."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_GLOBAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    .line 372
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ITEM_OPTION_LOCAL_EDIT"

    const/16 v14, 0xe7

    const-string v15, "Can manage Item Options in Items."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_LOCAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    .line 373
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ITEM_OPTION_EDIT_MASTER"

    const/16 v14, 0xe8

    const-string v15, "Can edit item options and items with item options."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_EDIT_MASTER:Lcom/squareup/settings/server/Features$Feature;

    .line 374
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "JUMBOTRON_SERVICE_KEY_T2"

    const/16 v14, 0xe9

    const-string v15, "Use T2 Jumbotron service key instead of the Spos one on T2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->JUMBOTRON_SERVICE_KEY_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 375
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LANDING_PAYMENT"

    const/16 v14, 0xea

    const-string v15, "Always show payment landing screen (will disable LANDING_WORLD)"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LANDING_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 376
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LANDING_WORLD"

    const/16 v14, 0xeb

    const-string v15, "Always show world landing screen (will disable LANDING_PAYMENT)"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LANDING_WORLD:Lcom/squareup/settings/server/Features$Feature;

    .line 377
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "LIBRARY_LIST_SHOW_SERVICES"

    const/16 v18, 0xec

    const-string v19, "Services on library list"

    const-string v20, "Show \'All Services\' on the library list."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LIBRARY_LIST_SHOW_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    .line 379
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "LIMIT_VARIATIONS_PER_ITEM"

    const/16 v23, 0xed

    const-string v24, "Limit the number of variations per item"

    const-string v25, "Determines if there will be a limit imposed on the number of variations each item is allowed to have."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LIMIT_VARIATIONS_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    .line 382
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_ADJUST_POINTS_SMS"

    const/16 v14, 0xee

    const-string v15, "Loyalty sends sms messages to buyers to notify them about points."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ADJUST_POINTS_SMS:Lcom/squareup/settings/server/Features$Feature;

    .line 384
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_ALPHANUMERIC_COUPONS"

    const/16 v14, 0xef

    const-string v15, "Allow input of alphanumeric coupon codes."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ALPHANUMERIC_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 385
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_CAN_ADJUST_POINTS_NEGATIVE"

    const/16 v14, 0xf0

    const-string v15, "When set to true, allow certain types of events (e.g. exchange, refund, manual adjustment) to result in a negative point balance."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CAN_ADJUST_POINTS_NEGATIVE:Lcom/squareup/settings/server/Features$Feature;

    .line 387
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_CASH_INTEGRATION"

    const/16 v14, 0xf1

    const-string v15, "Allow merchant to send texts inviting loyalty buyers to download cash app"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CASH_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    .line 389
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_CHECKOUT_X2"

    const/16 v14, 0xf2

    const-string v15, "Enable X2 Loyalty seller workflow"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CHECKOUT_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 390
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_DELETE_WILL_SEND_SMS"

    const/16 v14, 0xf3

    const-string v15, "Signals to clients that deleting a contact or a contact\'s loyalty account will trigger an SMS to buyers. This only occurs in certain locales where it is required by law."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

    .line 393
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_ENROLLMENT_WORKFLOW"

    const/16 v14, 0xf4

    const-string v15, "Gates if the post-transaction loyalty enrollment flow is backed by a workflow. Android/T2 and X2 will have different infrastructure around the same shared workflow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ENROLLMENT_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 396
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_EXPIRE_POINTS"

    const/16 v14, 0xf5

    const-string v15, "Merchants can set up expiration policy in their loyalty program to expire points after certain period."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_EXPIRE_POINTS:Lcom/squareup/settings/server/Features$Feature;

    .line 398
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_HANDLE_RETURN_ITEMS"

    const/16 v14, 0xf6

    const-string v15, "For retail transactions that involve returned items, the server should handle the potential subtraction in loyalty balance and the client should not show the loyalty flow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_HANDLE_RETURN_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    .line 401
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_ITEM_SELECTION"

    const/16 v14, 0xf7

    const-string v15, "Gates whether the POS shows an item selection flow when redeeming an item-based coupon"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ITEM_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 403
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_TUTORIALS_ENROLLMENT"

    const/16 v14, 0xf8

    const-string v15, "Gates the ability for merchants to view the Loyalty Enrollment tutorial."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_TUTORIALS_ENROLLMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 405
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN"

    const/16 v14, 0xf9

    const-string v15, "Display Check In button to Bran Idle/Cart screens for loyalty merchants that allows the buyer to check in and redeem rewards using just Bran."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN:Lcom/squareup/settings/server/Features$Feature;

    .line 408
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE"

    const/16 v14, 0xfa

    const-string v15, "Allow the seller to override Check in on Bran by pressing the Charge button."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    .line 410
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "LOYALTY_X2_POST_TRANSACTION_DARK_THEME"

    const/16 v14, 0xfb

    const-string v15, "Feature flags if X2 Post Transaction Check In flow uses the older dark + blue star background theming instead of the white + boxes theming."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_POST_TRANSACTION_DARK_THEME:Lcom/squareup/settings/server/Features$Feature;

    .line 413
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "MANDATORY_BREAK_COMPLETION"

    const/16 v14, 0xfc

    const-string v15, "When enabled, team members should be able to end breaks early only within configured threshold or with manager approval."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/settings/server/Features$Feature;

    .line 415
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "MULTIPLE_WAGES"

    const/16 v14, 0xfd

    const-string v15, "Enable support for multiple wages for employees"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES:Lcom/squareup/settings/server/Features$Feature;

    .line 416
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "MULTIPLE_WAGES_BETA"

    const/16 v14, 0xfe

    const-string v15, "Enable multiple wages Beta features e.g. switching jobs, clock out summary"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES_BETA:Lcom/squareup/settings/server/Features$Feature;

    .line 418
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "NOTIFICATION_CENTER"

    const/16 v18, 0xff

    const-string v19, "Notification Center"

    const-string v20, "Show Notification Center Applet"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    .line 420
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "O1_DEPRECATION_WARNING_JP"

    const/16 v14, 0x100

    const-string v15, "If O1 in use, show reminder of deprecation (JP) once daily at most."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->O1_DEPRECATION_WARNING_JP:Lcom/squareup/settings/server/Features$Feature;

    .line 422
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_ALLOW_SCREENSHOT"

    const/16 v14, 0x101

    const-string v15, "Allow screenshots during Onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ALLOW_SCREENSHOT:Lcom/squareup/settings/server/Features$Feature;

    .line 423
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_ASK_INTENT"

    const/16 v14, 0x102

    const-string v15, "Ask merchants where and how they\'ll take payments during onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ASK_INTENT:Lcom/squareup/settings/server/Features$Feature;

    .line 424
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_BANK"

    const/16 v14, 0x103

    const-string v15, "Offer bank linking during onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_BANK:Lcom/squareup/settings/server/Features$Feature;

    .line 425
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_CHOOSE_DEFAULT_DEPOSIT_METHOD"

    const/16 v14, 0x104

    const-string v15, "Show flow for selecting and setting up a default deposit method (next business day or same day) during onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_CHOOSE_DEFAULT_DEPOSIT_METHOD:Lcom/squareup/settings/server/Features$Feature;

    .line 427
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_FREE_READER"

    const/16 v14, 0x105

    const-string v15, "Offer a free reader during onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_FREE_READER:Lcom/squareup/settings/server/Features$Feature;

    .line 428
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_QUOTA"

    const/16 v14, 0x106

    const-string v15, "Ask the quota service whether there\'s a free reader during onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_QUOTA:Lcom/squareup/settings/server/Features$Feature;

    .line 429
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_REFERRAL"

    const/16 v14, 0x107

    const-string v15, "Offer referral during onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    .line 430
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "ONBOARDING_SERVER_DRIVEN_FLOW"

    const/16 v18, 0x108

    const-string v19, "Server-Driven Onboarding"

    const-string v20, "Complete payment activation and onboarding via the server-driven flow."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SERVER_DRIVEN_FLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 432
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_SUPPRESS_AUTO_FPT"

    const/16 v14, 0x109

    const-string v15, "If true, disable automatically showing the first payment tutorial"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SUPPRESS_AUTO_FPT:Lcom/squareup/settings/server/Features$Feature;

    .line 434
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ONBOARDING_X2_VERTICAL_SELECTION"

    const/16 v14, 0x10a

    const-string v15, "Show the vertical POS selection flow during onboarding on X2."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_X2_VERTICAL_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 436
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "OPEN_TICKETS_BULK_DELETE"

    const/16 v18, 0x10b

    const-string v19, "Open Tickets Bulk Delete"

    const-string v20, "Allow users to bulk delete tickets."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_BULK_DELETE:Lcom/squareup/settings/server/Features$Feature;

    .line 437
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "OPEN_TICKETS_DIP_TAP_TO_CREATE"

    const/16 v14, 0x10c

    const-string v15, "Create an open ticket by dipping or tapping in the tickets modal. X2 implementation only for now."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_DIP_TAP_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    .line 439
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "OPEN_TICKETS_EMPLOYEE_FILTERING"

    const/16 v14, 0x10d

    const-string v15, "Filter tickets list by employees."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    .line 440
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "OPEN_TICKETS_MERGE"

    const/16 v14, 0x10e

    const-string v15, "Merge Open Tickets."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MERGE:Lcom/squareup/settings/server/Features$Feature;

    .line 441
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v20, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "OPEN_TICKETS_MOBILE"

    const/16 v18, 0x10f

    const-string v19, "Open Tickets on mobile devices. Seperate feature from OPEN_TICKETS_TABLET for rollout. "

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 443
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v25, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "OPEN_TICKETS_TABLET"

    const/16 v23, 0x110

    const-string v24, "*Manage* multiple carts simultaneously. "

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 444
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v20, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v17, "OPEN_TICKETS_SHOW_BETA_OPT_IN"

    const/16 v18, 0x111

    const-string v19, "Show the Opt In Beta Warning when toggling Open Tickets."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SHOW_BETA_OPT_IN:Lcom/squareup/settings/server/Features$Feature;

    .line 446
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "OPEN_TICKETS_SWIPE_TO_CREATE"

    const/16 v14, 0x112

    const-string v15, "Create an open ticket by swiping in the tickets modal."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SWIPE_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    .line 447
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "OPEN_TICKETS_TRANSFER"

    const/16 v18, 0x113

    const-string v19, "Open Tickets Transfer"

    const-string v20, "Allow employees to transfer open tickets."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TRANSFER:Lcom/squareup/settings/server/Features$Feature;

    .line 448
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "OPEN_TICKETS_V2"

    const/16 v23, 0x114

    const-string v24, "Open Tickets V2"

    const-string v25, "Enable Open Tickets V2."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 449
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "OPEN_TICKETS_V2_NAME_AND_NOTES"

    const/16 v18, 0x115

    const-string v19, "Open Tickets V2 Name and Notes"

    const-string v20, "Enable editing the name and notes on an open ticket in Open Tickets V2"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2_NAME_AND_NOTES:Lcom/squareup/settings/server/Features$Feature;

    .line 451
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "OPEN_TICKETS_V2_SEARCH_SORT_FILTER"

    const/16 v23, 0x116

    const-string v24, "Open Tickets V2 Search, Sort, and Filter"

    const-string v25, "Searching tickets, sorting tickets, and filtering by ticket group in Open Tickets V2"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Lcom/squareup/settings/server/Features$Feature;

    .line 453
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS"

    const/16 v14, 0x117

    const-string v15, "Opt in to store and forwarding payments"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 454
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_ALLOW_BAZAAR_ORDERS"

    const/16 v14, 0x118

    const-string v15, "The orders applet should request to show Bazaar online store orders."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_ALLOW_BAZAAR_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 456
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_APPLET_ROLLOUT"

    const/16 v14, 0x119

    const-string v15, "Allows the merchant to see the orderhub applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    .line 457
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_CAN_SEARCH_ORDERS"

    const/16 v14, 0x11a

    const-string v15, "Allows merchant to search orders via a search term in order manager"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CAN_SEARCH_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 459
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_CHECK_EMPLOYEE_PERMISSIONS"

    const/16 v14, 0x11b

    const-string v15, "The Orders Applet should check employee permissions."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CHECK_EMPLOYEE_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 460
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_SHOW_QUICK_ACTIONS_SETTING"

    const/16 v14, 0x11c

    const-string v15, "Determines whether or not a client can show the setting that enables quick actions in Order Hub."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SHOW_QUICK_ACTIONS_SETTING:Lcom/squareup/settings/server/Features$Feature;

    .line 462
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_SUPPORTS_ECOM_DELIVERY_ORDERS"

    const/16 v14, 0x11d

    const-string v15, "The Orders Applet can display ECOM Delivery orders."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_ECOM_DELIVERY_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 463
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_SUPPORTS_UPCOMING_ORDERS"

    const/16 v14, 0x11e

    const-string v15, "The Orders Applet differentiates between active and upcoming orders"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_UPCOMING_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 465
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDERHUB_THROTTLE_SEARCH"

    const/16 v14, 0x11f

    const-string v15, "When enabled, use a throttled search range to search orders."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_THROTTLE_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    .line 466
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM"

    const/16 v14, 0x120

    const-string v15, "Shows a Create New item button at the bottom of the Order Entry Library list."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Lcom/squareup/settings/server/Features$Feature;

    .line 468
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS"

    const/16 v14, 0x121

    const-string v15, "When enabled, we show item suggestions in the library list if the merchant has no items yet."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 470
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS"

    const/16 v18, 0x122

    const-string v19, "Order Entry - Hide GiftCards/Rewards If No Items"

    const-string v20, "When enabled, we hide Gift Cards and Rewards in the library list if there are no items."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    .line 473
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS"

    const/16 v23, 0x123

    const-string v24, "Process CNP via orders"

    const-string v25, "Enable processing CNP payments with the orders and payment v2 endpoints"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    .line 475
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PAYMENT_FLOW_USE_PAYMENT_CONFIG"

    const/16 v14, 0x124

    const-string v15, "Use the payment config for the amount to collect if it\'s available."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    .line 477
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v17, "PAPER_SIGNATURE_EMPLOYEE_FILTERING"

    const/16 v18, 0x125

    const-string v19, "Paper Signature Employee Filtering"

    const-string v20, "Filter and sort tenders awaiting tip by the employee that created them."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PAPER_SIGNATURE_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    .line 480
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PERFORM_CATALOG_SYNC_AFTER_TRANSACTION_ON_X2"

    const/16 v14, 0x126

    const-string v15, "Perform a catalog background sync after each transaction on X2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PERFORM_CATALOG_SYNC_AFTER_TRANSACTION_ON_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 482
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "PERSISTENT_BUNDLE"

    const/16 v18, 0x127

    const-string v19, "Use Persistent Bundle"

    const-string v20, "Use persistent Bundle to avoid TransactionTooLargeException when saving Bundle."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PERSISTENT_BUNDLE:Lcom/squareup/settings/server/Features$Feature;

    .line 484
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PLAY_PAYMENT_APPROVED_SOUND_X2"

    const/16 v14, 0x128

    const-string v15, "Play sound when payment success on Hodor RA-22193"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PLAY_PAYMENT_APPROVED_SOUND_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 485
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v21, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "PREDEFINED_TICKETS_TABLET"

    const/16 v18, 0x129

    const-string v19, "Predefined Tickets on tablet"

    const-string v20, "Configure and use ticket templates. First step to table management."

    move-object/from16 v16, v0

    move-object/from16 v22, v2

    invoke-direct/range {v16 .. v22}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 487
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v23, "PREDEFINED_TICKETS_T2"

    const/16 v24, 0x12a

    const-string v25, "Predefined Tickets on T2"

    const-string v26, "Configure and use ticket templates. First step to table management."

    move-object/from16 v22, v0

    move-object/from16 v27, v2

    invoke-direct/range {v22 .. v27}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 489
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "PREDEFINED_TICKETS_V2"

    const/16 v18, 0x12b

    const-string v19, "Predefined Tickets V2"

    const-string v20, "Predefined Tickets V2."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 490
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PRINT_MERCHANT_LOGO_ON_RECEIPTS"

    const/16 v14, 0x12c

    const-string v15, "Print Merchant Logo On Receipts"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINT_MERCHANT_LOGO_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    .line 491
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PRINT_ITEMIZED_DISCOUNTS"

    const/16 v14, 0x12d

    const-string v15, "Show each discount individually on printed receipts"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINT_ITEMIZED_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 492
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PRINTER_DEBUGGING"

    const/16 v14, 0x12e

    const-string v15, "Enable extended debug information for printer scouting and printing"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    .line 493
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PRINTER_DITHERING_T2"

    const/16 v14, 0x12f

    const-string v15, "Enable dithering for T2 internal printer"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DITHERING_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 494
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PRINTING_DEBUG_RECEIPTS"

    const/16 v14, 0x130

    const-string v15, "Adds random background colors to each block in a receipt, to show its structure"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINTING_DEBUG_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    .line 496
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PROTECT_EDIT_TAX"

    const/16 v14, 0x131

    const-string v15, "Require employee permission to edit taxes during a sale"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

    .line 497
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "R12_EARLY_K400_POWERUP"

    const/16 v14, 0x132

    const-string v15, "Send a powerup hint to the R12\'s K400 when the cart is first changed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->R12_EARLY_K400_POWERUP:Lcom/squareup/settings/server/Features$Feature;

    .line 499
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_209030_QUICKCHIP"

    const/16 v14, 0x133

    const-string v15, "Enable quickchip on R12 for FW versions at or above 209030"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_209030_QUICKCHIP:Lcom/squareup/settings/server/Features$Feature;

    .line 500
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_ACCOUNT_TYPE_SELECTION"

    const/16 v14, 0x134

    const-string v15, "Enable interac account selection"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_ACCOUNT_TYPE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 501
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_COMMON_DEBIT_SUPPORT"

    const/16 v14, 0x135

    const-string v15, "Enable common debit support for LCR firmware."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_COMMON_DEBIT_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    .line 502
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_FELICA_NOTIFICATION"

    const/16 v14, 0x136

    const-string v15, "Enable felica notification in reader firmware."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_FELICA_NOTIFICATION:Lcom/squareup/settings/server/Features$Feature;

    .line 503
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_PINBLOCK_V2"

    const/16 v14, 0x137

    const-string v15, "Enable pinblock format v2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_PINBLOCK_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 504
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_SPOC_PRNG_SEED"

    const/16 v14, 0x138

    const-string v15, "Enable sending PRNG seed"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SPOC_PRNG_SEED:Lcom/squareup/settings/server/Features$Feature;

    .line 505
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_FW_SONIC_BRANDING"

    const/16 v14, 0x139

    const-string v15, "Enable sonic branding a.k.a. audio visual request support,"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    .line 506
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "READER_ORDER_HISTORY"

    const/16 v14, 0x13a

    const-string v15, "Shows a list of reader orders from solidshop."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_ORDER_HISTORY:Lcom/squareup/settings/server/Features$Feature;

    .line 507
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS"

    const/16 v14, 0x13b

    const-string v15, "Enables the printing of Japanese formal receipts."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    .line 508
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "RECEIPTS_PRINT_DISPOSITION"

    const/16 v14, 0x13c

    const-string v15, "Enables printing the approved disposition on receipts."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_PRINT_DISPOSITION:Lcom/squareup/settings/server/Features$Feature;

    .line 509
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REPORTS_SEE_MEASUREMENT_UNIT"

    const/16 v14, 0x13d

    const-string v15, "Shows measurement units on reports."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REPORTS_SEE_MEASUREMENT_UNIT:Lcom/squareup/settings/server/Features$Feature;

    .line 510
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REQUEST_BUSINESS_ADDRESS"

    const/16 v14, 0x13e

    const-string v15, "Request business address in onboarding."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    .line 511
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REQUEST_BUSINESS_ADDRESS_BADGE"

    const/16 v14, 0x13f

    const-string v15, "Show a badge for merchants with no business address on file."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_BADGE:Lcom/squareup/settings/server/Features$Feature;

    .line 512
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION"

    const/16 v14, 0x140

    const-string v15, "Enables PO Box checking for business address."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

    .line 514
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "RESILIENT_BUS"

    const/16 v14, 0x141

    const-string v15, "Enables resilient bus between Hodor and Bran"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->RESILIENT_BUS:Lcom/squareup/settings/server/Features$Feature;

    .line 515
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "RETAIN_PRINTER_CONNECTION"

    const/16 v14, 0x142

    const-string v15, "Don\'t disconnect printers for consecutive PrintJobs"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->RETAIN_PRINTER_CONNECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 516
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PORTRAIT_SIGNATURE"

    const/16 v14, 0x143

    const-string v15, "Forces signature screen to be in portrait orientation."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PORTRAIT_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    .line 517
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "PREFER_SQLITE_TASKS_QUEUE"

    const/16 v18, 0x144

    const-string v19, "Prefer SQLite tasks queue to Tape"

    const-string v20, "Treat the SQLite tasks queue as the source of truth for local payments and other background tasks."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PREFER_SQLITE_TASKS_QUEUE:Lcom/squareup/settings/server/Features$Feature;

    .line 520
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "PRINT_TAX_PERCENTAGE"

    const/16 v14, 0x145

    const-string v15, "Print the tax percentage on the receipt."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINT_TAX_PERCENTAGE:Lcom/squareup/settings/server/Features$Feature;

    .line 521
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "QUICK_AMOUNTS"

    const/16 v14, 0x146

    const-string v15, "Make quick amounts available to this merchant."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 522
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REDUCE_PRINTING_WASTE"

    const/16 v14, 0x147

    const-string v15, "Reduces receipt size by coalescing like items on receipts"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REDUCE_PRINTING_WASTE:Lcom/squareup/settings/server/Features$Feature;

    .line 523
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REFERRAL"

    const/16 v14, 0x148

    const-string v15, "Enable referral (free processing) in help applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    .line 524
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REFUND_TO_GIFT_CARD"

    const/16 v14, 0x149

    const-string v15, "Adds the ability to issue refunds to non-source-tender gift cards."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

    .line 525
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REMOTELY_CLOSE_OPEN_CASH_DRAWERS"

    const/16 v14, 0x14a

    const-string v15, "Allows viewing and closing open cash drawers remotely"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REMOTELY_CLOSE_OPEN_CASH_DRAWERS:Lcom/squareup/settings/server/Features$Feature;

    .line 526
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v4, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v3

    const-string v13, "REORDER_READER_IN_APP"

    const/16 v14, 0x14b

    const-string v15, "Swap out the dashboard link for an app page to reorder"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    .line 528
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v5, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v3

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v4

    const-string v13, "REORDER_READER_IN_APP_EMAIL_CONFIRM"

    const/16 v14, 0x14c

    const-string v15, "Show a message that we\'ll confirm reorder by email"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP_EMAIL_CONFIRM:Lcom/squareup/settings/server/Features$Feature;

    .line 530
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v21, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "REPORTS_APPLET_MOBILE"

    const/16 v18, 0x14d

    const-string v19, "Reports Applet"

    const-string v20, "Sales Summary"

    move-object/from16 v16, v0

    move-object/from16 v22, v2

    invoke-direct/range {v16 .. v22}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REPORTS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 531
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REQUIRES_TRACK_2_IF_NOT_AMEX"

    const/16 v14, 0x14e

    const-string v15, "Require track 2 info if not Amex"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REQUIRES_TRACK_2_IF_NOT_AMEX:Lcom/squareup/settings/server/Features$Feature;

    .line 532
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION"

    const/16 v14, 0x14f

    const-string v15, "Allow permissions to define if a passcode is necessary to cancel a transaction in passcode employee management mode."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION:Lcom/squareup/settings/server/Features$Feature;

    .line 535
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REQUIRE_SECURE_SESSION_FOR_R6_SWIPE"

    const/16 v14, 0x150

    const-string v15, "Require Secure Session for R6 swipes"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Lcom/squareup/settings/server/Features$Feature;

    .line 536
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "RESTART_APP_AFTER_CRASH"

    const/16 v14, 0x151

    const-string v15, "Automatically restart the app after a Crash or a Force Close Error"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->RESTART_APP_AFTER_CRASH:Lcom/squareup/settings/server/Features$Feature;

    .line 537
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "REWARDS"

    const/16 v14, 0x152

    const-string v15, "Allows sellers to redeem rewards from the category list, or for buyers to claimrewards after swiping"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->REWARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 539
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "S2_HODOR"

    const/16 v18, 0x153

    const-string v19, "Be Hodor"

    const-string v20, "Expect to have an X2 buyer screen."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->S2_HODOR:Lcom/squareup/settings/server/Features$Feature;

    .line 540
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "SAMPLE_APPLET"

    const/16 v23, 0x154

    const-string v24, "Sample Applet"

    const-string v25, "Adds a sample applet to the drawer."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SAMPLE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    .line 541
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SALES_LIMITS"

    const/16 v14, 0x155

    const-string v15, "Controls whether merchants can set sales limits on items."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SALES_LIMITS:Lcom/squareup/settings/server/Features$Feature;

    .line 542
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SALES_SUMMARY_CHARTS"

    const/16 v14, 0x156

    const-string v15, "Display a bar chart row in sales summary report."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SALES_SUMMARY_CHARTS:Lcom/squareup/settings/server/Features$Feature;

    .line 543
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "SALES_REPORT_V2"

    const/16 v18, 0x157

    const-string v19, "Sales Report V2"

    const-string v20, "Replaces existing sales report with new refreshed version (RA-34929)"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 545
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "SALES_REPORT_V2_FEATURE_CAROUSEL"

    const/16 v23, 0x158

    const-string v24, "Sales Report V2 Feature Carousel"

    const-string v25, "Shows a feature carousel page as part of onboarding for the new refreshed version of sales report (RA-34929)"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2_FEATURE_CAROUSEL:Lcom/squareup/settings/server/Features$Feature;

    .line 548
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "SANITIZE_EVENTSTREAM_COORDINATES"

    const/16 v18, 0x159

    const-string v19, "Sanitize Location"

    const-string v20, "Reduce the accuracy of the GPS location for analytics"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SANITIZE_EVENTSTREAM_COORDINATES:Lcom/squareup/settings/server/Features$Feature;

    .line 550
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SEPARATED_PRINTOUTS"

    const/16 v14, 0x15a

    const-string v15, "Separated printouts for T2."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SEPARATED_PRINTOUTS:Lcom/squareup/settings/server/Features$Feature;

    .line 551
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SETTINGS_V2"

    const/16 v14, 0x15b

    const-string v15, "Show Settings v2 Applet."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 552
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SETUP_GUIDE"

    const/16 v14, 0x15c

    const-string v15, "Show post-onboarding Setup Guide."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SETUP_GUIDE:Lcom/squareup/settings/server/Features$Feature;

    .line 553
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_ACCIDENTAL_CASH_MODAL"

    const/16 v14, 0x15d

    const-string v15, "Show accidental cash modal"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_ACCIDENTAL_CASH_MODAL:Lcom/squareup/settings/server/Features$Feature;

    .line 554
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_BUY_BUTTON"

    const/16 v14, 0x15e

    const-string v15, "Allow sellers to use Direct To Checkout feature. WCOM-2045"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_BUY_BUTTON:Lcom/squareup/settings/server/Features$Feature;

    .line 555
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_CHECKOUT_LINKS_SETTINGS"

    const/16 v14, 0x15f

    const-string v15, "This flag controls whether Checkout Links are shown in the Settings applet."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_CHECKOUT_LINKS_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 557
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_CP_PRICING_CHANGE"

    const/16 v14, 0x160

    const-string v15, "Show pricing change for card-present transactions."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_CP_PRICING_CHANGE:Lcom/squareup/settings/server/Features$Feature;

    .line 558
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_FEE_BREAKDOWN_TABLE"

    const/16 v14, 0x161

    const-string v15, "Show fee breakdown table on the receipt"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_FEE_BREAKDOWN_TABLE:Lcom/squareup/settings/server/Features$Feature;

    .line 559
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "SHOW_INCLUSIVE_TAXES_IN_CART"

    const/16 v18, 0x162

    const-string v19, "Show Inclusive Taxes in Cart"

    const-string v20, ""

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_INCLUSIVE_TAXES_IN_CART:Lcom/squareup/settings/server/Features$Feature;

    .line 560
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v26, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "SHOW_ITEMS_LIBRARY_AFTER_LOGIN"

    const/16 v23, 0x163

    const-string v24, "Show Library first (mobile)"

    const-string v25, "Upon login, show the items Library panel, not the Keypad panel, first"

    move-object/from16 v21, v0

    move-object/from16 v27, v2

    invoke-direct/range {v21 .. v27}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Lcom/squareup/settings/server/Features$Feature;

    .line 562
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "SHOW_LOYALTY"

    const/16 v18, 0x164

    const-string v19, "Show Loyalty"

    const-string v20, "Permanent feature flag to enable or disable the entire Loyalty product on Dashboard and POS. This allows the feature editor to control for international rollouts, etc."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_LOYALTY:Lcom/squareup/settings/server/Features$Feature;

    .line 566
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_LOYALTY_VALUE_METRICS"

    const/16 v14, 0x165

    const-string v15, "Toggles value of loyalty page in reports applet"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_LOYALTY_VALUE_METRICS:Lcom/squareup/settings/server/Features$Feature;

    .line 567
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_NOHO_RECEIPT"

    const/16 v14, 0x166

    const-string v15, "Show Noho design version of receipt screen"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_NOHO_RECEIPT:Lcom/squareup/settings/server/Features$Feature;

    .line 568
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_ONLINE_CHECKOUT_SETTINGS_V2"

    const/16 v14, 0x167

    const-string v15, "This flag is used for showing v2 iteration of Online Checkout Settings screens."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 570
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_PAY_ONLINE_TENDER_OPTION"

    const/16 v14, 0x168

    const-string v15, "Allow sellers to use Direct To Checkout feature as a tender type."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_PAY_ONLINE_TENDER_OPTION:Lcom/squareup/settings/server/Features$Feature;

    .line 572
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_SPOC_VERSION_NUMBER"

    const/16 v14, 0x169

    const-string v15, "Show the SPoC version number."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_SPOC_VERSION_NUMBER:Lcom/squareup/settings/server/Features$Feature;

    .line 573
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_WEEKEND_BALANCE_TOGGLE"

    const/16 v14, 0x16a

    const-string v15, "Enable viewing the weekend balance toggle on Friday deposit schedule."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKEND_BALANCE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    .line 575
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SHOW_WEEKVIEW"

    const/16 v14, 0x16b

    const-string v15, "Show Weekview on Android Appointments Point of Sale (APoS)."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKVIEW:Lcom/squareup/settings/server/Features$Feature;

    .line 577
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "SKIP_MODIFIER_DETAIL_SCREEN"

    const/16 v18, 0x16c

    const-string v19, "Skip modifier detail screen"

    const-string v20, "Allows employees to skip the modifier detail screen when adding an item to the cart."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SKIP_MODIFIER_DETAIL_SCREEN:Lcom/squareup/settings/server/Features$Feature;

    .line 579
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "SKIP_SIGNATURES_FOR_SMALL_PAYMENTS"

    const/16 v23, 0x16d

    const-string v24, "Skip Signature"

    const-string v25, "Allow user to skip signatures when payment amount is under certain value."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 581
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN"

    const/16 v14, 0x16e

    const-string v15, "On X2, use a 24 hour cache lifespan for Skyhook instead of 12."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

    .line 583
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL"

    const/16 v14, 0x16f

    const-string v15, "On T2, use a 6 hour check interval for Skyhook instead of 12."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Lcom/squareup/settings/server/Features$Feature;

    .line 585
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN"

    const/16 v14, 0x170

    const-string v15, "On T2, use a 3 hour cache lifespan for Skyhook instead of 12."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

    .line 587
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS"

    const/16 v14, 0x171

    const-string v15, "Enables only using IP location if WPS is totally unavailable."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Lcom/squareup/settings/server/Features$Feature;

    .line 589
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SMS"

    const/16 v14, 0x172

    const-string v15, "Allow buyers to receive SMS receipts"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SMS:Lcom/squareup/settings/server/Features$Feature;

    .line 590
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SPE_FWUP_CRQ"

    const/16 v14, 0x173

    const-string v15, "Enable SPE to FWUP Cirque."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_CRQ:Lcom/squareup/settings/server/Features$Feature;

    .line 591
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SPE_FWUP_WITHOUT_MATCHING_TMS"

    const/16 v14, 0x174

    const-string v15, "Allow SPE to skip updating TMS if there is no matching TMS asset."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    .line 593
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SPE_TMS_LOGIN_CHECK"

    const/16 v14, 0x175

    const-string v15, "Enables a check for TMS immediately upon login."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SPE_TMS_LOGIN_CHECK:Lcom/squareup/settings/server/Features$Feature;

    .line 594
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v20, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v17, "SPLIT_TICKET"

    const/16 v18, 0x176

    const-string v19, "Enable Split Ticket feature for Open Tickets"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SPLIT_TICKET:Lcom/squareup/settings/server/Features$Feature;

    .line 596
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SPM_SQUID"

    const/16 v14, 0x177

    const-string v15, "Enable SPM (Square Push Messaging) on a Squid device."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SPM_SQUID:Lcom/squareup/settings/server/Features$Feature;

    .line 597
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SPM_SQUID_LOG_TO_ES"

    const/16 v14, 0x178

    const-string v15, "Log received SPM (Square Push Messaging) events to EventStream."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SPM_SQUID_LOG_TO_ES:Lcom/squareup/settings/server/Features$Feature;

    .line 598
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SQUID_PTS_COMPLIANCE"

    const/16 v14, 0x179

    const-string v15, "Enable the PTS Compliance checks upon login to Squid."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SQUID_PTS_COMPLIANCE:Lcom/squareup/settings/server/Features$Feature;

    .line 599
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "STATUS_BAR_FULLSCREEN_T2"

    const/16 v14, 0x17a

    const-string v15, "Allow the StatusBar to transition to Fullscreen mode on T2 more smoothly."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->STATUS_BAR_FULLSCREEN_T2:Lcom/squareup/settings/server/Features$Feature;

    .line 601
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "SWIPE_INSTANT_DEPOSIT_CARD"

    const/16 v14, 0x17b

    const-string v15, "Allow linking a swiped card for Instant Deposits"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->SWIPE_INSTANT_DEPOSIT_CARD:Lcom/squareup/settings/server/Features$Feature;

    .line 602
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_API_URL_LIST"

    const/16 v14, 0x17c

    const-string v15, "Use multiple API urls"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_API_URL_LIST:Lcom/squareup/settings/server/Features$Feature;

    .line 603
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_BREAK_TRACKING"

    const/16 v18, 0x17d

    const-string v19, "Break Tracking"

    const-string v20, "Use Break Tracking Feature"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_BREAK_TRACKING:Lcom/squareup/settings/server/Features$Feature;

    .line 604
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_FILE_SIZE_ANALYTICS"

    const/16 v14, 0x17e

    const-string v15, "Enable file size analytics to report abnormaly big file and package sizes - on X2 and T2"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_FILE_SIZE_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 606
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v20, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_IN_APP_TIMECARDS"

    const/16 v18, 0x17f

    const-string v19, "Allowing viewing of the time clock canvas"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_IN_APP_TIMECARDS:Lcom/squareup/settings/server/Features$Feature;

    .line 607
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v13, "T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH"

    const/16 v14, 0x180

    const-string v15, "When enabled in the United States, the Buyer Checkout flow on T2 will default to US english regardless of the device locale."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Lcom/squareup/settings/server/Features$Feature;

    .line 610
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TERMINAL_API"

    const/16 v14, 0x181

    const-string v15, "Allow payments originating from other point of sale systems."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TERMINAL_API:Lcom/squareup/settings/server/Features$Feature;

    .line 611
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TERMINAL_API_SPM"

    const/16 v14, 0x182

    const-string v15, "Allow Terminal API to use SPM (Square Push Messaging)."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TERMINAL_API_SPM:Lcom/squareup/settings/server/Features$Feature;

    .line 612
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TIME_TRACKING_DEVICE_LEVEL"

    const/16 v14, 0x183

    const-string v15, "Deprecation of merchant-level permission for time tracking"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TIME_TRACKING_DEVICE_LEVEL:Lcom/squareup/settings/server/Features$Feature;

    .line 613
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TIMECARDS_RECEIPT_SUMMARY"

    const/16 v14, 0x184

    const-string v15, "Allow team members to print shift summaries to the receipt printer"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_RECEIPT_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    .line 614
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TIMECARDS_TEAM_MEMBER_NOTES"

    const/16 v14, 0x185

    const-string v15, "Allow team members to add and edit notes for their timecards."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_TEAM_MEMBER_NOTES:Lcom/squareup/settings/server/Features$Feature;

    .line 615
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TMN_RECORD_TIMINGS"

    const/16 v14, 0x186

    const-string v15, "Log timing data for TMN transactions, and send summary to eventstream."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TMN_RECORD_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 616
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "TMN_VERBOSE_TIMINGS"

    const/16 v14, 0x187

    const-string v15, "Additional to TMN_RECORD_TIMINGS: log verbose timing events to logcat and eventstream."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->TMN_VERBOSE_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 618
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "UPLOAD_LEDGER_AND_DIAGNOSTICS_X2"

    const/16 v14, 0x188

    const-string v15, "Enable ledger upload and send diagnostics report on X2."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 619
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "UPLOAD_LEDGER_AND_DIAGNOSTICS"

    const/16 v14, 0x189

    const-string v15, "Enable ledger upload and send diagnostics report."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 620
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "UPLOAD_SUPPORT_LEDGER"

    const/16 v18, 0x18a

    const-string v19, "Upload support ledger"

    const-string v20, "Enable uploading support ledger"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    .line 621
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "UPLOAD_SUPPORT_LEDGER_STREAM"

    const/16 v23, 0x18b

    const-string v24, "Stream support ledger upload"

    const-string v25, "Enable the streams-based support ledger upload"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER_STREAM:Lcom/squareup/settings/server/Features$Feature;

    .line 623
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "UPLOAD_X2_WIFI_EVENTS"

    const/16 v18, 0x18c

    const-string v19, "Upload X2 Wifi Events"

    const-string v20, "Enable uploading of Wifi-related logcat events from Hodor to EventStream"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_X2_WIFI_EVENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 625
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_ACCESSIBLE_PIN_TUTORIAL"

    const/16 v14, 0x18d

    const-string v15, "Display the accessible PIN tutorial in the secure touch flow."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ACCESSIBLE_PIN_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    .line 626
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_ALTERNATE_SALES_SUMMARY"

    const/16 v14, 0x18e

    const-string v15, "Uses the alternate sales summary layout for reports"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ALTERNATE_SALES_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    .line 627
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_AUTH_WORKFLOW"

    const/16 v14, 0x18f

    const-string v15, "This will control using the authorize screen workflow instead of the old presenter."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_AUTH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 629
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_AUTOMATIC_GRATUITY"

    const/16 v14, 0x190

    const-string v15, "Whether the merchant can apply automatic gratuity to a check in RST."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_AUTOMATIC_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    .line 630
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_CARD_ON_FILE"

    const/16 v18, 0x191

    const-string v19, "Can use Card on File"

    const-string v20, "Allow storing cards and charging stored cards."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE:Lcom/squareup/settings/server/Features$Feature;

    .line 631
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v25, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v3, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE:Lcom/squareup/settings/server/Features$Feature;

    aput-object v13, v2, v1

    const-string v22, "USE_CARD_ON_FILE_ON_MOBILE"

    const/16 v23, 0x192

    const-string v24, "Can use Card on File on mobile"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE_ON_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 633
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_CASH_OUT_REASON"

    const/16 v14, 0x193

    const-string v15, "In AU, \'Cash Out\' is not a valid reason for clearing a gift card balance. If this feature flag is OFF, the POS should not show the reason."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_OUT_REASON:Lcom/squareup/settings/server/Features$Feature;

    .line 635
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_CASH_WORKFLOW"

    const/16 v18, 0x194

    const-string v19, "Use the Cash Workflow"

    const-string v20, "Use the custom cash workflow instead of flow.set"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 636
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_COMP_VOID_ASSIGN_MOVE"

    const/16 v14, 0x195

    const-string v15, "Determines if RST merchants can comp, void, assign and move tickets."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_COMP_VOID_ASSIGN_MOVE:Lcom/squareup/settings/server/Features$Feature;

    .line 638
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_SPLIT_TICKET_RST"

    const/16 v14, 0x196

    const-string v15, "Determines if RST merchants can split tickets."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TICKET_RST:Lcom/squareup/settings/server/Features$Feature;

    .line 640
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_SELECT_TENDER_WORKFLOW"

    const/16 v18, 0x197

    const-string v19, "Can use Select tender workflow"

    const-string v20, "The merchant will use the updated Select Tender workflow instead of the deprecated RealSelectMethodWorkflow"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 643
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "USE_SELECT_METHOD_WORKFLOW"

    const/16 v23, 0x198

    const-string v24, "Use the workflow-ized Select Method Workflow"

    const-string v25, "Use the workflow-ized version of the Select Method Workflow."

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_METHOD_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 645
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_CUSTOMER_DIRECTORY_WITH_INVOICES"

    const/16 v14, 0x199

    const-string v15, "Use Customer Directory with Invoices."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Lcom/squareup/settings/server/Features$Feature;

    .line 646
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_DEPOSIT_SETTINGS_CARD_LINKING"

    const/16 v14, 0x19a

    const-string v15, "Enable viewing the card linking section in the Settings applet > Deposits. This section has been feature flagged starting in 4.83b."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_CARD_LINKING:Lcom/squareup/settings/server/Features$Feature;

    .line 648
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE"

    const/16 v14, 0x19b

    const-string v15, "Enable viewing the deposit schedule section in the Settings applet > Deposits. This section has been feature flagged starting in 4.83b."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    .line 651
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_GIFT_CARDS_V2"

    const/16 v14, 0x19c

    const-string v15, "Allow merchants to use v2 Square gift cards, which are physical gift cards"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_GIFT_CARDS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 652
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_INVOICE_DEFAULTS_MOBILE"

    const/16 v14, 0x19d

    const-string v15, "Sync Invoice Defaults with server."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 653
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_ITEM_OPTIONS"

    const/16 v14, 0x19e

    const-string v15, "Sync Item Options and Values with server."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ITEM_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 654
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_ITEMIZED_PAYMENTS_TUTORIAL"

    const/16 v14, 0x19f

    const-string v15, "Enable showing the Itemized Payments Tutorial."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMIZED_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    .line 655
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_ITEMS_TUTORIAL"

    const/16 v14, 0x1a0

    const-string v15, "Enable showing / access to Items Tutorial."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    .line 656
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_LOCAL_PAPER_SIGNATURE_SETTING"

    const/16 v14, 0x1a1

    const-string v15, "Enable sign on printed receipt setting to be at the device level"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_LOCAL_PAPER_SIGNATURE_SETTING:Lcom/squareup/settings/server/Features$Feature;

    .line 658
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_MAGSTRIPE_ONLY_READERS"

    const/16 v18, 0x1a2

    const-string v19, "Allow magstripe-only reader"

    const-string v20, ""

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    .line 659
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_V2_BLE_STATE_MACHINE"

    const/16 v14, 0x1a3

    const-string v15, "Use new (2/19) BLE connection state machine"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_V2_BLE_STATE_MACHINE:Lcom/squareup/settings/server/Features$Feature;

    .line 660
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_V2_CARDREADERS"

    const/16 v14, 0x1a4

    const-string v15, "Use new (Q4 \'19) cardreader connection code"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_V2_CARDREADERS:Lcom/squareup/settings/server/Features$Feature;

    .line 661
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    sget-object v20, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_ORDER_ENTRY_SCREEN_V2_ANDROID"

    const/16 v18, 0x1a5

    const-string v19, "Use tabs with icons at bottom of order entry screen (RA-36357)"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    .line 663
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_POS_INTENT_PAYMENTS_TUTORIAL"

    const/16 v14, 0x1a6

    const-string v15, "Enable starting the Payments Tutorial on Library or Favorites for sellers with \'POS\' Best Available Product Intent."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_POS_INTENT_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    .line 665
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_PRICING_ENGINE"

    const/16 v18, 0x1a7

    const-string v19, "Use Pricing Engine"

    const-string v20, "Use the Pricing Engine to show and search for discounts based on pricing rules"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    .line 667
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "USE_R6"

    const/16 v23, 0x1a8

    const-string v24, "R6"

    const-string v25, ""

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    .line 668
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_R12"

    const/16 v18, 0x1a9

    const-string v19, "R12"

    const-string v20, ""

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    .line 669
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_REPORTS_GRANULAR_PERMISSIONS"

    const/16 v14, 0x1aa

    const-string v15, "Use granular permissions for viewing reports."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 670
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_SAFETYNET"

    const/16 v18, 0x1ab

    const-string v19, "Enable SafetyNet"

    const-string v20, "SafetyNet is a Google Play services for app and device integrity and security."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SAFETYNET:Lcom/squareup/settings/server/Features$Feature;

    .line 672
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_SAME_DAY_DEPOSIT"

    const/16 v14, 0x1ac

    const-string v15, "Whether sellers are able to use same day (scheduled instant) deposit. Currently used for controlling whether they see the \'Same-Day Deposit\' help paragraph in the Deposits Report Help page on mobile."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SAME_DAY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    .line 675
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_SETTINGS_GRANULAR_PERMISSIONS"

    const/16 v14, 0x1ad

    const-string v15, "Use granular permissions for changing settings."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    .line 676
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_SPLIT_TENDER"

    const/16 v14, 0x1ae

    const-string v15, "Use split tender"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER:Lcom/squareup/settings/server/Features$Feature;

    .line 677
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_SPLIT_TENDER_WORKFLOW"

    const/16 v14, 0x1af

    const-string v15, "Use the split tender workflow to drive the creation andediting of split tenders"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    .line 679
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS"

    const/16 v14, 0x1b0

    const-string v15, "Use a unique queue for local payments"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 680
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_TABLE_MANAGEMENT"

    const/16 v14, 0x1b1

    const-string v15, "Enable table management for RST merchants"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_TABLE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 681
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_UPDATED_GUEST_ACCESS"

    const/16 v18, 0x1b2

    const-string v19, "Enable Updated Guest Access"

    const-string v20, "Hides employee passcode login prompt when in guest mode."

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    .line 683
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_BUYER_DISPLAY_SETTINGS_V2_X2"

    const/16 v14, 0x1b3

    const-string v15, "Use V2 of the Buyer Display settings screen on X2. V2 includes a big revamp to the UI, along with support for multiple Bran images."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_BUYER_DISPLAY_SETTINGS_V2_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 685
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_V3_CATALOG"

    const/16 v14, 0x1b4

    const-string v15, "Merchant is migrated to the catalog V3 datastore and can use the V3 API"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_V3_CATALOG:Lcom/squareup/settings/server/Features$Feature;

    .line 686
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_X2_PAYMENT_WORKFLOWS"

    const/16 v14, 0x1b5

    const-string v15, "Enable the X2 payment workflows, intended to replace most of the logic in X2 monitors."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_X2_PAYMENT_WORKFLOWS:Lcom/squareup/settings/server/Features$Feature;

    .line 688
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "USE_CONFIGURED_BUSINESS_HOURS"

    const/16 v14, 0x1b6

    const-string v15, "Whether end of day can be determined by configured business hours."

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_CONFIGURED_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

    .line 690
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION"

    const/16 v18, 0x1b7

    const-string v19, "Use View Detailed Sales Reports permissions"

    const-string v20, "Add Basic Sales Report and a new permission for detailed reports"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    .line 692
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "VERIFY_ADDRESS"

    const/16 v14, 0x1b8

    const-string v15, "Validate addresses before requesting a reader"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->VERIFY_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    .line 693
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "VALIDATE_SESSION_ON_START"

    const/16 v14, 0x1b9

    const-string v15, "Validate multipass session is still valid at app start"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->VALIDATE_SESSION_ON_START:Lcom/squareup/settings/server/Features$Feature;

    .line 694
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "VOID_COMP"

    const/16 v18, 0x1ba

    const-string v19, "Void Comp"

    const-string v20, "Allow sellers to void and comp items"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->VOID_COMP:Lcom/squareup/settings/server/Features$Feature;

    .line 695
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v13, "WAIT_FOR_BRAN_X2"

    const/16 v14, 0x1bb

    const-string v15, "Enable Hodor to block the UI and wait for Bran when switching monitor"

    invoke-direct {v0, v13, v14, v15, v2}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->WAIT_FOR_BRAN_X2:Lcom/squareup/settings/server/Features$Feature;

    .line 696
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v17, "ZERO_ARQC_TEST"

    const/16 v18, 0x1bc

    const-string v19, "Zero ARQC Test"

    const-string v20, "Start contactless payments for $0"

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    invoke-direct/range {v16 .. v21}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ZERO_ARQC_TEST:Lcom/squareup/settings/server/Features$Feature;

    .line 697
    new-instance v0, Lcom/squareup/settings/server/Features$Feature;

    new-array v2, v1, [Lcom/squareup/settings/server/Features$Feature;

    const-string v22, "ZERO_TENDER"

    const/16 v23, 0x1bd

    const-string v24, "Zero Tender"

    const-string v25, "Use Zero Amount tender in place of $0 cash tender"

    move-object/from16 v21, v0

    move-object/from16 v26, v2

    invoke-direct/range {v21 .. v26}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->ZERO_TENDER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v0, 0x1be

    new-array v0, v0, [Lcom/squareup/settings/server/Features$Feature;

    .line 32
    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ACCEPT_THIRD_PARTY_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_BANNER:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_NOTIFICATIONS:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ADJUST_INVENTORY_AFTER_ORDER_CANCELATION:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ALLOW_EXIT_JAIL_SCREEN_ON_ERROR:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ALWAYS_SHOW_ITEMIZED_CART:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_FORCE_SYNC:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_STAFF_HOURS:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_APPOINTMENT_LOCATION_EDIT:Lcom/squareup/settings/server/Features$Feature;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS_API:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS_SQUARE_ASSISTANT_EDU_MODAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_LIST_OF_EVENTS_VIEW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_ANDROID_EDIT_RECURRING_APPOINTMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_ANDROID_RESOURCE_BOOKING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SQUARE_ASSISTANT_CONVERSATIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_MULTISTAFF_CALENDAR:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_USE_SHORT_IDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_USE_PAGINATED_SYNCING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ATTEMPT_DEVICE_ID_UPDATE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->AUTO_CAPTURE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->AVAILABILITY_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_BALANCE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CAN_MANAGE_SQUARE_CARD:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CARD_FREEZE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_GOOGLE_PAY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_ACTIVATION_BILLING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STAMPS_CUSTOMIZATION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_SHOW_RESET_PIN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_NOTIFICATION_PREFERENCES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_COLLECT_MOBILE_PHONE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_UNIFIED_ACTIVITY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_SQUARE_CARD_PAN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_ACCOUNT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_POST_SIGNUP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINKING_WITH_TWO_ACCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_RESEND_EMAIL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BRAN_CART_SCROLL_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_API:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_CART_MONITOR_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BRAN_MULTIPLE_IMAGES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUNDLE_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ALLOW_SWIPE_FOR_CHIP_CARDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ALWAYS_SKIP_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_EMPLOYEE_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_OT_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ENABLE_TIPPING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_PAUSE_NIGHTLY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R12:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_COMPACT_TICKETS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_SINGLE_TICKET_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SPLIT_EMONEY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_ANDROID_ECR:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_QR_CODES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_INSTALLMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_TENDER_IN_EDIT_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BLE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_COVER_COUNTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_PAYROLL_UPSELL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SMALL_RED_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAPITAL_CAN_MANAGE_FLEX_OFFER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAPITAL_CAN_MANAGE_FLEX_PLAN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_NOT_PRESENT_SIGN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_POST_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_SIGN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CHECKOUT_APPLET_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CLOCK_SKEW_LOCKOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_CNP_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_COF_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_PREFERRED:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CONDITIONAL_TAXES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_EMV:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_ADHOC_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_BUYER_EMAIL_COLLECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_DISABLE_BUYER_PROFILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_DELETE_GROUPS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_GROUPS_AND_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MERGE_CONTACTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CAN_CONFIGURE_PROFILES_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_TABLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSIT_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_ALLOW_ADD_MONEY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_DETAIL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_CARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DO_NOT_USE_TASK_QUEUE_FOR_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISABLE_ITEM_EDITING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISABLE_MAGSTRIPE_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISALLOW_ITEMSFE_INVENTORY_API:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISMISS_REFERRAL_BADGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISMISS_TUTORIAL_BADGES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_LEARN_MORE_R4:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPUTES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPUTES_CHALLENGES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DUPLICATE_SKU_ON_ITEMS_APPLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_BUY_LINKS_SILENT_AUTH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_EPSON_PRINTERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_PAY_LINKS_ON_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_SERVICES_CATALOG_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_TAX_BASIS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMAIL_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMONEY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMONEY_SPEED_TEST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ESTIMATE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FAST_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FEATURE_TOUR_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_CNP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIRMWARE_UPDATE_JAIL_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIXED_PRICE_SERVICE_PRICE_EDITING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FORCED_OFFLINE_MODE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_REFUNDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_HISTORY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_IME:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_SETTINGS_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HAS_BAZAAR_ONLINE_STORE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_SUPPORT_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_SUPPORT_MESSAGING_TABLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HIDE_ANNOUNCEMENTS_SECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HIDE_MODIFIERS_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_ACCESSIBILITY_SCRUBBER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_AVAILABILITY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_PLUS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICE_APP_BANNER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ALLOW_PDF_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APP_ONBOARDING_FLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_CUSTOM_REMINDER_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_DOWNLOAD_INVOICE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_ESTIMATE_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ESTIMATES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ESTIMATES_PACKAGES_READONLY_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EVENT_TIMELINE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_HOME_TAB:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PARTIALLY_PAY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PREVIEW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RATING_PROMPT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECORD_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING_CANCEL_NEXT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_TIMELINE_CTA_LINKING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_VISIBLE_PUSH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_MODIFIERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IPOS_NO_IDV_EXPERIMENT_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_V1_DEPRECATION_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_GLOBAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_LOCAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_EDIT_MASTER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->JUMBOTRON_SERVICE_KEY_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LANDING_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LANDING_WORLD:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LIBRARY_LIST_SHOW_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LIMIT_VARIATIONS_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ADJUST_POINTS_SMS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ALPHANUMERIC_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CAN_ADJUST_POINTS_NEGATIVE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CASH_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CHECKOUT_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ENROLLMENT_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_EXPIRE_POINTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_HANDLE_RETURN_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ITEM_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_TUTORIALS_ENROLLMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_POST_TRANSACTION_DARK_THEME:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES_BETA:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->O1_DEPRECATION_WARNING_JP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ALLOW_SCREENSHOT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ASK_INTENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_BANK:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_CHOOSE_DEFAULT_DEPOSIT_METHOD:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_FREE_READER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_QUOTA:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SERVER_DRIVEN_FLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SUPPRESS_AUTO_FPT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_X2_VERTICAL_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_BULK_DELETE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_DIP_TAP_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MERGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SHOW_BETA_OPT_IN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SWIPE_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TRANSFER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2_NAME_AND_NOTES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_ALLOW_BAZAAR_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CAN_SEARCH_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CHECK_EMPLOYEE_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SHOW_QUICK_ACTIONS_SETTING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_ECOM_DELIVERY_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_UPCOMING_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_THROTTLE_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAPER_SIGNATURE_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PERFORM_CATALOG_SYNC_AFTER_TRANSACTION_ON_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PERSISTENT_BUNDLE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PLAY_PAYMENT_APPROVED_SOUND_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINT_MERCHANT_LOGO_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINT_ITEMIZED_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DITHERING_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTING_DEBUG_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->R12_EARLY_K400_POWERUP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_209030_QUICKCHIP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_ACCOUNT_TYPE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_COMMON_DEBIT_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_FELICA_NOTIFICATION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_PINBLOCK_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SPOC_PRNG_SEED:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x138

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x139

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_ORDER_HISTORY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_PRINT_DISPOSITION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_SEE_MEASUREMENT_UNIT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_BADGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x13f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x140

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->RESILIENT_BUS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x141

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->RETAIN_PRINTER_CONNECTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x142

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PORTRAIT_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x143

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PREFER_SQLITE_TASKS_QUEUE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x144

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINT_TAX_PERCENTAGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x145

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x146

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REDUCE_PRINTING_WASTE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x147

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x148

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x149

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REMOTELY_CLOSE_OPEN_CASH_DRAWERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP_EMAIL_CONFIRM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRES_TRACK_2_IF_NOT_AMEX:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x14f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x150

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->RESTART_APP_AFTER_CRASH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x151

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REWARDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x152

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->S2_HODOR:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x153

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SAMPLE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x154

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SALES_LIMITS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x155

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SALES_SUMMARY_CHARTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x156

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x157

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2_FEATURE_CAROUSEL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x158

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SANITIZE_EVENTSTREAM_COORDINATES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x159

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SEPARATED_PRINTOUTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SETUP_GUIDE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ACCIDENTAL_CASH_MODAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_BUY_BUTTON:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_CHECKOUT_LINKS_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x15f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_CP_PRICING_CHANGE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x160

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_FEE_BREAKDOWN_TABLE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x161

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_INCLUSIVE_TAXES_IN_CART:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x162

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x163

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_LOYALTY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x164

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_LOYALTY_VALUE_METRICS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x165

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_NOHO_RECEIPT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x166

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x167

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_PAY_ONLINE_TENDER_OPTION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x168

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_SPOC_VERSION_NUMBER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x169

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKEND_BALANCE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKVIEW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKIP_MODIFIER_DETAIL_SCREEN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x16f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x170

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x171

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SMS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x172

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_CRQ:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x173

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x174

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_TMS_LOGIN_CHECK:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x175

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPLIT_TICKET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x176

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPM_SQUID:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x177

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPM_SQUID_LOG_TO_ES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x178

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SQUID_PTS_COMPLIANCE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x179

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->STATUS_BAR_FULLSCREEN_T2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SWIPE_INSTANT_DEPOSIT_CARD:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_API_URL_LIST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_BREAK_TRACKING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_FILE_SIZE_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_IN_APP_TIMECARDS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x17f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x180

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TERMINAL_API:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x181

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TERMINAL_API_SPM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x182

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TIME_TRACKING_DEVICE_LEVEL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x183

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_RECEIPT_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x184

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_TEAM_MEMBER_NOTES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x185

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TMN_RECORD_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x186

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TMN_VERBOSE_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x187

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x188

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x189

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER_STREAM:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_X2_WIFI_EVENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ACCESSIBLE_PIN_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ALTERNATE_SALES_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_AUTH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x18f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_AUTOMATIC_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x190

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x191

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE_ON_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x192

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_OUT_REASON:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x193

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x194

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_COMP_VOID_ASSIGN_MOVE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x195

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TICKET_RST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x196

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x197

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_METHOD_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x198

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x199

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_CARD_LINKING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_GIFT_CARDS_V2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEM_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMIZED_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x19f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_LOCAL_PAPER_SIGNATURE_SETTING:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_V2_BLE_STATE_MACHINE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_V2_CARDREADERS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_POS_INTENT_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1a9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1aa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SAFETYNET:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1ab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SAME_DAY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1ac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1ad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1ae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1af

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_TABLE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_BUYER_DISPLAY_SETTINGS_V2_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_V3_CATALOG:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_X2_PAYMENT_WORKFLOWS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CONFIGURED_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VERIFY_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VALIDATE_SESSION_ON_START:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1b9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VOID_COMP:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1ba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->WAIT_FOR_BRAN_X2:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1bb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ZERO_ARQC_TEST:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1bc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ZERO_TENDER:Lcom/squareup/settings/server/Features$Feature;

    const/16 v2, 0x1bd

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/settings/server/Features$Feature;->$VALUES:[Lcom/squareup/settings/server/Features$Feature;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/server/Features$Feature$DeviceType;",
            "[",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 715
    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/server/Features$Feature$DeviceType;",
            "[",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")V"
        }
    .end annotation

    .line 718
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 719
    iput-object p4, p0, Lcom/squareup/settings/server/Features$Feature;->description:Ljava/lang/String;

    .line 720
    iput-object p5, p0, Lcom/squareup/settings/server/Features$Feature;->deviceType:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    .line 721
    iput-object p6, p0, Lcom/squareup/settings/server/Features$Feature;->prereqs:[Lcom/squareup/settings/server/Features$Feature;

    if-nez p3, :cond_0

    .line 722
    invoke-direct {p0}, Lcom/squareup/settings/server/Features$Feature;->buildPrettyName()Ljava/lang/String;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lcom/squareup/settings/server/Features$Feature;->name:Ljava/lang/String;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")V"
        }
    .end annotation

    .line 711
    sget-object v5, Lcom/squareup/settings/server/Features$Feature$DeviceType;->BOTH:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;[Lcom/squareup/settings/server/Features$Feature;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")V"
        }
    .end annotation

    .line 707
    sget-object v5, Lcom/squareup/settings/server/Features$Feature$DeviceType;->BOTH:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/Features$Feature;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/settings/server/Features$Feature$DeviceType;[Lcom/squareup/settings/server/Features$Feature;)V

    return-void
.end method

.method private buildPrettyName()Ljava/lang/String;
    .locals 3

    .line 730
    invoke-virtual {p0}, Lcom/squareup/settings/server/Features$Feature;->name()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->forceTitleCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRecursivePrereqs(Ljava/util/Set;Lcom/squareup/settings/server/Features$Feature;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/settings/server/Features$Feature;",
            ">;",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")V"
        }
    .end annotation

    .line 748
    iget-object p2, p2, Lcom/squareup/settings/server/Features$Feature;->prereqs:[Lcom/squareup/settings/server/Features$Feature;

    if-eqz p2, :cond_1

    .line 749
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p2, v1

    .line 750
    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 751
    invoke-interface {p1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 752
    invoke-direct {p0, p1, v2}, Lcom/squareup/settings/server/Features$Feature;->getRecursivePrereqs(Ljava/util/Set;Lcom/squareup/settings/server/Features$Feature;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/Features$Feature;
    .locals 1

    .line 32
    const-class v0, Lcom/squareup/settings/server/Features$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/Features$Feature;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/server/Features$Feature;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->$VALUES:[Lcom/squareup/settings/server/Features$Feature;

    invoke-virtual {v0}, [Lcom/squareup/settings/server/Features$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/server/Features$Feature;

    return-object v0
.end method


# virtual methods
.method public deviceType()Lcom/squareup/settings/server/Features$Feature$DeviceType;
    .locals 1

    .line 734
    iget-object v0, p0, Lcom/squareup/settings/server/Features$Feature;->deviceType:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 726
    iget-object v0, p0, Lcom/squareup/settings/server/Features$Feature;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRecursivePrereqs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/settings/server/Features$Feature;",
            ">;"
        }
    .end annotation

    .line 742
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 743
    invoke-direct {p0, v0, p0}, Lcom/squareup/settings/server/Features$Feature;->getRecursivePrereqs(Ljava/util/Set;Lcom/squareup/settings/server/Features$Feature;)V

    return-object v0
.end method
