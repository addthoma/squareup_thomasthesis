.class public Lcom/squareup/settings/server/FeatureFlagFeatures;
.super Ljava/lang/Object;
.source "FeatureFlagFeatures.java"

# interfaces
.implements Lcom/squareup/settings/server/Features;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;,
        Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final featureFlags:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/settings/server/Features$Feature;",
            "Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;",
            ">;"
        }
    .end annotation
.end field

.field private final isTablet:Z

.field private final lazyAccountStatus:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljavax/inject/Provider;Ldagger/Lazy;Lcom/squareup/util/Device;ZLcom/squareup/settings/server/FeatureServiceVertical;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Z",
            "Lcom/squareup/settings/server/FeatureServiceVertical;",
            ")V"
        }
    .end annotation

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->featureFlags:Ljava/util/Map;

    .line 82
    iput-object p1, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->accountStatusProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p2, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->lazyAccountStatus:Ldagger/Lazy;

    .line 84
    invoke-interface {p3}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->isTablet:Z

    .line 85
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ACCEPT_THIRD_PARTY_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$BGAlEK2vUBMUQcqSbwbW41VCVyQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$BGAlEK2vUBMUQcqSbwbW41VCVyQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 87
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$S6VJJLAD-QYLvmaetJZ_xtpxW9s;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$S6VJJLAD-QYLvmaetJZ_xtpxW9s;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 89
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_BANNER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$poFE6CWPFWyN3xczxobc8g6pqTQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$poFE6CWPFWyN3xczxobc8g6pqTQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 91
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_NOTIFICATIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8f56XUJacCFbFUNV3E8fKfEMQoA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8f56XUJacCFbFUNV3E8fKfEMQoA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 93
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ADJUST_INVENTORY_AFTER_ORDER_CANCELATION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_gBUosLOzwfuHPCJuQJVQ2JoZn8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_gBUosLOzwfuHPCJuQJVQ2JoZn8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 95
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ALWAYS_SHOW_ITEMIZED_CART:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Iizeg7SlHJ-GBF6JJ1156b8HRzs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Iizeg7SlHJ-GBF6JJ1156b8HRzs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 97
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_MULTISTAFF_CALENDAR:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$H6YbUGEVVGXqzDnmTiuIljhJbaA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$H6YbUGEVVGXqzDnmTiuIljhJbaA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 99
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ATTEMPT_DEVICE_ID_UPDATE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$eUSnLknL_0_aFdw00M86xGRXkIs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$eUSnLknL_0_aFdw00M86xGRXkIs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 101
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->AUTO_CAPTURE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$khf33t4HHrUWoegoGR5Pu51vAJ0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$khf33t4HHrUWoegoGR5Pu51vAJ0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 102
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->AVAILABILITY_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$535R1_LvpjieXuQEE0ssEyw1W9Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$535R1_LvpjieXuQEE0ssEyw1W9Y;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 104
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_BALANCE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hwdzpuH9gkU4wHyfReYDKMDjBcw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hwdzpuH9gkU4wHyfReYDKMDjBcw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 106
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CAN_MANAGE_SQUARE_CARD:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$e1AmIUZPrr4Fq6jFSN6traKs674;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$e1AmIUZPrr4Fq6jFSN6traKs674;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 108
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$g4NicQ4toon7Tb1fCHDpsroB4oE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$g4NicQ4toon7Tb1fCHDpsroB4oE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 110
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CARD_FREEZE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hWX9ilCwgmGvUmNj7LF8FT6EuX0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hWX9ilCwgmGvUmNj7LF8FT6EuX0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 112
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_GOOGLE_PAY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FQjbcJVed1aBYPyQgBkgXw1bIpk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FQjbcJVed1aBYPyQgBkgXw1bIpk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 114
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_ACTIVATION_BILLING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EoJ_PiGAi_QP9-VqOtGRU8usKeY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EoJ_PiGAi_QP9-VqOtGRU8usKeY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 116
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STAMPS_CUSTOMIZATION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TT_4kmaJ4FCcA0WnQwJK9zMSEE8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TT_4kmaJ4FCcA0WnQwJK9zMSEE8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 118
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FtJuwxsXCxMblxphYUltaIQbR7g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FtJuwxsXCxMblxphYUltaIQbR7g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 120
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$62fow1MK40gBT2an1VEyHzEdlcQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$62fow1MK40gBT2an1VEyHzEdlcQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 122
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_COLLECT_MOBILE_PHONE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$REJQc_Xc7FPMOVpzV_n7Z0inYX0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$REJQc_Xc7FPMOVpzV_n7Z0inYX0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 124
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_UNIFIED_ACTIVITY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mZrVhbU4apW77cKbeRVysE6Jr8U;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mZrVhbU4apW77cKbeRVysE6Jr8U;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 126
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_SQUARE_CARD_PAN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_lpK9txI6_JACt10H4SF2LePr7s;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_lpK9txI6_JACt10H4SF2LePr7s;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 128
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_SHOW_RESET_PIN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jkj3_HXSS3ilpISv_pCGI5MZn9c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jkj3_HXSS3ilpISv_pCGI5MZn9c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 130
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_NOTIFICATION_PREFERENCES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZW5XLtnCenqbqS4GgDf_62yOEfI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZW5XLtnCenqbqS4GgDf_62yOEfI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 132
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BANK_ACCOUNT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5vp04SHfBYeQyWszorHir5M1evc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5vp04SHfBYeQyWszorHir5M1evc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 133
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_POST_SIGNUP:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$i9HdYvdh8ZGrevaYb9DBoJZUWwU;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$i9HdYvdh8ZGrevaYb9DBoJZUWwU;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 135
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dqxn3QjA24pfj4YNFhf9RYIAJ3M;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dqxn3QjA24pfj4YNFhf9RYIAJ3M;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 137
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINKING_WITH_TWO_ACCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IbAfWapEPbeWTkQzoSW2n2DPuBA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IbAfWapEPbeWTkQzoSW2n2DPuBA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 139
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BANK_RESEND_EMAIL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p4AqDp_LLPud6Bv4vXk60vpuDA4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p4AqDp_LLPud6Bv4vXk60vpuDA4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 141
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lMLVUjIx1wkUMKc31QlLfy80ceo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lMLVUjIx1wkUMKc31QlLfy80ceo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 143
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BRAN_CART_SCROLL_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lsZVNzqryjDJS5t9fgRz0E8TB8w;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lsZVNzqryjDJS5t9fgRz0E8TB8w;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 146
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_API:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NhvDQRUxoAgcZd2TYIr6w07pJLs;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NhvDQRUxoAgcZd2TYIr6w07pJLs;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 149
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BRAN_DISPLAY_CART_MONITOR_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Mi8Kk-kn01_MKneU6zTa6-MdCQ0;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Mi8Kk-kn01_MKneU6zTa6-MdCQ0;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 153
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BRAN_MULTIPLE_IMAGES:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$SZvPkIS_w041cYizhjddxuX1eQw;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$SZvPkIS_w041cYizhjddxuX1eQw;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 156
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4JNNBycBI8mH9DhyaR7NBCOjNKk;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4JNNBycBI8mH9DhyaR7NBCOjNKk;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 159
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BUNDLE_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2J9BsGbziGk-m9Mha9cnfM55tFY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2J9BsGbziGk-m9Mha9cnfM55tFY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 161
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$bb6a7YdLbmJP98C4iTdokjhsF24;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$bb6a7YdLbmJP98C4iTdokjhsF24;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 163
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_ALLOW_SWIPE_FOR_CHIP_CARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UmX8rV6mum-b8hSJ-hO1Lgozduk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UmX8rV6mum-b8hSJ-hO1Lgozduk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 165
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_ALWAYS_SKIP_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$HRuYH4mvA022qPd5bRNqRTMJ6R0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$HRuYH4mvA022qPd5bRNqRTMJ6R0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 167
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_ENABLE_TIPPING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$L11ylUSbzB9Zu6zLuf4TZV4tnkU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$L11ylUSbzB9Zu6zLuf4TZV4tnkU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 169
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ALLOW_EXIT_JAIL_SCREEN_ON_ERROR:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$QvUK9i2S94Ej_nwLFSB9WP-RHho;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$QvUK9i2S94Ej_nwLFSB9WP-RHho;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 171
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_PAUSE_NIGHTLY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9pfTIWXTcBbEhjUMjmvQ25HsAoA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9pfTIWXTcBbEhjUMjmvQ25HsAoA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 173
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R12:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AW3eHKVZO7VYg_tOAGfRndRYA50;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AW3eHKVZO7VYg_tOAGfRndRYA50;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 175
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4uVM1765D7EdoKyhp34Os49_85A;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4uVM1765D7EdoKyhp34Os49_85A;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 177
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_COMPACT_TICKETS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$g0I0s1-30o9QAFv2Q8dDWRC6E78;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$g0I0s1-30o9QAFv2Q8dDWRC6E78;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 179
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_SINGLE_TICKET_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p2LbxcvQf8Kmzqn3QwEiCnAdAy4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p2LbxcvQf8Kmzqn3QwEiCnAdAy4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 181
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SPLIT_EMONEY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZYtUXVKpgMHnpIY8N4rDjyJjJKQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZYtUXVKpgMHnpIY8N4rDjyJjJKQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 183
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_PAYROLL_UPSELL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$PIPNFaeb_zGHyy-poaeu_GdUJIQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$PIPNFaeb_zGHyy-poaeu_GdUJIQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 188
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SMALL_RED_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1T5OiWrtwBRRr6ZGoJsSmj_nzT8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1T5OiWrtwBRRr6ZGoJsSmj_nzT8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 190
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$f9S9lQe4nshYMT6xwGcgdt3Kktw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$f9S9lQe4nshYMT6xwGcgdt3Kktw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 192
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$3Dl2JlgFhb2MtmYH3TDZXGoF6Ak;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$3Dl2JlgFhb2MtmYH3TDZXGoF6Ak;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 194
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DDdp-osLY7Jq2miMnlVIgGdh0XA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DDdp-osLY7Jq2miMnlVIgGdh0XA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 196
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_ANDROID_ECR:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-KM4qoOWyiwbejnki3mtnqSVKug;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-KM4qoOWyiwbejnki3mtnqSVKug;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 200
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$A_hulXmyst_gPg1aZZ7DjySPg6g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$A_hulXmyst_gPg1aZZ7DjySPg6g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 202
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IfAXXmgYpFLscIwoEE1tPyrsDfw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IfAXXmgYpFLscIwoEE1tPyrsDfw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 204
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_CASH_QR_CODES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$BWJexOZjLuacVExy6ZAq1UZ7wmk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$BWJexOZjLuacVExy6ZAq1UZ7wmk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 206
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_INSTALLMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1TndYubR0tEEPXcpP5Q00MvVNzc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1TndYubR0tEEPXcpP5Q00MvVNzc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 208
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_TENDER_IN_EDIT_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gBx0QVdgdKw_ywxiWxMC3hKamf4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gBx0QVdgdKw_ywxiWxMC3hKamf4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 210
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2JksnxIUrudXtfNiJgmgQR2Lv0o;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2JksnxIUrudXtfNiJgmgQR2Lv0o;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 212
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BLE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$i1edMULwO7D-Q5j3FrLyq7bZxpM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$i1edMULwO7D-Q5j3FrLyq7bZxpM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 214
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAPITAL_CAN_MANAGE_FLEX_OFFER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YzFKxOLKfkTIYdq9r5itARzOraQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YzFKxOLKfkTIYdq9r5itARzOraQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 216
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_NOT_PRESENT_SIGN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FX5XAOQ3DntGYW1jvoKswXG6KJs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FX5XAOQ3DntGYW1jvoKswXG6KJs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 218
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZiNIF6Yw0SKO9teTBiwV1ItXrgA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZiNIF6Yw0SKO9teTBiwV1ItXrgA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 220
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM_X2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dvc_qY_Q5q0oC5DMyJTdGC6gdGY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dvc_qY_Q5q0oC5DMyJTdGC6gdGY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 222
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_POST_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6qSmxcBM0LZiJVn2ZEs3yz5joaU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6qSmxcBM0LZiJVn2ZEs3yz5joaU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 224
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cIm6aWjc5Iz3muaUEbUTtbzgHh4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cIm6aWjc5Iz3muaUEbUTtbzgHh4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 226
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_SIGN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1GLKvlEWm26Gymwt_BB1QsRe0xs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1GLKvlEWm26Gymwt_BB1QsRe0xs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 228
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CHECKOUT_APPLET_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$kKVDizMPWCauaa1f-71twis79Tk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$kKVDizMPWCauaa1f-71twis79Tk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 230
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CONDITIONAL_TAXES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1dnwNXHsLL8jwVCJ3GFDvhSCCnU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1dnwNXHsLL8jwVCJ3GFDvhSCCnU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 232
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_CNP_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mYdq_6UJ4y6tVGW_RhmBuHYCRbA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mYdq_6UJ4y6tVGW_RhmBuHYCRbA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 234
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_COF_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$34LkXqUHsBy6Je7BoMgPFfvVQwA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$34LkXqUHsBy6Je7BoMgPFfvVQwA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 236
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_PREFERRED:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4lppMALOf3_jGc8pEOyDOF76PMs;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4lppMALOf3_jGc8pEOyDOF76PMs;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 239
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9fUK9_t095pDpk6K_hCk73UsTdo;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9fUK9_t095pDpk6K_hCk73UsTdo;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 242
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TERMINAL_API:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$tKkR6wQccWjWg8ZSvjtCzhufQZA;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$tKkR6wQccWjWg8ZSvjtCzhufQZA;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 245
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$00GXPDs3pkWrhvhsn4zqYOO4PeM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$00GXPDs3pkWrhvhsn4zqYOO4PeM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 247
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$F7k5YcqlMU619vqom9DVUmyV39U;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$F7k5YcqlMU619vqom9DVUmyV39U;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 249
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_EMV:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wc63it32uZWHNuys1A91TxKicLM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wc63it32uZWHNuys1A91TxKicLM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 251
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_ADHOC_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pzI06JEPmTZ5DzikTZcoTVGq07M;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pzI06JEPmTZ5DzikTZcoTVGq07M;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 253
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_kmap3Mnl5thrTR5BrCsuzkffAc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_kmap3Mnl5thrTR5BrCsuzkffAc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 255
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_BUYER_EMAIL_COLLECTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OF1F5RnM4evvOw5tvcY81UiNAjY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OF1F5RnM4evvOw5tvcY81UiNAjY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 257
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_CAN_CONFIGURE_PROFILES_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$w0FnEDdXEBjDQnCeDUoSSTlhzoQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$w0FnEDdXEBjDQnCeDUoSSTlhzoQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 259
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Fya2seHFIcmqM5tVtSYA_8Ws5Xo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Fya2seHFIcmqM5tVtSYA_8Ws5Xo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 261
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_DELETE_GROUPS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XocilkFvMKUjQ5WgVzPksfjb5XQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XocilkFvMKUjQ5WgVzPksfjb5XQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 262
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_GROUPS_AND_FILTERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LC2tjBrf9AkET0Rj4pga2b50_po;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LC2tjBrf9AkET0Rj4pga2b50_po;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 264
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TlR5Mly2KceXbigEMv-jgnHehX4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TlR5Mly2KceXbigEMv-jgnHehX4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 265
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_MERGE_CONTACTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YUpsVZbyzss0BByiWIlu1Gu6iPo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YUpsVZbyzss0BByiWIlu1Gu6iPo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 266
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qdA-M5ZfTP1N55CMOtkYqsfDNqM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qdA-M5ZfTP1N55CMOtkYqsfDNqM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 268
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0vcZHKJL3IXjHj6mLWBcEU5eE1M;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0vcZHKJL3IXjHj6mLWBcEU5eE1M;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 269
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_NOTES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DrpVnOMbqa8JalH3QGsDsh72X7Q;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DrpVnOMbqa8JalH3QGsDsh72X7Q;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 270
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WkOgegrCPg6E-f2VOf7IuZYnkkE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WkOgegrCPg6E-f2VOf7IuZYnkkE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 271
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$av2V0B8TE9z1fmoEsGOp6H3S-f0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$av2V0B8TE9z1fmoEsGOp6H3S-f0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 273
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KwcdU9RvNMJt7nYetOKCGWJZCSY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KwcdU9RvNMJt7nYetOKCGWJZCSY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 275
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$MYKl4hC6Tg39rumotSaRhjm9em8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$MYKl4hC6Tg39rumotSaRhjm9em8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 277
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CRM_DISABLE_BUYER_PROFILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jlLub0xkZ0ppC2P8h11OQzP2Yyc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jlLub0xkZ0ppC2P8h11OQzP2Yyc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 279
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pjrYvMM9T1uxerBk6htk9f1BbmQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pjrYvMM9T1uxerBk6htk9f1BbmQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 281
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$s--kAZ9QNDqWp8NmHXXHmTLYiKc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$s--kAZ9QNDqWp8NmHXXHmTLYiKc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 283
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Hk5H91Ft0LGxHc3jQU5eBeVxsEI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Hk5H91Ft0LGxHc3jQU5eBeVxsEI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 285
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$l1seX8Tv_oQ8PyAgfmKjen3X9TE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$l1seX8Tv_oQ8PyAgfmKjen3X9TE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 287
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_TABLET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Cdo4zTbl4UfWgfeUigaZBhKEu1I;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Cdo4zTbl4UfWgfeUigaZBhKEu1I;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 289
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$HixDXyF-yABUclxxxvJ5Oj-q3e0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$HixDXyF-yABUclxxxvJ5Oj-q3e0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 291
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DEPOSIT_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pXXg4n8iVPGtAmuhFm-87qKzC5o;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pXXg4n8iVPGtAmuhFm-87qKzC5o;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 293
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fyd9p1QY7HulVCdhQOYJQmOVGWA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fyd9p1QY7HulVCdhQOYJQmOVGWA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 295
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_DETAIL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$B397dW_D95gHA8EKwe8QeQvmv1I;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$B397dW_D95gHA8EKwe8QeQvmv1I;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 297
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_CARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$40TMagEm0QoAifT8UYUN_wNuXuU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$40TMagEm0QoAifT8UYUN_wNuXuU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 299
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_ALLOW_ADD_MONEY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$7Ix8XorFSJq0rpz_ka2yYA8Leds;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$7Ix8XorFSJq0rpz_ka2yYA8Leds;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 301
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gmQ7UoW5ELY_P7OpnCP9LfQ-BNk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gmQ7UoW5ELY_P7OpnCP9LfQ-BNk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 303
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hk_6enn75jQrbSpNklltHK9sWcQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hk_6enn75jQrbSpNklltHK9sWcQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 305
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DO_NOT_USE_TASK_QUEUE_FOR_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$l-a-e0CdlEp_45gag8dwt7OsYus;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$l-a-e0CdlEp_45gag8dwt7OsYus;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 307
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cBCkG87tBj5Wws4gZvTiAt0TjTI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cBCkG87tBj5Wws4gZvTiAt0TjTI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 308
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISABLE_ITEM_EDITING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KlHabw8HTRfDbUorXBVG5MCj6Ww;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KlHabw8HTRfDbUorXBVG5MCj6Ww;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 310
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISABLE_MAGSTRIPE_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jiLrb4PuW0lNZTsfovs-2SMJOms;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jiLrb4PuW0lNZTsfovs-2SMJOms;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 312
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISALLOW_ITEMSFE_INVENTORY_API:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$HESSN2ULZdhmybI4I-rcym3nvmk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$HESSN2ULZdhmybI4I-rcym3nvmk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 314
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hAbQ4u6Atm8VMFzYpCCLlCwQdWk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hAbQ4u6Atm8VMFzYpCCLlCwQdWk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 316
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISMISS_REFERRAL_BADGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Xr4W1O_wbGXPIgWeFnFFdGerRus;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Xr4W1O_wbGXPIgWeFnFFdGerRus;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 318
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISMISS_TUTORIAL_BADGES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NwrD1wz22YuSz4BQTmxE4V2wffY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NwrD1wz22YuSz4BQTmxE4V2wffY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 320
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_LEARN_MORE_R4:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DQNhw376lX9-pPRPKBQLpxfrGAE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DQNhw376lX9-pPRPKBQLpxfrGAE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 322
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_USE_PAGINATED_SYNCING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EX3Hvll0mb0k2y7jKEB6UfpLpvc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EX3Hvll0mb0k2y7jKEB6UfpLpvc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 324
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_MODIFIER_INSTEAD_OF_OPTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WLF2Tb0j1Jk7rbGXOPvdEs9CZWI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WLF2Tb0j1Jk7rbGXOPvdEs9CZWI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 326
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DISPUTES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$M1bDlbMuV8bIKakOVc0YxXRghn0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$M1bDlbMuV8bIKakOVc0YxXRghn0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 328
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->DUPLICATE_SKU_ON_ITEMS_APPLET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VwoPBbu1-HI1KC0kcL1w1Yu-kOo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VwoPBbu1-HI1KC0kcL1w1Yu-kOo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 330
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8Y4uMtyAb43nQDlhHv6LOHkJAZc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8Y4uMtyAb43nQDlhHv6LOHkJAZc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 332
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Bq8S7m7LxgrSvOMR86-dBCWkBaA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Bq8S7m7LxgrSvOMR86-dBCWkBaA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 334
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_BUY_LINKS_SILENT_AUTH:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AopKpwN92CDilUO6y16S8WNP9mU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AopKpwN92CDilUO6y16S8WNP9mU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 336
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Y_FFx1KeM-QCSLBVtGiqAlaX39U;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Y_FFx1KeM-QCSLBVtGiqAlaX39U;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 338
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_PAY_LINKS_ON_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$iRpfBzbMzAyNDneBolR0SBUHjAw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$iRpfBzbMzAyNDneBolR0SBUHjAw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 340
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$7fFfaWMXZ-WCWGRvv5ajpQRwA9Q;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$7fFfaWMXZ-WCWGRvv5ajpQRwA9Q;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 342
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_SERVICES_CATALOG_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1SbXhNVCuAV_J1pbzJ6BqyADx5c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1SbXhNVCuAV_J1pbzJ6BqyADx5c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 344
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$O3Bf3s54O7gDL5t0Dqd5xJ-KXiQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$O3Bf3s54O7gDL5t0Dqd5xJ-KXiQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 346
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HIDE_MODIFIERS_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wtAWaP8AOybCRr5x1c4m0-4uTbc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wtAWaP8AOybCRr5x1c4m0-4uTbc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 348
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EMAIL_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AGpreHR9RZmriVx5UU8MtaCs-cc;

    invoke-direct {p2, p4}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AGpreHR9RZmriVx5UU8MtaCs-cc;-><init>(Z)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 350
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EMONEY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$C9YUqTlqPcd06mIi6vSYXsMur6k;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$C9YUqTlqPcd06mIi6vSYXsMur6k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 352
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EMONEY_SPEED_TEST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$j82DcQJB7OEAi_GcWOz4qvde5hQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$j82DcQJB7OEAi_GcWOz4qvde5hQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 354
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pJ-WsoE3y94eu--3NPDNaxETR7M;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pJ-WsoE3y94eu--3NPDNaxETR7M;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 356
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$RiSQoRVa4nqCp7xFN3sH5iGV6jA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$RiSQoRVa4nqCp7xFN3sH5iGV6jA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 358
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ESTIMATE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k1F0qCHrxnuYVGNSWKCwQ3KH4gU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k1F0qCHrxnuYVGNSWKCwQ3KH4gU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 360
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FAST_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_3ugCie-8fUDavxWoGzTYGk-UOs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_3ugCie-8fUDavxWoGzTYGk-UOs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 362
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FEATURE_TOUR_X2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k3opcEgV3Fa8lDw_4ESvaaQ-Ztc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k3opcEgV3Fa8lDw_4ESvaaQ-Ztc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 364
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hDg-FVoFfxKk3DoW78Xjh6HZbBI;

    invoke-direct {p2, p0}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hDg-FVoFfxKk3DoW78Xjh6HZbBI;-><init>(Lcom/squareup/settings/server/FeatureFlagFeatures;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 366
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_CNP:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cMsHBDLWkSXGMBXRimOL_KJLemM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cMsHBDLWkSXGMBXRimOL_KJLemM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 368
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fIAzGFEiBu3SZ3odaOq8VhfNyCo;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fIAzGFEiBu3SZ3odaOq8VhfNyCo;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 371
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_FILE_SIZE_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nBNYe6dYKGkIgQkUhR-g2am2-48;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nBNYe6dYKGkIgQkUhR-g2am2-48;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 373
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FIRMWARE_UPDATE_JAIL_T2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DEzT56N7efzWlIsobuMzQBfuwNw;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DEzT56N7efzWlIsobuMzQBfuwNw;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 375
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FIXED_PRICE_SERVICE_PRICE_EDITING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fDoFuA-895QzB_0xFU5o0LnLmfY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fDoFuA-895QzB_0xFU5o0LnLmfY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 377
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->FORCED_OFFLINE_MODE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$MAp7GX8WmtTyThgYj4VQ71Pk8z0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$MAp7GX8WmtTyThgYj4VQ71Pk8z0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 379
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_REFUNDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9amCxTliXApXKX1thf4m9vJ6LEI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9amCxTliXApXKX1thf4m9vJ6LEI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 381
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_IME:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dryHOV81xkSuMb3tnRdfzUvPgPw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dryHOV81xkSuMb3tnRdfzUvPgPw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 383
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$02a_KudAXfRI0ZFKcRy5VD0SSao;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$02a_KudAXfRI0ZFKcRy5VD0SSao;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 385
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$axumMfn4VjLUrEe9GW9ez3oYwk8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$axumMfn4VjLUrEe9GW9ez3oYwk8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 387
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$a8wFr80qylYFnsXos_a7iK9oSgk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$a8wFr80qylYFnsXos_a7iK9oSgk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 389
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_SETTINGS_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YRIBLLb93F3Bpwq5lkCphJJyqcc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YRIBLLb93F3Bpwq5lkCphJJyqcc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 391
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p2jdel1ERvkmX9S6C3lFy1eNCKM;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p2jdel1ERvkmX9S6C3lFy1eNCKM;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 393
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LNZVOmCGX1rmvc_RKGSauigS-4g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LNZVOmCGX1rmvc_RKGSauigS-4g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 396
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nJE0Y9-7gORvCERk2nff-bnkjxQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nJE0Y9-7gORvCERk2nff-bnkjxQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 398
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_ACCESSIBLE_PIN_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jDnjw65MiJx-DcHwndfB-0OWKeA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jDnjw65MiJx-DcHwndfB-0OWKeA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 400
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HAS_BAZAAR_ONLINE_STORE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OUEjgm4oPvgJErl0T8spBInkUdg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OUEjgm4oPvgJErl0T8spBInkUdg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 402
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$z52VYAwfnQp3xzAGUF8lG5d1Hz8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$z52VYAwfnQp3xzAGUF8lG5d1Hz8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 403
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_T2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$kxytktJZNM8EDpeGfdARSI9O7z8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$kxytktJZNM8EDpeGfdARSI9O7z8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 405
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$w7upAJBOsa3yeLYUXSeRnuNTmcA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$w7upAJBOsa3yeLYUXSeRnuNTmcA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 408
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI_X2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8gPqfyO1lcHfzC10COS9pomwTfM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8gPqfyO1lcHfzC10COS9pomwTfM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 410
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_SUPPORT_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Py9dSat0tKiN26P3F5_FWwvpEFo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Py9dSat0tKiN26P3F5_FWwvpEFo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 412
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_SUPPORT_MESSAGING_TABLET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9vAlIhZYXCbdfTr1N74z84a2aaU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9vAlIhZYXCbdfTr1N74z84a2aaU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 414
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HIDE_ANNOUNCEMENTS_SECTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zUs11cIjx78c3kHFnEyd8T_VD6I;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zUs11cIjx78c3kHFnEyd8T_VD6I;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 416
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$CTeqR4PTi8Ud-w8mrODIB0CeqKY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$CTeqR4PTi8Ud-w8mrODIB0CeqKY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 417
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8w2gN-0SiP9yN4cwVAYzQ1gIYDM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8w2gN-0SiP9yN4cwVAYzQ1gIYDM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 419
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4CweDpi85zJ224qk0BdK0OJmCbk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4CweDpi85zJ224qk0BdK0OJmCbk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 421
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AisdRi4IUMhMYQ6LDx5Ntx2Z-tI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AisdRi4IUMhMYQ6LDx5Ntx2Z-tI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 423
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_PLUS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IMHmHyCplJ_gALQUHenJtjgDj48;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IMHmHyCplJ_gALQUHenJtjgDj48;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 424
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICE_APP_BANNER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$80d_zNj0xTmw7Urh5HTNTWzD0ao;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$80d_zNj0xTmw7Urh5HTNTWzD0ao;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 426
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ALLOW_PDF_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qEN9u0Aw4PZRM-lsxnodYkt_ZpM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qEN9u0Aw4PZRM-lsxnodYkt_ZpM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 428
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Z0hXPtj6-MnLAWOCvsrH5AVMf8c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Z0hXPtj6-MnLAWOCvsrH5AVMf8c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 430
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APP_ONBOARDING_FLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$G7A3YUBaO_iQHJ4f2RQ6f5FGH5U;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$G7A3YUBaO_iQHJ4f2RQ6f5FGH5U;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 432
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$rN8wg2l_7pWyGI5UY4wJkyHtWfo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$rN8wg2l_7pWyGI5UY4wJkyHtWfo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 434
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YTIBmlWSiECOABELoYecwtGFkS4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YTIBmlWSiECOABELoYecwtGFkS4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 436
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$SfFVwAiMSB58BWaeVPx4QlNLF7M;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$SfFVwAiMSB58BWaeVPx4QlNLF7M;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 438
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k7mKzwplxlD_RI-Q8kzR8af8jNA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k7mKzwplxlD_RI-Q8kzR8af8jNA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 440
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hcASIrGS0wwdxlGBPc6aos6N-ZY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hcASIrGS0wwdxlGBPc6aos6N-ZY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 442
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$feAZocrw_SI7D0qqQuVMB4Lnbnc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$feAZocrw_SI7D0qqQuVMB4Lnbnc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 443
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_CUSTOM_REMINDER_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fxR5wIg_GwMgVbV_y4pKQB24YcU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fxR5wIg_GwMgVbV_y4pKQB24YcU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 445
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_DOWNLOAD_INVOICE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UtQ-vrPrnwaPnajRxWev_BMX6dE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UtQ-vrPrnwaPnajRxWev_BMX6dE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 447
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_ESTIMATE_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WZUf7TcumQYGjEmApFT7_KgPrZw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WZUf7TcumQYGjEmApFT7_KgPrZw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 449
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$keoxeTu7sBi56zf3L7pIOhBUjEI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$keoxeTu7sBi56zf3L7pIOhBUjEI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 451
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$woTnpPY8Bd2KbO4nhF3wdkJLTyg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$woTnpPY8Bd2KbO4nhF3wdkJLTyg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 453
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ESTIMATES_PACKAGES_READONLY_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hZJmYPMAYBIy74poW3ykfiUj_RU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hZJmYPMAYBIy74poW3ykfiUj_RU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 455
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EVENT_TIMELINE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EXsBjE4FRrwCagqQfsxuSSVgmdw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EXsBjE4FRrwCagqQfsxuSSVgmdw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 457
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1m7Jori9GasqdYkLPPjnY87gSow;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1m7Jori9GasqdYkLPPjnY87gSow;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 462
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_HOME_TAB:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cJupcrUYaEVmThIN1a0Z8Fpl_18;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cJupcrUYaEVmThIN1a0Z8Fpl_18;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 464
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2rkz1k0yyGRPR_NO7yjI9wuOQIM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2rkz1k0yyGRPR_NO7yjI9wuOQIM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 466
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PARTIALLY_PAY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UKNKe7RoxJVgZkROOi-jbByL068;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UKNKe7RoxJVgZkROOi-jbByL068;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 468
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$crUT_932ZSp9u088o9P3PflD58A;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$crUT_932ZSp9u088o9P3PflD58A;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 470
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING_CANCEL_NEXT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mBkaacfOiuQa6T_lG05p-KEninE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mBkaacfOiuQa6T_lG05p-KEninE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 472
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PREVIEW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lqRinGnd2RaRR0tkOTnC6CkLy9U;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lqRinGnd2RaRR0tkOTnC6CkLy9U;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 474
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECORD_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IGUxCdxUTTbiQnk21fy52ffL7OE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IGUxCdxUTTbiQnk21fy52ffL7OE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 476
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ESTIMATES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$s4t_unWmLi3-65Zyuobz8V1QQk8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$s4t_unWmLi3-65Zyuobz8V1QQk8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 478
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RATING_PROMPT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zivtjDwsfkwlrfhqMqJpM6objIk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zivtjDwsfkwlrfhqMqJpM6objIk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 480
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_TIMELINE_CTA_LINKING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0A739uhKdT0gSOMD1Cc2t2zNEqo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0A739uhKdT0gSOMD1Cc2t2zNEqo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 482
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_VISIBLE_PUSH:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5P32ffVutseDFHwkDpkqTcM3pWI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5P32ffVutseDFHwkDpkqTcM3pWI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 484
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IK2dHI6ZwVNfo2o2l8s6s4twnIg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IK2dHI6ZwVNfo2o2l8s6s4twnIg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 486
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_V1_DEPRECATION_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XAJlSj-xP-Bg-3D0BZZiPjnby7c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XAJlSj-xP-Bg-3D0BZZiPjnby7c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 488
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zvnKIp915iJIlOqqBoW3Ig0uXyM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zvnKIp915iJIlOqqBoW3Ig0uXyM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 490
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_MODIFIERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5qX5-NNQ7op-H0sUdtBJ-m1VSLU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5qX5-NNQ7op-H0sUdtBJ-m1VSLU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 492
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->IPOS_NO_IDV_EXPERIMENT_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0WvD6g8sCy7Ruya6hRuhlYhBatI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0WvD6g8sCy7Ruya6hRuhlYhBatI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 494
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_GLOBAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$SljJzkPrC-e2_59kUPDERG3VAgY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$SljJzkPrC-e2_59kUPDERG3VAgY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 496
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_LOCAL_EDIT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dP6fmHsGhZSnTm5a4_wYy4Xjimg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dP6fmHsGhZSnTm5a4_wYy4Xjimg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 498
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ITEM_OPTION_EDIT_MASTER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6UqCmR93lq5JqVpYsAhPGp0sjlU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6UqCmR93lq5JqVpYsAhPGp0sjlU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 500
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LIBRARY_LIST_SHOW_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$iSAxTeLNCo55diBoG9EzIkjd7og;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$iSAxTeLNCo55diBoG9EzIkjd7og;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 502
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LIMIT_VARIATIONS_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VY1aY2CiimZy_rSokCNuKJnLMmw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VY1aY2CiimZy_rSokCNuKJnLMmw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 504
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ADJUST_POINTS_SMS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XoaGK6nQG_sT9Tnhz2iai7dXNko;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XoaGK6nQG_sT9Tnhz2iai7dXNko;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 506
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ALPHANUMERIC_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$b7CblXOFcqkwv-S3wHpK4XLeNgY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$b7CblXOFcqkwv-S3wHpK4XLeNgY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 508
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CAN_ADJUST_POINTS_NEGATIVE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_vGJ0T5J85idIokUOjxN8UOUbLw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_vGJ0T5J85idIokUOjxN8UOUbLw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 510
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CASH_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$X2WkCF2A1ZQoCPmgcXZcBpAvX-Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$X2WkCF2A1ZQoCPmgcXZcBpAvX-Y;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 512
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CHECKOUT_X2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IIxTe40q12M26UIZW79a4ZUAGNM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IIxTe40q12M26UIZW79a4ZUAGNM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 514
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$woZkPv1h3UWsVlTjfbgG9SZ_f1g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$woZkPv1h3UWsVlTjfbgG9SZ_f1g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 516
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ENROLLMENT_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$T16KuvBcbIzUIc4lu-uvZUkjKBY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$T16KuvBcbIzUIc4lu-uvZUkjKBY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 518
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_EXPIRE_POINTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$C88fsxCuIP-7NHT5VhEx2_riceE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$C88fsxCuIP-7NHT5VhEx2_riceE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 520
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_HANDLE_RETURN_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$U4g2hwLKJ4L7afEIoVL0jMsfO74;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$U4g2hwLKJ4L7afEIoVL0jMsfO74;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 522
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ITEM_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FNXPh8ymQJHuWte0mRhAHofeEAI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FNXPh8ymQJHuWte0mRhAHofeEAI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 524
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_TUTORIALS_ENROLLMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UV0d60d62fUz2t9MK4Qhqnk7vJU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$UV0d60d62fUz2t9MK4Qhqnk7vJU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 526
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OfgCPhAW7RKncn0UjYaYMMoozGI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OfgCPhAW7RKncn0UjYaYMMoozGI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 528
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$A6qVCUMPdYsZIJJmBV0W6nefDdE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$A6qVCUMPdYsZIJJmBV0W6nefDdE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 530
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_POST_TRANSACTION_DARK_THEME:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Dh9sjxIKxlgnemjbuNpy7UDlqaw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Dh9sjxIKxlgnemjbuNpy7UDlqaw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 532
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IeSSCQpU_QVGQIeDmBe6Tm8VV2w;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$IeSSCQpU_QVGQIeDmBe6Tm8VV2w;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 534
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EVCw7jXrdsM6Jkl0mQRX8maq3jk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EVCw7jXrdsM6Jkl0mQRX8maq3jk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 536
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES_BETA:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$oAbCvID9KX2dtctcxujAJeOyrIU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$oAbCvID9KX2dtctcxujAJeOyrIU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 538
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Srbc6rrl69gKuzHY5ZOSOM1ktlc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Srbc6rrl69gKuzHY5ZOSOM1ktlc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 540
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->O1_DEPRECATION_WARNING_JP:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ToetUv2Vc4LulKpYBvYdNAMf7Zo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ToetUv2Vc4LulKpYBvYdNAMf7Zo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 542
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ASK_INTENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$yMrTj-xzFJ7Rx8MFC5jZjttuIDA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$yMrTj-xzFJ7Rx8MFC5jZjttuIDA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 544
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_BANK:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VfpMAhhl6cTfROcvUH-gR905XqY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VfpMAhhl6cTfROcvUH-gR905XqY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 546
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_CHOOSE_DEFAULT_DEPOSIT_METHOD:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$D-33Dv8uX9AYJ5u5o5lpxVXLVwQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$D-33Dv8uX9AYJ5u5o5lpxVXLVwQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 548
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_FREE_READER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4qN5tGLQFBzHZ4AYJGyfKxzMlXc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4qN5tGLQFBzHZ4AYJGyfKxzMlXc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 550
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_QUOTA:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$l3RCM4FOUCuUX47kMAgov1YJPEM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$l3RCM4FOUCuUX47kMAgov1YJPEM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 552
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$RbReg3jYbiX_wbUfveN3DsfSB9w;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$RbReg3jYbiX_wbUfveN3DsfSB9w;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 554
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SERVER_DRIVEN_FLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nRUmyT7QGTcQVLo8AuCrfI5fk4s;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nRUmyT7QGTcQVLo8AuCrfI5fk4s;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 556
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SUPPRESS_AUTO_FPT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NwRM0Qum8E_T_d_v36LkaGgEgbE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NwRM0Qum8E_T_d_v36LkaGgEgbE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 558
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_X2_VERTICAL_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$73VD6qnM_RRqQbtnLsqMkY6eWBY;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$73VD6qnM_RRqQbtnLsqMkY6eWBY;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 560
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_BULK_DELETE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$aTDXyudN2icsAZsqW5iJYSxthXs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$aTDXyudN2icsAZsqW5iJYSxthXs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 562
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_DIP_TAP_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5c4fd9SdSGwhrCcesiuLliazYXQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5c4fd9SdSGwhrCcesiuLliazYXQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 564
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$i3sab5u7jUC8WB3zTJdWEkSCUJE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$i3sab5u7jUC8WB3zTJdWEkSCUJE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 566
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MERGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$7FFj-GDFFxG52DnK1udTvIRK9-c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$7FFj-GDFFxG52DnK1udTvIRK9-c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 568
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AEmrQMNtyJfAo6aLnoSz0YWkGJo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AEmrQMNtyJfAo6aLnoSz0YWkGJo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 570
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Og48Dt7O_-VJiF0q7izFAy9o1s8;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Og48Dt7O_-VJiF0q7izFAy9o1s8;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 572
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SHOW_BETA_OPT_IN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4QRBb0YddTQA-voCjqpr9eMyezw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4QRBb0YddTQA-voCjqpr9eMyezw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 574
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_SWIPE_TO_CREATE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dotukdLv8nbNP_net1JqRlYCy9w;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dotukdLv8nbNP_net1JqRlYCy9w;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 576
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TRANSFER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FyRAHvlMz7dr4PIRJ9tNZE675tc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FyRAHvlMz7dr4PIRJ9tNZE675tc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 578
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$U1yxjLc0MTZE76DoWf_lG_ZwCRU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$U1yxjLc0MTZE76DoWf_lG_ZwCRU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 580
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2_NAME_AND_NOTES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$q1bzC5Xpl4dGBck8eijBZFXPzIc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$q1bzC5Xpl4dGBck8eijBZFXPzIc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 582
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2_SEARCH_SORT_FILTER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$QWcjmMyJFBB7Yk4sbbLQG7BLv9E;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$QWcjmMyJFBB7Yk4sbbLQG7BLv9E;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 584
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$CG0Iv66aiVAHRl1HT8KGdqg4XLI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$CG0Iv66aiVAHRl1HT8KGdqg4XLI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 586
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_ALLOW_BAZAAR_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cPV9DVjSslKfemy8VsE9zedPB2g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cPV9DVjSslKfemy8VsE9zedPB2g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 588
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$h7tI2Sl5BSRWjFyKm6nCx-oUXmM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$h7tI2Sl5BSRWjFyKm6nCx-oUXmM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 590
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CAN_SEARCH_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZMnwIlzcqLlIPTL1Xf8p4bjiRoQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZMnwIlzcqLlIPTL1Xf8p4bjiRoQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 592
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CHECK_EMPLOYEE_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$piBcIzzmVnfhHsRB6_wV2_GWWTs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$piBcIzzmVnfhHsRB6_wV2_GWWTs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 594
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SHOW_QUICK_ACTIONS_SETTING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NGkyS9EU7ytS_4ejPDeMWYRulc8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NGkyS9EU7ytS_4ejPDeMWYRulc8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 596
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_ECOM_DELIVERY_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ec0oWE7llfWIdP2WJxy3d-vwRXE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ec0oWE7llfWIdP2WJxy3d-vwRXE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 598
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_UPCOMING_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lw0PF5nK12FPoRabxGua9ElXfSs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lw0PF5nK12FPoRabxGua9ElXfSs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 600
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$V53teffQ6dLYiN5wP-hv69SzlqI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$V53teffQ6dLYiN5wP-hv69SzlqI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 602
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$PsC-MOOJoxNnawZDjskVVw7WCpw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$PsC-MOOJoxNnawZDjskVVw7WCpw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 604
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$A8aZbxCFZtH-4N380-R1-p1YWCU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$A8aZbxCFZtH-4N380-R1-p1YWCU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 606
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dQvSrtzAjArQ0Ck5EA1ADhWfYd4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$dQvSrtzAjArQ0Ck5EA1ADhWfYd4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 608
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PAPER_SIGNATURE_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jHd58szbw-dErz2tybUiy0lj35Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jHd58szbw-dErz2tybUiy0lj35Y;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 610
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wkwoKZ9fOf2VlpxflcASXjIkmmg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wkwoKZ9fOf2VlpxflcASXjIkmmg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 612
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PERFORM_CATALOG_SYNC_AFTER_TRANSACTION_ON_X2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2krD8HzQAXjzSy4sNMCGM4MwcEs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$2krD8HzQAXjzSy4sNMCGM4MwcEs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 614
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PERSISTENT_BUNDLE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9bYtpbBRZDnu-NOEVRdTofv8c_g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$9bYtpbBRZDnu-NOEVRdTofv8c_g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 616
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4ptK_xPwFezcDxZCHi-st73WrI8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4ptK_xPwFezcDxZCHi-st73WrI8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 618
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_STAFF_HOURS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k5PqnkL3qnbdJu2M9N9p5VwkMRc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k5PqnkL3qnbdJu2M9N9p5VwkMRc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 620
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_APPOINTMENT_LOCATION_EDIT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$oiUiTl212hGHzV9izsDPX5sEzLM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$oiUiTl212hGHzV9izsDPX5sEzLM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 622
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DfoTREoQxDxU4UqeGKXutZBwy9Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DfoTREoQxDxU4UqeGKXutZBwy9Y;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 624
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS_API:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-2YrcO9PaoIw0iyKsXYb17a84Ek;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-2YrcO9PaoIw0iyKsXYb17a84Ek;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 626
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_CONFIRMATIONS_SQUARE_ASSISTANT_EDU_MODAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$q-ImRyS-saRj_DvaYRy7M8sGnRw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$q-ImRyS-saRj_DvaYRy7M8sGnRw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 628
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SQUARE_ASSISTANT_CONVERSATIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lv4IORDwacaKRyabAoLxSzg_6nQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lv4IORDwacaKRyabAoLxSzg_6nQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 630
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$vGmpPTsGnosrcA4347vbvt9VEc8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$vGmpPTsGnosrcA4347vbvt9VEc8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 632
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_LIST_OF_EVENTS_VIEW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$R03_3am-uyjAXMdk1h0t3BZx3js;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$R03_3am-uyjAXMdk1h0t3BZx3js;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 634
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_ANDROID_EDIT_RECURRING_APPOINTMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5g-F-gK3lGEkURTm9DxAaxnjclg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5g-F-gK3lGEkURTm9DxAaxnjclg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 636
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_ANDROID_RESOURCE_BOOKING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Gz24aJob9oDzozRTPyISStpd7Ms;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Gz24aJob9oDzozRTPyISStpd7Ms;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 638
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PRINT_ITEMIZED_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Qzo9Ab5hoqF6QyiQZUwX3Vsj8LI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Qzo9Ab5hoqF6QyiQZUwX3Vsj8LI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 640
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PRINT_MERCHANT_LOGO_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NJDEd9d5ABw1xoo35ZL8iz9g75E;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NJDEd9d5ABw1xoo35ZL8iz9g75E;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 642
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hivhIQpeA1TCOj2u4uk5S5OZpmM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$hivhIQpeA1TCOj2u4uk5S5OZpmM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 643
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DITHERING_T2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zJlBDFhazl89YTiRks-jPyPbv8s;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zJlBDFhazl89YTiRks-jPyPbv8s;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 646
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_T2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$uvWoNpakraiq5p-PyhVGmRhk-TE;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$uvWoNpakraiq5p-PyhVGmRhk-TE;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 650
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xxlZpmneuSzlCeduNTB2n24awX8;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xxlZpmneuSzlCeduNTB2n24awX8;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 652
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PREDEFINED_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$56p4egVMwtwzBUVUxOIwOCqOuGM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$56p4egVMwtwzBUVUxOIwOCqOuGM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 654
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$x10c6829HlJ2uTh3IIAEvH5yQvI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$x10c6829HlJ2uTh3IIAEvH5yQvI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 655
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->R12_EARLY_K400_POWERUP:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_LGP6B6oMBpyPrQnNvxDqY-vq-A;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_LGP6B6oMBpyPrQnNvxDqY-vq-A;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 657
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_209030_QUICKCHIP:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WrLVge4Po-3jl5AFE1nnFcIeTzw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WrLVge4Po-3jl5AFE1nnFcIeTzw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 659
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_ACCOUNT_TYPE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6qy9pS-K-34mBT4_qm3i2XhedyM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6qy9pS-K-34mBT4_qm3i2XhedyM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 661
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_COMMON_DEBIT_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5GerBTxbnlyBrve2qImmpcqPBiM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5GerBTxbnlyBrve2qImmpcqPBiM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 663
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_FELICA_NOTIFICATION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$w_cDRwpVJHZQ6ew2RCclrV2Yhoc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$w_cDRwpVJHZQ6ew2RCclrV2Yhoc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 665
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_PINBLOCK_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$v3Z2AHTYU9vthRfWfsI-s-wZF3c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$v3Z2AHTYU9vthRfWfsI-s-wZF3c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 667
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SPOC_PRNG_SEED:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6UeKpkQcJNVMF0Pecb6M5NaPOwU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6UeKpkQcJNVMF0Pecb6M5NaPOwU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 669
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6RNy14TUqWsgywEhu4VQro32XcI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6RNy14TUqWsgywEhu4VQro32XcI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 671
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->READER_ORDER_HISTORY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-B0j9xQhQRTXA0BEORcGvol7_xM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-B0j9xQhQRTXA0BEORcGvol7_xM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 673
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mdpTW9xSWkpUXFr44EPPXE55sKc;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$mdpTW9xSWkpUXFr44EPPXE55sKc;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 675
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_PRINT_DISPOSITION:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xq7bqRyQ8qtS0jJNY-6hICNfj1s;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xq7bqRyQ8qtS0jJNY-6hICNfj1s;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 677
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wSo2HgYHC_CPoIoeusxE8Nn4MDE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wSo2HgYHC_CPoIoeusxE8Nn4MDE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 679
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->RETAIN_PRINTER_CONNECTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$F30uijezO6cc_Vrh8FauSqptAI0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$F30uijezO6cc_Vrh8FauSqptAI0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 681
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PORTRAIT_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4gFX04f9iWWaL4tCCinc07lp1Fg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4gFX04f9iWWaL4tCCinc07lp1Fg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 683
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PREFER_SQLITE_TASKS_QUEUE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$D80sX-9TARMusRHp0bVgax56c9g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$D80sX-9TARMusRHp0bVgax56c9g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 685
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PRINT_TAX_PERCENTAGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nDHFrfPL8ESqXwZrKm2qotViJa8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nDHFrfPL8ESqXwZrKm2qotViJa8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 687
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nQWd2KcgKmd-oBDeHOx49h7U9OI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nQWd2KcgKmd-oBDeHOx49h7U9OI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 689
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REDUCE_PRINTING_WASTE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nmFsKlKLhVjMAbNJl8pJvnc4Qrw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$nmFsKlKLhVjMAbNJl8pJvnc4Qrw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 691
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LDq34gUkVD7esJLdfIrs38MwfBQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LDq34gUkVD7esJLdfIrs38MwfBQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 693
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$26ssSuZnVxQH_tjF342LQ6KxJg0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$26ssSuZnVxQH_tjF342LQ6KxJg0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 695
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Ng4IQlpvEbe8r4XpKjyNyRt5YF0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Ng4IQlpvEbe8r4XpKjyNyRt5YF0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 697
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REMOTELY_CLOSE_OPEN_CASH_DRAWERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KkZiFELqCnSaye84oY18IA4t8fY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KkZiFELqCnSaye84oY18IA4t8fY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 699
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$__6UkjHT1F0_kIw7lRvziRdKXG4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$__6UkjHT1F0_kIw7lRvziRdKXG4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 701
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP_EMAIL_CONFIRM:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$In4Ii9CkpCRkW-tOHWM2OWmly7E;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$In4Ii9CkpCRkW-tOHWM2OWmly7E;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 703
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$K_n8V3LWHraQB3e6iBYinbKp8-0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$K_n8V3LWHraQB3e6iBYinbKp8-0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 705
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_SEE_MEASUREMENT_UNIT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gwnaeP1bG4oTAf1cyMu880oOSbI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gwnaeP1bG4oTAf1cyMu880oOSbI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 707
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1MdmoptuKdHqN4iZCf5k4y84Cjk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1MdmoptuKdHqN4iZCf5k4y84Cjk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 709
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_BADGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$u7B_bb4n3f5kPZqOJS4251xFtao;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$u7B_bb4n3f5kPZqOJS4251xFtao;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 711
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ndrFy0N8UHL6UUOv2z11TtTUF1M;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ndrFy0N8UHL6UUOv2z11TtTUF1M;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 713
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REQUIRES_TRACK_2_IF_NOT_AMEX:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$x9DZlv4nMV9GTQ4pE4J22EaGtvM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$x9DZlv4nMV9GTQ4pE4J22EaGtvM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 715
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$kuWLOaOLE2RujvDZfcs7lgUMfWg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$kuWLOaOLE2RujvDZfcs7lgUMfWg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 717
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NKKwAqonV6pkFwUOvHadXIhCaXE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NKKwAqonV6pkFwUOvHadXIhCaXE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 719
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->RESTART_APP_AFTER_CRASH:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$RTpiZXrX7oVRqjC8G_wZO_3fKAs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$RTpiZXrX7oVRqjC8G_wZO_3fKAs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 721
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->RESILIENT_BUS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$MS3fsbMUdvIgwiUOtqheItKAQuw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$MS3fsbMUdvIgwiUOtqheItKAQuw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 722
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REWARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Z8om0fh7oLGOPwSJJRf-_FguicM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Z8om0fh7oLGOPwSJJRf-_FguicM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 723
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SALES_SUMMARY_CHARTS:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LFjYe7nd-Je_nm-iQsHSJ5N-0jw;

    invoke-direct {p2, p0}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LFjYe7nd-Je_nm-iQsHSJ5N-0jw;-><init>(Lcom/squareup/settings/server/FeatureFlagFeatures;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 727
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ENbSzSiL9SrNcf0FdN9suLxS09s;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ENbSzSiL9SrNcf0FdN9suLxS09s;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 729
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2_FEATURE_CAROUSEL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XLCI0y2n4L1QQ-wvEnUlWgEdDhI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XLCI0y2n4L1QQ-wvEnUlWgEdDhI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 731
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SEPARATED_PRINTOUTS:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LEdhxP96xPzSpX84JS8IlVLeE_k;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$LEdhxP96xPzSpX84JS8IlVLeE_k;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 733
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SANITIZE_EVENTSTREAM_COORDINATES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wC8tXZfrdLmI80edR6ct8QNWay4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wC8tXZfrdLmI80edR6ct8QNWay4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 735
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SETUP_GUIDE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$muvIP6GuxA4NQ1JBvJztobzaH2s;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$muvIP6GuxA4NQ1JBvJztobzaH2s;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 737
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ACCIDENTAL_CASH_MODAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$M9-hiQuJ0uu_1jnDss7XPgIdC1M;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$M9-hiQuJ0uu_1jnDss7XPgIdC1M;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 739
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_BUY_BUTTON:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zO5eBT0-ZEsjy64JS8AxaLBcd4k;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zO5eBT0-ZEsjy64JS8AxaLBcd4k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 741
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_CHECKOUT_LINKS_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Lnx2TqBuFYR4kLhE_e_vF_HHu8g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Lnx2TqBuFYR4kLhE_e_vF_HHu8g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 743
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_CP_PRICING_CHANGE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$u8OiCu6p63yEk_8-ok6ENaB-1RU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$u8OiCu6p63yEk_8-ok6ENaB-1RU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 745
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_FEE_BREAKDOWN_TABLE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$3Affz-m7A6A41MvPXWbb750CXlU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$3Affz-m7A6A41MvPXWbb750CXlU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 747
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_INCLUSIVE_TAXES_IN_CART:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$R4XBQrxvRcRyZ71JFDEgaGIca80;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$R4XBQrxvRcRyZ71JFDEgaGIca80;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 749
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EcUHLSToewB4fHB4GUoZa1Ej8MU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$EcUHLSToewB4fHB4GUoZa1Ej8MU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 751
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_LOYALTY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xQ7eSyj00kjvFjeL2XyevezyplM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xQ7eSyj00kjvFjeL2XyevezyplM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 753
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_LOYALTY_VALUE_METRICS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_d3fMv6XMNvd-1-f1QneyikFr9Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_d3fMv6XMNvd-1-f1QneyikFr9Y;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 755
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_NOHO_RECEIPT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$urPtKb9MdjWy1u8NgpWQKF6XhSs;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$urPtKb9MdjWy1u8NgpWQKF6XhSs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 757
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_PAY_ONLINE_TENDER_OPTION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Exsce0qRrx9BpUmMgcdT0KNa9PM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Exsce0qRrx9BpUmMgcdT0KNa9PM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 759
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_mlIuuSll_hOSOBTWm81dUh3ok4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_mlIuuSll_hOSOBTWm81dUh3ok4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 761
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_SPOC_VERSION_NUMBER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZgjWcCS-cgQLe-mXL3STvC-DlNE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZgjWcCS-cgQLe-mXL3STvC-DlNE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 763
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKEND_BALANCE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$bwZJ5zCvIUXqY_FGJ39M11lNZSM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$bwZJ5zCvIUXqY_FGJ39M11lNZSM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 765
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SHOW_WEEKVIEW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VriwlQO448C0CoPHo9L0Lz3HD0c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VriwlQO448C0CoPHo9L0Lz3HD0c;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 767
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SKIP_MODIFIER_DETAIL_SCREEN:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NyHfBvbuzR7iLLZymQwyR1TqWCw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NyHfBvbuzR7iLLZymQwyR1TqWCw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 769
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$R49pVfnIT6AU6_wzldM0apUTr9o;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$R49pVfnIT6AU6_wzldM0apUTr9o;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 771
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$bSIQQ_PrMYYQh_9V19ZZwcdi0-4;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$bSIQQ_PrMYYQh_9V19ZZwcdi0-4;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 775
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-eemhulKD786KPXrspZAWAjD7wc;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-eemhulKD786KPXrspZAWAjD7wc;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 779
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cqLqfxDW0aXPr5Qc4Dx274LjJzQ;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cqLqfxDW0aXPr5Qc4Dx274LjJzQ;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 783
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$JSBBQlD7QvLEu07CPJUd4an7Uoo;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$JSBBQlD7QvLEu07CPJUd4an7Uoo;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 787
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SMS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wS_ka3EEqDs3gyok6t3v2IQMGlY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wS_ka3EEqDs3gyok6t3v2IQMGlY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 788
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_CRQ:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZU7df0OBMirsXQHlsh-usy-j4Zg;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZU7df0OBMirsXQHlsh-usy-j4Zg;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 791
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$c0442dO7M-NEvCFUMB-KApFMOK8;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$c0442dO7M-NEvCFUMB-KApFMOK8;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 794
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SPE_TMS_LOGIN_CHECK:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TQiLhzgqL3Sy2v88fCRu7r4HX70;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TQiLhzgqL3Sy2v88fCRu7r4HX70;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 798
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SPLIT_TICKET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Hfeq5gNQzrpWWOkEuIJtmZBLjQI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Hfeq5gNQzrpWWOkEuIJtmZBLjQI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 800
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SPM_SQUID:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jYtOfKarmXmSQUm5r1IioIa0nmM;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jYtOfKarmXmSQUm5r1IioIa0nmM;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 802
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SPM_SQUID_LOG_TO_ES:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$oCJ4LNQi7q8Awvkp7BGjbHFXtcs;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$oCJ4LNQi7q8Awvkp7BGjbHFXtcs;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 804
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SQUID_PTS_COMPLIANCE:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fdkxATnpsXyKzZMkQErHuoBRf8M;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$fdkxATnpsXyKzZMkQErHuoBRf8M;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 806
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->STATUS_BAR_FULLSCREEN_T2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Ck5sCUc5Q5BgxcmHo-ZpT5EEW4o;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Ck5sCUc5Q5BgxcmHo-ZpT5EEW4o;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 808
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->JUMBOTRON_SERVICE_KEY_T2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qTAT2EKm1H06RFd5VUA90QKgYwg;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qTAT2EKm1H06RFd5VUA90QKgYwg;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 810
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qNzhGW5jcDpsyE3ZXvI2tBNNIpQ;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qNzhGW5jcDpsyE3ZXvI2tBNNIpQ;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 813
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TIME_TRACKING_DEVICE_LEVEL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5LypnbtbcucMEm-q4SQxfCZIRDM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5LypnbtbcucMEm-q4SQxfCZIRDM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 815
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_TEAM_MEMBER_NOTES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0b_xBH2xNi1C6wrTV0rYpxdCkMo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0b_xBH2xNi1C6wrTV0rYpxdCkMo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 817
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_RECEIPT_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VyTzIAO2uDO3Y-_j_Sry0Vx0J5I;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VyTzIAO2uDO3Y-_j_Sry0Vx0J5I;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 819
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TERMINAL_API_SPM:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$akK9GVtwPv3iGcUa7oeMGzGZmKQ;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$akK9GVtwPv3iGcUa7oeMGzGZmKQ;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 821
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TMN_RECORD_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$E_yCqn1zU4NQ07DpWeZXQ6XNf80;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$E_yCqn1zU4NQ07DpWeZXQ6XNf80;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 823
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->TMN_VERBOSE_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VyEqDhj1WdmvXivfrb6ddOLstDA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$VyEqDhj1WdmvXivfrb6ddOLstDA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 825
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_API_URL_LIST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-Gas6YMg9BAWQUjvuITHSywR5kI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-Gas6YMg9BAWQUjvuITHSywR5kI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 827
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_AUTH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pkqwMo8NXSbmg_7dAXUWoisZBvI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$pkqwMo8NXSbmg_7dAXUWoisZBvI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 829
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_IN_APP_TIMECARDS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cEdIV3H_pkcNyOJ_kHS1hGfKuhU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cEdIV3H_pkcNyOJ_kHS1hGfKuhU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 831
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wDg322iKYhKtwVkQlXszuk3ClJc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wDg322iKYhKtwVkQlXszuk3ClJc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 833
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_ALTERNATE_SALES_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YjbU2-FcXrt_D5JP6d5RujXqqUg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YjbU2-FcXrt_D5JP6d5RujXqqUg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 835
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_BREAK_TRACKING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TGX-Tz7ubghrLUxEP4mJkhCWb2Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TGX-Tz7ubghrLUxEP4mJkhCWb2Y;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 837
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p7N1KaJkQW_36xxzEFdmK8KrPkk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$p7N1KaJkQW_36xxzEFdmK8KrPkk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 839
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE_ON_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WwCrAaHKNZx3T183rlCHCoxdJoc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WwCrAaHKNZx3T183rlCHCoxdJoc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 841
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_OUT_REASON:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$b1QJBPydnPGwzJLMh9c3ar2htHc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$b1QJBPydnPGwzJLMh9c3ar2htHc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 843
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$sH5n_45Oiq5XmduxnJE3dvlzdCg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$sH5n_45Oiq5XmduxnJE3dvlzdCg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 845
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KK4P84bnHcfMPy3cGhSuQanp1pU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KK4P84bnHcfMPy3cGhSuQanp1pU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 847
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SELECT_METHOD_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$aKrQqYpDRDqkaJjIQYLvTiHjh2s;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$aKrQqYpDRDqkaJjIQYLvTiHjh2s;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 849
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5CJP9CLNIkqTvpnLvEtMhySpZgA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5CJP9CLNIkqTvpnLvEtMhySpZgA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 851
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_CARD_LINKING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4GX9lPeTmEr_NxjP8W-bLwgwncE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4GX9lPeTmEr_NxjP8W-bLwgwncE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 853
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$E4QzWyDrdyt_bWlOAbovqXMJA5Q;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$E4QzWyDrdyt_bWlOAbovqXMJA5Q;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 855
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_LOCAL_PAPER_SIGNATURE_SETTING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NK9kmfer9bBFWI0rNqDlrJjaODw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NK9kmfer9bBFWI0rNqDlrJjaODw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 857
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_GIFT_CARDS_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_9LYSFu8PDOQ46GYWepvwpbmpvg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_9LYSFu8PDOQ46GYWepvwpbmpvg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 859
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEM_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8wzshm-OBJFs6ldAJClBhtpYL8E;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8wzshm-OBJFs6ldAJClBhtpYL8E;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 861
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k5ay_1joqbRRfFKsF7EYFM15nJU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$k5ay_1joqbRRfFKsF7EYFM15nJU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 863
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_V2_BLE_STATE_MACHINE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lNo-np7YohAHkLU9UGgXd8XRTi8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$lNo-np7YohAHkLU9UGgXd8XRTi8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 865
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_V2_CARDREADERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AEz-z9ymiK0DO-KKMsQf4mbssDQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$AEz-z9ymiK0DO-KKMsQf4mbssDQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 867
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$iaO8GS4Gpq4NcxIkncxmv65tayU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$iaO8GS4Gpq4NcxIkncxmv65tayU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 869
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0bTvBvU004s5eg69qvCrE_ybFzc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0bTvBvU004s5eg69qvCrE_ybFzc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 871
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_TAX_BASIS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wE3FNVCV2AJw63qmk-wMxbqYUeI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wE3FNVCV2AJw63qmk-wMxbqYUeI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 873
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$QDe_KrT4WBiDV7aULHb23d9JlRA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$QDe_KrT4WBiDV7aULHb23d9JlRA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 875
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-DywLN8WOctfFYVNwkrIeo-tAuc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$-DywLN8WOctfFYVNwkrIeo-tAuc;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 876
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$y6f7TNwZ8t_RydQ2hQZwomJRiSU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$y6f7TNwZ8t_RydQ2hQZwomJRiSU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 877
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_WOz48LArNLiqu-AUrvx_x2iIWE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_WOz48LArNLiqu-AUrvx_x2iIWE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 879
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SAFETYNET:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wVmOw_J9ODjl-5aquMImV4x3-48;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wVmOw_J9ODjl-5aquMImV4x3-48;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 880
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SAME_DAY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Y01_xAb9UN-M2ymAFgnUdUN9Rbw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Y01_xAb9UN-M2ymAFgnUdUN9Rbw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 882
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wbIT1bH2l1loAnmTradUsYKxDmg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$wbIT1bH2l1loAnmTradUsYKxDmg;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 883
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DiidJGAD7eDithra6hi5q9XGLY4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$DiidJGAD7eDithra6hi5q9XGLY4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 885
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER_WORKFLOW:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$T6rEJTTBMNB0LUyxqO67_YwY2hE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$T6rEJTTBMNB0LUyxqO67_YwY2hE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 887
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$edEAaEngiYbfV0ZFnlObxtM3svw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$edEAaEngiYbfV0ZFnlObxtM3svw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 889
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5FqnasARVn4MPk_rlBc0ic30A88;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$5FqnasARVn4MPk_rlBc0ic30A88;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 891
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_BUYER_DISPLAY_SETTINGS_V2_X2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WmZF1H0cxv33M9kwKAmdR8V-udM;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$WmZF1H0cxv33M9kwKAmdR8V-udM;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 894
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_V3_CATALOG:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Gk6-lLyAzXPT3tvYFjmOIGdio68;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$Gk6-lLyAzXPT3tvYFjmOIGdio68;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 895
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_X2_PAYMENT_WORKFLOWS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XWpsAEW-fpB2dQ7Wvszqm89gVm8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$XWpsAEW-fpB2dQ7Wvszqm89gVm8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 897
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->VALIDATE_SESSION_ON_START:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$J10clJyVQKZCFdz9QUEs7zo2Q5k;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$J10clJyVQKZCFdz9QUEs7zo2Q5k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 899
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->VOID_COMP:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TcU84WfNqljUWyv6WoHqO_Jf0bU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$TcU84WfNqljUWyv6WoHqO_Jf0bU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 900
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->WAIT_FOR_BRAN_X2:Lcom/squareup/settings/server/Features$Feature;

    new-instance p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xEqdOBM5nB9VAo_Tr-mvGcfvmIY;

    invoke-direct {p2, p5}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$xEqdOBM5nB9VAo_Tr-mvGcfvmIY;-><init>(Lcom/squareup/settings/server/FeatureServiceVertical;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 902
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ZERO_ARQC_TEST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$42cfXsBuTdqQ3VZeMsWru2GgzOw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$42cfXsBuTdqQ3VZeMsWru2GgzOw;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 904
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ZERO_TENDER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZFdC45UlymyypO8mYVTdMyDYIUo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$ZFdC45UlymyypO8mYVTdMyDYIUo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 905
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->S2_HODOR:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$goYpGiIxsvjbtRxwvUd1odNXD9g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$goYpGiIxsvjbtRxwvUd1odNXD9g;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 906
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_X2_WIFI_EVENTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NsUuLP7QDRUwwntbPcJc0adPttE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NsUuLP7QDRUwwntbPcJc0adPttE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 907
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->VERIFY_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$rPxBkVRcgQiQU1FVtc8YZ9CqKTo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$rPxBkVRcgQiQU1FVtc8YZ9CqKTo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 908
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_ACCESSIBILITY_SCRUBBER:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KS_S0tIJJWlRnLtts3TyjxDFYQU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KS_S0tIJJWlRnLtts3TyjxDFYQU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 910
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CLOCK_SKEW_LOCKOUT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$eWzn0XLUHmYGqgKU7FP7zNuSlKU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$eWzn0XLUHmYGqgKU7FP7zNuSlKU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 912
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8bBGhHnywkIsDdTKzVT5A2CFdBI;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$8bBGhHnywkIsDdTKzVT5A2CFdBI;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 914
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zTdDlnczT2wfrAXJX0paqpldutA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$zTdDlnczT2wfrAXJX0paqpldutA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 916
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER_STREAM:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$V9yfom1UvJFRhmiIF0ocT1z9PHM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$V9yfom1UvJFRhmiIF0ocT1z9PHM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 918
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gzGooEK2xjDHtrhLkBrg25ixAyY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$gzGooEK2xjDHtrhLkBrg25ixAyY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 920
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qc3gbSb3oP0GV5aIqGzx21yEL8o;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$qc3gbSb3oP0GV5aIqGzx21yEL8o;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 922
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMIZED_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KD5b9yykVFj_kWGbAbq9qrGVHe0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$KD5b9yykVFj_kWGbAbq9qrGVHe0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 924
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_POS_INTENT_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$PsXrPu3G8L943Qo1UxfRty_VRvA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$PsXrPu3G8L943Qo1UxfRty_VRvA;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 926
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$e4uEuBswphZc0TAv-gbuCSBJ0xU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$e4uEuBswphZc0TAv-gbuCSBJ0xU;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 928
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_EPSON_PRINTERS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NzhjwkHGQBXV4OOM01bMeyYkUiQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$NzhjwkHGQBXV4OOM01bMeyYkUiQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 930
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_-spA6LyIS2fkzV3U7BR_y1rCIk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$_-spA6LyIS2fkzV3U7BR_y1rCIk;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 934
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_OT_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6rF8uYSXlwSRkxT3R7GnP5JFC6k;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$6rF8uYSXlwSRkxT3R7GnP5JFC6k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 937
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_EMPLOYEE_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jh243jjGbDd9WQ8r_CoQWsdk0zM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$jh243jjGbDd9WQ8r_CoQWsdk0zM;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 940
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$thGwKi9rFlIw9LAArHEl2aOrVl8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$thGwKi9rFlIw9LAArHEl2aOrVl8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 942
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0c7p-elvAh2wUitItn3EfOJk1pQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$0c7p-elvAh2wUitItn3EfOJk1pQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 945
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_COVER_COUNTS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$eAEKAB8a-Pr9Nafx74nA9Kb5Zh4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$eAEKAB8a-Pr9Nafx74nA9Kb5Zh4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 948
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVENTORY_AVAILABILITY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$3EaHBbHHv48QYCPT9ut41bbx3BY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$3EaHBbHHv48QYCPT9ut41bbx3BY;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 950
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_CONFIGURED_BUSINESS_HOURS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$z0CULxltc8jMLO35IxoJS5vB9P4;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$z0CULxltc8jMLO35IxoJS5vB9P4;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 952
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SALES_LIMITS:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4yq3KeeATvVYm7W8of3BTkhuPsQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$4yq3KeeATvVYm7W8of3BTkhuPsQ;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 954
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_COMP_VOID_ASSIGN_MOVE:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cKB3do1Dl9AiMKph_hj1KBvLVjE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$cKB3do1Dl9AiMKph_hj1KBvLVjE;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 956
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TICKET_RST:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$t5P-OFaQ5BXl5mrNWLsAbUf6Ulo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$t5P-OFaQ5BXl5mrNWLsAbUf6Ulo;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 958
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_TABLE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YyZ8B-4iVxjPuW-o6J_hxgvEwl8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$YyZ8B-4iVxjPuW-o6J_hxgvEwl8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 960
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_AUTOMATIC_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1z_xxbyjsos5r0NF1eRTsX7_Ei0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$1z_xxbyjsos5r0NF1eRTsX7_Ei0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 963
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FFVONpJIRRl4nyrr8WvBVpFulr0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$FFVONpJIRRl4nyrr8WvBVpFulr0;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    .line 965
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    sget-object p2, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$69o7dQT3X20HyjLh944dpyWjlW8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$69o7dQT3X20HyjLh944dpyWjlW8;

    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V

    return-void
.end method

.method synthetic constructor <init>(Ljavax/inject/Provider;Ldagger/Lazy;Lcom/squareup/util/Device;ZLcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/settings/server/FeatureFlagFeatures$1;)V
    .locals 0

    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/squareup/settings/server/FeatureFlagFeatures;-><init>(Ljavax/inject/Provider;Ldagger/Lazy;Lcom/squareup/util/Device;ZLcom/squareup/settings/server/FeatureServiceVertical;)V

    return-void
.end method

.method private static appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
    .locals 1

    .line 982
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$Ph0lIqu0En6dy7f5_qi8MiaRskU;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$Ph0lIqu0En6dy7f5_qi8MiaRskU;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    return-object p0
.end method

.method private static auth(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;
    .locals 1

    .line 987
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$GzbN7rFI_8A_gsB-POvqcPSfDDc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$GzbN7rFI_8A_gsB-POvqcPSfDDc;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    return-object p0
.end method

.method private static billing(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;
    .locals 1

    .line 991
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$3RiF4mWTEa37mP2WsrD2uyYYIRw;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$3RiF4mWTEa37mP2WsrD2uyYYIRw;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    return-object p0
.end method

.method private static bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
    .locals 1

    .line 1074
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$vOR7MP8eRQek0zkw48e6uzYO0GA;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$vOR7MP8eRQek0zkw48e6uzYO0GA;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    return-object p0
.end method

.method private static capital(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;
    .locals 1

    .line 999
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$sBUYaPZpvOCrs9gjEoZPMjEsBLg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$sBUYaPZpvOCrs9gjEoZPMjEsBLg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    return-object p0
.end method

.method private static cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;
    .locals 1

    .line 1132
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$Li3nWVskvdgZbhxy_7W7BwxE8G8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$Li3nWVskvdgZbhxy_7W7BwxE8G8;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/Cardreaders;

    return-object p0
.end method

.method private static catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
    .locals 1

    .line 1003
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$P43hzohQT6hwcNQyCNfLzPEcBNo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$P43hzohQT6hwcNQyCNfLzPEcBNo;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    return-object p0
.end method

.method private static crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
    .locals 1

    .line 1023
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$nV0Juacrk1gm6gHasXOKk-w8ivQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$nV0Juacrk1gm6gHasXOKk-w8ivQ;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    return-object p0
.end method

.method private static defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "TT;TB;>;:",
            "Lcom/squareup/wired/PopulatesDefaults<",
            "TT;>;B:",
            "Lcom/squareup/wire/Message$Builder<",
            "TT;TB;>;>(TT;",
            "Lkotlin/jvm/functions/Function0<",
            "TB;>;)TT;"
        }
    .end annotation

    if-eqz p0, :cond_0

    goto :goto_0

    .line 1162
    :cond_0
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message$Builder;

    invoke-virtual {p0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/wired/PopulatesDefaults;

    invoke-interface {p0}, Lcom/squareup/wired/PopulatesDefaults;->populateDefaults()Lcom/squareup/wired/PopulatesDefaults;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    :goto_0
    return-object p0
.end method

.method private static deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
    .locals 1

    .line 1011
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$sTCjAV2s_PqW35MZsXki9PATS4Q;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$sTCjAV2s_PqW35MZsXki9PATS4Q;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    return-object p0
.end method

.method private static discount(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;
    .locals 1

    .line 1100
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$RI5Gqii_AdXUPSmlntmdyMwK71c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$RI5Gqii_AdXUPSmlntmdyMwK71c;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    return-object p0
.end method

.method private static ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
    .locals 1

    .line 1015
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$ugDgF31SqXiQxu3maqNaq-8F7mk;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$ugDgF31SqXiQxu3maqNaq-8F7mk;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    return-object p0
.end method

.method private static employee(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
    .locals 1

    .line 1061
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$u_nfsbKsua6J3SgX6cQmfBe7tao;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$u_nfsbKsua6J3SgX6cQmfBe7tao;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    return-object p0
.end method

.method private static employeejobs(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;
    .locals 1

    .line 1065
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$yCF7xTYXldrlmDh9z1wNfA-W13g;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$yCF7xTYXldrlmDh9z1wNfA-W13g;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    return-object p0
.end method

.method private static giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
    .locals 1

    .line 1043
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$HYwXwCUYIVXBw6foCcFbc7Z12b0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$HYwXwCUYIVXBw6foCcFbc7Z12b0;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    return-object p0
.end method

.method private static invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
    .locals 1

    .line 1039
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$i3LfoIfv2hgZmB91F6HZcH1m-JM;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$i3LfoIfv2hgZmB91F6HZcH1m-JM;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    return-object p0
.end method

.method private isEnabled(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 2

    .line 1152
    invoke-virtual {p1}, Lcom/squareup/settings/server/Features$Feature;->deviceType()Lcom/squareup/settings/server/Features$Feature$DeviceType;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->isTablet:Z

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/Features$Feature$DeviceType;->applies(Z)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1156
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->featureFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;

    if-eqz p1, :cond_1

    .line 1157
    invoke-interface {p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;->isEnabled(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private isOwner(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Z
    .locals 1

    .line 970
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    const-string v0, "owner"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private static items(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
    .locals 1

    .line 1086
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$S0bww86p7SbEX_ZYteEcbwquaZc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$S0bww86p7SbEX_ZYteEcbwquaZc;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$1(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 88
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$10(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 105
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$100(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 292
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$101(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 294
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$102(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 296
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$103(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 298
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$104(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 300
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$105(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 302
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$106(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 304
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$107(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 306
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$108(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 307
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$109(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 309
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$11(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 107
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$110(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 311
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$111(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 313
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$112(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 315
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->discount(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->allow_stacking_coupons_of_same_discount:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$113(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 317
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_referrals:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$114(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 319
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->dismiss_tutorials:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$115(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 321
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$116(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 323
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_supports_pagination:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$117(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 325
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$118(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 327
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$119(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 329
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$12(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 109
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$120(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 331
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$121(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 333
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$122(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 335
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$123(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 337
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$124(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 339
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$125(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 341
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_rate_based_services:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$126(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 343
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->enable_services_catalog_integration:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$127(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 345
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$128(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 347
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$129(ZLcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    if-nez p0, :cond_1

    .line 349
    iget-object p0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method static synthetic lambda$new$13(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 111
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$130(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 351
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$131(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 353
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$132(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 355
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$133(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 357
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$134(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 359
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$135(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 361
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$136(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 363
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$138(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 367
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$139(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 370
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$14(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 113
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$140(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 372
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$141(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 374
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$142(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 376
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->fixed_price_service_itemization_price_override:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$143(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 378
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$144(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 380
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iget-object p0, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$145(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 382
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_ime:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$146(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 384
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->show_plastic_gift_cards:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$147(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 386
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->android_third_party_gc_support:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$148(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 388
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->sell_egift_on_pos:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$149(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 390
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->configure_egift_on_pos:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$15(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 115
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$150(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 392
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$151(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 394
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$152(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 397
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$153(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 399
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$154(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 401
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$155(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 402
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$156(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 404
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$157(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 406
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$158(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 409
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$159(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 411
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$16(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 117
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$160(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 413
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$161(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 415
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$162(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 416
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->reports(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$163(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 418
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$164(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 420
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$165(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 422
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$166(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 423
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$167(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 425
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$168(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 427
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$169(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 429
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$17(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 119
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$170(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 431
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$171(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 433
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$172(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 435
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$173(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 437
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$174(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 439
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$175(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 441
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$176(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 442
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$177(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 444
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$178(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 446
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$179(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 448
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$18(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 121
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$180(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 450
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$181(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 452
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$182(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 454
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$183(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 456
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$184(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 459
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 460
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p1

    if-nez p1, :cond_0

    .line 461
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$185(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 463
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$186(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 465
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$187(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 467
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$188(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 469
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$189(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 471
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$19(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 123
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$190(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 473
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$191(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 475
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$192(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 477
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$193(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 479
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$194(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 481
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$195(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 483
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$196(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 485
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$197(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 487
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$198(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 489
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$199(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 491
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$2(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 90
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$20(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 125
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$200(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 493
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$201(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 495
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_global_edit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$202(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 497
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->item_options_edit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$203(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 499
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$204(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 501
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->items(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$205(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 503
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->limit_variations_per_item:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$206(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 505
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$207(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 507
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$208(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 509
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$209(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 511
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$21(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 127
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$210(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 513
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$211(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 515
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$212(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 517
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$213(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 519
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$214(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 521
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$215(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 523
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$216(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 525
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$217(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 527
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$218(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 529
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$219(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 531
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$22(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 129
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$220(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 533
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->timecards(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$221(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 535
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employeejobs(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->can_use_multiple_wages:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$222(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 537
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employeejobs(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->can_use_multiple_wages_beta:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$223(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 539
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_notification_center:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$224(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 541
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$225(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 543
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ask_intent_questions:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$226(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 545
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->bank_link:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$227(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 547
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->choose_default_deposit_method:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$228(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 549
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->offer_free_reader:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$229(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 551
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$23(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 131
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->bizbank(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$230(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 553
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->referral:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$231(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 555
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->use_server_driven_flow:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$232(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 557
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->suppress_first_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$233(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 559
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->x2_vertical_selection:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$234(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 561
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$235(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 563
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$236(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 565
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$237(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 567
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$238(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 569
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$239(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 571
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2Rst()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$24(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 132
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$240(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 573
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$241(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 575
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$242(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 577
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$243(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 579
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$244(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 581
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$245(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 583
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$246(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 585
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$247(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 587
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$248(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 589
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_spos_order_hub:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$249(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 591
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$25(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 134
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2Rst()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$250(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 593
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$251(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 595
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$252(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 597
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$253(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 599
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$254(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 601
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$255(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 603
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$256(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 605
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$257(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 607
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$258(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 609
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$259(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 611
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$26(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 136
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2Rst()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$260(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 613
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$261(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 615
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$262(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 617
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_business_hours_enabled:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$263(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 619
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_staff_hours_enabled:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$264(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 621
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_appointment_location_editing_enabled:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$265(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 623
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_confirmations_enabled:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$266(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 625
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_backend:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$267(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 627
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->confirmations_and_square_assistant_educational_modal:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$268(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 629
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->use_conversations_sms:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$269(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 631
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->processing_time_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$27(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 138
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$270(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 633
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->list_of_events_view_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$271(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 635
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->android_edit_recurring_appointments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$272(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 637
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->resource_booking:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$273(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 639
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$274(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 641
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$275(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 642
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$276(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 644
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 645
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$277(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 647
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 649
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$278(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 651
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2Rst()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$279(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 653
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$28(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 140
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$280(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 654
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$281(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 656
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$282(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 658
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$283(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 660
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$284(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 662
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$285(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 664
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$286(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 666
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$287(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 668
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$288(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 670
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$289(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 672
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$29(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 2

    .line 142
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$290(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 674
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$291(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 676
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$292(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 678
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$293(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 680
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->printers(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$294(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 682
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$295(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 684
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$296(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 686
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$297(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 688
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->payments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$298(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 690
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$299(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 692
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$3(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 92
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$30(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 144
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$300(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 694
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$301(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 696
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->refund_to_gift_card_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$302(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 698
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$303(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 700
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$304(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 702
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$305(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 704
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method static synthetic lambda$new$306(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 706
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->reports(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$307(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 708
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->payments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$308(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 710
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->payments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$309(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 712
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->payments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$31(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 147
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$310(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 714
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$311(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 716
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$312(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 718
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$313(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 720
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$314(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 721
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$315(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 722
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$317(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 728
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$318(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 730
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$319(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 732
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$32(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 150
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$320(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 734
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$321(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 736
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->setup_guide:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$322(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 738
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$323(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 740
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$324(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 742
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$325(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 744
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$326(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 746
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$327(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 748
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$328(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 750
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$329(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 752
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$33(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 154
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$330(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 754
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$331(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 756
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$332(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 758
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$333(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 760
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->ecom(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$334(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 762
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->spoc(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->show_spoc_version_number:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$335(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 764
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$336(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 766
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->show_weekview_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$337(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 768
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$338(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 770
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$339(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 772
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 774
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$34(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 157
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$340(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 776
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 778
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$341(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 780
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 782
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$342(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 784
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 785
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 786
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$343(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 787
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$344(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 789
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 790
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$345(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 792
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 793
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$346(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 795
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 797
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$347(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 799
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$348(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 801
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$349(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 803
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$35(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 2

    .line 160
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$350(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 805
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$351(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 807
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$352(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 809
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$353(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 811
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 812
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$354(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 814
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employee(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$355(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 816
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->timecards(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$356(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 818
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->timecards(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$357(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 820
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$358(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 822
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$359(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 824
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$36(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 162
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$360(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 826
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$361(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 828
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$362(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 830
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$363(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 832
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$364(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 834
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->reports(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$365(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 836
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->timecards(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$366(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 838
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$367(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 840
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$368(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 842
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->giftcard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->can_use_cashout_reason:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$369(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 844
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$37(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$370(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 846
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$371(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 848
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$372(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 850
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$373(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 852
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$374(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 854
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$375(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 856
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->use_device_level_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$376(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 858
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$377(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 860
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->catalog(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->enable_item_options_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$378(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 862
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$379(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 864
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$38(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 166
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$380(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 866
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->cardreaders(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$381(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 868
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$382(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 870
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->billing(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->pricing_engine_register_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$383(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 872
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->prices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$384(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 874
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->prices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$385(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 875
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$386(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 876
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$387(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 878
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employee(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$388(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 879
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$389(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 881
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$39(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 168
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$390(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 882
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$391(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 884
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employee(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$392(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 886
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$393(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 888
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$394(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 890
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employee(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$395(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 892
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 893
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$396(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 894
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$397(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 896
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$398(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 898
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->auth(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->client_validate_session_on_start:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$399(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 899
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$4(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 94
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->retailer(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$40(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 170
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->posfeatures(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$400(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 901
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$401(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 903
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$402(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 904
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$403(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 905
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$404(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 906
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$405(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 907
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->validate_shipping_address:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$406(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 909
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$407(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 911
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$408(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 913
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$409(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 915
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$41(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 172
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->deposits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$410(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 917
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$411(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 919
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$412(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 921
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->items(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$413(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 923
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->items(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$414(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 925
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->items(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$415(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 927
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->employee(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$416(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 929
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->printers(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$417(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 931
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->printers(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$418(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 935
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_ot_defaults:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$419(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 938
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 939
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_change_employee_defaults:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$42(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 174
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$420(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 941
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_auto_grat:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$421(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 943
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_seating:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$422(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 946
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_rst_friendly_t2:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 947
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->can_see_cover_counts:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$423(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 949
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->manual_86:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$424(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 951
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_configured_business_hours:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$425(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 953
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_sales_limits:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$426(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 955
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_comp_void_assign_move_ticket:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$427(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 957
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_split_ticket:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$428(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 959
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_table_management:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$429(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 961
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->use_auto_gratuities:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$43(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 176
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$430(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 964
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->enable_felica_certification_environment:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$431(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 966
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_show_settings_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$44(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 178
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->printers(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$45(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 180
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$46(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 182
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$47(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 184
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 185
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    .line 186
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$48(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 189
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->retailer(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$49(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 191
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$5(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 96
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$50(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 193
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->marketing(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->enable_sms_pilot:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$51(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 195
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->marketing(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->enable_sms_pos_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$52(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_USE_ANDROID_ECR:Ljava/lang/Boolean;

    invoke-static {p0, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$53(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 201
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$54(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 203
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_cash_management:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$55(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 205
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$56(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 207
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$57(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 209
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$58(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 211
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_scales:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$59(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 213
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->can_use_ble_scales:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$6(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 98
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->multi_staff_view_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$60(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 215
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->capital(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->can_manage_flex_offer_in_pos_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$61(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 217
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$62(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 219
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$63(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 221
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$64(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 223
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$65(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 225
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$66(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 227
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$67(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 229
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$68(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 231
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$69(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 233
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$7(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 100
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->auth(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->client_attempt_device_id_updates:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$70(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 235
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$71(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 237
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$72(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 240
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isX2()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$73(Lcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 243
    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;->isT2()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$new$74(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 246
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$75(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 248
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$76(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 250
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$77(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 252
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method static synthetic lambda$new$78(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 254
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$79(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 256
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$8(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$80(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 258
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$81(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$82(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 261
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$83(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 263
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$84(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 264
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$85(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 265
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$86(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 267
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$87(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 268
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$88(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 269
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$89(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 270
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$9(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 103
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->appointments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->availability_override:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$90(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 272
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$91(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 274
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$92(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 276
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$93(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 278
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->crm(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$94(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 280
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$95(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$96(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 284
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$97(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 286
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$98(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 288
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$99(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 0

    .line 290
    invoke-static {p0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->invoices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method private static loyalty(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
    .locals 1

    .line 1007
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$8I4LlYiRMuyxuV5aEhyDTAeO85Q;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$8I4LlYiRMuyxuV5aEhyDTAeO85Q;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    return-object p0
.end method

.method private static marketing(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;
    .locals 1

    .line 1128
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$OTLeRQuncuc7-cBovFcoeD4ZpIg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$OTLeRQuncuc7-cBovFcoeD4ZpIg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    return-object p0
.end method

.method private static onboard(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
    .locals 1

    .line 1019
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$uRIKMgW9eN-Srelm3d1f8ZfG9Ek;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$uRIKMgW9eN-Srelm3d1f8ZfG9Ek;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    return-object p0
.end method

.method private static orderhub(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
    .locals 1

    .line 1119
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$TL81JY-OSI4dYrtwxoUz-5IlBLc;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$TL81JY-OSI4dYrtwxoUz-5IlBLc;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    return-object p0
.end method

.method private static paymentflow(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
    .locals 1

    .line 1047
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$SJchvfI_wTZRhkMmn-56HJ87yP8;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$SJchvfI_wTZRhkMmn-56HJ87yP8;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    return-object p0
.end method

.method private static payments(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
    .locals 1

    .line 1027
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$1bsPiJRYoOigG7ZjYu8tLaL9szY;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$1bsPiJRYoOigG7ZjYu8tLaL9szY;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    return-object p0
.end method

.method private static pos(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
    .locals 1

    .line 1052
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$8rnZRP3fQbX572rUBJwy7Mu5dCo;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$8rnZRP3fQbX572rUBJwy7Mu5dCo;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    return-object p0
.end method

.method private static posfeatures(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
    .locals 1

    .line 1056
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$oQVhJWzg5Mu_XXwZhOvdfCgfP5Y;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$oQVhJWzg5Mu_XXwZhOvdfCgfP5Y;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    return-object p0
.end method

.method private static prices(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
    .locals 1

    .line 995
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$mjWphB07IwqvPaXZPHpaVgIglZ0;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$mjWphB07IwqvPaXZPHpaVgIglZ0;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    return-object p0
.end method

.method private static printers(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
    .locals 1

    .line 1124
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$Vuu2kSlq_zH1s38SWipMHey-BTg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$Vuu2kSlq_zH1s38SWipMHey-BTg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    return-object p0
.end method

.method private static readerfw(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
    .locals 1

    .line 1078
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$ZIJy_VUWA1fDxEEYlQEVRVh1AlQ;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$ZIJy_VUWA1fDxEEYlQEVRVh1AlQ;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    return-object p0
.end method

.method private static regex(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .locals 1

    .line 1031
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$FaLgbYqsjdNIlrf5ZjR57R34m-c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$FaLgbYqsjdNIlrf5ZjR57R34m-c;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    return-object p0
.end method

.method private registerFeatureFlag(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/settings/server/FeatureFlagFeatures$FeatureFlag;)V
    .locals 1

    .line 974
    iget-object v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->featureFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->featureFlags:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 975
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Attempt to register a feature flag for a feature that is already registered"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static reports(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
    .locals 1

    .line 1035
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$Rxm-HEiwvYxiEiH4xUtyQOzGB1I;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$Rxm-HEiwvYxiEiH4xUtyQOzGB1I;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    return-object p0
.end method

.method private static restaurants(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
    .locals 1

    .line 1095
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$B_WB3HKWtalLeOU0eyUxxJZfVzg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$B_WB3HKWtalLeOU0eyUxxJZfVzg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    return-object p0
.end method

.method private static retailer(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
    .locals 1

    .line 1110
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$jdBNNVPhQu6jrDStlS0G93mhAcE;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$jdBNNVPhQu6jrDStlS0G93mhAcE;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    return-object p0
.end method

.method private static spoc(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
    .locals 1

    .line 1105
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$dpuaUq-h-IT2FCpshko4rPOE_Qg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$dpuaUq-h-IT2FCpshko4rPOE_Qg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    return-object p0
.end method

.method private static squaredevice(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
    .locals 1

    .line 1090
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$mFcyorOwq-krTn84rVQ9Hh4i3Ic;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$mFcyorOwq-krTn84rVQ9Hh4i3Ic;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    return-object p0
.end method

.method private static support(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
    .locals 1

    .line 1082
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$J0Ws7AVcCQ7NF252FDS-qqGjfEg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$J0Ws7AVcCQ7NF252FDS-qqGjfEg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    return-object p0
.end method

.method private static terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .locals 1

    .line 1115
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$U26gvj-4Ff44GvkNcdQjJcNoe1c;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$U26gvj-4Ff44GvkNcdQjJcNoe1c;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    return-object p0
.end method

.method private static timecards(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
    .locals 1

    .line 1070
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    sget-object v0, Lcom/squareup/settings/server/-$$Lambda$SCzwpdatbs3M2Ij3_QBPOiXEDDg;->INSTANCE:Lcom/squareup/settings/server/-$$Lambda$SCzwpdatbs3M2Ij3_QBPOiXEDDg;

    invoke-static {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->defaultIfNull(Lcom/squareup/wire/Message;Lkotlin/jvm/functions/Function0;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    return-object p0
.end method


# virtual methods
.method public enabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1140
    invoke-virtual {p0, p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features$Feature;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1144
    iget-object v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->lazyAccountStatus:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    .line 1145
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->accountStatusProvider:Ljavax/inject/Provider;

    .line 1146
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OkHWpQ064-a0ZksJI_lRlP9KNew;

    invoke-direct {v1, p0, p1}, Lcom/squareup/settings/server/-$$Lambda$FeatureFlagFeatures$OkHWpQ064-a0ZksJI_lRlP9KNew;-><init>(Lcom/squareup/settings/server/FeatureFlagFeatures;Lcom/squareup/settings/server/Features$Feature;)V

    .line 1147
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 1148
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z
    .locals 1

    .line 1136
    iget-object v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-direct {p0, p1, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->isEnabled(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$featureEnabled$432$FeatureFlagFeatures(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1147
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/FeatureFlagFeatures;->isEnabled(Lcom/squareup/settings/server/Features$Feature;Lcom/squareup/server/account/protos/AccountStatusResponse;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$new$137$FeatureFlagFeatures(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 365
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures;->isOwner(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/settings/server/FeatureFlagFeatures;->terminal(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$new$316$FeatureFlagFeatures(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    .line 725
    iget-boolean v0, p0, Lcom/squareup/settings/server/FeatureFlagFeatures;->isTablet:Z

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
