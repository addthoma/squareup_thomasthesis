.class public Lcom/squareup/settings/server/TipSettings;
.super Ljava/lang/Object;
.source "TipSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;,
        Lcom/squareup/settings/server/TipSettings$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTipSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TipSettings.kt\ncom/squareup/settings/server/TipSettings\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,171:1\n1360#2:172\n1429#2,3:173\n*E\n*S KotlinDebug\n*F\n+ 1 TipSettings.kt\ncom/squareup/settings/server/TipSettings\n*L\n54#1:172\n54#1,3:173\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0014\u0008\u0016\u0018\u0000 *2\u00020\u0001:\u0002*+BO\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000fR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0015R\u0014\u0010\u000e\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0015R\u0014\u0010\u0005\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0015R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0015R\u0014\u0010\u0006\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0015R\u0014\u0010\u0017\u001a\u00020\u00188VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001dR\u0014\u0010\u001e\u001a\u00020\u00188VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010\u001aR\u001a\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00110\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\u0013R\u0014\u0010\"\u001a\u00020\u00188VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010\u001aR\u001a\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00110\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\u0013R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/settings/server/TipSettings;",
        "",
        "isEnabled",
        "",
        "isUsingCustomPercentages",
        "isUsingCustomAmounts",
        "isUsingSeparateTippingScreen",
        "tippingCalculationPhase",
        "Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;",
        "customPercentages",
        "",
        "Lcom/squareup/util/Percentage;",
        "tipping",
        "Lcom/squareup/server/account/protos/Tipping;",
        "isPreAuthTippingRequired",
        "(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;Lcom/squareup/server/account/protos/Tipping;Z)V",
        "customPercentageOptions",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "getCustomPercentageOptions",
        "()Ljava/util/List;",
        "getCustomPercentages",
        "()Z",
        "isUsingTipPreTax",
        "manualTipEntryLargestMaxMoney",
        "Lcom/squareup/protos/common/Money;",
        "getManualTipEntryLargestMaxMoney",
        "()Lcom/squareup/protos/common/Money;",
        "manualTipEntryMaxPercentage",
        "getManualTipEntryMaxPercentage",
        "()Lcom/squareup/util/Percentage;",
        "manualTipEntrySmallestMaxMoney",
        "getManualTipEntrySmallestMaxMoney",
        "smartTippingOverThresholdOptions",
        "getSmartTippingOverThresholdOptions",
        "smartTippingThresholdMoney",
        "getSmartTippingThresholdMoney",
        "smartTippingUnderThresholdOptions",
        "getSmartTippingUnderThresholdOptions",
        "getTipping",
        "()Lcom/squareup/server/account/protos/Tipping;",
        "getTippingCalculationPhase",
        "()Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;",
        "Companion",
        "TippingCalculationPhase",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/settings/server/TipSettings$Companion;

.field public static final DEFAULT_MANUAL_TIP_ENTRY_LARGEST_MAX_MONEY:Lcom/squareup/protos/common/Money;

.field public static final DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Lcom/squareup/util/Percentage;

.field public static final DEFAULT_MANUAL_TIP_ENTRY_SMALLEST_MAX_MONEY:Lcom/squareup/protos/common/Money;

.field public static final DEFAULT_PERCENTAGES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SMART_TIPPING_OVER_THRESHOLD_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SMART_TIPPING_THRESHOLD_MONEY:Lcom/squareup/protos/common/Money;

.field public static final DEFAULT_SMART_TIPPING_UNDER_THRESHOLD_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_CUSTOM_PERCENTAGES:I = 0x3


# instance fields
.field private final customPercentages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final isEnabled:Z

.field private final isPreAuthTippingRequired:Z

.field private final isUsingCustomAmounts:Z

.field private final isUsingCustomPercentages:Z

.field private final isUsingSeparateTippingScreen:Z

.field private final tipping:Lcom/squareup/server/account/protos/Tipping;

.field private final tippingCalculationPhase:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/squareup/settings/server/TipSettings$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/TipSettings$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/settings/server/TipSettings;->Companion:Lcom/squareup/settings/server/TipSettings$Companion;

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/squareup/util/Percentage;

    .line 115
    sget-object v2, Lcom/squareup/util/Percentage;->FIFTEEN:Lcom/squareup/util/Percentage;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/util/Percentage;->TWENTY:Lcom/squareup/util/Percentage;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/squareup/util/Percentage;->TWENTY_FIVE:Lcom/squareup/util/Percentage;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/squareup/settings/server/TipSettings;->DEFAULT_PERCENTAGES:Ljava/util/List;

    new-array v1, v0, [Lcom/squareup/protos/common/tipping/TipOption;

    .line 119
    sget-object v2, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v6, 0x64

    invoke-static {v6, v7, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/server/account/TipOptionFactory;->forMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v2

    aput-object v2, v1, v3

    .line 120
    sget-object v2, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v6, 0xc8

    invoke-static {v6, v7, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/server/account/TipOptionFactory;->forMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v2

    aput-object v2, v1, v4

    .line 121
    sget-object v2, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v6, 0x12c

    invoke-static {v6, v7, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/server/account/TipOptionFactory;->forMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v2

    aput-object v2, v1, v5

    .line 118
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/squareup/settings/server/TipSettings;->DEFAULT_SMART_TIPPING_UNDER_THRESHOLD_OPTIONS:Ljava/util/List;

    new-array v0, v0, [Lcom/squareup/protos/common/tipping/TipOption;

    .line 124
    sget-object v1, Lcom/squareup/util/Percentage;->FIFTEEN:Lcom/squareup/util/Percentage;

    invoke-static {v1}, Lcom/squareup/server/account/TipOptionFactory;->forPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v1

    aput-object v1, v0, v3

    .line 125
    sget-object v1, Lcom/squareup/util/Percentage;->TWENTY:Lcom/squareup/util/Percentage;

    invoke-static {v1}, Lcom/squareup/server/account/TipOptionFactory;->forPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v1

    aput-object v1, v0, v4

    .line 126
    sget-object v1, Lcom/squareup/util/Percentage;->TWENTY_FIVE:Lcom/squareup/util/Percentage;

    invoke-static {v1}, Lcom/squareup/server/account/TipOptionFactory;->forPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v1

    aput-object v1, v0, v5

    .line 123
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_SMART_TIPPING_OVER_THRESHOLD_OPTIONS:Ljava/util/List;

    .line 128
    sget-object v0, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_SMART_TIPPING_THRESHOLD_MONEY:Lcom/squareup/protos/common/Money;

    .line 129
    sget-object v0, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v6, v7, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_MANUAL_TIP_ENTRY_SMALLEST_MAX_MONEY:Lcom/squareup/protos/common/Money;

    .line 130
    sget-object v0, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/32 v1, 0x186a0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_MANUAL_TIP_ENTRY_LARGEST_MAX_MONEY:Lcom/squareup/protos/common/Money;

    .line 131
    sget-object v0, Lcom/squareup/util/Percentage;->ONE_HUNDRED:Lcom/squareup/util/Percentage;

    sput-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Lcom/squareup/util/Percentage;

    return-void
.end method

.method public constructor <init>(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;Lcom/squareup/server/account/protos/Tipping;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ",
            "Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/server/account/protos/Tipping;",
            "Z)V"
        }
    .end annotation

    const-string/jumbo v0, "tippingCalculationPhase"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/settings/server/TipSettings;->isEnabled:Z

    iput-boolean p2, p0, Lcom/squareup/settings/server/TipSettings;->isUsingCustomPercentages:Z

    iput-boolean p3, p0, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts:Z

    iput-boolean p4, p0, Lcom/squareup/settings/server/TipSettings;->isUsingSeparateTippingScreen:Z

    iput-object p5, p0, Lcom/squareup/settings/server/TipSettings;->tippingCalculationPhase:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    iput-boolean p8, p0, Lcom/squareup/settings/server/TipSettings;->isPreAuthTippingRequired:Z

    .line 100
    sget-object p1, Lcom/squareup/settings/server/TipSettings;->Companion:Lcom/squareup/settings/server/TipSettings$Companion;

    invoke-static {p1, p6}, Lcom/squareup/settings/server/TipSettings$Companion;->access$cleanup(Lcom/squareup/settings/server/TipSettings$Companion;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/settings/server/TipSettings;->customPercentages:Ljava/util/List;

    if-eqz p7, :cond_0

    goto :goto_0

    .line 101
    :cond_0
    new-instance p1, Lcom/squareup/server/account/protos/Tipping$Builder;

    invoke-direct {p1}, Lcom/squareup/server/account/protos/Tipping$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tipping$Builder;->build()Lcom/squareup/server/account/protos/Tipping;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tipping;->populateDefaults()Lcom/squareup/server/account/protos/Tipping;

    move-result-object p7

    const-string p1, "Tipping.Builder().build().populateDefaults()"

    invoke-static {p7, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    iput-object p7, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    return-void
.end method

.method public static final from(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/Tipping;Z)Lcom/squareup/settings/server/TipSettings;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/settings/server/TipSettings;->Companion:Lcom/squareup/settings/server/TipSettings$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/settings/server/TipSettings$Companion;->from(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/Tipping;Z)Lcom/squareup/settings/server/TipSettings;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getCustomPercentageOptions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 54
    invoke-virtual {p0}, Lcom/squareup/settings/server/TipSettings;->getCustomPercentages()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 172
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 173
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 174
    check-cast v2, Lcom/squareup/util/Percentage;

    .line 55
    invoke-static {v2}, Lcom/squareup/server/account/TipOptionFactory;->forPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getCustomPercentages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->customPercentages:Ljava/util/List;

    return-object v0
.end method

.method public getManualTipEntryLargestMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 79
    :cond_0
    sget-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_MANUAL_TIP_ENTRY_LARGEST_MAX_MONEY:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object v0
.end method

.method public getManualTipEntryMaxPercentage()Lcom/squareup/util/Percentage;
    .locals 2

    .line 96
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v1, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v1, v1, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromNullableDouble(Ljava/lang/Double;)Lcom/squareup/util/Percentage;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    sget-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Lcom/squareup/util/Percentage;

    :goto_0
    return-object v0
.end method

.method public getManualTipEntrySmallestMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Tipping;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 88
    :cond_0
    sget-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_MANUAL_TIP_ENTRY_SMALLEST_MAX_MONEY:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object v0
.end method

.method public getSmartTippingOverThresholdOptions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_over_threshold_options:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    sget-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_SMART_TIPPING_OVER_THRESHOLD_OPTIONS:Ljava/util/List;

    :cond_0
    const-string/jumbo v1, "tipping.smart_tipping_ov\u2026R_THRESHOLD_OPTIONS\n    }"

    .line 68
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getSmartTippingThresholdMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_SMART_TIPPING_THRESHOLD_MONEY:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object v0
.end method

.method public getSmartTippingUnderThresholdOptions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Tipping;->smart_tipping_under_threshold_options:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    sget-object v0, Lcom/squareup/settings/server/TipSettings;->DEFAULT_SMART_TIPPING_UNDER_THRESHOLD_OPTIONS:Ljava/util/List;

    :cond_0
    const-string/jumbo v1, "tipping.smart_tipping_un\u2026R_THRESHOLD_OPTIONS\n    }"

    .line 63
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getTipping()Lcom/squareup/server/account/protos/Tipping;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tipping:Lcom/squareup/server/account/protos/Tipping;

    return-object v0
.end method

.method public final getTippingCalculationPhase()Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tippingCalculationPhase:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/settings/server/TipSettings;->isEnabled:Z

    return v0
.end method

.method public isPreAuthTippingRequired()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/settings/server/TipSettings;->isPreAuthTippingRequired:Z

    return v0
.end method

.method public isUsingCustomAmounts()Z
    .locals 1

    .line 28
    iget-boolean v0, p0, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts:Z

    return v0
.end method

.method public isUsingCustomPercentages()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/settings/server/TipSettings;->isUsingCustomPercentages:Z

    return v0
.end method

.method public isUsingSeparateTippingScreen()Z
    .locals 1

    .line 29
    iget-boolean v0, p0, Lcom/squareup/settings/server/TipSettings;->isUsingSeparateTippingScreen:Z

    return v0
.end method

.method public isUsingTipPreTax()Z
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/settings/server/TipSettings;->tippingCalculationPhase:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    sget-object v1, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->PRE_TAX_TIP_CALCULATION:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
