.class public final Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;
.super Ljava/lang/Object;
.source "DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;->preferencesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providesLoyaltyRedeemRewardsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/settings/DeviceSettingsModule;->providesLoyaltyRedeemRewardsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;->providesLoyaltyRedeemRewardsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
