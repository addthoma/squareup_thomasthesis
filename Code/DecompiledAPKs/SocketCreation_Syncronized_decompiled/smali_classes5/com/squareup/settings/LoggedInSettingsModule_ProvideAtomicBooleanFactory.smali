.class public final Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;
.super Ljava/lang/Object;
.source "LoggedInSettingsModule_ProvideAtomicBooleanFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/persistent/AtomicSyncedValue<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final userDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;->userDirProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;)",
            "Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAtomicBoolean(Lcom/google/gson/Gson;Ljava/io/File;)Lcom/squareup/persistent/AtomicSyncedValue;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/settings/LoggedInSettingsModule;->provideAtomicBoolean(Lcom/google/gson/Gson;Ljava/io/File;)Lcom/squareup/persistent/AtomicSyncedValue;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/persistent/AtomicSyncedValue;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/persistent/AtomicSyncedValue;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;->userDirProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-static {v0, v1}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;->provideAtomicBoolean(Lcom/google/gson/Gson;Ljava/io/File;)Lcom/squareup/persistent/AtomicSyncedValue;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAtomicBooleanFactory;->get()Lcom/squareup/persistent/AtomicSyncedValue;

    move-result-object v0

    return-object v0
.end method
