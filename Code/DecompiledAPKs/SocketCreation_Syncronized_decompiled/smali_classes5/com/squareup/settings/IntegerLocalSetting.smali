.class public Lcom/squareup/settings/IntegerLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "IntegerLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultValue:I


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;I)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 15
    iput p3, p0, Lcom/squareup/settings/IntegerLocalSetting;->defaultValue:I

    return-void
.end method


# virtual methods
.method protected doGet()Ljava/lang/Integer;
    .locals 3

    .line 20
    iget-object v0, p0, Lcom/squareup/settings/IntegerLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/IntegerLocalSetting;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/settings/IntegerLocalSetting;->defaultValue:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/IntegerLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/IntegerLocalSetting;->key:Ljava/lang/String;

    iget v2, p0, Lcom/squareup/settings/IntegerLocalSetting;->defaultValue:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doGet()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/settings/IntegerLocalSetting;->doGet()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Integer;)V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/settings/IntegerLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/IntegerLocalSetting;->key:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/settings/IntegerLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .line 5
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/IntegerLocalSetting;->set(Ljava/lang/Integer;)V

    return-void
.end method
