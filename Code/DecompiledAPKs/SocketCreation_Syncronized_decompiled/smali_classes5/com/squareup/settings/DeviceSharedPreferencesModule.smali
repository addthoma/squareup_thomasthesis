.class public abstract Lcom/squareup/settings/DeviceSharedPreferencesModule;
.super Ljava/lang/Object;
.source "DeviceSharedPreferencesModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideDevicePreferences(Landroid/app/Application;)Landroid/content/SharedPreferences;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "global-preferences"

    const/4 v1, 0x0

    .line 18
    invoke-virtual {p0, v0, v1}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method static provideRxDevicePreferences2(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    invoke-static {p0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindLocalSettingFactory(Lcom/squareup/settings/RealLocalSettingFactory;)Lcom/squareup/settings/LocalSettingFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
