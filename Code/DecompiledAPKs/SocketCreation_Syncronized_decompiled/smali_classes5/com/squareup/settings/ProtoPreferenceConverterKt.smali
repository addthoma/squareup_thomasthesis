.class public final Lcom/squareup/settings/ProtoPreferenceConverterKt;
.super Ljava/lang/Object;
.source "ProtoPreferenceConverter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0017\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001H\u0086\u0008\u001a\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001H\u0086\u0008\u00a8\u0006\u0005"
    }
    d2 = {
        "protoListPreferenceAdapter",
        "Lcom/squareup/settings/ProtoListPreferenceConverter;",
        "M",
        "protoPreferenceAdapter",
        "Lcom/squareup/settings/ProtoPreferenceConverter;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic protoListPreferenceAdapter()Lcom/squareup/settings/ProtoListPreferenceConverter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/settings/ProtoListPreferenceConverter<",
            "TM;>;"
        }
    .end annotation

    .line 60
    new-instance v0, Lcom/squareup/settings/ProtoListPreferenceConverter;

    const/4 v1, 0x4

    const-string v2, "M"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/squareup/settings/ProtoListPreferenceConverter;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static final synthetic protoPreferenceAdapter()Lcom/squareup/settings/ProtoPreferenceConverter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/settings/ProtoPreferenceConverter<",
            "TM;>;"
        }
    .end annotation

    .line 56
    new-instance v0, Lcom/squareup/settings/ProtoPreferenceConverter;

    const/4 v1, 0x4

    const-string v2, "M"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/squareup/settings/ProtoPreferenceConverter;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method
