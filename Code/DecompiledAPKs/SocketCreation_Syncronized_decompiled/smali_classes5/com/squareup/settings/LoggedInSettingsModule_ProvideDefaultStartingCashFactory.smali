.class public final Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;
.super Ljava/lang/Object;
.source "LoggedInSettingsModule_ProvideDefaultStartingCashFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LongLocalSetting;",
        ">;"
    }
.end annotation


# instance fields
.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;->preferencesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideDefaultStartingCash(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LongLocalSetting;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/settings/LoggedInSettingsModule;->provideDefaultStartingCash(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LongLocalSetting;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LongLocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LongLocalSetting;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;->provideDefaultStartingCash(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LongLocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideDefaultStartingCashFactory;->get()Lcom/squareup/settings/LongLocalSetting;

    move-result-object v0

    return-object v0
.end method
