.class public abstract Lcom/squareup/settings/AbstractLocalSetting;
.super Ljava/lang/Object;
.source "AbstractLocalSetting.java"

# interfaces
.implements Lcom/squareup/settings/LocalSetting;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/settings/LocalSetting<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z


# instance fields
.field protected final key:Ljava/lang/String;

.field protected final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/settings/AbstractLocalSetting;->preferences:Landroid/content/SharedPreferences;

    .line 14
    iput-object p2, p0, Lcom/squareup/settings/AbstractLocalSetting;->key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected apply(Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 45
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected abstract doGet()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 27
    invoke-virtual {p0}, Lcom/squareup/settings/AbstractLocalSetting;->doGet()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/settings/AbstractLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/AbstractLocalSetting;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 19
    iget-object v2, p0, Lcom/squareup/settings/AbstractLocalSetting;->key:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string v1, "[Settings] \"%s\" get(%s): [default]"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    .line 23
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/settings/AbstractLocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public remove()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/settings/AbstractLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/AbstractLocalSetting;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/settings/AbstractLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
