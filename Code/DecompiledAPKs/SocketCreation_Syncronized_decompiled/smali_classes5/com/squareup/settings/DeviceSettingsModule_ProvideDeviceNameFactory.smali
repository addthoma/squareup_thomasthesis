.class public final Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;
.super Ljava/lang/Object;
.source "DeviceSettingsModule_ProvideDeviceNameFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;->deviceNameProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideDeviceName(Lcom/squareup/settings/LocalSetting;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/settings/DeviceSettingsModule;->provideDeviceName(Lcom/squareup/settings/LocalSetting;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;->deviceNameProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;->provideDeviceName(Lcom/squareup/settings/LocalSetting;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
