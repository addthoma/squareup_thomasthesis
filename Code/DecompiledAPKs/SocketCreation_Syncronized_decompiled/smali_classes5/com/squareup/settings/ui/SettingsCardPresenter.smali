.class public abstract Lcom/squareup/settings/ui/SettingsCardPresenter;
.super Lmortar/ViewPresenter;
.source "SettingsCardPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/ViewGroup;",
        ":",
        "Lcom/squareup/ui/HasActionBar;",
        ">",
        "Lmortar/ViewPresenter<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u000c\u0008\u0000\u0010\u0001*\u00020\u0002*\u00020\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u0004:\u0001 B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0014\u001a\u00020\u0015H\u0002J\u0012\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0008\u0010\u001a\u001a\u00020\u001bH\u0014J\u0008\u0010\u001c\u001a\u00020\u0017H$J\u0010\u0010\u001d\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u001f0\u001eH$R\u0012\u0010\u0008\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\rX\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u000b\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/settings/ui/SettingsCardPresenter;",
        "T",
        "Landroid/view/ViewGroup;",
        "Lcom/squareup/ui/HasActionBar;",
        "Lmortar/ViewPresenter;",
        "coreParameters",
        "Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;",
        "(Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;)V",
        "actionbarText",
        "",
        "getActionbarText",
        "()Ljava/lang/String;",
        "device",
        "Lcom/squareup/util/Device;",
        "getDevice",
        "()Lcom/squareup/util/Device;",
        "flow",
        "Lflow/Flow;",
        "primaryButtonText",
        "getPrimaryButtonText",
        "buildDefaultConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
        "onLoad",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onUpPressed",
        "",
        "saveSettings",
        "screenForAssertion",
        "Ljava/lang/Class;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "CoreParameters",
        "settings-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;)V
    .locals 1

    const-string v0, "coreParameters"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->getDevice()Lcom/squareup/util/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/settings/ui/SettingsCardPresenter;->device:Lcom/squareup/util/Device;

    .line 34
    invoke-virtual {p1}, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/settings/ui/SettingsCardPresenter;->flow:Lflow/Flow;

    return-void
.end method

.method private final buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 69
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const-string v1, "Builder()\n        .hideP\u2026   .hideSecondaryButton()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract getActionbarText()Ljava/lang/String;
.end method

.method protected final getDevice()Lcom/squareup/util/Device;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/settings/ui/SettingsCardPresenter;->device:Lcom/squareup/util/Device;

    return-object v0
.end method

.method public abstract getPrimaryButtonText()Ljava/lang/String;
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 38
    invoke-direct {p0}, Lcom/squareup/settings/ui/SettingsCardPresenter;->buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 39
    iget-object v0, p0, Lcom/squareup/settings/ui/SettingsCardPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/settings/ui/SettingsCardPresenter;->getPrimaryButtonText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/settings/ui/SettingsCardPresenter$onLoad$1;

    invoke-direct {v1, p0}, Lcom/squareup/settings/ui/SettingsCardPresenter$onLoad$1;-><init>(Lcom/squareup/settings/ui/SettingsCardPresenter;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p0}, Lcom/squareup/settings/ui/SettingsCardPresenter;->getActionbarText()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/settings/ui/SettingsCardPresenter$onLoad$2;

    invoke-direct {v1, p0}, Lcom/squareup/settings/ui/SettingsCardPresenter$onLoad$2;-><init>(Lcom/squareup/settings/ui/SettingsCardPresenter;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/settings/ui/SettingsCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const-string/jumbo v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/HasActionBar;

    invoke-interface {v0}, Lcom/squareup/ui/HasActionBar;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.actionBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method protected onUpPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract saveSettings()V
.end method

.method protected abstract screenForAssertion()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation
.end method
