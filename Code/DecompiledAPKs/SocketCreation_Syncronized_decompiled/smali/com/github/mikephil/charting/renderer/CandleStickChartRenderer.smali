.class public Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;
.super Lcom/github/mikephil/charting/renderer/DataRenderer;
.source "CandleStickChartRenderer.java"


# instance fields
.field private mBodyBuffers:[Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;

.field protected mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

.field private mShadowBuffers:[Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/interfaces/CandleDataProvider;Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/utils/ViewPortHandler;)V
    .locals 0

    .line 33
    invoke-direct {p0, p2, p3}, Lcom/github/mikephil/charting/renderer/DataRenderer;-><init>(Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/utils/ViewPortHandler;)V

    .line 34
    iput-object p1, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    return-void
.end method


# virtual methods
.method public drawData(Landroid/graphics/Canvas;)V
    .locals 3

    .line 53
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getCandleData()Lcom/github/mikephil/charting/data/CandleData;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/CandleData;->getDataSets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/github/mikephil/charting/data/CandleDataSet;

    .line 57
    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/CandleDataSet;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    invoke-virtual {p0, p1, v1}, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/CandleDataSet;)V

    goto :goto_0
.end method

.method protected drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/CandleDataSet;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 64
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v2

    .line 66
    iget-object v3, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v3

    .line 67
    iget-object v4, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v4

    .line 69
    iget-object v5, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v5}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getCandleData()Lcom/github/mikephil/charting/data/CandleData;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/github/mikephil/charting/data/CandleData;->getIndexOfDataSet(Lcom/github/mikephil/charting/data/DataSet;)I

    move-result v5

    .line 71
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getYVals()Ljava/util/List;

    move-result-object v6

    .line 73
    iget v7, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mMinX:I

    invoke-virtual {v1, v7}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryForXIndex(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v7

    .line 74
    iget v8, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mMaxX:I

    invoke-virtual {v1, v8}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryForXIndex(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v8

    .line 76
    invoke-virtual {v1, v7}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryPosition(Lcom/github/mikephil/charting/data/Entry;)I

    move-result v7

    const/4 v9, 0x0

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 77
    invoke-virtual {v1, v8}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryPosition(Lcom/github/mikephil/charting/data/Entry;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v8, v10}, Ljava/lang/Math;->min(II)I

    move-result v8

    sub-int v10, v8, v7

    mul-int/lit8 v11, v10, 0x4

    int-to-float v10, v10

    mul-float v10, v10, v3

    int-to-float v12, v7

    add-float/2addr v10, v12

    float-to-double v12, v10

    .line 80
    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v10, v12

    .line 82
    iget-object v12, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mShadowBuffers:[Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;

    aget-object v12, v12, v5

    .line 83
    invoke-virtual {v12, v3, v4}, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;->setPhases(FF)V

    .line 84
    invoke-virtual {v12, v7}, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;->limitFrom(I)V

    .line 85
    invoke-virtual {v12, v8}, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;->limitTo(I)V

    .line 86
    invoke-virtual {v12, v6}, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;->feed(Ljava/util/List;)V

    .line 88
    iget-object v13, v12, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;->buffer:[F

    invoke-virtual {v2, v13}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 90
    iget-object v13, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v14, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getShadowColor()I

    move-result v13

    const/4 v14, -0x1

    if-ne v13, v14, :cond_0

    .line 94
    iget-object v13, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getColor()I

    move-result v15

    invoke-virtual {v13, v15}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 96
    :cond_0
    iget-object v13, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getShadowColor()I

    move-result v15

    invoke-virtual {v13, v15}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    :goto_0
    iget-object v13, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getShadowWidth()F

    move-result v15

    invoke-virtual {v13, v15}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 102
    iget-object v12, v12, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;->buffer:[F

    iget-object v13, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object/from16 v15, p1

    invoke-virtual {v15, v12, v9, v11, v13}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 104
    iget-object v12, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mBodyBuffers:[Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;

    aget-object v5, v12, v5

    .line 105
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getBodySpace()F

    move-result v12

    invoke-virtual {v5, v12}, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->setBodySpace(F)V

    .line 106
    invoke-virtual {v5, v3, v4}, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->setPhases(FF)V

    .line 107
    invoke-virtual {v5, v7}, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->limitFrom(I)V

    .line 108
    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->limitTo(I)V

    .line 109
    invoke-virtual {v5, v6}, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->feed(Ljava/util/List;)V

    .line 111
    iget-object v3, v5, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->buffer:[F

    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    :goto_1
    if-lt v9, v11, :cond_1

    return-void

    .line 117
    :cond_1
    div-int/lit8 v2, v9, 0x4

    add-int/2addr v2, v7

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/github/mikephil/charting/data/CandleEntry;

    .line 119
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/CandleEntry;->getXIndex()I

    move-result v3

    int-to-float v3, v3

    iget v4, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mMinX:I

    int-to-float v4, v4

    int-to-float v8, v10

    invoke-virtual {v0, v3, v4, v8}, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->fitsBounds(FFF)Z

    move-result v3

    if-nez v3, :cond_2

    goto/16 :goto_4

    .line 122
    :cond_2
    iget-object v3, v5, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->buffer:[F

    aget v16, v3, v9

    .line 123
    iget-object v3, v5, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->buffer:[F

    add-int/lit8 v4, v9, 0x1

    aget v3, v3, v4

    .line 124
    iget-object v4, v5, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->buffer:[F

    add-int/lit8 v8, v9, 0x2

    aget v18, v4, v8

    .line 125
    iget-object v4, v5, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;->buffer:[F

    add-int/lit8 v8, v9, 0x3

    aget v19, v4, v8

    cmpl-float v4, v3, v19

    if-lez v4, :cond_4

    .line 130
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getDecreasingColor()I

    move-result v4

    if-ne v4, v14, :cond_3

    .line 131
    iget-object v4, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    .line 133
    :cond_3
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getDecreasingColor()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    :goto_2
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getDecreasingPaintStyle()Landroid/graphics/Paint$Style;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 138
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object/from16 v15, p1

    move/from16 v17, v19

    move/from16 v19, v3

    move-object/from16 v20, v2

    invoke-virtual/range {v15 .. v20}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_4

    :cond_4
    cmpg-float v4, v3, v19

    if-gez v4, :cond_6

    .line 142
    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getIncreasingColor()I

    move-result v4

    if-ne v4, v14, :cond_5

    .line 143
    iget-object v4, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3

    .line 145
    :cond_5
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getIncreasingColor()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    :goto_3
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p2 .. p2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getIncreasingPaintStyle()Landroid/graphics/Paint$Style;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 150
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object/from16 v15, p1

    move/from16 v17, v3

    move-object/from16 v20, v2

    invoke-virtual/range {v15 .. v20}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_4

    .line 153
    :cond_6
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    iget-object v2, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object/from16 v15, p1

    move/from16 v17, v3

    move-object/from16 v20, v2

    invoke-virtual/range {v15 .. v20}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_4
    add-int/lit8 v9, v9, 0x4

    move-object/from16 v15, p1

    goto/16 :goto_1
.end method

.method public drawExtras(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public drawHighlighted(Landroid/graphics/Canvas;[Lcom/github/mikephil/charting/utils/Highlight;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 260
    :goto_0
    array-length v5, v2

    if-lt v4, v5, :cond_0

    return-void

    .line 262
    :cond_0
    aget-object v5, v2, v4

    invoke-virtual {v5}, Lcom/github/mikephil/charting/utils/Highlight;->getXIndex()I

    move-result v5

    .line 265
    iget-object v6, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v6}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getCandleData()Lcom/github/mikephil/charting/data/CandleData;

    move-result-object v6

    .line 266
    aget-object v7, v2, v4

    invoke-virtual {v7}, Lcom/github/mikephil/charting/utils/Highlight;->getDataSetIndex()I

    move-result v7

    .line 265
    invoke-virtual {v6, v7}, Lcom/github/mikephil/charting/data/CandleData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/DataSet;

    move-result-object v6

    check-cast v6, Lcom/github/mikephil/charting/data/CandleDataSet;

    if-nez v6, :cond_1

    goto/16 :goto_1

    .line 271
    :cond_1
    iget-object v7, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v6}, Lcom/github/mikephil/charting/data/CandleDataSet;->getHighLightColor()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 273
    invoke-virtual {v6, v5}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryForXIndex(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v7

    check-cast v7, Lcom/github/mikephil/charting/data/CandleEntry;

    if-nez v7, :cond_2

    goto/16 :goto_1

    .line 278
    :cond_2
    invoke-virtual {v7}, Lcom/github/mikephil/charting/data/CandleEntry;->getLow()F

    move-result v8

    iget-object v9, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v9}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v9

    mul-float v8, v8, v9

    .line 279
    invoke-virtual {v7}, Lcom/github/mikephil/charting/data/CandleEntry;->getHigh()F

    move-result v7

    iget-object v9, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v9}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v9

    mul-float v7, v7, v9

    .line 281
    iget-object v9, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v9}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getYChartMin()F

    move-result v9

    .line 282
    iget-object v10, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v10}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getYChartMax()F

    move-result v10

    const/16 v11, 0x8

    new-array v12, v11, [F

    int-to-float v5, v5

    const/high16 v13, 0x3f000000    # 0.5f

    sub-float v14, v5, v13

    aput v14, v12, v3

    const/4 v15, 0x1

    aput v10, v12, v15

    const/16 v16, 0x2

    aput v14, v12, v16

    const/4 v14, 0x3

    aput v9, v12, v14

    add-float/2addr v5, v13

    const/4 v13, 0x4

    aput v5, v12, v13

    const/16 v17, 0x5

    aput v10, v12, v17

    const/4 v10, 0x6

    aput v5, v12, v10

    const/4 v5, 0x7

    aput v9, v12, v5

    new-array v9, v11, [F

    .line 290
    iget-object v11, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v11}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getXChartMin()F

    move-result v11

    aput v11, v9, v3

    aput v8, v9, v15

    iget-object v11, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v11}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getXChartMax()F

    move-result v11

    aput v11, v9, v16

    aput v8, v9, v14

    iget-object v8, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v8}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getXChartMin()F

    move-result v8

    aput v8, v9, v13

    aput v7, v9, v17

    iget-object v8, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v8}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getXChartMax()F

    move-result v8

    aput v8, v9, v10

    aput v7, v9, v5

    .line 293
    iget-object v5, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-virtual {v6}, Lcom/github/mikephil/charting/data/CandleDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v7

    invoke-interface {v5, v7}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 294
    iget-object v5, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-virtual {v6}, Lcom/github/mikephil/charting/data/CandleDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 297
    iget-object v5, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v12, v5}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    .line 300
    iget-object v5, v0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v9, v5}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0
.end method

.method public drawValues(Landroid/graphics/Canvas;)V
    .locals 13

    .line 203
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getCandleData()Lcom/github/mikephil/charting/data/CandleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/CandleData;->getYValCount()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v1}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getMaxVisibleCount()I

    move-result v1

    int-to-float v1, v1

    .line 204
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->getScaleX()F

    move-result v2

    mul-float v1, v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 206
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getCandleData()Lcom/github/mikephil/charting/data/CandleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/CandleData;->getDataSets()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 208
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    goto/16 :goto_4

    .line 210
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/github/mikephil/charting/data/CandleDataSet;

    .line 212
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/CandleDataSet;->isDrawValuesEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    .line 216
    :cond_1
    invoke-virtual {p0, v3}, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->applyValueTextStyle(Lcom/github/mikephil/charting/data/DataSet;)V

    .line 218
    iget-object v4, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/CandleDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v6

    .line 220
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/CandleDataSet;->getYVals()Ljava/util/List;

    move-result-object v4

    .line 222
    iget v5, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mMinX:I

    invoke-virtual {v3, v5}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryForXIndex(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v5

    .line 223
    iget v7, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mMaxX:I

    invoke-virtual {v3, v7}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryForXIndex(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v7

    .line 225
    invoke-virtual {v3, v5}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryPosition(Lcom/github/mikephil/charting/data/Entry;)I

    move-result v5

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 226
    invoke-virtual {v3, v7}, Lcom/github/mikephil/charting/data/CandleDataSet;->getEntryPosition(Lcom/github/mikephil/charting/data/Entry;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 229
    iget-object v7, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v7}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v8

    iget-object v7, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v7}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v9

    move-object v7, v4

    move v10, v5

    .line 228
    invoke-virtual/range {v6 .. v11}, Lcom/github/mikephil/charting/utils/Transformer;->generateTransformedValuesCandle(Ljava/util/List;FFII)[F

    move-result-object v6

    const/high16 v7, 0x40a00000    # 5.0f

    .line 231
    invoke-static {v7}, Lcom/github/mikephil/charting/utils/Utils;->convertDpToPixel(F)F

    move-result v7

    const/4 v8, 0x0

    .line 233
    :goto_1
    array-length v9, v6

    if-lt v8, v9, :cond_2

    goto :goto_2

    .line 235
    :cond_2
    aget v9, v6, v8

    add-int/lit8 v10, v8, 0x1

    .line 236
    aget v10, v6, v10

    .line 238
    iget-object v11, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {v11, v9}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v11

    if-nez v11, :cond_3

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 241
    :cond_3
    iget-object v11, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {v11, v9}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {v11, v10}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v11

    if-nez v11, :cond_4

    goto :goto_3

    .line 244
    :cond_4
    div-int/lit8 v11, v8, 0x2

    add-int/2addr v11, v5

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/github/mikephil/charting/data/CandleEntry;

    invoke-virtual {v11}, Lcom/github/mikephil/charting/data/CandleEntry;->getHigh()F

    move-result v11

    .line 246
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/CandleDataSet;->getValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;

    move-result-object v12

    invoke-interface {v12, v11}, Lcom/github/mikephil/charting/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v11

    sub-float/2addr v10, v7

    .line 247
    iget-object v12, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    .line 246
    invoke-virtual {p1, v11, v9, v10, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    :goto_3
    add-int/lit8 v8, v8, 0x2

    goto :goto_1

    :cond_6
    :goto_4
    return-void
.end method

.method public initBuffers()V
    .locals 6

    .line 39
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/CandleDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/CandleDataProvider;->getCandleData()Lcom/github/mikephil/charting/data/CandleData;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/CandleData;->getDataSetCount()I

    move-result v1

    new-array v1, v1, [Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;

    iput-object v1, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mShadowBuffers:[Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;

    .line 41
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/CandleData;->getDataSetCount()I

    move-result v1

    new-array v1, v1, [Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;

    iput-object v1, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mBodyBuffers:[Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;

    const/4 v1, 0x0

    .line 43
    :goto_0
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mShadowBuffers:[Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    return-void

    .line 44
    :cond_0
    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/CandleData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/DataSet;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/CandleDataSet;

    .line 45
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mShadowBuffers:[Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;

    new-instance v4, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getValueCount()I

    move-result v5

    mul-int/lit8 v5, v5, 0x4

    invoke-direct {v4, v5}, Lcom/github/mikephil/charting/buffer/CandleShadowBuffer;-><init>(I)V

    aput-object v4, v3, v1

    .line 46
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/CandleStickChartRenderer;->mBodyBuffers:[Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;

    new-instance v4, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/CandleDataSet;->getValueCount()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    invoke-direct {v4, v2}, Lcom/github/mikephil/charting/buffer/CandleBodyBuffer;-><init>(I)V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
