.class Lcom/epson/epos2/printer/Printer$4;
.super Lcom/epson/epos2/printer/Printer$InnerThread;
.source "Printer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/epos2/printer/Printer;->setPrinterSetting(ILjava/util/Map;Lcom/epson/epos2/printer/PrinterSettingListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/epos2/printer/Printer;


# direct methods
.method constructor <init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V
    .locals 0

    .line 1485
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$4;->this$0:Lcom/epson/epos2/printer/Printer;

    invoke-direct/range {p0 .. p9}, Lcom/epson/epos2/printer/Printer$InnerThread;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1487
    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$4;->mPrn:Lcom/epson/epos2/printer/Printer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$4;->mListener:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$4;->mTypes:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$4;->mValues:[I

    if-nez v0, :cond_0

    goto :goto_0

    .line 1490
    :cond_0
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer$4;->mPrn:Lcom/epson/epos2/printer/Printer;

    iget-wide v2, p0, Lcom/epson/epos2/printer/Printer$4;->mHandle:J

    iget v4, p0, Lcom/epson/epos2/printer/Printer$4;->mTimeout:I

    iget-object v5, p0, Lcom/epson/epos2/printer/Printer$4;->mTypes:[I

    iget-object v6, p0, Lcom/epson/epos2/printer/Printer$4;->mValues:[I

    invoke-virtual/range {v1 .. v6}, Lcom/epson/epos2/printer/Printer;->nativeEpos2SetPrinterSetting(JI[I[I)I

    move-result v0

    .line 1491
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer$4;->this$0:Lcom/epson/epos2/printer/Printer;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "code->"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 1492
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "onSetPrinterSetting"

    .line 1491
    invoke-virtual {v1, v3, v2}, Lcom/epson/epos2/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1493
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer$4;->mListener:Ljava/lang/Object;

    check-cast v1, Lcom/epson/epos2/printer/PrinterSettingListener;

    invoke-interface {v1, v0}, Lcom/epson/epos2/printer/PrinterSettingListener;->onSetPrinterSetting(I)V

    :cond_1
    :goto_0
    return-void
.end method
