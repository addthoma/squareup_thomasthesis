.class public Lcom/epson/epos2/printer/Printer;
.super Lcom/epson/epos2/printer/CommonPrinter;
.source "Printer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/epos2/printer/Printer$InnerThread;
    }
.end annotation


# static fields
.field public static final BATTERY_LEVEL_0:I = 0x0

.field public static final BATTERY_LEVEL_1:I = 0x1

.field public static final BATTERY_LEVEL_2:I = 0x2

.field public static final BATTERY_LEVEL_3:I = 0x3

.field public static final BATTERY_LEVEL_4:I = 0x4

.field public static final BATTERY_LEVEL_5:I = 0x5

.field public static final BATTERY_LEVEL_6:I = 0x6

.field public static final EVENT_BATTERY_EMPTY:I = 0xb

.field public static final EVENT_BATTERY_ENOUGH:I = 0xa

.field public static final FEED_CURRENT_TOF:I = 0x2

.field public static final FEED_CUTTING:I = 0x1

.field public static final FEED_NEXT_TOF:I = 0x3

.field public static final FEED_PEELING:I = 0x0

.field public static final LAYOUT_LABEL:I = 0x1

.field public static final LAYOUT_LABEL_BM:I = 0x2

.field public static final LAYOUT_RECEIPT:I = 0x0

.field public static final LAYOUT_RECEIPT_BM:I = 0x3

.field public static final MAINTENANCE_COUNTER_AUTOCUTTER:I = 0x1

.field public static final MAINTENANCE_COUNTER_OTHER:I = 0x2

.field public static final MAINTENANCE_COUNTER_PAPERFEED:I = 0x0

.field private static final MAX_PRINTJOBID_LENGTH:I = 0x1e

.field private static final MIN_PRINTJOBID_LENGTH:I = 0x1

.field public static final PATTERN_1:I = 0x8

.field public static final PATTERN_10:I = 0x11

.field public static final PATTERN_2:I = 0x9

.field public static final PATTERN_3:I = 0xa

.field public static final PATTERN_4:I = 0xb

.field public static final PATTERN_5:I = 0xc

.field public static final PATTERN_6:I = 0xd

.field public static final PATTERN_7:I = 0xe

.field public static final PATTERN_8:I = 0xf

.field public static final PATTERN_9:I = 0x10

.field public static final PATTERN_A:I = 0x1

.field public static final PATTERN_B:I = 0x2

.field public static final PATTERN_C:I = 0x3

.field public static final PATTERN_D:I = 0x4

.field public static final PATTERN_E:I = 0x5

.field public static final PATTERN_ERROR:I = 0x6

.field public static final PATTERN_NONE:I = 0x0

.field public static final PATTERN_PAPER_EMPTY:I = 0x7

.field public static final SETTING_OTHER:I = 0x3

.field public static final SETTING_PAPERWIDTH:I = 0x0

.field public static final SETTING_PAPERWIDTH_58_0:I = 0x2

.field public static final SETTING_PAPERWIDTH_60_0:I = 0x3

.field public static final SETTING_PAPERWIDTH_80_0:I = 0x6

.field public static final SETTING_PAPERWIDTH_NOT_SETTING_TARGET:I = 0x186a0

.field public static final SETTING_PRINTDENSITY:I = 0x1

.field public static final SETTING_PRINTDENSITY_100:I = 0x0

.field public static final SETTING_PRINTDENSITY_105:I = 0x1

.field public static final SETTING_PRINTDENSITY_110:I = 0x2

.field public static final SETTING_PRINTDENSITY_115:I = 0x3

.field public static final SETTING_PRINTDENSITY_120:I = 0x4

.field public static final SETTING_PRINTDENSITY_125:I = 0x5

.field public static final SETTING_PRINTDENSITY_130:I = 0x6

.field public static final SETTING_PRINTDENSITY_70:I = 0xfffa

.field public static final SETTING_PRINTDENSITY_75:I = 0xfffb

.field public static final SETTING_PRINTDENSITY_80:I = 0xfffc

.field public static final SETTING_PRINTDENSITY_85:I = 0xfffd

.field public static final SETTING_PRINTDENSITY_90:I = 0xfffe

.field public static final SETTING_PRINTDENSITY_95:I = 0xffff

.field public static final SETTING_PRINTDENSITY_DIP:I = 0x64

.field public static final SETTING_PRINTDENSITY_NOT_SETTING_TARGET:I = 0x186a0

.field public static final SETTING_PRINTSPEED:I = 0x2

.field public static final SETTING_PRINTSPEED_1:I = 0x1

.field public static final SETTING_PRINTSPEED_10:I = 0xa

.field public static final SETTING_PRINTSPEED_11:I = 0xb

.field public static final SETTING_PRINTSPEED_12:I = 0xc

.field public static final SETTING_PRINTSPEED_13:I = 0xd

.field public static final SETTING_PRINTSPEED_14:I = 0xe

.field public static final SETTING_PRINTSPEED_2:I = 0x2

.field public static final SETTING_PRINTSPEED_3:I = 0x3

.field public static final SETTING_PRINTSPEED_4:I = 0x4

.field public static final SETTING_PRINTSPEED_5:I = 0x5

.field public static final SETTING_PRINTSPEED_6:I = 0x6

.field public static final SETTING_PRINTSPEED_7:I = 0x7

.field public static final SETTING_PRINTSPEED_8:I = 0x8

.field public static final SETTING_PRINTSPEED_9:I = 0x9

.field public static final SETTING_PRINTSPEED_NOT_SETTING_TARGET:I = 0x186a0

.field public static final TM_H6000:I = 0x12

.field public static final TM_L90:I = 0x11

.field public static final TM_M10:I = 0x0

.field public static final TM_M30:I = 0x1

.field public static final TM_P20:I = 0x2

.field public static final TM_P60:I = 0x3

.field public static final TM_P60II:I = 0x4

.field public static final TM_P80:I = 0x5

.field public static final TM_T100:I = 0x14

.field public static final TM_T20:I = 0x6

.field public static final TM_T60:I = 0x7

.field public static final TM_T70:I = 0x8

.field public static final TM_T81:I = 0x9

.field public static final TM_T82:I = 0xa

.field public static final TM_T83:I = 0xb

.field public static final TM_T83III:I = 0x13

.field public static final TM_T88:I = 0xc

.field public static final TM_T90:I = 0xd

.field public static final TM_T90KP:I = 0xe

.field public static final TM_U220:I = 0xf

.field public static final TM_U330:I = 0x10


# instance fields
.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mReceiveListener:Lcom/epson/epos2/printer/ReceiveListener;

.field private mStatusChangeListener:Lcom/epson/epos2/printer/StatusChangeListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 257
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;-><init>()V

    return-void
.end method

.method public constructor <init>(IILandroid/content/Context;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 234
    invoke-direct {p0}, Lcom/epson/epos2/printer/Printer;-><init>()V

    .line 235
    invoke-virtual {p0, p3}, Lcom/epson/epos2/printer/Printer;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 236
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const-string v5, "Printer"

    invoke-virtual {p0, v5, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 238
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v6, "initializeNativeEnv"

    new-array v7, v4, [Ljava/lang/Class;

    .line 239
    const-class v8, Landroid/content/Context;

    aput-object v8, v7, v3

    invoke-virtual {v1, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 240
    invoke-virtual {v6, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v7, v4, [Ljava/lang/Object;

    aput-object p3, v7, v3

    .line 241
    invoke-virtual {v6, v1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 244
    invoke-virtual {p0, v5, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 245
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 247
    :goto_0
    iput-object p3, p0, Lcom/epson/epos2/printer/Printer;->mContext:Landroid/content/Context;

    .line 249
    invoke-direct {p0, p1, p2}, Lcom/epson/epos2/printer/Printer;->initializePrinterInstance(II)V

    new-array v0, v0, [Ljava/lang/Object;

    .line 250
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    aput-object p3, v0, v2

    invoke-virtual {p0, v5, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private checkPrintJobIdFormat(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 207
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_1

    const/16 v1, 0x1e

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_1

    const-string v1, ".*[^-._0-9A-Za-z].*"

    .line 210
    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 211
    :cond_0
    new-instance p1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p1

    .line 208
    :cond_1
    new-instance p1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p1

    .line 205
    :cond_2
    new-instance p1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p1
.end method

.method private initializePrinterInstance(II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 280
    invoke-virtual {p0, p1, p2, v0}, Lcom/epson/epos2/printer/Printer;->nativeEpos2CreateHandle(II[J)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 286
    aget-wide v1, v0, p1

    iput-wide v1, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const-wide/16 v1, 0x0

    aput-wide v1, v0, p1

    return-void

    .line 283
    :cond_0
    new-instance p2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p2, p1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p2
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1583
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1585
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 1589
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 1588
    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1591
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1595
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-virtual {p0, v4, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onPtrReceive(ILcom/epson/epos2/printer/PrinterStatusInfo;Ljava/lang/String;)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    .line 1530
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getConnection()I

    move-result v3

    .line 1531
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getOnline()I

    move-result v4

    .line 1532
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getCoverOpen()I

    move-result v5

    .line 1533
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPaper()I

    move-result v6

    .line 1534
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPaperFeed()I

    move-result v7

    .line 1535
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPanelSwitch()I

    move-result v8

    .line 1536
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getOnline()I

    move-result v9

    .line 1537
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getDrawer()I

    move-result v10

    .line 1538
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getErrorStatus()I

    move-result v11

    .line 1539
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getAutoRecoverError()I

    move-result v12

    .line 1540
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getBuzzer()I

    move-result v13

    .line 1541
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getAdapter()I

    move-result v14

    .line 1542
    invoke-virtual/range {p2 .. p2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getBatteryLevel()I

    move-result v15

    move/from16 v16, v15

    const/4 v15, 0x4

    move/from16 v17, v14

    new-array v14, v15, [Ljava/lang/Object;

    .line 1546
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    const/4 v15, 0x0

    aput-object v18, v14, v15

    const/16 v18, 0x1

    aput-object v1, v14, v18

    const/16 v20, 0x2

    aput-object v2, v14, v20

    const/16 v21, 0x3

    aput-object v0, v14, v21

    const-string v15, "onPtrReceive"

    invoke-virtual {v0, v15, v14}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1548
    iget-object v14, v0, Lcom/epson/epos2/printer/Printer;->mReceiveListener:Lcom/epson/epos2/printer/ReceiveListener;

    if-eqz v14, :cond_0

    const/16 v14, 0x1e

    new-array v14, v14, [Ljava/lang/Object;

    const-string v23, "code->"

    const/16 v22, 0x0

    aput-object v23, v14, v22

    .line 1554
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v14, v18

    const-string v23, "connection->"

    aput-object v23, v14, v20

    .line 1555
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v14, v21

    const-string v3, "online->"

    const/16 v19, 0x4

    aput-object v3, v14, v19

    const/4 v3, 0x5

    .line 1556
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/4 v3, 0x6

    const-string v4, "coverOpen->"

    aput-object v4, v14, v3

    const/4 v3, 0x7

    .line 1557
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x8

    const-string v4, "paper->"

    aput-object v4, v14, v3

    const/16 v3, 0x9

    .line 1558
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0xa

    const-string v4, "paperFeed->"

    aput-object v4, v14, v3

    const/16 v3, 0xb

    .line 1559
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0xc

    const-string v4, "panelSwitch->"

    aput-object v4, v14, v3

    const/16 v3, 0xd

    .line 1560
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0xe

    const-string v4, "waitOnline->"

    aput-object v4, v14, v3

    const/16 v3, 0xf

    .line 1561
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x10

    const-string v4, "drawer->"

    aput-object v4, v14, v3

    const/16 v3, 0x11

    .line 1562
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x12

    const-string v4, "errorStatus->"

    aput-object v4, v14, v3

    const/16 v3, 0x13

    .line 1563
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x14

    const-string v4, "autoRecoverError->"

    aput-object v4, v14, v3

    const/16 v3, 0x15

    .line 1564
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x16

    const-string v4, "buzzer->"

    aput-object v4, v14, v3

    const/16 v3, 0x17

    .line 1565
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x18

    const-string v4, "adapter->"

    aput-object v4, v14, v3

    const/16 v3, 0x19

    .line 1566
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x1a

    const-string v4, "batteryLevel->"

    aput-object v4, v14, v3

    const/16 v3, 0x1b

    .line 1567
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v3

    const/16 v3, 0x1c

    const-string v4, "printJobId->"

    aput-object v4, v14, v3

    const/16 v3, 0x1d

    aput-object v2, v14, v3

    .line 1553
    invoke-virtual {v0, v15, v14}, Lcom/epson/epos2/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1570
    iget-object v3, v0, Lcom/epson/epos2/printer/Printer;->mReceiveListener:Lcom/epson/epos2/printer/ReceiveListener;

    move/from16 v4, p1

    invoke-interface {v3, v0, v4, v1, v2}, Lcom/epson/epos2/printer/ReceiveListener;->onPtrReceive(Lcom/epson/epos2/printer/Printer;ILcom/epson/epos2/printer/PrinterStatusInfo;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move/from16 v4, p1

    :goto_0
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    .line 1576
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    aput-object v1, v3, v18

    aput-object v2, v3, v20

    aput-object v0, v3, v21

    invoke-virtual {v0, v15, v5, v3}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onPtrStatusChange(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1513
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onPtrStatusChange"

    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1515
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer;->mStatusChangeListener:Lcom/epson/epos2/printer/StatusChangeListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 1519
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 1518
    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1521
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer;->mStatusChangeListener:Lcom/epson/epos2/printer/StatusChangeListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/printer/StatusChangeListener;->onPtrStatusChange(Lcom/epson/epos2/printer/Printer;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1525
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-virtual {p0, v4, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic addBarcode(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super/range {p0 .. p6}, Lcom/epson/epos2/printer/CommonPrinter;->addBarcode(Ljava/lang/String;IIIII)V

    return-void
.end method

.method public bridge synthetic addCommand([B)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addCommand([B)V

    return-void
.end method

.method public bridge synthetic addCut(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addCut(I)V

    return-void
.end method

.method public bridge synthetic addFeedLine(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addFeedLine(I)V

    return-void
.end method

.method public addFeedPosition(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 706
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addFeedPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 710
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 712
    iget-wide v4, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/Printer;->nativeEpos2AddFeedPosition(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 723
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 714
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 718
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 719
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 720
    throw v1
.end method

.method public bridge synthetic addFeedUnit(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addFeedUnit(I)V

    return-void
.end method

.method public addHLine(III)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    .line 553
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x0

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x2

    aput-object v1, v0, v13

    const-string v14, "addHLine"

    invoke-virtual {v9, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 557
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 559
    iget-wide v2, v9, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v15, p1

    int-to-long v4, v15

    move/from16 v8, p2

    int-to-long v6, v8

    move-object/from16 v1, p0

    move/from16 v8, p3

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/epos2/printer/Printer;->nativeEpos2AddHLine(JJJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v10, [Ljava/lang/Object;

    .line 570
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-virtual {v9, v14, v11, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 561
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v15, p1

    .line 565
    :goto_0
    invoke-virtual {v9, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 566
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-virtual {v9, v14, v1, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 567
    throw v0
.end method

.method public bridge synthetic addHPosition(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addHPosition(I)V

    return-void
.end method

.method public bridge synthetic addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super/range {p0 .. p11}, Lcom/epson/epos2/printer/CommonPrinter;->addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V

    return-void
.end method

.method public addLayout(IIIIIII)V
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v15, p0

    const/4 v13, 0x7

    new-array v0, v13, [Ljava/lang/Object;

    .line 755
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x0

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x1

    aput-object v1, v0, v17

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x2

    aput-object v1, v0, v18

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v19, 0x3

    aput-object v1, v0, v19

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v20, 0x4

    aput-object v1, v0, v20

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v21, 0x5

    aput-object v1, v0, v21

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v22, 0x6

    aput-object v1, v0, v22

    const-string v11, "addLayout"

    invoke-virtual {v15, v11, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 759
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 761
    iget-wide v2, v15, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v12, p2

    int-to-long v5, v12

    move/from16 v9, p3

    int-to-long v7, v9

    move/from16 v10, p4

    int-to-long v0, v10

    move/from16 v4, p5

    int-to-long v13, v4

    move/from16 v15, p6

    move-object/from16 v23, v11

    int-to-long v11, v15

    move/from16 v15, p7

    move-wide/from16 v24, v11

    int-to-long v11, v15

    move-wide/from16 v26, v0

    move-object/from16 v1, p0

    move/from16 v4, p1

    move-wide/from16 v9, v26

    move-object/from16 v28, v23

    move-wide/from16 v23, v24

    move-wide/from16 v25, v11

    move-wide v11, v13

    move-wide/from16 v13, v23

    move-wide/from16 v15, v25

    :try_start_1
    invoke-virtual/range {v1 .. v16}, Lcom/epson/epos2/printer/Printer;->nativeEpos2AddLayout(JIJJJJJJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/Object;

    .line 773
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v20

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v21

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v22

    move-object/from16 v3, p0

    move-object/from16 v4, v28

    invoke-virtual {v3, v4, v2, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v4, v28

    const/4 v1, 0x7

    const/4 v2, 0x0

    move-object/from16 v3, p0

    .line 764
    :try_start_2
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v4, v28

    const/4 v1, 0x7

    const/4 v2, 0x0

    move-object/from16 v3, p0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v4, v11

    move-object v3, v15

    const/4 v1, 0x7

    const/4 v2, 0x0

    .line 768
    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 769
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v17

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v18

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v19

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v20

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v21

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v22

    invoke-virtual {v3, v4, v5, v1}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 770
    throw v0
.end method

.method public bridge synthetic addLineSpace(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addLineSpace(I)V

    return-void
.end method

.method public bridge synthetic addLogo(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addLogo(II)V

    return-void
.end method

.method public bridge synthetic addPageArea(IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2, p3, p4}, Lcom/epson/epos2/printer/CommonPrinter;->addPageArea(IIII)V

    return-void
.end method

.method public bridge synthetic addPageBegin()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->addPageBegin()V

    return-void
.end method

.method public bridge synthetic addPageDirection(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addPageDirection(I)V

    return-void
.end method

.method public bridge synthetic addPageEnd()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->addPageEnd()V

    return-void
.end method

.method public bridge synthetic addPageLine(IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super/range {p0 .. p5}, Lcom/epson/epos2/printer/CommonPrinter;->addPageLine(IIIII)V

    return-void
.end method

.method public bridge synthetic addPagePosition(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addPagePosition(II)V

    return-void
.end method

.method public bridge synthetic addPageRectangle(IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super/range {p0 .. p5}, Lcom/epson/epos2/printer/CommonPrinter;->addPageRectangle(IIIII)V

    return-void
.end method

.method public bridge synthetic addPulse(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addPulse(II)V

    return-void
.end method

.method public addSound(III)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    .line 670
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x0

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x2

    aput-object v1, v0, v13

    const-string v14, "addSound"

    invoke-virtual {v9, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 674
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 676
    iget-wide v2, v9, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v15, p2

    int-to-long v5, v15

    move/from16 v7, p3

    int-to-long v0, v7

    move-wide/from16 v16, v0

    move-object/from16 v1, p0

    move/from16 v4, p1

    move-wide/from16 v7, v16

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/epos2/printer/Printer;->nativeEpos2AddSound(JIJJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v10, [Ljava/lang/Object;

    .line 687
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-virtual {v9, v14, v11, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 678
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v15, p2

    .line 682
    :goto_0
    invoke-virtual {v9, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 683
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-virtual {v9, v14, v1, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 684
    throw v0
.end method

.method public bridge synthetic addSymbol(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super/range {p0 .. p6}, Lcom/epson/epos2/printer/CommonPrinter;->addSymbol(Ljava/lang/String;IIIII)V

    return-void
.end method

.method public bridge synthetic addText(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addText(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic addTextAlign(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextAlign(I)V

    return-void
.end method

.method public bridge synthetic addTextFont(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextFont(I)V

    return-void
.end method

.method public bridge synthetic addTextLang(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextLang(I)V

    return-void
.end method

.method public bridge synthetic addTextRotate(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextRotate(I)V

    return-void
.end method

.method public bridge synthetic addTextSize(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addTextSize(II)V

    return-void
.end method

.method public bridge synthetic addTextSmooth(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextSmooth(I)V

    return-void
.end method

.method public bridge synthetic addTextStyle(IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2, p3, p4}, Lcom/epson/epos2/printer/CommonPrinter;->addTextStyle(IIII)V

    return-void
.end method

.method public addVLineBegin(II[I)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object v8, p0

    const/4 v9, 0x3

    new-array v0, v9, [Ljava/lang/Object;

    .line 593
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x0

    aput-object v1, v0, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x1

    aput-object v1, v0, v11

    const/4 v12, 0x2

    aput-object p3, v0, v12

    const-string v13, "addVLineBegin"

    invoke-virtual {p0, v13, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 599
    iget-wide v2, v8, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v14, p1

    int-to-long v4, v14

    move-object v1, p0

    move/from16 v6, p2

    move-object/from16 v7, p3

    :try_start_1
    invoke-virtual/range {v1 .. v7}, Lcom/epson/epos2/printer/Printer;->nativeEpos2AddVLineBegin(JJI[I)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v9, [Ljava/lang/Object;

    .line 610
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    aput-object p3, v0, v12

    invoke-virtual {p0, v13, v10, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 601
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v14, p1

    .line 605
    :goto_0
    invoke-virtual {p0, v13, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 606
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    aput-object p3, v2, v12

    invoke-virtual {p0, v13, v1, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 607
    throw v0
.end method

.method public addVLineEnd(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 630
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addVLineEnd"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 634
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 636
    iget-wide v4, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/Printer;->nativeEpos2AddVLineEnd(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 647
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 638
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 642
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 643
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 644
    throw v1
.end method

.method public bridge synthetic beginTransaction()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->beginTransaction()V

    return-void
.end method

.method public bridge synthetic clearCommandBuffer()V
    .locals 0

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->clearCommandBuffer()V

    return-void
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 346
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 355
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 357
    iget-wide v6, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/printer/Printer;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/Printer;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_0

    .line 362
    iget-wide v5, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    iget v1, p0, Lcom/epson/epos2/printer/Printer;->mInterval:I

    int-to-long v7, v1

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/epson/epos2/printer/Printer;->nativeEpos2SetInterval(JJ)I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 374
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 359
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 351
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 369
    :goto_0
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 370
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v5, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 371
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 394
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 398
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 400
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 411
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 402
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 406
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 407
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 408
    throw v1
.end method

.method public bridge synthetic endTransaction()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->endTransaction()V

    return-void
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 297
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 299
    iput-object v1, p0, Lcom/epson/epos2/printer/Printer;->mStatusChangeListener:Lcom/epson/epos2/printer/StatusChangeListener;

    .line 300
    iput-object v1, p0, Lcom/epson/epos2/printer/Printer;->mReceiveListener:Lcom/epson/epos2/printer/ReceiveListener;

    .line 301
    iput-object v1, p0, Lcom/epson/epos2/printer/Printer;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 304
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 306
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2Disconnect(J)I

    .line 307
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2ClearCommandBuffer(J)I

    .line 308
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2DestroyHandle(J)I

    .line 309
    iput-wide v5, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 316
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 313
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 314
    throw v0
.end method

.method public forceCommand([BI)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 929
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "forceCommand"

    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 933
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 935
    iget-wide v5, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/epos2/printer/Printer;->nativeEpos2ForceCommand(J[BI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 946
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 937
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 941
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 942
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v5, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 943
    throw v1
.end method

.method public bridge synthetic forcePulse(III)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1, p2, p3}, Lcom/epson/epos2/printer/CommonPrinter;->forcePulse(III)V

    return-void
.end method

.method public bridge synthetic forceRecover(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->forceRecover(I)V

    return-void
.end method

.method public bridge synthetic forceReset(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->forceReset(I)V

    return-void
.end method

.method public forceStopSound(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 889
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "forceStopSound"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 893
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 895
    iget-wide v4, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/Printer;->nativeEpos2ForceStopSound(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 906
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 897
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 901
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 902
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 903
    throw v1
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 1020
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1022
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1023
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1027
    :cond_0
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1029
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1032
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public bridge synthetic getInterval()I
    .locals 1

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getInterval()I

    move-result v0

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 1047
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1049
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1050
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1054
    :cond_0
    iget-wide v3, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1056
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1059
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method protected getLogoList()[Lcom/epson/epos2/printer/LogoKeyCode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaintenanceCounter(IILcom/epson/epos2/printer/MaintenanceCounterListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    move/from16 v12, p2

    const/4 v13, 0x2

    new-array v0, v13, [Ljava/lang/Object;

    .line 1100
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x0

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x1

    aput-object v1, v0, v15

    const-string v1, "getMaintenanceCounter"

    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1103
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v0, -0x2

    move/from16 v1, p1

    if-ne v1, v0, :cond_0

    const/16 v0, 0x2710

    const/16 v10, 0x2710

    goto :goto_0

    :cond_0
    move v10, v1

    :goto_0
    const/16 v0, 0x1388

    if-lt v10, v0, :cond_4

    const v0, 0xea60

    if-lt v0, v10, :cond_4

    if-eqz p3, :cond_3

    if-nez v12, :cond_1

    goto :goto_1

    :cond_1
    if-ne v12, v15, :cond_2

    .line 1129
    :goto_1
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1131
    :try_start_2
    new-instance v0, Lcom/epson/epos2/printer/Printer$1;

    iget-wide v5, v11, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v9, 0x0

    const/16 v16, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object/from16 v4, p3

    move v7, v10

    move/from16 v8, p2

    move/from16 v17, v10

    move-object/from16 v10, v16

    :try_start_3
    invoke-direct/range {v1 .. v10}, Lcom/epson/epos2/printer/Printer$1;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    .line 1147
    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer$InnerThread;->start()V

    .line 1148
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    new-array v0, v13, [Ljava/lang/Object;

    .line 1155
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    const-string v1, "getMaintenanceCounter"

    invoke-virtual {v11, v1, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    move/from16 v17, v10

    .line 1148
    :goto_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    move/from16 v17, v10

    goto :goto_3

    :cond_2
    move/from16 v17, v10

    .line 1126
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_3
    move/from16 v17, v10

    .line 1117
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_4
    move/from16 v17, v10

    .line 1113
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_5
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    move/from16 v1, p1

    move/from16 v17, v1

    :goto_3
    const-string v1, "getMaintenanceCounter"

    .line 1151
    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1152
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v15

    const-string v3, "getMaintenanceCounter"

    invoke-virtual {v11, v3, v1, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1153
    throw v0
.end method

.method public getPrinterSetting(IILcom/epson/epos2/printer/PrinterSettingListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    move/from16 v12, p2

    const/4 v13, 0x2

    new-array v0, v13, [Ljava/lang/Object;

    .line 1313
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x0

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x1

    aput-object v1, v0, v15

    const-string v1, "getPrinterSetting"

    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1317
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v0, -0x2

    move/from16 v1, p1

    if-ne v1, v0, :cond_0

    const/16 v0, 0x2710

    const/16 v10, 0x2710

    goto :goto_0

    :cond_0
    move v10, v1

    :goto_0
    const/16 v0, 0x1388

    if-lt v10, v0, :cond_5

    const v0, 0xea60

    if-lt v0, v10, :cond_5

    if-eqz p3, :cond_4

    if-nez v12, :cond_1

    goto :goto_1

    :cond_1
    if-ne v12, v15, :cond_2

    goto :goto_1

    :cond_2
    if-ne v12, v13, :cond_3

    .line 1346
    :goto_1
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1348
    :try_start_2
    new-instance v0, Lcom/epson/epos2/printer/Printer$3;

    iget-wide v5, v11, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v9, 0x0

    const/16 v16, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object/from16 v4, p3

    move v7, v10

    move/from16 v8, p2

    move/from16 v17, v10

    move-object/from16 v10, v16

    :try_start_3
    invoke-direct/range {v1 .. v10}, Lcom/epson/epos2/printer/Printer$3;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    .line 1363
    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer$InnerThread;->start()V

    .line 1364
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    new-array v0, v13, [Ljava/lang/Object;

    .line 1372
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    const-string v1, "getPrinterSetting"

    invoke-virtual {v11, v1, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    move/from16 v17, v10

    .line 1364
    :goto_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    move/from16 v17, v10

    goto :goto_3

    :cond_3
    move/from16 v17, v10

    .line 1343
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_4
    move/from16 v17, v10

    .line 1331
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_5
    move/from16 v17, v10

    .line 1327
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_5
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    move/from16 v1, p1

    move/from16 v17, v1

    :goto_3
    const-string v1, "getPrinterSetting"

    .line 1367
    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1368
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v15

    const-string v3, "getPrinterSetting"

    invoke-virtual {v11, v3, v1, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1369
    throw v0
.end method

.method public getStatus()Lcom/epson/epos2/printer/PrinterStatusInfo;
    .locals 18

    move-object/from16 v0, p0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "getStatus"

    .line 422
    invoke-virtual {v0, v3, v2}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 424
    iget-wide v4, v0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {v0, v4, v5}, Lcom/epson/epos2/printer/Printer;->nativeEpos2GetStatus(J)Lcom/epson/epos2/printer/PrinterStatusInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 426
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getConnection()I

    move-result v4

    .line 427
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getOnline()I

    move-result v5

    .line 428
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getCoverOpen()I

    move-result v6

    .line 429
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPaper()I

    move-result v7

    .line 430
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPaperFeed()I

    move-result v8

    .line 431
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getPanelSwitch()I

    move-result v9

    .line 432
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getOnline()I

    move-result v10

    .line 433
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getDrawer()I

    move-result v11

    .line 434
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getErrorStatus()I

    move-result v12

    .line 435
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getAutoRecoverError()I

    move-result v13

    .line 436
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getBuzzer()I

    move-result v14

    .line 437
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getAdapter()I

    move-result v15

    .line 438
    invoke-virtual {v2}, Lcom/epson/epos2/printer/PrinterStatusInfo;->getBatteryLevel()I

    move-result v1

    move-object/from16 v16, v2

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/Object;

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v17, v3

    const-string v3, "connection->"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "online->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "coverOpen->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paper->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paperFeed->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "panelSwitch->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "waitOnline->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawer->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "errorStatus->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "autoRecoverError->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "buzzer->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xb

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adapter->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "batteryLevel->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    move-object/from16 v16, v2

    move-object v1, v3

    const/4 v3, 0x0

    const/16 v2, 0x101

    new-array v3, v3, [Ljava/lang/Object;

    .line 445
    invoke-virtual {v0, v1, v2, v3}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v16
.end method

.method protected native nativeEpos2AddFeedPosition(JI)I
.end method

.method protected native nativeEpos2AddHLine(JJJI)I
.end method

.method protected native nativeEpos2AddLayout(JIJJJJJJ)I
.end method

.method protected native nativeEpos2AddSound(JIJJ)I
.end method

.method protected native nativeEpos2AddVLineBegin(JJI[I)I
.end method

.method protected native nativeEpos2AddVLineEnd(JI)I
.end method

.method protected native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method protected native nativeEpos2CreateHandle(II[J)I
.end method

.method protected native nativeEpos2DestroyHandle(J)I
.end method

.method protected native nativeEpos2Disconnect(J)I
.end method

.method protected native nativeEpos2ForceCommand(J[BI)I
.end method

.method protected native nativeEpos2ForceStopSound(JI)I
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLogoList(JLjava/util/Vector;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Vector<",
            "Lcom/epson/epos2/printer/LogoKeyCode;",
            ">;)I"
        }
    .end annotation
.end method

.method protected native nativeEpos2GetMaintenanceCounter(JII[I)I
.end method

.method protected native nativeEpos2GetPrinterSetting(JII[I)I
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/printer/PrinterStatusInfo;
.end method

.method protected native nativeEpos2RegisterLogo(JJJ[BJJJJJJIIID)I
.end method

.method protected native nativeEpos2RequestPrintJobStatus(JLjava/lang/String;)I
.end method

.method protected native nativeEpos2ResetMaintenanceCounter(JII)I
.end method

.method protected native nativeEpos2SendData(JJLjava/lang/String;I)I
.end method

.method protected native nativeEpos2SetPrinterSetting(JI[I[I)I
.end method

.method protected native nativeEpos2UnregisterAllLogo(J)I
.end method

.method protected native nativeEpos2UnregisterLogo(JJJ)I
.end method

.method protected registerLogo(IILandroid/graphics/Bitmap;IIIIIIID)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    return-void
.end method

.method public requestPrintJobStatus(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "getPrintJobStatus"

    .line 510
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 514
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 516
    invoke-direct {p0, p1}, Lcom/epson/epos2/printer/Printer;->checkPrintJobIdFormat(Ljava/lang/String;)V

    .line 519
    iget-wide v4, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/Printer;->nativeEpos2RequestPrintJobStatus(JLjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 530
    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 521
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 525
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 526
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-virtual {p0, v3, v4, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 527
    throw v1
.end method

.method public resetMaintenanceCounter(IILcom/epson/epos2/printer/MaintenanceCounterListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    move/from16 v12, p2

    const/4 v13, 0x2

    new-array v0, v13, [Ljava/lang/Object;

    .line 1193
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x0

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x1

    aput-object v1, v0, v15

    const-string v1, "resetMaintenanceCounter"

    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1197
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v0, -0x2

    move/from16 v1, p1

    if-ne v1, v0, :cond_0

    const/16 v0, 0x2710

    const/16 v10, 0x2710

    goto :goto_0

    :cond_0
    move v10, v1

    :goto_0
    const/16 v0, 0x1388

    if-lt v10, v0, :cond_4

    const v0, 0xea60

    if-lt v0, v10, :cond_4

    if-eqz p3, :cond_3

    if-nez v12, :cond_1

    goto :goto_1

    :cond_1
    if-ne v12, v15, :cond_2

    .line 1223
    :goto_1
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1225
    :try_start_2
    new-instance v0, Lcom/epson/epos2/printer/Printer$2;

    iget-wide v5, v11, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v9, 0x0

    const/16 v16, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object/from16 v4, p3

    move v7, v10

    move/from16 v8, p2

    move/from16 v17, v10

    move-object/from16 v10, v16

    :try_start_3
    invoke-direct/range {v1 .. v10}, Lcom/epson/epos2/printer/Printer$2;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    .line 1237
    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer$InnerThread;->start()V

    .line 1238
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    new-array v0, v13, [Ljava/lang/Object;

    .line 1245
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    const-string v1, "resetMaintenanceCounter"

    invoke-virtual {v11, v1, v14, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    move/from16 v17, v10

    .line 1238
    :goto_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    move/from16 v17, v10

    goto :goto_3

    :cond_2
    move/from16 v17, v10

    .line 1220
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_3
    move/from16 v17, v10

    .line 1211
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_4
    move/from16 v17, v10

    .line 1207
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v15}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_5
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    move/from16 v1, p1

    move/from16 v17, v1

    :goto_3
    const-string v1, "resetMaintenanceCounter"

    .line 1241
    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1242
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v15

    const-string v3, "resetMaintenanceCounter"

    invoke-virtual {v11, v3, v1, v2}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1243
    throw v0
.end method

.method public sendData(I)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 469
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "sendData"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    .line 475
    iget-wide v5, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    int-to-long v7, p1

    const-string v9, ""

    const/4 v10, 0x0

    move-object v4, p0

    invoke-virtual/range {v4 .. v10}, Lcom/epson/epos2/printer/Printer;->nativeEpos2SendData(JJLjava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 486
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 477
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 481
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 482
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 483
    throw v1
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 997
    invoke-virtual {p0, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 999
    iget-wide v0, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1004
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1007
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :goto_0
    return-void
.end method

.method public bridge synthetic setInterval(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->setInterval(I)V

    return-void
.end method

.method public setPrinterSetting(ILjava/util/Map;Lcom/epson/epos2/printer/PrinterSettingListener;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/epson/epos2/printer/PrinterSettingListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    const/4 v12, 0x2

    new-array v0, v12, [Ljava/lang/Object;

    .line 1442
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x0

    aput-object v1, v0, v13

    const/4 v14, 0x1

    aput-object p2, v0, v14

    const-string v1, "setPrinterSetting"

    invoke-virtual {v11, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1445
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/Printer;->checkHandle()V

    if-eqz p2, :cond_6

    .line 1454
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_5

    const/4 v1, 0x3

    if-gt v0, v1, :cond_4

    .line 1461
    new-array v9, v0, [I

    .line 1462
    new-array v10, v0, [I

    .line 1465
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1466
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v9, v1

    .line 1467
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v10, v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, -0x2

    move/from16 v1, p1

    if-ne v1, v0, :cond_1

    const/16 v0, 0x2710

    const/16 v15, 0x2710

    goto :goto_1

    :cond_1
    move v15, v1

    :goto_1
    const/16 v0, 0x1388

    if-lt v15, v0, :cond_3

    const v0, 0xea60

    if-lt v0, v15, :cond_3

    if-eqz p3, :cond_2

    .line 1483
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1485
    :try_start_2
    new-instance v0, Lcom/epson/epos2/printer/Printer$4;

    iget-wide v5, v11, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const/4 v8, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object/from16 v4, p3

    move v7, v15

    invoke-direct/range {v1 .. v10}, Lcom/epson/epos2/printer/Printer$4;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    .line 1496
    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer$InnerThread;->start()V

    .line 1497
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-array v0, v12, [Ljava/lang/Object;

    .line 1504
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    aput-object p2, v0, v14

    const-string v1, "setPrinterSetting"

    invoke-virtual {v11, v1, v13, v0}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 1497
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 1480
    :cond_2
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v14}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    .line 1476
    :cond_3
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v14}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_4
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    move v1, v15

    goto :goto_2

    :cond_4
    move/from16 v1, p1

    .line 1459
    :try_start_5
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v14}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_5
    move/from16 v1, p1

    .line 1456
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v14}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    :cond_6
    move/from16 v1, p1

    .line 1451
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v14}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_5
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    move/from16 v1, p1

    :goto_2
    const-string v2, "getPrinterSetting"

    .line 1500
    invoke-virtual {v11, v2, v0}, Lcom/epson/epos2/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1501
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v3, v12, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v13

    aput-object p2, v3, v14

    const-string v1, "setPrinterSetting"

    invoke-virtual {v11, v1, v2, v3}, Lcom/epson/epos2/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1502
    throw v0
.end method

.method public setReceiveEventListener(Lcom/epson/epos2/printer/ReceiveListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setReceiveEventListener"

    .line 976
    invoke-virtual {p0, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 978
    iget-wide v0, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 983
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer;->mReceiveListener:Lcom/epson/epos2/printer/ReceiveListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 986
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer;->mReceiveListener:Lcom/epson/epos2/printer/ReceiveListener;

    :goto_0
    return-void
.end method

.method public setStatusChangeEventListener(Lcom/epson/epos2/printer/StatusChangeListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setStatusChangeEventListener"

    .line 956
    invoke-virtual {p0, v1, v0}, Lcom/epson/epos2/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 958
    iget-wide v0, p0, Lcom/epson/epos2/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 963
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer;->mStatusChangeListener:Lcom/epson/epos2/printer/StatusChangeListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 966
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer;->mStatusChangeListener:Lcom/epson/epos2/printer/StatusChangeListener;

    :goto_0
    return-void
.end method

.method public bridge synthetic startMonitor()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->startMonitor()V

    return-void
.end method

.method public bridge synthetic stopMonitor()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 14
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->stopMonitor()V

    return-void
.end method

.method protected unregisterAllLogo()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    return-void
.end method

.method protected unregisterLogo(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    return-void
.end method
