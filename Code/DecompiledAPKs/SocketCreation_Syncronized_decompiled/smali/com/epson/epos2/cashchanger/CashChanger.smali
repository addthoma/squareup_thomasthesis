.class public Lcom/epson/epos2/cashchanger/CashChanger;
.super Ljava/lang/Object;
.source "CashChanger.java"


# static fields
.field public static final COLLECT_ALL_CASH:I = 0x0

.field public static final COLLECT_PART_OF_CASH:I = 0x1

.field public static final COUNT_MODE_AUTO_COUNT:I = 0x1

.field public static final COUNT_MODE_MANUAL_INPUT:I = 0x0

.field public static final DEPOSIT_CHANGE:I = 0x0

.field public static final DEPOSIT_NOCHANGE:I = 0x1

.field public static final DEPOSIT_REPAY:I = 0x2

.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final STATUS_BUSY:I = 0x0

.field public static final STATUS_END:I = 0x2

.field public static final STATUS_ERR:I = 0x3

.field public static final STATUS_PAUSE:I = 0x1

.field public static final ST_EMPTY:I = 0x0

.field public static final ST_FULL:I = 0x4

.field public static final ST_NEAR_EMPTY:I = 0x1

.field public static final ST_NEAR_FULL:I = 0x3

.field public static final ST_OK:I = 0x2

.field public static final SUE_POWER_OFF:I = 0x7d2

.field public static final SUE_POWER_OFFLINE:I = 0x7d3

.field public static final SUE_POWER_OFF_OFFLINE:I = 0x7d4

.field public static final SUE_POWER_ONLINE:I = 0x7d1

.field public static final SUE_STATUS_EMPTY:I = 0xb

.field public static final SUE_STATUS_EMPTYOK:I = 0xd

.field public static final SUE_STATUS_FULL:I = 0x15

.field public static final SUE_STATUS_FULLOK:I = 0x17

.field public static final SUE_STATUS_JAM:I = 0x1f

.field public static final SUE_STATUS_JAMOK:I = 0x20

.field public static final SUE_STATUS_NEAREMPTY:I = 0xc

.field public static final SUE_STATUS_NEARFULL:I = 0x16

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mCashChangerHandle:J

.field private mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

.field private mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

.field private mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

.field private mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

.field private mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

.field private mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

.field private mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 20
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 248
    sput v0, Lcom/epson/epos2/cashchanger/CashChanger;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 196
    iput-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const/4 v0, 0x0

    .line 197
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mContext:Landroid/content/Context;

    .line 229
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

    .line 230
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

    .line 231
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

    .line 232
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

    .line 233
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

    .line 234
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;

    .line 235
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

    .line 236
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

    .line 237
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

    .line 238
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

    .line 239
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 241
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    .line 242
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 243
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 244
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 245
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 246
    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 259
    invoke-direct {p0, p1}, Lcom/epson/epos2/cashchanger/CashChanger;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "CashChanger"

    .line 260
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 263
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 264
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 265
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    .line 266
    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 269
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 270
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 273
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mContext:Landroid/content/Context;

    .line 274
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->initializeCashChangerInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 275
    invoke-direct {p0, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 1650
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    .line 1652
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 1653
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1654
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 1655
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1656
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 1657
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1658
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 1659
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1660
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 1661
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1663
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1666
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onCChangerCashCount(I[Ljava/lang/String;[II)V
    .locals 11

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Object;

    .line 1382
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v4, 0x2

    aput-object p3, v1, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x3

    aput-object v5, v1, v6

    const/4 v5, 0x4

    aput-object p0, v1, v5

    const-string v7, "onCChangerCashCount"

    invoke-direct {p0, v7, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p4, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    .line 1392
    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v8, 0x0

    :goto_0
    if-ge v8, p4, :cond_1

    .line 1395
    aget-object v9, p2, v8

    aget v10, p3, v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v1, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1399
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

    if-eqz v8, :cond_2

    const/16 v8, 0x9

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "code->"

    aput-object v9, v8, v3

    .line 1401
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    const-string v9, "key->"

    aput-object v9, v8, v4

    aput-object p2, v8, v6

    const-string p2, "value->"

    aput-object p2, v8, v5

    aput-object p3, v8, v0

    const/4 p2, 0x6

    const-string p3, "dataSize->"

    aput-object p3, v8, p2

    const/4 p2, 0x7

    .line 1404
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v8, p2

    const/16 p2, 0x8

    aput-object p0, v8, p2

    .line 1400
    invoke-direct {p0, v7, v8}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1406
    iget-object p2, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

    invoke-interface {p2, p0, p1, v1}, Lcom/epson/epos2/cashchanger/CashCountListener;->onCChangerCashCount(Lcom/epson/epos2/cashchanger/CashChanger;ILjava/util/Map;)V

    new-array p2, v6, [Ljava/lang/Object;

    .line 1408
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p2, v3

    aput-object v1, p2, v2

    aput-object p0, p2, v4

    invoke-direct {p0, v7, v3, p2}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method

.method private onCChangerCollect(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1557
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onCChangerCollect"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1558
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "code->"

    aput-object v5, v1, v3

    .line 1560
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    aput-object p0, v1, v0

    .line 1559
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1562
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/cashchanger/CollectListener;->onCChangerCollect(Lcom/epson/epos2/cashchanger/CashChanger;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1564
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCChangerCommandReply(I[B)V
    .locals 7

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 1574
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v4, 0x2

    aput-object p0, v1, v4

    const-string v5, "onCChangerCommandReply"

    invoke-direct {p0, v5, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1575
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v6, "code->"

    aput-object v6, v1, v3

    .line 1577
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    const-string v6, "data->"

    aput-object v6, v1, v4

    aput-object p2, v1, v0

    const/4 v6, 0x4

    aput-object p0, v1, v6

    .line 1576
    invoke-direct {p0, v5, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1580
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

    invoke-interface {v1, p0, p1, p2}, Lcom/epson/epos2/cashchanger/CommandReplyListener;->onCChangerCommandReply(Lcom/epson/epos2/cashchanger/CashChanger;I[B)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1582
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p2, v0, v2

    aput-object p0, v0, v4

    invoke-direct {p0, v5, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCChangerConfigChange(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1541
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onCChangerConfigChange"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1542
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "code->"

    aput-object v5, v1, v3

    .line 1544
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    aput-object p0, v1, v0

    .line 1543
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1546
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/cashchanger/ConfigChangeListener;->onCChangerConfigChange(Lcom/epson/epos2/cashchanger/CashChanger;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1548
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCChangerDeposit(III[Ljava/lang/String;[II)V
    .locals 15

    move-object v6, p0

    move/from16 v0, p6

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Object;

    .line 1427
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v7, 0x0

    aput-object v3, v2, v7

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v8, 0x1

    aput-object v3, v2, v8

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v9, 0x2

    aput-object v3, v2, v9

    const/4 v10, 0x3

    aput-object p4, v2, v10

    const/4 v11, 0x4

    aput-object p5, v2, v11

    const/4 v12, 0x5

    aput-object v6, v2, v12

    const-string v13, "onCChangerDeposit"

    invoke-direct {p0, v13, v2}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v14, v0

    goto :goto_1

    .line 1437
    :cond_0
    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 1440
    aget-object v4, p4, v3

    aget v5, p5, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object v14, v2

    .line 1444
    :goto_1
    iget-object v0, v6, Lcom/epson/epos2/cashchanger/CashChanger;->mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

    if-eqz v0, :cond_2

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "code->"

    aput-object v2, v0, v7

    .line 1446
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v8

    const-string v2, "status->"

    aput-object v2, v0, v9

    .line 1447
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v10

    const-string v2, "amount->"

    aput-object v2, v0, v11

    .line 1448
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v12

    const-string v2, "key->"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    const-string v2, "value->"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    aput-object p5, v0, v1

    const/16 v1, 0xa

    aput-object v6, v0, v1

    .line 1445
    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1452
    iget-object v0, v6, Lcom/epson/epos2/cashchanger/CashChanger;->mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object v5, v14

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cashchanger/DepositListener;->onCChangerDeposit(Lcom/epson/epos2/cashchanger/CashChanger;IIILjava/util/Map;)V

    new-array v0, v12, [Ljava/lang/Object;

    .line 1454
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    aput-object v14, v0, v10

    aput-object v6, v0, v11

    invoke-direct {p0, v13, v7, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method

.method private onCChangerDirectIO(IILjava/lang/String;)V
    .locals 8

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    .line 1508
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v5, 0x3

    aput-object p0, v1, v5

    const-string v6, "onCChangerDirectIO"

    invoke-direct {p0, v6, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1509
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v7, "eventNumber->"

    aput-object v7, v1, v3

    .line 1511
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v4

    const-string v7, "data->"

    aput-object v7, v1, v2

    .line 1512
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v5

    const-string v7, "string->"

    aput-object v7, v1, v0

    const/4 v7, 0x5

    aput-object p3, v1, v7

    const/4 v7, 0x6

    aput-object p0, v1, v7

    .line 1510
    invoke-direct {p0, v6, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1515
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

    invoke-interface {v1, p0, p1, p2, p3}, Lcom/epson/epos2/cashchanger/DirectIOListener;->onCChangerDirectIO(Lcom/epson/epos2/cashchanger/CashChanger;IILjava/lang/String;)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1517
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    aput-object p3, v0, v2

    aput-object p0, v0, v5

    invoke-direct {p0, v6, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCChangerDirectIOCommandReply(IIILjava/lang/String;)V
    .locals 14

    move-object v6, p0

    const/4 v7, 0x5

    new-array v0, v7, [Ljava/lang/Object;

    .line 1487
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v9, 0x1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x2

    aput-object v1, v0, v10

    const/4 v11, 0x3

    aput-object p4, v0, v11

    const/4 v12, 0x4

    aput-object v6, v0, v12

    const-string v13, "onCChangerDirectIOCommandReply"

    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1488
    iget-object v0, v6, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "code->"

    aput-object v1, v0, v8

    .line 1490
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "command->"

    aput-object v1, v0, v10

    .line 1491
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    const-string v1, "data->"

    aput-object v1, v0, v12

    .line 1492
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "string->"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    aput-object v6, v0, v1

    .line 1489
    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1495
    iget-object v0, v6, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;->onCChangerDirectIOCommandReply(Lcom/epson/epos2/cashchanger/CashChanger;IIILjava/lang/String;)V

    :cond_0
    new-array v0, v7, [Ljava/lang/Object;

    .line 1497
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    aput-object p4, v0, v11

    aput-object v6, v0, v12

    invoke-direct {p0, v13, v8, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCChangerDispense(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1468
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onCChangerDispense"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1469
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "code->"

    aput-object v5, v1, v3

    .line 1471
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    aput-object p0, v1, v0

    .line 1470
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1473
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/cashchanger/DispenseListener;->onCChangerDispense(Lcom/epson/epos2/cashchanger/CashChanger;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1475
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCChangerStatusChange(I[Ljava/lang/String;[II)V
    .locals 11

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Object;

    .line 1594
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v4, 0x2

    aput-object p3, v1, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x3

    aput-object v5, v1, v6

    const/4 v5, 0x4

    aput-object p0, v1, v5

    const-string v7, "onCChangerStatusChange"

    invoke-direct {p0, v7, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p4, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    .line 1604
    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v8, 0x0

    :goto_0
    if-ge v8, p4, :cond_1

    .line 1607
    aget-object v9, p2, v8

    aget v10, p3, v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v1, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1611
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

    if-eqz v8, :cond_2

    const/16 v8, 0x9

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "code->"

    aput-object v9, v8, v3

    .line 1613
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    const-string v9, "key->"

    aput-object v9, v8, v4

    aput-object p2, v8, v6

    const-string v9, "status->"

    aput-object v9, v8, v5

    aput-object p3, v8, v0

    const/4 v9, 0x6

    const-string v10, "statusSize->"

    aput-object v10, v8, v9

    const/4 v9, 0x7

    .line 1616
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/16 v9, 0x8

    aput-object p0, v8, v9

    .line 1612
    invoke-direct {p0, v7, v8}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1618
    iget-object v8, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

    invoke-interface {v8, p0, p1, v1}, Lcom/epson/epos2/cashchanger/StatusChangeListener;->onCChangerStatusChange(Lcom/epson/epos2/cashchanger/CashChanger;ILjava/util/Map;)V

    new-array v0, v0, [Ljava/lang/Object;

    .line 1620
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p2, v0, v2

    aput-object p3, v0, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    aput-object p0, v0, v5

    invoke-direct {p0, v7, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method

.method private onCChangerStatusUpdate(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1526
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onCChangerStatusUpdate"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1527
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "status->"

    aput-object v5, v1, v3

    .line 1529
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    aput-object p0, v1, v0

    .line 1528
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1531
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/cashchanger/StatusUpdateListener;->onCChangerStatusUpdate(Lcom/epson/epos2/cashchanger/CashChanger;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1533
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1633
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1634
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 1635
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1636
    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1638
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 1690
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 1672
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 1699
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 1681
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public beginDeposit()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "beginDeposit"

    .line 524
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 527
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 529
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2BeginDeposit(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 540
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 531
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 535
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 536
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 537
    throw v1
.end method

.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 336
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 337
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public collectCash(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 876
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "collectCash"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 879
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 881
    iget-wide v4, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2CollectCash(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 892
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 883
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 887
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 888
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 889
    throw v1
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 377
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 383
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 385
    iget-wide v6, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 396
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 387
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 381
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 391
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 392
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 393
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 411
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 414
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 416
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 427
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 418
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 422
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 423
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 424
    throw v1
.end method

.method public dispenseCash(Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "dispenseCash"

    .line 704
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 707
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    if-eqz p1, :cond_3

    .line 714
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v9

    if-eqz v9, :cond_2

    .line 720
    new-array v7, v9, [Ljava/lang/String;

    .line 721
    new-array v8, v9, [J

    .line 723
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 724
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    aput-object v6, v7, v4

    .line 725
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v5, v5

    aput-wide v5, v8, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 729
    :cond_0
    iget-wide v5, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    move-object v4, p0

    invoke-virtual/range {v4 .. v9}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2DispenseCash(J[Ljava/lang/String;[JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 740
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 731
    :cond_1
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4

    .line 716
    :cond_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1

    .line 710
    :cond_3
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 735
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 736
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 737
    throw v1
.end method

.method public dispenseChange(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 658
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "dispenseChange"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 661
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 663
    iget-wide v4, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2DispenseChange(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 674
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 665
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 669
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 670
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 671
    throw v1
.end method

.method public endDeposit(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 623
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "endDeposit"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 626
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 628
    iget-wide v4, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2EndDeposit(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 639
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 630
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 634
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 635
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 636
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 301
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 303
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

    .line 304
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

    .line 305
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

    .line 306
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

    .line 307
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

    .line 308
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;

    .line 309
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

    .line 310
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

    .line 311
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

    .line 312
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

    .line 313
    iput-object v1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 316
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 317
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2Disconnect(J)I

    .line 318
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2DestroyHandle(J)I

    .line 319
    iput-wide v5, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 326
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 323
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 324
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 978
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 980
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 981
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 985
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 987
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 990
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 1008
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1010
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1011
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1015
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1017
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1020
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getOposErrorCode()I
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getOposErrorCode"

    .line 468
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    return v0

    .line 474
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2GetOposErrorCode(J)I

    move-result v1

    new-array v0, v0, [Ljava/lang/Object;

    .line 475
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return v1
.end method

.method public getStatus()Lcom/epson/epos2/cashchanger/CashChangerStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 443
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2GetStatus(J)Lcom/epson/epos2/cashchanger/CashChangerStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 447
    invoke-virtual {v1}, Lcom/epson/epos2/cashchanger/CashChangerStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/cashchanger/CashChanger;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 449
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/cashchanger/CashChanger;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 452
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializeCashChangerInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 285
    invoke-direct {p0, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 289
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 287
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2BeginDeposit(J)I
.end method

.method protected native nativeEpos2CollectCash(JI)I
.end method

.method protected native nativeEpos2DispenseCash(J[Ljava/lang/String;[JI)I
.end method

.method protected native nativeEpos2DispenseChange(JJ)I
.end method

.method protected native nativeEpos2EndDeposit(JI)I
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetOposErrorCode(J)I
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/cashchanger/CashChangerStatusInfo;
.end method

.method protected native nativeEpos2OpenDrawer(J)I
.end method

.method protected native nativeEpos2PauseDeposit(J)I
.end method

.method protected native nativeEpos2ReadCashCount(J)I
.end method

.method protected native nativeEpos2RestartDeposit(J)I
.end method

.method protected native nativeEpos2SendCommand(J[B)I
.end method

.method protected native nativeEpos2SendDirectIOCommand(JJJLjava/lang/String;)I
.end method

.method protected native nativeEpos2SetConfigCountMode(JI)I
.end method

.method protected native nativeEpos2SetConfigLeftCash(JJJ)I
.end method

.method public openDrawer()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "openDrawer"

    .line 909
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 912
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 914
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2OpenDrawer(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 925
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 916
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 920
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 921
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 922
    throw v1
.end method

.method public pauseDeposit()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "pauseDeposit"

    .line 555
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 558
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 560
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2PauseDeposit(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 571
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 562
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 566
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 567
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 568
    throw v1
.end method

.method public readCashCount()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "readCashCount"

    .line 493
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 496
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 498
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2ReadCashCount(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 509
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 500
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 504
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 505
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 506
    throw v1
.end method

.method public restartDeposit()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "restartDeposit"

    .line 586
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 589
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 591
    iget-wide v3, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2RestartDeposit(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 602
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 593
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 597
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 598
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 599
    throw v1
.end method

.method public sendCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "sendCommand"

    .line 944
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 947
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 949
    iget-wide v4, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2SendCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 960
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 951
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 955
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 956
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 957
    throw v1
.end method

.method public sendDirectIOCommand(IILjava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    .line 760
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x0

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    const/4 v13, 0x2

    aput-object p3, v0, v13

    const-string v14, "sendDirectIOCommand"

    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 763
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 765
    iget-wide v2, v9, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v15, p1

    int-to-long v4, v15

    move/from16 v8, p2

    int-to-long v6, v8

    move-object/from16 v1, p0

    move-object/from16 v8, p3

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2SendDirectIOCommand(JJJLjava/lang/String;)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v10, [Ljava/lang/Object;

    .line 776
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    aput-object p3, v0, v13

    invoke-direct {v9, v14, v11, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 767
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v15, p1

    .line 771
    :goto_0
    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 772
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    aput-object p3, v2, v13

    invoke-direct {v9, v14, v1, v2}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 773
    throw v0
.end method

.method public setCashCountEventListener(Lcom/epson/epos2/cashchanger/CashCountListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setCashCountEventListener"

    .line 1041
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1043
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1049
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1052
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashCountListener:Lcom/epson/epos2/cashchanger/CashCountListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setCollectEventListener(Lcom/epson/epos2/cashchanger/CollectListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setCollectEventListener"

    .line 1271
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1273
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1279
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1282
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCollectListener:Lcom/epson/epos2/cashchanger/CollectListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setCommandReplyEventListener(Lcom/epson/epos2/cashchanger/CommandReplyListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setCommandReplyEventListener"

    .line 1302
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1304
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1310
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1313
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCommandReplyListener:Lcom/epson/epos2/cashchanger/CommandReplyListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setConfigChangeEventListener(Lcom/epson/epos2/cashchanger/ConfigChangeListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConfigChangeEventListener"

    .line 1240
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1242
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1248
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1251
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConfigChangeListener:Lcom/epson/epos2/cashchanger/ConfigChangeListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setConfigCountMode(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 799
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setConfigCountMode"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 802
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 804
    iget-wide v4, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2SetConfigCountMode(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 815
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 806
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 810
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 811
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 812
    throw v1
.end method

.method public setConfigLeftCash(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 838
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "setConfigLeftCash"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 841
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cashchanger/CashChanger;->checkHandle()V

    .line 843
    iget-wide v6, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/cashchanger/CashChanger;->nativeEpos2SetConfigLeftCash(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 854
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 845
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 849
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cashchanger/CashChanger;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 850
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v2, v5, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 851
    throw v1
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 1358
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1360
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1366
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1369
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setDepositEventListener(Lcom/epson/epos2/cashchanger/DepositListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setDepositEventListener"

    .line 1079
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1081
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1087
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1090
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDepositListener:Lcom/epson/epos2/cashchanger/DepositListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setDirectIOCommandReplyEventListener(Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setDirectIOCommandReplyEventListener"

    .line 1143
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1145
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1151
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1154
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cashchanger/DirectIOCommandReplyListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setDirectIOEventListener(Lcom/epson/epos2/cashchanger/DirectIOListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setDirectIOEventListener"

    .line 1174
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1176
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1182
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1185
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDirectIOListener:Lcom/epson/epos2/cashchanger/DirectIOListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setDispenseEventListener(Lcom/epson/epos2/cashchanger/DispenseListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setDispenseEventListener"

    .line 1111
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1113
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1119
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1122
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mDispenseListener:Lcom/epson/epos2/cashchanger/DispenseListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setStatusChangeEventListener(Lcom/epson/epos2/cashchanger/StatusChangeListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setStatusChangeEventListener"

    .line 1332
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1334
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1340
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1343
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusChangeListener:Lcom/epson/epos2/cashchanger/StatusChangeListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setStatusUpdateEventListener(Lcom/epson/epos2/cashchanger/StatusUpdateListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setStatusUpdateEventListener"

    .line 1206
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cashchanger/CashChanger;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1208
    iget-wide v0, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mCashChangerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1214
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1217
    iput-object p1, p0, Lcom/epson/epos2/cashchanger/CashChanger;->mStatusUpdateListener:Lcom/epson/epos2/cashchanger/StatusUpdateListener;

    :cond_2
    :goto_0
    return-void
.end method
