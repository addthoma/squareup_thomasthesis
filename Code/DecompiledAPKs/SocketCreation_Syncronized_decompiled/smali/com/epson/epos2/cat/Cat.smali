.class public Lcom/epson/epos2/cat/Cat;
.super Ljava/lang/Object;
.source "Cat.java"


# static fields
.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field public static final PAYMENT_CONDITION_BONUS_1:I = 0x1

.field public static final PAYMENT_CONDITION_BONUS_2:I = 0x2

.field public static final PAYMENT_CONDITION_BONUS_3:I = 0x3

.field public static final PAYMENT_CONDITION_COMBINATION_1:I = 0x7

.field public static final PAYMENT_CONDITION_COMBINATION_2:I = 0x8

.field public static final PAYMENT_CONDITION_DEBIT:I = 0x9

.field public static final PAYMENT_CONDITION_ELECTRONIC_MONEY:I = 0xa

.field public static final PAYMENT_CONDITION_INSTALLMENT_1:I = 0x4

.field public static final PAYMENT_CONDITION_INSTALLMENT_2:I = 0x5

.field public static final PAYMENT_CONDITION_LUMP_SUM:I = 0x0

.field public static final PAYMENT_CONDITION_OTHER:I = 0xb

.field public static final PAYMENT_CONDITION_REVOLVING:I = 0x6

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final SERVICE_COMMON:I = 0xa

.field public static final SERVICE_CREDIT:I = 0x0

.field public static final SERVICE_DEBIT:I = 0x1

.field public static final SERVICE_EDY:I = 0x3

.field public static final SERVICE_ID:I = 0x4

.field public static final SERVICE_NANACO:I = 0x5

.field public static final SERVICE_POINT:I = 0x9

.field public static final SERVICE_QUICPAY:I = 0x6

.field public static final SERVICE_SUICA:I = 0x7

.field public static final SERVICE_UNIONPAY:I = 0x2

.field public static final SERVICE_WAON:I = 0x8

.field public static final SUE_LOGSTATUS_FULL:I = 0x2

.field public static final SUE_LOGSTATUS_NEARFULL:I = 0x1

.field public static final SUE_LOGSTATUS_OK:I = 0x0

.field public static final SUE_POWER_OFF_OFFLINE:I = 0x7d4

.field public static final SUE_POWER_ONLINE:I = 0x7d1

.field private static final TIMEOUT_DEFAULT:I = 0x0

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

.field private mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

.field private mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

.field private mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

.field private mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

.field private mCatHandle:J

.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

.field private mTimeout:I

.field private mTrainingMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 22
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 207
    sput v0, Lcom/epson/epos2/cat/Cat;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 174
    iput-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const/4 v0, 0x0

    .line 175
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mContext:Landroid/content/Context;

    .line 198
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

    .line 199
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

    .line 200
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

    .line 201
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

    .line 202
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

    .line 203
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

    .line 204
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

    .line 205
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    const/4 v1, 0x0

    .line 210
    iput v1, p0, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    .line 212
    iput v1, p0, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    .line 214
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    .line 215
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 216
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 217
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 218
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 219
    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 290
    invoke-direct {p0, p1}, Lcom/epson/epos2/cat/Cat;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    aput-object p1, v2, v1

    const-string v3, "Cat"

    .line 291
    invoke-direct {p0, v3, v2}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v2, "com.epson.epos2.NativeInitializer"

    .line 294
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 295
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v1

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 296
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v1

    .line 297
    invoke-virtual {v4, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 300
    invoke-direct {p0, v3, v2}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 301
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 304
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mContext:Landroid/content/Context;

    .line 305
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->initializeCatInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    .line 306
    invoke-direct {p0, v3, v0}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 1242
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    .line 1244
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 1245
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1246
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 1247
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1248
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 1249
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1250
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 1251
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1252
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/cat/Cat;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 1253
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1255
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1258
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onCatAccessDailyLog(III[Lcom/epson/epos2/cat/DailyLog;I)V
    .locals 16

    move-object/from16 v6, p0

    move/from16 v7, p5

    const/4 v8, 0x6

    new-array v0, v8, [Ljava/lang/Object;

    .line 1090
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v9, 0x0

    aput-object v1, v0, v9

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x1

    aput-object v1, v0, v10

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x2

    aput-object v1, v0, v11

    const/4 v12, 0x3

    aput-object p4, v0, v12

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x4

    aput-object v1, v0, v13

    const/4 v14, 0x5

    aput-object v6, v0, v14

    const-string v15, "onCatAccessDailyLog"

    invoke-direct {v6, v15, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1093
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    .line 1096
    aget-object v1, p4, v0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1100
    :cond_0
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "code->"

    aput-object v1, v0, v9

    .line 1102
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    const-string v1, "sequence->"

    aput-object v1, v0, v11

    .line 1103
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    const-string v1, "service->"

    aput-object v1, v0, v13

    .line 1104
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    const-string v1, "dailyLogArrayList->"

    aput-object v1, v0, v8

    const/4 v1, 0x7

    aput-object v5, v0, v1

    const/16 v1, 0x8

    aput-object v6, v0, v1

    .line 1101
    invoke-direct {v6, v15, v0}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1107
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cat/AccessDailyLogListener;->onCatAccessDailyLog(Lcom/epson/epos2/cat/Cat;IIILjava/util/ArrayList;)V

    :cond_1
    new-array v0, v8, [Ljava/lang/Object;

    .line 1109
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    aput-object p4, v0, v12

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    aput-object v6, v0, v14

    invoke-direct {v6, v15, v9, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private onCatAuthorizeCompletion(IIILcom/epson/epos2/cat/AuthorizeResult;)V
    .locals 14

    move-object v6, p0

    const/4 v7, 0x5

    new-array v0, v7, [Ljava/lang/Object;

    .line 1070
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v9, 0x1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x2

    aput-object v1, v0, v10

    const/4 v11, 0x3

    aput-object p4, v0, v11

    const/4 v12, 0x4

    aput-object v6, v0, v12

    const-string v13, "onCatAuthorizeCompletion"

    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1072
    :try_start_0
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "code->"

    aput-object v1, v0, v8

    .line 1074
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "sequence->"

    aput-object v1, v0, v10

    .line 1075
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    const-string v1, "service->"

    aput-object v1, v0, v12

    .line 1076
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "result->"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    aput-object v6, v0, v1

    .line 1073
    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1080
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cat/AuthorizeCompletionListener;->onCatAuthorizeCompletion(Lcom/epson/epos2/cat/Cat;IIILcom/epson/epos2/cat/AuthorizeResult;)V

    :cond_0
    new-array v0, v7, [Ljava/lang/Object;

    .line 1082
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    aput-object p4, v0, v11

    aput-object v6, v0, v12

    invoke-direct {p0, v13, v8, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private onCatAuthorizeRefund(IIILcom/epson/epos2/cat/AuthorizeResult;)V
    .locals 14

    move-object v6, p0

    const/4 v7, 0x5

    new-array v0, v7, [Ljava/lang/Object;

    .line 1050
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v9, 0x1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x2

    aput-object v1, v0, v10

    const/4 v11, 0x3

    aput-object p4, v0, v11

    const/4 v12, 0x4

    aput-object v6, v0, v12

    const-string v13, "onCatAuthorizeRefund"

    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1052
    :try_start_0
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "code->"

    aput-object v1, v0, v8

    .line 1054
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "sequence->"

    aput-object v1, v0, v10

    .line 1055
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    const-string v1, "service->"

    aput-object v1, v0, v12

    .line 1056
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "result->"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    aput-object v6, v0, v1

    .line 1053
    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1060
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cat/AuthorizeRefundListener;->onCatAuthorizeRefund(Lcom/epson/epos2/cat/Cat;IIILcom/epson/epos2/cat/AuthorizeResult;)V

    :cond_0
    new-array v0, v7, [Ljava/lang/Object;

    .line 1062
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    aput-object p4, v0, v11

    aput-object v6, v0, v12

    invoke-direct {p0, v13, v8, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private onCatAuthorizeSales(IIILcom/epson/epos2/cat/AuthorizeResult;)V
    .locals 14

    move-object v6, p0

    const/4 v7, 0x5

    new-array v0, v7, [Ljava/lang/Object;

    .line 1009
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v9, 0x1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x2

    aput-object v1, v0, v10

    const/4 v11, 0x3

    aput-object p4, v0, v11

    const/4 v12, 0x4

    aput-object v6, v0, v12

    const-string v13, "onCatAuthorizeSales"

    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1011
    :try_start_0
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "code->"

    aput-object v1, v0, v8

    .line 1013
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "sequence->"

    aput-object v1, v0, v10

    .line 1014
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    const-string v1, "service->"

    aput-object v1, v0, v12

    .line 1015
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "result->"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    aput-object v6, v0, v1

    .line 1012
    invoke-direct {p0, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1019
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cat/AuthorizeSalesListener;->onCatAuthorizeSales(Lcom/epson/epos2/cat/Cat;IIILcom/epson/epos2/cat/AuthorizeResult;)V

    :cond_0
    new-array v0, v7, [Ljava/lang/Object;

    .line 1021
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    aput-object p4, v0, v11

    aput-object v6, v0, v12

    invoke-direct {p0, v13, v8, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private onCatAuthorizeVoid(IIILcom/epson/epos2/cat/AuthorizeResult;)V
    .locals 13

    move-object v6, p0

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Object;

    .line 1029
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v7, 0x0

    aput-object v2, v1, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v8, 0x1

    aput-object v2, v1, v8

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v9, 0x2

    aput-object v2, v1, v9

    const/4 v10, 0x3

    aput-object p4, v1, v10

    const/4 v11, 0x4

    aput-object v6, v1, v11

    const-string v12, "onCatAuthorizeVoid"

    invoke-direct {p0, v12, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1031
    :try_start_0
    iget-object v1, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "code->"

    aput-object v2, v1, v7

    .line 1033
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    const-string v2, "sequence->"

    aput-object v2, v1, v9

    .line 1034
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const-string v2, "service->"

    aput-object v2, v1, v11

    .line 1035
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    const-string v2, "result->"

    aput-object v2, v1, v0

    const/4 v0, 0x7

    aput-object p4, v1, v0

    const/16 v0, 0x8

    aput-object v6, v1, v0

    .line 1032
    invoke-direct {p0, v12, v1}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1039
    iget-object v0, v6, Lcom/epson/epos2/cat/Cat;->mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/cat/AuthorizeVoidListener;->onCatAuthorizeVoid(Lcom/epson/epos2/cat/Cat;IIILcom/epson/epos2/cat/AuthorizeResult;)V

    :cond_0
    new-array v0, v11, [Ljava/lang/Object;

    .line 1041
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    aput-object v6, v0, v10

    invoke-direct {p0, v12, v7, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private onCatDirectIOCommandReply(IIILjava/lang/String;IILcom/epson/epos2/cat/DirectIOResult;)V
    .locals 20

    move-object/from16 v9, p0

    const/16 v10, 0x8

    new-array v0, v10, [Ljava/lang/Object;

    .line 1129
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x0

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x2

    aput-object v1, v0, v13

    const/4 v14, 0x3

    aput-object p4, v0, v14

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x4

    aput-object v1, v0, v15

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x5

    aput-object v1, v0, v16

    const/16 v17, 0x6

    aput-object p7, v0, v17

    const/16 v18, 0x7

    aput-object v9, v0, v18

    const-string v8, "onCatDirectIOCommandReply"

    invoke-direct {v9, v8, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1130
    iget-object v0, v9, Lcom/epson/epos2/cat/Cat;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "code->"

    aput-object v1, v0, v11

    .line 1132
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    const-string v1, "command->"

    aput-object v1, v0, v13

    .line 1133
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    const-string v1, "data->"

    aput-object v1, v0, v15

    .line 1134
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    const-string v1, "string->"

    aput-object v1, v0, v17

    aput-object p4, v0, v18

    const-string v1, "sequence->"

    aput-object v1, v0, v10

    const/16 v1, 0x9

    .line 1136
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "service->"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 1137
    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "result->"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    aput-object p7, v0, v1

    const/16 v1, 0xe

    aput-object v9, v0, v1

    .line 1131
    invoke-direct {v9, v8, v0}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1140
    iget-object v0, v9, Lcom/epson/epos2/cat/Cat;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v19, v8

    move-object/from16 v8, p7

    invoke-interface/range {v0 .. v8}, Lcom/epson/epos2/cat/DirectIOCommandReplyListener;->onCatDirectIOCommandReply(Lcom/epson/epos2/cat/Cat;IIILjava/lang/String;IILcom/epson/epos2/cat/DirectIOResult;)V

    goto :goto_0

    :cond_0
    move-object/from16 v19, v8

    :goto_0
    new-array v0, v10, [Ljava/lang/Object;

    .line 1142
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    aput-object p4, v0, v14

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    aput-object p7, v0, v17

    aput-object v9, v0, v18

    move-object/from16 v1, v19

    invoke-direct {v9, v1, v11, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onCatStatusUpdate(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1151
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onCatStatusUpdate"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1152
    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "status->"

    aput-object v5, v1, v3

    .line 1154
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    aput-object p0, v1, v0

    .line 1153
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1156
    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/cat/StatusUpdateListener;->onCatStatusUpdate(Lcom/epson/epos2/cat/Cat;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1158
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1167
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1168
    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 1169
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/cat/Cat;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1170
    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1172
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 1282
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 1264
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 1291
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 1273
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/cat/Cat;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/cat/Cat;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public accessDailyLog(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "accessDailyLog"

    .line 634
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 637
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 639
    iget-wide v4, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    int-to-long v7, p2

    iget p2, p0, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    int-to-long v9, p2

    iget v11, p0, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    move-object v3, p0

    move v6, p1

    invoke-virtual/range {v3 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2AccessDailyLog(JIJJI)I

    move-result p1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 650
    invoke-direct {p0, v2, v0, p1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 641
    :cond_0
    :try_start_1
    new-instance p2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p2, p1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p2
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 645
    invoke-direct {p0, v2, p1}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 646
    invoke-virtual {p1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result p2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, p2, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 647
    throw p1
.end method

.method public authorizeCompletion(III)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object v12, p0

    const/4 v13, 0x0

    new-array v0, v13, [Ljava/lang/Object;

    const-string v14, "authorizeCompletion"

    .line 592
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 595
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 597
    iget-wide v2, v12, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    move/from16 v0, p2

    int-to-long v5, v0

    move/from16 v0, p3

    int-to-long v7, v0

    iget v0, v12, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    int-to-long v9, v0

    iget v11, v12, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    move-object v1, p0

    move/from16 v4, p1

    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2AuthorizeCompletion(JIJJJI)I

    move-result v0
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    new-array v0, v13, [Ljava/lang/Object;

    .line 608
    invoke-direct {p0, v14, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 599
    :cond_0
    :try_start_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 603
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 604
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-direct {p0, v14, v1, v2}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 605
    throw v0
.end method

.method public authorizeRefund(III)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object v12, p0

    const/4 v13, 0x0

    new-array v0, v13, [Ljava/lang/Object;

    const-string v14, "authorizeRefund"

    .line 556
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 561
    iget-wide v2, v12, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    move/from16 v0, p2

    int-to-long v5, v0

    move/from16 v0, p3

    int-to-long v7, v0

    iget v0, v12, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    int-to-long v9, v0

    iget v11, v12, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    move-object v1, p0

    move/from16 v4, p1

    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2AuthorizeRefund(JIJJJI)I

    move-result v0
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    new-array v0, v13, [Ljava/lang/Object;

    .line 572
    invoke-direct {p0, v14, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 563
    :cond_0
    :try_start_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 567
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 568
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-direct {p0, v14, v1, v2}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 569
    throw v0
.end method

.method public authorizeSales(III)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object v12, p0

    const/4 v13, 0x0

    new-array v0, v13, [Ljava/lang/Object;

    const-string v14, "authorizeSales"

    .line 470
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 475
    iget-wide v2, v12, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    move/from16 v0, p2

    int-to-long v5, v0

    move/from16 v0, p3

    int-to-long v7, v0

    iget v0, v12, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    int-to-long v9, v0

    iget v11, v12, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    move-object v1, p0

    move/from16 v4, p1

    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2AuthorizeSale(JIJJJI)I

    move-result v0
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    new-array v0, v13, [Ljava/lang/Object;

    .line 486
    invoke-direct {p0, v14, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 477
    :cond_0
    :try_start_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 481
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 482
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-direct {p0, v14, v1, v2}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 483
    throw v0
.end method

.method public authorizeVoid(III)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object v12, p0

    const/4 v13, 0x0

    new-array v0, v13, [Ljava/lang/Object;

    const-string v14, "authorizeVoid"

    .line 515
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 518
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 520
    iget-wide v2, v12, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    move/from16 v0, p2

    int-to-long v5, v0

    move/from16 v0, p3

    int-to-long v7, v0

    iget v0, v12, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    int-to-long v9, v0

    iget v11, v12, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    move-object v1, p0

    move/from16 v4, p1

    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2AuthorizeVoid(JIJJJI)I

    move-result v0
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    new-array v0, v13, [Ljava/lang/Object;

    .line 531
    invoke-direct {p0, v14, v13, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 522
    :cond_0
    :try_start_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 526
    invoke-direct {p0, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 527
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v13, [Ljava/lang/Object;

    invoke-direct {p0, v14, v1, v2}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 528
    throw v0
.end method

.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 275
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 276
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 340
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 346
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 348
    iget-wide v6, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/cat/Cat;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 359
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 350
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 344
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 354
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 355
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 356
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 374
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 379
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 390
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 381
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 385
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 386
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 387
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 243
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 245
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

    .line 246
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

    .line 247
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

    .line 248
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

    .line 249
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

    .line 250
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

    .line 251
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

    .line 253
    iput-object v1, p0, Lcom/epson/epos2/cat/Cat;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 255
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 256
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2Disconnect(J)I

    .line 257
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2DestroyHandle(J)I

    .line 258
    iput-wide v5, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 265
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 262
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 263
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 1189
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1191
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1192
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1196
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1198
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1201
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 1220
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1223
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1227
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1229
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1232
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getOposErrorCode()I
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getOposErrorCode"

    .line 431
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 433
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    return v0

    .line 437
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2GetOposErrorCode(J)I

    move-result v1

    new-array v0, v0, [Ljava/lang/Object;

    .line 438
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return v1
.end method

.method public getStauts()Lcom/epson/epos2/cat/CatStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 408
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410
    iget-wide v3, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/cat/Cat;->nativeEpos2GetStatus(J)Lcom/epson/epos2/cat/CatStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 412
    invoke-virtual {v1}, Lcom/epson/epos2/cat/CatStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/cat/Cat;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 414
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/cat/Cat;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 417
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method public getTimeout()I
    .locals 1

    .line 751
    iget v0, p0, Lcom/epson/epos2/cat/Cat;->mTimeout:I

    return v0
.end method

.method public getTrainingMode()I
    .locals 1

    .line 809
    iget v0, p0, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I

    return v0
.end method

.method protected initializeCatInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 228
    invoke-direct {p0, v0}, Lcom/epson/epos2/cat/Cat;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 232
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 230
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2AccessDailyLog(JIJJI)I
.end method

.method protected native nativeEpos2AuthorizeCompletion(JIJJJI)I
.end method

.method protected native nativeEpos2AuthorizeRefund(JIJJJI)I
.end method

.method protected native nativeEpos2AuthorizeSale(JIJJJI)I
.end method

.method protected native nativeEpos2AuthorizeVoid(JIJJJI)I
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetOposErrorCode(J)I
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/cat/CatStatusInfo;
.end method

.method protected native nativeEpos2SendDirectIOCommand(JJJLjava/lang/String;JI)I
.end method

.method public sendDirectIOCommand(IILjava/lang/String;I)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v12, p0

    const/4 v13, 0x4

    new-array v0, v13, [Ljava/lang/Object;

    .line 681
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x0

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x1

    aput-object v1, v0, v15

    const/16 v16, 0x2

    aput-object p3, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x3

    aput-object v1, v0, v17

    const-string v11, "sendDirectIOCommand"

    invoke-direct {v12, v11, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 684
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    .line 686
    iget-wide v2, v12, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    move/from16 v9, p1

    int-to-long v4, v9

    move/from16 v10, p2

    int-to-long v6, v10

    move/from16 v8, p4

    int-to-long v0, v8

    iget v15, v12, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-wide/from16 v18, v0

    move-object/from16 v1, p0

    move-object/from16 v8, p3

    move-wide/from16 v9, v18

    move-object/from16 v20, v11

    move v11, v15

    :try_start_1
    invoke-virtual/range {v1 .. v11}, Lcom/epson/epos2/cat/Cat;->nativeEpos2SendDirectIOCommand(JJJLjava/lang/String;JI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    new-array v0, v13, [Ljava/lang/Object;

    .line 697
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    aput-object p3, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    move-object/from16 v1, v20

    invoke-direct {v12, v1, v14, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, v20

    .line 688
    :try_start_2
    new-instance v2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v2, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v2
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v1, v20

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v1, v11

    .line 692
    :goto_0
    invoke-direct {v12, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 693
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v3, v13, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    aput-object p3, v3, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v17

    invoke-direct {v12, v1, v2, v3}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 694
    throw v0
.end method

.method public setAccessDailyLogEventListener(Lcom/epson/epos2/cat/AccessDailyLogListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setAccessDailyLogEventListener"

    .line 916
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 917
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 919
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 922
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAccessDailyLogListener:Lcom/epson/epos2/cat/AccessDailyLogListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setAuthorizeCompletionEventListener(Lcom/epson/epos2/cat/AuthorizeCompletionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setAuthorizeCompletionEventListener"

    .line 893
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 895
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 897
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 900
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeCompletionListener:Lcom/epson/epos2/cat/AuthorizeCompletionListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setAuthorizeRefundEventListener(Lcom/epson/epos2/cat/AuthorizeRefundListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setAuthorizeRefundEventListener"

    .line 870
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 872
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 874
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 877
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeRefundLitener:Lcom/epson/epos2/cat/AuthorizeRefundListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setAuthorizeSalesEventListener(Lcom/epson/epos2/cat/AuthorizeSalesListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setAuthorizeSalesEventListener"

    .line 824
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 826
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 828
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 831
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeSalesListener:Lcom/epson/epos2/cat/AuthorizeSalesListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setAuthorizeVoidEventListener(Lcom/epson/epos2/cat/AuthorizeVoidListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setAuthorizeVoidEventListener"

    .line 847
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 849
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 851
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 854
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mAuthorizeVoidListener:Lcom/epson/epos2/cat/AuthorizeVoidListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 993
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 995
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 1001
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1004
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setDirectIOCommandReplyEventListener(Lcom/epson/epos2/cat/DirectIOCommandReplyListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setDirectIOCommandReplyEventListener"

    .line 940
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 942
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 948
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 951
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mDirectIOCommandReplyListener:Lcom/epson/epos2/cat/DirectIOCommandReplyListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setStatusUpdateEventListener(Lcom/epson/epos2/cat/StatusUpdateListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setStatusUpdateEventListener"

    .line 970
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 972
    iget-wide v0, p0, Lcom/epson/epos2/cat/Cat;->mCatHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    if-eqz p1, :cond_1

    .line 978
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 981
    iput-object p1, p0, Lcom/epson/epos2/cat/Cat;->mStatusUpdateListener:Lcom/epson/epos2/cat/StatusUpdateListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public setTimeout(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "setTimeout"

    .line 718
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 721
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    if-ltz p1, :cond_0

    const v1, 0xf3e58

    if-gt p1, v1, :cond_0

    .line 726
    iput p1, p0, Lcom/epson/epos2/cat/Cat;->mTimeout:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 734
    invoke-direct {p0, v2, v0, p1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 724
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/epos2/Epos2Exception;

    const/4 v1, 0x1

    invoke-direct {p1, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 729
    invoke-direct {p0, v2, p1}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 730
    invoke-virtual {p1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 731
    throw p1
.end method

.method public setTrainingMode(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "setTrainingMode"

    .line 774
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/cat/Cat;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 777
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/cat/Cat;->checkHandle()V

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 780
    :cond_0
    new-instance p1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p1, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p1

    .line 782
    :cond_1
    :goto_0
    iput p1, p0, Lcom/epson/epos2/cat/Cat;->mTrainingMode:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 790
    invoke-direct {p0, v2, v0, p1}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception p1

    .line 785
    invoke-direct {p0, v2, p1}, Lcom/epson/epos2/cat/Cat;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 786
    invoke-virtual {p1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/cat/Cat;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 787
    throw p1
.end method
