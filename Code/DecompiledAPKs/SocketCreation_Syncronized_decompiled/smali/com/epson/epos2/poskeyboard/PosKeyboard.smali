.class public Lcom/epson/epos2/poskeyboard/PosKeyboard;
.super Ljava/lang/Object;
.source "PosKeyboard.java"


# static fields
.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mPosKbdHandle:J

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 19
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 51
    sput v0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 26
    iput-wide v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

    .line 29
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 31
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    .line 32
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 33
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 34
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 35
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 36
    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 91
    invoke-direct {p0, p1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "PosKeyboard"

    .line 92
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 95
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 96
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 97
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    .line 98
    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 101
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 102
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 105
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mContext:Landroid/content/Context;

    .line 107
    invoke-virtual {p0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->initializePosKbdInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 109
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 420
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    .line 422
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 423
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 424
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 425
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 426
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 427
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 428
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 429
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 430
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 431
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 433
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 436
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 286
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 289
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 288
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 292
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 460
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 442
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 469
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 451
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 412
    iget-wide v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 413
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 145
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 151
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->checkHandle()V

    .line 153
    iget-wide v6, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 164
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 155
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 149
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 159
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 160
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 161
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 179
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->checkHandle()V

    .line 184
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 195
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 186
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 190
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 191
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 192
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 383
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 385
    iput-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

    .line 386
    iput-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 389
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 390
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2Disconnect(J)I

    .line 391
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2DestroyHandle(J)I

    .line 392
    iput-wide v5, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 399
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 396
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 397
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 307
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 310
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 314
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 316
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 319
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 337
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 340
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 344
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 346
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 349
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getStauts()Lcom/epson/epos2/poskeyboard/PosKeyboardStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 213
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    iget-wide v3, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2GetStatus(J)Lcom/epson/epos2/poskeyboard/PosKeyboardStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {v1}, Lcom/epson/epos2/poskeyboard/PosKeyboardStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/poskeyboard/PosKeyboard;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 218
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/poskeyboard/PosKeyboard;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 221
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializePosKbdInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 366
    invoke-direct {p0, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 372
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 369
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/poskeyboard/PosKeyboardStatusInfo;
.end method

.method protected onPosKbdKeyPress(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 257
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onPosKbdKeyPress"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 258
    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "posKeyCode->"

    aput-object v5, v1, v3

    .line 260
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 259
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    iget-object v1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/poskeyboard/KeyPressListener;->onPosKbdKeyPress(Lcom/epson/epos2/poskeyboard/PosKeyboard;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 263
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 273
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 275
    iget-wide v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 277
    iput-object p1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 280
    iput-object p1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setKeyPressEventListener(Lcom/epson/epos2/poskeyboard/KeyPressListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setKeyPressEventListener"

    .line 237
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/poskeyboard/PosKeyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    iget-wide v0, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mPosKbdHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 241
    iput-object p1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 244
    iput-object p1, p0, Lcom/epson/epos2/poskeyboard/PosKeyboard;->mKeyPressListener:Lcom/epson/epos2/poskeyboard/KeyPressListener;

    :cond_1
    :goto_0
    return-void
.end method
