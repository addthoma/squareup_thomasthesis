.class public Lcom/epson/epos2/linedisplay/LineDisplay;
.super Ljava/lang/Object;
.source "LineDisplay.java"


# static fields
.field public static final BRIGHTNESS_100:I = 0x3

.field public static final BRIGHTNESS_20:I = 0x0

.field public static final BRIGHTNESS_40:I = 0x1

.field public static final BRIGHTNESS_60:I = 0x2

.field public static final CURSOR_NONE:I = 0x0

.field public static final CURSOR_UNDERLINE:I = 0x1

.field public static final DM_D110:I = 0x0

.field public static final DM_D210:I = 0x1

.field public static final DM_D30:I = 0x2

.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field public static final LANG_EN:I = 0x0

.field public static final LANG_JA:I = 0x1

.field public static final MARQUEE_PLACE:I = 0x1

.field public static final MARQUEE_WALK:I = 0x0

.field public static final MOVE_BOTTOM_LEFT:I = 0x2

.field public static final MOVE_BOTTOM_RIGHT:I = 0x3

.field public static final MOVE_TOP_LEFT:I = 0x0

.field public static final MOVE_TOP_RIGHT:I = 0x1

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final SCROLL_HORIZONTAL:I = 0x2

.field public static final SCROLL_OVERWRITE:I = 0x0

.field public static final SCROLL_VERTICAL:I = 0x1

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mDisplayHandle:J

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mReceiveListener:Lcom/epson/epos2/linedisplay/ReceiveListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 14
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 120
    sput v0, Lcom/epson/epos2/linedisplay/LineDisplay;->connection:I

    return-void
.end method

.method public constructor <init>(ILandroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 21
    iput-wide v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    .line 27
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 28
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 29
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 30
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 31
    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 141
    invoke-direct {p0, p2}, Lcom/epson/epos2/linedisplay/LineDisplay;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 142
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const-string v4, "LineDisplay"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 145
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v5, "initializeNativeEnv"

    new-array v6, v2, [Ljava/lang/Class;

    .line 146
    const-class v7, Landroid/content/Context;

    aput-object v7, v6, v3

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 147
    invoke-virtual {v5, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v6, v2, [Ljava/lang/Object;

    aput-object p2, v6, v3

    .line 148
    invoke-virtual {v5, v1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 151
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 152
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 155
    :goto_0
    iput-object p2, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mContext:Landroid/content/Context;

    .line 156
    invoke-virtual {p0, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->initializeDisplayInstance(I)V

    new-array v0, v0, [Ljava/lang/Object;

    .line 157
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p2, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 1444
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    .line 1446
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 1447
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1448
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 1449
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1450
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 1451
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1452
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 1453
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1454
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 1455
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1457
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1460
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle(I[J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1425
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1427
    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 1431
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 1430
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1433
    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1437
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onDispReceive(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1406
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onDispReceive"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1408
    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReceiveListener:Lcom/epson/epos2/linedisplay/ReceiveListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "code->"

    aput-object v5, v1, v3

    .line 1412
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 1411
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1414
    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReceiveListener:Lcom/epson/epos2/linedisplay/ReceiveListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/linedisplay/ReceiveListener;->onDispReceive(Lcom/epson/epos2/linedisplay/LineDisplay;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 1418
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 1484
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 1466
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 1493
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 1475
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public addClearCurrentWindow()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addClearCurrentWindow"

    .line 614
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 617
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 618
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddClearCurrentWindow(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 629
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 620
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 624
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 625
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 626
    throw v1
.end method

.method public addCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addCommand"

    .line 1246
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1249
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1250
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 1261
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1252
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1256
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1257
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1258
    throw v1
.end method

.method public addCreateWindow(IIIIII)V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v15, p0

    const/4 v14, 0x6

    new-array v0, v14, [Ljava/lang/Object;

    .line 519
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x0

    aput-object v1, v0, v12

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x2

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x3

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v19, 0x4

    aput-object v1, v0, v19

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v20, 0x5

    aput-object v1, v0, v20

    const-string v13, "addCreateWindow"

    invoke-direct {v15, v13, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 522
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 523
    iget-wide v2, v15, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v10, p1

    int-to-long v4, v10

    move/from16 v11, p2

    int-to-long v6, v11

    move/from16 v8, p3

    int-to-long v0, v8

    move/from16 v9, p4

    move-object/from16 v21, v13

    int-to-long v12, v9

    move/from16 v15, p5

    int-to-long v10, v15

    move-wide/from16 v22, v0

    move-object/from16 v1, p0

    move-wide/from16 v8, v22

    move-wide/from16 v22, v10

    move-wide v10, v12

    move-object/from16 v24, v21

    const/4 v15, 0x0

    move-wide/from16 v12, v22

    const/4 v15, 0x6

    move/from16 v14, p6

    :try_start_1
    invoke-virtual/range {v1 .. v14}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddCreateWindow(JJJJJJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    new-array v0, v15, [Ljava/lang/Object;

    .line 534
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v20

    move-object/from16 v1, p0

    move-object/from16 v3, v24

    invoke-direct {v1, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, p0

    move-object/from16 v3, v24

    .line 525
    :try_start_2
    new-instance v2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v2, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v2
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v1, p0

    move-object/from16 v3, v24

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v3, v13

    move-object v1, v15

    const/4 v15, 0x6

    .line 529
    :goto_0
    invoke-direct {v1, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 530
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v4, v15, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v19

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v20

    invoke-direct {v1, v3, v2, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 531
    throw v0
.end method

.method public addDestroyWindow(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 552
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addDestroyWindow"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 555
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 556
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddDestroyWindow(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 567
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 558
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 562
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 563
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 564
    throw v1
.end method

.method public addInitialize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addInitialize"

    .line 461
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 466
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddInitialize(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 477
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 468
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 472
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 473
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 474
    throw v1
.end method

.method public addMarqueeText(Ljava/lang/String;IIIII)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v14, 0x6

    new-array v0, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object p1, v0, v15

    .line 1109
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v16, 0x1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v17, 0x2

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v18, 0x3

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v19, 0x4

    aput-object v1, v0, v19

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v20, 0x5

    aput-object v1, v0, v20

    const-string v12, "addMarqueeText"

    invoke-direct {v13, v12, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1112
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1113
    iget-wide v2, v13, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v10, p3

    int-to-long v6, v10

    move/from16 v11, p4

    int-to-long v8, v11

    move/from16 v5, p5

    int-to-long v0, v5

    move-wide/from16 v21, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v10, v21

    move-object/from16 v23, v12

    move/from16 v12, p6

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddMarqueeText(JLjava/lang/String;IJJJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    new-array v0, v14, [Ljava/lang/Object;

    aput-object p1, v0, v15

    .line 1124
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v20

    move-object/from16 v1, v23

    invoke-direct {v13, v1, v15, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, v23

    .line 1115
    :try_start_2
    new-instance v2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v2, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v2
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v1, v23

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v1, v12

    .line 1119
    :goto_0
    invoke-direct {v13, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1120
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v3, v14, [Ljava/lang/Object;

    aput-object p1, v3, v15

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v16

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v17

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v18

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v19

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v20

    invoke-direct {v13, v1, v2, v3}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1121
    throw v0
.end method

.method public addMoveCursorPosition(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 688
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addMoveCursorPosition"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 691
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 692
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddMoveCursorPosition(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 703
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 694
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 698
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 699
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 700
    throw v1
.end method

.method public addReverseText(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addReverseText"

    .line 923
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 926
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 927
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddReverseText(JLjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 938
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 929
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 933
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 934
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 935
    throw v1
.end method

.method public addReverseText(Ljava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 964
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "addReverseText"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 967
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 968
    iget-wide v5, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddReverseTextLang(JLjava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 979
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 970
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 974
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 975
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 976
    throw v1
.end method

.method public addReverseText(Ljava/lang/String;II)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v0, v11

    .line 1006
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x2

    aput-object v1, v0, v13

    const-string v14, "addReverseText"

    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1009
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1010
    iget-wide v2, v9, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v15, p2

    int-to-long v5, v15

    move/from16 v7, p3

    int-to-long v0, v7

    move-wide/from16 v16, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide/from16 v7, v16

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddReverseTextPosition(JLjava/lang/String;JJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v11

    .line 1021
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-direct {v9, v14, v11, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1012
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v15, p2

    .line 1016
    :goto_0
    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1017
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    aput-object p1, v2, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-direct {v9, v14, v1, v2}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1018
    throw v0
.end method

.method public addReverseText(Ljava/lang/String;III)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v10, p0

    const/4 v11, 0x4

    new-array v0, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p1, v0, v12

    .line 1053
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x1

    aput-object v1, v0, v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x2

    aput-object v1, v0, v14

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x3

    aput-object v1, v0, v15

    const-string v9, "addReverseText"

    invoke-direct {v10, v9, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1056
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1057
    iget-wide v2, v10, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v7, p2

    int-to-long v5, v7

    move/from16 v8, p3

    int-to-long v0, v8

    move-wide/from16 v16, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide/from16 v7, v16

    move-object/from16 v18, v9

    move/from16 v9, p4

    :try_start_1
    invoke-virtual/range {v1 .. v9}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddReverseTextPositionLang(JLjava/lang/String;JJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    new-array v0, v11, [Ljava/lang/Object;

    aput-object p1, v0, v12

    .line 1068
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    move-object/from16 v1, v18

    invoke-direct {v10, v1, v12, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, v18

    .line 1059
    :try_start_2
    new-instance v2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v2, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v2
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v1, v18

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v1, v9

    .line 1063
    :goto_0
    invoke-direct {v10, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1064
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v3, v11, [Ljava/lang/Object;

    aput-object p1, v3, v12

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v14

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v15

    invoke-direct {v10, v1, v2, v3}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1065
    throw v0
.end method

.method public addSetBlink(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1142
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addSetBlink"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1145
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1146
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddSetBlink(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1157
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1148
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1152
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1153
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1154
    throw v1
.end method

.method public addSetBrightness(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1177
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addSetBrightness"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1180
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1181
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddSetBrightness(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1192
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1183
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1187
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1188
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1189
    throw v1
.end method

.method public addSetCurrentWindow(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 587
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addSetCurrentWindow"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 590
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 591
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddSetCurrentWindow(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 602
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 593
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 597
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 598
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 599
    throw v1
.end method

.method public addSetCursorPosition(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 653
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addSetCursorPosition"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 656
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 657
    iget-wide v6, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddSetCursorPosition(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 668
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 659
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 663
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 664
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v2, v5, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 665
    throw v1
.end method

.method public addSetCursorType(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 721
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addSetCursorType"

    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 724
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 725
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddSetCursorType(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 736
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 727
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 731
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 732
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-direct {p0, v2, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 733
    throw v1
.end method

.method public addShowClock()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addShowClock"

    .line 1208
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1211
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 1212
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddShowClock(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 1223
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 1214
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 1218
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1219
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 1220
    throw v1
.end method

.method public addText(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addText"

    .line 757
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 760
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 761
    iget-wide v4, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddText(JLjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 772
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 763
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 767
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 768
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 769
    throw v1
.end method

.method public addText(Ljava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 798
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "addText"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 801
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 802
    iget-wide v5, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddTextLang(JLjava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 813
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 804
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 808
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 809
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 810
    throw v1
.end method

.method public addText(Ljava/lang/String;II)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v0, v11

    .line 840
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x2

    aput-object v1, v0, v13

    const-string v14, "addText"

    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 843
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 844
    iget-wide v2, v9, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v15, p2

    int-to-long v5, v15

    move/from16 v7, p3

    int-to-long v0, v7

    move-wide/from16 v16, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide/from16 v7, v16

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddTextPosition(JLjava/lang/String;JJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v11

    .line 855
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-direct {v9, v14, v11, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 846
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v15, p2

    .line 850
    :goto_0
    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 851
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    aput-object p1, v2, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-direct {v9, v14, v1, v2}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 852
    throw v0
.end method

.method public addText(Ljava/lang/String;III)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v10, p0

    const/4 v11, 0x4

    new-array v0, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p1, v0, v12

    .line 887
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x1

    aput-object v1, v0, v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v14, 0x2

    aput-object v1, v0, v14

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v15, 0x3

    aput-object v1, v0, v15

    const-string v9, "addText"

    invoke-direct {v10, v9, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 890
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 891
    iget-wide v2, v10, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v7, p2

    int-to-long v5, v7

    move/from16 v8, p3

    int-to-long v0, v8

    move-wide/from16 v16, v0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide/from16 v7, v16

    move-object/from16 v18, v9

    move/from16 v9, p4

    :try_start_1
    invoke-virtual/range {v1 .. v9}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2AddTextPositionLang(JLjava/lang/String;JJI)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    new-array v0, v11, [Ljava/lang/Object;

    aput-object p1, v0, v12

    .line 902
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    move-object/from16 v1, v18

    invoke-direct {v10, v1, v12, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v1, v18

    .line 893
    :try_start_2
    new-instance v2, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v2, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v2
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v1, v18

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v1, v9

    .line 897
    :goto_0
    invoke-direct {v10, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 898
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v2

    new-array v3, v11, [Ljava/lang/Object;

    aput-object p1, v3, v12

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v14

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v15

    invoke-direct {v10, v1, v2, v3}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 899
    throw v0
.end method

.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 358
    iget-wide v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 359
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public clearCommandBuffer()V
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "clearCommandBuffer"

    .line 440
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 443
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2ClearCommandBuffer(J)I

    :cond_0
    new-array v1, v0, [Ljava/lang/Object;

    .line 446
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 295
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 301
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 303
    iget-wide v6, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 314
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 305
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 299
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 309
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 310
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 311
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 329
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 332
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 334
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 345
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 336
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 340
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 341
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 342
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 193
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 195
    iput-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReceiveListener:Lcom/epson/epos2/linedisplay/ReceiveListener;

    .line 196
    iput-object v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 198
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 200
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2Disconnect(J)I

    .line 201
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2DestroyHandle(J)I

    .line 202
    iput-wide v5, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 209
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 206
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 207
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 1324
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1326
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1327
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1331
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1333
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1336
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 1355
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1357
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1358
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 1362
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 1364
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 1367
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getStatus()Lcom/epson/epos2/linedisplay/DisplayStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 419
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 421
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2GetStatus(J)Lcom/epson/epos2/linedisplay/DisplayStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 423
    invoke-virtual {v1}, Lcom/epson/epos2/linedisplay/DisplayStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/linedisplay/LineDisplay;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 425
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/linedisplay/LineDisplay;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 428
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializeDisplayInstance(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 178
    invoke-direct {p0, p1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2CreateHandle(I[J)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 182
    aget-wide v1, v0, p1

    iput-wide v1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const-wide/16 v1, 0x0

    aput-wide v1, v0, p1

    return-void

    .line 180
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, p1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2AddClearCurrentWindow(J)I
.end method

.method protected native nativeEpos2AddCommand(J[B)I
.end method

.method protected native nativeEpos2AddCreateWindow(JJJJJJI)I
.end method

.method protected native nativeEpos2AddDestroyWindow(JJ)I
.end method

.method protected native nativeEpos2AddInitialize(J)I
.end method

.method protected native nativeEpos2AddMarqueeText(JLjava/lang/String;IJJJI)I
.end method

.method protected native nativeEpos2AddMoveCursorPosition(JI)I
.end method

.method protected native nativeEpos2AddReverseText(JLjava/lang/String;)I
.end method

.method protected native nativeEpos2AddReverseTextLang(JLjava/lang/String;I)I
.end method

.method protected native nativeEpos2AddReverseTextPosition(JLjava/lang/String;JJ)I
.end method

.method protected native nativeEpos2AddReverseTextPositionLang(JLjava/lang/String;JJI)I
.end method

.method protected native nativeEpos2AddSetBlink(JJ)I
.end method

.method protected native nativeEpos2AddSetBrightness(JI)I
.end method

.method protected native nativeEpos2AddSetCurrentWindow(JJ)I
.end method

.method protected native nativeEpos2AddSetCursorPosition(JJJ)I
.end method

.method protected native nativeEpos2AddSetCursorType(JI)I
.end method

.method protected native nativeEpos2AddShowClock(J)I
.end method

.method protected native nativeEpos2AddText(JLjava/lang/String;)I
.end method

.method protected native nativeEpos2AddTextLang(JLjava/lang/String;I)I
.end method

.method protected native nativeEpos2AddTextPosition(JLjava/lang/String;JJ)I
.end method

.method protected native nativeEpos2AddTextPositionLang(JLjava/lang/String;JJI)I
.end method

.method protected native nativeEpos2ClearCommandBuffer(J)I
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/linedisplay/DisplayStatusInfo;
.end method

.method protected native nativeEpos2SendData(J)I
.end method

.method public sendData()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "sendData"

    .line 381
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/linedisplay/LineDisplay;->checkHandle()V

    .line 385
    iget-wide v3, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/linedisplay/LineDisplay;->nativeEpos2SendData(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 396
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 387
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 391
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 392
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 393
    throw v1
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 1380
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1382
    iget-wide v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1387
    iput-object p1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1390
    iput-object p1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :goto_0
    return-void
.end method

.method public setReceiveEventListener(Lcom/epson/epos2/linedisplay/ReceiveListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setReceiveEventListener"

    .line 1299
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/linedisplay/LineDisplay;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1301
    iget-wide v0, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mDisplayHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 1303
    iput-object p1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReceiveListener:Lcom/epson/epos2/linedisplay/ReceiveListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 1306
    iput-object p1, p0, Lcom/epson/epos2/linedisplay/LineDisplay;->mReceiveListener:Lcom/epson/epos2/linedisplay/ReceiveListener;

    :cond_1
    :goto_0
    return-void
.end method
