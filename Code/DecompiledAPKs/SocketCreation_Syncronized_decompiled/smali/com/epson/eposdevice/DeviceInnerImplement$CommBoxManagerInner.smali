.class public Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;
.super Lcom/epson/eposdevice/commbox/CommBoxManager;
.source "DeviceInnerImplement.java"

# interfaces
.implements Lcom/epson/eposdevice/DeviceInnerImplement$IHandleObject;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/DeviceInnerImplement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CommBoxManagerInner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/eposdevice/DeviceInnerImplement;


# direct methods
.method protected constructor <init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    .line 194
    invoke-direct {p0, p2, p3}, Lcom/epson/eposdevice/commbox/CommBoxManager;-><init>(J)V

    return-void
.end method


# virtual methods
.method protected createCommBoxInstance(J)Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;
    .locals 1

    .line 207
    new-instance v0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;

    invoke-direct {v0, p0, p1, p2}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;J)V

    return-object v0
.end method

.method protected bridge synthetic createCommBoxInstance(J)Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;
    .locals 0

    .line 192
    invoke-virtual {p0, p1, p2}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->createCommBoxInstance(J)Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;

    move-result-object p1

    return-object p1
.end method

.method protected deleteInstance()V
    .locals 0

    .line 202
    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->innerDeleteInstance()V

    return-void
.end method

.method public getDeviceHandle()J
    .locals 2

    .line 198
    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->getInnerHandle()J

    move-result-wide v0

    return-wide v0
.end method

.method protected outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 253
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_OUT_WITHOUT_RET:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputExceptionLog(IJLjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 243
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_IN:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 258
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_CB_EVENT:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 248
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_OUT_WITH_RET:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
