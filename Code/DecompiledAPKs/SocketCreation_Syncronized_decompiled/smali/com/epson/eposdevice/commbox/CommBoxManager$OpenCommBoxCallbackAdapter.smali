.class public Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;
.super Ljava/lang/Object;
.source "CommBoxManager.java"

# interfaces
.implements Lcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeOpenCommBoxCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/commbox/CommBoxManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "OpenCommBoxCallbackAdapter"
.end annotation


# instance fields
.field private mListener:Lcom/epson/eposdevice/commbox/OpenCommBoxListener;

.field final synthetic this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/commbox/CommBoxManager;Lcom/epson/eposdevice/commbox/OpenCommBoxListener;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 87
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/OpenCommBoxListener;

    .line 89
    iput-object p2, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/OpenCommBoxListener;

    return-void
.end method


# virtual methods
.method public nativeOnOpenCommBox(JJLjava/lang/String;IJ)V
    .locals 1

    .line 93
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/OpenCommBoxListener;

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    if-nez p6, :cond_0

    .line 96
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    invoke-virtual {p1, p3, p4}, Lcom/epson/eposdevice/commbox/CommBoxManager;->createCommBoxInstance(J)Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    move-result-object p1

    .line 97
    iget-object p2, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    invoke-static {p2}, Lcom/epson/eposdevice/commbox/CommBoxManager;->access$000(Lcom/epson/eposdevice/commbox/CommBoxManager;)Ljava/util/Vector;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    iget-object p2, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    const/4 p3, 0x4

    new-array p3, p3, [Ljava/lang/Object;

    const/4 p4, 0x0

    aput-object p5, p3, p4

    const/4 p4, 0x1

    aput-object p1, p3, p4

    const/4 p4, 0x2

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p3, p4

    const/4 p4, 0x3

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p3, p4

    const-string p4, "onOpenCommBox"

    invoke-virtual {p2, p4, p3}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object p2, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/OpenCommBoxListener;

    long-to-int p3, p7

    invoke-interface {p2, p5, p1, p6, p3}, Lcom/epson/eposdevice/commbox/OpenCommBoxListener;->onOpenCommBox(Ljava/lang/String;Lcom/epson/eposdevice/commbox/CommBox;II)V

    :cond_1
    return-void
.end method
