.class public final Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnServiceCharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
        "Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public applied_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public return_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public source_service_charge_uid:Ljava/lang/String;

.field public taxable:Ljava/lang/Boolean;

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 13745
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 13746
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->return_taxes:Ljava/util/List;

    .line 13747
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13824
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13840
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applied_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;"
        }
    .end annotation

    .line 13921
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 13922
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ReturnServiceCharge;
    .locals 18

    move-object/from16 v0, p0

    .line 13928
    new-instance v17, Lcom/squareup/orders/model/Order$ReturnServiceCharge;

    iget-object v2, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->uid:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->source_service_charge_uid:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->name:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v7, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->percentage:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v9, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v11, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v12, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    iget-object v13, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->taxable:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->return_taxes:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 13716
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->build()Lcom/squareup/orders/model/Order$ReturnServiceCharge;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13879
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object p0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13788
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13798
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13778
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13812
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public return_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;"
        }
    .end annotation

    .line 13906
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 13907
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->return_taxes:Ljava/util/List;

    return-object p0
.end method

.method public source_service_charge_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13768
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->source_service_charge_uid:Ljava/lang/String;

    return-object p0
.end method

.method public taxable(Ljava/lang/Boolean;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13890
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->taxable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13856
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13867
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 0

    .line 13756
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
