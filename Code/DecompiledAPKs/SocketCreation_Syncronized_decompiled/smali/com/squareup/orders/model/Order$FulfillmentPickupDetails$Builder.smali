.class public final Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public acceptance_acknowledged_at:Ljava/lang/String;

.field public accepted_at:Ljava/lang/String;

.field public auto_complete_duration:Ljava/lang/String;

.field public cancel_reason:Ljava/lang/String;

.field public canceled_at:Ljava/lang/String;

.field public expired_at:Ljava/lang/String;

.field public expires_at:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public picked_up_at:Ljava/lang/String;

.field public pickup_at:Ljava/lang/String;

.field public pickup_window_duration:Ljava/lang/String;

.field public placed_at:Ljava/lang/String;

.field public prep_time_duration:Ljava/lang/String;

.field public ready_at:Ljava/lang/String;

.field public recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

.field public rejected_at:Ljava/lang/String;

.field public schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8150
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptance_acknowledged_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8287
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->acceptance_acknowledged_at:Ljava/lang/String;

    return-object p0
.end method

.method public accepted_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8273
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->accepted_at:Ljava/lang/String;

    return-object p0
.end method

.method public auto_complete_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8188
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->auto_complete_duration:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;
    .locals 2

    .line 8369
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;-><init>(Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8115
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8363
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8353
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->canceled_at:Ljava/lang/String;

    return-object p0
.end method

.method public expired_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8326
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->expired_at:Ljava/lang/String;

    return-object p0
.end method

.method public expires_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8174
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->expires_at:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8245
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public picked_up_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8340
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->picked_up_at:Ljava/lang/String;

    return-object p0
.end method

.method public pickup_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8211
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->pickup_at:Ljava/lang/String;

    return-object p0
.end method

.method public pickup_window_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8223
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->pickup_window_duration:Ljava/lang/String;

    return-object p0
.end method

.method public placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8259
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->placed_at:Ljava/lang/String;

    return-object p0
.end method

.method public prep_time_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8234
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->prep_time_duration:Ljava/lang/String;

    return-object p0
.end method

.method public ready_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8313
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->ready_at:Ljava/lang/String;

    return-object p0
.end method

.method public recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8160
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    return-object p0
.end method

.method public rejected_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8300
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->rejected_at:Ljava/lang/String;

    return-object p0
.end method

.method public schedule_type(Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 0

    .line 8198
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    return-object p0
.end method
