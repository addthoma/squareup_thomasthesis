.class final Lcom/squareup/orders/model/Order$PricingOptions$ProtoAdapter_PricingOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$PricingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PricingOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$PricingOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 15215
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$PricingOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15232
    new-instance v0, Lcom/squareup/orders/model/Order$PricingOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;-><init>()V

    .line 15233
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 15234
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 15238
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 15236
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->auto_apply_discounts(Ljava/lang/Boolean;)Lcom/squareup/orders/model/Order$PricingOptions$Builder;

    goto :goto_0

    .line 15242
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 15243
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->build()Lcom/squareup/orders/model/Order$PricingOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15213
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$PricingOptions$ProtoAdapter_PricingOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$PricingOptions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$PricingOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15226
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$PricingOptions;->auto_apply_discounts:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15227
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$PricingOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15213
    check-cast p2, Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$PricingOptions$ProtoAdapter_PricingOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$PricingOptions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$PricingOptions;)I
    .locals 3

    .line 15220
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$PricingOptions;->auto_apply_discounts:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 15221
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$PricingOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 15213
    check-cast p1, Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$PricingOptions$ProtoAdapter_PricingOptions;->encodedSize(Lcom/squareup/orders/model/Order$PricingOptions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$PricingOptions;)Lcom/squareup/orders/model/Order$PricingOptions;
    .locals 0

    .line 15248
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$PricingOptions;->newBuilder()Lcom/squareup/orders/model/Order$PricingOptions$Builder;

    move-result-object p1

    .line 15249
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 15250
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->build()Lcom/squareup/orders/model/Order$PricingOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15213
    check-cast p1, Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$PricingOptions$ProtoAdapter_PricingOptions;->redact(Lcom/squareup/orders/model/Order$PricingOptions;)Lcom/squareup/orders/model/Order$PricingOptions;

    move-result-object p1

    return-object p1
.end method
