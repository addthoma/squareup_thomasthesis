.class public final Lcom/squareup/orders/model/Order$RefundGroup;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RefundGroup"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$RefundGroup$ProtoAdapter_RefundGroup;,
        Lcom/squareup/orders/model/Order$RefundGroup$State;,
        Lcom/squareup/orders/model/Order$RefundGroup$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$RefundGroup;",
        "Lcom/squareup/orders/model/Order$RefundGroup$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$RefundGroup;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATE:Lcom/squareup/orders/model/Order$RefundGroup$State;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final refund_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final state:Lcom/squareup/orders/model/Order$RefundGroup$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$RefundGroup$State#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11790
    new-instance v0, Lcom/squareup/orders/model/Order$RefundGroup$ProtoAdapter_RefundGroup;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$RefundGroup$ProtoAdapter_RefundGroup;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$RefundGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11796
    sget-object v0, Lcom/squareup/orders/model/Order$RefundGroup$State;->DO_NOT_USE:Lcom/squareup/orders/model/Order$RefundGroup$State;

    sput-object v0, Lcom/squareup/orders/model/Order$RefundGroup;->DEFAULT_STATE:Lcom/squareup/orders/model/Order$RefundGroup$State;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$RefundGroup$State;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$RefundGroup$State;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 11828
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orders/model/Order$RefundGroup;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$RefundGroup$State;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$RefundGroup$State;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$RefundGroup$State;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 11832
    sget-object v0, Lcom/squareup/orders/model/Order$RefundGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 11833
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    .line 11834
    iput-object p2, p0, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    const-string p1, "refund_ids"

    .line 11835
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 11851
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$RefundGroup;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 11852
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$RefundGroup;

    .line 11853
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$RefundGroup;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$RefundGroup;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    .line 11854
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    .line 11855
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    .line 11856
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 11861
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 11863
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$RefundGroup;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 11864
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11865
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$RefundGroup$State;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 11866
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 11867
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$RefundGroup$Builder;
    .locals 2

    .line 11840
    new-instance v0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$RefundGroup$Builder;-><init>()V

    .line 11841
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->uid:Ljava/lang/String;

    .line 11842
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    .line 11843
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->refund_ids:Ljava/util/List;

    .line 11844
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$RefundGroup;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 11789
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$RefundGroup;->newBuilder()Lcom/squareup/orders/model/Order$RefundGroup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 11874
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11875
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11876
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    if-eqz v1, :cond_1

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11877
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", refund_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup;->refund_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RefundGroup{"

    .line 11878
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
