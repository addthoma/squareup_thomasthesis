.class public final Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
        "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public line_item_uid:Ljava/lang/String;

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public quantity:Ljava/lang/String;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 7321
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 7322
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->metadata:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;
    .locals 7

    .line 7384
    new-instance v6, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->line_item_uid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->quantity:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->metadata:Ljava/util/Map;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7312
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->build()Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;

    move-result-object v0

    return-object v0
.end method

.method public line_item_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
    .locals 0

    .line 7336
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->line_item_uid:Ljava/lang/String;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;"
        }
    .end annotation

    .line 7377
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 7378
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
    .locals 0

    .line 7350
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
    .locals 0

    .line 7331
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
