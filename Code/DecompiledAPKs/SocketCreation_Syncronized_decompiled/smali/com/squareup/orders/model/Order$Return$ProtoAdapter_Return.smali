.class final Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Return;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Return"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$Return;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 11429
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$Return;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Return;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 11462
    new-instance v0, Lcom/squareup/orders/model/Order$Return$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Return$Builder;-><init>()V

    .line 11463
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 11464
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 11476
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 11474
    :pswitch_0
    sget-object v3, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts(Lcom/squareup/orders/model/Order$MoneyAmounts;)Lcom/squareup/orders/model/Order$Return$Builder;

    goto :goto_0

    .line 11473
    :pswitch_1
    sget-object v3, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment(Lcom/squareup/orders/model/Order$RoundingAdjustment;)Lcom/squareup/orders/model/Order$Return$Builder;

    goto :goto_0

    .line 11472
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_tips:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11471
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_discounts:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11470
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_taxes:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11469
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_service_charges:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11468
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_line_items:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11467
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Return$Builder;->source_order_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Return$Builder;

    goto :goto_0

    .line 11466
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Return$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Return$Builder;

    goto :goto_0

    .line 11480
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$Return$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 11481
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Return$Builder;->build()Lcom/squareup/orders/model/Order$Return;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 11427
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Return;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Return;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 11448
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11449
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11450
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11451
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11452
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11453
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11454
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11455
    sget-object v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11456
    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 11457
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$Return;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 11427
    check-cast p2, Lcom/squareup/orders/model/Order$Return;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Return;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$Return;)I
    .locals 4

    .line 11434
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 11435
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11436
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11437
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11438
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11439
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11440
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    const/16 v3, 0x8

    .line 11441
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/16 v3, 0x9

    .line 11442
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11443
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Return;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 11427
    check-cast p1, Lcom/squareup/orders/model/Order$Return;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;->encodedSize(Lcom/squareup/orders/model/Order$Return;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$Return;)Lcom/squareup/orders/model/Order$Return;
    .locals 2

    .line 11486
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Return;->newBuilder()Lcom/squareup/orders/model/Order$Return$Builder;

    move-result-object p1

    .line 11487
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_line_items:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 11488
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_service_charges:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 11489
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_taxes:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 11490
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_discounts:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 11491
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_tips:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 11492
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 11493
    :cond_0
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 11494
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Return$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 11495
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Return$Builder;->build()Lcom/squareup/orders/model/Order$Return;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11427
    check-cast p1, Lcom/squareup/orders/model/Order$Return;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;->redact(Lcom/squareup/orders/model/Order$Return;)Lcom/squareup/orders/model/Order$Return;

    move-result-object p1

    return-object p1
.end method
