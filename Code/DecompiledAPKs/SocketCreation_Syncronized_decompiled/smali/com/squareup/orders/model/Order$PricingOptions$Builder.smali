.class public final Lcom/squareup/orders/model/Order$PricingOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$PricingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$PricingOptions;",
        "Lcom/squareup/orders/model/Order$PricingOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public auto_apply_discounts:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15196
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public auto_apply_discounts(Ljava/lang/Boolean;)Lcom/squareup/orders/model/Order$PricingOptions$Builder;
    .locals 0

    .line 15203
    iput-object p1, p0, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->auto_apply_discounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$PricingOptions;
    .locals 3

    .line 15209
    new-instance v0, Lcom/squareup/orders/model/Order$PricingOptions;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->auto_apply_discounts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/orders/model/Order$PricingOptions;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 15193
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$PricingOptions$Builder;->build()Lcom/squareup/orders/model/Order$PricingOptions;

    move-result-object v0

    return-object v0
.end method
