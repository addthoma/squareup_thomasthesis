.class public final Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentShipmentDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CANCELED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCEL_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_CARRIER:Ljava/lang/String; = ""

.field public static final DEFAULT_DELIVERED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPECTED_DELIVERED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPECTED_SHIPPED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_FAILED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_FAILURE_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_IN_PROGRESS_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PACKAGED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_SHIPPED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_SHIPPING_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_SHIPPING_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_TRACKING_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_TRACKING_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final cancel_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final canceled_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final carrier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final delivered_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final expected_delivered_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final expected_shipped_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final failed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final failure_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final in_progress_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final packaged_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final placed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentRecipient#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final shipped_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final shipping_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final shipping_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final tracking_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final tracking_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9092
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;Lokio/ByteString;)V
    .locals 1

    .line 9337
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 9338
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9339
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->carrier:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    .line 9340
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    .line 9341
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_type:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    .line 9342
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    .line 9343
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    .line 9344
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->placed_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    .line 9345
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->in_progress_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    .line 9346
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->packaged_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    .line 9347
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_shipped_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    .line 9348
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipped_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    .line 9349
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_delivered_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    .line 9350
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->delivered_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    .line 9351
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->canceled_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    .line 9352
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->cancel_reason:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    .line 9353
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failed_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    .line 9354
    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failure_reason:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 9384
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 9385
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 9386
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9387
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    .line 9388
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    .line 9389
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    .line 9390
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    .line 9391
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    .line 9392
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    .line 9393
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    .line 9394
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    .line 9395
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    .line 9396
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    .line 9397
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    .line 9398
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    .line 9399
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    .line 9400
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    .line 9401
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    .line 9402
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    .line 9403
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 9408
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_11

    .line 9410
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 9411
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9412
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9413
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9414
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9415
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9416
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9417
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9418
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9419
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9420
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9421
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9422
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9423
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9424
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9425
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9426
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9427
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_10
    add-int/2addr v0, v2

    .line 9428
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_11
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 2

    .line 9359
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;-><init>()V

    .line 9360
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9361
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->carrier:Ljava/lang/String;

    .line 9362
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_note:Ljava/lang/String;

    .line 9363
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_type:Ljava/lang/String;

    .line 9364
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_number:Ljava/lang/String;

    .line 9365
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_url:Ljava/lang/String;

    .line 9366
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->placed_at:Ljava/lang/String;

    .line 9367
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->in_progress_at:Ljava/lang/String;

    .line 9368
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->packaged_at:Ljava/lang/String;

    .line 9369
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_shipped_at:Ljava/lang/String;

    .line 9370
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipped_at:Ljava/lang/String;

    .line 9371
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_delivered_at:Ljava/lang/String;

    .line 9372
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->delivered_at:Ljava/lang/String;

    .line 9373
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->canceled_at:Ljava/lang/String;

    .line 9374
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->cancel_reason:Ljava/lang/String;

    .line 9375
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failed_at:Ljava/lang/String;

    .line 9376
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failure_reason:Ljava/lang/String;

    .line 9377
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 9091
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 9435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9436
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v1, :cond_0

    const-string v1, ", recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9437
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", carrier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9438
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", shipping_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9439
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", shipping_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9440
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", tracking_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9441
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", tracking_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9442
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", placed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9443
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", in_progress_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9444
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", packaged_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9445
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", expected_shipped_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9446
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", shipped_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9447
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", expected_delivered_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9448
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", delivered_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9449
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9450
    :cond_d
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", cancel_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9451
    :cond_e
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", failed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9452
    :cond_f
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", failure_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentShipmentDetails{"

    .line 9453
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
