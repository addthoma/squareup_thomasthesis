.class public final Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentManagedDeliveryDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCEPTED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_PICKED_UP_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PICKUP_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_READY_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final accepted_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final picked_up_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final pickup_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final placed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final ready_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8853
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 8907
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 8912
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8913
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    .line 8914
    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    .line 8915
    iput-object p3, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    .line 8916
    iput-object p4, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    .line 8917
    iput-object p5, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    .line 8918
    iput-object p6, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8937
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8938
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    .line 8939
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    .line 8940
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    .line 8941
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    .line 8942
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    .line 8943
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    .line 8944
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    .line 8945
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8950
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 8952
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8953
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8954
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8955
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8956
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8957
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8958
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 8959
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 2

    .line 8923
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;-><init>()V

    .line 8924
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->note:Ljava/lang/String;

    .line 8925
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->placed_at:Ljava/lang/String;

    .line 8926
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->pickup_at:Ljava/lang/String;

    .line 8927
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->accepted_at:Ljava/lang/String;

    .line 8928
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->ready_at:Ljava/lang/String;

    .line 8929
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->picked_up_at:Ljava/lang/String;

    .line 8930
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 8852
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8966
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8967
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8968
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", placed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8969
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", pickup_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8970
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", accepted_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8971
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", ready_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8972
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", picked_up_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentManagedDeliveryDetails{"

    .line 8973
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
