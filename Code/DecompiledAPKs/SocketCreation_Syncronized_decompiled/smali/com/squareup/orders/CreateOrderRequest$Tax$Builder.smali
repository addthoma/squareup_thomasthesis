.class public final Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Tax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/CreateOrderRequest$Tax;",
        "Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 981
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/CreateOrderRequest$Tax;
    .locals 7

    .line 1034
    new-instance v6, Lcom/squareup/orders/CreateOrderRequest$Tax;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iget-object v4, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->percentage:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/CreateOrderRequest$Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 972
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Tax;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    .locals 0

    .line 992
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    .locals 0

    .line 1004
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    .locals 0

    .line 1028
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$LineItem$Tax$Type;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    .locals 0

    .line 1016
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    return-object p0
.end method
