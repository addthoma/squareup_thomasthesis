.class final Lcom/squareup/orders/CreateOrderRequest$Fulfillment$ProtoAdapter_Fulfillment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Fulfillment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1606
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Fulfillment;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1621
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;-><init>()V

    .line 1622
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1623
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1626
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1630
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1631
    invoke-virtual {v0}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1604
    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$ProtoAdapter_Fulfillment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Fulfillment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1616
    invoke-virtual {p2}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1604
    check-cast p2, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$ProtoAdapter_Fulfillment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Fulfillment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/CreateOrderRequest$Fulfillment;)I
    .locals 0

    .line 1611
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1604
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$ProtoAdapter_Fulfillment;->encodedSize(Lcom/squareup/orders/CreateOrderRequest$Fulfillment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/CreateOrderRequest$Fulfillment;)Lcom/squareup/orders/CreateOrderRequest$Fulfillment;
    .locals 0

    .line 1636
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;

    move-result-object p1

    .line 1637
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1638
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1604
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$ProtoAdapter_Fulfillment;->redact(Lcom/squareup/orders/CreateOrderRequest$Fulfillment;)Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    move-result-object p1

    return-object p1
.end method
