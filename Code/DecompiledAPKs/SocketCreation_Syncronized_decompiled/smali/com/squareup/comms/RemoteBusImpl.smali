.class public final Lcom/squareup/comms/RemoteBusImpl;
.super Ljava/lang/Object;
.source "RemoteBusImpl.java"

# interfaces
.implements Lcom/squareup/comms/RemoteBus;
.implements Lcom/squareup/comms/MessagePoster;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/RemoteBusImpl$ChannelListener;,
        Lcom/squareup/comms/RemoteBusImpl$State;
    }
.end annotation


# instance fields
.field private callbackExecutor:Ljava/util/concurrent/Executor;

.field private final channel:Lcom/squareup/comms/net/Channel;

.field private connection:Lcom/squareup/comms/RemoteBusConnectionImpl;

.field private connectionListener:Lcom/squareup/comms/ConnectionListener;

.field private device:Lcom/squareup/comms/net/Device;

.field private final healthChecker:Lcom/squareup/comms/HealthChecker;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;

.field private final serializer:Lcom/squareup/comms/Serializer;

.field private started:Z


# direct methods
.method public constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/Serializer;Lcom/squareup/comms/HealthChecker;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 29
    iput-boolean v0, p0, Lcom/squareup/comms/RemoteBusImpl;->started:Z

    .line 37
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 38
    iput-object p2, p0, Lcom/squareup/comms/RemoteBusImpl;->channel:Lcom/squareup/comms/net/Channel;

    .line 39
    iput-object p4, p0, Lcom/squareup/comms/RemoteBusImpl;->healthChecker:Lcom/squareup/comms/HealthChecker;

    .line 40
    iput-object p3, p0, Lcom/squareup/comms/RemoteBusImpl;->serializer:Lcom/squareup/comms/Serializer;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/HealthChecker;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->healthChecker:Lcom/squareup/comms/HealthChecker;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/ConnectionListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->connectionListener:Lcom/squareup/comms/ConnectionListener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/Serializer;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->serializer:Lcom/squareup/comms/Serializer;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/net/Channel;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->channel:Lcom/squareup/comms/net/Channel;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/net/Device;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->device:Lcom/squareup/comms/net/Device;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/net/Device;)Lcom/squareup/comms/net/Device;
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl;->device:Lcom/squareup/comms/net/Device;

    return-object p1
.end method

.method static synthetic access$500(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/common/IoThread;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->connection:Lcom/squareup/comms/RemoteBusConnectionImpl;

    return-object p0
.end method

.method static synthetic access$802(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusConnectionImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl;->connection:Lcom/squareup/comms/RemoteBusConnectionImpl;

    return-object p1
.end method

.method static synthetic access$900(Lcom/squareup/comms/RemoteBusImpl;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl;->callbackExecutor:Ljava/util/concurrent/Executor;

    return-object p0
.end method


# virtual methods
.method public post(Lcom/squareup/wire/Message;)V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/RemoteBusImpl$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/comms/RemoteBusImpl$2;-><init>(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public start(Lcom/squareup/comms/ConnectionListener;Ljava/util/concurrent/Executor;)V
    .locals 3

    .line 44
    iget-boolean v0, p0, Lcom/squareup/comms/RemoteBusImpl;->started:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "Duplicate call to start detected"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "RemoteBusImpl starting"

    .line 45
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    iput-boolean v1, p0, Lcom/squareup/comms/RemoteBusImpl;->started:Z

    .line 47
    invoke-static {p2, p1}, Lcom/squareup/comms/net/Callbacks;->serializedConnectionListener(Ljava/util/concurrent/Executor;Lcom/squareup/comms/ConnectionListener;)Lcom/squareup/comms/ConnectionListener;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl;->connectionListener:Lcom/squareup/comms/ConnectionListener;

    .line 48
    iput-object p2, p0, Lcom/squareup/comms/RemoteBusImpl;->callbackExecutor:Ljava/util/concurrent/Executor;

    .line 49
    iget-object p1, p0, Lcom/squareup/comms/RemoteBusImpl;->channel:Lcom/squareup/comms/net/Channel;

    new-instance p2, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    const/4 v0, 0x0

    invoke-direct {p2, p0, v0}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;-><init>(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusImpl$1;)V

    invoke-virtual {p1, p2}, Lcom/squareup/comms/net/Channel;->start(Lcom/squareup/comms/net/ChannelCallback;)V

    return-void
.end method

.method public stop()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/RemoteBusImpl$1;

    invoke-direct {v1, p0}, Lcom/squareup/comms/RemoteBusImpl$1;-><init>(Lcom/squareup/comms/RemoteBusImpl;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->postAndWait(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 59
    iput-object v0, p0, Lcom/squareup/comms/RemoteBusImpl;->connectionListener:Lcom/squareup/comms/ConnectionListener;

    .line 60
    iput-object v0, p0, Lcom/squareup/comms/RemoteBusImpl;->callbackExecutor:Ljava/util/concurrent/Executor;

    .line 61
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl;->channel:Lcom/squareup/comms/net/Channel;

    invoke-virtual {v0}, Lcom/squareup/comms/net/Channel;->close()V

    return-void
.end method
