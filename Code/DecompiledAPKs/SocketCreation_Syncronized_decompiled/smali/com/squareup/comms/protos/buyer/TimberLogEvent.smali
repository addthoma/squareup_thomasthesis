.class public final Lcom/squareup/comms/protos/buyer/TimberLogEvent;
.super Lcom/squareup/wire/AndroidMessage;
.source "TimberLogEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;,
        Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTimberLogEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TimberLogEvent.kt\ncom/squareup/comms/protos/buyer/TimberLogEvent\n*L\n1#1,202:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0006\u0018\u0000 \u00162\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0015\u0016B;\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ>\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bJ\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0096\u0002J\u0008\u0010\u0012\u001a\u00020\u0004H\u0016J\u0008\u0010\u0013\u001a\u00020\u0002H\u0016J\u0008\u0010\u0014\u001a\u00020\u0006H\u0016R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;",
        "priority",
        "",
        "tag",
        "",
        "message",
        "throwable_data",
        "Lcom/squareup/comms/protos/buyer/ThrowableData;",
        "unknownFields",
        "Lokio/ByteString;",
        "(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "newBuilder",
        "toString",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion;


# instance fields
.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final priority:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final tag:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.ThrowableData#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->Companion:Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion;

    .line 150
    new-instance v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion$ADAPTER$1;

    .line 151
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 152
    const-class v2, Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 199
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput p1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    iput-object p2, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 35
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_0
    move-object v3, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    .line 44
    move-object p3, v0

    check-cast p3, Ljava/lang/String;

    :cond_1
    move-object v4, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_2

    .line 50
    move-object p4, v0

    check-cast p4, Lcom/squareup/comms/protos/buyer/ThrowableData;

    :cond_2
    move-object v5, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_3

    .line 51
    sget-object p5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_3
    move-object v6, p5

    move-object v1, p0

    move v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/TimberLogEvent;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/TimberLogEvent;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    .line 96
    iget p1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 97
    iget-object p2, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    .line 98
    iget-object p3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 99
    iget-object p4, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 100
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->copy(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/TimberLogEvent;
    .locals 7

    const-string v0, "unknownFields"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 64
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 65
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    iget v3, p1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 74
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 76
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 81
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;
    .locals 2

    .line 54
    new-instance v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;-><init>()V

    .line 55
    iget v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->priority:Ljava/lang/Integer;

    .line 56
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->tag:Ljava/lang/String;

    .line 57
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->message:Ljava/lang/String;

    .line 58
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->newBuilder()Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 88
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "priority="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->priority:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->tag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_1
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "throwable_data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_2
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "TimberLogEvent{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
