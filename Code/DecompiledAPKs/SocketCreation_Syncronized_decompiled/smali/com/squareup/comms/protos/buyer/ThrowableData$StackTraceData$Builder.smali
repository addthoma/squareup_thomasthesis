.class public final Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ThrowableData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
        "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
        "()V",
        "declaring_class",
        "",
        "file_name",
        "line_number",
        "",
        "Ljava/lang/Integer;",
        "method_name",
        "build",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public declaring_class:Ljava/lang/String;

.field public file_name:Ljava/lang/String;

.field public line_number:Ljava/lang/Integer;

.field public method_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 278
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;
    .locals 7

    .line 311
    new-instance v6, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;

    .line 312
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->declaring_class:Ljava/lang/String;

    .line 313
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->method_name:Ljava/lang/String;

    .line 314
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->file_name:Ljava/lang/String;

    .line 315
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->line_number:Ljava/lang/Integer;

    .line 316
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 311
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 278
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->build()Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final declaring_class(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->declaring_class:Ljava/lang/String;

    return-object p0
.end method

.method public final file_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->file_name:Ljava/lang/String;

    return-object p0
.end method

.method public final line_number(Ljava/lang/Integer;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->line_number:Ljava/lang/Integer;

    return-object p0
.end method

.method public final method_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->method_name:Ljava/lang/String;

    return-object p0
.end method
