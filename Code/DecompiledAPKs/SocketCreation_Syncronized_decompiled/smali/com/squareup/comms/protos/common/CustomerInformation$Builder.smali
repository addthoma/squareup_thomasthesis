.class public final Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomerInformation.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/common/CustomerInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/common/CustomerInformation;",
        "Lcom/squareup/comms/protos/common/CustomerInformation$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0008\u0010\u0011\u001a\u00020\u0002H\u0016J\u0010\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u000b\u001a\u00020\u00002\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u000c\u001a\u00020\u00002\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0010\u001a\u00020\u00002\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/CustomerInformation$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/common/CustomerInformation;",
        "()V",
        "apartment",
        "",
        "birthday",
        "Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;",
        "city",
        "company",
        "email",
        "first_name",
        "last_name",
        "phone_number",
        "postal_code",
        "state",
        "street_address",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public apartment:Ljava/lang/String;

.field public birthday:Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

.field public city:Ljava/lang/String;

.field public company:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public first_name:Ljava/lang/String;

.field public last_name:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public postal_code:Ljava/lang/String;

.field public state:Ljava/lang/String;

.field public street_address:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 180
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final apartment(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->apartment:Ljava/lang/String;

    return-object p0
.end method

.method public final birthday(Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->birthday:Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/common/CustomerInformation;
    .locals 14

    .line 269
    new-instance v13, Lcom/squareup/comms/protos/common/CustomerInformation;

    .line 270
    iget-object v1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->first_name:Ljava/lang/String;

    .line 271
    iget-object v2, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->last_name:Ljava/lang/String;

    .line 272
    iget-object v3, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->email:Ljava/lang/String;

    .line 273
    iget-object v4, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->phone_number:Ljava/lang/String;

    .line 274
    iget-object v5, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->company:Ljava/lang/String;

    .line 275
    iget-object v6, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->street_address:Ljava/lang/String;

    .line 276
    iget-object v7, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->apartment:Ljava/lang/String;

    .line 277
    iget-object v8, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->city:Ljava/lang/String;

    .line 278
    iget-object v9, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->state:Ljava/lang/String;

    .line 279
    iget-object v10, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->postal_code:Ljava/lang/String;

    .line 280
    iget-object v11, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->birthday:Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    .line 281
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    .line 269
    invoke-direct/range {v0 .. v12}, Lcom/squareup/comms/protos/common/CustomerInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 180
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->build()Lcom/squareup/comms/protos/common/CustomerInformation;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final city(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->city:Ljava/lang/String;

    return-object p0
.end method

.method public final company(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->company:Ljava/lang/String;

    return-object p0
.end method

.method public final email(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public final first_name(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->first_name:Ljava/lang/String;

    return-object p0
.end method

.method public final last_name(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->last_name:Ljava/lang/String;

    return-object p0
.end method

.method public final phone_number(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public final postal_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method

.method public final state(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->state:Ljava/lang/String;

    return-object p0
.end method

.method public final street_address(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->street_address:Ljava/lang/String;

    return-object p0
.end method
