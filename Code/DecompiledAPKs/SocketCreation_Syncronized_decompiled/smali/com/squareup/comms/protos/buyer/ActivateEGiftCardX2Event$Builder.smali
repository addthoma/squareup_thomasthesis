.class public final Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ActivateEGiftCardX2Event.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;",
        "Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0007\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;",
        "()V",
        "event_snapshot",
        "Lokio/ByteString;",
        "state_snapshot",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public event_snapshot:Lokio/ByteString;

.field public state_snapshot:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;
    .locals 6

    .line 107
    new-instance v0, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;

    .line 108
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;->event_snapshot:Lokio/ByteString;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    .line 110
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;->state_snapshot:Lokio/ByteString;

    if-eqz v5, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 107
    invoke-direct {v0, v1, v5, v2}, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v5, v0, v3

    const-string v1, "state_snapshot"

    aput-object v1, v0, v2

    .line 110
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "event_snapshot"

    aput-object v1, v0, v2

    .line 108
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;->build()Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final event_snapshot(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;
    .locals 1

    const-string v0, "event_snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;->event_snapshot:Lokio/ByteString;

    return-object p0
.end method

.method public final state_snapshot(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;
    .locals 1

    const-string v0, "state_snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event$Builder;->state_snapshot:Lokio/ByteString;

    return-object p0
.end method
