.class public interface abstract Lcom/squareup/comms/HealthChecker$Callback;
.super Ljava/lang/Object;
.source "HealthChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/HealthChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onHealthy()V
.end method

.method public abstract onUnhealthy()V
.end method
