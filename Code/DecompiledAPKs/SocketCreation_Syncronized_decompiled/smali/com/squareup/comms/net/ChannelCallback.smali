.class public interface abstract Lcom/squareup/comms/net/ChannelCallback;
.super Ljava/lang/Object;
.source "ChannelCallback.java"


# virtual methods
.method public abstract onConnected(Lcom/squareup/comms/net/Device;)V
.end method

.method public abstract onDisconnected()V
.end method

.method public abstract onError(Ljava/lang/Exception;)V
.end method

.method public abstract onReceive([BII)V
.end method
