.class public interface abstract Lcom/squareup/comms/net/ConnectionCallback;
.super Ljava/lang/Object;
.source "ConnectionCallback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/net/ConnectionCallback$Null;
    }
.end annotation


# virtual methods
.method public abstract onDisconnect()V
.end method

.method public abstract onError(Ljava/lang/Exception;)V
.end method

.method public abstract onReceive([BII)V
.end method
