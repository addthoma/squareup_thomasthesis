.class public final Lcom/squareup/comms/net/Channel;
.super Ljava/lang/Object;
.source "Channel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/net/Channel$ConnectionListener;,
        Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;
    }
.end annotation


# instance fields
.field private callback:Lcom/squareup/comms/net/ChannelCallback;

.field private closed:Z

.field private connection:Lcom/squareup/comms/net/Connection;

.field private final connectionFactory:Lcom/squareup/comms/net/ConnectionFactory;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p2, p0, Lcom/squareup/comms/net/Channel;->connectionFactory:Lcom/squareup/comms/net/ConnectionFactory;

    .line 31
    iput-object p1, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/comms/net/Channel;->callback:Lcom/squareup/comms/net/ChannelCallback;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/ChannelCallback;)Lcom/squareup/comms/net/ChannelCallback;
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/comms/net/Channel;->callback:Lcom/squareup/comms/net/ChannelCallback;

    return-object p1
.end method

.method static synthetic access$100(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/common/IoThread;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ConnectionFactory;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/comms/net/Channel;->connectionFactory:Lcom/squareup/comms/net/ConnectionFactory;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/comms/net/Channel;->connection:Lcom/squareup/comms/net/Connection;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)Lcom/squareup/comms/net/Connection;
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/comms/net/Channel;->connection:Lcom/squareup/comms/net/Connection;

    return-object p1
.end method

.method static synthetic access$500(Lcom/squareup/comms/net/Channel;)Z
    .locals 0

    .line 14
    iget-boolean p0, p0, Lcom/squareup/comms/net/Channel;->closed:Z

    return p0
.end method

.method static synthetic access$502(Lcom/squareup/comms/net/Channel;Z)Z
    .locals 0

    .line 14
    iput-boolean p1, p0, Lcom/squareup/comms/net/Channel;->closed:Z

    return p1
.end method


# virtual methods
.method public close()V
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Channel$2;

    invoke-direct {v1, p0}, Lcom/squareup/comms/net/Channel$2;-><init>(Lcom/squareup/comms/net/Channel;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->postAndWait(Ljava/lang/Runnable;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    invoke-virtual {v0}, Lcom/squareup/comms/common/IoThread;->close()V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 74
    iget-object v1, p0, Lcom/squareup/comms/net/Channel;->connection:Lcom/squareup/comms/net/Connection;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Channel closed: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public disconnect()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Disconnecting and reconnecting"

    .line 101
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Channel$4;

    invoke-direct {v1, p0}, Lcom/squareup/comms/net/Channel$4;-><init>(Lcom/squareup/comms/net/Channel;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->postAndWait(Ljava/lang/Runnable;)V

    return-void
.end method

.method public send([BII)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Channel$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/comms/net/Channel$3;-><init>(Lcom/squareup/comms/net/Channel;[BII)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public start(Lcom/squareup/comms/net/ChannelCallback;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting channel"

    .line 43
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/comms/net/Channel;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Channel$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/comms/net/Channel$1;-><init>(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/ChannelCallback;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method
