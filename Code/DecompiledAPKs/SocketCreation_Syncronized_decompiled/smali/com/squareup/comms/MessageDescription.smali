.class public Lcom/squareup/comms/MessageDescription;
.super Ljava/lang/Object;
.source "MessageDescription.java"


# instance fields
.field private final messageClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final origin:Lcom/squareup/comms/MessageOrigin;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/MessageOrigin;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/comms/MessageOrigin;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/comms/MessageDescription;->origin:Lcom/squareup/comms/MessageOrigin;

    .line 20
    iput-object p2, p0, Lcom/squareup/comms/MessageDescription;->messageClass:Ljava/lang/Class;

    return-void
.end method

.method public static bran(Ljava/lang/Class;)Lcom/squareup/comms/MessageDescription;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;)",
            "Lcom/squareup/comms/MessageDescription;"
        }
    .end annotation

    .line 11
    new-instance v0, Lcom/squareup/comms/MessageDescription;

    sget-object v1, Lcom/squareup/comms/MessageOrigin;->BRAN:Lcom/squareup/comms/MessageOrigin;

    invoke-direct {v0, v1, p0}, Lcom/squareup/comms/MessageDescription;-><init>(Lcom/squareup/comms/MessageOrigin;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static hodor(Ljava/lang/Class;)Lcom/squareup/comms/MessageDescription;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;)",
            "Lcom/squareup/comms/MessageDescription;"
        }
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/comms/MessageDescription;

    sget-object v1, Lcom/squareup/comms/MessageOrigin;->HODOR:Lcom/squareup/comms/MessageOrigin;

    invoke-direct {v0, v1, p0}, Lcom/squareup/comms/MessageDescription;-><init>(Lcom/squareup/comms/MessageOrigin;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 27
    :cond_1
    check-cast p1, Lcom/squareup/comms/MessageDescription;

    .line 29
    iget-object v2, p0, Lcom/squareup/comms/MessageDescription;->messageClass:Ljava/lang/Class;

    iget-object v3, p1, Lcom/squareup/comms/MessageDescription;->messageClass:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 30
    :cond_2
    iget-object v2, p0, Lcom/squareup/comms/MessageDescription;->origin:Lcom/squareup/comms/MessageOrigin;

    iget-object p1, p1, Lcom/squareup/comms/MessageDescription;->origin:Lcom/squareup/comms/MessageOrigin;

    if-eq v2, p1, :cond_3

    return v1

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/comms/MessageDescription;->messageClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 37
    iget-object v1, p0, Lcom/squareup/comms/MessageDescription;->origin:Lcom/squareup/comms/MessageOrigin;

    invoke-virtual {v1}, Lcom/squareup/comms/MessageOrigin;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/comms/MessageDescription;->origin:Lcom/squareup/comms/MessageOrigin;

    invoke-virtual {v1}, Lcom/squareup/comms/MessageOrigin;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/comms/MessageDescription;->messageClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
