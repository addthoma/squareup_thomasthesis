.class final enum Lcom/caverock/androidsvg/SVGParser$SVGAttr;
.super Ljava/lang/Enum;
.source "SVGParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/caverock/androidsvg/SVGParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SVGAttr"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/caverock/androidsvg/SVGParser$SVGAttr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum CLASS:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field private static final cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/caverock/androidsvg/SVGParser$SVGAttr;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum clipPathUnits:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum cx:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum cy:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum d:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum font:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum height:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum pathLength:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum points:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum preserveAspectRatio:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum r:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum rx:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum ry:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum transform:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum version:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum viewBox:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum width:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum x:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum x1:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum x2:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum y:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum y1:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

.field public static final enum y2:Lcom/caverock/androidsvg/SVGParser$SVGAttr;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 114
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v1, 0x0

    const-string v2, "CLASS"

    invoke-direct {v0, v2, v1}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->CLASS:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 115
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v2, 0x1

    const-string v3, "clipPathUnits"

    invoke-direct {v0, v3, v2}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->clipPathUnits:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 116
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v3, 0x2

    const-string v4, "cx"

    invoke-direct {v0, v4, v3}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cx:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v4, 0x3

    const-string v5, "cy"

    invoke-direct {v0, v5, v4}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cy:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 117
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v5, 0x4

    const-string v6, "d"

    invoke-direct {v0, v6, v5}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->d:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 118
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v6, 0x5

    const-string v7, "font"

    invoke-direct {v0, v7, v6}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->font:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 120
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v7, 0x6

    const-string v8, "height"

    invoke-direct {v0, v8, v7}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->height:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 122
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/4 v8, 0x7

    const-string v9, "pathLength"

    invoke-direct {v0, v9, v8}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->pathLength:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 123
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v9, 0x8

    const-string v10, "points"

    invoke-direct {v0, v10, v9}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->points:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 124
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v10, 0x9

    const-string v11, "preserveAspectRatio"

    invoke-direct {v0, v11, v10}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->preserveAspectRatio:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 125
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v11, 0xa

    const-string v12, "r"

    invoke-direct {v0, v12, v11}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->r:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 126
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v12, 0xb

    const-string v13, "rx"

    invoke-direct {v0, v13, v12}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->rx:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v13, 0xc

    const-string v14, "ry"

    invoke-direct {v0, v14, v13}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->ry:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 127
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v14, 0xd

    const-string v15, "transform"

    invoke-direct {v0, v15, v14}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->transform:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 128
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v15, 0xe

    const-string v14, "version"

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->version:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 129
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string v14, "viewBox"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->viewBox:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 130
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string v14, "width"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->width:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 131
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string/jumbo v14, "x"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->x:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string/jumbo v14, "y"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->y:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 132
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string/jumbo v14, "x1"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->x1:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string/jumbo v14, "y1"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->y1:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 133
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string/jumbo v14, "x2"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->x2:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string/jumbo v14, "y2"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->y2:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 134
    new-instance v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const-string v14, "UNSUPPORTED"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 112
    sget-object v14, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->CLASS:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v14, v0, v1

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->clipPathUnits:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cx:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cy:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v4

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->d:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v5

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->font:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v6

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->height:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v7

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->pathLength:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v8

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->points:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v9

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->preserveAspectRatio:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v10

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->r:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v11

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->rx:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v12

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->ry:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    aput-object v1, v0, v13

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->transform:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->version:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->viewBox:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->width:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->x:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->y:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->x1:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->y1:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->x2:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->y2:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->$VALUES:[Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    .line 136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cache:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/caverock/androidsvg/SVGParser$SVGAttr;
    .locals 3

    .line 141
    sget-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cache:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "class"

    .line 145
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    sget-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cache:Ljava/util/Map;

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->CLASS:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object p0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->CLASS:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    return-object p0

    :cond_1
    const/16 v0, 0x5f

    .line 150
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 151
    sget-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cache:Ljava/util/Map;

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object p0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    return-object p0

    :cond_2
    const/16 v1, 0x2d

    .line 156
    :try_start_0
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->valueOf(Ljava/lang/String;)Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    move-result-object v0

    .line 157
    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->CLASS:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    if-eq v0, v1, :cond_3

    .line 158
    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cache:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 167
    :catch_0
    :cond_3
    sget-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->cache:Ljava/util/Map;

    sget-object v1, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object p0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->UNSUPPORTED:Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/caverock/androidsvg/SVGParser$SVGAttr;
    .locals 1

    .line 112
    const-class v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    return-object p0
.end method

.method public static values()[Lcom/caverock/androidsvg/SVGParser$SVGAttr;
    .locals 1

    .line 112
    sget-object v0, Lcom/caverock/androidsvg/SVGParser$SVGAttr;->$VALUES:[Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    invoke-virtual {v0}, [Lcom/caverock/androidsvg/SVGParser$SVGAttr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/caverock/androidsvg/SVGParser$SVGAttr;

    return-object v0
.end method
