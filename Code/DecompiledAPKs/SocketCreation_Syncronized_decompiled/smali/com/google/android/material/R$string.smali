.class public final Lcom/google/android/material/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f120001

.field public static final abc_action_bar_up_description:I = 0x7f120002

.field public static final abc_action_menu_overflow_description:I = 0x7f120003

.field public static final abc_action_mode_done:I = 0x7f120004

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120005

.field public static final abc_activitychooserview_choose_application:I = 0x7f120006

.field public static final abc_capital_off:I = 0x7f120007

.field public static final abc_capital_on:I = 0x7f120008

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120009

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f12000a

.field public static final abc_menu_delete_shortcut_label:I = 0x7f12000b

.field public static final abc_menu_enter_shortcut_label:I = 0x7f12000c

.field public static final abc_menu_function_shortcut_label:I = 0x7f12000d

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_space_shortcut_label:I = 0x7f120010

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120011

.field public static final abc_prepend_shortcut_label:I = 0x7f120012

.field public static final abc_search_hint:I = 0x7f120013

.field public static final abc_searchview_description_clear:I = 0x7f120014

.field public static final abc_searchview_description_query:I = 0x7f120015

.field public static final abc_searchview_description_search:I = 0x7f120016

.field public static final abc_searchview_description_submit:I = 0x7f120017

.field public static final abc_searchview_description_voice:I = 0x7f120018

.field public static final abc_shareactionprovider_share_with:I = 0x7f120019

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f12001a

.field public static final abc_toolbar_collapse_description:I = 0x7f12001b

.field public static final appbar_scrolling_view_behavior:I = 0x7f1200d2

.field public static final bottom_sheet_behavior:I = 0x7f120190

.field public static final character_counter_content_description:I = 0x7f1203f3

.field public static final character_counter_overflowed_content_description:I = 0x7f1203f4

.field public static final character_counter_pattern:I = 0x7f1203f5

.field public static final chip_text:I = 0x7f12041c

.field public static final clear_text_end_icon_content_description:I = 0x7f120428

.field public static final error_icon_content_description:I = 0x7f120a85

.field public static final exposed_dropdown_menu_content_description:I = 0x7f120aa1

.field public static final fab_transformation_scrim_behavior:I = 0x7f120aa4

.field public static final fab_transformation_sheet_behavior:I = 0x7f120aa5

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120b51

.field public static final icon_content_description:I = 0x7f120bef

.field public static final mtrl_badge_numberless_content_description:I = 0x7f121025

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f121026

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f121027

.field public static final mtrl_picker_a11y_next_month:I = 0x7f121028

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f121029

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12102a

.field public static final mtrl_picker_cancel:I = 0x7f12102b

.field public static final mtrl_picker_confirm:I = 0x7f12102c

.field public static final mtrl_picker_date_header_selected:I = 0x7f12102d

.field public static final mtrl_picker_date_header_title:I = 0x7f12102e

.field public static final mtrl_picker_date_header_unselected:I = 0x7f12102f

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f121030

.field public static final mtrl_picker_invalid_format:I = 0x7f121031

.field public static final mtrl_picker_invalid_format_example:I = 0x7f121032

.field public static final mtrl_picker_invalid_format_use:I = 0x7f121033

.field public static final mtrl_picker_invalid_range:I = 0x7f121034

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f121035

.field public static final mtrl_picker_out_of_range:I = 0x7f121036

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f121037

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f121038

.field public static final mtrl_picker_range_header_selected:I = 0x7f121039

.field public static final mtrl_picker_range_header_title:I = 0x7f12103a

.field public static final mtrl_picker_range_header_unselected:I = 0x7f12103b

.field public static final mtrl_picker_save:I = 0x7f12103c

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f12103d

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f12103e

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f12103f

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f121040

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f121041

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f121042

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f121043

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f121044

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f121045

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f121046

.field public static final password_toggle_content_description:I = 0x7f121391

.field public static final path_password_eye:I = 0x7f121394

.field public static final path_password_eye_mask_strike_through:I = 0x7f121395

.field public static final path_password_eye_mask_visible:I = 0x7f121396

.field public static final path_password_strike_through:I = 0x7f121397

.field public static final search_menu_title:I = 0x7f121790

.field public static final status_bar_notification_info_overflow:I = 0x7f1218ca


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
