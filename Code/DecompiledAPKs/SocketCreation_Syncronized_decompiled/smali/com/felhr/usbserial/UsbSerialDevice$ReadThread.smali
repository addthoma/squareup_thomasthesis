.class public Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "UsbSerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/UsbSerialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ReadThread"
.end annotation


# instance fields
.field private callback:Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field final synthetic this$0:Lcom/felhr/usbserial/UsbSerialDevice;

.field private final usbSerialDevice:Lcom/felhr/usbserial/UsbSerialDevice;


# direct methods
.method public constructor <init>(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice;)V
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    .line 422
    iput-object p2, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->usbSerialDevice:Lcom/felhr/usbserial/UsbSerialDevice;

    return-void
.end method

.method private onReceivedData([B)V
    .locals 1

    .line 470
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->callback:Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;

    if-eqz v0, :cond_0

    .line 471
    invoke-interface {v0, p1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;->onReceivedData([B)V

    :cond_0
    return-void
.end method


# virtual methods
.method public doRun()V
    .locals 5

    .line 435
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v3, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v3, v3, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v3}, Lcom/felhr/usbserial/SerialBuffer;->getBufferCompatible()[B

    move-result-object v3

    const/16 v4, 0x4000

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    :cond_0
    if-lez v1, :cond_2

    .line 443
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0, v1}, Lcom/felhr/usbserial/SerialBuffer;->getDataReceivedCompatible(I)[B

    move-result-object v0

    .line 447
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/UsbSerialDevice;->access$000(Lcom/felhr/usbserial/UsbSerialDevice;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 449
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->usbSerialDevice:Lcom/felhr/usbserial/UsbSerialDevice;

    check-cast v1, Lcom/felhr/usbserial/FTDISerialDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/FTDISerialDevice;->ftdiUtilities:Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

    invoke-virtual {v1, v0}, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->checkModemStatus([B)V

    .line 451
    array-length v1, v0

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    .line 453
    invoke-static {v0}, Lcom/felhr/usbserial/FTDISerialDevice;->adaptArray([B)[B

    move-result-object v0

    .line 454
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->onReceivedData([B)V

    goto :goto_0

    .line 458
    :cond_1
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->onReceivedData([B)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setCallback(Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;)V
    .locals 0

    .line 427
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->callback:Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;

    return-void
.end method

.method public setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V
    .locals 0

    .line 465
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method
