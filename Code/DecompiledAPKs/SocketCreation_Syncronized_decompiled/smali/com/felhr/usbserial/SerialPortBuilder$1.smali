.class Lcom/felhr/usbserial/SerialPortBuilder$1;
.super Landroid/content/BroadcastReceiver;
.source "SerialPortBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/SerialPortBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felhr/usbserial/SerialPortBuilder;


# direct methods
.method constructor <init>(Lcom/felhr/usbserial/SerialPortBuilder;)V
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 203
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "com.felhr.usbserial.USB_PERMISSION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 204
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "permission"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    .line 207
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$100(Lcom/felhr/usbserial/SerialPortBuilder;)Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    move-result-object v0

    iget-object v0, v0, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;->usbDeviceStatus:Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    invoke-static {p1, v0}, Lcom/felhr/usbserial/SerialPortBuilder;->access$200(Lcom/felhr/usbserial/SerialPortBuilder;Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)V

    .line 208
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$300(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/concurrent/ArrayBlockingQueue;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/ArrayBlockingQueue;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 209
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$400(Lcom/felhr/usbserial/SerialPortBuilder;)V

    goto :goto_0

    .line 211
    :cond_0
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1, p2}, Lcom/felhr/usbserial/SerialPortBuilder;->access$502(Lcom/felhr/usbserial/SerialPortBuilder;Z)Z

    .line 212
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$600(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result p1

    if-nez p1, :cond_1

    .line 213
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$800(Lcom/felhr/usbserial/SerialPortBuilder;)Lcom/felhr/usbserial/SerialPortCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p2}, Lcom/felhr/usbserial/SerialPortBuilder;->access$700(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/felhr/usbserial/SerialPortCallback;->onSerialPortsDetected(Ljava/util/List;)V

    goto :goto_0

    .line 215
    :cond_1
    new-instance p1, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;

    iget-object p2, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p2}, Lcom/felhr/usbserial/SerialPortBuilder;->access$700(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;Ljava/util/List;)V

    .line 216
    invoke-virtual {p1}, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->start()V

    goto :goto_0

    .line 220
    :cond_2
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$300(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/concurrent/ArrayBlockingQueue;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/ArrayBlockingQueue;->size()I

    move-result p1

    if-lez p1, :cond_3

    .line 221
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$400(Lcom/felhr/usbserial/SerialPortBuilder;)V

    goto :goto_0

    .line 223
    :cond_3
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1, p2}, Lcom/felhr/usbserial/SerialPortBuilder;->access$502(Lcom/felhr/usbserial/SerialPortBuilder;Z)Z

    .line 224
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$600(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result p1

    if-nez p1, :cond_4

    .line 225
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$800(Lcom/felhr/usbserial/SerialPortBuilder;)Lcom/felhr/usbserial/SerialPortCallback;

    move-result-object p1

    iget-object p2, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p2}, Lcom/felhr/usbserial/SerialPortBuilder;->access$700(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/felhr/usbserial/SerialPortCallback;->onSerialPortsDetected(Ljava/util/List;)V

    goto :goto_0

    .line 227
    :cond_4
    new-instance p1, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;

    iget-object p2, p0, Lcom/felhr/usbserial/SerialPortBuilder$1;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {p2}, Lcom/felhr/usbserial/SerialPortBuilder;->access$700(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;Ljava/util/List;)V

    .line 228
    invoke-virtual {p1}, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->start()V

    :cond_5
    :goto_0
    return-void
.end method
