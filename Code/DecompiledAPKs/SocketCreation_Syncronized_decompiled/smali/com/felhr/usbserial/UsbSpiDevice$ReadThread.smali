.class public Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "UsbSpiDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/UsbSpiDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ReadThread"
.end annotation


# instance fields
.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private misoCallback:Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;

.field final synthetic this$0:Lcom/felhr/usbserial/UsbSpiDevice;


# direct methods
.method protected constructor <init>(Lcom/felhr/usbserial/UsbSpiDevice;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    return-void
.end method

.method private onReceivedData([B)V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->misoCallback:Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;

    if-eqz v0, :cond_0

    .line 138
    invoke-interface {v0, p1}, Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;->onReceivedData([B)I

    :cond_0
    return-void
.end method


# virtual methods
.method public doRun()V
    .locals 5

    .line 116
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v3, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    iget-object v3, v3, Lcom/felhr/usbserial/UsbSpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v3}, Lcom/felhr/usbserial/SerialBuffer;->getBufferCompatible()[B

    move-result-object v3

    const/16 v4, 0x4000

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    :cond_0
    if-lez v1, :cond_1

    .line 124
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->this$0:Lcom/felhr/usbserial/UsbSpiDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0, v1}, Lcom/felhr/usbserial/SerialBuffer;->getDataReceivedCompatible(I)[B

    move-result-object v0

    .line 125
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->onReceivedData([B)V

    :cond_1
    return-void
.end method

.method public setCallback(Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->misoCallback:Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;

    return-void
.end method

.method public setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method
