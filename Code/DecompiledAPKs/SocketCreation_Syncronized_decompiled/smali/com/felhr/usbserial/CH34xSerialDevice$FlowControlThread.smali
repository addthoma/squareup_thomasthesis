.class Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "CH34xSerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/CH34xSerialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlowControlThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

.field private final time:J


# direct methods
.method private constructor <init>(Lcom/felhr/usbserial/CH34xSerialDevice;)V
    .locals 2

    .line 631
    iput-object p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    const-wide/16 v0, 0x64

    .line 633
    iput-wide v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->time:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/felhr/usbserial/CH34xSerialDevice;Lcom/felhr/usbserial/CH34xSerialDevice$1;)V
    .locals 0

    .line 631
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;-><init>(Lcom/felhr/usbserial/CH34xSerialDevice;)V

    return-void
.end method


# virtual methods
.method public doRun()V
    .locals 2

    .line 638
    iget-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->firstTime:Z

    if-nez v0, :cond_1

    .line 641
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$100(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 643
    invoke-virtual {p0}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->pollForCTS()Z

    move-result v0

    .line 644
    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$200(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$200(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$202(Lcom/felhr/usbserial/CH34xSerialDevice;Z)Z

    .line 647
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$300(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$300(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$200(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;->onCTSChanged(Z)V

    .line 653
    :cond_0
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$400(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 655
    invoke-virtual {p0}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->pollForDSR()Z

    move-result v0

    .line 656
    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$500(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    if-eq v1, v0, :cond_4

    .line 658
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$500(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$502(Lcom/felhr/usbserial/CH34xSerialDevice;Z)Z

    .line 659
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$600(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 660
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$600(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$500(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;->onDSRChanged(Z)V

    goto :goto_0

    .line 665
    :cond_1
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$100(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$300(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 666
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$300(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$200(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;->onCTSChanged(Z)V

    .line 668
    :cond_2
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$400(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$600(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 669
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$600(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$500(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;->onDSRChanged(Z)V

    :cond_3
    const/4 v0, 0x0

    .line 671
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->firstTime:Z

    :cond_4
    :goto_0
    return-void
.end method

.method public pollForCTS()Z
    .locals 2

    .line 677
    monitor-enter p0

    const-wide/16 v0, 0x64

    .line 681
    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 684
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 686
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 688
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$700(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v0

    return v0

    .line 686
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public pollForDSR()Z
    .locals 2

    .line 693
    monitor-enter p0

    const-wide/16 v0, 0x64

    .line 697
    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 700
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 702
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 704
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->access$800(Lcom/felhr/usbserial/CH34xSerialDevice;)Z

    move-result v0

    return v0

    .line 702
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
