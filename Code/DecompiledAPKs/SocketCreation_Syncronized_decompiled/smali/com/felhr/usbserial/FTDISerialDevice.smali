.class public Lcom/felhr/usbserial/FTDISerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "FTDISerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;
    }
.end annotation


# static fields
.field private static final CLASS_ID:Ljava/lang/String;

.field private static final EMPTY_BYTE_ARRAY:[B

.field public static final FTDI_BAUDRATE_115200:I = 0x1a

.field public static final FTDI_BAUDRATE_1200:I = 0x9c4

.field public static final FTDI_BAUDRATE_19200:I = 0x809c

.field public static final FTDI_BAUDRATE_230400:I = 0xd

.field public static final FTDI_BAUDRATE_2400:I = 0x4e2

.field public static final FTDI_BAUDRATE_300:I = 0x2710

.field public static final FTDI_BAUDRATE_38400:I = 0xc04e

.field public static final FTDI_BAUDRATE_460800:I = 0x4006

.field public static final FTDI_BAUDRATE_4800:I = 0x271

.field public static final FTDI_BAUDRATE_57600:I = 0x34

.field public static final FTDI_BAUDRATE_600:I = 0x1388

.field public static final FTDI_BAUDRATE_921600:I = 0x8003

.field public static final FTDI_BAUDRATE_9600:I = 0x4138

.field private static final FTDI_REQTYPE_HOST2DEVICE:I = 0x40

.field private static final FTDI_SET_DATA_DEFAULT:I = 0x8

.field private static final FTDI_SET_FLOW_CTRL_DEFAULT:I = 0x0

.field private static final FTDI_SET_MODEM_CTRL_DEFAULT1:I = 0x101

.field private static final FTDI_SET_MODEM_CTRL_DEFAULT2:I = 0x202

.field private static final FTDI_SET_MODEM_CTRL_DEFAULT3:I = 0x100

.field private static final FTDI_SET_MODEM_CTRL_DEFAULT4:I = 0x200

.field private static final FTDI_SIO_MODEM_CTRL:I = 0x1

.field private static final FTDI_SIO_RESET:I = 0x0

.field private static final FTDI_SIO_SET_BAUD_RATE:I = 0x3

.field private static final FTDI_SIO_SET_BREAK_OFF:I = 0x0

.field private static final FTDI_SIO_SET_BREAK_ON:I = 0x4000

.field private static final FTDI_SIO_SET_DATA:I = 0x4

.field private static final FTDI_SIO_SET_DTR_HIGH:I = 0x101

.field private static final FTDI_SIO_SET_DTR_LOW:I = 0x100

.field private static final FTDI_SIO_SET_DTR_MASK:I = 0x1

.field private static final FTDI_SIO_SET_FLOW_CTRL:I = 0x2

.field private static final FTDI_SIO_SET_RTS_HIGH:I = 0x202

.field private static final FTDI_SIO_SET_RTS_LOW:I = 0x200

.field private static final FTDI_SIO_SET_RTS_MASK:I = 0x2

.field private static final skip:[B


# instance fields
.field private breakCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

.field private ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

.field private ctsState:Z

.field private currentSioSetData:I

.field private dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

.field private dsrState:Z

.field private dtrDsrEnabled:Z

.field private firstTime:Z

.field private frameCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

.field public ftdiUtilities:Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private overrunCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

.field private parityCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

.field private rtsCtsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    const-class v0, Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/FTDISerialDevice;->CLASS_ID:Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 75
    sput-object v0, Lcom/felhr/usbserial/FTDISerialDevice;->EMPTY_BYTE_ARRAY:[B

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 749
    sput-object v0, Lcom/felhr/usbserial/FTDISerialDevice;->skip:[B

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    const/4 v0, -0x1

    .line 105
    invoke-direct {p0, p1, p2, v0}, Lcom/felhr/usbserial/FTDISerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 1

    .line 110
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x0

    .line 77
    iput p2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 111
    new-instance v0, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;-><init>(Lcom/felhr/usbserial/FTDISerialDevice;)V

    iput-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ftdiUtilities:Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

    .line 112
    iput-boolean p2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->rtsCtsEnabled:Z

    .line 113
    iput-boolean p2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dtrDsrEnabled:Z

    const/4 v0, 0x1

    .line 114
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ctsState:Z

    .line 115
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dsrState:Z

    .line 116
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->firstTime:Z

    if-ltz p3, :cond_0

    move p2, p3

    .line 117
    :cond_0
    invoke-virtual {p1, p2}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void
.end method

.method static synthetic access$000([B[B)V
    .locals 0

    .line 17
    invoke-static {p0, p1}, Lcom/felhr/usbserial/FTDISerialDevice;->copyData([B[B)V

    return-void
.end method

.method static synthetic access$100(Lcom/felhr/usbserial/FTDISerialDevice;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->firstTime:Z

    return p0
.end method

.method static synthetic access$1000(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->overrunCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    return-object p0
.end method

.method static synthetic access$102(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z
    .locals 0

    .line 17
    iput-boolean p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->firstTime:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->breakCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    return-object p0
.end method

.method static synthetic access$200(Lcom/felhr/usbserial/FTDISerialDevice;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ctsState:Z

    return p0
.end method

.method static synthetic access$202(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z
    .locals 0

    .line 17
    iput-boolean p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ctsState:Z

    return p1
.end method

.method static synthetic access$300(Lcom/felhr/usbserial/FTDISerialDevice;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dsrState:Z

    return p0
.end method

.method static synthetic access$302(Lcom/felhr/usbserial/FTDISerialDevice;Z)Z
    .locals 0

    .line 17
    iput-boolean p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dsrState:Z

    return p1
.end method

.method static synthetic access$400(Lcom/felhr/usbserial/FTDISerialDevice;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->rtsCtsEnabled:Z

    return p0
.end method

.method static synthetic access$500(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    return-object p0
.end method

.method static synthetic access$600(Lcom/felhr/usbserial/FTDISerialDevice;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dtrDsrEnabled:Z

    return p0
.end method

.method static synthetic access$700(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    return-object p0
.end method

.method static synthetic access$800(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->parityCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    return-object p0
.end method

.method static synthetic access$900(Lcom/felhr/usbserial/FTDISerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->frameCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    return-object p0
.end method

.method static adaptArray([B)[B
    .locals 4

    .line 495
    array-length v0, p0

    const/4 v1, 0x2

    const/16 v2, 0x40

    if-le v0, v2, :cond_1

    const/4 v3, 0x1

    :goto_0
    if-ge v2, v0, :cond_0

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v2, v3, 0x40

    goto :goto_0

    :cond_0
    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 507
    new-array v0, v0, [B

    .line 508
    invoke-static {p0, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->copyData([B[B)V

    return-object v0

    :cond_1
    if-ne v0, v1, :cond_2

    .line 513
    sget-object p0, Lcom/felhr/usbserial/FTDISerialDevice;->EMPTY_BYTE_ARRAY:[B

    return-object p0

    .line 517
    :cond_2
    invoke-static {p0, v1, v0}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p0

    return-object p0
.end method

.method private static copyData([B[B)V
    .locals 5

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v3, v1, -0x2

    .line 525
    array-length v4, p0

    add-int/lit8 v4, v4, -0x40

    if-gt v3, v4, :cond_0

    const/16 v3, 0x3e

    .line 527
    invoke-static {p0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v1, v1, 0x40

    add-int/lit8 v2, v2, 0x3e

    goto :goto_0

    .line 531
    :cond_0
    array-length v3, p0

    sub-int/2addr v3, v1

    add-int/2addr v3, v0

    if-lez v3, :cond_1

    sub-int/2addr v3, v0

    .line 534
    invoke-static {p0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    return-void
.end method

.method private encodedBaudRate(I)[S
    .locals 15

    move/from16 v0, p1

    const/4 v1, 0x2

    new-array v1, v1, [S

    const/16 v2, 0x8

    new-array v3, v2, [B

    .line 819
    fill-array-data v3, :array_0

    const/16 v4, 0x10

    new-array v5, v4, [B

    .line 823
    fill-array-data v5, :array_1

    .line 828
    invoke-direct {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->getBcdDevice()S

    move-result v6

    const/4 v7, 0x0

    const/4 v8, -0x1

    if-ne v6, v8, :cond_0

    return-object v7

    :cond_0
    const/16 v8, 0x200

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-ne v6, v8, :cond_1

    .line 834
    invoke-direct {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->getISerialNumber()B

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    :goto_0
    const/16 v11, 0x500

    const/16 v12, 0x900

    const/16 v13, 0x800

    const/16 v14, 0x700

    if-eq v6, v11, :cond_3

    if-eq v6, v14, :cond_3

    if-eq v6, v13, :cond_3

    if-eq v6, v12, :cond_3

    const/16 v11, 0x1000

    if-ne v6, v11, :cond_2

    goto :goto_1

    :cond_2
    const/4 v11, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v11, 0x1

    :goto_2
    if-eq v6, v14, :cond_5

    if-eq v6, v13, :cond_5

    if-ne v6, v12, :cond_4

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v6, 0x1

    :goto_4
    const/16 v12, 0x4b0

    if-lt v0, v12, :cond_6

    if-eqz v6, :cond_6

    const v6, 0xb71b00

    const/high16 v12, 0x20000

    goto :goto_5

    :cond_6
    const v6, 0x2dc6c0

    const/4 v12, 0x0

    :goto_5
    shr-int/lit8 v13, v6, 0xe

    if-lt v0, v13, :cond_e

    if-le v0, v6, :cond_7

    goto :goto_8

    :cond_7
    shl-int/lit8 v13, v6, 0x4

    .line 858
    div-int/2addr v13, v0

    and-int/lit8 v14, v13, 0xf

    if-ne v14, v10, :cond_8

    and-int/lit8 v5, v13, -0x8

    goto :goto_6

    :cond_8
    if-eqz v8, :cond_9

    .line 862
    aget-byte v5, v5, v14

    add-int/2addr v5, v13

    goto :goto_6

    :cond_9
    add-int/lit8 v5, v13, 0x1

    :goto_6
    shr-int/2addr v5, v10

    shl-int/lit8 v6, v6, 0x3

    .line 868
    div-int/2addr v6, v5

    int-to-long v13, v6

    move-object v8, v3

    int-to-long v2, v0

    move-object v0, p0

    .line 870
    invoke-direct {p0, v13, v14, v2, v3}, Lcom/felhr/usbserial/FTDISerialDevice;->isBaudTolerated(JJ)Z

    move-result v2

    if-nez v2, :cond_a

    return-object v7

    :cond_a
    and-int/lit8 v2, v5, 0x7

    shr-int/lit8 v3, v5, 0x3

    if-ne v3, v10, :cond_c

    if-nez v2, :cond_b

    const/4 v3, 0x0

    goto :goto_7

    :cond_b
    const/4 v2, 0x0

    .line 883
    :cond_c
    :goto_7
    aget-byte v2, v8, v2

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v2, v12

    or-int/2addr v2, v3

    int-to-short v3, v2

    aput-short v3, v1, v9

    shr-int/2addr v2, v4

    int-to-short v2, v2

    aput-short v2, v1, v10

    if-eqz v11, :cond_d

    .line 890
    aget-short v2, v1, v10

    const/16 v3, 0x8

    shl-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v1, v10

    :cond_d
    return-object v1

    :cond_e
    :goto_8
    move-object v0, p0

    return-object v7

    :array_0
    .array-data 1
        0x0t
        0x3t
        0x2t
        0x4t
        0x1t
        0x5t
        0x6t
        0x7t
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        -0x1t
        0x2t
        0x1t
        0x0t
        -0x1t
        -0x2t
        -0x3t
        0x4t
        0x3t
        0x2t
        0x1t
    .end array-data
.end method

.method private getBcdDevice()S
    .locals 3

    .line 787
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    .line 788
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->getRawDescriptors()[B

    move-result-object v0

    const/16 v2, 0xc

    .line 789
    aget-byte v2, v0, v2

    shl-int/lit8 v2, v2, 0x8

    aget-byte v0, v0, v1

    add-int/2addr v2, v0

    int-to-short v0, v2

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private getISerialNumber()B
    .locals 2

    .line 796
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    .line 797
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->getRawDescriptors()[B

    move-result-object v0

    const/16 v1, 0x10

    .line 798
    aget-byte v0, v0, v1

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private isBaudTolerated(JJ)Z
    .locals 3

    const-wide/16 v0, 0x64

    mul-long p3, p3, v0

    const-wide/16 v0, 0x67

    .line 805
    div-long v0, p3, v0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    const-wide/16 v0, 0x61

    div-long/2addr p3, v0

    cmp-long v0, p1, p3

    if-gtz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private openFTDI()Z
    .locals 7

    .line 437
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    .line 439
    sget-object v0, Lcom/felhr/usbserial/FTDISerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    const/4 v5, 0x2

    if-gt v3, v4, :cond_1

    .line 450
    iget-object v4, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 451
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 452
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v6, 0x80

    if-ne v5, v6, :cond_0

    .line 454
    iput-object v4, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 457
    :cond_0
    iput-object v4, p0, Lcom/felhr/usbserial/FTDISerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 462
    :cond_1
    iput-boolean v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->firstTime:Z

    .line 463
    invoke-direct {p0, v1, v1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    move-result v0

    if-gez v0, :cond_2

    return v1

    :cond_2
    const/4 v0, 0x4

    const/16 v3, 0x8

    .line 465
    invoke-direct {p0, v0, v3, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    move-result v0

    if-gez v0, :cond_3

    return v1

    .line 467
    :cond_3
    iput v3, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    const/16 v0, 0x101

    .line 468
    invoke-direct {p0, v2, v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    move-result v0

    if-gez v0, :cond_4

    return v1

    :cond_4
    const/16 v0, 0x202

    .line 470
    invoke-direct {p0, v2, v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    move-result v0

    if-gez v0, :cond_5

    return v1

    .line 472
    :cond_5
    invoke-direct {p0, v5, v1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    move-result v0

    if-gez v0, :cond_6

    return v1

    :cond_6
    const/4 v0, 0x3

    const/16 v3, 0x4138

    .line 474
    invoke-direct {p0, v0, v3, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    move-result v0

    if-gez v0, :cond_7

    return v1

    .line 478
    :cond_7
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->rtsCtsEnabled:Z

    .line 479
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dtrDsrEnabled:Z

    return v2

    .line 442
    :cond_8
    sget-object v0, Lcom/felhr/usbserial/FTDISerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private readSyncJelly([BIJ)I
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x0

    :cond_0
    if-lez p2, :cond_2

    .line 765
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sub-long v1, p3, v1

    long-to-int v2, v1

    if-gtz v2, :cond_1

    goto :goto_1

    :cond_1
    move v6, v2

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    .line 773
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    sget-object v3, Lcom/felhr/usbserial/FTDISerialDevice;->skip:[B

    array-length v4, v3

    invoke-virtual {v1, v2, v3, v4, v6}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_3

    .line 777
    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    const/16 v5, 0x3e

    move-object v3, p1

    move v4, v7

    invoke-virtual/range {v1 .. v6}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BIII)I

    move-result v1

    add-int/2addr v7, v1

    :cond_3
    if-lez v7, :cond_0

    :goto_1
    return v7
.end method

.method private setControlCommand(III)I
    .locals 8

    .line 487
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int v4, v1, p3

    const/4 v6, 0x0

    const/16 v1, 0x40

    const/4 v5, 0x0

    const/4 v7, 0x0

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 488
    sget-object p2, Lcom/felhr/usbserial/FTDISerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Control Transfer Response: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method

.method private setEncodedBaudRate([S)V
    .locals 8

    .line 897
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/4 v1, 0x0

    aget-short v3, p1, v1

    const/4 v1, 0x1

    aget-short v4, p1, v1

    const/16 v1, 0x40

    const/4 v2, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    return-void
.end method

.method private setOldBaudRate(I)V
    .locals 4

    const v0, 0x8003

    const/16 v1, 0x4138

    const/16 v2, 0x12c

    if-ltz p1, :cond_0

    if-gt p1, v2, :cond_0

    const/16 v0, 0x2710

    goto/16 :goto_0

    :cond_0
    const/16 v3, 0x258

    if-le p1, v2, :cond_1

    if-gt p1, v3, :cond_1

    const/16 v0, 0x1388

    goto/16 :goto_0

    :cond_1
    const/16 v2, 0x4b0

    if-le p1, v3, :cond_2

    if-gt p1, v2, :cond_2

    const/16 v0, 0x9c4

    goto/16 :goto_0

    :cond_2
    const/16 v3, 0x960

    if-le p1, v2, :cond_3

    if-gt p1, v3, :cond_3

    const/16 v0, 0x4e2

    goto/16 :goto_0

    :cond_3
    const/16 v2, 0x12c0

    if-le p1, v3, :cond_4

    if-gt p1, v2, :cond_4

    const/16 v0, 0x271

    goto :goto_0

    :cond_4
    const/16 v3, 0x2580

    if-le p1, v2, :cond_6

    if-gt p1, v3, :cond_6

    :cond_5
    const/16 v0, 0x4138

    goto :goto_0

    :cond_6
    const/16 v2, 0x4b00

    if-le p1, v3, :cond_7

    if-gt p1, v2, :cond_7

    const v0, 0x809c

    goto :goto_0

    :cond_7
    if-le p1, v2, :cond_8

    const v3, 0x9600

    if-gt p1, v3, :cond_8

    const v0, 0xc04e

    goto :goto_0

    :cond_8
    const v3, 0xe100

    if-le p1, v2, :cond_9

    if-gt p1, v3, :cond_9

    const/16 v0, 0x34

    goto :goto_0

    :cond_9
    const v2, 0x1c200

    if-le p1, v3, :cond_a

    if-gt p1, v2, :cond_a

    const/16 v0, 0x1a

    goto :goto_0

    :cond_a
    const v3, 0x38400

    if-le p1, v2, :cond_b

    if-gt p1, v3, :cond_b

    const/16 v0, 0xd

    goto :goto_0

    :cond_b
    const v2, 0x70800

    if-le p1, v3, :cond_c

    if-gt p1, v2, :cond_c

    const/16 v0, 0x4006

    goto :goto_0

    :cond_c
    const v3, 0xe1000

    if-le p1, v2, :cond_d

    if-gt p1, v3, :cond_d

    goto :goto_0

    :cond_d
    if-le p1, v3, :cond_5

    :goto_0
    const/4 p1, 0x3

    const/4 v1, 0x0

    .line 934
    invoke-direct {p0, p1, v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x100

    .line 152
    invoke-direct {p0, v0, v2, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    const/16 v2, 0x200

    .line 153
    invoke-direct {p0, v0, v2, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    .line 154
    iput v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 155
    invoke-virtual {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->killWorkingThread()V

    .line 156
    invoke-virtual {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->killWriteThread()V

    .line 157
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 158
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->isOpen:Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->breakCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    .line 402
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->frameCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    return-void
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->overrunCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    .line 432
    iput-object p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->parityCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    return-void
.end method

.method public open()Z
    .locals 3

    .line 123
    invoke-direct {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->openFTDI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    new-instance v0, Lcom/felhr/utils/SafeUsbRequest;

    invoke-direct {v0}, Lcom/felhr/utils/SafeUsbRequest;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 132
    invoke-virtual {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->restartWorkingThread()V

    .line 133
    invoke-virtual {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->restartWriteThread()V

    .line 136
    iget-object v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    const/4 v0, 0x1

    .line 138
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->asyncMode:Z

    .line 139
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->isOpen:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 144
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->isOpen:Z

    return v0
.end method

.method public setBaudRate(I)V
    .locals 1

    .line 197
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/FTDISerialDevice;->encodedBaudRate(I)[S

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setEncodedBaudRate([S)V

    goto :goto_0

    .line 202
    :cond_0
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/FTDISerialDevice;->setOldBaudRate(I)V

    :goto_0
    return-void
.end method

.method public setBreak(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 368
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x4000

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    goto :goto_0

    .line 370
    :cond_0
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x4001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    :goto_0
    const/4 p1, 0x4

    .line 372
    iget v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    return-void
.end method

.method public setDTR(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const/16 p1, 0x101

    .line 392
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    :cond_0
    const/16 p1, 0x100

    .line 395
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    :goto_0
    return-void
.end method

.method public setDataBits(I)V
    .locals 3

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    if-eq p1, v0, :cond_3

    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    const/4 v0, 0x7

    if-eq p1, v0, :cond_1

    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    .line 240
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 241
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x3

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 242
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x5

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 243
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/2addr p1, v0

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 244
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 233
    :cond_0
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 234
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x3

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 235
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x5

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 236
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/2addr p1, v0

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 237
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 226
    :cond_1
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 227
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 228
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/2addr p1, v2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 229
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x9

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 230
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 219
    :cond_2
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 220
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 221
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/2addr p1, v2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 222
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x9

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 223
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 212
    :cond_3
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 213
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x3

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 214
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/2addr p1, v2

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 215
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit8 p1, p1, -0x9

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 216
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    :goto_0
    return-void
.end method

.method public setFlowControl(I)V
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_2

    if-eq p1, v0, :cond_1

    const/4 v2, 0x3

    if-eq p1, v2, :cond_0

    .line 355
    invoke-direct {p0, v0, v1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    const/16 v1, 0x1311

    .line 352
    invoke-direct {p0, v0, v1, p1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 344
    :cond_1
    iput-boolean v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dtrDsrEnabled:Z

    .line 345
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->rtsCtsEnabled:Z

    .line 347
    invoke-direct {p0, v0, v1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 338
    :cond_2
    iput-boolean v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->rtsCtsEnabled:Z

    .line 339
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dtrDsrEnabled:Z

    .line 341
    invoke-direct {p0, v0, v1, v2}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 333
    :cond_3
    invoke-direct {p0, v0, v1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    .line 334
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->rtsCtsEnabled:Z

    .line 335
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->dtrDsrEnabled:Z

    :goto_0
    return-void
.end method

.method public setParity(I)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-eqz p1, :cond_4

    const/4 v2, 0x1

    if-eq p1, v2, :cond_3

    const/4 v2, 0x2

    if-eq p1, v2, :cond_2

    const/4 v2, 0x3

    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_0

    .line 318
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x101

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 319
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x201

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 320
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x401

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 321
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto/16 :goto_0

    .line 312
    :cond_0
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x101

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 313
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x201

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 314
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x400

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 315
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 306
    :cond_1
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x100

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 307
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x200

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 308
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x401

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 309
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 300
    :cond_2
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x101

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 301
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x200

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 302
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x401

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 303
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 294
    :cond_3
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x100

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 295
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x201

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 296
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x401

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 297
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 288
    :cond_4
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x101

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 289
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x201

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 290
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x401

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 291
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    :goto_0
    return-void
.end method

.method public setRTS(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const/16 p1, 0x202

    .line 380
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    :cond_0
    const/16 p1, 0x200

    .line 383
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    :goto_0
    return-void
.end method

.method public setStopBits(I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x4

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 274
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x801

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 275
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x1001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 276
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x2001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 277
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 262
    :cond_0
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x800

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 263
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x1001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 264
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x2001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 265
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 268
    :cond_1
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x801

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 269
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    or-int/lit16 p1, p1, 0x1000

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 270
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x2001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 271
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    goto :goto_0

    .line 256
    :cond_2
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x801

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 257
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x1001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 258
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    and-int/lit16 p1, p1, -0x2001

    iput p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 259
    iget p1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    invoke-direct {p0, v2, p1, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    :goto_0
    return-void
.end method

.method public syncClose()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x100

    .line 187
    invoke-direct {p0, v0, v2, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    const/16 v2, 0x200

    .line 188
    invoke-direct {p0, v0, v2, v1}, Lcom/felhr/usbserial/FTDISerialDevice;->setControlCommand(III)I

    .line 189
    iput v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->currentSioSetData:I

    .line 190
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 191
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->isOpen:Z

    return-void
.end method

.method public syncOpen()Z
    .locals 3

    .line 164
    invoke-direct {p0}, Lcom/felhr/usbserial/FTDISerialDevice;->openFTDI()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v2}, Lcom/felhr/usbserial/FTDISerialDevice;->setSyncParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    .line 168
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->asyncMode:Z

    .line 171
    new-instance v0, Lcom/felhr/usbserial/SerialInputStream;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/SerialInputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inputStream:Lcom/felhr/usbserial/SerialInputStream;

    .line 172
    new-instance v0, Lcom/felhr/usbserial/SerialOutputStream;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/SerialOutputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->outputStream:Lcom/felhr/usbserial/SerialOutputStream;

    const/4 v0, 0x1

    .line 174
    iput-boolean v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;->isOpen:Z

    return v0

    .line 179
    :cond_0
    iput-boolean v1, p0, Lcom/felhr/usbserial/FTDISerialDevice;->isOpen:Z

    return v1
.end method

.method public syncRead([BI)I
    .locals 10

    .line 638
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    int-to-long v2, p2

    add-long/2addr v0, v2

    .line 641
    iget-boolean v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->asyncMode:Z

    if-eqz v2, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 v2, 0x0

    if-nez p1, :cond_1

    return v2

    .line 651
    :cond_1
    array-length v3, p1

    div-int/lit8 v3, v3, 0x3e

    .line 652
    array-length v4, p1

    rem-int/lit8 v4, v4, 0x3e

    if-eqz v4, :cond_2

    add-int/lit8 v3, v3, 0x1

    .line 657
    :cond_2
    array-length v4, p1

    const/4 v5, 0x2

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v4, v3

    new-array v3, v4, [B

    const/4 v4, 0x0

    :cond_3
    if-lez p2, :cond_4

    .line 666
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v0, v6

    long-to-int v7, v6

    if-gtz v7, :cond_5

    goto :goto_0

    :cond_4
    const/4 v7, 0x0

    .line 673
    :cond_5
    iget-object v6, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v8, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    array-length v9, v3

    invoke-virtual {v6, v8, v3, v9, v7}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v6

    if-le v6, v5, :cond_7

    .line 677
    iget-object v4, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ftdiUtilities:Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

    invoke-virtual {v4, v3}, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->adaptArray([B)[B

    move-result-object v4

    .line 678
    array-length v7, p1

    invoke-static {v4, v2, p1, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 680
    div-int/lit8 v4, v6, 0x40

    .line 681
    rem-int/lit8 v7, v6, 0x40

    if-eqz v7, :cond_6

    add-int/lit8 v4, v4, 0x1

    :cond_6
    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v6, v4

    move v4, v6

    :cond_7
    if-lez v4, :cond_3

    :goto_0
    return v4
.end method

.method public syncRead([BIII)I
    .locals 10

    .line 695
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    int-to-long v2, p4

    add-long/2addr v0, v2

    .line 698
    iget-boolean v2, p0, Lcom/felhr/usbserial/FTDISerialDevice;->asyncMode:Z

    if-eqz v2, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 v2, 0x0

    if-nez p1, :cond_1

    return v2

    .line 708
    :cond_1
    div-int/lit8 v3, p3, 0x3e

    .line 709
    rem-int/lit8 v4, p3, 0x3e

    if-eqz v4, :cond_2

    add-int/lit8 v3, v3, 0x1

    :cond_2
    const/4 v4, 0x2

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p3

    .line 714
    new-array v3, v3, [B

    const/4 v5, 0x0

    :cond_3
    if-lez p4, :cond_4

    .line 723
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v0, v6

    long-to-int v7, v6

    if-gtz v7, :cond_5

    goto :goto_0

    :cond_4
    const/4 v7, 0x0

    .line 730
    :cond_5
    iget-object v6, p0, Lcom/felhr/usbserial/FTDISerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v8, p0, Lcom/felhr/usbserial/FTDISerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    array-length v9, v3

    invoke-virtual {v6, v8, v3, v9, v7}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v6

    if-le v6, v4, :cond_7

    .line 734
    iget-object v5, p0, Lcom/felhr/usbserial/FTDISerialDevice;->ftdiUtilities:Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;

    invoke-virtual {v5, v3}, Lcom/felhr/usbserial/FTDISerialDevice$FTDIUtilities;->adaptArray([B)[B

    move-result-object v5

    .line 735
    invoke-static {v5, v2, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 737
    div-int/lit8 v5, v6, 0x40

    .line 738
    rem-int/lit8 v7, v6, 0x40

    if-eqz v7, :cond_6

    add-int/lit8 v5, v5, 0x1

    :cond_6
    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v6, v5

    move v5, v6

    :cond_7
    if-lez v5, :cond_3

    :goto_0
    return v5
.end method
