.class public Lcom/felhr/usbserial/SerialBuffer;
.super Ljava/lang/Object;
.source "SerialBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;
    }
.end annotation


# static fields
.field static final DEFAULT_READ_BUFFER_SIZE:I = 0x4000

.field static final MAX_BULK_BUFFER:I = 0x4000


# instance fields
.field private debugging:Z

.field private readBuffer:Ljava/nio/ByteBuffer;

.field private readBufferCompatible:[B

.field private final writeBuffer:Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/felhr/usbserial/SerialBuffer;->debugging:Z

    .line 21
    new-instance v0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;-><init>(Lcom/felhr/usbserial/SerialBuffer;)V

    iput-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->writeBuffer:Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;

    const/16 v0, 0x4000

    if-eqz p1, :cond_0

    .line 24
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/SerialBuffer;->readBuffer:Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_0
    new-array p1, v0, [B

    .line 28
    iput-object p1, p0, Lcom/felhr/usbserial/SerialBuffer;->readBufferCompatible:[B

    :goto_0
    return-void
.end method

.method static synthetic access$000(Lcom/felhr/usbserial/SerialBuffer;)Z
    .locals 0

    .line 9
    iget-boolean p0, p0, Lcom/felhr/usbserial/SerialBuffer;->debugging:Z

    return p0
.end method


# virtual methods
.method public clearReadBuffer()V
    .locals 1

    .line 64
    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->readBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 67
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public debug(Z)V
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/felhr/usbserial/SerialBuffer;->debugging:Z

    return-void
.end method

.method public getBufferCompatible()[B
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->readBufferCompatible:[B

    return-object v0
.end method

.method public getDataReceived()[B
    .locals 4

    .line 51
    monitor-enter p0

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->readBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    new-array v0, v0, [B

    .line 54
    iget-object v1, p0, Lcom/felhr/usbserial/SerialBuffer;->readBuffer:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 55
    iget-object v1, p0, Lcom/felhr/usbserial/SerialBuffer;->readBuffer:Ljava/nio/ByteBuffer;

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 56
    iget-boolean v1, p0, Lcom/felhr/usbserial/SerialBuffer;->debugging:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 57
    invoke-static {v0, v1}, Lcom/felhr/usbserial/UsbSerialDebugger;->printReadLogGet([BZ)V

    .line 58
    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 59
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDataReceivedCompatible(I)[B
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->readBufferCompatible:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    return-object p1
.end method

.method public getReadBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .line 42
    monitor-enter p0

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->readBuffer:Ljava/nio/ByteBuffer;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 45
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWriteBuffer()[B
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->writeBuffer:Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;

    invoke-virtual {v0}, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->get()[B

    move-result-object v0

    return-object v0
.end method

.method public putWriteBuffer([B)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer;->writeBuffer:Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;

    invoke-virtual {v0, p1}, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->put([B)V

    return-void
.end method
