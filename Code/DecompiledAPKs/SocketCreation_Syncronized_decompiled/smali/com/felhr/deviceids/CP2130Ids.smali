.class public Lcom/felhr/deviceids/CP2130Ids;
.super Ljava/lang/Object;
.source "CP2130Ids.java"


# static fields
.field private static final cp2130Devices:[J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [J

    const/16 v1, 0x10c4

    const v2, 0x87a0

    .line 9
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/4 v3, 0x0

    aput-wide v1, v0, v3

    .line 8
    invoke-static {v0}, Lcom/felhr/deviceids/Helpers;->createTable([J)[J

    move-result-object v0

    sput-object v0, Lcom/felhr/deviceids/CP2130Ids;->cp2130Devices:[J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDeviceSupported(II)Z
    .locals 1

    .line 14
    sget-object v0, Lcom/felhr/deviceids/CP2130Ids;->cp2130Devices:[J

    invoke-static {v0, p0, p1}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result p0

    return p0
.end method
