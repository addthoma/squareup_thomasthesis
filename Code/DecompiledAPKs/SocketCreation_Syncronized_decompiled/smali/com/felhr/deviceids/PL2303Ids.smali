.class public Lcom/felhr/deviceids/PL2303Ids;
.super Ljava/lang/Object;
.source "PL2303Ids.java"


# static fields
.field private static final pl2303Devices:[J


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/16 v0, 0x39

    new-array v0, v0, [J

    const/16 v1, 0x4a5

    const/16 v2, 0x4027

    .line 14
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/4 v3, 0x0

    aput-wide v1, v0, v3

    const/16 v1, 0x2303

    const/16 v2, 0x67b

    .line 15
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v5, 0x1

    aput-wide v3, v0, v5

    const/16 v3, 0x4bb

    .line 16
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/4 v4, 0x2

    aput-wide v6, v0, v4

    const/16 v4, 0x1234

    .line 17
    invoke-static {v2, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/4 v8, 0x3

    aput-wide v6, v0, v8

    const v6, 0xaaa0

    .line 18
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/4 v9, 0x4

    aput-wide v6, v0, v9

    const v6, 0xaaa2

    .line 19
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/4 v10, 0x5

    aput-wide v6, v0, v10

    const/16 v6, 0x611

    .line 20
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/4 v11, 0x6

    aput-wide v6, v0, v11

    const/16 v6, 0x612

    .line 21
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/4 v11, 0x7

    aput-wide v6, v0, v11

    const/16 v6, 0x609

    .line 22
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v11, 0x8

    aput-wide v6, v0, v11

    const/16 v6, 0x331a

    .line 23
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v11, 0x9

    aput-wide v6, v0, v11

    const/16 v6, 0x307

    .line 24
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v11, 0xa

    aput-wide v6, v0, v11

    const/16 v6, 0x463

    .line 25
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v2, 0xb

    aput-wide v6, v0, v2

    const/16 v2, 0x557

    const/16 v6, 0x2008

    .line 26
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0xc

    aput-wide v11, v0, v7

    const/16 v7, 0x547

    .line 27
    invoke-static {v7, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0xd

    aput-wide v11, v0, v7

    const/16 v7, 0xa03

    .line 28
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0xe

    aput-wide v11, v0, v7

    const/16 v7, 0xa0e

    .line 29
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0xf

    aput-wide v11, v0, v3

    const/16 v3, 0x56e

    const/16 v7, 0x5003

    .line 30
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0x10

    aput-wide v11, v0, v7

    const/16 v7, 0x5004

    .line 31
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0x11

    aput-wide v11, v0, v3

    const/16 v3, 0xeba

    const/16 v7, 0x1080

    .line 32
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0x12

    aput-wide v11, v0, v7

    const/16 v7, 0x2080

    .line 33
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0x13

    aput-wide v11, v0, v3

    const/16 v3, 0xdf7

    const/16 v7, 0x620

    .line 34
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0x14

    aput-wide v11, v0, v3

    const/16 v3, 0x584

    const v7, 0xb000

    .line 35
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0x15

    aput-wide v11, v0, v3

    const/16 v3, 0x2478

    .line 36
    invoke-static {v3, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0x16

    aput-wide v11, v0, v3

    const/16 v3, 0x1453

    const/16 v7, 0x4026

    .line 37
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v3, 0x17

    aput-wide v11, v0, v3

    const/16 v3, 0x731

    const/16 v7, 0x528

    .line 38
    invoke-static {v3, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0x18

    aput-wide v11, v0, v7

    const/16 v7, 0x6189

    const/16 v11, 0x2068

    .line 39
    invoke-static {v7, v11}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0x19

    aput-wide v11, v0, v7

    const/16 v7, 0x11f7

    const/16 v11, 0x2df

    .line 40
    invoke-static {v7, v11}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0x1a

    aput-wide v11, v0, v7

    const/16 v7, 0x4e8

    const v11, 0x8001

    .line 41
    invoke-static {v7, v11}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v7, 0x1b

    aput-wide v11, v0, v7

    const/16 v7, 0x11f5

    .line 42
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v13, 0x1c

    aput-wide v11, v0, v13

    .line 43
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v11

    const/16 v8, 0x1d

    aput-wide v11, v0, v8

    .line 44
    invoke-static {v7, v9}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v8

    const/16 v11, 0x1e

    aput-wide v8, v0, v11

    .line 45
    invoke-static {v7, v10}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1f

    aput-wide v7, v0, v9

    const/16 v7, 0x745

    .line 46
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x20

    aput-wide v7, v0, v9

    const/16 v7, 0x78b

    .line 47
    invoke-static {v7, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x21

    aput-wide v7, v0, v4

    const/16 v4, 0x10b5

    const v7, 0xac70

    .line 48
    invoke-static {v4, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x22

    aput-wide v7, v0, v4

    const/16 v4, 0x79b

    const/16 v7, 0x27

    .line 49
    invoke-static {v4, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x23

    aput-wide v7, v0, v4

    const/16 v4, 0x413

    const/16 v7, 0x2101

    .line 50
    invoke-static {v4, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x24

    aput-wide v7, v0, v4

    const/16 v4, 0xe55

    const/16 v7, 0x110b

    .line 51
    invoke-static {v4, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x25

    aput-wide v7, v0, v4

    const/16 v4, 0x2003

    .line 52
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x26

    aput-wide v3, v0, v7

    const/16 v3, 0x50d

    const/16 v4, 0x257

    .line 53
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x27

    aput-wide v3, v0, v7

    const/16 v3, 0x58f

    const v4, 0x9720

    .line 54
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x28

    aput-wide v3, v0, v7

    const/16 v3, 0x11f6

    const/16 v4, 0x2001

    .line 55
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x29

    aput-wide v3, v0, v7

    const/16 v3, 0x7aa

    const/16 v4, 0x2a

    .line 56
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x2a

    aput-wide v3, v0, v7

    const/16 v3, 0x5ad

    const/16 v4, 0xfba

    .line 57
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x2b

    aput-wide v3, v0, v7

    const/16 v3, 0x5372

    .line 58
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x2c

    aput-wide v3, v0, v7

    const/16 v3, 0x3f0

    const/16 v4, 0xb39

    .line 59
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x2d

    aput-wide v7, v0, v4

    const/16 v4, 0x3139

    .line 60
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x2e

    aput-wide v7, v0, v4

    const/16 v4, 0x3239

    .line 61
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v4, 0x2f

    aput-wide v7, v0, v4

    const/16 v4, 0x3524

    .line 62
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x30

    aput-wide v3, v0, v7

    const/16 v3, 0x4b8

    const/16 v4, 0x521

    .line 63
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x31

    aput-wide v3, v0, v7

    const/16 v3, 0x4b8

    const/16 v4, 0x522

    .line 64
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x32

    aput-wide v3, v0, v7

    const/16 v3, 0x54c

    const/16 v4, 0x437

    .line 65
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x33

    aput-wide v3, v0, v7

    const/16 v3, 0x11ad

    .line 66
    invoke-static {v3, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x34

    aput-wide v3, v0, v5

    const/16 v3, 0xb63

    const/16 v4, 0x6530

    .line 67
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x35

    aput-wide v3, v0, v5

    const/16 v3, 0xb8c

    .line 68
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v1, 0x36

    aput-wide v3, v0, v1

    const/16 v1, 0x110a

    const/16 v3, 0x1150

    .line 69
    invoke-static {v1, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v1, 0x37

    aput-wide v3, v0, v1

    .line 70
    invoke-static {v2, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x38

    aput-wide v1, v0, v3

    .line 13
    invoke-static {v0}, Lcom/felhr/deviceids/Helpers;->createTable([J)[J

    move-result-object v0

    sput-object v0, Lcom/felhr/deviceids/PL2303Ids;->pl2303Devices:[J

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDeviceSupported(II)Z
    .locals 1

    .line 75
    sget-object v0, Lcom/felhr/deviceids/PL2303Ids;->pl2303Devices:[J

    invoke-static {v0, p0, p1}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result p0

    return p0
.end method
