.class Lcom/mattprecious/telescope/TelescopeLayout$6$1;
.super Ljava/lang/Object;
.source "TelescopeLayout.java"

# interfaces
.implements Landroid/media/ImageReader$OnImageAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mattprecious/telescope/TelescopeLayout$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

.field final synthetic val$display:Landroid/hardware/display/VirtualDisplay;

.field final synthetic val$height:I

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/mattprecious/telescope/TelescopeLayout$6;IILandroid/hardware/display/VirtualDisplay;)V
    .locals 0

    .line 623
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iput p2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$width:I

    iput p3, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$height:I

    iput-object p4, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$display:Landroid/hardware/display/VirtualDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImageAvailable(Landroid/media/ImageReader;)V
    .locals 7

    const/4 v0, 0x0

    .line 629
    :try_start_0
    invoke-virtual {p1}, Landroid/media/ImageReader;->acquireLatestImage()Landroid/media/Image;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631
    :try_start_1
    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object v2, v2, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    new-instance v3, Lcom/mattprecious/telescope/TelescopeLayout$6$1$1;

    invoke-direct {v3, p0}, Lcom/mattprecious/telescope/TelescopeLayout$6$1$1;-><init>(Lcom/mattprecious/telescope/TelescopeLayout$6$1;)V

    invoke-virtual {v2, v3}, Lcom/mattprecious/telescope/TelescopeLayout;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_1

    if-eqz v1, :cond_0

    .line 677
    invoke-virtual {v1}, Landroid/media/Image;->close()V

    .line 680
    :cond_0
    invoke-virtual {p1}, Landroid/media/ImageReader;->close()V

    .line 681
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$display:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {p1}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 682
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object p1, p1, Lcom/mattprecious/telescope/TelescopeLayout$6;->val$projection:Landroid/media/projection/MediaProjection;

    invoke-virtual {p1}, Landroid/media/projection/MediaProjection;->stop()V

    return-void

    .line 641
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object v2, v2, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1102(Lcom/mattprecious/telescope/TelescopeLayout;Z)Z

    .line 643
    invoke-virtual {v1}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object v2

    const/4 v3, 0x0

    .line 644
    aget-object v4, v2, v3

    invoke-virtual {v4}, Landroid/media/Image$Plane;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 645
    aget-object v5, v2, v3

    invoke-virtual {v5}, Landroid/media/Image$Plane;->getPixelStride()I

    move-result v5

    .line 646
    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/media/Image$Plane;->getRowStride()I

    move-result v2

    .line 647
    iget v6, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$width:I

    mul-int v6, v6, v5

    sub-int/2addr v2, v6

    .line 649
    iget v6, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$width:I

    div-int/2addr v2, v5

    add-int/2addr v6, v2

    iget v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$height:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 651
    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 654
    iget v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$width:I

    iget v4, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$height:I

    invoke-static {v0, v3, v3, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 656
    iget-object v3, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object v3, v3, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v3}, Lcom/mattprecious/telescope/TelescopeLayout;->access$900(Lcom/mattprecious/telescope/TelescopeLayout;)V

    .line 657
    iget-object v3, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object v3, v3, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v3}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1000(Lcom/mattprecious/telescope/TelescopeLayout;)Lcom/mattprecious/telescope/Lens;

    move-result-object v3

    new-instance v4, Lcom/mattprecious/telescope/TelescopeLayout$6$1$2;

    invoke-direct {v4, p0, v2}, Lcom/mattprecious/telescope/TelescopeLayout$6$1$2;-><init>(Lcom/mattprecious/telescope/TelescopeLayout$6$1;Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v2, v4}, Lcom/mattprecious/telescope/Lens;->onCapture(Landroid/graphics/Bitmap;Lcom/mattprecious/telescope/BitmapProcessorListener;)V
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_2

    .line 673
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    if-eqz v1, :cond_4

    goto :goto_1

    :catchall_0
    move-exception v2

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_0

    :catchall_1
    move-exception v2

    move-object v1, v0

    goto :goto_2

    :catch_1
    move-exception v2

    move-object v1, v0

    :goto_0
    :try_start_3
    const-string v3, "Telescope"

    const-string v4, "Failed to capture system screenshot. Setting the screenshot mode to CANVAS."

    .line 663
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 665
    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object v2, v2, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    sget-object v3, Lcom/mattprecious/telescope/ScreenshotMode;->CANVAS:Lcom/mattprecious/telescope/ScreenshotMode;

    invoke-virtual {v2, v3}, Lcom/mattprecious/telescope/TelescopeLayout;->setScreenshotMode(Lcom/mattprecious/telescope/ScreenshotMode;)V

    .line 666
    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object v2, v2, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    new-instance v3, Lcom/mattprecious/telescope/TelescopeLayout$6$1$3;

    invoke-direct {v3, p0}, Lcom/mattprecious/telescope/TelescopeLayout$6$1$3;-><init>(Lcom/mattprecious/telescope/TelescopeLayout$6$1;)V

    invoke-virtual {v2, v3}, Lcom/mattprecious/telescope/TelescopeLayout;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_3

    .line 673
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    if-eqz v1, :cond_4

    .line 677
    :goto_1
    invoke-virtual {v1}, Landroid/media/Image;->close()V

    .line 680
    :cond_4
    invoke-virtual {p1}, Landroid/media/ImageReader;->close()V

    .line 681
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$display:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {p1}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 682
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object p1, p1, Lcom/mattprecious/telescope/TelescopeLayout$6;->val$projection:Landroid/media/projection/MediaProjection;

    invoke-virtual {p1}, Landroid/media/projection/MediaProjection;->stop()V

    return-void

    :goto_2
    if-eqz v0, :cond_5

    .line 673
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    if-eqz v1, :cond_6

    .line 677
    invoke-virtual {v1}, Landroid/media/Image;->close()V

    .line 680
    :cond_6
    invoke-virtual {p1}, Landroid/media/ImageReader;->close()V

    .line 681
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->val$display:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {p1}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 682
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6$1;->this$1:Lcom/mattprecious/telescope/TelescopeLayout$6;

    iget-object p1, p1, Lcom/mattprecious/telescope/TelescopeLayout$6;->val$projection:Landroid/media/projection/MediaProjection;

    invoke-virtual {p1}, Landroid/media/projection/MediaProjection;->stop()V

    throw v2
.end method
