.class Lcom/helpshift/conversation/viewmodel/ConversationVM$11;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->markConversationResolutionStatus(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

.field final synthetic val$accepted:Z


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Z)V
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iput-boolean p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;->val$accepted:Z

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 3

    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sending resolution event : Accepted? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;->val$accepted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvVM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 563
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_0

    .line 564
    iget-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$11;->val$accepted:Z

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markConversationResolutionStatus(Z)V

    :cond_0
    return-void
.end method
