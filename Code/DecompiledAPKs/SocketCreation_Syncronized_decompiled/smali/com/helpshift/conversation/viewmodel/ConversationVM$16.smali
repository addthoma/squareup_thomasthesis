.class Lcom/helpshift/conversation/viewmodel/ConversationVM$16;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->clearNotifications()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;)V
    .locals 0

    .line 734
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$16;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 738
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$16;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 739
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$16;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->clearNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 740
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$16;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->resetPushNotificationCount(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    return-void
.end method
