.class public abstract Lcom/helpshift/conversation/activeconversation/ViewableConversation;
.super Ljava/lang/Object;
.source "ViewableConversation.java"

# interfaces
.implements Lcom/helpshift/conversation/activeconversation/ConversationDMListener;
.implements Lcom/helpshift/conversation/activeconversation/LiveUpdateDM$TypingIndicatorListener;
.implements Lcom/helpshift/conversation/loaders/ConversationsLoader$LoadMoreConversationsCallback;


# instance fields
.field protected conversationLoader:Lcom/helpshift/conversation/loaders/ConversationsLoader;

.field private conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

.field protected domain:Lcom/helpshift/common/domain/Domain;

.field private isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

.field protected platform:Lcom/helpshift/common/platform/Platform;

.field private sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field protected userDM:Lcom/helpshift/account/domainmodel/UserDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/ConversationsLoader;)V
    .locals 2

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 49
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->platform:Lcom/helpshift/common/platform/Platform;

    .line 50
    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->domain:Lcom/helpshift/common/domain/Domain;

    .line 51
    iput-object p3, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 52
    iput-object p4, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationLoader:Lcom/helpshift/conversation/loaders/ConversationsLoader;

    .line 53
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    return-void
.end method


# virtual methods
.method protected buildPaginationCursor(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/conversation/activeconversation/PaginationCursor;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 377
    :cond_0
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getCreatedAt()Ljava/lang/String;

    move-result-object v0

    .line 385
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-static {v1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p1, v0

    goto :goto_0

    .line 390
    :cond_1
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/helpshift/common/util/HSObservableList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->getCreatedAt()Ljava/lang/String;

    move-result-object p1

    .line 392
    :goto_0
    new-instance v1, Lcom/helpshift/conversation/activeconversation/PaginationCursor;

    invoke-direct {v1, v0, p1}, Lcom/helpshift/conversation/activeconversation/PaginationCursor;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public checkForReopen(ILjava/lang/String;Z)Z
    .locals 1

    .line 164
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 165
    invoke-virtual {v0, p1, p2, p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->checkForReOpen(ILjava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->refreshConversationOnIssueStateUpdate()V

    .line 173
    iget-object p2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_0
    return p1
.end method

.method public abstract getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;
.end method

.method public abstract getAllConversations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;"
        }
    .end annotation
.end method

.method public getConversationVMCallback()Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    return-object v0
.end method

.method public abstract getIdentifier()Ljava/lang/Long;
.end method

.method public abstract getPaginationCursor()Lcom/helpshift/conversation/activeconversation/PaginationCursor;
.end method

.method public getUIConversations()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/UIConversation;",
            ">;"
        }
    .end annotation

    .line 294
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getAllConversations()Ljava/util/List;

    move-result-object v0

    .line 295
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 296
    invoke-static {v0}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 299
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 301
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 302
    new-instance v13, Lcom/helpshift/conversation/activeconversation/UIConversation;

    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getCreatedAt()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    .line 303
    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v10

    iget-object v11, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    iget-boolean v12, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    move-object v4, v13

    move v7, v3

    invoke-direct/range {v4 .. v12}, Lcom/helpshift/conversation/activeconversation/UIConversation;-><init>(JILjava/lang/String;Ljava/lang/String;ZLcom/helpshift/conversation/dto/IssueState;Z)V

    .line 305
    invoke-interface {v1, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public handlePreIssueCreationSuccess()V
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    .line 205
    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->handlePreIssueCreationSuccess()V

    :cond_0
    return-void
.end method

.method public hasMoreMessages()Z
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationLoader:Lcom/helpshift/conversation/loaders/ConversationsLoader;

    invoke-virtual {v0}, Lcom/helpshift/conversation/loaders/ConversationsLoader;->hasMoreMessages()Z

    move-result v0

    return v0
.end method

.method public abstract init()V
.end method

.method public abstract initializeConversationsForUI()V
.end method

.method public isActiveConversationEqual(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    if-nez v1, :cond_1

    return v0

    .line 192
    :cond_1
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 193
    iget-object v0, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    .line 195
    :cond_2
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 196
    iget-object v0, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_3
    return v0
.end method

.method public isAgentTyping()Z
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;->isAgentTyping()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldEnableTypingIndicator()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isConversationVMAttached()Z
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisibleOnUI()Z
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->isVisibleOnUI()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public loadMoreMessages()V
    .locals 3

    .line 312
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationLoader:Lcom/helpshift/conversation/loaders/ConversationsLoader;

    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getPaginationCursor()Lcom/helpshift/conversation/activeconversation/PaginationCursor;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/conversation/loaders/ConversationsLoader;->loadMoreConversations(Lcom/helpshift/conversation/activeconversation/PaginationCursor;Lcom/helpshift/conversation/loaders/ConversationsLoader$LoadMoreConversationsCallback;)V

    :cond_0
    return-void
.end method

.method public loading()V
    .locals 2

    .line 363
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 364
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    .line 365
    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onHistoryLoadingStarted()V

    :cond_0
    return-void
.end method

.method public mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 4

    .line 121
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 122
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 123
    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    const/4 v3, 0x1

    .line 126
    invoke-virtual {v0, p1, v3, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    .line 129
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz p1, :cond_0

    .line 130
    invoke-interface {p1}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onConversationInboxPollSuccess()V

    :cond_0
    const-string p1, "preissue"

    .line 134
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    const-string p2, "issue"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->startLiveUpdates()V

    .line 139
    :cond_1
    iget-object p1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v1, :cond_5

    .line 142
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->refreshConversationOnIssueStateUpdate()V

    .line 148
    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z

    move-result p2

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    .line 150
    :goto_0
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->COMPLETED_ISSUE_CREATED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v0, :cond_3

    const/4 v2, 0x1

    :cond_3
    if-nez v2, :cond_4

    if-nez p2, :cond_5

    .line 154
    :cond_4
    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_5
    return-void
.end method

.method public mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 3

    .line 99
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 100
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    const/4 v2, 0x1

    .line 103
    invoke-virtual {v0, p1, v2, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    .line 106
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz p1, :cond_0

    .line 107
    invoke-interface {p1}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onConversationInboxPollSuccess()V

    .line 111
    :cond_0
    iget-object p1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v1, :cond_1

    .line 114
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->refreshConversationOnIssueStateUpdate()V

    .line 116
    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_1
    return-void
.end method

.method public onAdminAttachmentMessageClicked(Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V
    .locals 2

    .line 86
    sget-object v0, Lcom/helpshift/conversation/activeconversation/ViewableConversation$1;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;

    .line 93
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;->handleClick(Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;)V

    goto :goto_0

    .line 88
    :cond_1
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    .line 89
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->handleClick(Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;)V

    :goto_0
    return-void
.end method

.method public onAgentTypingUpdate(Z)V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    .line 238
    invoke-interface {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onAgentTypingUpdate(Z)V

    :cond_0
    return-void
.end method

.method public onError()V
    .locals 2

    .line 355
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 356
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    .line 357
    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onHistoryLoadingError()V

    :cond_0
    return-void
.end method

.method public onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    .line 212
    invoke-interface {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_0
    return-void
.end method

.method public abstract onNewConversationStarted(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
.end method

.method public onScreenshotMessageClicked(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->handleClick(Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;Z)V"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_0

    .line 323
    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onHistoryLoadingSuccess()V

    .line 326
    :cond_0
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 327
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 330
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz p1, :cond_1

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1, v0, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->prependConversations(Ljava/util/List;Z)V

    :cond_1
    return-void

    .line 336
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 337
    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v4, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v5, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2, v3, v4, v5}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 338
    invoke-virtual {p0, v2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isActiveConversationEqual(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 340
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v3

    invoke-virtual {v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldEnableMessagesClick()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    .line 341
    :goto_1
    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v2, v4, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->initializeMessageListForUI(Ljava/util/List;Z)V

    goto :goto_0

    .line 344
    :cond_4
    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->prependConversations(Ljava/util/List;)V

    .line 346
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    if-eqz v0, :cond_5

    .line 347
    invoke-interface {v0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->prependConversations(Ljava/util/List;Z)V

    .line 350
    :cond_5
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isLoadMoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public abstract prependConversations(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract registerMessagesObserver(Lcom/helpshift/common/util/HSListObserver;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/common/util/HSListObserver<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation
.end method

.method public setConversationVMCallback(Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;)V
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    .line 248
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setListener(Lcom/helpshift/conversation/activeconversation/ConversationDMListener;)V

    return-void
.end method

.method public setLiveUpdateDM(Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    return-void
.end method

.method public abstract shouldOpen()Z
.end method

.method public startLiveUpdates()V
    .locals 2

    .line 218
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 220
    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldEnableTypingIndicator()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v1, p0, v0}, Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;->registerListener(Lcom/helpshift/conversation/activeconversation/LiveUpdateDM$TypingIndicatorListener;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public stopLiveUpdates()V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;->unregisterListener()V

    :cond_0
    return-void
.end method

.method public unregisterConversationVMCallback()V
    .locals 2

    const/4 v0, 0x0

    .line 252
    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->conversationVMCallback:Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    .line 253
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setListener(Lcom/helpshift/conversation/activeconversation/ConversationDMListener;)V

    return-void
.end method
