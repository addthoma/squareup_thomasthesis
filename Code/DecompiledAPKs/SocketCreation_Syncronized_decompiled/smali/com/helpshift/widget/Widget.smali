.class public abstract Lcom/helpshift/widget/Widget;
.super Ljava/lang/Object;
.source "Widget.java"


# instance fields
.field private mediator:Lcom/helpshift/widget/WidgetMediator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method notifyChanged()V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/helpshift/widget/Widget;->mediator:Lcom/helpshift/widget/WidgetMediator;

    if-eqz v0, :cond_0

    .line 17
    invoke-interface {v0, p0}, Lcom/helpshift/widget/WidgetMediator;->onChanged(Lcom/helpshift/widget/Widget;)V

    :cond_0
    return-void
.end method

.method public setMediator(Lcom/helpshift/widget/WidgetMediator;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/helpshift/widget/Widget;->mediator:Lcom/helpshift/widget/WidgetMediator;

    return-void
.end method
