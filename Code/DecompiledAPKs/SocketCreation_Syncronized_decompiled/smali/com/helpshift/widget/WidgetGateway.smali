.class public Lcom/helpshift/widget/WidgetGateway;
.super Ljava/lang/Object;
.source "WidgetGateway.java"


# instance fields
.field private final config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field private final conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 44
    iput-object p2, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    return-void
.end method

.method private getVisibilityForNewConversationAttachImageButton(Lcom/helpshift/widget/ImageAttachmentWidget;)Z
    .locals 2

    .line 182
    invoke-virtual {p0}, Lcom/helpshift/widget/WidgetGateway;->getDefaultVisibilityForAttachImageButton()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p1}, Lcom/helpshift/widget/ImageAttachmentWidget;->getImagePath()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 184
    iget-object p1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isEmailRequired()Z
    .locals 4

    .line 263
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "fullPrivacy"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "requireNameAndEmail"

    invoke-virtual {v0, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v3, "profileFormEnable"

    invoke-virtual {v0, v3}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v3, "requireEmail"

    invoke-virtual {v0, v3}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    return v1
.end method

.method private isProfileFormVisible(Lcom/helpshift/widget/TextWidget;Lcom/helpshift/widget/TextWidget;)Z
    .locals 6

    .line 303
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "fullPrivacy"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "profileFormEnable"

    invoke-virtual {v0, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 308
    iget-object v2, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v3, "hideNameAndEmail"

    invoke-virtual {v2, v3}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 309
    invoke-virtual {p1}, Lcom/helpshift/widget/TextWidget;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v3, 0x1

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 310
    :goto_0
    invoke-virtual {p2}, Lcom/helpshift/widget/TextWidget;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    .line 311
    :goto_1
    iget-object v4, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v5, "requireNameAndEmail"

    invoke-virtual {v4, v5}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v2, :cond_5

    if-eqz p1, :cond_3

    if-nez p2, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1

    :cond_5
    if-eqz v0, :cond_8

    if-eqz v2, :cond_7

    .line 317
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "requireEmail"

    .line 319
    invoke-virtual {v0, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p2, :cond_7

    :cond_6
    if-nez p1, :cond_8

    :cond_7
    const/4 v1, 0x1

    :cond_8
    return v1
.end method


# virtual methods
.method public getDefaultVisibilityForAttachImageButton()Z
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "fullPrivacy"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "allowUserAttachments"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getDefaultVisibilityForConversationInfoButtonWidget()Z
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "showConversationInfoScreen"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public makeAttachImageButtonWidget()Lcom/helpshift/widget/ButtonWidget;
    .locals 2

    .line 163
    new-instance v0, Lcom/helpshift/widget/ButtonWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ButtonWidget;-><init>()V

    .line 164
    invoke-virtual {p0}, Lcom/helpshift/widget/WidgetGateway;->getDefaultVisibilityForAttachImageButton()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-object v0
.end method

.method public makeConfirmationBoxWidget(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/widget/ButtonWidget;
    .locals 1

    .line 141
    new-instance v0, Lcom/helpshift/widget/ButtonWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ButtonWidget;-><init>()V

    .line 142
    invoke-virtual {p0, v0, p1}, Lcom/helpshift/widget/WidgetGateway;->updateConfirmationBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    return-object v0
.end method

.method public makeConversationFooterWidget(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)Lcom/helpshift/widget/ConversationFooterWidget;
    .locals 1

    .line 92
    new-instance v0, Lcom/helpshift/widget/ConversationFooterWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ConversationFooterWidget;-><init>()V

    .line 93
    invoke-virtual {p0, v0, p1, p2}, Lcom/helpshift/widget/WidgetGateway;->updateConversationFooterWidget(Lcom/helpshift/widget/ConversationFooterWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    return-object v0
.end method

.method public makeDescriptionWidget()Lcom/helpshift/widget/DescriptionWidget;
    .locals 10

    .line 200
    new-instance v0, Lcom/helpshift/widget/DescriptionWidget;

    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getMinimumConversationDescriptionLength()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/helpshift/widget/DescriptionWidget;-><init>(I)V

    .line 204
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationArchivalPrefillText()Ljava/lang/String;

    move-result-object v1

    .line 205
    iget-object v2, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v3, "conversationPrefillText"

    invoke-virtual {v2, v3}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    iget-object v3, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationDetail()Lcom/helpshift/conversation/dto/ConversationDetailDTO;

    move-result-object v3

    const-string v4, ""

    if-eqz v3, :cond_1

    .line 208
    iget v5, v3, Lcom/helpshift/conversation/dto/ConversationDetailDTO;->type:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 210
    iget-object v5, v3, Lcom/helpshift/conversation/dto/ConversationDetailDTO;->title:Ljava/lang/String;

    .line 211
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    iget-wide v8, v3, Lcom/helpshift/conversation/dto/ConversationDetailDTO;->timestamp:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-ltz v3, :cond_0

    .line 213
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 214
    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    const-wide/16 v8, 0x1c20

    cmp-long v3, v6, v8

    if-lez v3, :cond_2

    .line 215
    :cond_0
    iget-object v3, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveDescriptionDetail(Ljava/lang/String;I)V

    :cond_1
    move-object v5, v4

    .line 220
    :cond_2
    invoke-static {v5}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v1, v5

    goto :goto_0

    .line 223
    :cond_3
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 225
    iget-object v2, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const/4 v3, 0x3

    invoke-virtual {v2, v1, v3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveDescriptionDetail(Ljava/lang/String;I)V

    goto :goto_0

    .line 227
    :cond_4
    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 229
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveDescriptionDetail(Ljava/lang/String;I)V

    move-object v1, v2

    goto :goto_0

    :cond_5
    move-object v1, v4

    .line 232
    :goto_0
    invoke-virtual {v0, v1}, Lcom/helpshift/widget/DescriptionWidget;->setText(Ljava/lang/String;)V

    return-object v0
.end method

.method public makeEmailWidget()Lcom/helpshift/widget/EmailWidget;
    .locals 2

    .line 254
    new-instance v0, Lcom/helpshift/widget/EmailWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/EmailWidget;-><init>()V

    .line 255
    invoke-direct {p0}, Lcom/helpshift/widget/WidgetGateway;->isEmailRequired()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/EmailWidget;->setRequired(Z)V

    .line 256
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldCreateConversationAnonymously()Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/EmailWidget;->setText(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public makeImageAttachmentWidget()Lcom/helpshift/widget/ImageAttachmentWidget;
    .locals 3

    .line 280
    new-instance v0, Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ImageAttachmentWidget;-><init>()V

    .line 281
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "fullPrivacy"

    invoke-virtual {v1, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 282
    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->setImagePickerFile(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 283
    invoke-virtual {p0, v0}, Lcom/helpshift/widget/WidgetGateway;->save(Lcom/helpshift/widget/ImageAttachmentWidget;)V

    goto :goto_0

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getImageAttachmentDraft()Lcom/helpshift/conversation/dto/ImagePickerFile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->setImagePickerFile(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 287
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->setClickable(Z)V

    :goto_0
    return-object v0
.end method

.method public makeNameWidget()Lcom/helpshift/widget/NameWidget;
    .locals 2

    .line 241
    new-instance v0, Lcom/helpshift/widget/NameWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/NameWidget;-><init>()V

    .line 243
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldCreateConversationAnonymously()Z

    move-result v1

    if-nez v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, "Anonymous"

    .line 249
    :goto_0
    invoke-virtual {v0, v1}, Lcom/helpshift/widget/NameWidget;->setText(Ljava/lang/String;)V

    return-object v0
.end method

.method public makeNewConversationAttachImageButtonWidget(Lcom/helpshift/widget/ImageAttachmentWidget;)Lcom/helpshift/widget/ButtonWidget;
    .locals 1

    .line 169
    new-instance v0, Lcom/helpshift/widget/ButtonWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ButtonWidget;-><init>()V

    .line 170
    invoke-direct {p0, p1}, Lcom/helpshift/widget/WidgetGateway;->getVisibilityForNewConversationAttachImageButton(Lcom/helpshift/widget/ImageAttachmentWidget;)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-object v0
.end method

.method public makeProfileFormWidget(Lcom/helpshift/widget/TextWidget;Lcom/helpshift/widget/TextWidget;)Lcom/helpshift/widget/ProfileFormWidget;
    .locals 1

    .line 297
    new-instance v0, Lcom/helpshift/widget/ProfileFormWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ProfileFormWidget;-><init>()V

    .line 298
    invoke-direct {p0, p1, p2}, Lcom/helpshift/widget/WidgetGateway;->isProfileFormVisible(Lcom/helpshift/widget/TextWidget;Lcom/helpshift/widget/TextWidget;)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/helpshift/widget/ProfileFormWidget;->setVisible(Z)V

    return-object v0
.end method

.method public makeProgressBarWidget()Lcom/helpshift/widget/ProgressBarWidget;
    .locals 2

    .line 324
    new-instance v0, Lcom/helpshift/widget/ProgressBarWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ProgressBarWidget;-><init>()V

    .line 325
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    return-object v0
.end method

.method public makeReplyBoxWidget(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)Lcom/helpshift/widget/ButtonWidget;
    .locals 1

    .line 59
    new-instance v0, Lcom/helpshift/widget/ButtonWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ButtonWidget;-><init>()V

    .line 60
    invoke-virtual {p0, v0, p1, p2}, Lcom/helpshift/widget/WidgetGateway;->updateReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    return-object v0
.end method

.method public makeReplyFieldWidget()Lcom/helpshift/widget/ReplyFieldWidget;
    .locals 2

    .line 54
    new-instance v0, Lcom/helpshift/widget/ReplyFieldWidget;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/helpshift/widget/ReplyFieldWidget;-><init>(Z)V

    return-object v0
.end method

.method public makeScrollJumperWidget()Lcom/helpshift/widget/ScrollJumperWidget;
    .locals 2

    .line 83
    new-instance v0, Lcom/helpshift/widget/ScrollJumperWidget;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/helpshift/widget/ScrollJumperWidget;-><init>(ZZ)V

    return-object v0
.end method

.method public makeStartConversationButtonWidget()Lcom/helpshift/widget/ButtonWidget;
    .locals 2

    .line 48
    new-instance v0, Lcom/helpshift/widget/ButtonWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ButtonWidget;-><init>()V

    .line 49
    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-object v0
.end method

.method public save(Lcom/helpshift/widget/DescriptionWidget;)V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {p1}, Lcom/helpshift/widget/DescriptionWidget;->getText()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveDescriptionDetail(Ljava/lang/String;I)V

    return-void
.end method

.method public save(Lcom/helpshift/widget/ImageAttachmentWidget;)V
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/helpshift/widget/WidgetGateway;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {p1}, Lcom/helpshift/widget/ImageAttachmentWidget;->getImagePickerFile()Lcom/helpshift/conversation/dto/ImagePickerFile;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveImageAttachmentDraft(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    return-void
.end method

.method public updateConfirmationBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 1

    .line 154
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-nez v0, :cond_0

    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 156
    invoke-virtual {p2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldShowConversationResolutionQuestion()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 159
    :goto_0
    invoke-virtual {p1, p2}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-void
.end method

.method public updateConversationFooterWidget(Lcom/helpshift/widget/ConversationFooterWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V
    .locals 3

    .line 99
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->NONE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    .line 101
    iget-boolean v1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v1, :cond_0

    .line 102
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->REDACTED_STATE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 104
    :cond_0
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_2

    .line 105
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldShowCSATInFooter()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 106
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->CSAT_RATING:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 109
    :cond_1
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->START_NEW_CONVERSATION:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 112
    :cond_2
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_3

    .line 113
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->REJECTED_MESSAGE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 115
    :cond_3
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_4

    .line 116
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->ARCHIVAL_MESSAGE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 118
    :cond_4
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/helpshift/widget/WidgetGateway;->config:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 119
    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldShowConversationResolutionQuestion()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 120
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->CONVERSATION_ENDED_MESSAGE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 122
    :cond_5
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_8

    if-eqz p3, :cond_6

    .line 124
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->NONE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 127
    :cond_6
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldShowCSATInFooter()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 128
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->CSAT_RATING:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 131
    :cond_7
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->START_NEW_CONVERSATION:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    goto :goto_0

    .line 134
    :cond_8
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object p3, Lcom/helpshift/conversation/dto/IssueState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p2, p3, :cond_9

    .line 135
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    .line 137
    :cond_9
    :goto_0
    invoke-virtual {p1, v0}, Lcom/helpshift/widget/ConversationFooterWidget;->setState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    return-void
.end method

.method public updateReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V
    .locals 3

    .line 69
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 75
    :cond_2
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 79
    :goto_0
    invoke-virtual {p1, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-void
.end method
