.class public Lcom/helpshift/widget/ScrollJumperWidget;
.super Lcom/helpshift/widget/Widget;
.source "ScrollJumperWidget.java"


# instance fields
.field private isVisible:Z

.field private shouldShowUnreadMessagesIndicator:Z


# direct methods
.method constructor <init>(ZZ)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    .line 14
    iput-boolean p1, p0, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible:Z

    .line 15
    iput-boolean p2, p0, Lcom/helpshift/widget/ScrollJumperWidget;->shouldShowUnreadMessagesIndicator:Z

    return-void
.end method


# virtual methods
.method public isVisible()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible:Z

    return v0
.end method

.method public setShouldShowUnreadMessagesIndicator(Z)V
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/helpshift/widget/ScrollJumperWidget;->shouldShowUnreadMessagesIndicator:Z

    if-eq v0, p1, :cond_0

    .line 35
    iput-boolean p1, p0, Lcom/helpshift/widget/ScrollJumperWidget;->shouldShowUnreadMessagesIndicator:Z

    .line 36
    invoke-virtual {p0}, Lcom/helpshift/widget/ScrollJumperWidget;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible:Z

    if-eq v0, p1, :cond_0

    .line 24
    iput-boolean p1, p0, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible:Z

    .line 25
    invoke-virtual {p0}, Lcom/helpshift/widget/ScrollJumperWidget;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public shouldShowUnreadMessagesIndicator()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/helpshift/widget/ScrollJumperWidget;->shouldShowUnreadMessagesIndicator:Z

    return v0
.end method
