.class public Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;
.super Lcom/helpshift/support/conversations/messages/MessageViewDataBinder;
.source "UserMessageViewDataBinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/helpshift/support/conversations/messages/MessageViewDataBinder<",
        "Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;",
        "Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/helpshift/support/conversations/messages/MessageViewDataBinder;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic bind(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;

    check-cast p2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    invoke-virtual {p0, p1, p2}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->bind(Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V

    return-void
.end method

.method public bind(Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V
    .locals 8

    .line 54
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->getState()Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    move-result-object v0

    .line 55
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->body:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->escapeHtml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    iget-boolean v2, p2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->isRedacted:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->getRedactedBodyText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 57
    :cond_0
    iget-object v2, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageText:Landroid/widget/TextView;

    invoke-virtual {p0, p2, v1}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->applyRedactionStyleIfNeeded(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Landroid/widget/TextView;)V

    const/high16 v1, 0x3f000000    # 0.5f

    .line 64
    iget-object v2, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    const v3, 0x1010038

    invoke-static {v2, v3}, Lcom/helpshift/util/Styles;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 66
    sget-object v3, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$1;->$SwitchMap$com$helpshift$conversation$activeconversation$message$UserMessageState:[I

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/message/UserMessageState;->ordinal()I

    move-result v0

    aget v0, v3, v0

    const-string v3, ""

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eq v0, v4, :cond_4

    const/4 v6, 0x2

    if-eq v0, v6, :cond_3

    const/4 v6, 0x3

    if-eq v0, v6, :cond_2

    const/4 v6, 0x4

    if-eq v0, v6, :cond_1

    move-object v0, v3

    :goto_0
    const/4 v6, 0x1

    goto :goto_1

    .line 86
    :cond_1
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->getSubText()Ljava/lang/String;

    move-result-object v3

    .line 87
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v1, Lcom/helpshift/R$string;->hs__user_sent_message_voice_over:I

    new-array v6, v4, [Ljava/lang/Object;

    .line 88
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->getAccessbilityMessageTime()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v3, Lcom/helpshift/R$string;->hs__sending_msg:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 81
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v4, Lcom/helpshift/R$string;->hs__user_sending_message_voice_over:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v6, 0x0

    goto :goto_1

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v2, Lcom/helpshift/R$string;->hs__sending_fail_msg:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 75
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v2, Lcom/helpshift/R$string;->hs__user_failed_message_voice_over:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    iget-object v2, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v6, Lcom/helpshift/R$attr;->hs__errorTextColor:I

    invoke-static {v2, v6}, Lcom/helpshift/util/Styles;->getColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    goto :goto_0

    .line 68
    :cond_4
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v2, Lcom/helpshift/R$string;->hs__sending_fail_msg:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 69
    iget-object v0, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v2, Lcom/helpshift/R$string;->hs__user_failed_message_voice_over:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v2, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    sget v6, Lcom/helpshift/R$attr;->hs__errorTextColor:I

    invoke-static {v2, v6}, Lcom/helpshift/util/Styles;->getColor(Landroid/content/Context;I)I

    move-result v2

    goto :goto_0

    .line 92
    :goto_1
    iget-object v7, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageLayout:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->subText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->subText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    iget-object v0, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageBubble:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    const/4 v0, 0x0

    if-eqz v4, :cond_5

    .line 98
    iget-object v1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageText:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->linkify(Landroid/widget/TextView;Lcom/helpshift/util/HSLinkify$LinkClickListener;)V

    .line 100
    :cond_5
    iget-object v1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageText:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 101
    iget-object v1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->retryButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v5}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->setViewVisibility(Landroid/view/View;Z)V

    .line 102
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->getUiViewState()Lcom/helpshift/conversation/activeconversation/message/UIViewState;

    move-result-object p2

    .line 103
    iget-object v1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->subText:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/UIViewState;->isFooterVisible()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->setViewVisibility(Landroid/view/View;Z)V

    .line 106
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/UIViewState;->isRoundedBackground()Z

    move-result p2

    if-eqz p2, :cond_6

    sget p2, Lcom/helpshift/R$drawable;->hs__chat_bubble_rounded:I

    goto :goto_2

    :cond_6
    sget p2, Lcom/helpshift/R$drawable;->hs__chat_bubble_user:I

    .line 107
    :goto_2
    iget-object v1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageBubble:Landroid/widget/FrameLayout;

    sget v2, Lcom/helpshift/R$attr;->hs__chatBubbleUserBackgroundColor:I

    invoke-virtual {p0, v1, p2, v2}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->setDrawable(Landroid/view/View;II)V

    if-eqz v5, :cond_7

    .line 110
    iget-object p2, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->retryButton:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 113
    :cond_7
    iget-object p1, p1, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->retryButton:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    return-void
.end method

.method public bridge synthetic createViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->createViewHolder(Landroid/view/ViewGroup;)Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public createViewHolder(Landroid/view/ViewGroup;)Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;
    .locals 6

    .line 28
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/helpshift/R$layout;->hs__msg_txt_user:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 31
    new-instance v0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;-><init>(Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;Landroid/view/View;)V

    .line 34
    iget-object p1, v0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->messageBubble:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    .line 35
    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 36
    iget-object v1, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    .line 39
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 40
    iget-object v3, p0, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/helpshift/R$dimen;->hs__screen_to_conversation_view_ratio:I

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 41
    invoke-virtual {v2}, Landroid/util/TypedValue;->getFloat()F

    move-result v2

    mul-float v1, v1, v2

    const v2, 0x3e4ccccd    # 0.2f

    mul-float v1, v1, v2

    float-to-int v1, v1

    .line 45
    iget v2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 48
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/messages/UserMessageViewDataBinder$ViewHolder;->setListeners()V

    return-object v0
.end method
