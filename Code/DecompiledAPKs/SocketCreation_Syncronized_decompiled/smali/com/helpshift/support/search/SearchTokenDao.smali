.class public interface abstract Lcom/helpshift/support/search/SearchTokenDao;
.super Ljava/lang/Object;
.source "SearchTokenDao.java"


# virtual methods
.method public abstract clear()V
.end method

.method public abstract get(Ljava/lang/String;)Lcom/helpshift/support/search/SearchTokenDto;
.end method

.method public abstract save(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/support/search/SearchTokenDto;",
            ">;)V"
        }
    .end annotation
.end method
