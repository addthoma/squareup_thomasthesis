.class public final Lcom/helpshift/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080020

.field public static final abc_action_bar_item_background_material:I = 0x7f080021

.field public static final abc_btn_borderless_material:I = 0x7f080022

.field public static final abc_btn_check_material:I = 0x7f080023

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080025

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080026

.field public static final abc_btn_colored_material:I = 0x7f080027

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080028

.field public static final abc_btn_radio_material:I = 0x7f080029

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08002b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08002c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08002d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08002e

.field public static final abc_cab_background_internal_bg:I = 0x7f08002f

.field public static final abc_cab_background_top_material:I = 0x7f080030

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080031

.field public static final abc_control_background_material:I = 0x7f080032

.field public static final abc_dialog_material_background:I = 0x7f080033

.field public static final abc_edit_text_material:I = 0x7f080034

.field public static final abc_ic_ab_back_material:I = 0x7f080035

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080036

.field public static final abc_ic_clear_material:I = 0x7f080037

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080038

.field public static final abc_ic_go_search_api_material:I = 0x7f080039

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f08003a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08003b

.field public static final abc_ic_menu_overflow_material:I = 0x7f08003c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08003d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08003e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08003f

.field public static final abc_ic_search_api_material:I = 0x7f080040

.field public static final abc_ic_star_black_16dp:I = 0x7f080041

.field public static final abc_ic_star_black_36dp:I = 0x7f080042

.field public static final abc_ic_star_black_48dp:I = 0x7f080043

.field public static final abc_ic_star_half_black_16dp:I = 0x7f080044

.field public static final abc_ic_star_half_black_36dp:I = 0x7f080045

.field public static final abc_ic_star_half_black_48dp:I = 0x7f080046

.field public static final abc_ic_voice_search_api_material:I = 0x7f080047

.field public static final abc_item_background_holo_dark:I = 0x7f080048

.field public static final abc_item_background_holo_light:I = 0x7f080049

.field public static final abc_list_divider_material:I = 0x7f08004a

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f08004b

.field public static final abc_list_focused_holo:I = 0x7f08004c

.field public static final abc_list_longpressed_holo:I = 0x7f08004d

.field public static final abc_list_pressed_holo_dark:I = 0x7f08004e

.field public static final abc_list_pressed_holo_light:I = 0x7f08004f

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f080050

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f080051

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f080052

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f080053

.field public static final abc_list_selector_holo_dark:I = 0x7f080054

.field public static final abc_list_selector_holo_light:I = 0x7f080055

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080056

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080057

.field public static final abc_ratingbar_indicator_material:I = 0x7f080058

.field public static final abc_ratingbar_material:I = 0x7f080059

.field public static final abc_ratingbar_small_material:I = 0x7f08005a

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f08005b

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f08005c

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f08005d

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f08005e

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f08005f

.field public static final abc_seekbar_thumb_material:I = 0x7f080060

.field public static final abc_seekbar_tick_mark_material:I = 0x7f080061

.field public static final abc_seekbar_track_material:I = 0x7f080062

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f080063

.field public static final abc_spinner_textfield_background_material:I = 0x7f080064

.field public static final abc_switch_thumb_material:I = 0x7f080065

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080066

.field public static final abc_tab_indicator_material:I = 0x7f080067

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080068

.field public static final abc_text_cursor_material:I = 0x7f080069

.field public static final abc_text_select_handle_left_mtrl_dark:I = 0x7f08006a

.field public static final abc_text_select_handle_left_mtrl_light:I = 0x7f08006b

.field public static final abc_text_select_handle_middle_mtrl_dark:I = 0x7f08006c

.field public static final abc_text_select_handle_middle_mtrl_light:I = 0x7f08006d

.field public static final abc_text_select_handle_right_mtrl_dark:I = 0x7f08006e

.field public static final abc_text_select_handle_right_mtrl_light:I = 0x7f08006f

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080070

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f080071

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f080072

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f080073

.field public static final abc_textfield_search_material:I = 0x7f080074

.field public static final abc_vector_test:I = 0x7f080075

.field public static final avd_hide_password:I = 0x7f080081

.field public static final avd_show_password:I = 0x7f080082

.field public static final design_bottom_navigation_item_background:I = 0x7f080119

.field public static final design_fab_background:I = 0x7f08011a

.field public static final design_ic_visibility:I = 0x7f08011b

.field public static final design_ic_visibility_off:I = 0x7f08011c

.field public static final design_password_eye:I = 0x7f08011d

.field public static final design_snackbar_background:I = 0x7f08011e

.field public static final hs___star_filled:I = 0x7f0801f7

.field public static final hs___star_hollow:I = 0x7f0801f8

.field public static final hs__action_download:I = 0x7f0801f9

.field public static final hs__action_download_background:I = 0x7f0801fa

.field public static final hs__action_no:I = 0x7f0801fb

.field public static final hs__action_search:I = 0x7f0801fc

.field public static final hs__action_yes:I = 0x7f0801fd

.field public static final hs__actionbar_compat_shadow:I = 0x7f0801fe

.field public static final hs__admin_image_background:I = 0x7f0801ff

.field public static final hs__attach_screenshot_action_button:I = 0x7f080200

.field public static final hs__attachment_icon:I = 0x7f080201

.field public static final hs__attachment_progressbar_background:I = 0x7f080202

.field public static final hs__back:I = 0x7f080203

.field public static final hs__button_with_border:I = 0x7f080204

.field public static final hs__chat_bubble_admin:I = 0x7f080205

.field public static final hs__chat_bubble_rounded:I = 0x7f080206

.field public static final hs__chat_bubble_user:I = 0x7f080207

.field public static final hs__chat_notif:I = 0x7f080208

.field public static final hs__circle:I = 0x7f080209

.field public static final hs__circle_shape_scroll_jump:I = 0x7f08020a

.field public static final hs__close:I = 0x7f08020b

.field public static final hs__collapse:I = 0x7f08020c

.field public static final hs__disclosure:I = 0x7f08020d

.field public static final hs__error_icon:I = 0x7f08020e

.field public static final hs__expand:I = 0x7f08020f

.field public static final hs__logo:I = 0x7f080210

.field public static final hs__network_error:I = 0x7f080211

.field public static final hs__picker_search:I = 0x7f080212

.field public static final hs__pill:I = 0x7f080213

.field public static final hs__pill_small:I = 0x7f080214

.field public static final hs__placeholder_image:I = 0x7f080215

.field public static final hs__rating_bar:I = 0x7f080216

.field public static final hs__ratingbar_full_empty:I = 0x7f080217

.field public static final hs__ratingbar_full_filled:I = 0x7f080218

.field public static final hs__report_issue:I = 0x7f080219

.field public static final hs__ring:I = 0x7f08021a

.field public static final hs__rounded_corner_filled_rectangle_user_option_selection:I = 0x7f08021b

.field public static final hs__rounded_corner_rectangle:I = 0x7f08021c

.field public static final hs__rounded_corner_rectangle_user_option_selection:I = 0x7f08021d

.field public static final hs__screenshot_clear:I = 0x7f08021e

.field public static final hs__scroll_jump_indicator:I = 0x7f08021f

.field public static final hs__search_on_conversation_done:I = 0x7f080220

.field public static final hs__send:I = 0x7f080221

.field public static final hs__skip_pill_background:I = 0x7f080222

.field public static final hs_action_retry:I = 0x7f080223

.field public static final ic_mtrl_chip_checked_black:I = 0x7f08022f

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f080230

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080231

.field public static final mtrl_tabs_default_indicator:I = 0x7f08039b

.field public static final navigation_empty_icon:I = 0x7f08039c

.field public static final notification_action_background:I = 0x7f080404

.field public static final notification_bg:I = 0x7f080405

.field public static final notification_bg_low:I = 0x7f080406

.field public static final notification_bg_low_normal:I = 0x7f080407

.field public static final notification_bg_low_pressed:I = 0x7f080408

.field public static final notification_bg_normal:I = 0x7f080409

.field public static final notification_bg_normal_pressed:I = 0x7f08040a

.field public static final notification_icon_background:I = 0x7f08040b

.field public static final notification_template_icon_bg:I = 0x7f080410

.field public static final notification_template_icon_low_bg:I = 0x7f080411

.field public static final notification_tile_bg:I = 0x7f080412

.field public static final notify_panel_notification_icon_bg:I = 0x7f080413

.field public static final tooltip_frame_dark:I = 0x7f0804c6

.field public static final tooltip_frame_light:I = 0x7f0804c7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
