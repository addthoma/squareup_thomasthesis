.class public final Lcom/evernote/android/job/util/BatteryStatus;
.super Ljava/lang/Object;
.source "BatteryStatus.java"


# static fields
.field public static final DEFAULT:Lcom/evernote/android/job/util/BatteryStatus;


# instance fields
.field private final mBatteryPercent:F

.field private final mCharging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    new-instance v0, Lcom/evernote/android/job/util/BatteryStatus;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Lcom/evernote/android/job/util/BatteryStatus;-><init>(ZF)V

    sput-object v0, Lcom/evernote/android/job/util/BatteryStatus;->DEFAULT:Lcom/evernote/android/job/util/BatteryStatus;

    return-void
.end method

.method constructor <init>(ZF)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean p1, p0, Lcom/evernote/android/job/util/BatteryStatus;->mCharging:Z

    .line 33
    iput p2, p0, Lcom/evernote/android/job/util/BatteryStatus;->mBatteryPercent:F

    return-void
.end method


# virtual methods
.method public getBatteryPercent()F
    .locals 1

    .line 47
    iget v0, p0, Lcom/evernote/android/job/util/BatteryStatus;->mBatteryPercent:F

    return v0
.end method

.method public isBatteryLow()Z
    .locals 2

    .line 55
    iget v0, p0, Lcom/evernote/android/job/util/BatteryStatus;->mBatteryPercent:F

    const v1, 0x3e19999a    # 0.15f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-boolean v0, p0, Lcom/evernote/android/job/util/BatteryStatus;->mCharging:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCharging()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/evernote/android/job/util/BatteryStatus;->mCharging:Z

    return v0
.end method
