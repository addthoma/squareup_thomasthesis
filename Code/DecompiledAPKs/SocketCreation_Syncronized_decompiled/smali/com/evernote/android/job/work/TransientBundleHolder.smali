.class final Lcom/evernote/android/job/work/TransientBundleHolder;
.super Ljava/lang/Object;
.source "TransientBundleHolder.java"


# static fields
.field private static bundles:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/evernote/android/job/work/TransientBundleHolder;->bundles:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized cleanUpBundle(I)V
    .locals 2

    const-class v0, Lcom/evernote/android/job/work/TransientBundleHolder;

    monitor-enter v0

    .line 30
    :try_start_0
    sget-object v1, Lcom/evernote/android/job/work/TransientBundleHolder;->bundles:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->remove(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized getBundle(I)Landroid/os/Bundle;
    .locals 2

    const-class v0, Lcom/evernote/android/job/work/TransientBundleHolder;

    monitor-enter v0

    .line 26
    :try_start_0
    sget-object v1, Lcom/evernote/android/job/work/TransientBundleHolder;->bundles:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized putBundle(ILandroid/os/Bundle;)V
    .locals 2

    const-class v0, Lcom/evernote/android/job/work/TransientBundleHolder;

    monitor-enter v0

    .line 21
    :try_start_0
    sget-object v1, Lcom/evernote/android/job/work/TransientBundleHolder;->bundles:Landroid/util/SparseArray;

    invoke-virtual {v1, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method
