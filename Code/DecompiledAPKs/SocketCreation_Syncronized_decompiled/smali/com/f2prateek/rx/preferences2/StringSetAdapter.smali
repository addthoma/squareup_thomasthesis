.class final Lcom/f2prateek/rx/preferences2/StringSetAdapter;
.super Ljava/lang/Object;
.source "StringSetAdapter.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/RealPreference$Adapter<",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final INSTANCE:Lcom/f2prateek/rx/preferences2/StringSetAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/f2prateek/rx/preferences2/StringSetAdapter;

    invoke-direct {v0}, Lcom/f2prateek/rx/preferences2/StringSetAdapter;-><init>()V

    sput-object v0, Lcom/f2prateek/rx/preferences2/StringSetAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/StringSetAdapter;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 0

    .line 11
    invoke-virtual {p0, p1, p2}, Lcom/f2prateek/rx/preferences2/StringSetAdapter;->get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/SharedPreferences;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 16
    invoke-interface {p2, p1, v0}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    .line 18
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 11
    check-cast p2, Ljava/util/Set;

    invoke-virtual {p0, p1, p2, p3}, Lcom/f2prateek/rx/preferences2/StringSetAdapter;->set(Ljava/lang/String;Ljava/util/Set;Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public set(Ljava/lang/String;Ljava/util/Set;Landroid/content/SharedPreferences$Editor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SharedPreferences$Editor;",
            ")V"
        }
    .end annotation

    .line 23
    invoke-interface {p3, p1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    return-void
.end method
