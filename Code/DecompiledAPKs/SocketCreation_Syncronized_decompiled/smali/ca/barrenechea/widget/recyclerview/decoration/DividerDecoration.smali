.class public Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "DividerDecoration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration$Builder;
    }
.end annotation


# instance fields
.field private mHeight:I

.field private mLPadding:I

.field private mPaint:Landroid/graphics/Paint;

.field private mRPadding:I


# direct methods
.method private constructor <init>(IIII)V
    .locals 0

    .line 42
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 43
    iput p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mHeight:I

    .line 44
    iput p2, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mLPadding:I

    .line 45
    iput p3, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mRPadding:I

    .line 46
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mPaint:Landroid/graphics/Paint;

    .line 47
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method synthetic constructor <init>(IIIILca/barrenechea/widget/recyclerview/decoration/DividerDecoration$1;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;-><init>(IIII)V

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 0

    .line 76
    iget p2, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mHeight:I

    const/4 p3, 0x0

    invoke-virtual {p1, p3, p3, p3, p2}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 11

    .line 55
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 58
    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 60
    iget v3, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mHeight:I

    add-int/2addr v3, v2

    .line 62
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v5, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mLPadding:I

    add-int/2addr v4, v5

    .line 63
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v5, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mRPadding:I

    sub-int/2addr v1, v5

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v6, v4

    int-to-float v7, v2

    int-to-float v8, v1

    int-to-float v9, v3

    .line 66
    iget-object v10, p0, Lca/barrenechea/widget/recyclerview/decoration/DividerDecoration;->mPaint:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 67
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
