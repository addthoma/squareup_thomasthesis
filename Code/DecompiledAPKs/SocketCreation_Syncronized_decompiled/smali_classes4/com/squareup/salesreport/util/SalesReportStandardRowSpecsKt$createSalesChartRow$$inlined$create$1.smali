.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSalesChartRow(Lcom/squareup/cycler/StandardRowSpec;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "TI;TS;TV;>;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$create$1\n+ 2 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n+ 3 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,87:1\n150#2:88\n253#2:91\n64#3,2:89\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/cycler/StandardRowSpec$create$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currencyCode$inlined:Lcom/squareup/protos/common/CurrencyCode;

.field final synthetic $current24HourClockMode$inlined:Lcom/squareup/time/Current24HourClockMode;

.field final synthetic $exactValueMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

.field final synthetic $exactValueNumberFormatter$inlined:Lcom/squareup/text/Formatter;

.field final synthetic $layoutId:I

.field final synthetic $localTimeFormatter$inlined:Lcom/squareup/salesreport/util/LocalTimeFormatter;

.field final synthetic $localeProvider$inlined:Ljavax/inject/Provider;

.field final synthetic $rangeMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

.field final synthetic $rangeNumberFormatter$inlined:Lcom/squareup/text/Formatter;

.field final synthetic $res$inlined:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(ILcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/Current24HourClockMode;)V
    .locals 0

    iput p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$layoutId:I

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localeProvider$inlined:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localTimeFormatter$inlined:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    iput-object p7, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$rangeMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    iput-object p8, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$rangeNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    iput-object p9, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$currencyCode$inlined:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p10, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$current24HourClockMode$inlined:Lcom/squareup/time/Current24HourClockMode;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$layoutId:I

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 89
    new-instance p2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    invoke-direct {p2, p1, p0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 37
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type V"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
