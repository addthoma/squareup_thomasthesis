.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n433#2,5:88\n438#2:100\n439#2:108\n1103#3,7:93\n1103#3,7:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$$special$$inlined$bind$7"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $tab1$inlined:Lcom/squareup/noho/NohoLabel;

.field final synthetic $tab2$inlined:Lcom/squareup/noho/NohoLabel;

.field final synthetic this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoLabel;Lcom/squareup/noho/NohoLabel;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab1$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab2$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab1$inlined:Lcom/squareup/noho/NohoLabel;

    const-string v0, "tab1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;->getTab1Label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab2$inlined:Lcom/squareup/noho/NohoLabel;

    const-string v1, "tab2"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;->getTab2Label()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab1$inlined:Lcom/squareup/noho/NohoLabel;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;

    iget-object v0, v0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;->getTab1Selected()Z

    move-result v2

    invoke-static {p1, v0, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$setSelection(Lcom/squareup/noho/NohoLabel;Lcom/squareup/util/Res;Z)V

    .line 91
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab2$inlined:Lcom/squareup/noho/NohoLabel;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;

    iget-object v0, v0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;->getTab1Selected()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$setSelection(Lcom/squareup/noho/NohoLabel;Lcom/squareup/util/Res;Z)V

    .line 92
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab1$inlined:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;->getTab1ClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1;->$tab2$inlined:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;->getTab2ClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 101
    new-instance v0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1$2;

    invoke-direct {v0, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1$lambda$1$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
