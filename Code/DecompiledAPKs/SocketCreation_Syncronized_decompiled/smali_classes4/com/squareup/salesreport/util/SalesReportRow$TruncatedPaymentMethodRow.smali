.class public final Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;
.super Lcom/squareup/salesreport/util/SalesReportRow;
.source "SalesReportRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/util/SalesReportRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TruncatedPaymentMethodRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J3\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u0008H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "name",
        "",
        "percentage",
        "Lcom/squareup/util/Percentage;",
        "sales",
        "colorResId",
        "",
        "(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)V",
        "getColorResId",
        "()I",
        "getName",
        "()Ljava/lang/String;",
        "getPercentage",
        "()Lcom/squareup/util/Percentage;",
        "getSales",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final colorResId:I

.field private final name:Ljava/lang/String;

.field private final percentage:Lcom/squareup/util/Percentage;

.field private final sales:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sales"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 108
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/util/SalesReportRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->copy(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/Percentage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sales"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;-><init>(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    iget p1, p1, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getColorResId()I
    .locals 1

    .line 107
    iget v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public final getSales()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TruncatedPaymentMethodRow(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->sales:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", colorResId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;->colorResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
