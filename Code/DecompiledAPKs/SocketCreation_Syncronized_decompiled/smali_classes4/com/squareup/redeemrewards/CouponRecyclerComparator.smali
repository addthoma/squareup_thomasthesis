.class public final Lcom/squareup/redeemrewards/CouponRecyclerComparator;
.super Ljava/lang/Object;
.source "CouponRecyclerComparator.kt"

# interfaces
.implements Lcom/squareup/cycler/ItemComparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/ItemComparator<",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\u0018\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/CouponRecyclerComparator;",
        "Lcom/squareup/cycler/ItemComparator;",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        "()V",
        "areSameContent",
        "",
        "oldItem",
        "newItem",
        "areSameIdentity",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public areSameContent(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)Z
    .locals 1

    const-string v0, "oldItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic areSameContent(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    check-cast p2, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/CouponRecyclerComparator;->areSameContent(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)Z

    move-result p1

    return p1
.end method

.method public areSameIdentity(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)Z
    .locals 1

    const-string v0, "oldItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic areSameIdentity(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    check-cast p2, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/CouponRecyclerComparator;->areSameIdentity(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)Z

    move-result p1

    return p1
.end method

.method public getChangePayload(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)Ljava/lang/Object;
    .locals 1

    const-string v0, "oldItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-static {p0, p1, p2}, Lcom/squareup/cycler/ItemComparator$DefaultImpls;->getChangePayload(Lcom/squareup/cycler/ItemComparator;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getChangePayload(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    check-cast p2, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/CouponRecyclerComparator;->getChangePayload(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
