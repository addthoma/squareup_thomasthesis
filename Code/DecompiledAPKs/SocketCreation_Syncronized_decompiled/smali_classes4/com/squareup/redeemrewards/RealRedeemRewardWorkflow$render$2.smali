.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->render(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/redeemrewards/RedeemRewardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "searchTerm",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/redeemrewards/RedeemRewardState;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/redeemrewards/RedeemRewardState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->$state:Lcom/squareup/redeemrewards/RedeemRewardState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 3

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;->$state:Lcom/squareup/redeemrewards/RedeemRewardState;

    check-cast v2, Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;

    invoke-static {v1, v2, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$searchTermChangedAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
