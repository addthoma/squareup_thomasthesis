.class public interface abstract Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;
.super Ljava/lang/Object;
.source "RedeemRewardsV2Screen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008`\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\u0005H&J\u0008\u0010\t\u001a\u00020\u0005H&J\u0010\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u000cH&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH&J\u0008\u0010\u0010\u001a\u00020\u0005H&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;",
        "",
        "actionBarUsesBackArrow",
        "",
        "applyCustomerCoupon",
        "",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "closeAddItemDialog",
        "closeRedeemPointsScreen",
        "redeemPoints",
        "couponDefinitionToken",
        "",
        "redeemRewardsV2ScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
        "showAddItemDialog",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract actionBarUsesBackArrow()Z
.end method

.method public abstract applyCustomerCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V
.end method

.method public abstract closeAddItemDialog()V
.end method

.method public abstract closeRedeemPointsScreen()V
.end method

.method public abstract redeemPoints(Ljava/lang/String;)V
.end method

.method public abstract redeemRewardsV2ScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showAddItemDialog()V
.end method
