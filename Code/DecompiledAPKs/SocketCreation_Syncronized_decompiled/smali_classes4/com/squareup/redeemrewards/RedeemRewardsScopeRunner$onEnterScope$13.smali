.class final synthetic Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function9;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function9<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/String;",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        ">;",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u0015\u0010\u0002\u001a\u00110\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00062\u0015\u0010\u0007\u001a\u00110\u0008\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\t2\u0015\u0010\n\u001a\u00110\u000b\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u000c2\u0015\u0010\r\u001a\u00110\u0008\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u000e2\u0015\u0010\u000f\u001a\u00110\u0010\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00112\u0015\u0010\u0012\u001a\u00110\u0010\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00132\u0015\u0010\u0014\u001a\u00110\u0008\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00152\u001b\u0010\u0016\u001a\u0017\u0012\u0004\u0012\u00020\u00180\u0017\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00192\u001b\u0010\u001a\u001a\u0017\u0012\u0004\u0012\u00020\u00180\u0017\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u001b\u00a2\u0006\u0002\u0008\u001c"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
        "p1",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "loading",
        "p2",
        "",
        "title",
        "p3",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
        "actionBarAction",
        "p4",
        "errorMessage",
        "p5",
        "",
        "cartPoints",
        "p6",
        "contactPoints",
        "p7",
        "pointsMessage",
        "p8",
        "",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "coupons",
        "p9",
        "rewardTiers",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)V"

    return-object v0
.end method

.method public final invoke(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;)",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;"
        }
    .end annotation

    const-string v0, "p2"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p4"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p7"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p8"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p9"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    move-object v1, v0

    move v2, p1

    move/from16 v6, p5

    move/from16 v7, p6

    .line 355
    invoke-direct/range {v1 .. v10}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;-><init>(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .line 95
    move-object v0, p1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    move-object v4, p3

    check-cast v4, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    move-object v5, p4

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p5

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v6

    move-object/from16 v0, p6

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v7

    move-object/from16 v8, p7

    check-cast v8, Ljava/lang/String;

    move-object/from16 v9, p8

    check-cast v9, Ljava/util/List;

    move-object/from16 v10, p9

    check-cast v10, Ljava/util/List;

    move-object v1, p0

    invoke-virtual/range {v1 .. v10}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;->invoke(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    move-result-object v0

    return-object v0
.end method
