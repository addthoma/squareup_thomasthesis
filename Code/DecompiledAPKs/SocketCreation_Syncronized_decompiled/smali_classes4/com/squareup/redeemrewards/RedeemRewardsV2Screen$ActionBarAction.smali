.class public abstract Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;
.super Ljava/lang/Object;
.source "RedeemRewardsV2Screen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ActionBarAction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$RemoveCustomer;,
        Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$AddThisCustomer;,
        Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$ChooseCustomerToAdd;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000c\r\u000eB!\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007R\u0019\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u0082\u0001\u0003\u000f\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
        "",
        "text",
        "",
        "clickEvent",
        "Lkotlin/Function0;",
        "",
        "(ILkotlin/jvm/functions/Function0;)V",
        "getClickEvent",
        "()Lkotlin/jvm/functions/Function0;",
        "getText",
        "()I",
        "AddThisCustomer",
        "ChooseCustomerToAdd",
        "RemoveCustomer",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$RemoveCustomer;",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$AddThisCustomer;",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction$ChooseCustomerToAdd;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clickEvent:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final text:I


# direct methods
.method private constructor <init>(ILkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;->text:I

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;->clickEvent:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;-><init>(ILkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public final getClickEvent()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;->clickEvent:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getText()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;->text:I

    return v0
.end method
