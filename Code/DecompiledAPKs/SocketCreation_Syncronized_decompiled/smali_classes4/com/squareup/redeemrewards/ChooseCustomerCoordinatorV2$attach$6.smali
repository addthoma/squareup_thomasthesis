.class final Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$6;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerCoordinatorV2.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$6;->this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$6;->invoke(Lcom/squareup/protos/client/rolodex/Contact;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$6;->this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->access$getRunner$p(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    move-result-object v0

    const-string v1, "contact"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;->selectContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
