.class final Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardByCodeWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->render(Lkotlin/Unit;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;->$state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;

    iget-object v2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;->$state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    check-cast v2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;-><init>(Lcom/squareup/protos/client/coupons/Coupon;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
