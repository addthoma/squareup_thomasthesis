.class public final Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;
.super Ljava/lang/Object;
.source "ChooseEligibleItemLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseEligibleItemLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseEligibleItemLayoutRunner.kt\ncom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n*L\n1#1,111:1\n49#2:112\n50#2,3:118\n53#2:133\n599#3,4:113\n601#3:117\n310#4,3:121\n313#4,3:130\n35#5,6:124\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseEligibleItemLayoutRunner.kt\ncom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner\n*L\n71#1:112\n71#1,3:118\n71#1:133\n71#1,4:113\n71#1:117\n71#1,3:121\n71#1,3:130\n71#1,6:124\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001!BM\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020 H\u0016R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen$ItemRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen$ItemRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final res:Lcom/squareup/util/Res;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
    .locals 1
    .param p5    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->view:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p4, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p7, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p8, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->res:Lcom/squareup/util/Res;

    .line 65
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p3, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 66
    iget-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->view:Landroid/view/View;

    sget p3, Lcom/squareup/redeemrewards/addeligible/impl/R$id;->items_recyclerview:I

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 71
    iget-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 112
    sget-object p3, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 113
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 114
    new-instance p3, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {p3}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 118
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 119
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 122
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object p4, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$$special$$inlined$row$1;

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, p4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 73
    sget p4, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    .line 124
    new-instance p5, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {p5, p4, p0}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$$special$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)V

    check-cast p5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, p5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 122
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 121
    invoke-virtual {p3, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 116
    invoke-virtual {p3, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 113
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getDurationFormatter$p(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)Lcom/squareup/text/DurationFormatter;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-object p0
.end method

.method public static final synthetic access$getItemPhotos$p(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-object p0
.end method

.method public static final synthetic access$getPercentageFormatter$p(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getPriceFormatter$p(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;)Lcom/squareup/util/Res;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public showRendering(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 4

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object p2, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$showRendering$1;-><init>(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 100
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;->getUseBackArrow()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 102
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 103
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/redeemrewards/addeligible/impl/R$string;->redeem_rewards_item_in_category_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 104
    new-instance v2, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$showRendering$2;

    invoke-direct {v2, p1}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$showRendering$2;-><init>(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, p2, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 105
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 108
    iget-object p2, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    .line 107
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;->getItems()Ljava/util/List;

    move-result-object p1

    .line 108
    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner;->showRendering(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
