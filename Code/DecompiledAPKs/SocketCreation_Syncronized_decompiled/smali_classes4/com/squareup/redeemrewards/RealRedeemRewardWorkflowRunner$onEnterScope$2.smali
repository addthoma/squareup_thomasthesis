.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRedeemRewardWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRedeemRewardWorkflowRunner.kt\ncom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,58:1\n152#2:59\n*E\n*S KotlinDebug\n*F\n+ 1 RealRedeemRewardWorkflowRunner.kt\ncom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2\n*L\n40#1:59\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "output",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;->invoke(Lcom/squareup/redeemrewards/RedeemRewardOutput;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/redeemrewards/RedeemRewardOutput;)V
    .locals 4

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->access$getContainer$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 59
    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    .line 43
    instance-of v0, p1, Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->access$getAddEligibleItemForCouponLegacyOutputs$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    move-result-object v0

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;->getMatchingItemAdded()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    invoke-virtual {v0, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;->emitOutput(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;)V

    goto :goto_0

    .line 48
    :cond_0
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardOutput$Close;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardOutput$Close;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    :goto_0
    return-void
.end method
