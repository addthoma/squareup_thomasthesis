.class public abstract Lcom/squareup/redeemrewards/RedeemRewardsWorkflowModule;
.super Ljava/lang/Object;
.source "RedeemRewardsWorkflowModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH!\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsWorkflowModule;",
        "",
        "()V",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "transactionDiscountAdapter",
        "Lcom/squareup/payment/TransactionDiscountAdapter;",
        "holdsCoupons$impl_release",
        "provideRedeemRewardWorkflow",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflow;",
        "realRedeemRewardWorkflow",
        "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;",
        "provideRedeemRewardWorkflowRunner",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;",
        "redeemRewardWorkflowRunner",
        "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract holdsCoupons$impl_release(Lcom/squareup/payment/TransactionDiscountAdapter;)Lcom/squareup/checkout/HoldsCoupons;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideRedeemRewardWorkflow(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/redeemrewards/RedeemRewardWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideRedeemRewardWorkflowRunner(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
