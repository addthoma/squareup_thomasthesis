.class public final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\"$\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00018\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0003\u0010\u0004\u001a\u0004\u0008\u0005\u0010\u0006\"\u0016\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00080\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "EMPTY_COUPON",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "kotlin.jvm.PlatformType",
        "EMPTY_COUPON$annotations",
        "()V",
        "getEMPTY_COUPON",
        "()Lcom/squareup/protos/client/coupons/Coupon;",
        "NO_CONTACT",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "redeem-rewards_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EMPTY_COUPON:Lcom/squareup/protos/client/coupons/Coupon;

.field private static final NO_CONTACT:Lcom/squareup/protos/client/rolodex/Contact;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 91
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->NO_CONTACT:Lcom/squareup/protos/client/rolodex/Contact;

    .line 92
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/Coupon$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->build()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->EMPTY_COUPON:Lcom/squareup/protos/client/coupons/Coupon;

    return-void
.end method

.method public static synthetic EMPTY_COUPON$annotations()V
    .locals 0

    return-void
.end method

.method public static final synthetic access$getNO_CONTACT$p()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->NO_CONTACT:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public static final getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->EMPTY_COUPON:Lcom/squareup/protos/client/coupons/Coupon;

    return-object v0
.end method
