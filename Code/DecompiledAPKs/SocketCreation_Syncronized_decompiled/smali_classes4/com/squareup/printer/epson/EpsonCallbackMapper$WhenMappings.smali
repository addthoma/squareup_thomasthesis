.class public final synthetic Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 18

    invoke-static {}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->values()[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_CONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_TIMEOUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_IN_USE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_TYPE_INVALID:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_DISCONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_ALREADY_OPENED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_ALREADY_USED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_BOX_COUNT_OVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_BOX_CLIENT_OVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v11, 0xa

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_UNSUPPORTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v12, 0xb

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_PARAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v13, 0xc

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_MEMORY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v14, 0xd

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_ILLEGAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v15, 0xe

    aput v15, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_PROCESSING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v16, 0xf

    aput v16, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result v1

    const/16 v17, 0x10

    aput v17, v0, v1

    invoke-static {}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->values()[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_SUCCESS:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_DEVICE_BUSY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_TIMEOUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_COVER_OPEN:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_CUTTER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_MECHANICAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_EMPTY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_UNRECOVERABLE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PAPER_PULLED_OUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_ILLEGAL_LENGTH:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PAPER_JAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_TOO_MANY_REQUESTS:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v15, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_SPOOLER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    aput v16, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_AUTORECOVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_SYSTEM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PORT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_INVALID_WINDOW:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_JOB_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_BATTERY_LOW:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NO_MICR_DATA:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NO_MAGNETIC_DATA:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_RECOGNITION:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_READ:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_NOISE_DETECTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_CANCEL_FAILED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PAPER_TYPE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_WAIT_INSERTION:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_ILLEGAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_INSERTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_WAIT_REMOVAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_IN_USE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_CONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_DISCONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_MEMORY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PROCESSING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_PARAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_CANCELED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_PRINTING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1

    return-void
.end method
