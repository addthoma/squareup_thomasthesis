.class final Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;
.super Lkotlin/coroutines/jvm/internal/ContinuationImpl;
.source "EpsonPrinterConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinterConnection;->connectWithRetry(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0082@"
    }
    d2 = {
        "connectWithRetry",
        "",
        "epsonPrinterSdk",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "epsonPrinterInfo",
        "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
        "printTimingData",
        "Lcom/squareup/print/PrintTimingData;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "continuation",
        "Lkotlin/coroutines/Continuation;",
        ""
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.printer.epson.EpsonPrinterConnection"
    f = "EpsonPrinterConnection.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0
    }
    l = {
        0xda
    }
    m = "connectWithRetry"
    n = {
        "this",
        "epsonPrinterSdk",
        "epsonPrinterInfo",
        "printTimingData",
        "clock",
        "connectAttemptCount",
        "retryDelayMs"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "I$0",
        "J$0"
    }
.end annotation


# instance fields
.field I$0:I

.field I$1:I

.field I$2:I

.field J$0:J

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field label:I

.field synthetic result:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-direct {p0, p2}, Lkotlin/coroutines/jvm/internal/ContinuationImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->result:Ljava/lang/Object;

    iget p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    const/high16 v0, -0x80000000

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->connectWithRetry(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
