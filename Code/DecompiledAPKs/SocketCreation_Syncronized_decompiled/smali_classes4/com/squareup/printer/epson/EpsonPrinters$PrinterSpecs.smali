.class public final enum Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;
.super Ljava/lang/Enum;
.source "EpsonPrinters.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonPrinters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrinterSpecs"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008j\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;",
        "",
        "dotsPerLine",
        "",
        "dotsPerInch",
        "maxLineLength",
        "(Ljava/lang/String;IIII)V",
        "getDotsPerInch",
        "()I",
        "getDotsPerLine",
        "getMaxLineLength",
        "TWO_INCH",
        "THREE_INCH_LOW_DPI",
        "THREE_INCH_HIGH_DPI",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

.field public static final enum THREE_INCH_HIGH_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

.field public static final enum THREE_INCH_LOW_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

.field public static final enum TWO_INCH:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;


# instance fields
.field private final dotsPerInch:I

.field private final dotsPerLine:I

.field private final maxLineLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    new-instance v7, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v2, "TWO_INCH"

    const/4 v3, 0x0

    const/16 v4, 0x1a4

    const/16 v5, 0xcb

    const/16 v6, 0x18

    move-object v1, v7

    .line 56
    invoke-direct/range {v1 .. v6}, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;-><init>(Ljava/lang/String;IIII)V

    sput-object v7, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->TWO_INCH:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v9, "THREE_INCH_LOW_DPI"

    const/4 v10, 0x1

    const/16 v11, 0x200

    const/16 v12, 0xb4

    const/16 v13, 0x28

    move-object v8, v1

    .line 57
    invoke-direct/range {v8 .. v13}, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;-><init>(Ljava/lang/String;IIII)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_LOW_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v4, "THREE_INCH_HIGH_DPI"

    const/4 v5, 0x2

    const/16 v6, 0x240

    const/16 v7, 0xcb

    const/16 v8, 0x28

    move-object v3, v1

    .line 58
    invoke-direct/range {v3 .. v8}, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;-><init>(Ljava/lang/String;IIII)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_HIGH_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->$VALUES:[Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->dotsPerLine:I

    iput p4, p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->dotsPerInch:I

    iput p5, p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->maxLineLength:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;
    .locals 1

    const-class v0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    return-object p0
.end method

.method public static values()[Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;
    .locals 1

    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->$VALUES:[Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    invoke-virtual {v0}, [Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    return-object v0
.end method


# virtual methods
.method public final getDotsPerInch()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->dotsPerInch:I

    return v0
.end method

.method public final getDotsPerLine()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->dotsPerLine:I

    return v0
.end method

.method public final getMaxLineLength()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->maxLineLength:I

    return v0
.end method
