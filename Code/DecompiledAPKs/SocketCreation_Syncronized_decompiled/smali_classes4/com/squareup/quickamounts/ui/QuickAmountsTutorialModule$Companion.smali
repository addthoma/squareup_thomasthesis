.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialModule.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;",
        "",
        "()V",
        "provideQuickAmountsPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "preferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "gson",
        "Lcom/google/gson/Gson;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideQuickAmountsPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 3
    .param p1    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lcom/squareup/quickamounts/QuickAmounts;->Companion:Lcom/squareup/quickamounts/QuickAmounts$Companion;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/QuickAmounts$Companion;->getEMPTY()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    new-instance v1, Lcom/squareup/settings/GsonPreferenceAdapter;

    const-class v2, Lcom/squareup/quickamounts/QuickAmounts;

    check-cast v2, Ljava/lang/reflect/Type;

    invoke-direct {v1, p2, v2}, Lcom/squareup/settings/GsonPreferenceAdapter;-><init>(Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)V

    check-cast v1, Lcom/f2prateek/rx/preferences2/Preference$Converter;

    const-string p2, "last-quick-amounts"

    .line 24
    invoke-virtual {p1, p2, v0, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    const-string p2, "preferences.getObject(\n \u2026unts::class.java)\n      )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
