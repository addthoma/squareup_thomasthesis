.class final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;",
        "kotlin.jvm.PlatformType",
        "newAmounts",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/quickamounts/QuickAmounts;)Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;",
            ">;"
        }
    .end annotation

    const-string v0, "newAmounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getPreference$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getPreference$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "preference.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/quickamounts/QuickAmounts;

    .line 42
    new-instance v1, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;

    .line 43
    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {v2, v0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$checkForAutoAmountsEligibility(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z

    move-result v2

    .line 44
    iget-object v3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {v3, v0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$checkForAutoAmountsUpdated(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;)Z

    move-result v0

    .line 42
    invoke-direct {v1, v2, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;-><init>(ZZ)V

    .line 47
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getPreference$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 48
    invoke-static {v1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getPreference$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 51
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$tutorialVisibilities$1;->apply(Lcom/squareup/quickamounts/QuickAmounts;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
