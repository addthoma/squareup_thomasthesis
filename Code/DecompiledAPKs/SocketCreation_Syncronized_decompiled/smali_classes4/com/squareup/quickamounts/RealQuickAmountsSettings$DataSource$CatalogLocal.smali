.class public final Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;
.super Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
.source "RealQuickAmountsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CatalogLocal"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
        "()V",
        "onCache",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;",
        "status",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "newSetAmounts",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 168
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;

    invoke-direct {v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;-><init>()V

    sput-object v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;->INSTANCE:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 168
    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    invoke-direct {v0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
    .locals 0

    .line 168
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;->onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    move-result-object p1

    check-cast p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    return-object p1
.end method
