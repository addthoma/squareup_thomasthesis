.class final Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings;->doUpdate(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $amounts:Ljava/util/List;

.field final synthetic $status:Lcom/squareup/quickamounts/QuickAmountsStatus;

.field final synthetic this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    iput-object p2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->$status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iput-object p3, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->$amounts:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lio/reactivex/Single;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
            ")",
            "Lio/reactivex/Single<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-static {v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$getCogs$p(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Lcom/squareup/cogs/Cogs;

    move-result-object v0

    new-instance v8, Lcom/squareup/quickamounts/SetQuickAmountsTask;

    iget-object v3, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->$status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iget-object v4, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->$amounts:Ljava/util/List;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/quickamounts/SetQuickAmountsTask;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v8, Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;

    invoke-interface {v0, v8}, Lcom/squareup/cogs/Cogs;->asSingleOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;

    move-result-object p1

    .line 70
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1$1;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 73
    sget-object v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1$2;->INSTANCE:Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$doUpdate$1;->apply(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
