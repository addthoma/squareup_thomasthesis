.class public final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Factory;,
        Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Binding;,
        Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmountsSettingsLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmountsSettingsLayoutRunner.kt\ncom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n*L\n1#1,249:1\n1370#2:250\n1401#2,4:251\n1103#3,7:255\n599#4,4:262\n601#4:266\n310#5,6:267\n*E\n*S KotlinDebug\n*F\n+ 1 QuickAmountsSettingsLayoutRunner.kt\ncom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner\n*L\n158#1:250\n158#1,4:251\n176#1,7:255\n79#1,4:262\n79#1:266\n79#1,6:267\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003678B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0018\u0010$\u001a\u00020%2\u0006\u0010#\u001a\u00020\u00022\u0006\u0010&\u001a\u00020\'H\u0002J\u0010\u0010(\u001a\u00020%2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0010\u0010)\u001a\u00020%2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0010\u0010*\u001a\u00020%2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0010\u0010+\u001a\u00020%2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0010\u0010,\u001a\u00020%2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0018\u0010-\u001a\u00020%2\u0006\u0010#\u001a\u00020\u00022\u0006\u0010.\u001a\u00020/H\u0016J\u000c\u00100\u001a\u000201*\u00020\u0002H\u0002J\u000c\u00102\u001a\u00020\'*\u000203H\u0003J\u000c\u00104\u001a\u000203*\u00020\'H\u0002J\u000c\u00105\u001a\u00020\'*\u000203H\u0003R\u0016\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u000f*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \u000f*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\n \u000f*\u0004\u0018\u00010\u00140\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\n \u000f*\u0004\u0018\u00010\u001b0\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001c\u001a\n \u000f*\u0004\u0018\u00010\u001d0\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001e\u001a\n \u000f*\u0004\u0018\u00010\u001d0\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;",
        "view",
        "Landroid/view/View;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "scrubber",
        "Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;",
        "(Landroid/view/View;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;)V",
        "addButton",
        "Lcom/squareup/noho/NohoRow;",
        "kotlin.jvm.PlatformType",
        "previewAmounts",
        "Lcom/squareup/quickamounts/ui/QuickAmountsView;",
        "previewGroup",
        "previewPadlock",
        "Lcom/squareup/padlock/Padlock;",
        "quickAmountsSwitch",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "settingsDescription",
        "Landroid/widget/TextView;",
        "statusDescription",
        "statusGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "createActionBarConfig",
        "Lcom/squareup/noho/NohoActionBar$Config;",
        "rendering",
        "onAmountCleared",
        "",
        "index",
        "",
        "onAmountsUnfocused",
        "setupAddButton",
        "setupAmountData",
        "setupPreview",
        "setupStatus",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "canEditValues",
        "",
        "descriptionId",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "toStatus",
        "viewId",
        "Binding",
        "Factory",
        "RowData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addButton:Lcom/squareup/noho/NohoRow;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final previewAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

.field private final previewGroup:Landroid/view/View;

.field private final previewPadlock:Lcom/squareup/padlock/Padlock;

.field private final quickAmountsSwitch:Lcom/squareup/noho/NohoCheckableRow;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final scrubber:Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;

.field private final settingsDescription:Landroid/widget/TextView;

.field private final statusDescription:Landroid/widget/TextView;

.field private final statusGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrubber"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p3, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->scrubber:Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;

    .line 68
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->status_description:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusDescription:Landroid/widget/TextView;

    .line 69
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->settings_description:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->settingsDescription:Landroid/widget/TextView;

    .line 70
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_checkable:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.quick_amounts_checkable)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 71
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_switch:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.quick_amounts_switch)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->quickAmountsSwitch:Lcom/squareup/noho/NohoCheckableRow;

    .line 72
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->amounts_panel:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 73
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->add_quick_amount:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->addButton:Lcom/squareup/noho/NohoRow;

    .line 74
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_preview_root:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewGroup:Landroid/view/View;

    .line 75
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_preview_amounts:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/quickamounts/ui/QuickAmountsView;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    .line 76
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_preview_padlock:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock;

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewPadlock:Lcom/squareup/padlock/Padlock;

    .line 79
    sget-object p1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string p2, "recyclerView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 263
    new-instance p2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {p2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 81
    sget-object p3, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$1$1;->INSTANCE:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$1$1;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, p3}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 268
    new-instance p3, Lcom/squareup/cycler/StandardRowSpec;

    sget-object p4, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$row$1;

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-direct {p3, p4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 84
    new-instance p4, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {p4, p0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p3, p4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 268
    check-cast p3, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 267
    invoke-virtual {p2, p3}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 265
    invoke-virtual {p2, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    .line 113
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewPadlock:Lcom/squareup/padlock/Padlock;

    iget-object p2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p3, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p2, p3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    return-void

    .line 262
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getScrubber$p(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->scrubber:Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;

    return-object p0
.end method

.method public static final synthetic access$onAmountCleared(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;I)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->onAmountCleared(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;I)V

    return-void
.end method

.method public static final synthetic access$onAmountsUnfocused(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->onAmountsUnfocused(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    return-void
.end method

.method public static final synthetic access$toStatus(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;I)Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->toStatus(I)Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object p0

    return-object p0
.end method

.method private final canEditValues(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)Z
    .locals 1

    .line 225
    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getQuickAmountsStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object p1

    sget-object v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmountsStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 228
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :cond_2
    return v0
.end method

.method private final createActionBarConfig(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    .line 185
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    const/4 v1, 0x1

    .line 186
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitleCentered(Z)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 187
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/quickamounts/settings/impl/R$string;->quick_amounts_settings_title:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$createActionBarConfig$1;

    invoke-direct {v2, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$createActionBarConfig$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 192
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 195
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private final descriptionId(Lcom/squareup/quickamounts/QuickAmountsStatus;)I
    .locals 1

    .line 219
    sget-object v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmountsStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 222
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$string;->quick_amounts_settings_status_off:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 221
    :cond_1
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$string;->quick_amounts_settings_status_set:I

    goto :goto_0

    .line 220
    :cond_2
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$string;->quick_amounts_settings_status_auto:I

    :goto_0
    return p1
.end method

.method private final onAmountCleared(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;I)V
    .locals 0

    .line 199
    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getOnAmountCleared()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final onAmountsUnfocused(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 0

    .line 203
    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getUpdateSetAmountsSettings()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method private final setupAddButton(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->addButton:Lcom/squareup/noho/NohoRow;

    const-string v1, "addButton"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->canEditValues(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 176
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->addButton:Lcom/squareup/noho/NohoRow;

    check-cast v0, Landroid/view/View;

    .line 255
    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAddButton$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAddButton$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setupAmountData(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 157
    invoke-direct/range {p0 .. p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->canEditValues(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)Z

    move-result v11

    .line 158
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getAmounts()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 250
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object v12, v3

    check-cast v12, Ljava/util/Collection;

    .line 252
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v15, v2, 0x1

    if-gez v2, :cond_0

    .line 253
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/squareup/quickamounts/settings/RecyclerEditableText;

    .line 160
    invoke-virtual {v3}, Lcom/squareup/quickamounts/settings/RecyclerEditableText;->getStableId()J

    move-result-wide v4

    .line 161
    invoke-virtual {v3}, Lcom/squareup/quickamounts/settings/RecyclerEditableText;->getEditableText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v6

    .line 163
    invoke-virtual {v3}, Lcom/squareup/quickamounts/settings/RecyclerEditableText;->getEditableText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v7, 0x1

    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getAmounts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v7, :cond_3

    goto :goto_3

    :cond_3
    const/4 v8, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 v8, 0x1

    .line 164
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getFocusLastRow()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getAmounts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v7

    if-ne v2, v3, :cond_5

    const/4 v9, 0x1

    goto :goto_5

    :cond_5
    const/4 v9, 0x0

    .line 165
    :goto_5
    new-instance v3, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$1;

    invoke-direct {v3, v0, v11, v1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;ZLcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    move-object v10, v3

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 168
    new-instance v3, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;

    invoke-direct {v3, v2, v0, v11, v1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;-><init>(ILcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;ZLcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    move-object/from16 v16, v3

    check-cast v16, Lkotlin/jvm/functions/Function0;

    .line 159
    new-instance v7, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    move-object v2, v7

    move-wide v3, v4

    move-object v5, v6

    move v6, v11

    move-object v14, v7

    move v7, v8

    move v8, v9

    move-object/from16 v9, v16

    invoke-direct/range {v2 .. v10}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;-><init>(JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    .line 169
    invoke-interface {v12, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v2, v15

    goto :goto_0

    .line 254
    :cond_6
    check-cast v12, Ljava/util/List;

    .line 171
    iget-object v1, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    invoke-static {v12}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method

.method private final setupPreview(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getPreviewUiAmounts()Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->setQuickAmounts(Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewGroup:Landroid/view/View;

    const-string v0, "previewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->previewAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    const-string v1, "previewAmounts"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final setupStatus(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 4

    .line 131
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->settingsDescription:Landroid/widget/TextView;

    const-string v1, "settingsDescription"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getEligibleForAutoAmounts()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 132
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusGroup:Lcom/squareup/noho/NohoCheckableGroup;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getEligibleForAutoAmounts()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 133
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->quickAmountsSwitch:Lcom/squareup/noho/NohoCheckableRow;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getEligibleForAutoAmounts()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 135
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusGroup:Lcom/squareup/noho/NohoCheckableGroup;

    const/4 v1, 0x0

    move-object v3, v1

    check-cast v3, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusGroup:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getQuickAmountsStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->viewId(Lcom/squareup/quickamounts/QuickAmountsStatus;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 137
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusGroup:Lcom/squareup/noho/NohoCheckableGroup;

    new-instance v3, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupStatus$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupStatus$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    check-cast v3, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->quickAmountsSwitch:Lcom/squareup/noho/NohoCheckableRow;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->quickAmountsSwitch:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getQuickAmountsStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v1

    sget-object v3, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 143
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->quickAmountsSwitch:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupStatus$2;

    invoke-direct {v1, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupStatus$2;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 148
    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getEligibleForAutoAmounts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->getQuickAmountsStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->descriptionId(Lcom/squareup/quickamounts/QuickAmountsStatus;)I

    move-result p1

    goto :goto_1

    .line 151
    :cond_1
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$string;->quick_amounts_settings_status_use_quick_amounts:I

    .line 153
    :goto_1
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->statusDescription:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private final toStatus(I)Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 3

    .line 207
    sget v0, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_auto:I

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    goto :goto_0

    .line 208
    :cond_0
    sget v0, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_set:I

    if-ne p1, v0, :cond_1

    sget-object p1, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    goto :goto_0

    .line 209
    :cond_1
    sget v0, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_off:I

    if-ne p1, v0, :cond_2

    sget-object p1, Lcom/squareup/quickamounts/QuickAmountsStatus;->DISABLED:Lcom/squareup/quickamounts/QuickAmountsStatus;

    :goto_0
    return-object p1

    .line 210
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown quick amounts status view id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final viewId(Lcom/squareup/quickamounts/QuickAmountsStatus;)I
    .locals 1

    .line 213
    sget-object v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmountsStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 216
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_off:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 215
    :cond_1
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_set:I

    goto :goto_0

    .line 214
    :cond_2
    sget p1, Lcom/squareup/quickamounts/settings/impl/R$id;->quick_amounts_auto:I

    :goto_0
    return p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    sget-object p2, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findActionBar(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p2

    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->createActionBarConfig(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 121
    iget-object p2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$showRendering$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->setupStatus(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    .line 124
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->setupAmountData(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->setupAddButton(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    .line 126
    invoke-direct {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->setupPreview(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->showRendering(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
