.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettingsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/Analytics;",
            ")",
            "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;-><init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->newInstance(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow_Factory;->get()Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    move-result-object v0

    return-object v0
.end method
