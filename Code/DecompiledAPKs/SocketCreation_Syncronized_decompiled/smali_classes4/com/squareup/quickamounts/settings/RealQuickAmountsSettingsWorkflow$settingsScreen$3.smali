.class final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealQuickAmountsSettingsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->settingsScreen(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;Z)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "newStatus",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;->invoke(Lcom/squareup/quickamounts/QuickAmountsStatus;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/quickamounts/QuickAmountsStatus;)V
    .locals 3

    const-string v0, "newStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    new-instance v1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-static {v2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->access$getAnalytics$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
