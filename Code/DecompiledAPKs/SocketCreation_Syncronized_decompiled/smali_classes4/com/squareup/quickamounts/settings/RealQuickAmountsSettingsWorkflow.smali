.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealQuickAmountsSettingsWorkflow.kt"

# interfaces
.implements Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;,
        Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealQuickAmountsSettingsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealQuickAmountsSettingsWorkflow.kt\ncom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,376:1\n32#2,12:377\n41#3:389\n56#3,2:390\n41#3:393\n56#3,2:394\n41#3:397\n56#3,2:398\n276#4:392\n276#4:396\n276#4:400\n1370#5:401\n1401#5,4:402\n149#6,5:406\n149#6,5:411\n*E\n*S KotlinDebug\n*F\n+ 1 RealQuickAmountsSettingsWorkflow.kt\ncom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow\n*L\n82#1,12:377\n91#1:389\n91#1,2:390\n98#1:393\n98#1,2:394\n109#1:397\n109#1,2:398\n91#1:392\n98#1:396\n109#1:400\n139#1:401\n139#1,4:402\n175#1,5:406\n185#1,5:411\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 -2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0002,-B-\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u001f\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016\u00a2\u0006\u0002\u0010\u001dJF\u0010\u001e\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020 2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0002JS\u0010#\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u00042\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0016\u00a2\u0006\u0002\u0010%JP\u0010&\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020 2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"2\u0008\u0008\u0002\u0010\'\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020\u001c2\u0006\u0010$\u001a\u00020\u0004H\u0016J\u000c\u0010*\u001a\u00020+*\u00020 H\u0002R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u00168BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;)V",
        "currency",
        "Lcom/squareup/protos/connect/v2/common/Currency;",
        "getCurrency",
        "()Lcom/squareup/protos/connect/v2/common/Currency;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "itemsTutorialDialog",
        "quickAmounts",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "render",
        "state",
        "(Lkotlin/Unit;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "settingsScreen",
        "focusLastRow",
        "",
        "snapshotState",
        "formatForPreview",
        "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final AMOUNTS_KEY:Ljava/lang/String; = "quickAmountsLoader"

.field public static final CREATE_ITEM_KEY:Ljava/lang/String; = "createItemKey"

.field public static final Companion:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Companion;

.field public static final MAX_SET_AMOUNTS:I = 0x3


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->Companion:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "quickAmountsSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p4, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getCurrency$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->getCurrency()Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object p0

    return-object p0
.end method

.method private final formatForPreview(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;
    .locals 2

    .line 189
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    if-ne v0, v1, :cond_0

    .line 190
    invoke-static {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsFormattingKt;->filterZeroes(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsFormattingKt;->formatForPreview(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/text/Formatter;)Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    move-result-object p1

    return-object p1

    .line 192
    :cond_0
    new-instance p1, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;-><init>(Ljava/util/List;)V

    return-object p1
.end method

.method private final getCurrency()Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toCurrency(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v0

    return-object v0
.end method

.method private final itemsTutorialDialog(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 182
    new-instance v0, Lcom/squareup/quickamounts/settings/CreateItemDialogScreen;

    .line 183
    new-instance v1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$itemsTutorialDialog$1;

    invoke-direct {v1, p2, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$itemsTutorialDialog$1;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/quickamounts/QuickAmounts;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 184
    new-instance p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$itemsTutorialDialog$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$itemsTutorialDialog$2;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    .line 182
    invoke-direct {v0, v1, p1}, Lcom/squareup/quickamounts/settings/CreateItemDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 412
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 413
    const-class p2, Lcom/squareup/quickamounts/settings/CreateItemDialogScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 414
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 412
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 185
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final settingsScreen(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;Z)Ljava/util/Map;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;Z)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 134
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v3

    sget-object v4, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    if-ne v3, v4, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/QuickAmounts;->getAutoAmounts()Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 136
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 139
    :goto_0
    check-cast v3, Ljava/lang/Iterable;

    .line 401
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    const/4 v5, 0x0

    .line 403
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v7, v5, 0x1

    if-gez v5, :cond_2

    .line 404
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_2
    check-cast v6, Lcom/squareup/protos/connect/v2/common/Money;

    .line 140
    invoke-static {v6}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_3

    goto :goto_2

    .line 144
    :cond_3
    iget-object v8, v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {v6}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v8, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 147
    :cond_4
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "amount-"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 148
    new-instance v8, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;

    invoke-direct {v8, v5, v0, v2, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$$inlined$mapIndexed$lambda$1;-><init>(ILcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/quickamounts/QuickAmounts;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v9, v6, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v5

    .line 152
    new-instance v8, Lcom/squareup/quickamounts/settings/RecyclerEditableText;

    invoke-direct {v8, v6, v5}, Lcom/squareup/quickamounts/settings/RecyclerEditableText;-><init>(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    invoke-interface {v4, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v5, v7

    goto :goto_1

    .line 405
    :cond_5
    move-object v12, v4

    check-cast v12, Ljava/util/List;

    .line 154
    new-instance v3, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    .line 155
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v10

    .line 156
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts()Z

    move-result v11

    .line 158
    invoke-direct/range {p0 .. p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->formatForPreview(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    move-result-object v13

    .line 160
    new-instance v4, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$1;

    invoke-direct {v4, v0, v2, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$1;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/quickamounts/QuickAmounts;)V

    move-object v15, v4

    check-cast v15, Lkotlin/jvm/functions/Function0;

    .line 163
    new-instance v4, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$2;

    invoke-direct {v4, v0, v2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$2;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object/from16 v16, v4

    check-cast v16, Lkotlin/jvm/functions/Function0;

    .line 166
    new-instance v4, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;

    invoke-direct {v4, v0, v2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$3;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object/from16 v17, v4

    check-cast v17, Lkotlin/jvm/functions/Function1;

    .line 169
    new-instance v4, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$4;

    invoke-direct {v4, v0, v2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$4;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object/from16 v19, v4

    check-cast v19, Lkotlin/jvm/functions/Function0;

    .line 172
    new-instance v4, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;

    invoke-direct {v4, v0, v2, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/quickamounts/QuickAmounts;)V

    move-object/from16 v18, v4

    check-cast v18, Lkotlin/jvm/functions/Function1;

    move-object v9, v3

    move/from16 v14, p3

    .line 154
    invoke-direct/range {v9 .. v19}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 407
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 408
    const-class v2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v4, ""

    invoke-static {v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 409
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 407
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 175
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method static synthetic settingsScreen$default(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;ZILjava/lang/Object;)Ljava/util/Map;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 130
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->settingsScreen(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;Z)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 377
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 382
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 383
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 384
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 385
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 386
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 387
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 388
    :cond_3
    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 82
    :cond_4
    sget-object p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$InitialState;->INSTANCE:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$InitialState;

    move-object v1, p1

    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->render(Lkotlin/Unit;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    instance-of p1, p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$InitialState;

    const-string v0, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    const-string v1, "quickAmountsLoader"

    const-string v2, "this.toFlowable(BUFFER)"

    if-eqz p1, :cond_1

    .line 91
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {p1}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object p1

    .line 389
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 391
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 392
    const-class p2, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    .line 91
    new-instance p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$1;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0, v1, p1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 94
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 p2, 0x0

    new-array p2, p2, [Lkotlin/Pair;

    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 391
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 97
    :cond_1
    instance-of p1, p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    if-eqz p1, :cond_3

    .line 98
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {p1}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object p1

    .line 393
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_2

    .line 395
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 396
    const-class v0, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    .line 98
    new-instance p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$2;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2, v1, p1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 105
    check-cast p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;->getAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;->getNewRowAdded()Z

    move-result p2

    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->settingsScreen(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;Z)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 395
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 108
    :cond_3
    instance-of p1, p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    if-eqz p1, :cond_5

    .line 109
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {p1}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object p1

    .line 397
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_4

    .line 399
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 400
    const-class v0, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    .line 109
    new-instance p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;

    invoke-direct {p1, p0, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2, v1, p1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 117
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$4;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$4;-><init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const-string v0, "createItemKey"

    .line 116
    invoke-static {p3, p1, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 120
    check-cast p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;->getAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    invoke-direct {p0, p1, p3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->itemsTutorialDialog(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;->getAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->settingsScreen$default(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;ZILjava/lang/Object;)Ljava/util/Map;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    .line 399
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 120
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->snapshotState(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
