.class public final Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideLoggedInSqliteQueueFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final converterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final taskInjectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->contextProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->clockProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->taskInjectorProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->converterProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;"
        }
    .end annotation

    .line 66
    new-instance v8, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideLoggedInSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ")",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;"
        }
    .end annotation

    .line 73
    invoke-static/range {p0 .. p6}, Lcom/squareup/queue/QueueModule;->provideLoggedInSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->taskInjectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tape/TaskInjector;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->converterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tape/FileObjectQueue$Converter;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static/range {v1 .. v7}, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->provideLoggedInSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideLoggedInSqliteQueueFactoryFactory;->get()Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    move-result-object v0

    return-object v0
.end method
