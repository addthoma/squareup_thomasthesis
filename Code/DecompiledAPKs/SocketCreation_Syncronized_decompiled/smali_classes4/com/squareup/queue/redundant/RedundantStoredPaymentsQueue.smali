.class public Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;
.super Lcom/squareup/queue/StoredPaymentsQueue;
.source "RedundantStoredPaymentsQueue.java"


# instance fields
.field private final addDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

.field private final removeDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;


# direct methods
.method constructor <init>(Lcom/squareup/queue/ReadableFileObjectQueue;Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/CorruptQueueHelper;)V
    .locals 0
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/ReadableFileObjectQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;",
            "Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ")V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p3}, Lcom/squareup/queue/StoredPaymentsQueue;-><init>(Lcom/squareup/queue/ReadableFileObjectQueue;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    .line 29
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 30
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 36
    iput-object p2, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    .line 37
    iput-object p4, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 2

    .line 44
    invoke-super {p0, p1}, Lcom/squareup/queue/StoredPaymentsQueue;->add(Lcom/squareup/payment/offline/StoredPayment;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/CorruptQueueHelper;->allowInSqliteQueue(Lcom/squareup/payment/offline/StoredPayment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    invoke-virtual {v1, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    :cond_0
    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->add(Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method

.method public close()V
    .locals 1

    .line 61
    invoke-super {p0}, Lcom/squareup/queue/StoredPaymentsQueue;->close()V

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 63
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 64
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->close()Lio/reactivex/Completable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method getSqliteQueue()Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    return-object v0
.end method

.method public getSqliteQueueMonitor()Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    return-object v0
.end method

.method getTapeQueue()Lcom/squareup/queue/ReadableFileObjectQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/ReadableFileObjectQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    return-object v0
.end method

.method public peek()Lcom/squareup/payment/offline/StoredPayment;
    .locals 1

    .line 82
    invoke-super {p0}, Lcom/squareup/queue/StoredPaymentsQueue;->peek()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->peek()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object v0

    return-object v0
.end method

.method public readAll(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)V"
        }
    .end annotation

    .line 104
    invoke-super {p0, p1}, Lcom/squareup/queue/StoredPaymentsQueue;->readAll(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method public remove()V
    .locals 3

    .line 52
    invoke-super {p0}, Lcom/squareup/queue/StoredPaymentsQueue;->remove()V

    .line 53
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->removeFirst()Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;->INSTANCE:Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)V"
        }
    .end annotation

    .line 91
    invoke-super {p0, p1}, Lcom/squareup/queue/StoredPaymentsQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method public size()I
    .locals 1

    .line 73
    invoke-super {p0}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result v0

    return v0
.end method
