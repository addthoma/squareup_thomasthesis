.class public final Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;
.super Ljava/lang/Object;
.source "RedundantStoredPaymentsQueueFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final converterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;",
            ">;"
        }
    .end annotation
.end field

.field private final corruptQueueHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final fileSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->converterProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;)",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;Lcom/squareup/queue/CorruptQueueHelper;)Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;-><init>(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;Lcom/squareup/queue/CorruptQueueHelper;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->converterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/queue/CorruptQueueHelper;

    invoke-static/range {v1 .. v6}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;Lcom/squareup/queue/CorruptQueueHelper;)Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->get()Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;

    move-result-object v0

    return-object v0
.end method
