.class public Lcom/squareup/queue/redundant/RedundantRetrofitQueue;
.super Lcom/squareup/queue/retrofit/RetrofitTaskQueue;
.source "RedundantRetrofitQueue.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/MonitorSqliteQueue;


# instance fields
.field private final addDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

.field private final removeDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

.field private final sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/tape/FileObjectQueue;Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;Lcom/squareup/tape/TaskInjector;Lcom/squareup/queue/CorruptQueueHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ")V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p3}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;-><init>(Lcom/squareup/tape/FileObjectQueue;Lcom/squareup/tape/TaskInjector;)V

    .line 34
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 35
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 41
    iput-object p2, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    .line 42
    iput-object p4, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    .line 43
    new-instance p1, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    new-instance p2, Lcom/squareup/queue/redundant/-$$Lambda$RedundantRetrofitQueue$DgyvGffUqaO1z6lAa3tIBzBqZu4;

    invoke-direct {p2, p0}, Lcom/squareup/queue/redundant/-$$Lambda$RedundantRetrofitQueue$DgyvGffUqaO1z6lAa3tIBzBqZu4;-><init>(Lcom/squareup/queue/redundant/RedundantRetrofitQueue;)V

    invoke-direct {p1, p2}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;-><init>(Lrx/functions/Action0;)V

    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 2

    .line 48
    invoke-super {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/CorruptQueueHelper;->allowInSqliteQueue(Lcom/squareup/queue/retrofit/RetrofitTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v1, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    :cond_0
    return-void
.end method

.method public bridge synthetic add(Lcom/squareup/tape/Task;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public asList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 113
    invoke-super {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->asList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .line 68
    invoke-super {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->close()V

    .line 69
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 71
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    invoke-virtual {v0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->close()V

    return-void
.end method

.method public delayClose(Z)V
    .locals 1

    .line 81
    invoke-super {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->delayClose(Z)V

    .line 82
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->delayClose(Z)V

    return-void
.end method

.method getSqliteQueue()Lcom/squareup/queue/sqlite/shared/SqliteQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    return-object v0
.end method

.method public getSqliteQueueMonitor()Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    return-object v0
.end method

.method getTapeQueue()Lcom/squareup/tape/FileObjectQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/FileObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->backingFileQueue:Lcom/squareup/tape/FileObjectQueue;

    return-object v0
.end method

.method public synthetic lambda$new$0$RedundantRetrofitQueue()V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->close()Lio/reactivex/Completable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public peek()Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 1

    .line 100
    invoke-super {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->peek()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic peek()Lcom/squareup/tape/Task;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->peek()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->peek()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 3

    .line 56
    invoke-super {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->remove()V

    .line 57
    iget-object v0, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->removeFirst()Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;->INSTANCE:Lcom/squareup/queue/redundant/-$$Lambda$HHZoph2UpGUnWJcI_1_WGelnpJA;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)V"
        }
    .end annotation

    .line 122
    invoke-super {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method public size()I
    .locals 1

    .line 91
    invoke-super {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->size()I

    move-result v0

    return v0
.end method
