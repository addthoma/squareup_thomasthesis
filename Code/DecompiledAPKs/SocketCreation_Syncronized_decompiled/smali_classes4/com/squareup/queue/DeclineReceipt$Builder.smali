.class public Lcom/squareup/queue/DeclineReceipt$Builder;
.super Ljava/lang/Object;
.source "DeclineReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/DeclineReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private legacyBillId:Ljava/lang/String;

.field private paymentId:Ljava/lang/String;

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->legacyBillId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/DeclineReceipt;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/queue/DeclineReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/DeclineReceipt;-><init>(Lcom/squareup/queue/DeclineReceipt$Builder;Lcom/squareup/queue/DeclineReceipt$1;)V

    return-object v0
.end method

.method public paymentId(Ljava/lang/String;)Lcom/squareup/queue/DeclineReceipt$Builder;
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method public paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/DeclineReceipt$Builder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 57
    sget-object v0, Lcom/squareup/PaymentType;->BILL:Lcom/squareup/PaymentType;

    if-ne p2, v0, :cond_0

    .line 58
    iput-object p1, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->legacyBillId:Ljava/lang/String;

    goto :goto_0

    .line 60
    :cond_0
    iput-object p1, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->paymentId:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method uniqueKey(Ljava/lang/String;)Lcom/squareup/queue/DeclineReceipt$Builder;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/queue/DeclineReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method
