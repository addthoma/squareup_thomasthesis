.class public abstract Lcom/squareup/queue/QueueRootModule;
.super Ljava/lang/Object;
.source "QueueRootModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueRootModule$Component;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCorruptQueuePreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .line 161
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    new-instance v1, Lcom/squareup/settings/MapPreferenceAdapter;

    invoke-direct {v1, p1}, Lcom/squareup/settings/MapPreferenceAdapter;-><init>(Lcom/google/gson/Gson;)V

    const-string p1, "users-with-corrupt-queue-files"

    invoke-virtual {p0, p1, v0, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideCrossSessionStoreAndForwardTasksQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/RetrofitQueueFactory;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 106
    new-instance v0, Ljava/io/File;

    const-string v1, "logged-out-queue"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p0

    .line 111
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 112
    new-instance v0, Lcom/squareup/queue/QueueRootModule$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/queue/QueueRootModule$1;-><init>(Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/queue/QueueServiceStarter;)V

    invoke-interface {p0, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    const/4 p2, 0x0

    .line 122
    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p0
.end method

.method static provideLastEmptyStoredPaymentLoggedAt(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 149
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "last-empty-stored-payment-logged-at"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideLastQueueServiceStart(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 154
    new-instance v0, Lcom/squareup/settings/LongLocalSetting;

    const-string v1, "last-queue-service-start"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideLoggedOutRetrofitQueueFactory(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/queue/retrofit/RetrofitQueueFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)",
            "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;"
        }
    .end annotation

    .line 99
    new-instance v0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;-><init>(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    return-object v0
.end method

.method static provideLoggedOutTaskInjector()Lcom/squareup/tape/TaskInjector;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 92
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/queue/QueueRootModule$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueRootModule$Component;

    .line 93
    new-instance v1, Lcom/squareup/queue/RetrofitTaskInjector;

    invoke-direct {v1, v0}, Lcom/squareup/queue/RetrofitTaskInjector;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method

.method static provideLoggedOutTaskWatcher(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Landroid/content/SharedPreferences;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)Lcom/squareup/queue/TaskWatcher;
    .locals 9
    .param p0    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 133
    new-instance v3, Lcom/squareup/settings/StringLocalSetting;

    const-string v0, "last-task"

    invoke-direct {v3, p2, v0}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 134
    new-instance v4, Lcom/squareup/settings/IntegerLocalSetting;

    const-string v0, "last-task-repeated-count"

    invoke-direct {v4, p2, v0}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 141
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    const-string v0, "last-task-requires-retry"

    invoke-virtual {p3, v0, p2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v5

    .line 142
    new-instance p2, Lcom/squareup/queue/TaskWatcher;

    move-object v0, p2

    move-object v1, p0

    move-object v2, p1

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/squareup/queue/TaskWatcher;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/f2prateek/rx/preferences2/Preference;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)V

    return-object p2
.end method

.method static provideQueueConverter(Lcom/google/gson/Gson;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/tape/FileObjectQueue$Converter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/queue/QueueGsonConverter;

    invoke-direct {v0, p0}, Lcom/squareup/queue/QueueGsonConverter;-><init>(Lcom/google/gson/Gson;)V

    .line 66
    new-instance p0, Lcom/squareup/RegisterConverter;

    .line 67
    invoke-virtual {p1}, Lcom/squareup/queue/CorruptQueueHelper;->corruptRetrofitTaskPlaceholder()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object p1

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/RegisterConverter;-><init>(Lcom/squareup/queue/QueueGsonConverter;Ljava/io/Serializable;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-object p0
.end method

.method static provideSerializedConverter()Lcom/squareup/tape/SerializedConverter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;"
        }
    .end annotation

    .line 71
    new-instance v0, Lcom/squareup/tape/SerializedConverter;

    invoke-direct {v0}, Lcom/squareup/tape/SerializedConverter;-><init>()V

    return-object v0
.end method

.method static provideStoreAndForwardPaymentTaskConverter(Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/tape/SerializedConverter;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;"
        }
    .end annotation

    .line 81
    new-instance v6, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;-><init>(Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/tape/SerializedConverter;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-object v6
.end method

.method static provideStoredPaymentsQueueCache(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-direct {v0, p0}, Lcom/squareup/queue/retrofit/QueueCache;-><init>(Lcom/squareup/queue/retrofit/QueueFactory;)V

    return-object v0
.end method
