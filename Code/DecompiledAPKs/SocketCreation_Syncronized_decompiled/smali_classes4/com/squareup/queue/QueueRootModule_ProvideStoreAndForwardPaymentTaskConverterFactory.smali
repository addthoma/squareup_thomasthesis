.class public final Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;",
        ">;"
    }
.end annotation


# instance fields
.field private final corruptQueueHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final corruptQueueRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final lastEmptyStoredPaymentLoggedAtProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final queueConverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private final serializedConverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->queueConverterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->serializedConverterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->lastEmptyStoredPaymentLoggedAtProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)",
            "Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;"
        }
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideStoreAndForwardPaymentTaskConverter(Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/tape/SerializedConverter;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;"
        }
    .end annotation

    .line 63
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/queue/QueueRootModule;->provideStoreAndForwardPaymentTaskConverter(Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/tape/SerializedConverter;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->queueConverterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tape/FileObjectQueue$Converter;

    iget-object v1, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->serializedConverterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tape/SerializedConverter;

    iget-object v2, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->lastEmptyStoredPaymentLoggedAtProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v3, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/queue/CorruptQueueHelper;

    iget-object v4, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->provideStoreAndForwardPaymentTaskConverter(Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/tape/SerializedConverter;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->get()Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    move-result-object v0

    return-object v0
.end method
