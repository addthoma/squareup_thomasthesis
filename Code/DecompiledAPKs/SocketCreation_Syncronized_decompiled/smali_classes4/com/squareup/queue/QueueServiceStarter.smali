.class public interface abstract Lcom/squareup/queue/QueueServiceStarter;
.super Ljava/lang/Object;
.source "QueueServiceStarter.java"


# virtual methods
.method public abstract isPaused()Z
.end method

.method public abstract pause()V
.end method

.method public abstract resume()V
.end method

.method public abstract start(Ljava/lang/String;)V
.end method

.method public abstract startWaitingForForeground(Ljava/lang/String;)V
.end method
