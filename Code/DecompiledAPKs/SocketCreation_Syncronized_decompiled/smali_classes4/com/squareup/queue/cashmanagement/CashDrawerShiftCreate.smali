.class public final Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;
.super Lcom/squareup/queue/QueueModuleRpcThreadTask;
.source "CashDrawerShiftCreate.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/QueueModuleRpcThreadTask<",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u000f\u0008\u0012\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000f\u001a\u00020\u0002H\u0014J\u0006\u0010\u0010\u001a\u00020\u0000J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0014J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0019H\u0016R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;",
        "Lcom/squareup/queue/QueueModuleRpcThreadTask;",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;",
        "shift",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
        "(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V",
        "request",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;",
        "(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;)V",
        "cashManagementService",
        "Lcom/squareup/server/cashmanagement/CashManagementService;",
        "request$annotations",
        "()V",
        "getRequest",
        "()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;",
        "callOnRpcThread",
        "createForTest",
        "handleResponseOnMainThread",
        "Lcom/squareup/server/SimpleResponse;",
        "response",
        "inject",
        "",
        "component",
        "Lcom/squareup/queue/QueueModule$Component;",
        "secureCopyWithoutPIIForLogs",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public transient cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    const-string v0, "shift"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/queue/QueueModuleRpcThreadTask;-><init>()V

    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;-><init>()V

    .line 26
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 27
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 28
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opening_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 29
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 30
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opened_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 31
    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->starting_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    move-result-object p1

    const-string v0, "CreateCashDrawerShiftReq\u2026h_money)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/queue/QueueModuleRpcThreadTask;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    return-void
.end method

.method public static synthetic request$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    invoke-interface {v0, v1}, Lcom/squareup/server/cashmanagement/CashManagementService;->createDrawer(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;

    move-result-object v0

    const-string v1, "cashManagementService!!.createDrawer(request)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->callOnRpcThread()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;

    move-result-object v0

    return-object v0
.end method

.method public final createForTest()Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;
    .locals 3

    .line 55
    new-instance v0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;

    .line 56
    iget-object v1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v1

    const-string v2, "create_cash_drawer_shift_request_uuid"

    .line 57
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    move-result-object v1

    const-string v2, "request.newBuilder()\n   \u2026uuid\")\n          .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {v0, v1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;-><init>(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;)V

    return-object v0
.end method

.method public final getRequest()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;->status:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse$Status;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse$Status;->CREATED:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse$Status;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method public bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->handleResponseOnMainThread(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;->request:Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "request.toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
