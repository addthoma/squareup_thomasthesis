.class public final Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideStoredPaymentsQueueFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/StoredPaymentsQueue;",
        ">;"
    }
.end annotation


# instance fields
.field private final directoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsQueueCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;->directoryProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;->storedPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;)",
            "Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;)",
            "Lcom/squareup/queue/StoredPaymentsQueue;"
        }
    .end annotation

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/queue/QueueModule;->provideStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/StoredPaymentsQueue;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;->directoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;->storedPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;->provideStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueFactory;->get()Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object v0

    return-object v0
.end method
