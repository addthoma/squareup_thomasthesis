.class public final Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideQueueConverterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tape/FileObjectQueue$Converter<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final corruptQueueHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final corruptQueueRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)",
            "Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideQueueConverter(Lcom/google/gson/Gson;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/tape/FileObjectQueue$Converter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/queue/QueueRootModule;->provideQueueConverter(Lcom/google/gson/Gson;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/tape/FileObjectQueue$Converter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/tape/FileObjectQueue$Converter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/tape/FileObjectQueue$Converter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/CorruptQueueHelper;

    iget-object v2, p0, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-static {v0, v1, v2}, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->provideQueueConverter(Lcom/google/gson/Gson;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/tape/FileObjectQueue$Converter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->get()Lcom/squareup/tape/FileObjectQueue$Converter;

    move-result-object v0

    return-object v0
.end method
