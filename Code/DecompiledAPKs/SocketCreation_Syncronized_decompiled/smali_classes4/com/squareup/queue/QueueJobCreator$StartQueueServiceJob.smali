.class public Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;
.super Lcom/squareup/backgroundjob/BackgroundJob;
.source "QueueJobCreator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueJobCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StartQueueServiceJob"
.end annotation


# static fields
.field private static final DEBUG_NOTIFICATION_TEXT_FORMAT_STRING:Ljava/lang/String; = "Start queue service job scheduled to be run in ~%d min"

.field private static final DEBUG_NOTIFICATION_TITLE:Ljava/lang/String; = "Debug: Start Queue Service"

.field private static final DEBUG_NOTIFICATION_TITLE_WITH_NETWORK:Ljava/lang/String; = "Debug: Start Queue Service with Network"

.field private static final INITIAL_BACKOFF_INTERVAL_MS:J

.field static final MAX_TIME_UNTIL_START_MS:J

.field static final MIN_TIME_UNTIL_START_MS:J

.field public static final QUALIFIED_NAME:Ljava/lang/String;

.field static final START_REASON_KEY:Ljava/lang/String;

.field public static final START_TAG:Ljava/lang/String;

.field public static final START_WITH_NETWORK_TAG:Ljava/lang/String;


# instance fields
.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 51
    const-class v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->QUALIFIED_NAME:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":start.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_TAG:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":start.with.network.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_WITH_NETWORK_TAG:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":start.reason.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_REASON_KEY:Ljava/lang/String;

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    sput-wide v3, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MIN_TIME_UNTIL_START_MS:J

    .line 57
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x6

    invoke-virtual {v0, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    sput-wide v3, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MAX_TIME_UNTIL_START_MS:J

    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->INITIAL_BACKOFF_INTERVAL_MS:J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/backgroundjob/BackgroundJob;-><init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    .line 70
    iput-object p2, p0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-void
.end method

.method public static startQueueServiceRequest(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest;
    .locals 5

    .line 79
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    .line 80
    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_REASON_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance p0, Lcom/evernote/android/job/JobRequest$Builder;

    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_TAG:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    sget-wide v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MIN_TIME_UNTIL_START_MS:J

    sget-wide v3, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MAX_TIME_UNTIL_START_MS:J

    .line 88
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    .line 89
    invoke-virtual {p0, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    .line 90
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p0

    .line 91
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v3, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MIN_TIME_UNTIL_START_MS:J

    .line 92
    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Start queue service job scheduled to be run in ~%d min"

    .line 91
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Debug: Start Queue Service"

    .line 93
    invoke-static {p0, v1, v0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public static startQueueServiceRequestWithNetwork(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest;
    .locals 5

    .line 108
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    .line 109
    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_REASON_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance p0, Lcom/evernote/android/job/JobRequest$Builder;

    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_WITH_NETWORK_TAG:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    sget-wide v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MIN_TIME_UNTIL_START_MS:J

    sget-wide v3, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MAX_TIME_UNTIL_START_MS:J

    .line 117
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    const/4 v1, 0x1

    .line 130
    invoke-virtual {p0, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setUpdateCurrent(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    sget-object v2, Lcom/evernote/android/job/JobRequest$NetworkType;->CONNECTED:Lcom/evernote/android/job/JobRequest$NetworkType;

    .line 135
    invoke-virtual {p0, v2}, Lcom/evernote/android/job/JobRequest$Builder;->setRequiredNetworkType(Lcom/evernote/android/job/JobRequest$NetworkType;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    .line 136
    invoke-virtual {p0, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setRequirementsEnforced(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    sget-wide v2, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->INITIAL_BACKOFF_INTERVAL_MS:J

    sget-object v4, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->LINEAR:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 137
    invoke-virtual {p0, v2, v3, v4}, Lcom/evernote/android/job/JobRequest$Builder;->setBackoffCriteria(JLcom/evernote/android/job/JobRequest$BackoffPolicy;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    .line 138
    invoke-virtual {p0, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p0

    .line 139
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p0

    .line 140
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v3, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->MIN_TIME_UNTIL_START_MS:J

    .line 141
    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Start queue service job scheduled to be run in ~%d min"

    .line 140
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 142
    sget-object v1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_WITH_NETWORK_TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v2, "Debug: Start Queue Service with Network"

    invoke-static {p0, v1, v2, v0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;ILjava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method


# virtual methods
.method public runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
    .locals 2

    .line 148
    invoke-interface {p1}, Lcom/squareup/backgroundjob/JobParams;->getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object p1

    sget-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_REASON_KEY:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 149
    iget-object v0, p0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-interface {v0, p1}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    .line 150
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method
