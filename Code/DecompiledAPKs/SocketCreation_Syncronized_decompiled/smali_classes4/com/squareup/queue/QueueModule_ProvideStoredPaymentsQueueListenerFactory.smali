.class public final Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideStoredPaymentsQueueListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;->threadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStoredPaymentsQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/queue/QueueModule;->provideStoredPaymentsQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueServiceStarter;

    iget-object v1, p0, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;->provideStoredPaymentsQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideStoredPaymentsQueueListenerFactory;->get()Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    move-result-object v0

    return-object v0
.end method
