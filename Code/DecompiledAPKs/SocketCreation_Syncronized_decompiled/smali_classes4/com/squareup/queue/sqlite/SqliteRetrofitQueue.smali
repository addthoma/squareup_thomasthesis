.class public final Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;
.super Ljava/lang/Object;
.source "SqliteRetrofitQueue.kt"

# interfaces
.implements Lcom/squareup/queue/retrofit/RetrofitQueue;
.implements Lcom/squareup/queue/sqlite/MonitorSqliteQueue;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSqliteRetrofitQueue.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SqliteRetrofitQueue.kt\ncom/squareup/queue/sqlite/SqliteRetrofitQueue\n*L\n1#1,104:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B1\u0012\u0010\u0010\u0003\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0004\u0012\u0010\u0010\u0006\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0005H\u0016J\u0012\u0010\u0014\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0015H\u0017J\u0008\u0010\u0016\u001a\u00020\u0011H\u0016J\u0010\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0005H\u0016J\u0008\u0010\u001c\u001a\u00020\u0011H\u0016J\u001a\u0010\u001d\u001a\u00020\u00112\u0010\u0010\u001e\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u001fH\u0016J\u0008\u0010 \u001a\u00020!H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0003\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "Lcom/squareup/queue/sqlite/MonitorSqliteQueue;",
        "sqliteQueue",
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        "taskInjector",
        "Lcom/squareup/tape/TaskInjector;",
        "queueServiceStarter",
        "Lcom/squareup/queue/QueueServiceStarter;",
        "(Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;Lcom/squareup/tape/TaskInjector;Lcom/squareup/queue/QueueServiceStarter;)V",
        "addDisposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "removeDisposable",
        "sqliteCloser",
        "Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;",
        "add",
        "",
        "task",
        "",
        "asList",
        "",
        "close",
        "delayClose",
        "",
        "getSqliteQueueMonitor",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;",
        "peek",
        "remove",
        "setListener",
        "listener",
        "Lcom/squareup/tape/ObjectQueue$Listener;",
        "size",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

.field private final removeDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

.field private final sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final taskInjector:Lcom/squareup/tape/TaskInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;Lcom/squareup/tape/TaskInjector;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ")V"
        }
    .end annotation

    const-string v0, "sqliteQueue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taskInjector"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queueServiceStarter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->taskInjector:Lcom/squareup/tape/TaskInjector;

    iput-object p3, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    .line 29
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 30
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 31
    new-instance p1, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    new-instance p2, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$sqliteCloser$1;

    invoke-direct {p2, p0}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$sqliteCloser$1;-><init>(Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;)V

    check-cast p2, Lrx/functions/Action0;

    invoke-direct {p1, p2}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;-><init>(Lrx/functions/Action0;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    return-void
.end method

.method public static final synthetic access$getQueueServiceStarter$p(Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;)Lcom/squareup/queue/QueueServiceStarter;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-object p0
.end method

.method public static final synthetic access$getSqliteQueue$p(Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;)Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    return-object p0
.end method


# virtual methods
.method public add(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "task"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 38
    iget-object v1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v1, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p1

    new-instance v1, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$add$1;

    invoke-direct {v1, p0}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$add$1;-><init>(Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {p1, v1}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 37
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public asList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Memory pressure"
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->allEntries()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrDefault(Lio/reactivex/Observable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public close()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 71
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 72
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->close()Lio/reactivex/Completable;

    return-void
.end method

.method public delayClose(Z)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteCloser:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->delayClose(Z)V

    return-void
.end method

.method public getSqliteQueueMonitor()Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    check-cast v0, Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;

    return-object v0
.end method

.method public peek()Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->peekFirst()Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/reactivex/Observable;->blockingFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitTask;

    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->taskInjector:Lcom/squareup/tape/TaskInjector;

    move-object v2, v0

    check-cast v2, Lcom/squareup/tape/Task;

    invoke-interface {v1, v2}, Lcom/squareup/tape/TaskInjector;->injectMembers(Lcom/squareup/tape/Task;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->peek()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->removeDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->removeFirst()Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;)V"
        }
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Calling setListener on an Sqlite queue"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public size()I
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->size()Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrDefault(Lio/reactivex/Observable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method
