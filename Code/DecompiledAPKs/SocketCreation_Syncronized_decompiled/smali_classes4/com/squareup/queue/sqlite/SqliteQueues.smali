.class public final Lcom/squareup/queue/sqlite/SqliteQueues;
.super Ljava/lang/Object;
.source "SqliteQueues.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;
    }
.end annotation


# static fields
.field static final EMPTY_ID:Ljava/lang/String; = "none"

.field static final EMPTY_TIME:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method static convertStreamAndBuffer(Lrx/Observable;Lrx/functions/Func1;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;",
            "Lrx/functions/Func1<",
            "TT;TR;>;)",
            "Lrx/Observable$Transformer<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "TR;>;>;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/queue/sqlite/-$$Lambda$SqliteQueues$J5aekr79fjEtTSeLCrvSDwQRrZ4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/-$$Lambda$SqliteQueues$J5aekr79fjEtTSeLCrvSDwQRrZ4;-><init>(Lrx/Observable;Lrx/functions/Func1;)V

    return-object v0
.end method

.method static synthetic lambda$convertStreamAndBuffer$1(Lrx/Observable;Lrx/functions/Func1;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/queue/sqlite/-$$Lambda$SqliteQueues$_dW5SXGt0vqh7bZYpcQBb2MgQno;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/-$$Lambda$SqliteQueues$_dW5SXGt0vqh7bZYpcQBb2MgQno;-><init>(Lrx/Observable;Lrx/functions/Func1;)V

    invoke-virtual {p2, v0}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lrx/Observable;Lrx/functions/Func1;Ljava/lang/Integer;)Lrx/Observable;
    .locals 1

    .line 31
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p0, p1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 35
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lrx/Observable;->buffer(I)Lrx/Observable;

    move-result-object p0

    :goto_0
    return-object p0
.end method
