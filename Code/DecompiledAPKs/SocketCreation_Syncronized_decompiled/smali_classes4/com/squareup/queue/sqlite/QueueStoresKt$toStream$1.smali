.class final Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;
.super Ljava/lang/Object;
.source "QueueStores.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/QueueStoresKt;->toStream(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u0001H\u0002H\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $mapper:Lcom/squareup/sqldelight/prerelease/RowMapper;

.field final synthetic $this_toStream:Landroid/database/Cursor;


# direct methods
.method constructor <init>(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;->$this_toStream:Landroid/database/Cursor;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;->$mapper:Lcom/squareup/sqldelight/prerelease/RowMapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance v0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1$1;-><init>(Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;)V

    check-cast v0, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 165
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;->$this_toStream:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;->$mapper:Lcom/squareup/sqldelight/prerelease/RowMapper;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;->$this_toStream:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Lcom/squareup/sqldelight/prerelease/RowMapper;->map(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 168
    :cond_0
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 170
    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->tryOnError(Ljava/lang/Throwable;)Z

    :goto_1
    return-void
.end method
