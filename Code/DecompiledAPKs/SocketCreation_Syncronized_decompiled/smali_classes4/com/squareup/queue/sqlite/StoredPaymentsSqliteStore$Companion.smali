.class public final Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;
.super Ljava/lang/Object;
.source "StoredPaymentsSqliteStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStoredPaymentsSqliteStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StoredPaymentsSqliteStore.kt\ncom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion\n*L\n1#1,257:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\nH\u0007\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;",
        "",
        "()V",
        "create",
        "Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;",
        "context",
        "Landroid/app/Application;",
        "userDir",
        "Ljava/io/File;",
        "fileScheduler",
        "Lio/reactivex/Scheduler;",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 233
    invoke-direct {p0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Landroid/app/Application;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;
    .locals 3
    .param p2    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userDir"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    new-instance v1, Lcom/squareup/sqlbrite3/SqlBrite$Builder;

    invoke-direct {v1}, Lcom/squareup/sqlbrite3/SqlBrite$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->build()Lcom/squareup/sqlbrite3/SqlBrite;

    move-result-object v1

    const-string v2, "SqlBrite.Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;-><init>(Landroid/app/Application;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V

    .line 240
    invoke-static {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->access$preload(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;)V

    return-object v0
.end method
