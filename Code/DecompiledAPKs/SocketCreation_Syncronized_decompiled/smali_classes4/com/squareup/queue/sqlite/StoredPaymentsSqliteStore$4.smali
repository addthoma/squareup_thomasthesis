.class final Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;
.super Lkotlin/jvm/internal/Lambda;
.source "StoredPaymentsSqliteStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;-><init>(Landroid/app/Application;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;

    invoke-direct {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;-><init>()V

    sput-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->run()Landroid/database/Cursor;

    move-result-object p1

    sget-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getOLDEST_ENTRY_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/sqlite/QueueStoresKt;->toEntry(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/sqlbrite3/SqlBrite$Query;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;->invoke(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    move-result-object p1

    return-object p1
.end method
