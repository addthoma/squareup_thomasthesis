.class public final Lcom/squareup/queue/sqlite/TasksEntry$Companion;
.super Ljava/lang/Object;
.source "TasksEntry.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/TasksEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u001a\u001a\u00020\u00052\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0007J9\u0010\"\u001a\u00020\u00052\u0008\u0010#\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0007\u00a2\u0006\u0002\u0010$R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0007R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0007R\u001f\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u000f*\u0004\u0018\u00010\u00050\u00050\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0007R\u0017\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0007R\u0017\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0007R\u0017\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0007\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/TasksEntry$Companion;",
        "",
        "()V",
        "ALL_ENTRIES_MAPPER",
        "Lcom/squareup/sqldelight/prerelease/RowMapper;",
        "Lcom/squareup/queue/sqlite/TasksEntry;",
        "getALL_ENTRIES_MAPPER",
        "()Lcom/squareup/sqldelight/prerelease/RowMapper;",
        "ALL_LOCAL_PAYMENT_ENTRIES_MAPPER",
        "getALL_LOCAL_PAYMENT_ENTRIES_MAPPER",
        "COUNT_MAPPER",
        "",
        "getCOUNT_MAPPER",
        "FACTORY",
        "Lcom/squareup/queue/sqlite/TasksModel$Factory;",
        "kotlin.jvm.PlatformType",
        "getFACTORY",
        "()Lcom/squareup/queue/sqlite/TasksModel$Factory;",
        "FIRST_ENTRY_MAPPER",
        "getFIRST_ENTRY_MAPPER",
        "GET_ENTRY_MAPPER",
        "getGET_ENTRY_MAPPER",
        "LOCAL_PAYMENTS_COUNT_MAPPER",
        "getLOCAL_PAYMENTS_COUNT_MAPPER",
        "RIPENED_LOCAL_PAYMENTS_COUNT_MAPPER",
        "getRIPENED_LOCAL_PAYMENTS_COUNT_MAPPER",
        "newTasksEntry",
        "entryId",
        "",
        "timestampMs",
        "isLocalPayment",
        "",
        "data",
        "",
        "newTasksEntryForTest",
        "_id",
        "(Ljava/lang/Long;Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/squareup/queue/sqlite/TasksEntry$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getALL_ENTRIES_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation

    .line 85
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getALL_ENTRIES_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getALL_LOCAL_PAYMENT_ENTRIES_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation

    .line 83
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getALL_LOCAL_PAYMENT_ENTRIES_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getCOUNT_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 77
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getCOUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getFACTORY()Lcom/squareup/queue/sqlite/TasksModel$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/TasksModel$Factory<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation

    .line 66
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getFACTORY$cp()Lcom/squareup/queue/sqlite/TasksModel$Factory;

    move-result-object v0

    return-object v0
.end method

.method public final getFIRST_ENTRY_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation

    .line 81
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getFIRST_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getGET_ENTRY_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation

    .line 82
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getGET_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getLOCAL_PAYMENTS_COUNT_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 78
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getLOCAL_PAYMENTS_COUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getRIPENED_LOCAL_PAYMENTS_COUNT_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 79
    invoke-static {}, Lcom/squareup/queue/sqlite/TasksEntry;->access$getRIPENED_LOCAL_PAYMENTS_COUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final newTasksEntry(Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "data"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-static {p1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p4}, Lcom/squareup/queue/sqlite/QueueStoresKt;->toLong(Z)J

    move-result-wide v6

    const/4 v2, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    move-wide v4, p2

    move-object v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/squareup/queue/sqlite/TasksEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;JJ[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final newTasksEntryForTest(Ljava/lang/Long;Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "data"

    move-object/from16 v8, p6

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-static {p2}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->toLong(Z)J

    move-result-wide v6

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/queue/sqlite/TasksEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;JJ[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
