.class final Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
.source "StoredPaymentsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "GetEntryQuery"
.end annotation


# instance fields
.field private final entry_id:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;Ljava/lang/String;)V
    .locals 1

    .line 232
    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;->this$0:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    .line 233
    new-instance p1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v0, "stored_payments"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v0, "SELECT *\nFROM stored_payments\nWHERE entry_id = ?1\nORDER BY timestamp_ms DESC\nLIMIT 1"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    .line 240
    iput-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;->entry_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bindTo(Landroidx/sqlite/db/SupportSQLiteProgram;)V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;->entry_id:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {p1, v1, v0}, Landroidx/sqlite/db/SupportSQLiteProgram;->bindString(ILjava/lang/String;)V

    return-void
.end method
