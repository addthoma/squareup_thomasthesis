.class public final Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideMaybeSqliteTasksQueueFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final sqliteQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final tapeQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->tapeQueueProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->sqliteQueueProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMaybeSqliteTasksQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/Features;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 45
    invoke-static {p0, p1, p2}, Lcom/squareup/queue/QueueModule;->provideMaybeSqliteTasksQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/Features;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->tapeQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, p0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->sqliteQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v2, p0, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->provideMaybeSqliteTasksQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/Features;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideMaybeSqliteTasksQueueFactory;->get()Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    return-object v0
.end method
