.class public Lcom/squareup/queue/WireMessageJsonAdapter;
.super Ljava/lang/Object;
.source "WireMessageJsonAdapter.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;
.implements Lcom/google/gson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/WireMessageJsonAdapter$MessageWrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer<",
        "Lcom/squareup/wire/Message;",
        ">;",
        "Lcom/google/gson/JsonSerializer<",
        "Lcom/squareup/wire/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final DATA_KEY:Ljava/lang/String; = "proto_serialized_data"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/squareup/wire/Message;
    .locals 1

    .line 27
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object p3

    const-string v0, "proto_serialized_data"

    .line 28
    invoke-virtual {p3, v0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p3

    if-nez p3, :cond_0

    .line 31
    invoke-static {}, Lcom/squareup/LegacyMessageGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    return-object p1

    .line 34
    :cond_0
    :try_start_0
    invoke-virtual {p3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x2

    invoke-static {p1, p3}, Lcom/squareup/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    .line 35
    check-cast p2, Ljava/lang/Class;

    invoke-static {p2}, Lcom/squareup/wire/ProtoAdapter;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object p2

    .line 36
    invoke-virtual {p2, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 38
    new-instance p2, Lcom/google/gson/JsonParseException;

    invoke-direct {p2, p1}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .line 22
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/queue/WireMessageJsonAdapter;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/squareup/wire/Message;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Lcom/squareup/wire/Message;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 1

    .line 44
    check-cast p2, Ljava/lang/Class;

    invoke-static {p2}, Lcom/squareup/wire/ProtoAdapter;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object p2

    .line 45
    invoke-virtual {p2, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p2}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    .line 46
    new-instance p2, Lcom/squareup/queue/WireMessageJsonAdapter$MessageWrapper;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/queue/WireMessageJsonAdapter$MessageWrapper;-><init>(Ljava/lang/String;Lcom/squareup/queue/WireMessageJsonAdapter$1;)V

    .line 47
    invoke-interface {p3, p2}, Lcom/google/gson/JsonSerializationContext;->serialize(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/wire/Message;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/queue/WireMessageJsonAdapter;->serialize(Lcom/squareup/wire/Message;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;

    move-result-object p1

    return-object p1
.end method
