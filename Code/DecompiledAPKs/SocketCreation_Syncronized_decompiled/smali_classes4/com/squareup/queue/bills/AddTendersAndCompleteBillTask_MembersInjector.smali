.class public final Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;
.super Ljava/lang/Object;
.source "AddTendersAndCompleteBillTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final addTendersRequestServerIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;"
        }
    .end annotation
.end field

.field private final billPaymentEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final danglingAuthProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTenderCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;)V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p2, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p4, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->serviceProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p5, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p6, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->danglingAuthProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p7, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p8, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p9, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p10, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p11, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p12, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->billPaymentEventsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v13, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;)V"
        }
    .end annotation

    .line 116
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectBillPaymentEvents(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/payment/BillPaymentEvents;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 110
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectLocalTenderCache(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/print/LocalTenderCache;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method

.method public static injectSettings(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/BillTask_MembersInjector;->injectTickets(Lcom/squareup/queue/bills/BillTask;Lcom/squareup/tickets/Tickets;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillCreationService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectService(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/server/bills/BillCreationService;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->danglingAuthProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectDanglingAuth(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/LocalSetting;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/LocalSetting;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/LocalTenderCache;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectLocalTenderCache(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/print/LocalTenderCache;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectFeatures(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/server/Features;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectSettings(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->billPaymentEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/BillPaymentEvents;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectBillPaymentEvents(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;Lcom/squareup/payment/BillPaymentEvents;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;)V

    return-void
.end method
