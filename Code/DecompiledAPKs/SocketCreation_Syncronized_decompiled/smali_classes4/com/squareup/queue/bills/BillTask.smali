.class public abstract Lcom/squareup/queue/bills/BillTask;
.super Lcom/squareup/queue/TransactionRpcThreadTask;
.source "BillTask.java"

# interfaces
.implements Lcom/squareup/queue/PaymentTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/TransactionRpcThreadTask<",
        "Lcom/squareup/protos/client/bills/CompleteBillResponse;",
        ">;",
        "Lcom/squareup/queue/PaymentTask;"
    }
.end annotation


# instance fields
.field protected final ticketId:Ljava/lang/String;

.field transient tickets:Lcom/squareup/tickets/Tickets;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/queue/TransactionRpcThreadTask;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/bills/BillTask;->ticketId:Ljava/lang/String;

    return-void
.end method

.method static sanityCheck(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 6

    .line 58
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 65
    :goto_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    :goto_1
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 69
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 72
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v3}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_12

    const-wide/16 v3, 0x0

    if-nez p2, :cond_3

    if-eqz v1, :cond_4

    .line 78
    iget-object p1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    cmp-long v5, p1, v3

    if-gtz v5, :cond_2

    goto :goto_2

    .line 79
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Cart includes a tip, but tenders do not"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 81
    :cond_3
    invoke-static {p2, v1}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_11

    .line 86
    :cond_4
    :goto_2
    invoke-static {v3, v4, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 89
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz p2, :cond_8

    .line 90
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Itemization;

    .line 91
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_3

    .line 93
    :cond_5
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz p2, :cond_6

    .line 94
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 95
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 97
    :cond_6
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    if-eqz p2, :cond_8

    .line 98
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_7
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    .line 99
    iget-object v3, v2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v3}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 101
    iget-object v2, v2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 102
    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v3}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_4

    .line 109
    :cond_8
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    if-eqz p2, :cond_e

    .line 110
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_9
    :goto_5
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_e

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 111
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 112
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 113
    invoke-static {p1, v3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_6

    .line 115
    :cond_a
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    if-eqz v2, :cond_b

    .line 116
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    .line 117
    iget-object v3, v3, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_7

    .line 120
    :cond_b
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    if-eqz v2, :cond_d

    .line 121
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;

    .line 122
    iget-object v4, v3, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->surcharge_line_item:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 123
    invoke-static {p1, v4}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 125
    iget-object v3, v3, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->surcharge_line_item:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 126
    iget-object v4, v4, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 127
    invoke-static {p1, v4}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_8

    .line 131
    :cond_d
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v2, :cond_9

    .line 132
    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto/16 :goto_5

    .line 139
    :cond_e
    iget-object p0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    .line 141
    invoke-static {p0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 143
    :cond_f
    invoke-static {p1, p0}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-eqz p2, :cond_10

    return-void

    .line 144
    :cond_10
    new-instance p2, Ljava/lang/IllegalStateException;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    aput-object p1, v0, v1

    const/4 p1, 0x1

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    aput-object p0, v0, p1

    const-string p0, "Itemization amounts (%d) do not sum to cart total (%d)"

    .line 145
    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 82
    :cond_11
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Tender tips do not sum to cart total tips"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 73
    :cond_12
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Tender amounts do not sum to cart total"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abstract getBillId()Lcom/squareup/protos/client/IdPair;
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/bills/BillTask;->ticketId:Ljava/lang/String;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CompleteBillResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 3

    .line 35
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CompleteBillResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/queue/bills/BillTask;->ticketId:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/server/SimpleResponse;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 37
    iget-object p1, p0, Lcom/squareup/queue/bills/BillTask;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/queue/bills/BillTask;->ticketId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/queue/bills/BillTask;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    :cond_0
    return-object v0
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteBillResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/BillTask;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CompleteBillResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method
