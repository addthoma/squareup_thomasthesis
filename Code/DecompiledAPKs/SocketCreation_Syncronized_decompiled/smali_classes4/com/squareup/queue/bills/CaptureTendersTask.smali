.class public final Lcom/squareup/queue/bills/CaptureTendersTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "CaptureTendersTask.kt"

# interfaces
.implements Lcom/squareup/queue/CaptureTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/protos/client/bills/CaptureTendersResponse;",
        "Lcom/squareup/queue/TransactionTasksComponent;",
        ">;",
        "Lcom/squareup/queue/CaptureTask;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004BG\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000e\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00100\u001a\u0002012\u0006\u0010.\u001a\u00020/H\u0002J\u0018\u00100\u001a\u0002012\u0006\u0010.\u001a\u00020/2\u0006\u00102\u001a\u000203H\u0002J\u0008\u00104\u001a\u00020\u0002H\u0014J\u0008\u00105\u001a\u000206H\u0002J\u000e\u00107\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016J\u0008\u00108\u001a\u00020\u0008H\u0016J\u0008\u00109\u001a\u00020:H\u0002J\u000e\u0010;\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000eH\u0016J\u0008\u0010<\u001a\u00020\u0008H\u0016J\u0008\u0010=\u001a\u00020>H\u0016J\u0008\u0010?\u001a\u00020@H\u0016J\u0010\u0010A\u001a\u00020B2\u0006\u0010C\u001a\u00020\u0002H\u0014J\u0010\u0010D\u001a\u0002062\u0006\u0010E\u001a\u00020\u0003H\u0016J\u0010\u0010F\u001a\u0002062\u0006\u0010G\u001a\u00020\'H\u0016J\u0014\u0010H\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020J0IH\u0002J\u0008\u0010K\u001a\u00020LH\u0016R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0014R\u001e\u0010\u0016\u001a\u00020\u00178\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u001e\u0010\u001e\u001a\u00020\u001f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u001e\u0010&\u001a\u00020\'8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010)\"\u0004\u0008*\u0010+\u00a8\u0006M"
    }
    d2 = {
        "Lcom/squareup/queue/bills/CaptureTendersTask;",
        "Lcom/squareup/queue/RpcThreadTask;",
        "Lcom/squareup/protos/client/bills/CaptureTendersResponse;",
        "Lcom/squareup/queue/TransactionTasksComponent;",
        "Lcom/squareup/queue/CaptureTask;",
        "request",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "clientId",
        "",
        "captureTicketId",
        "tenders",
        "",
        "Lcom/squareup/protos/client/bills/Tender;",
        "captureAdjustments",
        "",
        "Lcom/squareup/server/payment/AdjustmentMessage;",
        "capturedItemizations",
        "Lcom/squareup/server/payment/ItemizationMessage;",
        "(Lcom/squareup/protos/client/bills/CaptureTendersRequest;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
        "getCaptureTicketId",
        "()Ljava/lang/String;",
        "getClientId",
        "danglingAuth",
        "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
        "getDanglingAuth",
        "()Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
        "setDanglingAuth",
        "(Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V",
        "getRequest",
        "()Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "service",
        "Lcom/squareup/server/tenders/CaptureTenderService;",
        "getService",
        "()Lcom/squareup/server/tenders/CaptureTenderService;",
        "setService",
        "(Lcom/squareup/server/tenders/CaptureTenderService;)V",
        "getTenders",
        "()Ljava/util/List;",
        "transactionLedgerManager",
        "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "getTransactionLedgerManager",
        "()Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "setTransactionLedgerManager",
        "(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V",
        "asBill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "res",
        "Lcom/squareup/util/Res;",
        "asBillBuilder",
        "Lcom/squareup/billhistory/model/BillHistory$Builder;",
        "billHistoryId",
        "Lcom/squareup/billhistory/model/BillHistoryId;",
        "callOnRpcThread",
        "clearDanglingAuthForThisBill",
        "",
        "getAdjustments",
        "getAuthorizationId",
        "getCompletionDate",
        "Ljava/util/Date;",
        "getItemizations",
        "getTicketId",
        "getTime",
        "",
        "getTotal",
        "Lcom/squareup/protos/common/Money;",
        "handleResponseOnMainThread",
        "Lcom/squareup/server/SimpleResponse;",
        "response",
        "inject",
        "component",
        "logEnqueued",
        "ledger",
        "requireCompleteTenders",
        "",
        "Lcom/squareup/protos/client/bills/CompleteTender;",
        "secureCopyWithoutPIIForLogs",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final captureAdjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final captureTicketId:Ljava/lang/String;

.field private final capturedItemizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final clientId:Ljava/lang/String;

.field public transient danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
    .annotation runtime Lcom/squareup/payment/DanglingPayment;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

.field public transient service:Lcom/squareup/server/tenders/CaptureTenderService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field

.field public transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/CaptureTendersRequest;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "captureTicketId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenders"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "captureAdjustments"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capturedItemizations"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iput-object p2, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->clientId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->captureTicketId:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->tenders:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->captureAdjustments:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->capturedItemizations:Ljava/util/List;

    return-void
.end method

.method private final asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBillIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    const-string v1, "BillHistoryId.forBillIdPair(request.bill_id_pair)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/queue/bills/CaptureTendersTask;->asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method private final asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 11

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 96
    invoke-direct {p0}, Lcom/squareup/queue/bills/CaptureTendersTask;->requireCompleteTenders()Ljava/util/Map;

    move-result-object v1

    .line 98
    iget-object v2, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->tenders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender;

    .line 99
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CompleteTender;

    if-nez v4, :cond_0

    .line 100
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v4, v4, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    const/4 v5, 0x0

    .line 101
    check-cast v5, Lcom/squareup/util/Percentage;

    .line 102
    iget-object v6, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 104
    :try_start_0
    sget-object v6, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v7, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    const-string v8, "amounts.tip_percentage"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    .line 106
    check-cast v6, Ljava/lang/Throwable;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to parse tip percentage "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 111
    :cond_1
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v6

    iget-object v7, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 112
    iget-object v4, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    .line 110
    invoke-static {v3, v6, v7, v4, v5}, Lcom/squareup/billhistory/model/TenderHistory;->fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v3

    .line 109
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v6, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 118
    iget-object v1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v1, p1}, Lcom/squareup/billhistory/Bills;->createBillNoteFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v8

    .line 119
    new-instance p1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 120
    move-object v4, v0

    check-cast v4, Ljava/util/List;

    move-object v0, p0

    check-cast v0, Lcom/squareup/queue/PaymentTask;

    invoke-static {v0}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/Order;

    .line 121
    invoke-direct {p0}, Lcom/squareup/queue/bills/CaptureTendersTask;->getCompletionDate()Ljava/util/Date;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v2, p1

    move-object v3, p2

    .line 119
    invoke-direct/range {v2 .. v10}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    .line 123
    iget-object p2, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    const-string p2, "BillHistory.Builder(\n   \u2026   .setCart(request.cart)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final clearDanglingAuthForThisBill()V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    const-string v1, "danglingAuth"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->getDanglingAuthBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 150
    iget-object v2, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V

    .line 155
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Found danglingAuth for a bill that is already enqueued"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    .line 154
    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 160
    check-cast v0, Ljava/lang/Throwable;

    const-string v1, "Found danglingAuth for a bill that is already enqueued, but couldn\'t clear it"

    .line 159
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final getCompletionDate()Ljava/util/Date;
    .locals 2

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    const-string v1, "Times.parseIso8601Date(r\u2026completed_at.date_string)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 140
    new-instance v1, Ljava/lang/RuntimeException;

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method private final requireCompleteTenders()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation

    .line 127
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 128
    iget-object v1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 129
    move-object v3, v0

    check-cast v3, Ljava/util/Map;

    iget-object v4, v2, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v5, "completeTender.tender_id_pair.client_id"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "completeTender"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 131
    :cond_0
    check-cast v0, Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/queue/bills/CaptureTendersTask;->asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    const-string v0, "asBillBuilder(res).build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected callOnRpcThread()Lcom/squareup/protos/client/bills/CaptureTendersResponse;
    .locals 3

    .line 59
    invoke-direct {p0}, Lcom/squareup/queue/bills/CaptureTendersTask;->clearDanglingAuthForThisBill()V

    .line 60
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->service:Lcom/squareup/server/tenders/CaptureTenderService;

    if-nez v0, :cond_0

    const-string v1, "service"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-interface {v0, v1}, Lcom/squareup/server/tenders/CaptureTenderService;->captureTenders(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v1, :cond_1

    const-string v2, "transactionLedgerManager"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureTenderResponse(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)V

    return-object v0
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CaptureTendersTask;->callOnRpcThread()Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    move-result-object v0

    return-object v0
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->captureAdjustments:Ljava/util/List;

    return-object v0
.end method

.method public getAuthorizationId()Ljava/lang/String;
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v1, "request.bill_id_pair.server_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getCaptureTicketId()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->captureTicketId:Ljava/lang/String;

    return-object v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDanglingAuth()Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    if-nez v0, :cond_0

    const-string v1, "danglingAuth"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->capturedItemizations:Ljava/util/List;

    return-object v0
.end method

.method public final getRequest()Lcom/squareup/protos/client/bills/CaptureTendersRequest;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    return-object v0
.end method

.method public final getService()Lcom/squareup/server/tenders/CaptureTenderService;
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->service:Lcom/squareup/server/tenders/CaptureTenderService;

    if-nez v0, :cond_0

    const-string v1, "service"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->tenders:Ljava/util/List;

    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->captureTicketId:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 134
    invoke-direct {p0}, Lcom/squareup/queue/bills/CaptureTendersTask;->getCompletionDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const-string v1, "request.cart.amounts.total_money"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v0, :cond_0

    const-string v1, "transactionLedgerManager"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CaptureTendersResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CaptureTendersTask;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/CaptureTendersTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CaptureTendersTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    const-string v0, "ledger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureTenderRequest(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->request:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "request.toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setDanglingAuth(Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    return-void
.end method

.method public final setService(Lcom/squareup/server/tenders/CaptureTenderService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->service:Lcom/squareup/server/tenders/CaptureTenderService;

    return-void
.end method

.method public final setTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method
