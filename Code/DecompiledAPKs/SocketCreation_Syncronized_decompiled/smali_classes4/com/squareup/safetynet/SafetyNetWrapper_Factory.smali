.class public final Lcom/squareup/safetynet/SafetyNetWrapper_Factory;
.super Ljava/lang/Object;
.source "SafetyNetWrapper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/safetynet/SafetyNetWrapper;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiAvailabilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final safetyNetClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            ">;"
        }
    .end annotation
.end field

.field private final taskWaiterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/TaskWaiter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/TaskWaiter;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->safetyNetClientProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->apiAvailabilityProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->taskWaiterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/safetynet/SafetyNetWrapper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/TaskWaiter;",
            ">;)",
            "Lcom/squareup/safetynet/SafetyNetWrapper_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/squareup/safetynet/TaskWaiter;)Lcom/squareup/safetynet/SafetyNetWrapper;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/safetynet/SafetyNetWrapper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/safetynet/SafetyNetWrapper;-><init>(Landroid/app/Application;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/squareup/safetynet/TaskWaiter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/safetynet/SafetyNetWrapper;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->safetyNetClientProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/safetynet/SafetyNetClient;

    iget-object v2, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->apiAvailabilityProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/GoogleApiAvailability;

    iget-object v3, p0, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->taskWaiterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/safetynet/TaskWaiter;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->newInstance(Landroid/app/Application;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/squareup/safetynet/TaskWaiter;)Lcom/squareup/safetynet/SafetyNetWrapper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/safetynet/SafetyNetWrapper_Factory;->get()Lcom/squareup/safetynet/SafetyNetWrapper;

    move-result-object v0

    return-object v0
.end method
