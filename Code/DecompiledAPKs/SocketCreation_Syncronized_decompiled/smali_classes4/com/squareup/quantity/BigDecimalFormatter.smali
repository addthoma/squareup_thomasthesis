.class public final Lcom/squareup/quantity/BigDecimalFormatter;
.super Ljava/lang/Object;
.source "BigDecimalFormatter.kt"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quantity/BigDecimalFormatter$Format;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Ljava/math/BigDecimal;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001-B3\u0008\u0007\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u000eH\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0005H\u0002J\u0012\u0010\u0006\u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010\u0002H\u0016J\u0006\u0010#\u001a\u00020$J\u0006\u0010%\u001a\u00020&J\u0008\u0010\'\u001a\u00020\u000eH\u0002J \u0010(\u001a\u00020\u001e2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\u000e\u0010)\u001a\u00020\u001e2\u0006\u0010*\u001a\u00020&J\u000e\u0010+\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020\tR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R\u001a\u0010\u0008\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001c\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/quantity/BigDecimalFormatter;",
        "Lcom/squareup/text/Formatter;",
        "Ljava/math/BigDecimal;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "format",
        "Lcom/squareup/quantity/BigDecimalFormatter$Format;",
        "roundingScale",
        "",
        "roundingMode",
        "Ljava/math/RoundingMode;",
        "(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;)V",
        "decimalFormat",
        "Ljava/text/DecimalFormat;",
        "getFormat",
        "()Lcom/squareup/quantity/BigDecimalFormatter$Format;",
        "setFormat",
        "(Lcom/squareup/quantity/BigDecimalFormatter$Format;)V",
        "locale",
        "maxPaddedFractionDigits",
        "getRoundingMode",
        "()Ljava/math/RoundingMode;",
        "setRoundingMode",
        "(Ljava/math/RoundingMode;)V",
        "getRoundingScale",
        "()I",
        "setRoundingScale",
        "(I)V",
        "copyConfigurableProperties",
        "",
        "from",
        "to",
        "",
        "value",
        "getDecimalSeparator",
        "",
        "isGroupingEnabled",
        "",
        "recreateDecimalFormatIfStale",
        "setFractionDigitsFor",
        "setGroupingEnabled",
        "enabled",
        "setMaxPaddedFractionDigits",
        "count",
        "Format",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private decimalFormat:Ljava/text/DecimalFormat;

.field private format:Lcom/squareup/quantity/BigDecimalFormatter$Format;

.field private locale:Ljava/util/Locale;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private maxPaddedFractionDigits:I

.field private roundingMode:Ljava/math/RoundingMode;

.field private roundingScale:I


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/quantity/BigDecimalFormatter$Format;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/quantity/BigDecimalFormatter$Format;",
            "I)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/quantity/BigDecimalFormatter$Format;",
            "I",
            "Ljava/math/RoundingMode;",
            ")V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "format"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roundingMode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/quantity/BigDecimalFormatter;->format:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    iput p3, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingScale:I

    iput-object p4, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingMode:Ljava/math/RoundingMode;

    .line 73
    iget-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "localeProvider.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Locale;

    iput-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->locale:Ljava/util/Locale;

    .line 74
    iget-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->locale:Ljava/util/Locale;

    invoke-direct {p0, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat(Ljava/util/Locale;)Ljava/text/DecimalFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    .line 75
    iget p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingScale:I

    iput p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->maxPaddedFractionDigits:I

    .line 78
    iget-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    return-void
.end method

.method public synthetic constructor <init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    .line 57
    sget-object p2, Lcom/squareup/quantity/BigDecimalFormatter$Format;->SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 65
    sget p3, Lcom/squareup/util/BigDecimals;->DEFAULT_ROUNDING_SCALE:I

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 70
    sget-object p4, Lcom/squareup/quantity/ItemQuantities;->DEFAULT_QUANTITY_ROUNDING_MODE:Ljava/math/RoundingMode;

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;ILjava/math/RoundingMode;)V

    return-void
.end method

.method private final copyConfigurableProperties(Ljava/text/DecimalFormat;Ljava/text/DecimalFormat;)V
    .locals 1

    .line 158
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->getMinimumFractionDigits()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 159
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 160
    invoke-virtual {p1}, Ljava/text/DecimalFormat;->isGroupingUsed()Z

    move-result p1

    invoke-virtual {p2, p1}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    return-void
.end method

.method private final decimalFormat(Ljava/util/Locale;)Ljava/text/DecimalFormat;
    .locals 1

    .line 144
    invoke-static {p1}, Lcom/squareup/currency_db/NumberFormats;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Ljava/text/DecimalFormat;

    .line 146
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingMode:Ljava/math/RoundingMode;

    invoke-virtual {p1, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    return-object p1

    .line 144
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.text.DecimalFormat"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final recreateDecimalFormatIfStale()Ljava/text/DecimalFormat;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 133
    iget-object v1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->locale:Ljava/util/Locale;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const-string v1, "currentLocale"

    .line 134
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->locale:Ljava/util/Locale;

    .line 135
    iget-object v1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    .line 136
    invoke-direct {p0, v0}, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat(Ljava/util/Locale;)Ljava/text/DecimalFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    .line 137
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    invoke-direct {p0, v1, v0}, Lcom/squareup/quantity/BigDecimalFormatter;->copyConfigurableProperties(Ljava/text/DecimalFormat;Ljava/text/DecimalFormat;)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method private final setFractionDigitsFor(Ljava/text/DecimalFormat;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter$Format;)V
    .locals 1

    .line 168
    sget-object v0, Lcom/squareup/quantity/BigDecimalFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    if-eq p3, v0, :cond_1

    const/4 p2, 0x2

    if-eq p3, p2, :cond_0

    goto :goto_0

    .line 173
    :cond_0
    iget p2, p0, Lcom/squareup/quantity/BigDecimalFormatter;->maxPaddedFractionDigits:I

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {p2}, Ljava/math/BigDecimal;->scale()I

    move-result p2

    iget p3, p0, Lcom/squareup/quantity/BigDecimalFormatter;->maxPaddedFractionDigits:I

    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 176
    :goto_0
    iget p2, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingScale:I

    invoke-virtual {p1, p2}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 54
    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p0, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public format(Ljava/math/BigDecimal;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 87
    :cond_0
    invoke-direct {p0}, Lcom/squareup/quantity/BigDecimalFormatter;->recreateDecimalFormatIfStale()Ljava/text/DecimalFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    .line 88
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    iget-object v1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->format:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/quantity/BigDecimalFormatter;->setFractionDigitsFor(Ljava/text/DecimalFormat;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter$Format;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v0, p1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "decimalFormat.format(value)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getDecimalSeparator()C
    .locals 2

    .line 109
    invoke-direct {p0}, Lcom/squareup/quantity/BigDecimalFormatter;->recreateDecimalFormatIfStale()Ljava/text/DecimalFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    .line 110
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    const-string v1, "decimalFormat.decimalFormatSymbols"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    return v0
.end method

.method public final getFormat()Lcom/squareup/quantity/BigDecimalFormatter$Format;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->format:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    return-object v0
.end method

.method public final getRoundingMode()Ljava/math/RoundingMode;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingMode:Ljava/math/RoundingMode;

    return-object v0
.end method

.method public final getRoundingScale()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingScale:I

    return v0
.end method

.method public final isGroupingEnabled()Z
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->isGroupingUsed()Z

    move-result v0

    return v0
.end method

.method public final setFormat(Lcom/squareup/quantity/BigDecimalFormatter$Format;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->format:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    return-void
.end method

.method public final setGroupingEnabled(Z)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->decimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v0, p1}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    return-void
.end method

.method public final setMaxPaddedFractionDigits(I)V
    .locals 1

    .line 101
    iget v0, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingScale:I

    if-gt p1, v0, :cond_0

    .line 104
    iput p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->maxPaddedFractionDigits:I

    return-void

    .line 102
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "maxPaddedFractionDigits must not exceed roundingScale"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setRoundingMode(Ljava/math/RoundingMode;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingMode:Ljava/math/RoundingMode;

    return-void
.end method

.method public final setRoundingScale(I)V
    .locals 0

    .line 65
    iput p1, p0, Lcom/squareup/quantity/BigDecimalFormatter;->roundingScale:I

    return-void
.end method
