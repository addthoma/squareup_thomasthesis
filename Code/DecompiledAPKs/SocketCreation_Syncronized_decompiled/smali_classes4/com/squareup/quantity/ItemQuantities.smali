.class public final Lcom/squareup/quantity/ItemQuantities;
.super Ljava/lang/Object;
.source "ItemQuantities.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemQuantities.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemQuantities.kt\ncom/squareup/quantity/ItemQuantities\n*L\n1#1,155:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0013\u001a\u00020\u0014*\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0007\u001a\n\u0010\u0016\u001a\u00020\u0017*\u00020\u0018\u001a\u0012\u0010\u0019\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u001a\u001a\u00020\u0001\u001a\u000c\u0010\u001b\u001a\u00020\u0017*\u0004\u0018\u00010\u0018\u001a\u000c\u0010\u001c\u001a\u00020\u0017*\u0004\u0018\u00010\u001d\u001a\u000e\u0010\u001e\u001a\u0004\u0018\u00010\u001f*\u0004\u0018\u00010\u0018\u001a\u0014\u0010 \u001a\u00020!*\u0004\u0018\u00010\"2\u0006\u0010#\u001a\u00020$\u001a\u000e\u0010%\u001a\u0004\u0018\u00010\u001d*\u0004\u0018\u00010&\u001a\n\u0010\'\u001a\u00020\u001f*\u00020(\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0008\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\r\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\u0011\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000f\u00a8\u0006)"
    }
    d2 = {
        "ALLOWED_QUANTITY_CHARACTERS",
        "",
        "ALLOWED_QUANTITY_PRECISION_RANGE",
        "Lkotlin/ranges/IntRange;",
        "getALLOWED_QUANTITY_PRECISION_RANGE",
        "()Lkotlin/ranges/IntRange;",
        "DEFAULT_QUANTITY_PRECISION",
        "",
        "DEFAULT_QUANTITY_ROUNDING_MODE",
        "Ljava/math/RoundingMode;",
        "DEFAULT_UNIT_ABBREVIATION",
        "DEFAULT_UNIT_NAME",
        "GENERIC_UNIT_ABBREVIATION",
        "GENERIC_UNIT_NAME_ID",
        "getGENERIC_UNIT_NAME_ID",
        "()I",
        "UNKNOWN_UNIT_ABBREVIATION",
        "UNKNOWN_UNIT_NAME_ID",
        "getUNKNOWN_UNIT_NAME_ID",
        "applyQuantityPrecision",
        "Ljava/math/BigDecimal;",
        "quantityPrecision",
        "displayNameOnly",
        "",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "formatWithUnit",
        "unitAbbreviation",
        "isUnitPriced",
        "isWeight",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "measurementUnit",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "toQuantityEntrySuffix",
        "",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        "res",
        "Lcom/squareup/util/Res;",
        "toQuantityUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "toUnit",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ALLOWED_QUANTITY_CHARACTERS:Ljava/lang/String; = "0123456789,.\u00a0"

.field private static final ALLOWED_QUANTITY_PRECISION_RANGE:Lkotlin/ranges/IntRange;

.field public static final DEFAULT_QUANTITY_PRECISION:I = 0x0

.field public static final DEFAULT_QUANTITY_ROUNDING_MODE:Ljava/math/RoundingMode;

.field public static final DEFAULT_UNIT_ABBREVIATION:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_NAME:Ljava/lang/String; = ""

.field public static final GENERIC_UNIT_ABBREVIATION:Ljava/lang/String; = ""

.field private static final GENERIC_UNIT_NAME_ID:I

.field public static final UNKNOWN_UNIT_ABBREVIATION:Ljava/lang/String; = ""

.field private static final UNKNOWN_UNIT_NAME_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 36
    new-instance v0, Lkotlin/ranges/IntRange;

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lkotlin/ranges/IntRange;-><init>(II)V

    sput-object v0, Lcom/squareup/quantity/ItemQuantities;->ALLOWED_QUANTITY_PRECISION_RANGE:Lkotlin/ranges/IntRange;

    .line 41
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    sput-object v0, Lcom/squareup/quantity/ItemQuantities;->DEFAULT_QUANTITY_ROUNDING_MODE:Ljava/math/RoundingMode;

    .line 57
    sget v0, Lcom/squareup/proto_utilities/R$string;->generic_unit_name:I

    sput v0, Lcom/squareup/quantity/ItemQuantities;->GENERIC_UNIT_NAME_ID:I

    .line 64
    sget v0, Lcom/squareup/quantity/ItemQuantities;->GENERIC_UNIT_NAME_ID:I

    sput v0, Lcom/squareup/quantity/ItemQuantities;->UNKNOWN_UNIT_NAME_ID:I

    return-void
.end method

.method public static final applyQuantityPrecision(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$applyQuantityPrecision"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    sget-object v0, Lcom/squareup/quantity/ItemQuantities;->DEFAULT_QUANTITY_ROUNDING_MODE:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p0

    const-string p1, "setScale(quantityPrecisi\u2026T_QUANTITY_ROUNDING_MODE)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final displayNameOnly(Lcom/squareup/protos/client/bills/Itemization;)Z
    .locals 4

    const-string v0, "$this$displayNameOnly"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v0, "quantity"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double p0, v0, v2

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$formatWithUnit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitAbbreviation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object p0, p1

    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getALLOWED_QUANTITY_PRECISION_RANGE()Lkotlin/ranges/IntRange;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/quantity/ItemQuantities;->ALLOWED_QUANTITY_PRECISION_RANGE:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public static final getGENERIC_UNIT_NAME_ID()I
    .locals 1

    .line 57
    sget v0, Lcom/squareup/quantity/ItemQuantities;->GENERIC_UNIT_NAME_ID:I

    return v0
.end method

.method public static final getUNKNOWN_UNIT_NAME_ID()I
    .locals 1

    .line 64
    sget v0, Lcom/squareup/quantity/ItemQuantities;->UNKNOWN_UNIT_NAME_ID:I

    return v0
.end method

.method public static final isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z
    .locals 0

    .line 110
    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->measurementUnit(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isWeight(Lcom/squareup/orders/model/Order$QuantityUnit;)Z
    .locals 0

    if-eqz p0, :cond_0

    .line 154
    iget-object p0, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method public static final measurementUnit(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 0

    if-eqz p0, :cond_0

    .line 118
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final toQuantityEntrySuffix(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p0, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    sget-object v0, Lcom/squareup/quantity/ItemQuantities$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    :goto_0
    const-string p0, ""

    .line 147
    check-cast p0, Ljava/lang/CharSequence;

    goto :goto_1

    .line 146
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0xa0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v0, Lcom/squareup/proto_utilities/R$string;->quantity_entry_manual_with_connected_scale:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    :goto_1
    return-object p0
.end method

.method public static final toQuantityUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/orders/model/Order$QuantityUnit;
    .locals 3

    if-eqz p0, :cond_0

    .line 86
    new-instance v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;-><init>()V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->precision(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getVersion()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object p0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->build()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final toUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 1

    const-string v0, "$this$toUnit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    const-string v0, "MeasurementUnit.Builder(\u2026weight_unit(this).build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
