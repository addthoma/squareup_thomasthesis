.class public final Lcom/squareup/quantity/DecimalQuantity;
.super Ljava/lang/Object;
.source "DecimalQuantity.kt"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quantity/DecimalQuantity$JsonAdapter;,
        Lcom/squareup/quantity/DecimalQuantity$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/quantity/DecimalQuantity;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDecimalQuantity.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DecimalQuantity.kt\ncom/squareup/quantity/DecimalQuantity\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,190:1\n79#2:191\n68#2,5:192\n79#2:197\n68#2,5:198\n*E\n*S KotlinDebug\n*F\n+ 1 DecimalQuantity.kt\ncom/squareup/quantity/DecimalQuantity\n*L\n104#1:191\n104#1,5:192\n105#1:197\n105#1,5:198\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use BigDecimal and utility methods from BigDecimals instead"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 \u001a2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\u001a\u001bB\u0019\u0008\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0019\u0008\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007B\u0017\u0008\u0002\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\nJ\u0011\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0000H\u0096\u0002J\t\u0010\u0012\u001a\u00020\tH\u0086\u0002J\t\u0010\u0013\u001a\u00020\u0005H\u0086\u0002J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0016H\u0096\u0002J\u0008\u0010\u000b\u001a\u00020\u0005H\u0016J\u0008\u0010\u0017\u001a\u00020\u0005H\u0002J\u0008\u0010\u0018\u001a\u00020\u0003H\u0016J\u0006\u0010\u0019\u001a\u00020\u0003R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0008\u001a\u00020\t8\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/quantity/DecimalQuantity;",
        "",
        "quantity",
        "",
        "roundingScale",
        "",
        "(Ljava/lang/String;I)V",
        "(II)V",
        "value",
        "Ljava/math/BigDecimal;",
        "(Ljava/math/BigDecimal;I)V",
        "hashCode",
        "getRoundingScale",
        "()I",
        "asBigDecimal",
        "()Ljava/math/BigDecimal;",
        "compareTo",
        "other",
        "component1",
        "component2",
        "equals",
        "",
        "",
        "hashCodeIgnoreScaleAndRoundingScale",
        "toString",
        "valueString",
        "Companion",
        "JsonAdapter",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;


# instance fields
.field private final transient hashCode:I

.field private final roundingScale:I

.field private final value:Ljava/math/BigDecimal;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/quantity/DecimalQuantity$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/quantity/DecimalQuantity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/quantity/DecimalQuantity;->Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;

    return-void
.end method

.method private constructor <init>(II)V
    .locals 1

    .line 72
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/math/BigDecimal;I)V

    return-void
.end method

.method synthetic constructor <init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 71
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(II)V

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(II)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 67
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/math/BigDecimal;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 66
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Ljava/math/BigDecimal;I)V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    iput p2, p0, Lcom/squareup/quantity/DecimalQuantity;->roundingScale:I

    .line 59
    iget p1, p0, Lcom/squareup/quantity/DecimalQuantity;->roundingScale:I

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string p2, "Rounding scale must be non-negative."

    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 61
    invoke-direct {p0}, Lcom/squareup/quantity/DecimalQuantity;->hashCodeIgnoreScaleAndRoundingScale()I

    move-result p1

    iput p1, p0, Lcom/squareup/quantity/DecimalQuantity;->hashCode:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/math/BigDecimal;I)V

    return-void
.end method

.method private final hashCodeIgnoreScaleAndRoundingScale()I
    .locals 7

    .line 101
    iget-object v0, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "asString"

    .line 102
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    const/16 v2, 0x2e

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 192
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    const-string v4, ""

    if-ltz v0, :cond_1

    .line 193
    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 104
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    const/16 v6, 0x30

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 194
    invoke-interface {v1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_1
    move-object v0, v4

    check-cast v0, Ljava/lang/CharSequence;

    .line 191
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    check-cast v0, Ljava/lang/CharSequence;

    .line 198
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    :goto_1
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_3

    .line 199
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 105
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    add-int/lit8 v1, v1, 0x1

    .line 200
    invoke-interface {v0, v3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    :cond_2
    goto :goto_1

    .line 202
    :cond_3
    move-object v0, v4

    check-cast v0, Ljava/lang/CharSequence;

    .line 197
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0

    .line 108
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public static final of(I)Lcom/squareup/quantity/DecimalQuantity;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/DecimalQuantity;->Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of(I)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/lang/String;)Lcom/squareup/quantity/DecimalQuantity;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/DecimalQuantity;->Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of(Ljava/lang/String;)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/lang/String;I)Lcom/squareup/quantity/DecimalQuantity;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/DecimalQuantity;->Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of(Ljava/lang/String;I)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/math/BigDecimal;)Lcom/squareup/quantity/DecimalQuantity;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/DecimalQuantity;->Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p0, v1, v2, v3}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of$default(Lcom/squareup/quantity/DecimalQuantity$Companion;Ljava/math/BigDecimal;IILjava/lang/Object;)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/DecimalQuantity;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/DecimalQuantity;->Companion:Lcom/squareup/quantity/DecimalQuantity$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final asBigDecimal()Ljava/math/BigDecimal;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public compareTo(Lcom/squareup/quantity/DecimalQuantity;)I
    .locals 1

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    iget-object p1, p1, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/quantity/DecimalQuantity;

    invoke-virtual {p0, p1}, Lcom/squareup/quantity/DecimalQuantity;->compareTo(Lcom/squareup/quantity/DecimalQuantity;)I

    move-result p1

    return p1
.end method

.method public final component1()Ljava/math/BigDecimal;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    .line 76
    iget v0, p0, Lcom/squareup/quantity/DecimalQuantity;->roundingScale:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 87
    instance-of v0, p1, Lcom/squareup/quantity/DecimalQuantity;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    check-cast p1, Lcom/squareup/quantity/DecimalQuantity;

    iget-object p1, p1, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method public final getRoundingScale()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/squareup/quantity/DecimalQuantity;->roundingScale:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 93
    iget v0, p0, Lcom/squareup/quantity/DecimalQuantity;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DecimalQuantity(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/quantity/DecimalQuantity;->valueString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", roundingScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/quantity/DecimalQuantity;->roundingScale:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final valueString()Ljava/lang/String;
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/quantity/DecimalQuantity;->value:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "value.toPlainString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
