.class public final Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RepaymentTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final_payment_money:Lcom/squareup/protos/common/Money;

.field public payment_count:Ljava/lang/Long;

.field public payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

.field public payment_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 286
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
    .locals 7

    .line 311
    new-instance v6, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_count:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 277
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    move-result-object v0

    return-object v0
.end method

.method public final_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public payment_count(Ljava/lang/Long;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_count:Ljava/lang/Long;

    return-object p0
.end method

.method public payment_frequency(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    return-object p0
.end method

.method public payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
