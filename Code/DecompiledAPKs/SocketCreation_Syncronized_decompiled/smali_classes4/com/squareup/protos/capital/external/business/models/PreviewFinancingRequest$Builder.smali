.class public final Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreviewFinancingRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;",
        "Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public financing_request_id:Ljava/lang/String;

.field public status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;->financing_request_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    move-result-object v0

    return-object v0
.end method

.method public financing_request_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;->financing_request_id:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;)Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest$Builder;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0
.end method
