.class public final enum Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
.super Ljava/lang/Enum;
.source "Plan.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public static final enum ACTIVE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public static final enum ACTIVE_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public static final enum CLOSED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public static final enum S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 759
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/4 v1, 0x0

    const-string v2, "S_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 761
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/4 v2, 0x1

    const-string v3, "ACTIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ACTIVE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 763
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/4 v3, 0x2

    const-string v4, "ACTIVE_PAST_DUE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ACTIVE_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 765
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/4 v4, 0x3

    const-string v5, "CANCELED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 767
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/4 v5, 0x4

    const-string v6, "CLOSED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->CLOSED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 758
    sget-object v6, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ACTIVE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ACTIVE_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->CLOSED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 769
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 773
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 774
    iput p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 786
    :cond_0
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->CLOSED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0

    .line 785
    :cond_1
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->CANCELED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0

    .line 784
    :cond_2
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ACTIVE_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0

    .line 783
    :cond_3
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->ACTIVE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0

    .line 782
    :cond_4
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
    .locals 1

    .line 758
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
    .locals 1

    .line 758
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 793
    iget v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->value:I

    return v0
.end method
