.class public final Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;
.super Lcom/squareup/wire/Message;
.source "Plan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$ProtoAdapter_DefaultStatus;,
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;,
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;,
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field private static final serialVersionUID:J


# instance fields
.field public final reasons:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Plan$DefaultStatus$Reason#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Plan$DefaultStatus$Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 809
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$ProtoAdapter_DefaultStatus;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$ProtoAdapter_DefaultStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 813
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DS_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->DEFAULT_STATUS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
            ">;)V"
        }
    .end annotation

    .line 829
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 833
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 834
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const-string p1, "reasons"

    .line 835
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 850
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 851
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    .line 852
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 853
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    .line 854
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 859
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 861
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 862
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 863
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 864
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;
    .locals 2

    .line 840
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;-><init>()V

    .line 841
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 842
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->reasons:Ljava/util/List;

    .line 843
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 808
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 871
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 872
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 873
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", reasons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->reasons:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DefaultStatus{"

    .line 874
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
