.class public final Lcom/squareup/protos/capital/servicing/plan/models/Document;
.super Lcom/squareup/wire/Message;
.source "Document.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Document$ProtoAdapter_Document;,
        Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;,
        Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Document;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Document;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

.field public static final DEFAULT_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Document$DocumentType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$ProtoAdapter_Document;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Document$ProtoAdapter_Document;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->DT_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->DEFAULT_TYPE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;Ljava/lang/String;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/capital/servicing/plan/models/Document;-><init>(Ljava/lang/String;Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 58
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Document;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Document;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Document;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Document;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Document;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->created_at:Ljava/lang/String;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->url:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Document;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Document;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Document{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
