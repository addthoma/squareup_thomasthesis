.class public final Lcom/squareup/protos/queuebert/model/Payment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/queuebert/model/Payment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/queuebert/model/Payment;",
        "Lcom/squareup/protos/queuebert/model/Payment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

.field public bletchley_key_id:Ljava/lang/String;

.field public card_brand:Ljava/lang/String;

.field public card_last_four:Ljava/lang/String;

.field public client_timestamp:Ljava/lang/String;

.field public encrypted_payload:Lokio/ByteString;

.field public merchant_token:Ljava/lang/String;

.field public total_money:Lcom/squareup/protos/common/Money;

.field public unique_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 254
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public auth_code(Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 339
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    return-object p0
.end method

.method public bletchley_key_id(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->bletchley_key_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/queuebert/model/Payment;
    .locals 12

    .line 345
    new-instance v11, Lcom/squareup/protos/queuebert/model/Payment;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->bletchley_key_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->encrypted_payload:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->client_timestamp:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->card_brand:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->card_last_four:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->merchant_token:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/queuebert/model/Payment;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 235
    invoke-virtual {p0}, Lcom/squareup/protos/queuebert/model/Payment$Builder;->build()Lcom/squareup/protos/queuebert/model/Payment;

    move-result-object v0

    return-object v0
.end method

.method public card_brand(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->card_brand:Ljava/lang/String;

    return-object p0
.end method

.method public card_last_four(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->card_last_four:Ljava/lang/String;

    return-object p0
.end method

.method public client_timestamp(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->client_timestamp:Ljava/lang/String;

    return-object p0
.end method

.method public encrypted_payload(Lokio/ByteString;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->encrypted_payload:Lokio/ByteString;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public unique_key(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/Payment$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/Payment$Builder;->unique_key:Ljava/lang/String;

    return-object p0
.end method
