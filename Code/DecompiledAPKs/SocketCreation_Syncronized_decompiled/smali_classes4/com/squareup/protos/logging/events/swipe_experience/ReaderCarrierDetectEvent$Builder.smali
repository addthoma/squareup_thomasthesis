.class public final Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderCarrierDetectEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

.field public event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object v0

    return-object v0
.end method

.method public carrier_detect_info(Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    return-object p0
.end method

.method public event(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0
.end method

.method public signal_found(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    return-object p0
.end method
