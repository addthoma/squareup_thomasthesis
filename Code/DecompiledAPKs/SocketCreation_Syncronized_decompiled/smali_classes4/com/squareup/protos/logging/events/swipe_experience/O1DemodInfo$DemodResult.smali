.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;
.super Ljava/lang/Enum;
.source "O1DemodInfo.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DemodResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult$ProtoAdapter_DemodResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final enum FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final enum FAIL_DECODE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final enum FAIL_INVALID_COUNTER:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final enum FAIL_LENGTH_MISMATCH:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final enum FAIL_LRC:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field public static final enum SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 103
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 109
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v2, 0x1

    const-string v3, "FAILURE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 115
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v3, 0x2

    const-string v4, "FAIL_BAD_STREAM"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 121
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v4, 0x3

    const-string v5, "FAIL_DECODE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_DECODE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 126
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v5, 0x4

    const-string v6, "FAIL_INVALID_COUNTER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_INVALID_COUNTER:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 133
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v6, 0x5

    const-string v7, "FAIL_LENGTH_MISMATCH"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LENGTH_MISMATCH:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 139
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v7, 0x6

    const-string v8, "FAIL_LRC"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LRC:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 98
    sget-object v8, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_DECODE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_INVALID_COUNTER:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LENGTH_MISMATCH:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LRC:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 141
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult$ProtoAdapter_DemodResult;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult$ProtoAdapter_DemodResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 146
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 160
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LRC:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    .line 159
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LENGTH_MISMATCH:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    .line 158
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_INVALID_COUNTER:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    .line 157
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_DECODE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    .line 156
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    .line 155
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    .line 154
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;
    .locals 1

    .line 98
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;
    .locals 1

    .line 98
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 167
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->value:I

    return v0
.end method
