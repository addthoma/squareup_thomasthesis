.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;
.super Ljava/lang/Enum;
.source "SignalFound.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IssuerId"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId$ProtoAdapter_IssuerId;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum CHINA_UNIONPAY:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum DINERS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum DISCOVER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum JCB:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum MASTERCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum OTHER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum SQUARE_GIFTCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final enum VISA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 803
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v1, 0x0

    const-string v2, "VISA"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->VISA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 805
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v2, 0x1

    const-string v3, "MASTERCARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->MASTERCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 807
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v3, 0x2

    const-string v4, "DISCOVER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DISCOVER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 809
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v4, 0x3

    const-string v5, "AMERICAN_EXPRESS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->AMERICAN_EXPRESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 811
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v5, 0x4

    const-string v6, "JCB"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->JCB:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 813
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v6, 0x5

    const-string v7, "OTHER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->OTHER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 815
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v7, 0x6

    const-string v8, "DINERS"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DINERS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 817
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/4 v8, 0x7

    const-string v9, "CHINA_UNIONPAY"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->CHINA_UNIONPAY:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 819
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/16 v9, 0x8

    const-string v10, "SQUARE_GIFTCARD"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->SQUARE_GIFTCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 802
    sget-object v10, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->VISA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->MASTERCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DISCOVER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->AMERICAN_EXPRESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->JCB:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->OTHER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DINERS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->CHINA_UNIONPAY:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->SQUARE_GIFTCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 821
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId$ProtoAdapter_IssuerId;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId$ProtoAdapter_IssuerId;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 825
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 826
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 842
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->SQUARE_GIFTCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 841
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->CHINA_UNIONPAY:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 840
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DINERS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 839
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->OTHER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 838
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->JCB:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 837
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->AMERICAN_EXPRESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 836
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DISCOVER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 835
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->MASTERCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 834
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->VISA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;
    .locals 1

    .line 802
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;
    .locals 1

    .line 802
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 849
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->value:I

    return v0
.end method
