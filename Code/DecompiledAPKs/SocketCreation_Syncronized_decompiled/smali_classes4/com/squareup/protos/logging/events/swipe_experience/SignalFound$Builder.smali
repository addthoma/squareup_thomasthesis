.class public final Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SignalFound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public demod_info:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;",
            ">;"
        }
    .end annotation
.end field

.field public deprecated_number_of_sample:Ljava/lang/Integer;

.field public expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

.field public packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

.field public reader_hardware_id:Ljava/lang/String;

.field public reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public sample_rate:Ljava/lang/Integer;

.field public signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

.field public swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

.field public track1_read_success:Ljava/lang/Boolean;

.field public track2_read_success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 391
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 392
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
    .locals 2

    .line 557
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 356
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    move-result-object v0

    return-object v0
.end method

.method public classified_link_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0
.end method

.method public decision(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 480
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0
.end method

.method public demod_info(Ljava/util/List;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;",
            ">;)",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;"
        }
    .end annotation

    .line 542
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 543
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    return-object p0
.end method

.method public deprecated_number_of_sample(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->deprecated_number_of_sample:Ljava/lang/Integer;

    return-object p0
.end method

.method public expected_reader_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 433
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0
.end method

.method public issuer_id(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 489
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0
.end method

.method public link_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0
.end method

.method public o1_packet(Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 524
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    return-object p0
.end method

.method public packet_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0
.end method

.method public r4_packet(Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 532
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    return-object p0
.end method

.method public reader_hardware_id(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 472
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0
.end method

.method public sample_rate(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 506
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate:Ljava/lang/Integer;

    return-object p0
.end method

.method public signal_start_to_decision_duration_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 462
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    return-object p0
.end method

.method public swipe_direction(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 497
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0
.end method

.method public track1_read_success(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 442
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track1_read_success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public track2_read_success(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 451
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    return-object p0
.end method
