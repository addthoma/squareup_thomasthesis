.class final Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;
.super Lcom/squareup/wire/ProtoAdapter;
.source "R4CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_R4TrackDecodeResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 694
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 725
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;-><init>()V

    .line 726
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 727
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 738
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 736
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->lrc_failure(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 735
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->parity_error(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 734
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_end_sentinel(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 733
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->missing_start_sentinel(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 732
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_short(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 731
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->card_data_too_long(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 730
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->no_zeros_detected(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 729
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->track_not_present(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    goto :goto_0

    .line 742
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 743
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 692
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 712
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 713
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 714
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 715
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 716
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 717
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 718
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 719
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 720
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 692
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)I
    .locals 4

    .line 699
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->track_not_present:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->no_zeros_detected:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 700
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_long:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 701
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->card_data_too_short:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 702
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_start_sentinel:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 703
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->missing_end_sentinel:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 704
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->parity_error:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 705
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->lrc_failure:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 706
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 692
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
    .locals 0

    .line 748
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;

    move-result-object p1

    .line 749
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 750
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 692
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult$ProtoAdapter_R4TrackDecodeResult;->redact(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;)Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    move-result-object p1

    return-object p1
.end method
