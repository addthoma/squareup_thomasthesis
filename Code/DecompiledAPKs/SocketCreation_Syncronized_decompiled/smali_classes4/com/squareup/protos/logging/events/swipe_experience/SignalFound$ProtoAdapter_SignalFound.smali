.class final Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SignalFound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SignalFound"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 910
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 959
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;-><init>()V

    .line 960
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 961
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1037
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1030
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1032
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1027
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1026
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet(Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto :goto_0

    .line 1025
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet(Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto :goto_0

    .line 1024
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->deprecated_number_of_sample(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto :goto_0

    .line 1023
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto :goto_0

    .line 1017
    :pswitch_7
    :try_start_1
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 1019
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1009
    :pswitch_8
    :try_start_2
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 1011
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1001
    :pswitch_9
    :try_start_3
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->decision(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    .line 1003
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 998
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto/16 :goto_0

    .line 997
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->signal_start_to_decision_duration_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto/16 :goto_0

    .line 996
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto/16 :goto_0

    .line 995
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track1_read_success(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    goto/16 :goto_0

    .line 989
    :pswitch_e
    :try_start_4
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->expected_reader_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_4
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v4

    .line 991
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 981
    :pswitch_f
    :try_start_5
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_5
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v4

    .line 983
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 973
    :pswitch_10
    :try_start_6
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_6
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v4

    .line 975
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 965
    :pswitch_11
    :try_start_7
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    :try_end_7
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v4

    .line 967
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1041
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1042
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 908
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 937
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 938
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 939
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 940
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 941
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 942
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 943
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 944
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 945
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 946
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 947
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 948
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 949
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 950
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 951
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 952
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 953
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 954
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 908
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)I
    .locals 4

    .line 915
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v3, 0x2

    .line 916
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v3, 0x3

    .line 917
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v3, 0x4

    .line 918
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 919
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 920
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 921
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    const/16 v3, 0x8

    .line 922
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/16 v3, 0x9

    .line 923
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    const/16 v3, 0xa

    .line 924
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    const/16 v3, 0xb

    .line 925
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    const/16 v3, 0xc

    .line 926
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    const/16 v3, 0xd

    .line 927
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    const/16 v3, 0xf

    .line 928
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    const/16 v3, 0x10

    .line 929
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 930
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    const/16 v3, 0x11

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/16 v3, 0x12

    .line 931
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 932
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 908
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
    .locals 2

    .line 1047
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    move-result-object p1

    .line 1048
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    .line 1049
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    .line 1050
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1051
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1052
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 908
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;->redact(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    move-result-object p1

    return-object p1
.end method
