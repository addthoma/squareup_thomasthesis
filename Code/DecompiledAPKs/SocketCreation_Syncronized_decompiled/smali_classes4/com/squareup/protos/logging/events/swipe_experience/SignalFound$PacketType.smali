.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;
.super Ljava/lang/Enum;
.source "SignalFound.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PacketType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType$ProtoAdapter_PacketType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GEN2_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum O1_FLASH_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum O1_GENERAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum O1_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum R4_AWAKE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum R4_BLANK:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum R4_CARD_DATA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final enum R4_DEAD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 618
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v1, 0x0

    const-string v2, "GEN2_SUCCESSFUL_SWIPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->GEN2_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 620
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v2, 0x1

    const-string v3, "O1_SUCCESSFUL_SWIPE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 622
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v3, 0x2

    const-string v4, "O1_GENERAL_ERROR"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_GENERAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 624
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v4, 0x3

    const-string v5, "O1_FLASH_ERROR"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_FLASH_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 626
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v5, 0x4

    const-string v6, "R4_CARD_DATA"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_CARD_DATA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 628
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v6, 0x5

    const-string v7, "R4_AWAKE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_AWAKE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 630
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v7, 0x6

    const-string v8, "R4_DEAD"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_DEAD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 632
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v8, 0x7

    const-string v9, "R4_BLANK"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_BLANK:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 617
    sget-object v9, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->GEN2_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_GENERAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_FLASH_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_CARD_DATA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_AWAKE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_DEAD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_BLANK:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 634
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType$ProtoAdapter_PacketType;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType$ProtoAdapter_PacketType;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 638
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 639
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 654
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_BLANK:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 653
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_DEAD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 652
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_AWAKE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 651
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_CARD_DATA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 650
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_FLASH_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 649
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_GENERAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 648
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    .line 647
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->GEN2_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;
    .locals 1

    .line 617
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;
    .locals 1

    .line 617
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 661
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->value:I

    return v0
.end method
