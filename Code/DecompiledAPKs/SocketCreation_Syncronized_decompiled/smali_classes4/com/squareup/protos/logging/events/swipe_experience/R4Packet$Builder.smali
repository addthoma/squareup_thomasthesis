.class public final Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "R4Packet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public hardware_major_revision:Ljava/lang/Integer;

.field public hardware_minor_revision:Ljava/lang/Integer;

.field public r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 132
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;
    .locals 5

    .line 174
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_major_revision:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_minor_revision:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    move-result-object v0

    return-object v0
.end method

.method public hardware_major_revision(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_major_revision:Ljava/lang/Integer;

    return-object p0
.end method

.method public hardware_minor_revision(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_minor_revision:Ljava/lang/Integer;

    return-object p0
.end method

.method public r4_card_data(Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    return-object p0
.end method
