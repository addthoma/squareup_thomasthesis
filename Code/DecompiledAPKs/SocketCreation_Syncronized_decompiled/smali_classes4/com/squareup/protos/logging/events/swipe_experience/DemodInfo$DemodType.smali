.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;
.super Ljava/lang/Enum;
.source "DemodInfo.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DemodType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType$ProtoAdapter_DemodType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GEN2:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

.field public static final enum O1:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

.field public static final enum R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

.field public static final enum R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 211
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v1, 0x0

    const-string v2, "GEN2"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 213
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v2, 0x1

    const-string v3, "O1"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 215
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v3, 0x2

    const-string v4, "R4_FAST"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 217
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v4, 0x3

    const-string v5, "R4_SLOW"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 210
    sget-object v5, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 219
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType$ProtoAdapter_DemodType;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType$ProtoAdapter_DemodType;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 224
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 235
    :cond_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object p0

    .line 234
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object p0

    .line 233
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object p0

    .line 232
    :cond_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;
    .locals 1

    .line 210
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;
    .locals 1

    .line 210
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 242
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->value:I

    return v0
.end method
