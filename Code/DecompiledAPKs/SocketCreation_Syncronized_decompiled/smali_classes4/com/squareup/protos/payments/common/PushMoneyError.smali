.class public final enum Lcom/squareup/protos/payments/common/PushMoneyError;
.super Ljava/lang/Enum;
.source "PushMoneyError.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/payments/common/PushMoneyError$ProtoAdapter_PushMoneyError;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/payments/common/PushMoneyError;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/payments/common/PushMoneyError;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AMOUNT_TOO_HIGH:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum BLACKLISTED_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum CAPTURE_CARD:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum CARD_EXPIRED:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum COMPLIANCE_FAILURE:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum COULD_NOT_LOCATE_PAYMENT_FOR_REPLAY:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum CURRENCY_MISMATCH:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum DECLINED_REFER_TO_ISSUER:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum DEFAULT_PUSH_MONEY_ERROR_ENUM_DO_NOT_USE:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum EXCEEDS_APPROVAL_AMOUNT_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum GATEWAY_TIMEOUT:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum INCORRECT_EXPIRATION:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum INSUFFICIENT_FUNDS:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum INVALID_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum ISSUER_UNAVAILABLE:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum NOT_FULFILL_AML_REQUIREMENT:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum NO_ROUTE_FOUND:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final enum OTHER:Lcom/squareup/protos/payments/common/PushMoneyError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 18
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_PUSH_MONEY_ERROR_ENUM_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->DEFAULT_PUSH_MONEY_ERROR_ENUM_DO_NOT_USE:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 20
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v2, 0x1

    const-string v3, "OTHER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->OTHER:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 22
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v3, 0x2

    const-string v4, "INVALID_ACCOUNT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->INVALID_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 27
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v4, 0x3

    const-string v5, "CARD_EXPIRED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->CARD_EXPIRED:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 34
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v5, 0x4

    const-string v6, "CURRENCY_MISMATCH"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->CURRENCY_MISMATCH:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 36
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v6, 0x5

    const-string v7, "BLACKLISTED_ACCOUNT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->BLACKLISTED_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 41
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v7, 0x6

    const-string v8, "INCORRECT_EXPIRATION"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->INCORRECT_EXPIRATION:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 46
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v8, 0x7

    const-string v9, "GATEWAY_TIMEOUT"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->GATEWAY_TIMEOUT:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 48
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v9, 0x8

    const-string v10, "EXCEEDS_APPROVAL_AMOUNT_LIMIT"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->EXCEEDS_APPROVAL_AMOUNT_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 50
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v10, 0x9

    const-string v11, "NOT_FULFILL_AML_REQUIREMENT"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->NOT_FULFILL_AML_REQUIREMENT:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 52
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v11, 0xa

    const-string v12, "EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 54
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v12, 0xb

    const-string v13, "ISSUER_UNAVAILABLE"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->ISSUER_UNAVAILABLE:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 56
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v13, 0xc

    const-string v14, "INSUFFICIENT_FUNDS"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 58
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v14, 0xd

    const-string v15, "CAPTURE_CARD"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->CAPTURE_CARD:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 60
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v15, 0xe

    const-string v14, "DECLINED_REFER_TO_ISSUER"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->DECLINED_REFER_TO_ISSUER:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 62
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const-string v14, "COULD_NOT_LOCATE_PAYMENT_FOR_REPLAY"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->COULD_NOT_LOCATE_PAYMENT_FOR_REPLAY:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 68
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const-string v13, "NO_ROUTE_FOUND"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->NO_ROUTE_FOUND:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 77
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const-string v13, "COMPLIANCE_FAILURE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->COMPLIANCE_FAILURE:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 83
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    const-string v13, "AMOUNT_TOO_HIGH"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/payments/common/PushMoneyError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->AMOUNT_TOO_HIGH:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 13
    sget-object v13, Lcom/squareup/protos/payments/common/PushMoneyError;->DEFAULT_PUSH_MONEY_ERROR_ENUM_DO_NOT_USE:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->OTHER:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->INVALID_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->CARD_EXPIRED:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->CURRENCY_MISMATCH:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->BLACKLISTED_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->INCORRECT_EXPIRATION:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->GATEWAY_TIMEOUT:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->EXCEEDS_APPROVAL_AMOUNT_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->NOT_FULFILL_AML_REQUIREMENT:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->ISSUER_UNAVAILABLE:Lcom/squareup/protos/payments/common/PushMoneyError;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->CAPTURE_CARD:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->DECLINED_REFER_TO_ISSUER:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->COULD_NOT_LOCATE_PAYMENT_FOR_REPLAY:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->NO_ROUTE_FOUND:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->COMPLIANCE_FAILURE:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/payments/common/PushMoneyError;->AMOUNT_TOO_HIGH:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->$VALUES:[Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 85
    new-instance v0, Lcom/squareup/protos/payments/common/PushMoneyError$ProtoAdapter_PushMoneyError;

    invoke-direct {v0}, Lcom/squareup/protos/payments/common/PushMoneyError$ProtoAdapter_PushMoneyError;-><init>()V

    sput-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 90
    iput p3, p0, Lcom/squareup/protos/payments/common/PushMoneyError;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/payments/common/PushMoneyError;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 116
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->AMOUNT_TOO_HIGH:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 115
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->COMPLIANCE_FAILURE:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 114
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->NO_ROUTE_FOUND:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 113
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->COULD_NOT_LOCATE_PAYMENT_FOR_REPLAY:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 112
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->DECLINED_REFER_TO_ISSUER:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 111
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->CAPTURE_CARD:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 110
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 109
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->ISSUER_UNAVAILABLE:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 108
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 107
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->NOT_FULFILL_AML_REQUIREMENT:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 106
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->EXCEEDS_APPROVAL_AMOUNT_LIMIT:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 105
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->GATEWAY_TIMEOUT:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 104
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->INCORRECT_EXPIRATION:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 103
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->BLACKLISTED_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 102
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->CURRENCY_MISMATCH:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 101
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->CARD_EXPIRED:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 100
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->INVALID_ACCOUNT:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 99
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->OTHER:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    .line 98
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/payments/common/PushMoneyError;->DEFAULT_PUSH_MONEY_ERROR_ENUM_DO_NOT_USE:Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/payments/common/PushMoneyError;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/payments/common/PushMoneyError;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->$VALUES:[Lcom/squareup/protos/payments/common/PushMoneyError;

    invoke-virtual {v0}, [Lcom/squareup/protos/payments/common/PushMoneyError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/payments/common/PushMoneyError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/squareup/protos/payments/common/PushMoneyError;->value:I

    return v0
.end method
