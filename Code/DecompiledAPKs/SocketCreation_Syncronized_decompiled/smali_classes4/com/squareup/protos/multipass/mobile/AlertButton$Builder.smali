.class public final Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AlertButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/mobile/AlertButton;",
        "Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

.field public dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

.field public open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 136
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public begin_flow(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    const/4 p1, 0x0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/multipass/mobile/AlertButton;
    .locals 7

    .line 179
    new-instance v6, Lcom/squareup/protos/multipass/mobile/AlertButton;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    iget-object v3, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iget-object v4, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/multipass/mobile/AlertButton;-><init>(Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButton;

    move-result-object v0

    return-object v0
.end method

.method public dismiss(Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    const/4 p1, 0x0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    return-object p0
.end method

.method public open_url(Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    const/4 p1, 0x0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
