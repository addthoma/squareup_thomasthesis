.class public final Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
.super Lcom/squareup/wire/Message;
.source "AlertButtonActionBeginFlow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;,
        Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;,
        Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FLOW:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

.field private static final serialVersionUID:J


# instance fields
.field public final flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.AlertButtonActionBeginFlow$Flow#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->NONE:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->DEFAULT_FLOW:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;)V
    .locals 1

    .line 41
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;-><init>(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;Lokio/ByteString;)V
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 60
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 61
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    iget-object p1, p1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    .line 63
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 68
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 72
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;
    .locals 2

    .line 51
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;-><init>()V

    .line 52
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->newBuilder()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    if-eqz v1, :cond_0

    const-string v1, ", flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AlertButtonActionBeginFlow{"

    .line 81
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
