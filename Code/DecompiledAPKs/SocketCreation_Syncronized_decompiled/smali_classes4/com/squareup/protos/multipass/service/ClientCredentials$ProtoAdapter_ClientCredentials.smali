.class final Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientCredentials.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/ClientCredentials;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ClientCredentials"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/service/ClientCredentials;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 220
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/service/ClientCredentials;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/ClientCredentials;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;-><init>()V

    .line 248
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 249
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 258
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 256
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    goto :goto_0

    .line 255
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/multipass/service/SessionHeaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers(Lcom/squareup/protos/multipass/service/SessionHeaders;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    goto :goto_0

    .line 254
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie(Lcom/squareup/protos/multipass/service/ClientSessionCookie;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    goto :goto_0

    .line 253
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token(Lcom/squareup/protos/multipass/service/ClientWebSessionToken;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    goto :goto_0

    .line 252
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/multipass/service/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details(Lcom/squareup/protos/multipass/service/DeviceDetails;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    goto :goto_0

    .line 251
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/multipass/service/ClientSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id(Lcom/squareup/protos/multipass/service/ClientSessionToken;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    goto :goto_0

    .line 262
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 263
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->build()Lcom/squareup/protos/multipass/service/ClientCredentials;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/ClientCredentials;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/ClientCredentials;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 236
    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 237
    sget-object v0, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 238
    sget-object v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 239
    sget-object v0, Lcom/squareup/protos/multipass/service/ClientSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 240
    sget-object v0, Lcom/squareup/protos/multipass/service/SessionHeaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 241
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 242
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/service/ClientCredentials;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    check-cast p2, Lcom/squareup/protos/multipass/service/ClientCredentials;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/ClientCredentials;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/service/ClientCredentials;)I
    .locals 4

    .line 225
    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    const/4 v3, 0x3

    .line 226
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    const/4 v3, 0x4

    .line 227
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/service/ClientSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    const/4 v3, 0x1

    .line 228
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/service/SessionHeaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    const/4 v3, 0x5

    .line 229
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    const/4 v3, 0x6

    .line 230
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientCredentials;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 218
    check-cast p1, Lcom/squareup/protos/multipass/service/ClientCredentials;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;->encodedSize(Lcom/squareup/protos/multipass/service/ClientCredentials;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/service/ClientCredentials;)Lcom/squareup/protos/multipass/service/ClientCredentials;
    .locals 2

    .line 268
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientCredentials;->newBuilder()Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    move-result-object p1

    .line 269
    iget-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/service/DeviceDetails;

    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    .line 270
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    .line 271
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 272
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/multipass/service/ClientSessionToken;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/service/ClientSessionToken;

    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 273
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/multipass/service/SessionHeaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/service/SessionHeaders;

    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    :cond_4
    const/4 v0, 0x0

    .line 274
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    .line 275
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 276
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->build()Lcom/squareup/protos/multipass/service/ClientCredentials;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 218
    check-cast p1, Lcom/squareup/protos/multipass/service/ClientCredentials;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;->redact(Lcom/squareup/protos/multipass/service/ClientCredentials;)Lcom/squareup/protos/multipass/service/ClientCredentials;

    move-result-object p1

    return-object p1
.end method
