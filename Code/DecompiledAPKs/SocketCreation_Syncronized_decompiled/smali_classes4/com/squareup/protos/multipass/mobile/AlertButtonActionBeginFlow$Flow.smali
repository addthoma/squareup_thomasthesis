.class public final enum Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;
.super Ljava/lang/Enum;
.source "AlertButtonActionBeginFlow.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Flow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow$ProtoAdapter_Flow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum NONE:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

.field public static final enum RESET_PASSWORD:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->NONE:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    .line 117
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    const/4 v2, 0x1

    const-string v3, "RESET_PASSWORD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->RESET_PASSWORD:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    .line 107
    sget-object v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->NONE:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->RESET_PASSWORD:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->$VALUES:[Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    .line 119
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow$ProtoAdapter_Flow;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow$ProtoAdapter_Flow;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 124
    iput p3, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 133
    :cond_0
    sget-object p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->RESET_PASSWORD:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-object p0

    .line 132
    :cond_1
    sget-object p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->NONE:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;
    .locals 1

    .line 107
    const-class v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->$VALUES:[Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    invoke-virtual {v0}, [Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 140
    iget v0, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->value:I

    return v0
.end method
