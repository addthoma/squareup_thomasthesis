.class public final Lcom/squareup/protos/eventstream/v1/Event;
.super Lcom/squareup/wire/Message;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/Event$ProtoAdapter_Event;,
        Lcom/squareup/protos/eventstream/v1/Event$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        "Lcom/squareup/protos/eventstream/v1/Event$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_EVENT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final data:Lcom/squareup/protos/eventstream/v1/Data;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Data#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.EventMetadata#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final event_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final event_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final experiment:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Experiment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Experiment;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final session:Lcom/squareup/protos/eventstream/v1/Session;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Session#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final source:Lcom/squareup/protos/eventstream/v1/Source;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Source#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final subject:Lcom/squareup/protos/eventstream/v1/Subject;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Subject#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Event$ProtoAdapter_Event;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Event$ProtoAdapter_Event;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/eventstream/v1/EventMetadata;Lcom/squareup/protos/eventstream/v1/Source;Lcom/squareup/protos/eventstream/v1/Session;Ljava/util/List;Lcom/squareup/protos/eventstream/v1/Subject;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Data;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/eventstream/v1/EventMetadata;",
            "Lcom/squareup/protos/eventstream/v1/Source;",
            "Lcom/squareup/protos/eventstream/v1/Session;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Experiment;",
            ">;",
            "Lcom/squareup/protos/eventstream/v1/Subject;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/eventstream/v1/Data;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 89
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/eventstream/v1/Event;-><init>(Lcom/squareup/protos/eventstream/v1/EventMetadata;Lcom/squareup/protos/eventstream/v1/Source;Lcom/squareup/protos/eventstream/v1/Session;Ljava/util/List;Lcom/squareup/protos/eventstream/v1/Subject;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Data;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/eventstream/v1/EventMetadata;Lcom/squareup/protos/eventstream/v1/Source;Lcom/squareup/protos/eventstream/v1/Session;Ljava/util/List;Lcom/squareup/protos/eventstream/v1/Subject;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Data;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/eventstream/v1/EventMetadata;",
            "Lcom/squareup/protos/eventstream/v1/Source;",
            "Lcom/squareup/protos/eventstream/v1/Session;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Experiment;",
            ">;",
            "Lcom/squareup/protos/eventstream/v1/Subject;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/eventstream/v1/Data;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 95
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    .line 97
    iput-object p2, p0, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    .line 98
    iput-object p3, p0, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    const-string p1, "experiment"

    .line 99
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    .line 100
    iput-object p5, p0, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    .line 101
    iput-object p6, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    .line 102
    iput-object p7, p0, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    .line 103
    iput-object p8, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 124
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/Event;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 125
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Event;

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Event;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    .line 130
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    .line 134
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 139
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Event;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Source;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Session;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Subject;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Data;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 150
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 2

    .line 108
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Event$Builder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->source:Lcom/squareup/protos/eventstream/v1/Source;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->session:Lcom/squareup/protos/eventstream/v1/Session;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->experiment:Ljava/util/List;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_name:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->data:Lcom/squareup/protos/eventstream/v1/Data;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_value:Ljava/lang/String;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Event;->newBuilder()Lcom/squareup/protos/eventstream/v1/Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    if-eqz v1, :cond_0

    const-string v1, ", event_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    if-eqz v1, :cond_1

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->source:Lcom/squareup/protos/eventstream/v1/Source;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    if-eqz v1, :cond_2

    const-string v1, ", session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->session:Lcom/squareup/protos/eventstream/v1/Session;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", experiment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->experiment:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    if-eqz v1, :cond_4

    const-string v1, ", subject="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", event_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    if-eqz v1, :cond_6

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->data:Lcom/squareup/protos/eventstream/v1/Data;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", event_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event;->event_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Event{"

    .line 166
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
