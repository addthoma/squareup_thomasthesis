.class public final Lcom/squareup/protos/eventstream/v1/Event$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        "Lcom/squareup/protos/eventstream/v1/Event$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data:Lcom/squareup/protos/eventstream/v1/Data;

.field public event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

.field public event_name:Ljava/lang/String;

.field public event_value:Ljava/lang/String;

.field public experiment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Experiment;",
            ">;"
        }
    .end annotation
.end field

.field public session:Lcom/squareup/protos/eventstream/v1/Session;

.field public source:Lcom/squareup/protos/eventstream/v1/Source;

.field public subject:Lcom/squareup/protos/eventstream/v1/Subject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 186
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 187
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->experiment:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/eventstream/v1/Event;
    .locals 11

    .line 240
    new-instance v10, Lcom/squareup/protos/eventstream/v1/Event;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->source:Lcom/squareup/protos/eventstream/v1/Source;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->session:Lcom/squareup/protos/eventstream/v1/Session;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->experiment:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    iget-object v6, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_name:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->data:Lcom/squareup/protos/eventstream/v1/Data;

    iget-object v8, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/eventstream/v1/Event;-><init>(Lcom/squareup/protos/eventstream/v1/EventMetadata;Lcom/squareup/protos/eventstream/v1/Source;Lcom/squareup/protos/eventstream/v1/Session;Ljava/util/List;Lcom/squareup/protos/eventstream/v1/Subject;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Data;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->build()Lcom/squareup/protos/eventstream/v1/Event;

    move-result-object v0

    return-object v0
.end method

.method public data(Lcom/squareup/protos/eventstream/v1/Data;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->data:Lcom/squareup/protos/eventstream/v1/Data;

    return-object p0
.end method

.method public event_metadata(Lcom/squareup/protos/eventstream/v1/EventMetadata;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_metadata:Lcom/squareup/protos/eventstream/v1/EventMetadata;

    return-object p0
.end method

.method public event_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_name:Ljava/lang/String;

    return-object p0
.end method

.method public event_value(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_value:Ljava/lang/String;

    return-object p0
.end method

.method public experiment(Ljava/util/List;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Experiment;",
            ">;)",
            "Lcom/squareup/protos/eventstream/v1/Event$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 210
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->experiment:Ljava/util/List;

    return-object p0
.end method

.method public session(Lcom/squareup/protos/eventstream/v1/Session;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->session:Lcom/squareup/protos/eventstream/v1/Session;

    return-object p0
.end method

.method public source(Lcom/squareup/protos/eventstream/v1/Source;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->source:Lcom/squareup/protos/eventstream/v1/Source;

    return-object p0
.end method

.method public subject(Lcom/squareup/protos/eventstream/v1/Subject;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Event$Builder;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    return-object p0
.end method
