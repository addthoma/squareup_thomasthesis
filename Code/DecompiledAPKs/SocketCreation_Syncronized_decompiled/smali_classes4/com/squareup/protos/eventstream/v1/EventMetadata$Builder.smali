.class public final Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EventMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/EventMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/EventMetadata;",
        "Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

.field public collector:Ljava/lang/String;

.field public library_name:Ljava/lang/String;

.field public library_version:Ljava/lang/String;

.field public recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

.field public uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 187
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/eventstream/v1/EventMetadata;
    .locals 9

    .line 252
    new-instance v8, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->uuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collector:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_version:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/eventstream/v1/EventMetadata;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 174
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->build()Lcom/squareup/protos/eventstream/v1/EventMetadata;

    move-result-object v0

    return-object v0
.end method

.method public collected_timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public collector(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collector:Ljava/lang/String;

    return-object p0
.end method

.method public library_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_name:Ljava/lang/String;

    return-object p0
.end method

.method public library_version(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_version:Ljava/lang/String;

    return-object p0
.end method

.method public recorded_timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public uuid(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->uuid:Ljava/lang/String;

    return-object p0
.end method
