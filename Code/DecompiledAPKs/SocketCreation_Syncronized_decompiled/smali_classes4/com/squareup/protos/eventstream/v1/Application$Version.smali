.class public final Lcom/squareup/protos/eventstream/v1/Application$Version;
.super Lcom/squareup/wire/Message;
.source "Application.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Application;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Version"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/Application$Version$ProtoAdapter_Version;,
        Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/Application$Version;",
        "Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/Application$Version;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUILD_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MAJOR:Ljava/lang/Integer;

.field public static final DEFAULT_MINOR:Ljava/lang/Integer;

.field public static final DEFAULT_PRERELEASE:Ljava/lang/Integer;

.field public static final DEFAULT_PRERELEASE_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_REVISION:Ljava/lang/Integer;

.field public static final DEFAULT_VERSION_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final build_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final major:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final minor:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final prerelease:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final prerelease_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final revision:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final version_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final version_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 213
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Application$Version$ProtoAdapter_Version;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Application$Version$ProtoAdapter_Version;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Application$Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 217
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Application$Version;->DEFAULT_MAJOR:Ljava/lang/Integer;

    .line 219
    sput-object v0, Lcom/squareup/protos/eventstream/v1/Application$Version;->DEFAULT_MINOR:Ljava/lang/Integer;

    .line 221
    sput-object v0, Lcom/squareup/protos/eventstream/v1/Application$Version;->DEFAULT_REVISION:Ljava/lang/Integer;

    .line 225
    sput-object v0, Lcom/squareup/protos/eventstream/v1/Application$Version;->DEFAULT_PRERELEASE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 301
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/eventstream/v1/Application$Version;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 307
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Application$Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    .line 309
    iput-object p2, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    .line 310
    iput-object p3, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    .line 311
    iput-object p4, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    .line 312
    iput-object p5, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    .line 313
    iput-object p6, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    .line 314
    iput-object p7, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    .line 315
    iput-object p8, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 336
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 337
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Application$Version;

    .line 338
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Application$Version;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Application$Version;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    .line 339
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    .line 340
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    .line 341
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    .line 342
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    .line 343
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    .line 344
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    .line 345
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    .line 346
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 351
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 353
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Application$Version;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 358
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 359
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 360
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 361
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 362
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;
    .locals 2

    .line 320
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;-><init>()V

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->major:Ljava/lang/Integer;

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->minor:Ljava/lang/Integer;

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->revision:Ljava/lang/Integer;

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->prerelease_type:Ljava/lang/String;

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->prerelease:Ljava/lang/Integer;

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->build_id:Ljava/lang/String;

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_name:Ljava/lang/String;

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_code:Ljava/lang/String;

    .line 329
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Application$Version;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 212
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Application$Version;->newBuilder()Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", major="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->major:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 371
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", minor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->minor:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 372
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->revision:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 373
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", prerelease_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", prerelease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->prerelease:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 375
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", build_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->build_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", version_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", version_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Version;->version_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Version{"

    .line 378
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
