.class public final Lcom/squareup/protos/eventstream/v1/EventMetadata;
.super Lcom/squareup/wire/Message;
.source "EventMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;,
        Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/EventMetadata;",
        "Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/EventMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COLLECTOR:Ljava/lang/String; = ""

.field public static final DEFAULT_LIBRARY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LIBRARY_VERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_UUID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final collected_timestamp:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final collector:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final library_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final library_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 105
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/eventstream/v1/EventMetadata;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 110
    sget-object v0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 113
    iput-object p3, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 114
    iput-object p4, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    .line 115
    iput-object p5, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    .line 116
    iput-object p6, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 135
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 136
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    .line 137
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    .line 143
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 148
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 157
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->uuid:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collector:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_name:Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_version:Ljava/lang/String;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->newBuilder()Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", recorded_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", collected_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", collector="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", library_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", library_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EventMetadata{"

    .line 171
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
