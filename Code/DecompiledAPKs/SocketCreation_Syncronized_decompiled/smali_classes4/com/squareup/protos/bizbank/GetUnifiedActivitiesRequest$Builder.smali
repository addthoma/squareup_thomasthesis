.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivitiesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

.field public batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

.field public filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activity_state_category(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    return-object p0
.end method

.method public batch_request(Lcom/squareup/protos/bizbank/BatchRequest;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;
    .locals 7

    .line 178
    new-instance v6, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    iget-object v4, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;Lcom/squareup/protos/bizbank/BatchRequest;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;

    move-result-object v0

    return-object v0
.end method

.method public filters(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
