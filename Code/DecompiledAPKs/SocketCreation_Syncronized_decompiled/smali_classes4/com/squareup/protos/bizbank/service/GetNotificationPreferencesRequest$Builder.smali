.class public final Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetNotificationPreferencesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;",
        "Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;
    .locals 3

    .line 89
    new-instance v0, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;->unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;->build()Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;

    move-result-object v0

    return-object v0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
