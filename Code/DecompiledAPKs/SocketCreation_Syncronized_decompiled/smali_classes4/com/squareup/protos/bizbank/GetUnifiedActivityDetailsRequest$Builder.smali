.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivityDetailsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activity_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->activity_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->activity_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;

    move-result-object v0

    return-object v0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
