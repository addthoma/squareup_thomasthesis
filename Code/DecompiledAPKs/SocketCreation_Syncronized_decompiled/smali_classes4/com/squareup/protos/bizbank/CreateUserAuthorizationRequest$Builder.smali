.class public final Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateUserAuthorizationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authentication_token:Ljava/lang/String;

.field public card_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authentication_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->authentication_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->card_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->authentication_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->build()Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
