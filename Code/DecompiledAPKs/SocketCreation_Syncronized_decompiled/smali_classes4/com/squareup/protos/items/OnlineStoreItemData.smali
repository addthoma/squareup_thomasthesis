.class public final Lcom/squareup/protos/items/OnlineStoreItemData;
.super Lcom/squareup/wire/Message;
.source "OnlineStoreItemData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;,
        Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;,
        Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/items/OnlineStoreItemData;",
        "Lcom/squareup/protos/items/OnlineStoreItemData$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/items/OnlineStoreItemData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OPTED_OUT_FOR_AUTO_SHARING:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.items.OnlineStoreDonationData#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final event_data:Lcom/squareup/protos/items/OnlineStoreEventData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.items.OnlineStoreEventData#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final opted_out_for_auto_sharing:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.items.OnlineStorePickupData#ADAPTER"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.items.OnlineStoreItemData$OnlineStoreItemType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;

    invoke-direct {v0}, Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;-><init>()V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/items/OnlineStoreItemData;->DEFAULT_VERSION:Ljava/lang/Integer;

    .line 31
    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    sput-object v1, Lcom/squareup/protos/items/OnlineStoreItemData;->DEFAULT_TYPE:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData;->DEFAULT_OPTED_OUT_FOR_AUTO_SHARING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;Lcom/squareup/protos/items/OnlineStoreEventData;Lcom/squareup/protos/items/OnlineStoreDonationData;Lcom/squareup/protos/items/OnlineStorePickupData;Ljava/lang/Boolean;)V
    .locals 8

    .line 75
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/items/OnlineStoreItemData;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;Lcom/squareup/protos/items/OnlineStoreEventData;Lcom/squareup/protos/items/OnlineStoreDonationData;Lcom/squareup/protos/items/OnlineStorePickupData;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;Lcom/squareup/protos/items/OnlineStoreEventData;Lcom/squareup/protos/items/OnlineStoreDonationData;Lcom/squareup/protos/items/OnlineStorePickupData;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/protos/items/OnlineStoreItemData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    .line 84
    iput-object p2, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 85
    iput-object p3, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    .line 86
    iput-object p4, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    .line 87
    iput-object p5, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    .line 88
    iput-object p6, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 107
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/items/OnlineStoreItemData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 108
    :cond_1
    check-cast p1, Lcom/squareup/protos/items/OnlineStoreItemData;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreItemData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/items/OnlineStoreItemData;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    iget-object v3, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    iget-object v3, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    iget-object v3, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    iget-object v3, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreItemData;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/items/OnlineStoreEventData;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/items/OnlineStoreDonationData;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/items/OnlineStorePickupData;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 129
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 2

    .line 93
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->version:Ljava/lang/Integer;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    iput-object v1, v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    iput-object v1, v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    iput-object v1, v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    iput-object v1, v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreItemData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreItemData;->newBuilder()Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    if-eqz v1, :cond_2

    const-string v1, ", event_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    if-eqz v1, :cond_3

    const-string v1, ", donation_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    if-eqz v1, :cond_4

    const-string v1, ", pickup_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", opted_out_for_auto_sharing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OnlineStoreItemData{"

    .line 143
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
