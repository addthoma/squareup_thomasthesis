.class public final Lcom/squareup/protos/items/OnlineStoreEventData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OnlineStoreEventData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/items/OnlineStoreEventData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/items/OnlineStoreEventData;",
        "Lcom/squareup/protos/items/OnlineStoreEventData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public admission_time:Lcom/squareup/protos/common/time/DateTime;

.field public end_time:Lcom/squareup/protos/common/time/DateTime;

.field public location:Ljava/lang/String;

.field public start_time:Lcom/squareup/protos/common/time/DateTime;

.field public venue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public admission_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/items/OnlineStoreEventData$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->admission_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/items/OnlineStoreEventData;
    .locals 8

    .line 172
    new-instance v7, Lcom/squareup/protos/items/OnlineStoreEventData;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->start_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->location:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->admission_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v5, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->venue:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/items/OnlineStoreEventData;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->build()Lcom/squareup/protos/items/OnlineStoreEventData;

    move-result-object v0

    return-object v0
.end method

.method public end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/items/OnlineStoreEventData$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public location(Ljava/lang/String;)Lcom/squareup/protos/items/OnlineStoreEventData$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->location:Ljava/lang/String;

    return-object p0
.end method

.method public start_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/items/OnlineStoreEventData$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->start_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public venue(Ljava/lang/String;)Lcom/squareup/protos/items/OnlineStoreEventData$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreEventData$Builder;->venue:Ljava/lang/String;

    return-object p0
.end method
