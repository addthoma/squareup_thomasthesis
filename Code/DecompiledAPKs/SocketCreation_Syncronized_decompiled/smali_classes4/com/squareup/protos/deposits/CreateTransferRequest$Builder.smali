.class public final Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateTransferRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/CreateTransferRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/CreateTransferRequest;",
        "Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/Money;

.field public balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

.field public client_ip:Ljava/lang/String;

.field public destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

.field public idempotence_key:Ljava/lang/String;

.field public source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 185
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public balance_activity_type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/CreateTransferRequest;
    .locals 10

    .line 242
    new-instance v9, Lcom/squareup/protos/deposits/CreateTransferRequest;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    iget-object v5, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iget-object v6, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iget-object v7, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->client_ip:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/deposits/CreateTransferRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/deposits/BalanceActivityType;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 170
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_ip(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->client_ip:Ljava/lang/String;

    return-object p0
.end method

.method public destination(Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    return-object p0
.end method

.method public idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key:Ljava/lang/String;

    return-object p0
.end method

.method public source(Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
