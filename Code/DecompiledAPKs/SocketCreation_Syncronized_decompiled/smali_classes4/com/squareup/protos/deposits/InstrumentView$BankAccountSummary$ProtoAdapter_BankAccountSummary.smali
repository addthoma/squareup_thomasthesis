.class final Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$ProtoAdapter_BankAccountSummary;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BankAccountSummary"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 486
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 505
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;-><init>()V

    .line 506
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 507
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 512
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 510
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->bank_name(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;

    goto :goto_0

    .line 509
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;

    goto :goto_0

    .line 516
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 517
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 484
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$ProtoAdapter_BankAccountSummary;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 498
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->account_number_suffix:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 499
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->bank_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 500
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 484
    check-cast p2, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$ProtoAdapter_BankAccountSummary;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)I
    .locals 4

    .line 491
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->account_number_suffix:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->bank_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 492
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 493
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 484
    check-cast p1, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$ProtoAdapter_BankAccountSummary;->encodedSize(Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;
    .locals 0

    .line 522
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->newBuilder()Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;

    move-result-object p1

    .line 523
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 524
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 484
    check-cast p1, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$ProtoAdapter_BankAccountSummary;->redact(Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    move-result-object p1

    return-object p1
.end method
