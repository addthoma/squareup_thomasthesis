.class public final Lcom/squareup/protos/deposits/BalanceActivity;
.super Lcom/squareup/wire/Message;
.source "BalanceActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;,
        Lcom/squareup/protos/deposits/BalanceActivity$State;,
        Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/BalanceActivity;",
        "Lcom/squareup/protos/deposits/BalanceActivity$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/BalanceActivity;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BALANCE_TYPE:Lcom/squareup/protos/deposits/BalanceType;

.field public static final DEFAULT_CLIENT_IP:Ljava/lang/String; = ""

.field public static final DEFAULT_EXTERNAL_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PAGINATION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/deposits/BalanceActivity$State;

.field public static final DEFAULT_SYNC_ID:Ljava/lang/Long;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/deposits/BalanceActivityType;

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final balance_type:Lcom/squareup/protos/deposits/BalanceType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceType#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final client_ip:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityEntityView#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final external_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityFailure#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final fee_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.ledger.service.FeeStructure#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final pagination_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityEntityView#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/deposits/BalanceActivity$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivity$State#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final sync_id:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0xb
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/deposits/BalanceActivityType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityType#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityType;->DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceActivityType;

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->DEFAULT_TYPE:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 34
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivity$State;->PENDING:Lcom/squareup/protos/deposits/BalanceActivity$State;

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->DEFAULT_STATE:Lcom/squareup/protos/deposits/BalanceActivity$State;

    const-wide/16 v0, 0x0

    .line 38
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->DEFAULT_SYNC_ID:Ljava/lang/Long;

    .line 40
    sget-object v0, Lcom/squareup/protos/deposits/BalanceType;->UNKNOWN_BALANCE_TYPE_DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceType;

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->DEFAULT_BALANCE_TYPE:Lcom/squareup/protos/deposits/BalanceType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/ledger/service/FeeStructure;Lcom/squareup/protos/deposits/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/deposits/BalanceActivity$State;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/deposits/BalanceType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/BalanceActivityFailure;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 188
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/protos/deposits/BalanceActivity;-><init>(Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/ledger/service/FeeStructure;Lcom/squareup/protos/deposits/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/deposits/BalanceActivity$State;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/deposits/BalanceType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/BalanceActivityFailure;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/ledger/service/FeeStructure;Lcom/squareup/protos/deposits/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/deposits/BalanceActivity$State;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/deposits/BalanceType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/BalanceActivityFailure;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 196
    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 197
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    move-object v1, p2

    .line 198
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    move-object v1, p3

    .line 199
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    move-object v1, p4

    .line 200
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    move-object v1, p5

    .line 201
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    move-object v1, p6

    .line 202
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    move-object v1, p7

    .line 203
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    move-object v1, p8

    .line 204
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    move-object v1, p9

    .line 205
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    move-object v1, p10

    .line 206
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    move-object v1, p11

    .line 207
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    move-object v1, p12

    .line 208
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    move-object/from16 v1, p13

    .line 209
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 210
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    move-object/from16 v1, p15

    .line 211
    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 239
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/BalanceActivity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 240
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivity;

    .line 241
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivity;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    .line 244
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 245
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    .line 246
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    .line 248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 249
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    .line 250
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    .line 251
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    .line 252
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    .line 253
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    .line 254
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    .line 255
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    iget-object p1, p1, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    .line 256
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 261
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_f

    .line 263
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivity;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/ledger/service/FeeStructure;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityType;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivity$State;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceType;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 277
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 278
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->hashCode()I

    move-result v2

    :cond_e
    add-int/2addr v0, v2

    .line 279
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_f
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 2

    .line 216
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;-><init>()V

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->token:Ljava/lang/String;

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->pagination_token:Ljava/lang/String;

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->sync_id:Ljava/lang/Long;

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->client_ip:Ljava/lang/String;

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->external_token:Ljava/lang/String;

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    .line 232
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivity;->newBuilder()Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    if-eqz v1, :cond_0

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    if-eqz v1, :cond_1

    const-string v1, ", destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 290
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", fee_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 291
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    if-eqz v1, :cond_4

    const-string v1, ", fee_structure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 292
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    if-eqz v1, :cond_5

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 295
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    if-eqz v1, :cond_8

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 296
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", pagination_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    if-eqz v1, :cond_a

    const-string v1, ", sync_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 298
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    if-eqz v1, :cond_b

    const-string v1, ", balance_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 299
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", client_ip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", external_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    if-eqz v1, :cond_e

    const-string v1, ", failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BalanceActivity{"

    .line 302
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
