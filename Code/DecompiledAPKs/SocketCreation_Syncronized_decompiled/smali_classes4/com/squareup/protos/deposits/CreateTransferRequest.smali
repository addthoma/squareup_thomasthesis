.class public final Lcom/squareup/protos/deposits/CreateTransferRequest;
.super Lcom/squareup/wire/Message;
.source "CreateTransferRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;,
        Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/CreateTransferRequest;",
        "Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/CreateTransferRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BALANCE_ACTIVITY_TYPE:Lcom/squareup/protos/deposits/BalanceActivityType;

.field public static final DEFAULT_CLIENT_IP:Ljava/lang/String; = ""

.field public static final DEFAULT_IDEMPOTENCE_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final client_ip:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityEntityIdentifier#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final idempotence_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityEntityIdentifier#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/CreateTransferRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityType;->DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceActivityType;

    sput-object v0, Lcom/squareup/protos/deposits/CreateTransferRequest;->DEFAULT_BALANCE_ACTIVITY_TYPE:Lcom/squareup/protos/deposits/BalanceActivityType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/deposits/BalanceActivityType;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Ljava/lang/String;)V
    .locals 9

    .line 95
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/deposits/CreateTransferRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/deposits/BalanceActivityType;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/deposits/BalanceActivityType;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/protos/deposits/CreateTransferRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    .line 105
    iput-object p4, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 106
    iput-object p5, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 107
    iput-object p6, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 108
    iput-object p7, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 128
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 129
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferRequest;

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/CreateTransferRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    iget-object v3, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iget-object v3, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iget-object v3, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    .line 137
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 142
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/CreateTransferRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 152
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    .locals 2

    .line 113
    new-instance v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token:Ljava/lang/String;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->client_ip:Ljava/lang/String;

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/CreateTransferRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/CreateTransferRequest;->newBuilder()Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotence_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    if-eqz v1, :cond_3

    const-string v1, ", balance_activity_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    if-eqz v1, :cond_4

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    if-eqz v1, :cond_5

    const-string v1, ", destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", client_ip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateTransferRequest{"

    .line 167
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
