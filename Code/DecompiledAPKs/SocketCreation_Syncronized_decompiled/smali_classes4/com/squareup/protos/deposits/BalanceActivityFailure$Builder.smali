.class public final Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BalanceActivityFailure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/BalanceActivityFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/BalanceActivityFailure;",
        "Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

.field public failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

.field public money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/deposits/BalanceActivityFailure;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    iget-object v2, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    iget-object v3, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/deposits/BalanceActivityFailure;-><init>(Lcom/squareup/protos/ledger/service/EligibilityBlocker;Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityFailure;

    move-result-object v0

    return-object v0
.end method

.method public eligibility_blocker(Lcom/squareup/protos/ledger/service/EligibilityBlocker;)Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    return-object p0
.end method

.method public failure_metadata(Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;)Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    return-object p0
.end method

.method public money_moving_blocker(Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;)Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0
.end method
