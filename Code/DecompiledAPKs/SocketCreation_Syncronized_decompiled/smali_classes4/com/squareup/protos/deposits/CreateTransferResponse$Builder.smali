.class public final Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateTransferResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/CreateTransferResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

.field public failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

.field public status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public balance_activity(Lcom/squareup/protos/deposits/BalanceActivity;)Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    const/4 p1, 0x0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/CreateTransferResponse;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/deposits/CreateTransferResponse;

    iget-object v1, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    iget-object v2, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    iget-object v3, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/deposits/CreateTransferResponse;-><init>(Lcom/squareup/protos/deposits/LocalizedStatusMessage;Lcom/squareup/protos/deposits/BalanceActivity;Lcom/squareup/protos/deposits/BalanceActivityFailure;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferResponse;

    move-result-object v0

    return-object v0
.end method

.method public failure(Lcom/squareup/protos/deposits/BalanceActivityFailure;)Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    const/4 p1, 0x0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    return-object p0
.end method

.method public status_message(Lcom/squareup/protos/deposits/LocalizedStatusMessage;)Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    return-object p0
.end method
