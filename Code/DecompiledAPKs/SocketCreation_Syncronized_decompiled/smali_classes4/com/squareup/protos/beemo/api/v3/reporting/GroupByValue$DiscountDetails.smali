.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiscountDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$ProtoAdapter_DiscountDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RATE:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

.field private static final serialVersionUID:J


# instance fields
.field public final discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Discount#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final rate:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final single_quantity_applied_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DiscountDetails$DiscountBasisType#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5514
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$ProtoAdapter_DiscountDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$ProtoAdapter_DiscountDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5518
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->DEFAULT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 6

    .line 5554
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 5559
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5560
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 5561
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    .line 5562
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    .line 5563
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5580
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5581
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;

    .line 5582
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 5583
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    .line 5584
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    .line 5585
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    .line 5586
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5591
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 5593
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5594
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5595
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5596
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5597
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 5598
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;
    .locals 2

    .line 5568
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;-><init>()V

    .line 5569
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 5570
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    .line 5571
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->rate:Ljava/lang/String;

    .line 5572
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    .line 5573
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5513
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5606
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    if-eqz v1, :cond_0

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5607
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5608
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->rate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5609
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", single_quantity_applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DiscountDetails{"

    .line 5610
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
