.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemizationType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType$ProtoAdapter_ItemizationType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

.field public static final enum SURCHARGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 7985
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    .line 7987
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    const/4 v2, 0x1

    const-string v3, "ITEM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    .line 7989
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    const/4 v3, 0x2

    const-string v4, "SURCHARGE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->SURCHARGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    .line 7984
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->SURCHARGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    .line 7991
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType$ProtoAdapter_ItemizationType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType$ProtoAdapter_ItemizationType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 7995
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7996
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 8006
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->SURCHARGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-object p0

    .line 8005
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-object p0

    .line 8004
    :cond_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;
    .locals 1

    .line 7984
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;
    .locals 1

    .line 7984
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 8013
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->value:I

    return v0
.end method
