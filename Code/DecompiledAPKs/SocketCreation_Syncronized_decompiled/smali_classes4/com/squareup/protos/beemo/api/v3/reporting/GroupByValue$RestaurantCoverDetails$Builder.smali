.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7684
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;
    .locals 3

    .line 7697
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7681
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    move-result-object v0

    return-object v0
.end method

.method public cover_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;
    .locals 0

    .line 7691
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-object p0
.end method
