.class final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ThirdPartyFeeEntity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2017
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2038
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;-><init>()V

    .line 2039
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2040
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 2046
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2044
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->unit_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    goto :goto_0

    .line 2043
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    goto :goto_0

    .line 2042
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    goto :goto_0

    .line 2050
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2051
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2015
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2030
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2031
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2032
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2033
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2015
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)I
    .locals 4

    .line 2022
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 2023
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    const/4 v3, 0x3

    .line 2024
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2025
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2015
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;
    .locals 0

    .line 2056
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    move-result-object p1

    .line 2057
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2058
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2015
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    move-result-object p1

    return-object p1
.end method
