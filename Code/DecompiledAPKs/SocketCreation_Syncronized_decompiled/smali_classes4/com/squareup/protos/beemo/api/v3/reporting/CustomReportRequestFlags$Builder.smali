.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomReportRequestFlags.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cache_custom_report:Ljava/lang/Boolean;

.field public local_aggregation_report_size_limit_override:Ljava/lang/Integer;

.field public proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

.field public proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

.field public request_timeout_override_seconds:Ljava/lang/Integer;

.field public spanner_max_fanouts_per_request:Ljava/lang/Integer;

.field public spanner_max_threads_per_request:Ljava/lang/Integer;

.field public spanner_scan_info_threshold_size:Ljava/lang/Integer;

.field public spanner_scan_infos_per_thread:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;
    .locals 12

    .line 286
    new-instance v11, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->request_timeout_override_seconds:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_max_threads_per_request:Ljava/lang/Integer;

    iget-object v9, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->cache_custom_report:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 204
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    move-result-object v0

    return-object v0
.end method

.method public cache_custom_report(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->cache_custom_report:Ljava/lang/Boolean;

    return-object p0
.end method

.method public local_aggregation_report_size_limit_override(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    return-object p0
.end method

.method public proto_aggregator_bill_event_count_limit_override(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    return-object p0
.end method

.method public proto_aggregator_report_size_limit_override(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    return-object p0
.end method

.method public request_timeout_override_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->request_timeout_override_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public spanner_max_fanouts_per_request(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    return-object p0
.end method

.method public spanner_max_threads_per_request(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_max_threads_per_request:Ljava/lang/Integer;

    return-object p0
.end method

.method public spanner_scan_info_threshold_size(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    return-object p0
.end method

.method public spanner_scan_infos_per_thread(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    return-object p0
.end method
