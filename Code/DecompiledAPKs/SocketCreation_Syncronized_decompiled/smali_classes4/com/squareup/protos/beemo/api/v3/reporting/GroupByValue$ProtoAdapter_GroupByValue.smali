.class final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GroupByValue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8742
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8889
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 8890
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8891
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 9030
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 9028
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_rule(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto :goto_0

    .line 9022
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/riskevaluation/external/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/riskevaluation/external/Action;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_action(Lcom/squareup/protos/riskevaluation/external/Action;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 9024
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 9019
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto :goto_0

    .line 9018
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient(Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto :goto_0

    .line 9012
    :pswitch_4
    :try_start_1
    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_state(Lcom/squareup/orders/model/Order$Fulfillment$State;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 9014
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 9004
    :pswitch_5
    :try_start_2
    sget-object v4, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_type(Lcom/squareup/orders/model/Order$FulfillmentType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 9006
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 9001
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_display_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8995
    :pswitch_7
    :try_start_3
    sget-object v4, Lcom/squareup/protos/riskevaluation/external/Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/riskevaluation/external/Level;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_level(Lcom/squareup/protos/riskevaluation/external/Level;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    .line 8997
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8992
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8991
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8990
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8989
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8988
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8987
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8986
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8985
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8984
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8983
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8982
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8981
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_completed_at_ms(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8980
    :pswitch_14
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8979
    :pswitch_15
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8978
    :pswitch_16
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8977
    :pswitch_17
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8976
    :pswitch_18
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8975
    :pswitch_19
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8974
    :pswitch_1a
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8973
    :pswitch_1b
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8972
    :pswitch_1c
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8971
    :pswitch_1d
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8970
    :pswitch_1e
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8969
    :pswitch_1f
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8968
    :pswitch_20
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8967
    :pswitch_21
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8966
    :pswitch_22
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8965
    :pswitch_23
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8964
    :pswitch_24
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8963
    :pswitch_25
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8962
    :pswitch_26
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8961
    :pswitch_27
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8955
    :pswitch_28
    :try_start_4
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card_activity_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_4
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v4

    .line 8957
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8952
    :pswitch_29
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8951
    :pswitch_2a
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8950
    :pswitch_2b
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8949
    :pswitch_2c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour_of_day(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8943
    :pswitch_2d
    :try_start_5
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day_of_week(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_5
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v4

    .line 8945
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8935
    :pswitch_2e
    :try_start_6
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->taxable(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_6
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v4

    .line 8937
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8927
    :pswitch_2f
    :try_start_7
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->product(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_7
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v4

    .line 8929
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8919
    :pswitch_30
    :try_start_8
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_8
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_8 .. :try_end_8} :catch_8

    goto/16 :goto_0

    :catch_8
    move-exception v4

    .line 8921
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8916
    :pswitch_31
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8915
    :pswitch_32
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8914
    :pswitch_33
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8913
    :pswitch_34
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8912
    :pswitch_35
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8911
    :pswitch_36
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8910
    :pswitch_37
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8904
    :pswitch_38
    :try_start_9
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_method(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    :try_end_9
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_9 .. :try_end_9} :catch_9

    goto/16 :goto_0

    :catch_9
    move-exception v4

    .line 8906
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 8901
    :pswitch_39
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8900
    :pswitch_3a
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8899
    :pswitch_3b
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8898
    :pswitch_3c
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8897
    :pswitch_3d
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8896
    :pswitch_3e
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8895
    :pswitch_3f
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8894
    :pswitch_40
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 8893
    :pswitch_41
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    goto/16 :goto_0

    .line 9034
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 9035
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8740
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8818
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    const/16 v2, 0x2f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8819
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8820
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8821
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8822
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    const/16 v2, 0x32

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8823
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8824
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8825
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8826
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8827
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8828
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8829
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8830
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8831
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8832
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8833
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8834
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8835
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8836
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8837
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8838
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8839
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8840
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8841
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8842
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8843
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8844
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8845
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8846
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8847
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8848
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8849
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8850
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    const/16 v2, 0x1e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8851
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8852
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8853
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    const/16 v2, 0x21

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8854
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    const/16 v2, 0x22

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8855
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    const/16 v2, 0x24

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8856
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    const/16 v2, 0x25

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8857
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    const/16 v2, 0x26

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8858
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    const/16 v2, 0x27

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8859
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    const/16 v2, 0x28

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8860
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    const/16 v2, 0x29

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8861
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    const/16 v2, 0x2a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8862
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    const/16 v2, 0x2b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8863
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    const/16 v2, 0x2c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8864
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    const/16 v2, 0x2d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8865
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    const/16 v2, 0x2e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8866
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    const/16 v2, 0x30

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8867
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    const/16 v2, 0x31

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8868
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    const/16 v2, 0x33

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8869
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    const/16 v2, 0x34

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8870
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    const/16 v2, 0x35

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8871
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    const/16 v2, 0x36

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8872
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    const/16 v2, 0x37

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8873
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    const/16 v2, 0x38

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8874
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    const/16 v2, 0x39

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8875
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    const/16 v2, 0x3a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8876
    sget-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    const/16 v2, 0x3b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8877
    sget-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    const/16 v2, 0x41

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8878
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    const/16 v2, 0x42

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8879
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    const/16 v2, 0x3c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8880
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/16 v2, 0x3d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8881
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/16 v2, 0x3e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8882
    sget-object v0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    const/16 v2, 0x3f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8883
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    const/16 v2, 0x40

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8884
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8740
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)I
    .locals 4

    .line 8747
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    const/16 v2, 0x2f

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    const/4 v3, 0x1

    .line 8748
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    const/4 v3, 0x2

    .line 8749
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    const/4 v3, 0x3

    .line 8750
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    const/16 v3, 0x32

    .line 8751
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    const/16 v3, 0x23

    .line 8752
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    const/4 v3, 0x4

    .line 8753
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    const/4 v3, 0x5

    .line 8754
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    const/4 v3, 0x6

    .line 8755
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    const/4 v3, 0x7

    .line 8756
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    const/16 v3, 0x8

    .line 8757
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    const/16 v3, 0x9

    .line 8758
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/16 v3, 0xa

    .line 8759
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    const/16 v3, 0xb

    .line 8760
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v3, 0xc

    .line 8761
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v3, 0xd

    .line 8762
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    const/16 v3, 0xe

    .line 8763
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    const/16 v3, 0xf

    .line 8764
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v3, 0x10

    .line 8765
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v3, 0x11

    .line 8766
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    const/16 v3, 0x12

    .line 8767
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/16 v3, 0x13

    .line 8768
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    const/16 v3, 0x14

    .line 8769
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/16 v3, 0x15

    .line 8770
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    const/16 v3, 0x16

    .line 8771
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    const/16 v3, 0x17

    .line 8772
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    const/16 v3, 0x18

    .line 8773
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    const/16 v3, 0x19

    .line 8774
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    const/16 v3, 0x1a

    .line 8775
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    const/16 v3, 0x1b

    .line 8776
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    const/16 v3, 0x1c

    .line 8777
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    const/16 v3, 0x1d

    .line 8778
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    const/16 v3, 0x1e

    .line 8779
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    const/16 v3, 0x1f

    .line 8780
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    const/16 v3, 0x20

    .line 8781
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    const/16 v3, 0x21

    .line 8782
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    const/16 v3, 0x22

    .line 8783
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    const/16 v3, 0x24

    .line 8784
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    const/16 v3, 0x25

    .line 8785
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    const/16 v3, 0x26

    .line 8786
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    const/16 v3, 0x27

    .line 8787
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    const/16 v3, 0x28

    .line 8788
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    const/16 v3, 0x29

    .line 8789
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    const/16 v3, 0x2a

    .line 8790
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    const/16 v3, 0x2b

    .line 8791
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    const/16 v3, 0x2c

    .line 8792
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    const/16 v3, 0x2d

    .line 8793
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    const/16 v3, 0x2e

    .line 8794
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    const/16 v3, 0x30

    .line 8795
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    const/16 v3, 0x31

    .line 8796
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    const/16 v3, 0x33

    .line 8797
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    const/16 v3, 0x34

    .line 8798
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    const/16 v3, 0x35

    .line 8799
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    const/16 v3, 0x36

    .line 8800
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    const/16 v3, 0x37

    .line 8801
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    const/16 v3, 0x38

    .line 8802
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    const/16 v3, 0x39

    .line 8803
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    const/16 v3, 0x3a

    .line 8804
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    const/16 v3, 0x3b

    .line 8805
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    const/16 v3, 0x41

    .line 8806
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    const/16 v3, 0x42

    .line 8807
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    const/16 v3, 0x3c

    .line 8808
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/16 v3, 0x3d

    .line 8809
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/16 v3, 0x3e

    .line 8810
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    const/16 v3, 0x3f

    .line 8811
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    const/16 v3, 0x40

    .line 8812
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8813
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8740
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
    .locals 2

    .line 9040
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    move-result-object p1

    .line 9041
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 9042
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 9043
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 9044
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 9045
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    .line 9046
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    .line 9047
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 9048
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 9049
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    .line 9050
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    .line 9051
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 9052
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    .line 9053
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 9054
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 9055
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 9056
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    .line 9057
    :cond_f
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 9058
    :cond_10
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 9059
    :cond_11
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 9060
    :cond_12
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    .line 9061
    :cond_13
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    .line 9062
    :cond_14
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    .line 9063
    :cond_15
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    .line 9064
    :cond_16
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    .line 9065
    :cond_17
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    .line 9066
    :cond_18
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    .line 9067
    :cond_19
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    .line 9068
    :cond_1a
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    .line 9069
    :cond_1b
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    .line 9070
    :cond_1c
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    .line 9071
    :cond_1d
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    if-eqz v0, :cond_1e

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    .line 9072
    :cond_1e
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    .line 9073
    :cond_1f
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    if-eqz v0, :cond_20

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    .line 9074
    :cond_20
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    if-eqz v0, :cond_21

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    .line 9075
    :cond_21
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    if-eqz v0, :cond_22

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    .line 9076
    :cond_22
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    if-eqz v0, :cond_23

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    .line 9077
    :cond_23
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    if-eqz v0, :cond_24

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    .line 9078
    :cond_24
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    if-eqz v0, :cond_25

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    .line 9079
    :cond_25
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    if-eqz v0, :cond_26

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    .line 9080
    :cond_26
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    if-eqz v0, :cond_27

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    .line 9081
    :cond_27
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    if-eqz v0, :cond_28

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    .line 9082
    :cond_28
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    if-eqz v0, :cond_29

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    .line 9083
    :cond_29
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    if-eqz v0, :cond_2a

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    .line 9084
    :cond_2a
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    if-eqz v0, :cond_2b

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    .line 9085
    :cond_2b
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    if-eqz v0, :cond_2c

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    .line 9086
    :cond_2c
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    if-eqz v0, :cond_2d

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    .line 9087
    :cond_2d
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    if-eqz v0, :cond_2e

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    .line 9088
    :cond_2e
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    if-eqz v0, :cond_2f

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    .line 9089
    :cond_2f
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    if-eqz v0, :cond_30

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    .line 9090
    :cond_30
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    if-eqz v0, :cond_31

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    .line 9091
    :cond_31
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    if-eqz v0, :cond_32

    sget-object v0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    .line 9092
    :cond_32
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    if-eqz v0, :cond_33

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    .line 9093
    :cond_33
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 9094
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8740
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object p1

    return-object p1
.end method
