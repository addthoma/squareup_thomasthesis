.class final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CustomReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CustomReport"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 203
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;-><init>()V

    .line 229
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 230
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 245
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 243
    :cond_0
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    goto :goto_0

    .line 242
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->sub_report:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    :cond_2
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    goto :goto_0

    .line 240
    :cond_3
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    goto :goto_0

    .line 234
    :cond_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 236
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 249
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 250
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 201
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 219
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 201
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)I
    .locals 4

    .line 208
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    const/4 v3, 0x3

    .line 209
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 210
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    const/4 v3, 0x2

    .line 211
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    const/4 v3, 0x5

    .line 212
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 201
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
    .locals 2

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    move-result-object p1

    .line 256
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    .line 257
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->sub_report:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 258
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    .line 259
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    .line 260
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 261
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 201
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p1

    return-object p1
.end method
