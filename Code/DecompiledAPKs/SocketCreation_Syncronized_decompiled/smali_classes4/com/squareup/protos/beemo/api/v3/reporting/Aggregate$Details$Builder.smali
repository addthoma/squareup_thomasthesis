.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public australian_gross_sales_money:Lcom/squareup/protos/common/Money;

.field public auto_gratuity_money:Lcom/squareup/protos/common/Money;

.field public average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

.field public average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

.field public average_gross_sales_money:Lcom/squareup/protos/common/Money;

.field public cover_count:Ljava/lang/Long;

.field public credit_money:Lcom/squareup/protos/common/Money;

.field public discount_count:Ljava/lang/Long;

.field public discount_money:Lcom/squareup/protos/common/Money;

.field public fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

.field public gift_card_load_count:Ljava/lang/Long;

.field public gift_card_load_money:Lcom/squareup/protos/common/Money;

.field public gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

.field public gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

.field public gift_cards_discounted_count:Ljava/lang/Long;

.field public gross_sales_money:Lcom/squareup/protos/common/Money;

.field public item_count:Ljava/lang/String;

.field public item_gross_sales_money:Lcom/squareup/protos/common/Money;

.field public item_net_sales_money:Lcom/squareup/protos/common/Money;

.field public item_quantity:Ljava/lang/String;

.field public items_discounted_count:Ljava/lang/String;

.field public items_modified_count:Ljava/lang/String;

.field public items_taxed_count:Ljava/lang/String;

.field public lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

.field public measured_quantity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;",
            ">;"
        }
    .end annotation
.end field

.field public modifier_count:Ljava/lang/Long;

.field public modifier_money:Lcom/squareup/protos/common/Money;

.field public modifier_quantity:Ljava/lang/String;

.field public net_sales_money:Lcom/squareup/protos/common/Money;

.field public net_total_money:Lcom/squareup/protos/common/Money;

.field public processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

.field public processing_fee_money:Lcom/squareup/protos/common/Money;

.field public product_sales_money:Lcom/squareup/protos/common/Money;

.field public refund_count:Ljava/lang/Long;

.field public surcharge_count:Ljava/lang/Long;

.field public surcharge_money:Lcom/squareup/protos/common/Money;

.field public swedish_rounding_money:Lcom/squareup/protos/common/Money;

.field public tax_money:Lcom/squareup/protos/common/Money;

.field public taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

.field public taxable_net_sales_money:Lcom/squareup/protos/common/Money;

.field public tender_count:Ljava/lang/Long;

.field public tender_tax_money:Lcom/squareup/protos/common/Money;

.field public tip_money:Lcom/squareup/protos/common/Money;

.field public total_collected_money:Lcom/squareup/protos/common/Money;

.field public total_sales_money:Lcom/squareup/protos/common/Money;

.field public transaction_count:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 975
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 976
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->measured_quantity:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public australian_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1226
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public auto_gratuity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1314
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public average_australian_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1236
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public average_cost_of_goods_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1294
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    return-object p0
.end method

.method public average_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1193
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
    .locals 2

    .line 1370
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 882
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    move-result-object v0

    return-object v0
.end method

.method public cover_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1323
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->cover_count:Ljava/lang/Long;

    return-object p0
.end method

.method public credit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1161
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public discount_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1001
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_count:Ljava/lang/Long;

    return-object p0
.end method

.method public discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 988
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public fifo_cost_of_goods_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1274
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    return-object p0
.end method

.method public gift_card_load_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1201
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_count:Ljava/lang/Long;

    return-object p0
.end method

.method public gift_card_load_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1185
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public gift_card_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1177
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public gift_cards_discounted_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1217
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public gift_cards_discounted_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1209
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_count:Ljava/lang/Long;

    return-object p0
.end method

.method public gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1033
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1100
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_count:Ljava/lang/String;

    return-object p0
.end method

.method public item_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1041
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1116
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_quantity(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1090
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_quantity:Ljava/lang/String;

    return-object p0
.end method

.method public items_discounted_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1252
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_discounted_count:Ljava/lang/String;

    return-object p0
.end method

.method public items_modified_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1260
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_modified_count:Ljava/lang/String;

    return-object p0
.end method

.method public items_taxed_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1244
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_taxed_count:Ljava/lang/String;

    return-object p0
.end method

.method public lifo_cost_of_goods_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1283
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    return-object p0
.end method

.method public measured_quantity(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;"
        }
    .end annotation

    .line 1363
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1364
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->measured_quantity:Ljava/util/List;

    return-object p0
.end method

.method public modifier_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1017
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_count:Ljava/lang/Long;

    return-object p0
.end method

.method public modifier_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1009
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public modifier_quantity(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1025
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_quantity:Ljava/lang/String;

    return-object p0
.end method

.method public net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1108
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public net_total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1152
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public processing_fee_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1265
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    return-object p0
.end method

.method public processing_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1057
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public product_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1049
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public refund_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1347
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->refund_count:Ljava/lang/Long;

    return-object p0
.end method

.method public surcharge_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1331
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_count:Ljava/lang/Long;

    return-object p0
.end method

.method public surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1302
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public swedish_rounding_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1169
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1073
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public taxable_item_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1136
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public taxable_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1126
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tender_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1339
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_count:Ljava/lang/Long;

    return-object p0
.end method

.method public tender_tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1081
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1065
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_collected_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1144
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 1355
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public transaction_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 0

    .line 980
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->transaction_count:Ljava/lang/Long;

    return-object p0
.end method
