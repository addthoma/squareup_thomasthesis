.class final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ItemVariation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2424
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2447
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;-><init>()V

    .line 2448
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2449
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 2463
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2456
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->default_measurement_unit(Lcom/squareup/protos/beemo/translation_types/TranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 2458
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 2453
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    goto :goto_0

    .line 2452
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->sku(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    goto :goto_0

    .line 2451
    :cond_3
    sget-object v3, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    goto :goto_0

    .line 2467
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2468
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2422
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2438
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2439
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2440
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2441
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2442
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2422
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)I
    .locals 4

    .line 2429
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    const/4 v3, 0x2

    .line 2430
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v3, 0x3

    .line 2431
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v3, 0x4

    .line 2432
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2433
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2422
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
    .locals 2

    .line 2473
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    move-result-object p1

    .line 2474
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2475
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 2476
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2477
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2422
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    move-result-object p1

    return-object p1
.end method
