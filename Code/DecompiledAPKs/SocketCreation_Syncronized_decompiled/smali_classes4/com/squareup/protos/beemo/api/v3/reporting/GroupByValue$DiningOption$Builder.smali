.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dining_option_id:Ljava/lang/String;

.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3904
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;
    .locals 4

    .line 3927
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->dining_option_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;-><init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3899
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public dining_option_id(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3913
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->dining_option_id:Ljava/lang/String;

    return-object p0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;
    .locals 0

    .line 3921
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
