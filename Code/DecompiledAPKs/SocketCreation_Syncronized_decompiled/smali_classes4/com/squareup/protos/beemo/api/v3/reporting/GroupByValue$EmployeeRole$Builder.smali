.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_role_token:Ljava/lang/String;

.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1832
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;
    .locals 4

    .line 1850
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;->employee_role_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1827
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    move-result-object v0

    return-object v0
.end method

.method public employee_role_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;
    .locals 0

    .line 1844
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;->employee_role_token:Ljava/lang/String;

    return-object p0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;
    .locals 0

    .line 1839
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
