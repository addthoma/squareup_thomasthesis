.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_basis_points:Ljava/lang/Long;

.field public interchange_cents:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7311
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;
    .locals 4

    .line 7332
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->discount_basis_points:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->interchange_cents:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7306
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    move-result-object v0

    return-object v0
.end method

.method public discount_basis_points(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;
    .locals 0

    .line 7318
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->discount_basis_points:Ljava/lang/Long;

    return-object p0
.end method

.method public interchange_cents(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;
    .locals 0

    .line 7326
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->interchange_cents:Ljava/lang/Long;

    return-object p0
.end method
