.class final Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReportingGroupConfigDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReportingGroupConfigDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 119
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;-><init>()V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 140
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 152
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 150
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->reporting_group:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 146
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 156
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 157
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 132
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 133
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)I
    .locals 4

    .line 124
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 125
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 117
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
    .locals 2

    .line 162
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;

    move-result-object p1

    .line 163
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->reporting_group:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 117
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    move-result-object p1

    return-object p1
.end method
