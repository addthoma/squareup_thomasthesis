.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public inclusion_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

.field public rate:Ljava/lang/String;

.field public tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5389
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;
    .locals 5

    .line 5412
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->inclusion_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->rate:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5382
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    move-result-object v0

    return-object v0
.end method

.method public inclusion_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;
    .locals 0

    .line 5398
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->inclusion_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    return-object p0
.end method

.method public rate(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;
    .locals 0

    .line 5406
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->rate:Ljava/lang/String;

    return-object p0
.end method

.method public tax(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;
    .locals 0

    .line 5393
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    return-object p0
.end method
