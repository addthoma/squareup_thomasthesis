.class final Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RequestFlags.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RequestFlags"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 243
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 270
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;-><init>()V

    .line 271
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 272
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 281
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 279
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->allow_partial_fanouts(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    goto :goto_0

    .line 278
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->fetch_contact_tokens(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    goto :goto_0

    .line 277
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->force_include_future_effective_at_adjustment_fees(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    goto :goto_0

    .line 276
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->ignore_items_service_failures(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    goto :goto_0

    .line 275
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_cost_of_goods_sold(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    goto :goto_0

    .line 274
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_ledger_fees(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    goto :goto_0

    .line 285
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 286
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 259
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 260
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 261
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 262
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 263
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 264
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 265
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    check-cast p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)I
    .locals 4

    .line 248
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 250
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 251
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 252
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 253
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;->encodedSize(Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
    .locals 0

    .line 291
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    move-result-object p1

    .line 292
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 293
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;->redact(Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    move-result-object p1

    return-object p1
.end method
