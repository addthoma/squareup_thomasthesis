.class public final Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeclineSubscriptionInvitationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;",
        "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invitation_id:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;
    .locals 5

    .line 129
    new-instance v0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->invitation_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->build()Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;

    move-result-object v0

    return-object v0
.end method

.method public invitation_id(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->invitation_id:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    return-object p0
.end method
