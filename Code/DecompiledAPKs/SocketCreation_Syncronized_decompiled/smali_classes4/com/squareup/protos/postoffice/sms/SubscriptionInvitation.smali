.class public final Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
.super Lcom/squareup/wire/Message;
.source "SubscriptionInvitation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;,
        Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;",
        "Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBTITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_TERMS:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.postoffice.sms.Subscriber#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final subtitle:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final terms:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;-><init>()V

    sput-object v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 64
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;-><init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    .line 72
    iput-object p3, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iget-object v3, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    .line 99
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 104
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/postoffice/sms/Subscriber;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->id:Ljava/lang/String;

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iput-object v1, v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->title:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subtitle:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->terms:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->newBuilder()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    if-eqz v1, :cond_1

    const-string v1, ", subscriber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", subtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", terms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SubscriptionInvitation{"

    .line 125
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
