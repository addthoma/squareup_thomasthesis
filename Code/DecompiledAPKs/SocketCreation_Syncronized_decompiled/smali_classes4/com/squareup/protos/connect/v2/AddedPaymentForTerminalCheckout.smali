.class public final Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;
.super Lcom/squareup/wire/Message;
.source "AddedPaymentForTerminalCheckout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$ProtoAdapter_AddedPaymentForTerminalCheckout;,
        Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
        "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAYMENT_FROM_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

.field public static final DEFAULT_PAYMENT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final from:Lcom/squareup/protos/common/location/GeoLocation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GeoLocation#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final payment_from_country_code:Lcom/squareup/protos/common/countries/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.countries.Country#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final payment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$ProtoAdapter_AddedPaymentForTerminalCheckout;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$ProtoAdapter_AddedPaymentForTerminalCheckout;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/common/countries/Country;->US:Lcom/squareup/protos/common/countries/Country;

    sput-object v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->DEFAULT_PAYMENT_FROM_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GeoLocation;Lcom/squareup/protos/common/countries/Country;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GeoLocation;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GeoLocation;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_id:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    .line 59
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 75
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 76
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_id:Ljava/lang/String;

    .line 78
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    .line 80
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 85
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GeoLocation;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/countries/Country;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 91
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;
    .locals 2

    .line 64
    new-instance v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;-><init>()V

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->payment_id:Ljava/lang/String;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->from:Lcom/squareup/protos/common/location/GeoLocation;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->newBuilder()Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ", payment_id="

    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    if-eqz v1, :cond_0

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->from:Lcom/squareup/protos/common/location/GeoLocation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_1

    const-string v1, ", payment_from_country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;->payment_from_country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AddedPaymentForTerminalCheckout{"

    .line 102
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
