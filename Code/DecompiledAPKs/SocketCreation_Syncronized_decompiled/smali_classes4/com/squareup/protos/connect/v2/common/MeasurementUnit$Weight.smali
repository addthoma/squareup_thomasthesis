.class public final enum Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;
.super Ljava/lang/Enum;
.source "MeasurementUnit.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Weight"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight$ProtoAdapter_Weight;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final enum IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final enum IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final enum INVALID_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final enum METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final enum METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field public static final enum METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 869
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v1, 0x0

    const-string v2, "INVALID_WEIGHT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->INVALID_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 876
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v2, 0x1

    const-string v3, "IMPERIAL_WEIGHT_OUNCE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 883
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v3, 0x5

    const/4 v4, 0x2

    const-string v5, "IMPERIAL_POUND"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 890
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v5, 0x6

    const/4 v6, 0x3

    const-string v7, "IMPERIAL_STONE"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 897
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v7, 0x4

    const-string v8, "METRIC_MILLIGRAM"

    invoke-direct {v0, v8, v7, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 904
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const-string v8, "METRIC_GRAM"

    invoke-direct {v0, v8, v3, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 911
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const-string v8, "METRIC_KILOGRAM"

    invoke-direct {v0, v8, v5, v7}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 868
    sget-object v8, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->INVALID_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 913
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight$ProtoAdapter_Weight;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight$ProtoAdapter_Weight;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 918
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 929
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    .line 928
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    .line 932
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    .line 931
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    .line 930
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    .line 927
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    .line 926
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->INVALID_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;
    .locals 1

    .line 868
    const-class v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;
    .locals 1

    .line 868
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 939
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->value:I

    return v0
.end method
