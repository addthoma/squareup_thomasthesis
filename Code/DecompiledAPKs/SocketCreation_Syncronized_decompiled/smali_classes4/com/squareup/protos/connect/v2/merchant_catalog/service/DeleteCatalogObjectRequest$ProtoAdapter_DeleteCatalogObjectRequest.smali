.class final Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$ProtoAdapter_DeleteCatalogObjectRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DeleteCatalogObjectRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DeleteCatalogObjectRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 109
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 126
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;-><init>()V

    .line 127
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 128
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 132
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 130
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->object_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;

    goto :goto_0

    .line 136
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 137
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 107
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$ProtoAdapter_DeleteCatalogObjectRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 120
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;->object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 121
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 107
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$ProtoAdapter_DeleteCatalogObjectRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)I
    .locals 3

    .line 114
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;->object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 115
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$ProtoAdapter_DeleteCatalogObjectRequest;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;
    .locals 0

    .line 142
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;

    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 144
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$ProtoAdapter_DeleteCatalogObjectRequest;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    move-result-object p1

    return-object p1
.end method
