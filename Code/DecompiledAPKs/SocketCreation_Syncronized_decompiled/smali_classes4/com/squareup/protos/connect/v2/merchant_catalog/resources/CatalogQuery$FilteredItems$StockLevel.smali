.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;
.super Ljava/lang/Enum;
.source "CatalogQuery.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StockLevel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel$ProtoAdapter_StockLevel;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LOW:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

.field public static final enum OUT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

.field public static final enum STOCK_LEVEL_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 2212
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    const/4 v1, 0x0

    const-string v2, "STOCK_LEVEL_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->STOCK_LEVEL_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    .line 2214
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    const/4 v2, 0x1

    const-string v3, "OUT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->OUT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    .line 2216
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    const/4 v3, 0x2

    const-string v4, "LOW"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->LOW:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    .line 2211
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->STOCK_LEVEL_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->OUT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->LOW:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    .line 2218
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel$ProtoAdapter_StockLevel;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel$ProtoAdapter_StockLevel;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 2222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2223
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 2233
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->LOW:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    return-object p0

    .line 2232
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->OUT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    return-object p0

    .line 2231
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->STOCK_LEVEL_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;
    .locals 1

    .line 2211
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;
    .locals 1

    .line 2211
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 2240
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$StockLevel;->value:I

    return v0
.end method
