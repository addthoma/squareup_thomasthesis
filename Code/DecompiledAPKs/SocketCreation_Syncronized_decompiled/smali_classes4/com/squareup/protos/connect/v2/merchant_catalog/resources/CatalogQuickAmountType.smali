.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;
.super Ljava/lang/Enum;
.source "CatalogQuickAmountType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType$ProtoAdapter_CatalogQuickAmountType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum QUICK_AMOUNT_TYPE_AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

.field public static final enum QUICK_AMOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

.field public static final enum QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 16
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    const/4 v1, 0x0

    const-string v2, "QUICK_AMOUNT_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    const/4 v2, 0x1

    const-string v3, "QUICK_AMOUNT_TYPE_MANUAL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    .line 30
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    const/4 v3, 0x2

    const-string v4, "QUICK_AMOUNT_TYPE_AUTO"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    .line 15
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    .line 32
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType$ProtoAdapter_CatalogQuickAmountType;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType$ProtoAdapter_CatalogQuickAmountType;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 47
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    return-object p0

    .line 46
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    return-object p0

    .line 45
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->value:I

    return v0
.end method
