.class public final Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckoutMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CheckoutMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CheckoutMetadata;",
        "Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public acked_at:Ljava/lang/String;

.field public canceled_at:Ljava/lang/String;

.field public completed_at:Ljava/lang/String;

.field public payments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
            ">;"
        }
    .end annotation
.end field

.field public pushed_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 190
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 191
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->payments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public ack_reason(Lcom/squareup/protos/connect/v2/AckReasons$AckReason;)Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0
.end method

.method public acked_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->acked_at:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/CheckoutMetadata;
    .locals 9

    .line 255
    new-instance v8, Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->pushed_at:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->acked_at:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->completed_at:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->canceled_at:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->payments:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 177
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->build()Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    move-result-object v0

    return-object v0
.end method

.method public canceled_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->canceled_at:Ljava/lang/String;

    return-object p0
.end method

.method public completed_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->completed_at:Ljava/lang/String;

    return-object p0
.end method

.method public payments(Ljava/util/List;)Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;"
        }
    .end annotation

    .line 248
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->payments:Ljava/util/List;

    return-object p0
.end method

.method public pushed_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->pushed_at:Ljava/lang/String;

    return-object p0
.end method
