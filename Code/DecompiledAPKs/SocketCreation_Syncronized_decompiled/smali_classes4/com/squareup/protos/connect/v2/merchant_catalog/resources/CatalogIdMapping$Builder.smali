.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogIdMapping.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_object_id:Ljava/lang/String;

.field public object_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;->client_object_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;->object_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;

    move-result-object v0

    return-object v0
.end method

.method public client_object_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;->client_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public object_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping$Builder;->object_id:Ljava/lang/String;

    return-object p0
.end method
