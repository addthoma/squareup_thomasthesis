.class final Lcom/squareup/protos/connect/v2/GeoLocation$ProtoAdapter_GeoLocation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GeoLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/GeoLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GeoLocation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/GeoLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 179
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/GeoLocation;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 200
    new-instance v0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;-><init>()V

    .line 201
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 202
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 208
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 206
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->uncertainty_meters(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/GeoLocation$Builder;

    goto :goto_0

    .line 205
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->altitude_meters(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/GeoLocation$Builder;

    goto :goto_0

    .line 204
    :cond_2
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->coordinates(Lcom/squareup/protos/connect/v2/common/Coordinates;)Lcom/squareup/protos/connect/v2/GeoLocation$Builder;

    goto :goto_0

    .line 212
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 213
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->build()Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/GeoLocation$ProtoAdapter_GeoLocation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/GeoLocation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/GeoLocation;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/GeoLocation;->altitude_meters:Ljava/lang/Double;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/GeoLocation;->uncertainty_meters:Ljava/lang/Double;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/GeoLocation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    check-cast p2, Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/GeoLocation$ProtoAdapter_GeoLocation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/GeoLocation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/GeoLocation;)I
    .locals 4

    .line 184
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/GeoLocation;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/GeoLocation;->altitude_meters:Ljava/lang/Double;

    const/4 v3, 0x2

    .line 185
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/GeoLocation;->uncertainty_meters:Ljava/lang/Double;

    const/4 v3, 0x3

    .line 186
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/GeoLocation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/GeoLocation$ProtoAdapter_GeoLocation;->encodedSize(Lcom/squareup/protos/connect/v2/GeoLocation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/GeoLocation;)Lcom/squareup/protos/connect/v2/GeoLocation;
    .locals 2

    .line 218
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/GeoLocation;->newBuilder()Lcom/squareup/protos/connect/v2/GeoLocation$Builder;

    move-result-object p1

    .line 219
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Coordinates;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    .line 220
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 221
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->build()Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/GeoLocation$ProtoAdapter_GeoLocation;->redact(Lcom/squareup/protos/connect/v2/GeoLocation;)Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object p1

    return-object p1
.end method
