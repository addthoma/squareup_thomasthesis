.class public final Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 416
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;
    .locals 0

    .line 436
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;
    .locals 4

    .line 442
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->abbreviation:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 411
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;
    .locals 0

    .line 425
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
