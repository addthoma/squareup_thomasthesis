.class public final Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;
.super Lcom/squareup/wire/Message;
.source "PaymentPriceAllocationSpec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$ProtoAdapter_PaymentPriceAllocationSpec;,
        Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;",
        "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PARTY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final party:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final price_selectors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$ProtoAdapter_PaymentPriceAllocationSpec;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$ProtoAdapter_PaymentPriceAllocationSpec;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    const-string p1, "price_selectors"

    .line 79
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 96
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 97
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    .line 100
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->party:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->price_selectors:Ljava/util/List;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->newBuilder()Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", party="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->party:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", price_selectors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->price_selectors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", fee_calculation_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentPriceAllocationSpec{"

    .line 123
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
