.class public final Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreatePaymentResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_emv_data:Ljava/lang/String;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public payment:Lcom/squareup/protos/connect/v2/Payment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 132
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
    .locals 5

    .line 165
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->encrypted_emv_data:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/Payment;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_emv_data(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->encrypted_emv_data:Ljava/lang/String;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;"
        }
    .end annotation

    .line 139
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public payment(Lcom/squareup/protos/connect/v2/Payment;)Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    return-object p0
.end method
