.class public final Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AppProcessingFee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/AppProcessingFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/AppProcessingFee;",
        "Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public effective_at:Ljava/lang/String;

.field public party_account_id:Ljava/lang/String;

.field public price_selector:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 171
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 172
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->price_selector:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/AppProcessingFee;
    .locals 8

    .line 229
    new-instance v7, Lcom/squareup/protos/connect/v2/AppProcessingFee;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->price_selector:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->type:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->party_account_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->effective_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/connect/v2/AppProcessingFee;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->build()Lcom/squareup/protos/connect/v2/AppProcessingFee;

    move-result-object v0

    return-object v0
.end method

.method public effective_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->effective_at:Ljava/lang/String;

    return-object p0
.end method

.method public party_account_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->party_account_id:Ljava/lang/String;

    return-object p0
.end method

.method public price_selector(Ljava/util/List;)Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;"
        }
    .end annotation

    .line 181
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->price_selector:Ljava/util/List;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AppProcessingFee$Builder;->type:Ljava/lang/String;

    return-object p0
.end method
