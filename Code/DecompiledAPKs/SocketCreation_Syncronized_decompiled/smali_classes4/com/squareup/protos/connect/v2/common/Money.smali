.class public final Lcom/squareup/protos/connect/v2/common/Money;
.super Lcom/squareup/wire/Message;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/Money$ProtoAdapter_Money;,
        Lcom/squareup/protos/connect/v2/common/Money$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "Lcom/squareup/protos/connect/v2/common/Money$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMOUNT:Ljava/lang/Long;

.field public static final DEFAULT_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final currency:Lcom/squareup/protos/connect/v2/common/Currency;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Currency#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Money$ProtoAdapter_Money;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Money$ProtoAdapter_Money;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 35
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Money;->DEFAULT_AMOUNT:Ljava/lang/Long;

    .line 37
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Money;->DEFAULT_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/connect/v2/common/Currency;)V
    .locals 1

    .line 68
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/connect/v2/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/connect/v2/common/Currency;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/connect/v2/common/Currency;Lokio/ByteString;)V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 89
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 90
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/common/Money;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/Money;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 93
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Currency;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 103
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/common/Money$Builder;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;->amount:Ljava/lang/Long;

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money;->newBuilder()Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v1, :cond_1

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Money{"

    .line 113
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
