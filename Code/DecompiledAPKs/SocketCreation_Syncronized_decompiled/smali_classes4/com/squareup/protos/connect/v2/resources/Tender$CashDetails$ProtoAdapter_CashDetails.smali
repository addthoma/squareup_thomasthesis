.class final Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CashDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1127
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1146
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;-><init>()V

    .line 1147
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1148
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1153
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1151
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;

    goto :goto_0

    .line 1150
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;

    goto :goto_0

    .line 1157
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1158
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1125
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1139
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1140
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1141
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1125
    check-cast p2, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)I
    .locals 4

    .line 1132
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x2

    .line 1133
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1134
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1125
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;->encodedSize(Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
    .locals 2

    .line 1163
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;

    move-result-object p1

    .line 1164
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1165
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1166
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1167
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1125
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;->redact(Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    move-result-object p1

    return-object p1
.end method
