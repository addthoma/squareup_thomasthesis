.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogCustomAttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allowed_object_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

.field public custom_attribute_usage_count:Ljava/lang/Integer;

.field public description:Ljava/lang/String;

.field public key:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

.field public seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

.field public source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

.field public string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

.field public type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 283
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 284
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->allowed_object_types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allowed_object_types(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;"
        }
    .end annotation

    .line 331
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 332
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->allowed_object_types:Ljava/util/List;

    return-object p0
.end method

.method public app_visibility(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;
    .locals 14

    .line 393
    new-instance v13, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->allowed_object_types:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    iget-object v10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->custom_attribute_usage_count:Ljava/lang/Integer;

    iget-object v11, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 260
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public custom_attribute_usage_count(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->custom_attribute_usage_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 312
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public selection_config(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    return-object p0
.end method

.method public seller_visibility(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    return-object p0
.end method

.method public source_application(Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    return-object p0
.end method

.method public string_config(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 359
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    return-object p0
.end method
