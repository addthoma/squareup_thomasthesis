.class public final enum Lcom/squareup/protos/connect/v2/resources/Location$Status;
.super Ljava/lang/Enum;
.source "Location.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Location$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Location$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Status;

.field public static final enum ACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Location$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 918
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "ACTIVE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/connect/v2/resources/Location$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->ACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 926
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;

    const/4 v3, 0x2

    const-string v4, "INACTIVE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/connect/v2/resources/Location$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->INACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    new-array v0, v3, [Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 914
    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Location$Status;->ACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Location$Status;->INACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 928
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Location$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 932
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 933
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Location$Status;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 942
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->INACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    return-object p0

    .line 941
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->ACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Status;
    .locals 1

    .line 914
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Location$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Location$Status;
    .locals 1

    .line 914
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Location$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Location$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 949
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->value:I

    return v0
.end method
