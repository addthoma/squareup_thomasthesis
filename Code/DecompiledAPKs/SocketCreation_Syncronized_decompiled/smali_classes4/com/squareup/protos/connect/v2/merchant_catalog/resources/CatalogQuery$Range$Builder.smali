.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_max_value:Ljava/lang/Long;

.field public attribute_min_value:Ljava/lang/Long;

.field public attribute_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1020
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attribute_max_value(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;
    .locals 0

    .line 1043
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->attribute_max_value:Ljava/lang/Long;

    return-object p0
.end method

.method public attribute_min_value(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;
    .locals 0

    .line 1035
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->attribute_min_value:Ljava/lang/Long;

    return-object p0
.end method

.method public attribute_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;
    .locals 0

    .line 1027
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->attribute_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;
    .locals 5

    .line 1049
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->attribute_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->attribute_min_value:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->attribute_max_value:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1013
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    move-result-object v0

    return-object v0
.end method
