.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogItemModifierListInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enabled:Ljava/lang/Boolean;

.field public max_selected_modifiers:Ljava/lang/Integer;

.field public min_selected_modifiers:Ljava/lang/Integer;

.field public modifier_list_id:Ljava/lang/String;

.field public modifier_overrides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;",
            ">;"
        }
    .end annotation
.end field

.field public visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 181
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 182
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->modifier_overrides:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;
    .locals 9

    .line 233
    new-instance v8, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->modifier_list_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->modifier_overrides:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->min_selected_modifiers:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->max_selected_modifiers:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->enabled:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo;

    move-result-object v0

    return-object v0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public max_selected_modifiers(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->max_selected_modifiers:Ljava/lang/Integer;

    return-object p0
.end method

.method public min_selected_modifiers(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->min_selected_modifiers:Ljava/lang/Integer;

    return-object p0
.end method

.method public modifier_list_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->modifier_list_id:Ljava/lang/String;

    return-object p0
.end method

.method public modifier_overrides(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;"
        }
    .end annotation

    .line 202
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->modifier_overrides:Ljava/util/List;

    return-object p0
.end method

.method public visibility(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemModifierListInfo$Builder;->visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogVisibility;

    return-object p0
.end method
