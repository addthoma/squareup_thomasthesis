.class public final Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Coordinates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/Coordinates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/common/Coordinates;",
        "Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public latitude:Ljava/lang/Double;

.field public longitude:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/common/Coordinates;
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Coordinates;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->latitude:Ljava/lang/Double;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->longitude:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/common/Coordinates;-><init>(Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->build()Lcom/squareup/protos/connect/v2/common/Coordinates;

    move-result-object v0

    return-object v0
.end method

.method public latitude(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->latitude:Ljava/lang/Double;

    return-object p0
.end method

.method public longitude(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->longitude:Ljava/lang/Double;

    return-object p0
.end method
