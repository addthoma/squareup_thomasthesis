.class public final Lcom/squareup/protos/connect/v2/GeoLocation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GeoLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/GeoLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/GeoLocation;",
        "Lcom/squareup/protos/connect/v2/GeoLocation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public altitude_meters:Ljava/lang/Double;

.field public coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

.field public uncertainty_meters:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public altitude_meters(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/GeoLocation$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->altitude_meters:Ljava/lang/Double;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/GeoLocation;
    .locals 5

    .line 173
    new-instance v0, Lcom/squareup/protos/connect/v2/GeoLocation;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->altitude_meters:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->uncertainty_meters:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/GeoLocation;-><init>(Lcom/squareup/protos/connect/v2/common/Coordinates;Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->build()Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object v0

    return-object v0
.end method

.method public coordinates(Lcom/squareup/protos/connect/v2/common/Coordinates;)Lcom/squareup/protos/connect/v2/GeoLocation$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    return-object p0
.end method

.method public uncertainty_meters(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/GeoLocation$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/GeoLocation$Builder;->uncertainty_meters:Ljava/lang/Double;

    return-object p0
.end method
