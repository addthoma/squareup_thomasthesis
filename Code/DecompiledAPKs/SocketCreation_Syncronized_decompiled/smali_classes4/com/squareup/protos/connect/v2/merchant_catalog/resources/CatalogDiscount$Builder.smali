.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogDiscount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

.field public comp_ordinal:Ljava/lang/Integer;

.field public discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

.field public label_color:Ljava/lang/String;

.field public maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

.field public modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public pin_required:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 282
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public application_method(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;
    .locals 13

    .line 396
    new-instance v12, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->percentage:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->pin_required:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->label_color:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->application_method:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->comp_ordinal:Ljava/lang/Integer;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    iget-object v10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountApplicationMethod;Ljava/lang/Integer;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 261
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    move-result-object v0

    return-object v0
.end method

.method public comp_ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->comp_ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public discount_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 299
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->discount_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountType;

    return-object p0
.end method

.method public label_color(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 344
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->label_color:Ljava/lang/String;

    return-object p0
.end method

.method public maximum_amount(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 390
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->maximum_amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public modify_tax_basis(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->modify_tax_basis:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscountModifyTaxBasis;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public pin_required(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount$Builder;->pin_required:Ljava/lang/Boolean;

    return-object p0
.end method
