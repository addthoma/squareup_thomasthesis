.class public final Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProcessingFee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/ProcessingFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/ProcessingFee;",
        "Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public effective_at:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 132
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/ProcessingFee;
    .locals 5

    .line 170
    new-instance v0, Lcom/squareup/protos/connect/v2/ProcessingFee;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->effective_at:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->type:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/ProcessingFee;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->build()Lcom/squareup/protos/connect/v2/ProcessingFee;

    move-result-object v0

    return-object v0
.end method

.method public effective_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->effective_at:Ljava/lang/String;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->type:Ljava/lang/String;

    return-object p0
.end method
