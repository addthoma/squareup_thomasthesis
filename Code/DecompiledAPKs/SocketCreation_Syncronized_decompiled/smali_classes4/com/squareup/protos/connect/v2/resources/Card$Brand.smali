.class public final enum Lcom/squareup/protos/connect/v2/resources/Card$Brand;
.super Ljava/lang/Enum;
.source "Card.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Brand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Card$Brand$ProtoAdapter_Brand;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Card$Brand;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Card$Brand;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum CHINA_UNIONPAY:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum DISCOVER:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum JCB:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum MASTERCARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum OTHER_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public static final enum VISA:Lcom/squareup/protos/connect/v2/resources/Card$Brand;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 496
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v1, 0x0

    const-string v2, "OTHER_BRAND"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->OTHER_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 498
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v2, 0x1

    const-string v3, "VISA"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->VISA:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 500
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v3, 0x2

    const-string v4, "MASTERCARD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->MASTERCARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 502
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v4, 0x3

    const-string v5, "AMERICAN_EXPRESS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 504
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v5, 0x4

    const-string v6, "DISCOVER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 506
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v6, 0x5

    const-string v7, "DISCOVER_DINERS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 508
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v7, 0x6

    const-string v8, "JCB"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->JCB:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 510
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/4 v8, 0x7

    const-string v9, "CHINA_UNIONPAY"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 512
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/16 v9, 0x8

    const-string v10, "SQUARE_GIFT_CARD"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 514
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/16 v10, 0x9

    const-string v11, "SQUARE_CAPITAL_CARD"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/connect/v2/resources/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 495
    sget-object v11, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->OTHER_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->VISA:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->MASTERCARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->JCB:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 516
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand$ProtoAdapter_Brand;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Card$Brand$ProtoAdapter_Brand;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 520
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 521
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Card$Brand;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 538
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 537
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 536
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 535
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->JCB:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 534
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 533
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 532
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 531
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->MASTERCARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 530
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->VISA:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    .line 529
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->OTHER_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Brand;
    .locals 1

    .line 495
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Card$Brand;
    .locals 1

    .line 495
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Card$Brand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 545
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->value:I

    return v0
.end method
