.class public final Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
.super Lcom/squareup/wire/Message;
.source "DeviceCheckoutOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;,
        Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;",
        "Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_SPLIT_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_COLLECT_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_DEVICE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DISPLAY_DIGITAL_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_PRINT_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_split_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final collect_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final device_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final display_digital_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final print_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final skip_receipt_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.TipSettings#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->DEFAULT_DISPLAY_DIGITAL_RECEIPT:Ljava/lang/Boolean;

    .line 29
    sput-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->DEFAULT_PRINT_RECEIPT:Ljava/lang/Boolean;

    .line 31
    sput-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->DEFAULT_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

    .line 33
    sput-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->DEFAULT_COLLECT_SIGNATURE:Ljava/lang/Boolean;

    .line 35
    sput-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->DEFAULT_ALLOW_SPLIT_PAYMENT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/TipSettings;Ljava/lang/Boolean;)V
    .locals 9

    .line 120
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/TipSettings;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/TipSettings;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 126
    sget-object v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    .line 128
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    .line 129
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    .line 130
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    .line 131
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    .line 132
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    .line 133
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 153
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 154
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    .line 162
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/TipSettings;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 177
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->device_id:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->display_digital_receipt:Ljava/lang/Boolean;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->print_receipt:Ljava/lang/Boolean;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->collect_signature:Ljava/lang/Boolean;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->allow_split_payment:Ljava/lang/Boolean;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->newBuilder()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", device_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", display_digital_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", print_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", skip_receipt_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", collect_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    if-eqz v1, :cond_5

    const-string v1, ", tip_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", allow_split_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeviceCheckoutOptions{"

    .line 192
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
