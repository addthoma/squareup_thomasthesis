.class public final Lcom/squareup/protos/agenda/ResponseStatus;
.super Lcom/squareup/wire/Message;
.source "ResponseStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;,
        Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/ResponseStatus;",
        "Lcom/squareup/protos/agenda/ResponseStatus$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/ResponseStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/agenda/StatusType;

.field private static final serialVersionUID:J


# instance fields
.field public final application_errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.ApplicationError#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ApplicationError;",
            ">;"
        }
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/agenda/StatusType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.StatusType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final validation_errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.ValidationError#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ValidationError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/agenda/StatusType;->SUCCESS:Lcom/squareup/protos/agenda/StatusType;

    sput-object v0, Lcom/squareup/protos/agenda/ResponseStatus;->DEFAULT_TYPE:Lcom/squareup/protos/agenda/StatusType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/StatusType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/agenda/StatusType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ValidationError;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ApplicationError;",
            ">;)V"
        }
    .end annotation

    .line 73
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/agenda/ResponseStatus;-><init>(Lcom/squareup/protos/agenda/StatusType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/StatusType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/agenda/StatusType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ValidationError;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ApplicationError;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 79
    sget-object v0, Lcom/squareup/protos/agenda/ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 80
    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    .line 81
    iput-object p2, p0, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    const-string/jumbo p1, "validation_errors"

    .line 82
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    const-string p1, "application_errors"

    .line 83
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 100
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/ResponseStatus;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 101
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/ResponseStatus;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ResponseStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ResponseStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    .line 105
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    .line 106
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 111
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ResponseStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/StatusType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->type:Lcom/squareup/protos/agenda/StatusType;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->error_message:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->validation_errors:Ljava/util/List;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->application_errors:Ljava/util/List;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ResponseStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ResponseStatus;->newBuilder()Lcom/squareup/protos/agenda/ResponseStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", error_message=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", validation_errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", application_errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ResponseStatus{"

    .line 130
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
