.class public final Lcom/squareup/protos/agenda/ClientMessage$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ClientMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/ClientMessage;",
        "Lcom/squareup/protos/agenda/ClientMessage$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public custom_client_message_body:Ljava/lang/String;

.field public send_custom_client_message_body_to_client:Ljava/lang/Boolean;

.field public send_email:Ljava/lang/Boolean;

.field public send_sms:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/ClientMessage;
    .locals 7

    .line 195
    new-instance v6, Lcom/squareup/protos/agenda/ClientMessage;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->custom_client_message_body:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_sms:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_email:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/agenda/ClientMessage;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientMessage$Builder;->build()Lcom/squareup/protos/agenda/ClientMessage;

    move-result-object v0

    return-object v0
.end method

.method public custom_client_message_body(Ljava/lang/String;)Lcom/squareup/protos/agenda/ClientMessage$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->custom_client_message_body:Ljava/lang/String;

    return-object p0
.end method

.method public send_custom_client_message_body_to_client(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/ClientMessage$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    return-object p0
.end method

.method public send_email(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/ClientMessage$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_email:Ljava/lang/Boolean;

    return-object p0
.end method

.method public send_sms(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/ClientMessage$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_sms:Ljava/lang/Boolean;

    return-object p0
.end method
