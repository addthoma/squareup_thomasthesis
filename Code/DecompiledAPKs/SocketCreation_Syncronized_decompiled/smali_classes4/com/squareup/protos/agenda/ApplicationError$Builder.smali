.class public final Lcom/squareup/protos/agenda/ApplicationError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ApplicationError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ApplicationError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/ApplicationError;",
        "Lcom/squareup/protos/agenda/ApplicationError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public error_type:Lcom/squareup/protos/agenda/ApplicationError$ErrorType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/ApplicationError;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/agenda/ApplicationError;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ApplicationError$Builder;->error_type:Lcom/squareup/protos/agenda/ApplicationError$ErrorType;

    iget-object v2, p0, Lcom/squareup/protos/agenda/ApplicationError$Builder;->description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/agenda/ApplicationError;-><init>(Lcom/squareup/protos/agenda/ApplicationError$ErrorType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ApplicationError$Builder;->build()Lcom/squareup/protos/agenda/ApplicationError;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/agenda/ApplicationError$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/agenda/ApplicationError$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public error_type(Lcom/squareup/protos/agenda/ApplicationError$ErrorType;)Lcom/squareup/protos/agenda/ApplicationError$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/agenda/ApplicationError$Builder;->error_type:Lcom/squareup/protos/agenda/ApplicationError$ErrorType;

    return-object p0
.end method
