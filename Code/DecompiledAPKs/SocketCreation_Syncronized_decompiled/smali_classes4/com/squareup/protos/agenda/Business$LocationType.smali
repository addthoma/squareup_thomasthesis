.class public final enum Lcom/squareup/protos/agenda/Business$LocationType;
.super Ljava/lang/Enum;
.source "Business.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Business;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LocationType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/Business$LocationType$ProtoAdapter_LocationType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/agenda/Business$LocationType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/agenda/Business$LocationType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/Business$LocationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CUSTOMER:Lcom/squareup/protos/agenda/Business$LocationType;

.field public static final enum OTHER:Lcom/squareup/protos/agenda/Business$LocationType;

.field public static final enum PHONE:Lcom/squareup/protos/agenda/Business$LocationType;

.field public static final enum PREMISES:Lcom/squareup/protos/agenda/Business$LocationType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 702
    new-instance v0, Lcom/squareup/protos/agenda/Business$LocationType;

    const/4 v1, 0x0

    const-string v2, "PREMISES"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/agenda/Business$LocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->PREMISES:Lcom/squareup/protos/agenda/Business$LocationType;

    .line 707
    new-instance v0, Lcom/squareup/protos/agenda/Business$LocationType;

    const/4 v2, 0x1

    const-string v3, "CUSTOMER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/agenda/Business$LocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->CUSTOMER:Lcom/squareup/protos/agenda/Business$LocationType;

    .line 712
    new-instance v0, Lcom/squareup/protos/agenda/Business$LocationType;

    const/4 v3, 0x2

    const-string v4, "PHONE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/agenda/Business$LocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->PHONE:Lcom/squareup/protos/agenda/Business$LocationType;

    .line 717
    new-instance v0, Lcom/squareup/protos/agenda/Business$LocationType;

    const/4 v4, 0x3

    const-string v5, "OTHER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/agenda/Business$LocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->OTHER:Lcom/squareup/protos/agenda/Business$LocationType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/agenda/Business$LocationType;

    .line 698
    sget-object v5, Lcom/squareup/protos/agenda/Business$LocationType;->PREMISES:Lcom/squareup/protos/agenda/Business$LocationType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/Business$LocationType;->CUSTOMER:Lcom/squareup/protos/agenda/Business$LocationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/Business$LocationType;->PHONE:Lcom/squareup/protos/agenda/Business$LocationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/agenda/Business$LocationType;->OTHER:Lcom/squareup/protos/agenda/Business$LocationType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->$VALUES:[Lcom/squareup/protos/agenda/Business$LocationType;

    .line 719
    new-instance v0, Lcom/squareup/protos/agenda/Business$LocationType$ProtoAdapter_LocationType;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Business$LocationType$ProtoAdapter_LocationType;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 723
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 724
    iput p3, p0, Lcom/squareup/protos/agenda/Business$LocationType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/agenda/Business$LocationType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 735
    :cond_0
    sget-object p0, Lcom/squareup/protos/agenda/Business$LocationType;->OTHER:Lcom/squareup/protos/agenda/Business$LocationType;

    return-object p0

    .line 734
    :cond_1
    sget-object p0, Lcom/squareup/protos/agenda/Business$LocationType;->PHONE:Lcom/squareup/protos/agenda/Business$LocationType;

    return-object p0

    .line 733
    :cond_2
    sget-object p0, Lcom/squareup/protos/agenda/Business$LocationType;->CUSTOMER:Lcom/squareup/protos/agenda/Business$LocationType;

    return-object p0

    .line 732
    :cond_3
    sget-object p0, Lcom/squareup/protos/agenda/Business$LocationType;->PREMISES:Lcom/squareup/protos/agenda/Business$LocationType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$LocationType;
    .locals 1

    .line 698
    const-class v0, Lcom/squareup/protos/agenda/Business$LocationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/agenda/Business$LocationType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/agenda/Business$LocationType;
    .locals 1

    .line 698
    sget-object v0, Lcom/squareup/protos/agenda/Business$LocationType;->$VALUES:[Lcom/squareup/protos/agenda/Business$LocationType;

    invoke-virtual {v0}, [Lcom/squareup/protos/agenda/Business$LocationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/agenda/Business$LocationType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 742
    iget v0, p0, Lcom/squareup/protos/agenda/Business$LocationType;->value:I

    return v0
.end method
