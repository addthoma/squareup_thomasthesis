.class public final Lcom/squareup/protos/agenda/ResponseStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ResponseStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ResponseStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/ResponseStatus;",
        "Lcom/squareup/protos/agenda/ResponseStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ApplicationError;",
            ">;"
        }
    .end annotation
.end field

.field public error_message:Ljava/lang/String;

.field public type:Lcom/squareup/protos/agenda/StatusType;

.field public validation_errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ValidationError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 142
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 143
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->validation_errors:Ljava/util/List;

    .line 144
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->application_errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public application_errors(Ljava/util/List;)Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ApplicationError;",
            ">;)",
            "Lcom/squareup/protos/agenda/ResponseStatus$Builder;"
        }
    .end annotation

    .line 176
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->application_errors:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/agenda/ResponseStatus;
    .locals 7

    .line 183
    new-instance v6, Lcom/squareup/protos/agenda/ResponseStatus;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->type:Lcom/squareup/protos/agenda/StatusType;

    iget-object v2, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->error_message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->validation_errors:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->application_errors:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/agenda/ResponseStatus;-><init>(Lcom/squareup/protos/agenda/StatusType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->build()Lcom/squareup/protos/agenda/ResponseStatus;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/agenda/StatusType;)Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->type:Lcom/squareup/protos/agenda/StatusType;

    return-object p0
.end method

.method public validation_errors(Ljava/util/List;)Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/ValidationError;",
            ">;)",
            "Lcom/squareup/protos/agenda/ResponseStatus$Builder;"
        }
    .end annotation

    .line 167
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->validation_errors:Ljava/util/List;

    return-object p0
.end method
