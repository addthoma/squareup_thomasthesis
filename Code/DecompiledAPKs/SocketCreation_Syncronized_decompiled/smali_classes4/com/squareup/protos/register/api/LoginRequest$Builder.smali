.class public final Lcom/squareup/protos/register/api/LoginRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoginRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/LoginRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "Lcom/squareup/protos/register/api/LoginRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public captcha_response:Ljava/lang/String;

.field public device_code:Ljava/lang/String;

.field public devicecheck_token:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public idempotence_token:Ljava/lang/String;

.field public password:Ljava/lang/String;

.field public phone:Ljava/lang/String;

.field public trusted_device_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;"
        }
    .end annotation
.end field

.field public verification_code:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 253
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 254
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->trusted_device_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/LoginRequest;
    .locals 12

    .line 347
    new-instance v11, Lcom/squareup/protos/register/api/LoginRequest;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->password:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->verification_code:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->trusted_device_details:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->captcha_response:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->devicecheck_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->idempotence_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->phone:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/register/api/LoginRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object v0

    return-object v0
.end method

.method public captcha_response(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->captcha_response:Ljava/lang/String;

    return-object p0
.end method

.method public device_code(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 339
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code:Ljava/lang/String;

    const/4 p1, 0x0

    .line 340
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email:Ljava/lang/String;

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method

.method public devicecheck_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->devicecheck_token:Ljava/lang/String;

    return-object p0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email:Ljava/lang/String;

    const/4 p1, 0x0

    .line 320
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->phone:Ljava/lang/String;

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code:Ljava/lang/String;

    return-object p0
.end method

.method public idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->idempotence_token:Ljava/lang/String;

    return-object p0
.end method

.method public password(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->password:Ljava/lang/String;

    return-object p0
.end method

.method public phone(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->phone:Ljava/lang/String;

    const/4 p1, 0x0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email:Ljava/lang/String;

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code:Ljava/lang/String;

    return-object p0
.end method

.method public trusted_device_details(Ljava/util/List;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;)",
            "Lcom/squareup/protos/register/api/LoginRequest$Builder;"
        }
    .end annotation

    .line 279
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->trusted_device_details:Ljava/util/List;

    return-object p0
.end method

.method public verification_code(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->verification_code:Ljava/lang/String;

    return-object p0
.end method
