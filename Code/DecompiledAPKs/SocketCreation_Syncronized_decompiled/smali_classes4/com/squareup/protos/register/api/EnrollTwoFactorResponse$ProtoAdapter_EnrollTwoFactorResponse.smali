.class final Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EnrollTwoFactorResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EnrollTwoFactorResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 239
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 266
    new-instance v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;-><init>()V

    .line 267
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 268
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 277
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 275
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    goto :goto_0

    .line 274
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    goto :goto_0

    .line 273
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    goto :goto_0

    .line 272
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    goto :goto_0

    .line 271
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    goto :goto_0

    .line 270
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->complete(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    goto :goto_0

    .line 281
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 282
    invoke-virtual {v0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->build()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 237
    invoke-virtual {p0, p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 255
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 256
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 257
    sget-object v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 258
    sget-object v0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 259
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 260
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 261
    invoke-virtual {p2}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 237
    check-cast p2, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)I
    .locals 4

    .line 244
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 245
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    const/4 v3, 0x3

    .line 246
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    const/4 v3, 0x6

    .line 247
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    const/4 v3, 0x4

    .line 248
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x5

    .line 249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 237
    check-cast p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;->encodedSize(Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;
    .locals 2

    .line 287
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->newBuilder()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 288
    iput-object v0, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->session_token:Ljava/lang/String;

    .line 289
    iget-object v0, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iput-object v0, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 290
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    iput-object v0, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    .line 291
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 292
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->build()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 237
    check-cast p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;->redact(Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;)Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    move-result-object p1

    return-object p1
.end method
