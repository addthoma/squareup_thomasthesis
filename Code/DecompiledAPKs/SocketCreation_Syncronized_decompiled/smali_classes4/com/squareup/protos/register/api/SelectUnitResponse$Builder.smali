.class public final Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SelectUnitResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/SelectUnitResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/SelectUnitResponse;",
        "Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public session_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/SelectUnitResponse;
    .locals 5

    .line 157
    new-instance v0, Lcom/squareup/protos/register/api/SelectUnitResponse;

    iget-object v1, p0, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/register/api/SelectUnitResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->build()Lcom/squareup/protos/register/api/SelectUnitResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/register/api/SelectUnitResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method
