.class public final Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;
.super Lcom/squareup/wire/Message;
.source "SubscriptionFeatureStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$ProtoAdapter_SubscriptionFeatureStatus;,
        Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;",
        "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FEATURE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final DEFAULT_SUBSCRIPTION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final feature:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.sub2.common.SubscriptionStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final subscription_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$ProtoAdapter_SubscriptionFeatureStatus;

    invoke-direct {v0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$ProtoAdapter_SubscriptionFeatureStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->INVALID:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    sput-object v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->DEFAULT_STATUS:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/sub2/common/SubscriptionStatus;Ljava/lang/String;)V
    .locals 1

    .line 62
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;-><init>(Ljava/lang/String;Lcom/squareup/protos/sub2/common/SubscriptionStatus;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/sub2/common/SubscriptionStatus;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 70
    iput-object p3, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 86
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    iget-object v3, p1, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    .line 91
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 96
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 102
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->feature:Ljava/lang/String;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    iput-object v1, v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->subscription_token:Ljava/lang/String;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->newBuilder()Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", feature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->feature:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    if-eqz v1, :cond_1

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", subscription_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;->subscription_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SubscriptionFeatureStatus{"

    .line 113
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
