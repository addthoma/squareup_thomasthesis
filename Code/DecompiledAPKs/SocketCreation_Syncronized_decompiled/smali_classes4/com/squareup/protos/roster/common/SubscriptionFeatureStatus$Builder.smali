.class public final Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SubscriptionFeatureStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;",
        "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public feature:Ljava/lang/String;

.field public status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public subscription_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;

    iget-object v1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->feature:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    iget-object v3, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->subscription_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;-><init>(Ljava/lang/String;Lcom/squareup/protos/sub2/common/SubscriptionStatus;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->build()Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;

    move-result-object v0

    return-object v0
.end method

.method public feature(Ljava/lang/String;)Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->feature:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/sub2/common/SubscriptionStatus;)Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->status:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0
.end method

.method public subscription_token(Ljava/lang/String;)Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus$Builder;->subscription_token:Ljava/lang/String;

    return-object p0
.end method
