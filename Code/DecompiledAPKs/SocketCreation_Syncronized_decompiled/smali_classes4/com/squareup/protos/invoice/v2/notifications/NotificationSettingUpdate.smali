.class public final Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;
.super Lcom/squareup/wire/Message;
.source "NotificationSettingUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$ProtoAdapter_NotificationSettingUpdate;,
        Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATEGORY:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL_SETTING:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

.field public static final DEFAULT_GROUP:Ljava/lang/String; = ""

.field public static final DEFAULT_PUSH_SETTING:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

.field private static final serialVersionUID:J


# instance fields
.field public final category:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.invoice.v2.notifications.PreferenceSetting#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final group:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.invoice.v2.notifications.PreferenceSetting#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$ProtoAdapter_NotificationSettingUpdate;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$ProtoAdapter_NotificationSettingUpdate;-><init>()V

    sput-object v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->DO_NOT_USE:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    sput-object v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->DEFAULT_EMAIL_SETTING:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    .line 30
    sget-object v0, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->DO_NOT_USE:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    sput-object v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->DEFAULT_PUSH_SETTING:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)V
    .locals 6

    .line 58
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lokio/ByteString;)V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    .line 67
    iput-object p4, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    iget-object v3, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    iget-object p1, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 102
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->group:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->category:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->newBuilder()Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->group:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    if-eqz v1, :cond_2

    const-string v1, ", email_setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    if-eqz v1, :cond_3

    const-string v1, ", push_setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "NotificationSettingUpdate{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
