.class public final Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateNotificationSettingsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;",
        "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employment_token:Ljava/lang/String;

.field public setting_update:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 112
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->setting_update:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;
    .locals 5

    .line 133
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->employment_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->setting_update:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;

    move-result-object v0

    return-object v0
.end method

.method public employment_token(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->employment_token:Ljava/lang/String;

    return-object p0
.end method

.method public setting_update(Ljava/util/List;)Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;",
            ">;)",
            "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;"
        }
    .end annotation

    .line 126
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->setting_update:Ljava/util/List;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
