.class public final Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetAllNotificationSettingsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;",
        "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employment_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;->employment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;

    move-result-object v0

    return-object v0
.end method

.method public employment_token(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;->employment_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
