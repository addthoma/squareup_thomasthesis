.class public final Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationSettingUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Ljava/lang/String;

.field public email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

.field public group:Ljava/lang/String;

.field public push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;
    .locals 7

    .line 151
    new-instance v6, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->group:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->category:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    iget-object v4, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;

    move-result-object v0

    return-object v0
.end method

.method public category(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->category:Ljava/lang/String;

    return-object p0
.end method

.method public email_setting(Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    return-object p0
.end method

.method public group(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->group:Ljava/lang/String;

    return-object p0
.end method

.method public push_setting(Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate$Builder;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    return-object p0
.end method
