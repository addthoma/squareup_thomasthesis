.class public final Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CountrySpecificDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;",
        "Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

.field public ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

.field public fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

.field public gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

.field public jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

.field public us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

.field public vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 186
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public au_account(Lcom/squareup/protos/bank_accounts/AuAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    const/4 p1, 0x0

    .line 241
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;
    .locals 10

    .line 277
    new-instance v9, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iget-object v2, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    iget-object v3, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    iget-object v4, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    iget-object v5, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    iget-object v6, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    iget-object v7, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;-><init>(Lcom/squareup/protos/common/privacyvault/VaultedData;Lcom/squareup/protos/bank_accounts/UsAccount;Lcom/squareup/protos/bank_accounts/CaAccount;Lcom/squareup/protos/bank_accounts/JpAccount;Lcom/squareup/protos/bank_accounts/AuAccount;Lcom/squareup/protos/bank_accounts/GbAccount;Lcom/squareup/protos/bank_accounts/FrAccount;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->build()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    move-result-object v0

    return-object v0
.end method

.method public ca_account(Lcom/squareup/protos/bank_accounts/CaAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    const/4 p1, 0x0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    return-object p0
.end method

.method public fr_account(Lcom/squareup/protos/bank_accounts/FrAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    const/4 p1, 0x0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 268
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    return-object p0
.end method

.method public gb_account(Lcom/squareup/protos/bank_accounts/GbAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    const/4 p1, 0x0

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 258
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    return-object p0
.end method

.method public jp_account(Lcom/squareup/protos/bank_accounts/JpAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    const/4 p1, 0x0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    return-object p0
.end method

.method public us_account(Lcom/squareup/protos/bank_accounts/UsAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    const/4 p1, 0x0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    return-object p0
.end method

.method public vaulted_data(Lcom/squareup/protos/common/privacyvault/VaultedData;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    return-object p0
.end method
