.class final Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CountrySpecificDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CountrySpecificDetail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 283
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 312
    new-instance v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;-><init>()V

    .line 313
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 314
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 324
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 322
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data(Lcom/squareup/protos/common/privacyvault/VaultedData;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 321
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/bank_accounts/FrAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account(Lcom/squareup/protos/bank_accounts/FrAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 320
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/bank_accounts/GbAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account(Lcom/squareup/protos/bank_accounts/GbAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 319
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/bank_accounts/AuAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/AuAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account(Lcom/squareup/protos/bank_accounts/AuAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 318
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/bank_accounts/JpAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/JpAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account(Lcom/squareup/protos/bank_accounts/JpAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 317
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/bank_accounts/CaAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/CaAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account(Lcom/squareup/protos/bank_accounts/CaAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 316
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/bank_accounts/UsAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/UsAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account(Lcom/squareup/protos/bank_accounts/UsAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    goto :goto_0

    .line 328
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 329
    invoke-virtual {v0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->build()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 300
    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 301
    sget-object v0, Lcom/squareup/protos/bank_accounts/UsAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 302
    sget-object v0, Lcom/squareup/protos/bank_accounts/CaAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 303
    sget-object v0, Lcom/squareup/protos/bank_accounts/JpAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/protos/bank_accounts/AuAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    sget-object v0, Lcom/squareup/protos/bank_accounts/GbAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 306
    sget-object v0, Lcom/squareup/protos/bank_accounts/FrAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    invoke-virtual {p2}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    check-cast p2, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)I
    .locals 4

    .line 288
    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/bank_accounts/UsAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    const/4 v3, 0x1

    .line 289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bank_accounts/CaAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    const/4 v3, 0x2

    .line 290
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bank_accounts/JpAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    const/4 v3, 0x3

    .line 291
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bank_accounts/AuAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    const/4 v3, 0x4

    .line 292
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bank_accounts/GbAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    const/4 v3, 0x5

    .line 293
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bank_accounts/FrAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    const/4 v3, 0x6

    .line 294
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;->encodedSize(Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;
    .locals 2

    .line 334
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->newBuilder()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    move-result-object p1

    .line 335
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 336
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/bank_accounts/UsAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/UsAccount;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 337
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/bank_accounts/CaAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/CaAccount;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 338
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/bank_accounts/JpAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/JpAccount;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 339
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/bank_accounts/AuAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/AuAccount;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 340
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/bank_accounts/GbAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/GbAccount;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 341
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/bank_accounts/FrAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/FrAccount;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    .line 342
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 343
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->build()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;->redact(Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    move-result-object p1

    return-object p1
.end method
