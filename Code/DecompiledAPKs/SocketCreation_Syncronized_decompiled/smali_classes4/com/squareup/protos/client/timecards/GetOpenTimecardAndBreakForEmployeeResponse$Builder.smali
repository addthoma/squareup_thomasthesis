.class public final Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetOpenTimecardAndBreakForEmployeeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;",
        "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public clocked_in_current_workday:Ljava/lang/Boolean;

.field public timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

.field public total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 136
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;
    .locals 7

    .line 173
    new-instance v6, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->clocked_in_current_workday:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    move-result-object v0

    return-object v0
.end method

.method public clocked_in_current_workday(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->clocked_in_current_workday:Ljava/lang/Boolean;

    return-object p0
.end method

.method public timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    return-object p0
.end method

.method public total_seconds_clocked_in_for_current_workday(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    return-object p0
.end method
