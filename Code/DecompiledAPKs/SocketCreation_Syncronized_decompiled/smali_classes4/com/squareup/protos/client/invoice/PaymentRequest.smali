.class public final Lcom/squareup/protos/client/invoice/PaymentRequest;
.super Lcom/squareup/wire/Message;
.source "PaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/PaymentRequest$ProtoAdapter_PaymentRequest;,
        Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;,
        Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMOUNT_TYPE:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

.field public static final DEFAULT_INSTRUMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final DEFAULT_PERCENTAGE_AMOUNT:Ljava/lang/Long;

.field public static final DEFAULT_RELATIVE_DUE_ON:Ljava/lang/Long;

.field public static final DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.PaymentRequest$AmountType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final fixed_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final instrument_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.Invoice$PaymentMethod#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final percentage_amount:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x7
    .end annotation
.end field

.field public final relative_due_on:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final reminders:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceReminderInstance#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation
.end field

.field public final tipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequest$ProtoAdapter_PaymentRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$ProtoAdapter_PaymentRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 32
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->DEFAULT_RELATIVE_DUE_ON:Ljava/lang/Long;

    .line 34
    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    sput-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 38
    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sput-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->DEFAULT_AMOUNT_TYPE:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 40
    sput-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->DEFAULT_PERCENTAGE_AMOUNT:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 42
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;)V"
        }
    .end annotation

    .line 143
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/invoice/PaymentRequest;-><init>(Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/String;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/Money;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 150
    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    .line 152
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    .line 153
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 154
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    .line 155
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 156
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 157
    iput-object p7, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    .line 158
    iput-object p8, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    .line 159
    iput-object p9, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    const-string p1, "reminders"

    .line 160
    invoke-static {p1, p10}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 183
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 184
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 185
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    .line 195
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 200
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 202
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 2

    .line 165
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;-><init>()V

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->token:Ljava/lang/String;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->instrument_token:Ljava/lang/String;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->name:Ljava/lang/String;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest;->newBuilder()Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", relative_due_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_2

    const-string v1, ", payment_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", instrument_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->instrument_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v1, :cond_4

    const-string v1, ", amount_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 226
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", fixed_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 227
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", percentage_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 228
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", tipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 229
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", reminders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentRequest{"

    .line 231
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
