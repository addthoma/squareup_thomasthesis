.class public final Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CancelEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/CancelEstimateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/CancelEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public send_email_to_recipients:Ljava/lang/Boolean;

.field public token_pair:Lcom/squareup/protos/client/IdPair;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/CancelEstimateRequest;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->version:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    move-result-object v0

    return-object v0
.end method

.method public send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
