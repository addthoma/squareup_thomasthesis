.class public final enum Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Authorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeclineReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason$ProtoAdapter_DeclineReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CHIP_CARD_INSERTION_REQUIRED:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

.field public static final enum DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1189
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1191
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    const/4 v2, 0x1

    const-string v3, "CHIP_CARD_INSERTION_REQUIRED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->CHIP_CARD_INSERTION_REQUIRED:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1188
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->CHIP_CARD_INSERTION_REQUIRED:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1193
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason$ProtoAdapter_DeclineReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason$ProtoAdapter_DeclineReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1198
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1207
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->CHIP_CARD_INSERTION_REQUIRED:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    return-object p0

    .line 1206
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;
    .locals 1

    .line 1188
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;
    .locals 1

    .line 1188
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1214
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->value:I

    return v0
.end method
