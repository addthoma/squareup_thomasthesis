.class public final Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LinkCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

.field public card_data:Lcom/squareup/protos/client/bills/CardData;

.field public merchant_token:Ljava/lang/String;

.field public request_uuid:Ljava/lang/String;

.field public skip_verification_email:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authentication_detail(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
    .locals 8

    .line 207
    new-instance v7, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->request_uuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v3, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->skip_verification_email:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    iget-object v5, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->merchant_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData;Ljava/lang/Boolean;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public skip_verification_email(Ljava/lang/Boolean;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->skip_verification_email:Ljava/lang/Boolean;

    return-object p0
.end method
