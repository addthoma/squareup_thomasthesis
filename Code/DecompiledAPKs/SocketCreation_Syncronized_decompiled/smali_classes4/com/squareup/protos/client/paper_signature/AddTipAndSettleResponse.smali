.class public final Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;
.super Lcom/squareup/wire/Message;
.source "AddTipAndSettleResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$ProtoAdapter_AddTipAndSettleResponse;,
        Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PROCESSING_STATUS:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

.field public static final DEFAULT_TENDER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.paper_signature.TenderAwaitingMerchantTipProcessingStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final tender_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$ProtoAdapter_AddTipAndSettleResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$ProtoAdapter_AddTipAndSettleResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->QUEUED_FOR_PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    sput-object v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->DEFAULT_PROCESSING_STATUS:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->tender_id:Ljava/lang/String;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    if-eqz v1, :cond_1

    const-string v1, ", processing_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AddTipAndSettleResponse{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
