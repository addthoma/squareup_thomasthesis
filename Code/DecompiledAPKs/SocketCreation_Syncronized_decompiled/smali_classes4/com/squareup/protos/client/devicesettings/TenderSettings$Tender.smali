.class public final Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
.super Lcom/squareup/wire/Message;
.source "TenderSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/TenderSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tender"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$ProtoAdapter_Tender;,
        Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LEGACY_OTHER_TENDER_TYPE_ID:Ljava/lang/Integer;

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field private static final serialVersionUID:J


# instance fields
.field public final legacy_other_tender_type_id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.devicesettings.TenderSettings$TenderType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 218
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$ProtoAdapter_Tender;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$ProtoAdapter_Tender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 222
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->UNKNOWN:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v0, 0x0

    .line 224
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->DEFAULT_LEGACY_OTHER_TENDER_TYPE_ID:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;)V
    .locals 1

    .line 242
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 247
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 249
    iput-object p2, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 264
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 265
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 266
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    iget-object v3, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    .line 268
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 273
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 275
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 277
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 278
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;
    .locals 2

    .line 254
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;-><init>()V

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    iput-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->legacy_other_tender_type_id:Ljava/lang/Integer;

    .line 257
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 217
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->newBuilder()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-eqz v1, :cond_0

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", legacy_other_tender_type_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tender{"

    .line 288
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
