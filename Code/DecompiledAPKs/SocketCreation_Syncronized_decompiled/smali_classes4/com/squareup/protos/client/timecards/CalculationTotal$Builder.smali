.class public final Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CalculationTotal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/CalculationTotal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/CalculationTotal;",
        "Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public doubletime_seconds:Ljava/lang/Integer;

.field public overtime_seconds:Ljava/lang/Integer;

.field public regular_seconds:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/CalculationTotal;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/client/timecards/CalculationTotal;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->regular_seconds:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->overtime_seconds:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->doubletime_seconds:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/CalculationTotal;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->build()Lcom/squareup/protos/client/timecards/CalculationTotal;

    move-result-object v0

    return-object v0
.end method

.method public doubletime_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->doubletime_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public overtime_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->overtime_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public regular_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->regular_seconds:Ljava/lang/Integer;

    return-object p0
.end method
