.class final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Response"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 307
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 332
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;-><init>()V

    .line 333
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 334
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 356
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 354
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_retries_remaining(Ljava/lang/Integer;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    goto :goto_0

    .line 348
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_error(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 350
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 345
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->can_retry_idv(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    goto :goto_0

    .line 344
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_needs_ssn(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    goto :goto_0

    .line 338
    :cond_4
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bizbank/IdvState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/IdvState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_state(Lcom/squareup/protos/client/bizbank/IdvState;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 340
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 360
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 361
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 305
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 322
    sget-object v0, Lcom/squareup/protos/client/bizbank/IdvState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 323
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 324
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 325
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 326
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 327
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 305
    check-cast p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)I
    .locals 4

    .line 312
    sget-object v0, Lcom/squareup/protos/client/bizbank/IdvState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 313
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 314
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 315
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v3, 0x4

    .line 316
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 305
    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;->encodedSize(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
    .locals 0

    .line 366
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->newBuilder()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    move-result-object p1

    .line 367
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 368
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 305
    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;->redact(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    move-result-object p1

    return-object p1
.end method
