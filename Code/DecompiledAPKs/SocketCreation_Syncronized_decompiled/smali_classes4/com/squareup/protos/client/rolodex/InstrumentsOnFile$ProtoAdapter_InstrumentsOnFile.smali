.class final Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$ProtoAdapter_InstrumentsOnFile;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstrumentsOnFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InstrumentsOnFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 104
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    new-instance v0, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;-><init>()V

    .line 122
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 123
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 127
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 125
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->instrument:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/instruments/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 132
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->build()Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$ProtoAdapter_InstrumentsOnFile;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    sget-object v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 116
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    check-cast p2, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$ProtoAdapter_InstrumentsOnFile;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)I
    .locals 3

    .line 109
    sget-object v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 110
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 102
    check-cast p1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$ProtoAdapter_InstrumentsOnFile;->encodedSize(Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;
    .locals 2

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->newBuilder()Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;

    move-result-object p1

    .line 138
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->instrument:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->build()Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 102
    check-cast p1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$ProtoAdapter_InstrumentsOnFile;->redact(Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    move-result-object p1

    return-object p1
.end method
