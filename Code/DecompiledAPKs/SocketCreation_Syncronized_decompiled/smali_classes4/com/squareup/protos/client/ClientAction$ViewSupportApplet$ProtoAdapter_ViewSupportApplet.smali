.class final Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$ProtoAdapter_ViewSupportApplet;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewSupportApplet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3425
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3440
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;-><init>()V

    .line 3441
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3442
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 3445
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3449
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3450
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3423
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$ProtoAdapter_ViewSupportApplet;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3435
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3423
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$ProtoAdapter_ViewSupportApplet;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)I
    .locals 0

    .line 3430
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3423
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$ProtoAdapter_ViewSupportApplet;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;
    .locals 0

    .line 3455
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;

    move-result-object p1

    .line 3456
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3457
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3423
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet$ProtoAdapter_ViewSupportApplet;->redact(Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    move-result-object p1

    return-object p1
.end method
