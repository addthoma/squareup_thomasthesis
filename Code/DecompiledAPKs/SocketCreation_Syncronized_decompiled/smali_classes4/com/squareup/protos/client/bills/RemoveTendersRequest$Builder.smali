.class public final Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RemoveTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RemoveTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest;",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public merchant:Lcom/squareup/protos/client/Merchant;

.field public tenders_to_remove:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 117
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->tenders_to_remove:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/RemoveTendersRequest;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->tenders_to_remove:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    move-result-object v0

    return-object v0
.end method

.method public merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    return-object p0
.end method

.method public tenders_to_remove(Ljava/util/List;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;"
        }
    .end annotation

    .line 138
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->tenders_to_remove:Ljava/util/List;

    return-object p0
.end method
