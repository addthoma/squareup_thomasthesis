.class public final Lcom/squareup/protos/client/MobileStaff$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MobileStaff.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/MobileStaff;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/MobileStaff;",
        "Lcom/squareup/protos/client/MobileStaff$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public full_name:Ljava/lang/String;

.field public photo_url:Ljava/lang/String;

.field public user_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/MobileStaff;
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/protos/client/MobileStaff;

    iget-object v1, p0, Lcom/squareup/protos/client/MobileStaff$Builder;->user_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/MobileStaff$Builder;->photo_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/MobileStaff$Builder;->full_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/MobileStaff;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/MobileStaff$Builder;->build()Lcom/squareup/protos/client/MobileStaff;

    move-result-object v0

    return-object v0
.end method

.method public full_name(Ljava/lang/String;)Lcom/squareup/protos/client/MobileStaff$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/MobileStaff$Builder;->full_name:Ljava/lang/String;

    return-object p0
.end method

.method public photo_url(Ljava/lang/String;)Lcom/squareup/protos/client/MobileStaff$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/MobileStaff$Builder;->photo_url:Ljava/lang/String;

    return-object p0
.end method

.method public user_token(Ljava/lang/String;)Lcom/squareup/protos/client/MobileStaff$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/MobileStaff$Builder;->user_token:Ljava/lang/String;

    return-object p0
.end method
