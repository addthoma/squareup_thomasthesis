.class public final Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateAttributeSchemaRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_definitions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public attribute_keys_in_order:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public schema_version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 119
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_definitions:Ljava/util/List;

    .line 120
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_keys_in_order:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_definitions(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;"
        }
    .end annotation

    .line 133
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_definitions:Ljava/util/List;

    return-object p0
.end method

.method public attribute_keys_in_order(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;"
        }
    .end annotation

    .line 142
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_keys_in_order:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;
    .locals 5

    .line 149
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->schema_version:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_definitions:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_keys_in_order:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;

    move-result-object v0

    return-object v0
.end method

.method public schema_version(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->schema_version:Ljava/lang/String;

    return-object p0
.end method
