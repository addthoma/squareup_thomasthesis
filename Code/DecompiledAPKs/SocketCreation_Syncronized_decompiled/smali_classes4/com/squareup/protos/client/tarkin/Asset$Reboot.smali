.class public final enum Lcom/squareup/protos/client/tarkin/Asset$Reboot;
.super Ljava/lang/Enum;
.source "Asset.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/Asset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reboot"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/Asset$Reboot$ProtoAdapter_Reboot;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/tarkin/Asset$Reboot;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/Asset$Reboot;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUDIO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field public static final enum BLE:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field public static final enum NO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field public static final enum USB:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field public static final enum YES:Lcom/squareup/protos/client/tarkin/Asset$Reboot;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 218
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v1, 0x0

    const-string v2, "NO"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->NO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 220
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v2, 0x1

    const-string v3, "YES"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->YES:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 222
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v3, 0x2

    const-string v4, "AUDIO"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->AUDIO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 224
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v4, 0x3

    const-string v5, "USB"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->USB:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 226
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v5, 0x4

    const-string v6, "BLE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->BLE:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 217
    sget-object v6, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->NO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->YES:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->AUDIO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->USB:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->BLE:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->$VALUES:[Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 228
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot$ProtoAdapter_Reboot;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/Asset$Reboot$ProtoAdapter_Reboot;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 233
    iput p3, p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/tarkin/Asset$Reboot;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 245
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->BLE:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0

    .line 244
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->USB:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0

    .line 243
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->AUDIO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0

    .line 242
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->YES:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0

    .line 241
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->NO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/Asset$Reboot;
    .locals 1

    .line 217
    const-class v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/tarkin/Asset$Reboot;
    .locals 1

    .line 217
    sget-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->$VALUES:[Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/tarkin/Asset$Reboot;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 252
    iget v0, p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->value:I

    return v0
.end method
