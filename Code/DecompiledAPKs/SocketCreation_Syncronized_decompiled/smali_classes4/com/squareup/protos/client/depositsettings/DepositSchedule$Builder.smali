.class public final Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DepositSchedule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/DepositSchedule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/DepositSchedule;",
        "Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public effective_at:Lcom/squareup/protos/common/time/DateTime;

.field public interval:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DepositInterval;",
            ">;"
        }
    .end annotation
.end field

.field public timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->interval:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/DepositSchedule;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->interval:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->timezone:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/depositsettings/DepositSchedule;-><init>(Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->build()Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    move-result-object v0

    return-object v0
.end method

.method public effective_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public interval(Ljava/util/List;)Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DepositInterval;",
            ">;)",
            "Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;"
        }
    .end annotation

    .line 142
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->interval:Ljava/util/List;

    return-object p0
.end method

.method public timezone(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DepositSchedule$Builder;->timezone:Ljava/lang/String;

    return-object p0
.end method
