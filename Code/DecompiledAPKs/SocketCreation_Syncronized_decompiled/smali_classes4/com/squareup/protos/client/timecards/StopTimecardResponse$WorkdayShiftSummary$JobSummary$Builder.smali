.class public final Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

.field public paid_seconds:Ljava/lang/Long;

.field public start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

.field public stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

.field public timecard_notes:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 412
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;
    .locals 8

    .line 442
    new-instance v7, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->paid_seconds:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->timecard_notes:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;-><init>(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 401
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    move-result-object v0

    return-object v0
.end method

.method public job_info(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    return-object p0
.end method

.method public paid_seconds(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;
    .locals 0

    .line 431
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->paid_seconds:Ljava/lang/Long;

    return-object p0
.end method

.method public start_zoned_date_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public stop_zoned_date_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public timecard_notes(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;
    .locals 0

    .line 436
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->timecard_notes:Ljava/lang/String;

    return-object p0
.end method
