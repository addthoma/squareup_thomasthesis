.class public final Lcom/squareup/protos/client/bills/Refund;
.super Lcom/squareup/wire/Message;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Refund$ProtoAdapter_Refund;,
        Lcom/squareup/protos/client/bills/Refund$Destination;,
        Lcom/squareup/protos/client/bills/Refund$ReasonOption;,
        Lcom/squareup/protos/client/bills/Refund$State;,
        Lcom/squareup/protos/client/bills/Refund$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Refund;",
        "Lcom/squareup/protos/client/bills/Refund$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Refund;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_READ_ONLY_LOCALIZED_ERROR_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_LOCALIZED_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON_OPTION:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/bills/Refund$State;

.field public static final DEFAULT_WRITE_ONLY_LINKED_BILL_SERVER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_WRITE_ONLY_LINKED_TENDER_SERVER_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Refund$Destination#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final read_only_localized_error_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final read_only_localized_error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Refund$ReasonOption#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final refund_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/bills/Refund$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Refund$State#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final write_only_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final write_only_linked_bill_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final write_only_linked_tender_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ProtoAdapter_Refund;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$ProtoAdapter_Refund;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$State;->UNKNOWN_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Refund$State;

    sput-object v0, Lcom/squareup/protos/client/bills/Refund;->DEFAULT_STATE:Lcom/squareup/protos/client/bills/Refund$State;

    .line 36
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    sput-object v0, Lcom/squareup/protos/client/bills/Refund;->DEFAULT_REASON_OPTION:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Refund$State;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/Refund$Destination;)V
    .locals 16

    .line 179
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/protos/client/bills/Refund;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Refund$State;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/Refund$Destination;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Refund$State;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/Refund$Destination;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 188
    sget-object v1, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 189
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    move-object v1, p2

    .line 190
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    move-object v1, p3

    .line 191
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    move-object v1, p4

    .line 192
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    move-object v1, p5

    .line 193
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    move-object v1, p6

    .line 194
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    move-object v1, p7

    .line 195
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    move-object v1, p8

    .line 196
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    move-object v1, p9

    .line 197
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    move-object v1, p10

    .line 198
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    move-object v1, p11

    .line 199
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    move-object v1, p12

    .line 200
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    move-object/from16 v1, p13

    .line 201
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    move-object/from16 v1, p14

    .line 202
    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 229
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Refund;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 230
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Refund;

    .line 231
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    .line 240
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    .line 244
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    .line 245
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 250
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_e

    .line 252
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$State;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$Destination;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 267
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 2

    .line 207
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$Builder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->state:Lcom/squareup/protos/client/bills/Refund$State;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_bill_server_token:Ljava/lang/String;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_tender_server_token:Ljava/lang/String;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_amount:Lcom/squareup/protos/common/Money;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->reason:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_localized_error_title:Ljava/lang/String;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_localized_error_description:Ljava/lang/String;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Refund$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund;->newBuilder()Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", refund_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    if-eqz v1, :cond_1

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->state:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", write_only_linked_bill_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", write_only_linked_tender_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", write_only_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_5

    const-string v1, ", write_only_creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    if-eqz v1, :cond_6

    const-string v1, ", reason_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", read_only_localized_error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", read_only_localized_error_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_localized_error_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_a

    const-string v1, ", write_only_requested_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 286
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_b

    const-string v1, ", read_only_refunded_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v1, :cond_c

    const-string v1, ", read_only_linked_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    if-eqz v1, :cond_d

    const-string v1, ", read_only_destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Refund{"

    .line 289
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
