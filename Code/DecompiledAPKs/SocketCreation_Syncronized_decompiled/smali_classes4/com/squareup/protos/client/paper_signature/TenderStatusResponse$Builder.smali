.class public final Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public next_request_backoff_seconds:Ljava/lang/Integer;

.field public success:Ljava/lang/Boolean;

.field public tender_status_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 166
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 167
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->tender_status_list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;
    .locals 8

    .line 215
    new-instance v7, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->tender_status_list:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public next_request_backoff_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tender_status_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;)",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;"
        }
    .end annotation

    .line 198
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->tender_status_list:Ljava/util/List;

    return-object p0
.end method
