.class final Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$ProtoAdapter_Register;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SquareProductAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Register"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 198
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 215
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;-><init>()V

    .line 216
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 217
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 221
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 219
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->is_legacy_payment_transaction(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;

    goto :goto_0

    .line 225
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 226
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 196
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$ProtoAdapter_Register;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 209
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->is_legacy_payment_transaction:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 210
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 196
    check-cast p2, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$ProtoAdapter_Register;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)I
    .locals 3

    .line 203
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->is_legacy_payment_transaction:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 204
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 196
    check-cast p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$ProtoAdapter_Register;->encodedSize(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;
    .locals 0

    .line 231
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->newBuilder()Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;

    move-result-object p1

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 233
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 196
    check-cast p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$ProtoAdapter_Register;->redact(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    move-result-object p1

    return-object p1
.end method
