.class final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProvisionDigitalWalletTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ApplePayResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 274
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 295
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;-><init>()V

    .line 296
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 297
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 303
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 301
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->ephemeral_public_key(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    goto :goto_0

    .line 300
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->activation_data(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    goto :goto_0

    .line 299
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->encrypted_pass_data(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    goto :goto_0

    .line 307
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 308
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 272
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 288
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 289
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 290
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 272
    check-cast p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)I
    .locals 4

    .line 279
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    const/4 v3, 0x2

    .line 280
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    const/4 v3, 0x3

    .line 281
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 272
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;
    .locals 1

    .line 313
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 314
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->encrypted_pass_data:Lokio/ByteString;

    .line 315
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->activation_data:Lokio/ByteString;

    .line 316
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->ephemeral_public_key:Lokio/ByteString;

    .line 317
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 318
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 272
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;->redact(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    move-result-object p1

    return-object p1
.end method
