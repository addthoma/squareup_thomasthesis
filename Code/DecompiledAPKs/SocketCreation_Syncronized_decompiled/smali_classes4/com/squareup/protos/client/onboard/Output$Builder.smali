.class public final Lcom/squareup/protos/client/onboard/Output$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Output.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Output;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Output;",
        "Lcom/squareup/protos/client/onboard/Output$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public boolean_value:Ljava/lang/Boolean;

.field public component_name:Ljava/lang/String;

.field public integer_value:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public string_value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public boolean_value(Ljava/lang/Boolean;)Lcom/squareup/protos/client/onboard/Output$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->boolean_value:Ljava/lang/Boolean;

    const/4 p1, 0x0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->string_value:Ljava/lang/String;

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->integer_value:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/onboard/Output;
    .locals 8

    .line 185
    new-instance v7, Lcom/squareup/protos/client/onboard/Output;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->component_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->string_value:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->boolean_value:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->integer_value:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/Output;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Output$Builder;->build()Lcom/squareup/protos/client/onboard/Output;

    move-result-object v0

    return-object v0
.end method

.method public component_name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->component_name:Ljava/lang/String;

    return-object p0
.end method

.method public integer_value(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/Output$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->integer_value:Ljava/lang/Integer;

    const/4 p1, 0x0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->string_value:Ljava/lang/String;

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->boolean_value:Ljava/lang/Boolean;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public string_value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->string_value:Ljava/lang/String;

    const/4 p1, 0x0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->boolean_value:Ljava/lang/Boolean;

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Output$Builder;->integer_value:Ljava/lang/Integer;

    return-object p0
.end method
