.class final Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SelectedOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1658
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1685
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;-><init>()V

    .line 1686
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1687
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1696
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1694
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    goto :goto_0

    .line 1693
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    goto :goto_0

    .line 1692
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1691
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1690
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1689
    :pswitch_5
    sget-object v3, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    goto :goto_0

    .line 1700
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1701
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1656
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1674
    sget-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1675
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1676
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1677
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1678
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1679
    sget-object v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1680
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1656
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)I
    .locals 4

    .line 1663
    sget-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1664
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1665
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1666
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    const/4 v3, 0x5

    .line 1667
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    const/4 v3, 0x6

    .line 1668
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1669
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1656
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
    .locals 2

    .line 1706
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p1

    .line 1707
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    .line 1708
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1709
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1710
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1711
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    .line 1712
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    .line 1713
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1714
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1656
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ProtoAdapter_SelectedOptions;->redact(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p1

    return-object p1
.end method
