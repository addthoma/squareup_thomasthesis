.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;
.super Lcom/squareup/wire/Message;
.source "GetBillFamiliesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$ProtoAdapter_Query;,
        Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BILL_SERVER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_QUERY_STRING:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bill_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetBillFamiliesRequest$InstrumentSearch#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final payment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final query_string:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 452
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$ProtoAdapter_Query;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$ProtoAdapter_Query;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 500
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 505
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p5, 0x0

    new-array p5, p5, [Ljava/lang/Object;

    .line 506
    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result p5

    const/4 v0, 0x1

    if-gt p5, v0, :cond_0

    .line 509
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    .line 510
    iput-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 511
    iput-object p3, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    .line 512
    iput-object p4, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    return-void

    .line 507
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of query_string, instrument_search, bill_server_token, payment_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 529
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 530
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    .line 531
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    .line 532
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 533
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    .line 534
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    .line 535
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 540
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 542
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 543
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 544
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 545
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 546
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 547
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
    .locals 2

    .line 517
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;-><init>()V

    .line 518
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string:Ljava/lang/String;

    .line 519
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 520
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token:Ljava/lang/String;

    .line 521
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->payment_token:Ljava/lang/String;

    .line 522
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 451
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 554
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 555
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", query_string="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->query_string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    if-eqz v1, :cond_1

    const-string v1, ", instrument_search="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 557
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", bill_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->payment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Query{"

    .line 559
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
