.class public final enum Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;
.super Ljava/lang/Enum;
.source "AccumulateLoyaltyStatusResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AccumulationError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError$ProtoAdapter_AccumulationError;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

.field public static final enum ACCUMULATION_ERROR_INVALID_PHONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

.field public static final enum ACCUMULATION_ERROR_NONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 182
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    const/4 v1, 0x0

    const-string v2, "ACCUMULATION_ERROR_NONE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_NONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    .line 187
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    const/4 v2, 0x1

    const-string v3, "ACCUMULATION_ERROR_INVALID_PHONE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_INVALID_PHONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    .line 181
    sget-object v3, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_NONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_INVALID_PHONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->$VALUES:[Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    .line 189
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError$ProtoAdapter_AccumulationError;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError$ProtoAdapter_AccumulationError;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 194
    iput p3, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 203
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_INVALID_PHONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    return-object p0

    .line 202
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_NONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;
    .locals 1

    .line 181
    const-class v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;
    .locals 1

    .line 181
    sget-object v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->$VALUES:[Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 210
    iget v0, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->value:I

    return v0
.end method
