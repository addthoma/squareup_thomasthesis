.class public final Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;",
        "Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_details:Lcom/squareup/protos/client/bills/CompleteCardTender;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 411
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;
    .locals 3

    .line 421
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;->card_details:Lcom/squareup/protos/client/bills/CompleteCardTender;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;-><init>(Lcom/squareup/protos/client/bills/CompleteCardTender;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 408
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    move-result-object v0

    return-object v0
.end method

.method public card_details(Lcom/squareup/protos/client/bills/CompleteCardTender;)Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;
    .locals 0

    .line 415
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;->card_details:Lcom/squareup/protos/client/bills/CompleteCardTender;

    return-object p0
.end method
