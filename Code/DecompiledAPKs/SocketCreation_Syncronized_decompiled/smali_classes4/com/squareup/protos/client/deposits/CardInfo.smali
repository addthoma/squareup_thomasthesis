.class public final Lcom/squareup/protos/client/deposits/CardInfo;
.super Lcom/squareup/wire/Message;
.source "CardInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/deposits/CardInfo$ProtoAdapter_CardInfo;,
        Lcom/squareup/protos/client/deposits/CardInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/deposits/CardInfo;",
        "Lcom/squareup/protos/client/deposits/CardInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/deposits/CardInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_INSTRUMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_SUPPORTS_INSTANT_DEPOSIT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final instrument_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final supports_instant_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/deposits/CardInfo$ProtoAdapter_CardInfo;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/CardInfo$ProtoAdapter_CardInfo;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/deposits/CardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/protos/client/deposits/CardInfo;->DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/deposits/CardInfo;->DEFAULT_SUPPORTS_INSTANT_DEPOSIT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 6

    .line 72
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/deposits/CardInfo;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/protos/client/deposits/CardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 79
    iput-object p2, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    .line 81
    iput-object p4, p0, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/deposits/CardInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/deposits/CardInfo;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/CardInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/CardInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/CardInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 116
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/deposits/CardInfo$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/CardInfo$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->pan_suffix:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->supports_instant_deposit:Ljava/lang/Boolean;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->instrument_token:Ljava/lang/String;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/CardInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/CardInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/CardInfo;->newBuilder()Lcom/squareup/protos/client/deposits/CardInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_0

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", supports_instant_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->supports_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", instrument_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardInfo{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
