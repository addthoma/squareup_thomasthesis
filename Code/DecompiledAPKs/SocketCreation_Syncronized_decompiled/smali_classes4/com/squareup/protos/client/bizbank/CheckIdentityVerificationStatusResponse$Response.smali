.class public final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
.super Lcom/squareup/wire/Message;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Response"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;,
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_RETRY_IDV:Ljava/lang/Boolean;

.field public static final DEFAULT_IDV_ERROR:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

.field public static final DEFAULT_IDV_NEEDS_SSN:Ljava/lang/Boolean;

.field public static final DEFAULT_IDV_RETRIES_REMAINING:Ljava/lang/Integer;

.field public static final DEFAULT_IDV_STATE:Lcom/squareup/protos/client/bizbank/IdvState;

.field private static final serialVersionUID:J


# instance fields
.field public final can_retry_idv:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CheckIdentityVerificationStatusResponse$IdvError#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final idv_needs_ssn:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final idv_retries_remaining:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final idv_state:Lcom/squareup/protos/client/bizbank/IdvState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.IdvState#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 142
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$ProtoAdapter_Response;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 146
    sget-object v0, Lcom/squareup/protos/client/bizbank/IdvState;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/IdvState;

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->DEFAULT_IDV_STATE:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v0, 0x0

    .line 148
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->DEFAULT_IDV_NEEDS_SSN:Ljava/lang/Boolean;

    .line 150
    sput-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->DEFAULT_CAN_RETRY_IDV:Ljava/lang/Boolean;

    .line 152
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->DEFAULT_IDV_RETRIES_REMAINING:Ljava/lang/Integer;

    .line 154
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->DEFAULT_IDV_ERROR:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;)V
    .locals 7

    .line 192
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;-><init>(Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;Lokio/ByteString;)V
    .locals 1

    .line 197
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    .line 199
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    .line 200
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    .line 201
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    .line 202
    iput-object p5, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 220
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 221
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 227
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 232
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/IdvState;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 240
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;
    .locals 2

    .line 207
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_needs_ssn:Ljava/lang/Boolean;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->can_retry_idv:Ljava/lang/Boolean;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_retries_remaining:Ljava/lang/Integer;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 213
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->newBuilder()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    if-eqz v1, :cond_0

    const-string v1, ", idv_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", idv_needs_ssn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_needs_ssn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_retry_idv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", idv_retries_remaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_retries_remaining:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    if-eqz v1, :cond_4

    const-string v1, ", idv_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Response{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
