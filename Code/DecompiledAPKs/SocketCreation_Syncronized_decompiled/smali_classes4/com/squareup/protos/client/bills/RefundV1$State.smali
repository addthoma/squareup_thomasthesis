.class public final enum Lcom/squareup/protos/client/bills/RefundV1$State;
.super Ljava/lang/Enum;
.source "RefundV1.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/RefundV1$State$ProtoAdapter_State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/RefundV1$State;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/RefundV1$State;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/RefundV1$State;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPROVED:Lcom/squareup/protos/client/bills/RefundV1$State;

.field public static final enum COMPLETED:Lcom/squareup/protos/client/bills/RefundV1$State;

.field public static final enum FAILED:Lcom/squareup/protos/client/bills/RefundV1$State;

.field public static final enum REJECTED:Lcom/squareup/protos/client/bills/RefundV1$State;

.field public static final enum REQUESTED:Lcom/squareup/protos/client/bills/RefundV1$State;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/RefundV1$State;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 237
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/RefundV1$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->UNKNOWN:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 239
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v2, 0x1

    const-string v3, "REQUESTED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/RefundV1$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->REQUESTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 241
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v3, 0x2

    const-string v4, "APPROVED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/RefundV1$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->APPROVED:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 243
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v4, 0x3

    const-string v5, "COMPLETED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/RefundV1$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->COMPLETED:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 245
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v5, 0x4

    const-string v6, "REJECTED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/RefundV1$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->REJECTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 247
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v6, 0x5

    const-string v7, "FAILED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/RefundV1$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->FAILED:Lcom/squareup/protos/client/bills/RefundV1$State;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 236
    sget-object v7, Lcom/squareup/protos/client/bills/RefundV1$State;->UNKNOWN:Lcom/squareup/protos/client/bills/RefundV1$State;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->REQUESTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->APPROVED:Lcom/squareup/protos/client/bills/RefundV1$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->COMPLETED:Lcom/squareup/protos/client/bills/RefundV1$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->REJECTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->FAILED:Lcom/squareup/protos/client/bills/RefundV1$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->$VALUES:[Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 249
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$State$ProtoAdapter_State;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundV1$State$ProtoAdapter_State;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 253
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 254
    iput p3, p0, Lcom/squareup/protos/client/bills/RefundV1$State;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/RefundV1$State;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 267
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/RefundV1$State;->FAILED:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0

    .line 266
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/RefundV1$State;->REJECTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0

    .line 265
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/RefundV1$State;->COMPLETED:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0

    .line 264
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/RefundV1$State;->APPROVED:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0

    .line 263
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/RefundV1$State;->REQUESTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0

    .line 262
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bills/RefundV1$State;->UNKNOWN:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/RefundV1$State;
    .locals 1

    .line 236
    const-class v0, Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/RefundV1$State;
    .locals 1

    .line 236
    sget-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->$VALUES:[Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/RefundV1$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/RefundV1$State;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 274
    iget v0, p0, Lcom/squareup/protos/client/bills/RefundV1$State;->value:I

    return v0
.end method
