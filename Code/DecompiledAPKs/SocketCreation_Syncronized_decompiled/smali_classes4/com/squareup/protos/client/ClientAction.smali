.class public final Lcom/squareup/protos/client/ClientAction;
.super Lcom/squareup/wire/Message;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;,
        Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;,
        Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;,
        Lcom/squareup/protos/client/ClientAction$LinkBankAccount;,
        Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;,
        Lcom/squareup/protos/client/ClientAction$ActivateAccount;,
        Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;,
        Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;,
        Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;,
        Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;,
        Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;,
        Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewBankAccount;,
        Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;,
        Lcom/squareup/protos/client/ClientAction$CreateItem;,
        Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;,
        Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;,
        Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;,
        Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewDispute;,
        Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;,
        Lcom/squareup/protos/client/ClientAction$ViewSalesReport;,
        Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewInvoice;,
        Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;,
        Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;,
        Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;,
        Lcom/squareup/protos/client/ClientAction$FallbackBehavior;,
        Lcom/squareup/protos/client/ClientAction$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/ClientAction;",
        "Lcom/squareup/protos/client/ClientAction$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/ClientAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FALLBACK_BEHAVIOR:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

.field private static final serialVersionUID:J


# instance fields
.field public final activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ActivateAccount#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$CreateItem#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$FallbackBehavior#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$FinishAccountSetup#ADAPTER"
        tag = 0x1c
    .end annotation
.end field

.field public final link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$LinkBankAccount#ADAPTER"
        tag = 0x1d
    .end annotation
.end field

.field public final start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$StartCreateItemTutorial#ADAPTER"
        tag = 0x1e
    .end annotation
.end field

.field public final start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$StartFirstPaymentTutorial#ADAPTER"
        tag = 0x1f
    .end annotation
.end field

.field public final view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewAllDeposits#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewAllDisputes#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewAllInvoices#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewBankAccount#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewCheckoutApplet#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewCustomerConversation#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewCustomerMessageCenter#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewCustomersApplet#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewDepositsApplet#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewDepositsSettings#ADAPTER"
        tag = 0x18
    .end annotation
.end field

.field public final view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewDispute#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewFreeProcessing#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewInvoice#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewInvoicesApplet#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewItemsApplet#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewOrdersApplet#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewOverdueInvoices#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewReportsApplet#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewSalesReport#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewSettingsApplet#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewSupportApplet#ADAPTER"
        tag = 0x14
    .end annotation
.end field

.field public final view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewSupportMessageCenter#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewTransactionsApplet#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction$ViewTutorialsAndTours#ADAPTER"
        tag = 0x16
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    sput-object v0, Lcom/squareup/protos/client/ClientAction;->DEFAULT_FALLBACK_BEHAVIOR:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;Lcom/squareup/protos/client/ClientAction$ViewInvoice;Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;Lcom/squareup/protos/client/ClientAction$ViewSalesReport;Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;Lcom/squareup/protos/client/ClientAction$ViewDispute;Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;Lcom/squareup/protos/client/ClientAction$CreateItem;Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;Lcom/squareup/protos/client/ClientAction$ViewBankAccount;Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;Lcom/squareup/protos/client/ClientAction$ActivateAccount;Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;Lcom/squareup/protos/client/ClientAction$LinkBankAccount;Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;)V
    .locals 33

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    move-object/from16 v23, p23

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p28

    move-object/from16 v29, p29

    move-object/from16 v30, p30

    move-object/from16 v31, p31

    .line 235
    sget-object v32, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v32}, Lcom/squareup/protos/client/ClientAction;-><init>(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;Lcom/squareup/protos/client/ClientAction$ViewInvoice;Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;Lcom/squareup/protos/client/ClientAction$ViewSalesReport;Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;Lcom/squareup/protos/client/ClientAction$ViewDispute;Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;Lcom/squareup/protos/client/ClientAction$CreateItem;Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;Lcom/squareup/protos/client/ClientAction$ViewBankAccount;Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;Lcom/squareup/protos/client/ClientAction$ActivateAccount;Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;Lcom/squareup/protos/client/ClientAction$LinkBankAccount;Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;Lcom/squareup/protos/client/ClientAction$ViewInvoice;Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;Lcom/squareup/protos/client/ClientAction$ViewSalesReport;Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;Lcom/squareup/protos/client/ClientAction$ViewDispute;Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;Lcom/squareup/protos/client/ClientAction$CreateItem;Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;Lcom/squareup/protos/client/ClientAction$ViewBankAccount;Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;Lcom/squareup/protos/client/ClientAction$ActivateAccount;Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;Lcom/squareup/protos/client/ClientAction$LinkBankAccount;Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;Lokio/ByteString;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    move-object/from16 v3, p19

    move-object/from16 v4, p20

    .line 255
    sget-object v4, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v3, p32

    invoke-direct {v0, v4, v3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/16 v3, 0x1a

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v6, v3, v4

    const/16 v16, 0x2

    aput-object v7, v3, v16

    const/16 v16, 0x3

    aput-object v8, v3, v16

    const/16 v16, 0x4

    aput-object v9, v3, v16

    const/16 v16, 0x5

    aput-object v10, v3, v16

    const/16 v16, 0x6

    aput-object v11, v3, v16

    const/16 v16, 0x7

    aput-object v12, v3, v16

    const/16 v16, 0x8

    aput-object v13, v3, v16

    const/16 v16, 0x9

    aput-object v14, v3, v16

    const/16 v16, 0xa

    aput-object v15, v3, v16

    const/16 v16, 0xb

    aput-object v1, v3, v16

    const/16 v16, 0xc

    aput-object v2, v3, v16

    const/16 v16, 0xd

    aput-object p19, v3, v16

    const/16 v16, 0xe

    aput-object p20, v3, v16

    const/16 v16, 0xf

    move-object/from16 v4, p21

    aput-object v4, v3, v16

    const/16 v16, 0x10

    move-object/from16 v1, p22

    aput-object v1, v3, v16

    const/16 v16, 0x11

    move-object/from16 v2, p23

    aput-object v2, v3, v16

    const/16 v16, 0x12

    move-object/from16 v2, p24

    aput-object v2, v3, v16

    const/16 v16, 0x13

    move-object/from16 v2, p25

    aput-object v2, v3, v16

    const/16 v16, 0x14

    move-object/from16 v2, p26

    aput-object v2, v3, v16

    const/16 v16, 0x15

    move-object/from16 v2, p27

    aput-object v2, v3, v16

    const/16 v16, 0x16

    move-object/from16 v2, p28

    aput-object v2, v3, v16

    const/16 v16, 0x17

    move-object/from16 v2, p29

    aput-object v2, v3, v16

    const/16 v16, 0x18

    move-object/from16 v2, p30

    aput-object v2, v3, v16

    const/16 v16, 0x19

    move-object/from16 v2, p31

    aput-object v2, v3, v16

    move-object/from16 v2, p2

    move-object/from16 v1, p3

    move-object/from16 v4, p4

    move-object/from16 v15, p5

    .line 256
    invoke-static {v2, v1, v4, v15, v3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v3

    const/4 v14, 0x1

    if-gt v3, v14, :cond_0

    move-object/from16 v3, p1

    .line 259
    iput-object v3, v0, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    .line 260
    iput-object v2, v0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 261
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 262
    iput-object v4, v0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 263
    iput-object v15, v0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 264
    iput-object v5, v0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 265
    iput-object v6, v0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 266
    iput-object v7, v0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 267
    iput-object v8, v0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 268
    iput-object v9, v0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 269
    iput-object v10, v0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 270
    iput-object v11, v0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 271
    iput-object v12, v0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 272
    iput-object v13, v0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    move-object/from16 v1, p15

    .line 273
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    move-object/from16 v1, p16

    .line 274
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    move-object/from16 v1, p17

    .line 275
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    move-object/from16 v1, p18

    .line 276
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    move-object/from16 v1, p19

    .line 277
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    move-object/from16 v1, p20

    .line 278
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    move-object/from16 v1, p21

    .line 279
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    move-object/from16 v1, p22

    .line 280
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    move-object/from16 v1, p23

    move-object/from16 v2, p24

    .line 281
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 282
    iput-object v2, v0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    move-object/from16 v1, p25

    move-object/from16 v2, p26

    .line 283
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 284
    iput-object v2, v0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-object/from16 v1, p27

    move-object/from16 v2, p28

    .line 285
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 286
    iput-object v2, v0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-object/from16 v1, p29

    move-object/from16 v2, p30

    .line 287
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 288
    iput-object v2, v0, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    move-object/from16 v1, p31

    .line 289
    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-void

    .line 257
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "at most one of view_checkout_applet, view_orders_applet, view_invoices_applet, view_all_invoices, view_invoice, view_transactions_applet, view_reports_applet, view_sales_report, view_all_disputes, view_dispute, view_deposits_applet, view_all_deposits, view_customers_applet, view_customer_message_center, view_customer_conversation, view_items_applet, create_item, view_settings_applet, view_bank_account, view_support_applet, view_support_message_center, view_tutorials_and_tours, view_deposits_settings, view_overdue_invoices, view_free_processing, activate_account, finish_account_setup, link_bank_account, start_create_item_tutorial, start_first_payment_tutorial may be non-null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 333
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/ClientAction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 334
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/ClientAction;

    .line 335
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    .line 336
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 337
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 338
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 339
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 340
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 341
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 342
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 343
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 344
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 345
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 346
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 347
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 348
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 349
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 350
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 351
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 352
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 353
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 355
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 356
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 357
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 358
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 359
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 360
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 361
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 362
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 363
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 364
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    iget-object v3, p1, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 365
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    .line 366
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 371
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1f

    .line 373
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 374
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 375
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 376
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 377
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 378
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 379
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 380
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 381
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 382
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 383
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 384
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewDispute;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 385
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 386
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 387
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 388
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 389
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 390
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 391
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$CreateItem;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 392
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 393
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 394
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 395
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 396
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 397
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 398
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 399
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 400
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$ActivateAccount;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 401
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;->hashCode()I

    move-result v2

    :cond_1e
    add-int/2addr v0, v2

    .line 405
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1f
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 2

    .line 294
    new-instance v0, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 295
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    .line 296
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 297
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 298
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 299
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 300
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 301
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 302
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 303
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 305
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 306
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 307
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 308
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    iput-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    .line 326
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/ClientAction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction;->newBuilder()Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 413
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    if-eqz v1, :cond_0

    const-string v1, ", fallback_behavior="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 414
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    if-eqz v1, :cond_1

    const-string v1, ", view_checkout_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    if-eqz v1, :cond_2

    const-string v1, ", view_orders_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 416
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    if-eqz v1, :cond_3

    const-string v1, ", view_invoices_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 417
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    if-eqz v1, :cond_4

    const-string v1, ", view_all_invoices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 418
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    if-eqz v1, :cond_5

    const-string v1, ", view_invoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 419
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    if-eqz v1, :cond_6

    const-string v1, ", view_transactions_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 420
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    if-eqz v1, :cond_7

    const-string v1, ", view_reports_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 421
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    if-eqz v1, :cond_8

    const-string v1, ", view_sales_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 422
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    if-eqz v1, :cond_9

    const-string v1, ", view_all_disputes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 423
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    if-eqz v1, :cond_a

    const-string v1, ", view_dispute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 424
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    if-eqz v1, :cond_b

    const-string v1, ", view_deposits_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 425
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    if-eqz v1, :cond_c

    const-string v1, ", view_all_deposits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 426
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    if-eqz v1, :cond_d

    const-string v1, ", view_customers_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 427
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    if-eqz v1, :cond_e

    const-string v1, ", view_customer_message_center="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 428
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    if-eqz v1, :cond_f

    const-string v1, ", view_customer_conversation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 429
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    if-eqz v1, :cond_10

    const-string v1, ", view_items_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 430
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    if-eqz v1, :cond_11

    const-string v1, ", create_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 431
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    if-eqz v1, :cond_12

    const-string v1, ", view_settings_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 432
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    if-eqz v1, :cond_13

    const-string v1, ", view_bank_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 433
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    if-eqz v1, :cond_14

    const-string v1, ", view_support_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 434
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    if-eqz v1, :cond_15

    const-string v1, ", view_support_message_center="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 435
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    if-eqz v1, :cond_16

    const-string v1, ", view_tutorials_and_tours="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 436
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    if-eqz v1, :cond_17

    const-string v1, ", view_deposits_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 437
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    if-eqz v1, :cond_18

    const-string v1, ", view_overdue_invoices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 438
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    if-eqz v1, :cond_19

    const-string v1, ", view_free_processing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 439
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    if-eqz v1, :cond_1a

    const-string v1, ", activate_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 440
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    if-eqz v1, :cond_1b

    const-string v1, ", finish_account_setup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 441
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    if-eqz v1, :cond_1c

    const-string v1, ", link_bank_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 442
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    if-eqz v1, :cond_1d

    const-string v1, ", start_create_item_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 443
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    if-eqz v1, :cond_1e

    const-string v1, ", start_first_payment_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ClientAction{"

    .line 444
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
