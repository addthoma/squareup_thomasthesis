.class public final Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;
.super Lcom/squareup/wire/Message;
.source "SettledBillEntriesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SettledBillEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$ProtoAdapter_SettledBillEntry;,
        Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;,
        Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SETTLED_BILL_ENTRY_TYPE:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

.field private static final serialVersionUID:J


# instance fields
.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final collected_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final request_created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettledBillEntryTokenGroup#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final to_user_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 108
    new-instance v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$ProtoAdapter_SettledBillEntry;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$ProtoAdapter_SettledBillEntry;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 112
    sget-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;->SETTLED_BILL_ENTRY_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    sput-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->DEFAULT_SETTLED_BILL_ENTRY_TYPE:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 8

    .line 173
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;-><init>(Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V
    .locals 1

    .line 179
    sget-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    .line 181
    iput-object p2, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 182
    iput-object p3, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    .line 183
    iput-object p4, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    .line 184
    iput-object p5, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    .line 185
    iput-object p6, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 204
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 205
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 212
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 217
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 219
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 226
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 2

    .line 190
    new-instance v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;-><init>()V

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->collected_money:Lcom/squareup/protos/common/Money;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->to_user_money:Lcom/squareup/protos/common/Money;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 197
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->newBuilder()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    if-eqz v1, :cond_0

    const-string v1, ", settled_bill_entry_token_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", request_created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    if-eqz v1, :cond_2

    const-string v1, ", settled_bill_entry_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", collected_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 238
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", to_user_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_5

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SettledBillEntry{"

    .line 240
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
