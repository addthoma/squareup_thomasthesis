.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OpenTicket"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$ProtoAdapter_OpenTicket;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OpenTicket$PredefinedTicket#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final products_opened_on:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;"
        }
    .end annotation
.end field

.field public final ticket_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final ticket_owner:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1450
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$ProtoAdapter_OpenTicket;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$ProtoAdapter_OpenTicket;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/CreatorDetails;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;)V"
        }
    .end annotation

    .line 1543
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/CreatorDetails;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 1550
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1551
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    .line 1552
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    .line 1553
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1554
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 1555
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 1556
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    .line 1557
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const-string p1, "products_opened_on"

    .line 1558
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1579
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1580
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 1581
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    .line 1582
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    .line 1583
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1584
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 1585
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 1586
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    .line 1587
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1588
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    .line 1589
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1594
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 1596
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1597
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1598
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1599
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1600
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1601
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1602
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1603
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 1604
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1605
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 2

    .line 1563
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;-><init>()V

    .line 1564
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->name:Ljava/lang/String;

    .line 1565
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->note:Ljava/lang/String;

    .line 1566
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1567
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 1568
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 1569
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->unit_token:Ljava/lang/String;

    .line 1570
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1571
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->products_opened_on:Ljava/util/List;

    .line 1572
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1449
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1612
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1613
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1614
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", note=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1615
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_2

    const-string v1, ", ticket_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1616
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    const-string v1, ", ticket_owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1617
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    if-eqz v1, :cond_4

    const-string v1, ", predefined_ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1618
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1619
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1620
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", products_opened_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->products_opened_on:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OpenTicket{"

    .line 1621
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
