.class public final enum Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;
.super Ljava/lang/Enum;
.source "CashDrawerShiftEvent.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CASH_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum CASH_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum CASH_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum NO_SALE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum OTHER_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum OTHER_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum OTHER_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 290
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->UNKNOWN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 292
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v2, 0x1

    const-string v3, "NO_SALE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->NO_SALE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 294
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v3, 0x2

    const-string v4, "CASH_TENDER_PAYMENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 299
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v4, 0x3

    const-string v5, "OTHER_TENDER_PAYMENT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 304
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v5, 0x4

    const-string v6, "CASH_TENDER_CANCELLED_PAYMENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 309
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v6, 0x5

    const-string v7, "OTHER_TENDER_CANCELLED_PAYMENT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 311
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v7, 0x6

    const-string v8, "CASH_TENDER_REFUND"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 313
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/4 v8, 0x7

    const-string v9, "OTHER_TENDER_REFUND"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 318
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/16 v9, 0x8

    const-string v10, "PAID_IN"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 323
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/16 v10, 0x9

    const-string v11, "PAID_OUT"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 289
    sget-object v11, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->UNKNOWN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->NO_SALE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->$VALUES:[Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 325
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 329
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 330
    iput p3, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 347
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 346
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 345
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 344
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 343
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 342
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 341
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 340
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 339
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->NO_SALE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    .line 338
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->UNKNOWN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;
    .locals 1

    .line 289
    const-class v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;
    .locals 1

    .line 289
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->$VALUES:[Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 354
    iget v0, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->value:I

    return v0
.end method
