.class public final Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMetricsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public metric:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;->metric:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetMetricsResponse;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;->metric:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse;

    move-result-object v0

    return-object v0
.end method

.method public metric(Ljava/util/List;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;"
        }
    .end annotation

    .line 107
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;->metric:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
