.class public final Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReportCoredumpRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;",
        "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

.field public coredump_data:Lokio/ByteString;

.field public coredump_key:Lokio/ByteString;

.field public merchant_token:Ljava/lang/String;

.field public reader_serial_number:Ljava/lang/String;

.field public reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public user_agent:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
    .locals 10

    .line 233
    new-instance v9, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_key:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_data:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iget-object v4, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_serial_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    iget-object v7, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->user_agent:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;-><init>(Lokio/ByteString;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/ChipType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    move-result-object v0

    return-object v0
.end method

.method public chip_type(Lcom/squareup/protos/client/tarkin/ChipType;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->chip_type:Lcom/squareup/protos/client/tarkin/ChipType;

    return-object p0
.end method

.method public coredump_data(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_data:Lokio/ByteString;

    return-object p0
.end method

.method public coredump_key(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_key:Lokio/ByteString;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public reader_serial_number(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0
.end method

.method public user_agent(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->user_agent:Ljava/lang/String;

    return-object p0
.end method
