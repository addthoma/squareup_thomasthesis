.class final Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReceiptDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 721
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 742
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;-><init>()V

    .line 743
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 744
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 757
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 755
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;

    goto :goto_0

    .line 754
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;

    goto :goto_0

    .line 748
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->receipt_behavior(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 750
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 761
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 762
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 719
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 734
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 735
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 736
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 737
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 719
    check-cast p2, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)I
    .locals 4

    .line 726
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    const/4 v3, 0x2

    .line 727
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    const/4 v3, 0x3

    .line 728
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 729
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 719
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;->encodedSize(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
    .locals 2

    .line 767
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->newBuilder()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;

    move-result-object p1

    .line 768
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    .line 769
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    .line 770
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 771
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 719
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;->redact(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    move-result-object p1

    return-object p1
.end method
