.class public final Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/UnitMetadata;",
        "Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public default_message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/UnitMetadata;
    .locals 3

    .line 99
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadata;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->default_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/invoice/UnitMetadata;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadata;

    move-result-object v0

    return-object v0
.end method

.method public default_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->default_message:Ljava/lang/String;

    return-object p0
.end method
