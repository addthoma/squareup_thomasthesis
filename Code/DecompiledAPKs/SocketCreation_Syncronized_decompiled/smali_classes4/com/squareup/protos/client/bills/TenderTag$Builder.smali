.class public final Lcom/squareup/protos/client/bills/TenderTag$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/TenderTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/TenderTag;",
        "Lcom/squareup/protos/client/bills/TenderTag$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public key:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/TenderTag;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/bills/TenderTag;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/TenderTag$Builder;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/TenderTag$Builder;->value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/TenderTag;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TenderTag$Builder;->build()Lcom/squareup/protos/client/bills/TenderTag;

    move-result-object v0

    return-object v0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/protos/client/bills/TenderTag$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TenderTag$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/bills/TenderTag$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TenderTag$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
