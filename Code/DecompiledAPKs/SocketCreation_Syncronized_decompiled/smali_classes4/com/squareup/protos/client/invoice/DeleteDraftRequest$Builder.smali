.class public final Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteDraftRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/DeleteDraftRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/DeleteDraftRequest;",
        "Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/DeleteDraftRequest;
    .locals 3

    .line 94
    new-instance v0, Lcom/squareup/protos/client/invoice/DeleteDraftRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/invoice/DeleteDraftRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;->build()Lcom/squareup/protos/client/invoice/DeleteDraftRequest;

    move-result-object v0

    return-object v0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
