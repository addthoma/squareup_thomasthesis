.class public final enum Lcom/squareup/protos/client/orders/OrderGroup;
.super Ljava/lang/Enum;
.source "OrderGroup.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/OrderGroup$ProtoAdapter_OrderGroup;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum NEEDS_ACTION:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum NEW:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum READY:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/orders/OrderGroup;

.field public static final enum UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 14
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->UNKNOWN:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 23
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v2, 0x1

    const-string v3, "ACTIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v3, 0x2

    const-string v4, "NEEDS_ACTION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->NEEDS_ACTION:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 34
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v4, 0x3

    const-string v5, "IN_PROGRESS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v5, 0x4

    const-string v6, "COMPLETED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 44
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v6, 0x5

    const-string v7, "READY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->READY:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 49
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v7, 0x6

    const-string v8, "NEW"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->NEW:Lcom/squareup/protos/client/orders/OrderGroup;

    .line 57
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup;

    const/4 v8, 0x7

    const-string v9, "UPCOMING"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/orders/OrderGroup;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/client/orders/OrderGroup;

    .line 13
    sget-object v9, Lcom/squareup/protos/client/orders/OrderGroup;->UNKNOWN:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->NEEDS_ACTION:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->READY:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->NEW:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->$VALUES:[Lcom/squareup/protos/client/orders/OrderGroup;

    .line 59
    new-instance v0, Lcom/squareup/protos/client/orders/OrderGroup$ProtoAdapter_OrderGroup;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderGroup$ProtoAdapter_OrderGroup;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, Lcom/squareup/protos/client/orders/OrderGroup;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/orders/OrderGroup;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 79
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 78
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->NEW:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 77
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->READY:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 76
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 75
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 74
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->NEEDS_ACTION:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 73
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    .line 72
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/orders/OrderGroup;->UNKNOWN:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderGroup;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/orders/OrderGroup;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/orders/OrderGroup;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->$VALUES:[Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/orders/OrderGroup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/orders/OrderGroup;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/squareup/protos/client/orders/OrderGroup;->value:I

    return v0
.end method
