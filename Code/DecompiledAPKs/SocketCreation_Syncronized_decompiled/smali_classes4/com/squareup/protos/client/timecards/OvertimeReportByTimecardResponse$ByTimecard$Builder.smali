.class public final Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OvertimeReportByTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

.field public timecard:Lcom/squareup/protos/client/timecards/Timecard;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 225
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;
    .locals 5

    .line 248
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;-><init>(Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/CalculationTotal;Lcom/squareup/protos/client/timecards/Timecard;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 218
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    move-result-object v0

    return-object v0
.end method

.method public calculated_time_interval(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public calculation_total(Lcom/squareup/protos/client/timecards/CalculationTotal;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    return-object p0
.end method

.method public timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method
