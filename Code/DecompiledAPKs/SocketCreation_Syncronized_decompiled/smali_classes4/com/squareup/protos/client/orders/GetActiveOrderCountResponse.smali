.class public final Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;
.super Lcom/squareup/wire/Message;
.source "GetActiveOrderCountResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$ProtoAdapter_GetActiveOrderCountResponse;,
        Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;",
        "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$ProtoAdapter_GetActiveOrderCountResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$ProtoAdapter_GetActiveOrderCountResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->DEFAULT_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 1

    .line 37
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 56
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 57
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    .line 59
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 64
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 68
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;-><init>()V

    .line 48
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;->count:Ljava/lang/Integer;

    .line 49
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->newBuilder()Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetActiveOrderCountResponse;->count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetActiveOrderCountResponse{"

    .line 77
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
