.class public final Lcom/squareup/protos/client/rolodex/FrequentItem;
.super Lcom/squareup/wire/Message;
.source "FrequentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/FrequentItem$ProtoAdapter_FrequentItem;,
        Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/FrequentItem;",
        "Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/FrequentItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ABBREVIATION:Ljava/lang/String; = ""

.field public static final DEFAULT_CATEGORY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_IMAGE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_PURCHASE_COUNT:Ljava/lang/Double;

.field public static final DEFAULT_TRANSACTION_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final abbreviation:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final category_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final color:Lcom/squareup/protos/common/RGBAColor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.RGBAColor#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final purchase_count:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x3
    .end annotation
.end field

.field public final transaction_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/FrequentItem$ProtoAdapter_FrequentItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/FrequentItem$ProtoAdapter_FrequentItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/FrequentItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/FrequentItem;->DEFAULT_PURCHASE_COUNT:Ljava/lang/Double;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/FrequentItem;->DEFAULT_TRANSACTION_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 9

    .line 108
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/rolodex/FrequentItem;-><init>(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/protos/client/rolodex/FrequentItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    .line 116
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    .line 117
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    .line 118
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    .line 119
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    .line 120
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    .line 121
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 141
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 142
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/FrequentItem;

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/FrequentItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/FrequentItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    .line 150
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 155
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/FrequentItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/RGBAColor;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 165
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 2

    .line 126
    new-instance v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->display_name:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->purchase_count:Ljava/lang/Double;

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->image_url:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->color:Lcom/squareup/protos/common/RGBAColor;

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->category_name:Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->transaction_count:Ljava/lang/Integer;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->abbreviation:Ljava/lang/String;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/FrequentItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/FrequentItem;->newBuilder()Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    if-eqz v1, :cond_1

    const-string v1, ", purchase_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->purchase_count:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    if-eqz v1, :cond_3

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->color:Lcom/squareup/protos/common/RGBAColor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", category_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", transaction_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 179
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", abbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem;->abbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FrequentItem{"

    .line 180
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
