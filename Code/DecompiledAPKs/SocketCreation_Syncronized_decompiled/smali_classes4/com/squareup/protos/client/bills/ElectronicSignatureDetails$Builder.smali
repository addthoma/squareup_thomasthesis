.class public final Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ElectronicSignatureDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;",
        "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public certificate_version:Ljava/lang/Integer;

.field public display_signature:Ljava/lang/String;

.field public electronic_signature:Ljava/lang/String;

.field public signature_sequence_number:Ljava/lang/Long;

.field public signed_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;
    .locals 8

    .line 206
    new-instance v7, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->certificate_version:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->electronic_signature:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->signature_sequence_number:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->display_signature:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    move-result-object v0

    return-object v0
.end method

.method public certificate_version(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->certificate_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public display_signature(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->display_signature:Ljava/lang/String;

    return-object p0
.end method

.method public electronic_signature(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->electronic_signature:Ljava/lang/String;

    return-object p0
.end method

.method public signature_sequence_number(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->signature_sequence_number:Ljava/lang/Long;

    return-object p0
.end method

.method public signed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
