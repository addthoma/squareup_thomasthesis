.class public final Lcom/squareup/protos/client/rolodex/Group$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Group.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Group;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "Lcom/squareup/protos/client/rolodex/Group$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

.field public display_name:Ljava/lang/String;

.field public filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public group_token:Ljava/lang/String;

.field public group_type:Lcom/squareup/protos/client/rolodex/GroupType;

.field public merchant_token:Ljava/lang/String;

.field public num_customers:Ljava/lang/Long;

.field public num_manual_contacts:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 205
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 206
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public audience_type(Lcom/squareup/protos/client/rolodex/AudienceType;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/Group;
    .locals 11

    .line 268
    new-instance v10, Lcom/squareup/protos/client/rolodex/Group;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_manual_contacts:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_customers:Ljava/lang/Long;

    iget-object v8, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/rolodex/Group;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/GroupType;Lcom/squareup/protos/client/rolodex/AudienceType;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    return-object v0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Group$Builder;"
        }
    .end annotation

    .line 261
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters:Ljava/util/List;

    return-object p0
.end method

.method public group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_token:Ljava/lang/String;

    return-object p0
.end method

.method public group_type(Lcom/squareup/protos/client/rolodex/GroupType;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public num_customers(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_customers:Ljava/lang/Long;

    return-object p0
.end method

.method public num_manual_contacts(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_manual_contacts:Ljava/lang/Long;

    return-object p0
.end method
