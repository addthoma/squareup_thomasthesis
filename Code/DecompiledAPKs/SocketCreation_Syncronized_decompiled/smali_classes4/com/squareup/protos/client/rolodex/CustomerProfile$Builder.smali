.class public final Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomerProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/CustomerProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public birthday:Lcom/squareup/protos/common/time/YearMonthDay;

.field public company_name:Ljava/lang/String;

.field public email_address:Ljava/lang/String;

.field public given_name:Ljava/lang/String;

.field public masked_email_address:Ljava/lang/String;

.field public masked_phone_number:Ljava/lang/String;

.field public memo:Ljava/lang/String;

.field public merchant_provided_id:Ljava/lang/String;

.field public nickname:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public surname:Ljava/lang/String;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 299
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 369
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public birthday(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/CustomerProfile;
    .locals 17

    move-object/from16 v0, p0

    .line 391
    new-instance v16, Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v2, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->version:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->merchant_provided_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->nickname:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->company_name:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v9, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->memo:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v13, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->masked_email_address:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->masked_phone_number:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/rolodex/CustomerProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 272
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object v0

    return-object v0
.end method

.method public company_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->company_name:Ljava/lang/String;

    return-object p0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 359
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name:Ljava/lang/String;

    return-object p0
.end method

.method public masked_email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->masked_email_address:Ljava/lang/String;

    return-object p0
.end method

.method public masked_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->masked_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public memo(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->memo:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_provided_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->merchant_provided_id:Ljava/lang/String;

    return-object p0
.end method

.method public nickname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->nickname:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public surname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->version:Ljava/lang/String;

    return-object p0
.end method
