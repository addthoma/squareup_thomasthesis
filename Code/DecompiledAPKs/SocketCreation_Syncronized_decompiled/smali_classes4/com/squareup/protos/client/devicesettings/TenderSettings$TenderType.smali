.class public final enum Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;
.super Ljava/lang/Enum;
.source "TenderSettings.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/TenderSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TenderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType$ProtoAdapter_TenderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum DEFERRED_PAY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 147
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->UNKNOWN:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 149
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v2, 0x1

    const-string v3, "CARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 154
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v3, 0x2

    const-string v4, "GIFT_CARD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 156
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v4, 0x3

    const-string v5, "CASH"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 158
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v5, 0x4

    const-string v6, "INVOICE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 160
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v6, 0x5

    const-string v7, "CARD_ON_FILE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 162
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v7, 0x6

    const-string v8, "E_MONEY"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 164
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v8, 0x7

    const-string v9, "INSTALLMENTS"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 166
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/16 v9, 0x8

    const-string v10, "CHECKOUT_LINK"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 171
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/16 v10, 0x9

    const-string v11, "DEFERRED_PAY"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->DEFERRED_PAY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 146
    sget-object v11, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->UNKNOWN:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->DEFERRED_PAY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->$VALUES:[Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 173
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType$ProtoAdapter_TenderType;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType$ProtoAdapter_TenderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 178
    iput p3, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 195
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->DEFERRED_PAY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 194
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 193
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 192
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 191
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 190
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 189
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 188
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 187
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    .line 186
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->UNKNOWN:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;
    .locals 1

    .line 146
    const-class v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;
    .locals 1

    .line 146
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->$VALUES:[Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 202
    iget v0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->value:I

    return v0
.end method
