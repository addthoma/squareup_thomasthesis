.class final Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "StartTimecardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StartTimecardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_StartTimecardRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/StartTimecardRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 172
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/StartTimecardRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 197
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    .line 204
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 202
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->use_job_token(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    goto :goto_0

    .line 201
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->job_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    goto :goto_0

    .line 200
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->clockin_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    goto :goto_0

    .line 199
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    goto :goto_0

    .line 208
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 209
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 170
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/StartTimecardRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 186
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 187
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 190
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 170
    check-cast p2, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/StartTimecardRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/StartTimecardRequest;)I
    .locals 4

    .line 177
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 178
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 179
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 180
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 170
    check-cast p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;->encodedSize(Lcom/squareup/protos/client/timecards/StartTimecardRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/StartTimecardRequest;)Lcom/squareup/protos/client/timecards/StartTimecardRequest;
    .locals 0

    .line 214
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->newBuilder()Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    move-result-object p1

    .line 215
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 216
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 170
    check-cast p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;->redact(Lcom/squareup/protos/client/timecards/StartTimecardRequest;)Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    move-result-object p1

    return-object p1
.end method
