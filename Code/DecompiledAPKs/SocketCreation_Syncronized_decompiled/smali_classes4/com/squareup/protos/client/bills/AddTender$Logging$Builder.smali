.class public final Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTender$Logging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddTender$Logging;",
        "Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public manual_request_try_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 309
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddTender$Logging;
    .locals 3

    .line 322
    new-instance v0, Lcom/squareup/protos/client/bills/AddTender$Logging;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->manual_request_try_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/AddTender$Logging;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 306
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->build()Lcom/squareup/protos/client/bills/AddTender$Logging;

    move-result-object v0

    return-object v0
.end method

.method public manual_request_try_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Logging$Builder;->manual_request_try_count:Ljava/lang/Integer;

    return-object p0
.end method
