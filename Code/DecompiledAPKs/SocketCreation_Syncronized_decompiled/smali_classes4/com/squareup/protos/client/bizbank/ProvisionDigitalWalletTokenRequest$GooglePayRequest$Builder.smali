.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionDigitalWalletTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public device_id:Ljava/lang/String;

.field public wallet_account_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 483
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;
    .locals 4

    .line 504
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->wallet_account_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->device_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 478
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    move-result-object v0

    return-object v0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;
    .locals 0

    .line 498
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method

.method public wallet_account_id(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;
    .locals 0

    .line 490
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->wallet_account_id:Ljava/lang/String;

    return-object p0
.end method
