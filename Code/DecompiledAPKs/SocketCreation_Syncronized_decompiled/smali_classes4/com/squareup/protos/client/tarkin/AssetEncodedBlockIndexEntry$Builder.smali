.class public final Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AssetEncodedBlockIndexEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry;",
        "Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encoded_image_block_length:Ljava/lang/Integer;

.field public encoded_image_offset:Ljava/lang/Integer;

.field public plaintext_image_block_length:Ljava/lang/Integer;

.field public plaintext_image_offset:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry;
    .locals 7

    .line 178
    new-instance v6, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->plaintext_image_offset:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->plaintext_image_block_length:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->encoded_image_offset:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->encoded_image_block_length:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry;

    move-result-object v0

    return-object v0
.end method

.method public encoded_image_block_length(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->encoded_image_block_length:Ljava/lang/Integer;

    return-object p0
.end method

.method public encoded_image_offset(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->encoded_image_offset:Ljava/lang/Integer;

    return-object p0
.end method

.method public plaintext_image_block_length(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->plaintext_image_block_length:Ljava/lang/Integer;

    return-object p0
.end method

.method public plaintext_image_offset(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexEntry$Builder;->plaintext_image_offset:Ljava/lang/Integer;

    return-object p0
.end method
