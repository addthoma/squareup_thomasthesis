.class public final Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateAttachmentResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment_response:Lcom/squareup/protos/client/rolodex/AttachmentResponse;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attachment_response(Lcom/squareup/protos/client/rolodex/AttachmentResponse;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;->attachment_response:Lcom/squareup/protos/client/rolodex/AttachmentResponse;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;->attachment_response:Lcom/squareup/protos/client/rolodex/AttachmentResponse;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;-><init>(Lcom/squareup/protos/client/rolodex/AttachmentResponse;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
