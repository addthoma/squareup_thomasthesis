.class public final Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListGroupsV2Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListGroupsV2Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListGroupsV2Response;",
        "Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

.field public groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 115
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->groups:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListGroupsV2Response;
    .locals 5

    .line 143
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->groups:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/rolodex/GroupV2Counts;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->build()Lcom/squareup/protos/client/rolodex/ListGroupsV2Response;

    move-result-object v0

    return-object v0
.end method

.method public counts(Lcom/squareup/protos/client/rolodex/GroupV2Counts;)Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    return-object p0
.end method

.method public groups(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;"
        }
    .end annotation

    .line 128
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->groups:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Response$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
