.class final Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ProtoAdapter_OvertimeReportByTimecardResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "OvertimeReportByTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OvertimeReportByTimecardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 305
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 327
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;-><init>()V

    .line 328
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 329
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 335
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 333
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->next_cursor(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;

    goto :goto_0

    .line 332
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->by_timecard:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->calculation_total(Lcom/squareup/protos/client/timecards/CalculationTotal;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;

    goto :goto_0

    .line 339
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 340
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ProtoAdapter_OvertimeReportByTimecardResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 319
    sget-object v0, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 320
    sget-object v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->by_timecard:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 321
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->next_cursor:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 322
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    check-cast p2, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ProtoAdapter_OvertimeReportByTimecardResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;)I
    .locals 4

    .line 310
    sget-object v0, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 311
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->by_timecard:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->next_cursor:Ljava/lang/String;

    const/4 v3, 0x3

    .line 312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 303
    check-cast p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ProtoAdapter_OvertimeReportByTimecardResponse;->encodedSize(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;
    .locals 2

    .line 345
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;->newBuilder()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;

    move-result-object p1

    .line 346
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/timecards/CalculationTotal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/CalculationTotal;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    .line 347
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->by_timecard:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 348
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 349
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 303
    check-cast p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ProtoAdapter_OvertimeReportByTimecardResponse;->redact(Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    move-result-object p1

    return-object p1
.end method
