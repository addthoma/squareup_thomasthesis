.class final Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ClientAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4379
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4456
    new-instance v0, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 4457
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4458
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 4499
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4497
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial(Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4496
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial(Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4495
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account(Lcom/squareup/protos/client/ClientAction$LinkBankAccount;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4494
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4493
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ActivateAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account(Lcom/squareup/protos/client/ClientAction$ActivateAccount;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4492
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4491
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices(Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4490
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings(Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto :goto_0

    .line 4484
    :pswitch_8
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 4486
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/ClientAction$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 4481
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours(Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4480
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center(Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4479
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet(Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4478
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account(Lcom/squareup/protos/client/ClientAction$ViewBankAccount;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4477
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet(Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4476
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/client/ClientAction$CreateItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$CreateItem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->create_item(Lcom/squareup/protos/client/ClientAction$CreateItem;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4475
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet(Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4474
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation(Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4473
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center(Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4472
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet(Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4471
    :pswitch_13
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits(Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4470
    :pswitch_14
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet(Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4469
    :pswitch_15
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewDispute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewDispute;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute(Lcom/squareup/protos/client/ClientAction$ViewDispute;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4468
    :pswitch_16
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes(Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4467
    :pswitch_17
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report(Lcom/squareup/protos/client/ClientAction$ViewSalesReport;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4466
    :pswitch_18
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet(Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4465
    :pswitch_19
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet(Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4464
    :pswitch_1a
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice(Lcom/squareup/protos/client/ClientAction$ViewInvoice;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4463
    :pswitch_1b
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices(Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4462
    :pswitch_1c
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet(Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4461
    :pswitch_1d
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4460
    :pswitch_1e
    sget-object v3, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet(Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    goto/16 :goto_0

    .line 4503
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4504
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4377
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4420
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4421
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4422
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4423
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4424
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4425
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4426
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4427
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4428
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4429
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4430
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewDispute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4431
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4432
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4433
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4434
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4435
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4436
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4437
    sget-object v0, Lcom/squareup/protos/client/ClientAction$CreateItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4438
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4439
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4440
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4441
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4442
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4443
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4444
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4445
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4446
    sget-object v0, Lcom/squareup/protos/client/ClientAction$ActivateAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4447
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4448
    sget-object v0, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4449
    sget-object v0, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    const/16 v2, 0x1e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4450
    sget-object v0, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4451
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4377
    check-cast p2, Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction;)I
    .locals 4

    .line 4384
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    const/16 v2, 0x17

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    const/4 v3, 0x1

    .line 4385
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    const/4 v3, 0x2

    .line 4386
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    const/4 v3, 0x3

    .line 4387
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    const/4 v3, 0x4

    .line 4388
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    const/4 v3, 0x5

    .line 4389
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    const/4 v3, 0x6

    .line 4390
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    const/4 v3, 0x7

    .line 4391
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    const/16 v3, 0x8

    .line 4392
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    const/16 v3, 0x9

    .line 4393
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewDispute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    const/16 v3, 0xa

    .line 4394
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    const/16 v3, 0xb

    .line 4395
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    const/16 v3, 0xc

    .line 4396
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    const/16 v3, 0xd

    .line 4397
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    const/16 v3, 0xe

    .line 4398
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    const/16 v3, 0xf

    .line 4399
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    const/16 v3, 0x10

    .line 4400
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$CreateItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    const/16 v3, 0x11

    .line 4401
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    const/16 v3, 0x12

    .line 4402
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    const/16 v3, 0x13

    .line 4403
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    const/16 v3, 0x14

    .line 4404
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    const/16 v3, 0x15

    .line 4405
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    const/16 v3, 0x16

    .line 4406
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    const/16 v3, 0x18

    .line 4407
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    const/16 v3, 0x19

    .line 4408
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    const/16 v3, 0x1a

    .line 4409
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$ActivateAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    const/16 v3, 0x1b

    .line 4410
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    const/16 v3, 0x1c

    .line 4411
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    const/16 v3, 0x1d

    .line 4412
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    const/16 v3, 0x1e

    .line 4413
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/ClientAction;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    const/16 v3, 0x1f

    .line 4414
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4415
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4377
    check-cast p1, Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;->encodedSize(Lcom/squareup/protos/client/ClientAction;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/protos/client/ClientAction;
    .locals 2

    .line 4509
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction;->newBuilder()Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p1

    .line 4510
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 4511
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 4512
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 4513
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 4514
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 4515
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 4516
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 4517
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 4518
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 4519
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewDispute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewDispute;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 4520
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 4521
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 4522
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 4523
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 4524
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 4525
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 4526
    :cond_f
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/protos/client/ClientAction$CreateItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$CreateItem;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 4527
    :cond_10
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 4528
    :cond_11
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 4529
    :cond_12
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 4530
    :cond_13
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 4531
    :cond_14
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 4532
    :cond_15
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 4533
    :cond_16
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 4534
    :cond_17
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 4535
    :cond_18
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/squareup/protos/client/ClientAction$ActivateAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 4536
    :cond_19
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 4537
    :cond_1a
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 4538
    :cond_1b
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 4539
    :cond_1c
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    iput-object v0, p1, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    .line 4540
    :cond_1d
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4541
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4377
    check-cast p1, Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ProtoAdapter_ClientAction;->redact(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/protos/client/ClientAction;

    move-result-object p1

    return-object p1
.end method
